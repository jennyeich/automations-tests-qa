#!/bin/bash
TEST_PROP=$1

wget http://stedolan.github.io/jq/download/linux64/jq
chmod +x ./jq
sudo cp jq /usr/bin
echo $TEST_PROP > temptest.json
cat temptest.json | jq '.'
# retrieve the first test from the json
numOfTests=$(jq '. | length' temptest.json)
echo 'num of tests to run:'
echo $numOfTests

package=$(cat temptest.json | jq '.[0].pkg' | tr -d '"')
echo $package
className=$(cat temptest.json | jq '.[0].className' | tr -d '"')
echo $className
methodName=$(cat temptest.json | jq '.[0].testcase' | tr -d '"')
echo $methodName
fullTestClass=${package}"."$className
echo $fullTestClass

testToRun=${fullTestClass}"#"$methodName
testToRun=\"${testToRun}\"
echo $testToRun

command="-Dtest=$testToRun"
echo $command
mvn test $command