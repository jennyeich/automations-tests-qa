############################
# Job start script
# Please provide to arguments : job name, and test name
#
#!/bin/bash



JOB_NAME=$1
TEST_NAME=$2
PROFILE=$3

cd /home/ubuntu/workspace/$JOB_NAME/
mkdir -p /home/ubuntu/workspace/$JOB_NAME/certificates
cp /home/ubuntu/workspace/*p12 /home/ubuntu/workspace/$JOB_NAME/certificates
if [ "$PROFILE" = "" ]
then
    echo run without profile: mvn clean test -Dtest=$TEST_NAME
    mvn clean test -Dtest=$TEST_NAME
else
    echo run with profile: mvn clean test -Dtest=$TEST_NAME -P $PROFILE -DfailIfNoTests=false
    mvn clean test -Dtest=$TEST_NAME -P $PROFILE -DfailIfNoTests=false
fi

