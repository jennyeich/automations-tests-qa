package mobile;

import org.junit.Assert;
import org.junit.Test;
import utils.PropsReader;

import java.util.concurrent.TimeUnit;

/**
 * Created by tatiana on 4/04/17.
 */

public class TimeMeasurement {
    String platform = ComoMobileAppiumHelper.IOS;

    @Test
    public void testTimeMeasurement() {
        // platform = System.getProperty("platform");TODO check how to get param from command line
        TimeWatch mTimeWatch;
        mTimeWatch = TimeWatch.start();
        ComoMobileAppiumHelper element = ComoMobileAppiumHelper.getInstance(platform);

        //start app
        if (ComoMobileAppiumHelper.IOS.equals(platform))
            element.getOkElem().click();

        try {
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        element.getTapToStartElem().click();

        long loadTime = mTimeWatch.time("TimeMeasurement: appLoadTime");

        //  fail if loadTime > 14000 ms
        Assert.assertTrue(loadTime > 14000);
    }
}
