package environments;

import com.google.common.collect.Lists;
import hub.base.BaseService;
import hub.common.objects.common.TimeUnits;
import hub.common.objects.oldObjects.BudgetTypeOldAction;
import hub.common.objects.oldObjects.OldAction;
import hub.common.objects.oldObjects.OldAutomation;
import hub.common.objects.smarts.ISmartAssetCounter;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.SmartClubDeal;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.smarObjects.Scenario;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.*;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftActions;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.common.objects.smarts.smarObjects.triggers.OldAutomationTriggers;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.discounts.BaseHub1Discount;
import hub.discounts.DiscountsConstants;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SendAsset;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.services.Navigator;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.basePages.loyalty.cases.RuleCase;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.PointsAccumulationAction;
import hub.hub2_0.common.actions.dealActions.*;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.builders.CasesConditionsBuilder;
import hub.hub2_0.common.conditions.AssetCondition;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.cases.GeneralCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.ShoppingCartOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.common.items.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.automations.OldAutomationService;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.utils.HubOldAutomationCreator;
import integration.base.BaseHub2DealTest;
import org.apache.commons.collections.map.HashedMap;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.*;

import static hub.base.BaseService.checkForAlerts;
import static hub.smartAutomations.BaseAutomationTest.JOINED_CLUB;
import static integration.base.BaseHub2DealTest.createEntireTicketDiscountDeal;
import static integration.base.BaseHub2DealTest.dealBuilder;
import static integration.base.BaseRuleHub2Test.ruleBuilder;

/**
 * Created by Goni on 10/31/2017.
 */
public class IntegrationEnvInitializer extends BaseDemoBusinessInitializer{


    private static final String DEFAULT_SCENARIO_NAME = null;
    private static final String DEFAULT_ACTION_NAME = null;
    private static final String TEN_PERCENT_OFF = "10 percent off";
    private static final String ON_741852_ITEM = "1+1 on 741852 item";
    private static final String ITEM_CODE_57501 = "Item code 57501";
    private static final String $_10_DISCOUNT = "$10 discount";
    private static final String ITEM_CODE_258741 = "Item Code 258741";
    private static final String DEAL_CODE_456987 = "Deal Code 456987";
    private static final String ITEM_CODE_146984 = "Item Code 146984";
    private static final String ITEM_CODES_258741_246813B = "Item codes 258741 & 246813b";
    private static final String CLUB_DEAL_1_PLUS_1_ITEM_CODE_963852 = "1+1 on 963852 item";
    private static final String CODE_963852 = "963852";
    private static final String CODE_741852 = "741852";
    private static final String CODE_258741 = "258741";
    private static final String CODE_146984 ="146984";


    private static final String PUNCH_CARD_MEGA_COFFEE = "Punch Card Mega Coffee";
    private static final String PUNCH_ITEMS = "Punch Items";
    private static final String FREE_MEGA_MACCHIATO = "FREE Mega Macchiato";
    private static final String PUNCH_JOIN = "Punch Join";
    private static final String FREE_MAC = "Free Mac";
    private static final String YOU_HAVE_EARNED_A_FREE_MEGA_MACCHIATO = "You have earned a Free MEGA Macchiato!";
    private static final String FREE_MEGA_COFFEE = "Free Mega Coffee";
    private static final String CODE_456987 = "456987";
    private static final IntConditionDetails NO_CONDITION_DETAILS = null;
    private static final String GENERAL_5_PERCENT = "General 5 Percent";
    private static final String RENEW_GIFT = "General 5 Percent";
    private static final String SMART_PUNCH_CARD = "Smart punch card";
    private static final String JOINED_THE_CLUB = "Joined the club";
    private static final String GROUP_NAME_1PLUS1_OFFERS = "1+1 offers";
    private static final String CLUB_DEAL_1PLUS1 = "1+1 club deal";



    @BeforeClass
    public static void setup() throws Exception {
        init();
        // currently all buisnesses in production have no smart action in the menu. In order to support the test we g
        addSmartAutomationToBuisness();
        Navigator.refresh();
        log4j.info("HUB login succeeded...");
    }

    private static void addSmartAutomationToBuisness() throws Exception {
        Navigator.operationsTabPermissionSettings();
        List<WebElement> locationPlans=  BaseService.getDriver().findElements(By.className("color_pack_primary"));
        Iterator<WebElement> iter = locationPlans.iterator();

        while(iter.hasNext()) {
            WebElement we = iter.next();

            if (we.getText().equals("BP for New Businesses")) {
                WebElement toTrash = we.findElement(By.className("icon-trash_can"));
                toTrash.click();
                checkForAlerts();
                //dismissAreYouSureMsg();
            }
        }
    }


    public static void dismissAreYouSureMsg() throws Exception {
        try {
            Alert ale = BaseService.getDriver().switchTo().alert();
            ale.accept();
        } catch (Exception e) {
            throw new Exception("Failed to dismiss the Alert message");
        }

    }




    @Test
    public void createPointsAccumulationAutomationHub1() throws Exception{
        OldAutomation oldAutomation = buildOldAutomation("Points accumulation",OldAutomationTriggers.MakesPurchase,"10000");

        OldAction action = HubOldAutomationCreator.buildPointsCreditAccumulationOldAction(oldAutomation, "1","0", BudgetTypeOldAction.Credit);
        oldAutomation.addAction(action,oldAutomation);
        OldAutomationService.createAutomation(oldAutomation);
    }

    @Test
    // create the Gift and automation/Rules for Hub1+Hub2
    public void createSmartGiftsAndSmartAutomation() throws Exception {

        log4j.info("Creating Smart gifts");
        createSmartGiftWithDiscountPercentOff(TEN_PERCENT_OFF, "General 10 Percent Off, Enjoy!", DEFAULT_SCENARIO_NAME, DEFAULT_ACTION_NAME, TEN_PERCENT_OFF, "10");
        createSmartGiftWithDiscountAmountOff($_10_DISCOUNT, $_10_DISCOUNT, $_10_DISCOUNT, DEFAULT_ACTION_NAME, "$10 discount", "10");

        createSmartGiftWithAddItemCode(ITEM_CODE_57501, ITEM_CODE_57501, DEFAULT_SCENARIO_NAME, DEFAULT_ACTION_NAME, Lists.newArrayList("57501"));
        createSmartGiftWithAddItemCode(ITEM_CODE_258741, "Response item code 258741", DEFAULT_SCENARIO_NAME, "ADD ITEM CODE 258741", Lists.newArrayList(CODE_258741));
        createSmartGiftWithAddItemCode(ITEM_CODES_258741_246813B, "Response Item codes 258741 & 246813b", ITEM_CODES_258741_246813B, "ADD ITEM CODES 258741 & 246813B", Lists.newArrayList(CODE_258741, "246813b"));
        createSmartGiftWithAddItemCode(ITEM_CODE_146984, "response item code 146984", "ADD ITEM CODE 146984", "ADD ITEM CODE 146984", Lists.newArrayList(CODE_146984));
        createSmartGiftWithAddDealCode(DEAL_CODE_456987, "Response deal code 456987", "Add deal code 456987", "ADD DEAL CODE 456987", Lists.newArrayList(CODE_456987));

        createSmartGiftWithGroup();

        log4j.info("Creating Smart automations");

        //Hub1 Smart Automations
        createRegistrationSmartAutomationsHub1();
        log4j.info("Finished create Registration smart automation Hub1");
        createRenewGiftsAutomationHub1();
        log4j.info("Finished create Renew Gift smart automation Hub1");

        //Hub2 rules
        //createJoinTheClubRuleHub2();
        //createRenewGiftsRuleHub2();

    }

    @Test
    public void createSmartClubDealGeneral5PercentHub1() throws Exception {
        createSmartClubDealWithDiscountPercentOff(GENERAL_5_PERCENT, "General 5 Percent to all club members", GENERAL_5_PERCENT, NO_CONDITION_DETAILS,  GENERAL_5_PERCENT.toUpperCase(),
                "General 5 Percent to all club members", "5");
    }

    @Test
    public void createSmartClubDealGeneral5PercentHub2() throws Exception {
        Box box = dealBuilder.buildBox(GENERAL_5_PERCENT);
        LoyaltyService.createBox(box);

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        globalCondition.clickAllMembersBtn();
        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = BaseHub2DealTest.buildPercentOffDiscountOnEntireTicket("5");
        createEntireTicketDiscountDeal(box, Lists.newArrayList(globalCondition),Lists.newArrayList(percentOffDiscountEntireTicketAction));
    }


    @Test
    public void createSmartClubDeal1Plus1Hub1() throws Exception{

        SmartClubDeal smartClubDeal = hubAssetsCreator.buildSmartClubDeal(CLUB_DEAL_1_PLUS_1_ITEM_CODE_963852, CLUB_DEAL_1_PLUS_1_ITEM_CODE_963852);
        setExpirationDate(smartClubDeal);
        Scenario scenario = hubAssetsCreator.createSmartAssetScenario(smartClubDeal,CLUB_DEAL_1_PLUS_1_ITEM_CODE_963852);

        createSmartAssetCondition(smartClubDeal, CLUB_DEAL_1PLUS1, CODE_963852);
        SmartGiftAction smartGiftAction = hubAssetsCreator.createSmartGiftAction(SmartGiftActions.AddDiscount.toString());
        //create discount smartGiftAction
        AddDiscountAction discount = BaseHub1Discount.createAddDiscountAction(smartClubDeal,  CLUB_DEAL_1_PLUS_1_ITEM_CODE_963852, DiscountTypeOperation.FreeItem.toString());

        FreeItem freeItem = new FreeItem(smartClubDeal);
        SetDiscountOnItemsCheckBox checkBox = new SetDiscountOnItemsCheckBox(smartClubDeal);
        freeItem.setSetDiscountOnItemsCheckBox(checkBox);
        SetDiscountOnItems d1 = BaseHub1Discount.createSetDiscountOnTimes(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "1", DiscountsConstants.DISCOUNT_OFF);
        freeItem.addSetDiscountOnItems(d1, smartClubDeal);
        createGroupForBusket(smartClubDeal, d1, CLUB_DEAL_1PLUS1, GroupField.ItemCode,GroupOperators.Equals,CODE_963852);

        SetDiscountOnItems d2 = BaseHub1Discount.createSetDiscountOnTimes(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "1", DiscountsConstants.DISCOUNT_ON);
        freeItem.addSetDiscountOnItems(d2, smartClubDeal);
        //Create new group
        createGroupForBusket(smartClubDeal, d2, CLUB_DEAL_1PLUS1, GroupField.ItemCode,GroupOperators.Equals,CODE_963852);
        BaseHub1Discount.createDiscount(smartClubDeal, smartGiftAction, scenario, freeItem, discount);
    }

    @Test
    public void createSmartClubDeal1Plus1Hub2() throws Exception{
        Box box = dealBuilder.buildBox(CLUB_DEAL_1_PLUS_1_ITEM_CODE_963852);
        LoyaltyService.createBox(box);

        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        CodesGroup itemCodes = new CodesGroup();
        itemCodes.setCodes(CodesGroup.ITEM_CODES_GROUP, CODE_963852);
        String groupName = "CLUB_DEAL_1PLUS1";
        itemCodes.clickSaveAsGroupBtn(groupName, NewGroup.SAVE);
        selectItems.setItemCodes(itemCodes);
        selectItems.clickApply();

        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItems);
        occurrencesCondition.setConditionValue("2");

        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickSpecificItemsFromBundleButton();
        selectBundle.clickApplyButton();

        MakeItemsFreeAction makeItemsFreeAction = BaseHub2DealTest.buildMakeItemFreeAction(selectBundle);
        BaseHub2DealTest.createAdvancedDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(occurrencesCondition), Lists.newArrayList(makeItemsFreeAction));
    }


    @Test
    public void createSmartPunchCardAutomationsHub1() throws Exception {
        createPunchCardWithAutogeneratedCodes(FREE_MEGA_MACCHIATO, FREE_MAC, FREE_MEGA_COFFEE, "5", "4", "4", YOU_HAVE_EARNED_A_FREE_MEGA_MACCHIATO);
        createSmartAutomationNoCondition(PUNCH_JOIN, Triggers.JoinsTheClub, SmartAutomationActions.SendAnAsset, FREE_MEGA_MACCHIATO, null);


        IntConditionDetails conditionDetails = new IntConditionDetails(ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, IntOperator.Int_Equals_enum.GreaterOrEqualsTo, "1");

        createSmartAutomationWithOccurrences(PUNCH_CARD_MEGA_COFFEE, Triggers.SubmitAPurchase, conditionDetails, SmartAutomationActions.PunchThePunchCard, FREE_MEGA_MACCHIATO, null,
                "1", 1, ForEachTotalType.TotalQuantity, null, PUNCH_ITEMS, GroupField.ItemCode,GroupOperators.Equals, "11111");
    }
 // need to check if it fails.
 //   @Test
    public void CreateSmartPunchCardRulesHub2() throws Exception {

        Box box = dealBuilder.buildBox(SMART_PUNCH_CARD);

        createPunchCardWithAutogeneratedCodes(FREE_MEGA_MACCHIATO, FREE_MAC, FREE_MEGA_COFFEE, "5", "4", "4", YOU_HAVE_EARNED_A_FREE_MEGA_MACCHIATO);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, "Join the club - Send punch card" + timestamp);
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.JoinTheClub.toString(), box, rule, Lists.newArrayList(ActivityActions.SendBenefit), FREE_MEGA_MACCHIATO);
        LoyaltyService.createActivityInBox(activity);

        //Item group of item code "11111" (punch items)
        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        CodesGroup itemCodes = new CodesGroup();
        itemCodes.setCodes(CodesGroup.ITEM_CODES_GROUP, "11111");
        itemCodes.clickSaveAsGroupBtn(PUNCH_ITEMS + timestamp, NewGroup.SAVE);
        selectItems.setItemCodes(itemCodes);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        //Activity condition "Shopping cart" "Contains at least" "1" "punch items" (The item group)
        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_AT_LEAST)
                .setConditionValue("1")
                .setSelectItems(selectItems);

        //Per occurrences with "Punch items" item group in the quantity of 1
        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItems);
        occurrencesCondition.setConditionValue("1");

        //Rule build
        Rule rule2 = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, "Make a purchase - Punch per occ" + timestamp);
        Activity activity2 = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(), box, rule2, Collections.singletonList(condition)
                , Collections.singletonList(occurrencesCondition), Lists.newArrayList(ActionsBuilder.buildPunchAPunchCardAction(FREE_MEGA_MACCHIATO, "1")));
        LoyaltyService.createActivityInBox(activity2);
    }



    private void createRenewGiftsAutomationHub1() throws Exception {

        timestamp = String.valueOf(System.currentTimeMillis());
        Settings settings = new Settings();
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(Triggers.RedeemAnAsset, "Renew gifts", settings);

        createScenario(TEN_PERCENT_OFF,automation);
        createScenario(ON_741852_ITEM,automation);
        createScenario($_10_DISCOUNT,automation);
        createScenario(ITEM_CODE_57501,automation);
        createScenario(ITEM_CODE_258741,automation);
        createScenario(ITEM_CODES_258741_246813B,automation);
        createScenario(ITEM_CODE_146984,automation);
        createScenario(DEAL_CODE_456987,automation);
        SmartAutomationService.createSmartAutomation(automation);
    }

    //HUB2
    private void createRenewGiftsRuleHub2() throws Exception {
        Box box = dealBuilder.buildBox(RENEW_GIFT);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, "Renew gifts" + timestamp);

        Case case1 = createCase(TEN_PERCENT_OFF);
        Case case2 = createCase(ON_741852_ITEM);
        Case case3 = createCase($_10_DISCOUNT);
        Case case4 = createCase(ITEM_CODE_57501);
        Case case5 = createCase(ITEM_CODE_258741);
        Case case6 = createCase(ITEM_CODES_258741_246813B);
        Case case7 = createCase(ITEM_CODE_146984);
        Case case8 = createCase(DEAL_CODE_456987);

        Activity activity = ruleBuilder.buildActivityWithCases(ActivityTrigger.RedeemsAnAsset.toString(), box, rule, Lists.newArrayList(),
                Lists.newArrayList(case1, case2, case3, case4, case5, case6, case7, case8));

        LoyaltyService.createActivityInBox(activity);
    }

    private void createSmartGiftWithGroup() throws Exception{

        SmartGift smartGift = hubAssetsCreator.buildSmartGift(ON_741852_ITEM, ON_741852_ITEM, ON_741852_ITEM);
        setExpirationDate(smartGift);
        setRedeemAutoGenerateCode(smartGift, "52", TimeUnits.WEEKS);
        Scenario scenario = hubAssetsCreator.createSmartAssetScenario(smartGift,"Buy one Get one");

        createSmartAssetCondition(smartGift, GROUP_NAME_1PLUS1_OFFERS, CODE_741852);

        SmartGiftAction smartGiftAction = hubAssetsCreator.createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = BaseHub1Discount.createAddDiscountAction(smartGift,  ON_741852_ITEM, DiscountTypeOperation.FreeItem.toString());

        FreeItem freeItem = new FreeItem(smartGift);
        SetDiscountOnItemsCheckBox checkBox = new SetDiscountOnItemsCheckBox(smartGift);
        freeItem.setSetDiscountOnItemsCheckBox(checkBox);
        SetDiscountOnItems d1 = BaseHub1Discount.createSetDiscountOnTimes(smartGift, DiscountItems_ForEach.quantity.toString(), "1", DiscountsConstants.DISCOUNT_OFF);
        freeItem.addSetDiscountOnItems(d1, smartGift);

        createGroupForBusket(smartGift, d1, GROUP_NAME_1PLUS1_OFFERS, GroupField.ItemCode,GroupOperators.Equals,CODE_741852);

        SetDiscountOnItems d2 = BaseHub1Discount.createSetDiscountOnTimes(smartGift, DiscountItems_ForEach.quantity.toString(), "1", DiscountsConstants.DISCOUNT_ON);
        freeItem.addSetDiscountOnItems(d2, smartGift);
        //Create new group
        createGroupForBusket(smartGift, d2, GROUP_NAME_1PLUS1_OFFERS, GroupField.ItemCode,GroupOperators.Equals,CODE_741852);
        BaseHub1Discount.createDiscount(smartGift, smartGiftAction, scenario, freeItem, discount);
    }

    private void createGroupForBusket(ISmartAssetCounter smartAsset, SetDiscountOnItems d,String groupName,GroupField groupField, GroupOperators groupOperators,String value) {
        Group group = hubAssetsCreator.createSmartAssetGroupWithConds(groupName, smartAsset, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartAsset, groupField, groupOperators.toString(), value, group);
    }


    private void createSmartAssetCondition(ISmartAssetCounter smartAsset, String groupName, String itemCode) throws Exception {
        IntOperator equal = hubAssetsCreator.getIntAssetOperator(smartAsset, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        ConditionFieldOprValue cond1 = hubAssetsCreator.createConditionNoGroup(smartAsset, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "2");
        smartAsset.getScenarios().get(0).addCondition(cond1, smartAsset);

        Group group = hubAssetsCreator.createGroup(smartAsset,groupName);
        hubAssetsCreator.createGroupCondition(smartAsset, GroupField.ItemCode, GroupOperators.Equals.toString(), itemCode, group);
        cond1.setGroup(group);
    }


    private void createRegistrationSmartAutomationsHub1() throws Exception {
        Settings settings = new Settings();
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(Triggers.JoinsTheClub, "Registration automations", settings);
        SmartAutomationScenario smartAutomationscenario = new SmartAutomationScenario();
        smartAutomationscenario.setScenarioName("Send Assets Points and Credit");
        automation.addScenario(smartAutomationscenario);

        Map<SmartAutomationAction,ISmartAction> smartActionMap = new LinkedHashMap<>();
        smartActionMap.put(buildAutomationAction("GENERAL 10 PERCENT DISCOUNT", SmartAutomationActions.SendAnAsset.toString()),buildSendAsset(automation,TEN_PERCENT_OFF));
        smartActionMap.put(buildAutomationAction(ON_741852_ITEM, SmartAutomationActions.SendAnAsset.toString()),buildSendAsset(automation, ON_741852_ITEM));
        smartActionMap.put(buildAutomationAction(ITEM_CODE_57501, SmartAutomationActions.SendAnAsset.toString()),buildSendAsset(automation, ITEM_CODE_57501));
        smartActionMap.put(buildAutomationAction(ITEM_CODE_258741, SmartAutomationActions.SendAnAsset.toString()),buildSendAsset(automation, ITEM_CODE_258741));
        smartActionMap.put(buildAutomationAction(ITEM_CODE_146984, SmartAutomationActions.SendAnAsset.toString()),buildSendAsset(automation, ITEM_CODE_146984));
        smartActionMap.put(buildAutomationAction(ITEM_CODES_258741_246813B, SmartAutomationActions.SendAnAsset.toString()),buildSendAsset(automation, ITEM_CODES_258741_246813B));
        smartActionMap.put(buildAutomationAction(DEAL_CODE_456987, SmartAutomationActions.SendAnAsset.toString()),buildSendAsset(automation, DEAL_CODE_456987));
        smartActionMap.put(buildAutomationAction("Points", SmartAutomationActions.AddSetNumberofPoints.toString()),buildAddPoints(automation,"20000",AmountType.Points));
        smartActionMap.put(buildAutomationAction("Credit", SmartAutomationActions.AddSetNumberofPoints.toString()),buildAddPoints(automation,"20000",AmountType.Credit));

        createSmartAutomations(smartAutomationscenario, automation,smartActionMap);

        SmartAutomationService.createSmartAutomation(automation);
    }

    //HUB2
    private void createJoinTheClubRuleHub2() throws Exception {
        Box box = dealBuilder.buildBox(JOINED_THE_CLUB);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, "Registration automations" + timestamp);

        ActivityAction activityAction1 = ActionsBuilder.buildAction(ActivityActions.SendBenefit.toString(), TEN_PERCENT_OFF);
        ActivityAction activityAction2 = ActionsBuilder.buildAction(ActivityActions.SendBenefit.toString(), ON_741852_ITEM);
        ActivityAction activityAction3 = ActionsBuilder.buildAction(ActivityActions.SendBenefit.toString(), ITEM_CODE_57501);
        ActivityAction activityAction4 = ActionsBuilder.buildAction(ActivityActions.SendBenefit.toString(), ITEM_CODE_258741);
        ActivityAction activityAction5 = ActionsBuilder.buildAction(ActivityActions.SendBenefit.toString(), ITEM_CODE_146984);
        ActivityAction activityAction6 = ActionsBuilder.buildAction(ActivityActions.SendBenefit.toString(), ITEM_CODES_258741_246813B);
        ActivityAction activityAction7 = ActionsBuilder.buildAction(ActivityActions.SendBenefit.toString(), DEAL_CODE_456987);
        ActivityAction activityAction8 = ActionsBuilder.buildAction(ActivityActions.PointsAccumulation.toString(), "200000");
        PointsAccumulationAction pointsAccumulationAction = new PointsAccumulationAction();
        pointsAccumulationAction.clickPointsAccumulationBtn();
        pointsAccumulationAction.setNumOfPoints("20000");
        pointsAccumulationAction.setAccumulationUnits(PointsAccumulationAction.AccumulationUnits.CREDIT);

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.JoinTheClub.toString(), box, rule, Lists.newArrayList(),
                Lists.newArrayList(activityAction1, activityAction2, activityAction3, activityAction4, activityAction5, activityAction6,
                        activityAction7, activityAction8, pointsAccumulationAction));

        LoyaltyService.createActivityInBox(activity);

    }




    private void createScenario(String assetName, SmartAutomation automation) throws Exception{

        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal, assetName);
        Map<SmartAutomationActions,String> actionMap = new HashedMap();
        actionMap.put(SmartAutomationActions.SendAnAsset, assetName);
        cond.setName(assetName);

        automationsHelper.buildSmartAutomationScenario(assetName,automation,Lists.newArrayList(cond), false, actionMap,assetName, "");

    }


    private void createSmartAutomations(SmartAutomationScenario smartAutomationscenario, SmartAutomation automation, Map<SmartAutomationAction, ISmartAction> smartActionMap) {

        for (SmartAutomationAction action:smartActionMap.keySet()) {
            smartAutomationscenario.addAction(action,automation);
            ISmartAction smartAction = smartActionMap.get(action);
            if(smartAction instanceof SendAsset){
                action.setSendAsset((SendAsset) smartAction);
            }else if(smartAction instanceof AddPoints){
                action.setAddPoints((AddPoints) smartAction);
            }
        }
    }

    private SmartAutomationAction buildAutomationAction(String actionName,String performTheAction ) {
        SmartAutomationAction smartAction = new SmartAutomationAction();
        smartAction.setActionName(actionName);
        smartAction.setPerformTheAction(performTheAction);
        return smartAction;
    }

    private ISmartAction buildSendAsset(SmartAutomation automation,String assetName) {
        SendAsset sendAsset =  new SendAsset(automation);
        sendAsset.setAssetName(assetName);
        return sendAsset;
    }

    private ISmartAction buildAddPoints(SmartAutomation automation,String numOfPoints,AmountType amountType) {
        AddPoints  points = new AddPoints(automation);
        points.setAmountType(amountType.toString());
        points.setNumOfPoints(numOfPoints);
        return points;

    }

    //HUB2
    private Case createCase(String assetName) throws Exception {
        GeneralCondition generalCondition = CasesConditionsBuilder
                .buildGeneralConditionReceivedAnAsset(AssetCondition.ConditionKey.BENEFIT, InputOperators.IS_EXACTLY, assetName);

        RuleCase aCase = new RuleCase();
        aCase.setConditions(Lists.newArrayList(generalCondition));
        ActivityAction activityAction1 = ActionsBuilder.buildSendBenefitAction(assetName);
        aCase.setActions(Lists.newArrayList(activityAction1));
        return aCase;
    }

}
