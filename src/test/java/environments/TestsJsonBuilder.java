package environments;

import com.google.appengine.repackaged.org.codehaus.jackson.map.ObjectMapper;
import hub.base.BaseService;
import integration.base.BaseRuleHub2Test;
import integration.base.BaseIntegrationTest;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import server.base.BaseServerTest;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;

/**
 * Created by Goni on 12/26/2017.
 */

public class TestsJsonBuilder {


    public static final Logger log4j = Logger.getLogger(TestsJsonBuilder.class);

    public static void main(String[] args) {

        List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());
        List<TestMapper> testMappers = getTestMappersForServer(classLoadersList);
        testMappers.addAll(getTestMappersForServerWithHub(classLoadersList));
        testMappers.addAll(getTestMappersForHub(classLoadersList));
        testMappers.addAll(getTestMappersForHub2(classLoadersList));
        testMappers.addAll(getTestMappersForIntegration(classLoadersList));
        writeJsonToFile(testMappers);

    }


    private static List<TestMapper> getTestMappersForServer(List<ClassLoader> classLoadersList) {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false), new ResourcesScanner(), new MethodAnnotationsScanner(),new TypeAnnotationsScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("server"))));
        Set<Class<? extends BaseServerTest>> classes = reflections.getSubTypesOf(BaseServerTest.class);

        List<TestMapper> testMappers = new ArrayList<>();
        for (Class classInstance : classes) {

                if(classInstance.getName().contains("v2_8")) {
                    Method[] methods = classInstance.getDeclaredMethods();
                    for (int i=0; i< methods.length; i++) {
                        Method m = methods[i];
                        if(m.isAnnotationPresent(org.junit.Test.class)) {
                            log4j.info("method: " + classInstance.getName() + "." + m.getName());
                            TestMapper testMapper = buildTestMapper(false,false,"qa","SERVER 2.8 (" + classInstance.getPackage().getName() + ")",classInstance.getPackage().getName(),classInstance.getSimpleName(),m.getName(),1);
                            testMappers.add(testMapper);
                        }
                    }
                }
            else if(classInstance.getName().contains("v4")) {
                Method[] methods = classInstance.getDeclaredMethods();
                for (int i=0; i< methods.length; i++) {
                    Method m = methods[i];
                    if(m.isAnnotationPresent(org.junit.Test.class) && !classInstance.getPackage().getName().contains("sla")) {

                        log4j.info("method: " + classInstance.getName() + "." + m.getName());
                        TestMapper testMapper = buildTestMapper(false,false,"qa","SERVER 4.0 (" + classInstance.getPackage().getName() + ")",classInstance.getPackage().getName(),classInstance.getSimpleName(),m.getName(),1);
                        testMappers.add(testMapper);
                    }
                }
            }

            else if(classInstance.getName().contains("internal")) {
                Method[] methods = classInstance.getDeclaredMethods();
                for (int i=0; i< methods.length; i++) {
                    Method m = methods[i];
                    if(m.isAnnotationPresent(org.junit.Test.class)) {
                        log4j.info("method: " + classInstance.getName() + "." + m.getName());
                        TestMapper testMapper = buildTestMapper(false,false,"qa","SERVER Internal (" + classInstance.getPackage().getName() + ")",classInstance.getPackage().getName(),classInstance.getSimpleName(),m.getName(),1);
                        testMappers.add(testMapper);
                    }
                }
            }
        }
        return testMappers;
    }

    private static List<TestMapper> getTestMappersForServerWithHub(List<ClassLoader> classLoadersList) {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false), new ResourcesScanner(), new MethodAnnotationsScanner(),new TypeAnnotationsScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("server"))));
        Set<Class<? extends BaseIntegrationTest>> classes = reflections.getSubTypesOf(BaseIntegrationTest.class);

        List<TestMapper> testMappers = new ArrayList<>();
        for (Class classInstance : classes) {

            if(classInstance.getName().contains("v4")) {
                Method[] methods = classInstance.getDeclaredMethods();
                for (int i=0; i< methods.length; i++) {
                    Method m = methods[i];
                    if(m.isAnnotationPresent(org.junit.Test.class) && !classInstance.getPackage().getName().contains("sla")) {

                        log4j.info("method: " + classInstance.getName() + "." + m.getName());
                        TestMapper testMapper = buildTestMapper(false,false,"qa","SERVER-HUB 4.0 (" + classInstance.getPackage().getName() + ")",classInstance.getPackage().getName(),classInstance.getSimpleName(),m.getName(),1);
                        testMappers.add(testMapper);
                    }
                }
            }
        }
        return testMappers;
    }
    private static List<TestMapper> getTestMappersForHub(List<ClassLoader> classLoadersList) {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false), new ResourcesScanner(), new MethodAnnotationsScanner(),new TypeAnnotationsScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("hub"))));
        Set<Class<? extends BaseIntegrationTest>> classes = reflections.getSubTypesOf(BaseIntegrationTest.class);

        List<TestMapper> testMappers = new ArrayList<>();
        for (Class classInstance : classes) {

            if(classInstance.getName().contains("discounts")) {
                Method[] methods = classInstance.getDeclaredMethods();
                for (int i=0; i< methods.length; i++) {
                    Method m = methods[i];
                    if(m.isAnnotationPresent(org.junit.Test.class)) {
                        log4j.info("method: " + classInstance.getName() + "." + m.getName());
                        TestMapper testMapper = buildTestMapper(false,false,"qa","HUB: Discounts (" + classInstance.getPackage().getName() + ")",classInstance.getPackage().getName(),classInstance.getSimpleName(),m.getName(),1);
                        testMappers.add(testMapper);
                    }
                }
            }
            else if(classInstance.getName().contains("smartAutomations")) {
                Method[] methods = classInstance.getDeclaredMethods();
                for (int i=0; i< methods.length; i++) {
                    Method m = methods[i];
                    if(m.isAnnotationPresent(org.junit.Test.class)) {
                        log4j.info("method: " + classInstance.getName() + "." + m.getName());
                        TestMapper testMapper = buildTestMapper(false,false,"qa","HUB: Smart Automations (" + classInstance.getPackage().getName() + ")",classInstance.getPackage().getName(),classInstance.getSimpleName(),m.getName(),1);
                        testMappers.add(testMapper);
                    }
                }
            }
            else if(classInstance.getName().contains("webTests")) {
                Method[] methods = classInstance.getDeclaredMethods();
                for (int i=0; i< methods.length; i++) {
                    Method m = methods[i];
                    if(m.isAnnotationPresent(org.junit.Test.class)) {
                        log4j.info("method: " + classInstance.getName() + "." + m.getName());
                        TestMapper testMapper = buildTestMapper(false,false,"qa","HUB: webTests (" + classInstance.getPackage().getName() + ")",classInstance.getPackage().getName(),classInstance.getSimpleName(),m.getName(),1);
                        testMappers.add(testMapper);
                    }
                }
            }
        }
        return testMappers;
    }

    private static List<TestMapper> getTestMappersForHub2(List<ClassLoader> classLoadersList) {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false), new ResourcesScanner(), new MethodAnnotationsScanner(),new TypeAnnotationsScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("hub2_0"))));
        Set<Class<? extends BaseRuleHub2Test>> classes = reflections.getSubTypesOf(BaseRuleHub2Test.class);

        List<TestMapper> testMappers = new ArrayList<>();
        for (Class classInstance : classes) {

            if(classInstance.getName().contains("actions")) {
                Method[] methods = classInstance.getDeclaredMethods();
                for (int i=0; i< methods.length; i++) {
                    Method m = methods[i];
                    if(m.isAnnotationPresent(org.junit.Test.class)) {
                        log4j.info("method: " + classInstance.getName() + "." + m.getName());
                        TestMapper testMapper = buildTestMapper(false,false,"qa","HUB2: Actions (" + classInstance.getPackage().getName() + ")",classInstance.getPackage().getName(),classInstance.getSimpleName(),m.getName(),1);
                        testMappers.add(testMapper);
                    }
                }
            }
            else if(classInstance.getName().contains("triggers")) {
                Method[] methods = classInstance.getDeclaredMethods();
                for (int i=0; i< methods.length; i++) {
                    Method m = methods[i];
                    if(m.isAnnotationPresent(org.junit.Test.class)) {
                        log4j.info("method: " + classInstance.getName() + "." + m.getName());
                        TestMapper testMapper = buildTestMapper(false,false,"qa","HUB2: Triggers (" + classInstance.getPackage().getName() + ")",classInstance.getPackage().getName(),classInstance.getSimpleName(),m.getName(),1);
                        testMappers.add(testMapper);
                    }
                }
            } else if(classInstance.getName().contains("deals")) {
                Method[] methods = classInstance.getDeclaredMethods();
                for (int i=0; i< methods.length; i++) {
                    Method m = methods[i];
                    if(m.isAnnotationPresent(org.junit.Test.class)) {
                        log4j.info("method: " + classInstance.getName() + "." + m.getName());
                        TestMapper testMapper = buildTestMapper(false,false,"qa","HUB2: Deals (" + classInstance.getPackage().getName() + ")",classInstance.getPackage().getName(),classInstance.getSimpleName(),m.getName(),1);
                        testMappers.add(testMapper);
                    }
                }
            } else if(classInstance.getName().contains("templates")) {
                Method[] methods = classInstance.getDeclaredMethods();
                for (int i=0; i< methods.length; i++) {
                    Method m = methods[i];
                    if(m.isAnnotationPresent(org.junit.Test.class)) {
                        log4j.info("method: " + classInstance.getName() + "." + m.getName());
                        TestMapper testMapper = buildTestMapper(false,false,"qa","HUB2: Templates (" + classInstance.getPackage().getName() + ")",classInstance.getPackage().getName(),classInstance.getSimpleName(),m.getName(),1);
                        testMappers.add(testMapper);
                    }
                }
            }else if(classInstance.getName().contains("activityActions")) {
                Method[] methods = classInstance.getDeclaredMethods();
                for (int i=0; i< methods.length; i++) {
                    Method m = methods[i];
                    if(m.isAnnotationPresent(org.junit.Test.class)) {
                        log4j.info("method: " + classInstance.getName() + "." + m.getName());
                        TestMapper testMapper = buildTestMapper(false,false,"qa","HUB2: Activity Actions (" + classInstance.getPackage().getName() + ")",classInstance.getPackage().getName(),classInstance.getSimpleName(),m.getName(),1);
                        testMappers.add(testMapper);
                    }
                }
            }else if(classInstance.getName().contains("gifts")) {
                Method[] methods = classInstance.getDeclaredMethods();
                for (int i=0; i< methods.length; i++) {
                    Method m = methods[i];
                    if(m.isAnnotationPresent(org.junit.Test.class)) {
                        log4j.info("method: " + classInstance.getName() + "." + m.getName());
                        TestMapper testMapper = buildTestMapper(false,false,"qa","HUB2: Gifts (" + classInstance.getPackage().getName() + ")",classInstance.getPackage().getName(),classInstance.getSimpleName(),m.getName(),1);
                        testMappers.add(testMapper);
                    }
                }
            }else if(classInstance.getName().contains("settings")) {
                Method[] methods = classInstance.getDeclaredMethods();
                for (int i=0; i< methods.length; i++) {
                    Method m = methods[i];
                    if(m.isAnnotationPresent(org.junit.Test.class)) {
                        log4j.info("method: " + classInstance.getName() + "." + m.getName());
                        TestMapper testMapper = buildTestMapper(false,false,"qa","HUB2: Settings (" + classInstance.getPackage().getName() + ")",classInstance.getPackage().getName(),classInstance.getSimpleName(),m.getName(),1);
                        testMappers.add(testMapper);
                    }
                }
            }
        }
        return testMappers;
    }

    private static List<TestMapper> getTestMappersForIntegration(List<ClassLoader> classLoadersList) {
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false), new ResourcesScanner(), new MethodAnnotationsScanner(),new TypeAnnotationsScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("integration"))));
        Set<Class<? extends BaseIntegrationTest>> classes = reflections.getSubTypesOf(BaseIntegrationTest.class);

        List<TestMapper> testMappers = new ArrayList<>();
        for (Class classInstance : classes) {

            if (classInstance.getName().contains("base"))
                continue;

            Method[] methods = classInstance.getDeclaredMethods();
            for (int i = 0; i < methods.length; i++) {
                Method m = methods[i];
                if (m.isAnnotationPresent(org.junit.Test.class)) {
                    log4j.info("method: " + classInstance.getName() + "." + m.getName());
                    TestMapper testMapper = buildTestMapper(false, false, "qa", "INTEGRATION Tests (" + classInstance.getPackage().getName() + ")", classInstance.getPackage().getName(), classInstance.getSimpleName(), m.getName(), 1);
                    testMappers.add(testMapper);
                }
            }
        }

        return testMappers;
    }


    private static TestMapper buildTestMapper(Boolean enabled,Boolean suite, String owner, String testgroup, String pkg, String className,String testcase, Integer multiply){
        TestMapper testMapper = new TestMapper();
        testMapper.setEnabled(enabled);
        testMapper.setSuite(suite);
        testMapper.setMultiply(multiply);
        testMapper.setOwner(owner);
        testMapper.setPkg(pkg);
        testMapper.setClassName(className);
        testMapper.setTestcase(testcase);
        testMapper.setTestgroup(testgroup);
        return testMapper;
    }

    private static void writeJsonToFile(List<TestMapper> testMappers){

        JSONArray list = new JSONArray() ;
        ObjectMapper mapper = new ObjectMapper();
        for (int i = 0; i < testMappers.size(); i++) {
            TestMapper testMapper = testMappers.get(i);
            list.put(testMapper);
        }

        try {

            // Writing to a file
            mapper.writeValue(new File(BaseService.resourcesDir + File.separator + "tests.json"),list);

        } catch (IOException e) {
            e.printStackTrace();
        }



    }
    //"enabled":false,"suite":true,"owner":"qa","testgroup":"SERVER TESTS CancelBudget2.8","pkg":"server.v2_8.CancelBudgetTest","testcase":"testCancelBudgetPaymentWithPaymentUID","multiply":2
    private static class TestMapper{
        Boolean enabled;
        Boolean suite;
        String owner;
        String testgroup;
        String pkg;
        String className;
        String testcase;
        Integer multiply;

        public String getClassName() {
            return className;
        }

        public void setClassName(String className) {
            this.className = className;
        }

        public Boolean getEnabled() {
            return enabled;
        }

        public void setEnabled(Boolean enabled) {
            this.enabled = enabled;
        }

        public Boolean getSuite() {
            return suite;
        }

        public void setSuite(Boolean suite) {
            this.suite = suite;
        }

        public String getOwner() {
            return owner;
        }

        public void setOwner(String owner) {
            this.owner = owner;
        }

        public String getTestgroup() {
            return testgroup;
        }

        public void setTestgroup(String testgroup) {
            this.testgroup = testgroup;
        }

        public String getPkg() {
            return pkg;
        }

        public void setPkg(String pkg) {
            this.pkg = pkg;
        }

        public String getTestcase() {
            return testcase;
        }

        public void setTestcase(String testcase) {
            this.testcase = testcase;
        }

        public Integer getMultiply() {
            return multiply;
        }

        public void setMultiply(Integer multiply) {
            this.multiply = multiply;
        }
    }



    /**
     * Finds all package names starting with prefix
     * @return Set of package names
     */
    public Set<String> findAllPackagesStartingWith(String prefix) {
        List<ClassLoader> classLoadersList = new LinkedList<ClassLoader>();
        classLoadersList.add(ClasspathHelper.contextClassLoader());
        classLoadersList.add(ClasspathHelper.staticClassLoader());
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setScanners(new SubTypesScanner(false), new ResourcesScanner())
                .setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
                .filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix("server"))));
        Set<Class<? extends Object>> classes = reflections.getSubTypesOf(Object.class);

        Set<String> packageNameSet = new TreeSet<String>();
        for (Class classInstance : classes) {
            String packageName = classInstance.getPackage().getName();
            if (packageName.startsWith(prefix)) {
                packageNameSet.add(packageName);
            }
        }
        return packageNameSet;
    }

    private static List<String> findPackageNamesStartingWith(String prefix) {
        List<String> result = new ArrayList<>();
        for(Package p : Package.getPackages()) {
            if (p.getName().startsWith(prefix)) {
                result.add(p.getName());
            }
        }
        return result;
    }

    /**
     * Attempts to list all the classes in the specified package as determined
     * by the context class loader
     *
     * @param pckgname
     *            the package name to search
     * @return a list of classes that exist within that package
     * @throws ClassNotFoundException
     *             if something went wrong
     */
    private static List<Class> getClassesForPackage(String pckgname) throws ClassNotFoundException {
        // This will hold a list of directories matching the pckgname. There may be more than one if a package is split over multiple jars/paths
        ArrayList<File> directories = new ArrayList<File>();
        try {
            ClassLoader cld = Thread.currentThread().getContextClassLoader();
            if (cld == null) {
                throw new ClassNotFoundException("Can't get class loader.");
            }
            String path = pckgname.replace('.', '/');
            // Ask for all resources for the path
            Enumeration<URL> resources = cld.getResources(path);
            while (resources.hasMoreElements()) {
                directories.add(new File(URLDecoder.decode(resources.nextElement().getPath(), "UTF-8")));
            }
        } catch (NullPointerException x) {
            throw new ClassNotFoundException(pckgname + " does not appear to be a valid package (Null pointer exception)");
        } catch (UnsupportedEncodingException encex) {
            throw new ClassNotFoundException(pckgname + " does not appear to be a valid package (Unsupported encoding)");
        } catch (IOException ioex) {
            throw new ClassNotFoundException("IOException was thrown when trying to get all resources for " + pckgname);
        }

        ArrayList<Class> classes = new ArrayList<Class>();
        // For every directory identified capture all the .class files
        for (File directory : directories) {
            if (directory.exists()) {
                // Get the list of the files contained in the package
                String[] files = directory.list();
                for (String file : files) {
                    // we are only interested in .class files
                    if (file.endsWith(".class")) {
                        // removes the .class extension
                        try
                        {
                            classes.add(Class.forName(pckgname + '.' + file.substring(0, file.length() - 6)));
                        }
                        catch (NoClassDefFoundError e)
                        {
                            // do nothing. this class hasn't been found by the loader, and we don't care.
                        }
                    }
                }
            } else {
                throw new ClassNotFoundException(pckgname + " (" + directory.getPath() + ") does not appear to be a valid package");
            }
        }
        return classes;
    }


}

