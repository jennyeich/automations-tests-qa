package environments;

import com.google.common.collect.ImmutableMap;
import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.Job;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JenkinsConfigurator {

    public static void main(String[] args) throws Exception{

        JenkinsServer jenkinsServer = new JenkinsServer(new URI("http://54.77.156.99/"),"automation","Auto2018!");
        List<String> jobs = getJobsList(jenkinsServer,"QA-Nightly","_nightly");


//        enableJobs(jenkinsServer,jobs);
//        disableJobs(jenkinsServer,jobs);
         updateJobConfiguration(jenkinsServer, jobs);

    }


    private static void updateJobConfiguration(JenkinsServer jenkinsServer, List<String> jobs) throws Exception {

        for (String jobName:jobs) {

            String jobConfig = jenkinsServer.getJobXml(jobName);
            Document new_job_config = replaceCommand(jobConfig,"");
            System.out.println("jobName: " + jobName);
            updateJob(jobName, new_job_config, jenkinsServer);
        }

    }


    private static void updateJobsInList(JenkinsServer jenkinsServer, List<String> jobs) throws Exception {
        Map<String,String> jobCommand = new HashMap<>();

        for (String jobName:jobs) {

            String job_config = jenkinsServer.getJobXml(jobName);
            String command = getJobExecuterLine(job_config);
            if(command.indexOf("#") != -1){
                command = command.substring(1);
            }
            System.out.println("command: " + command);
            jobCommand.put(jobName ,command);

        }

        //select one job that you would like to copy its configuration
        String jobConfig = jenkinsServer.getJobXml("Discounts-Smart-ClubDealsPercentOffDiscount-BestForBusiness1_nightly_light");
        for (String jobName:jobCommand.keySet()) {
                String oldCommand = jobCommand.get(jobName);
                System.out.println("old command: " + oldCommand);
                Document new_job_config = replaceCommand(jobConfig, oldCommand);
                //createNewJob(jobName, new_job_config, jenkinsServer);
                //updateJob(jobName, new_job_config, jenkinsServer);
        }
    }


    private static void enableJobs(JenkinsServer jenkinsServer ,List<String> jobs) throws IOException {
        for (String jobName:jobs) {
            System.out.println("jobName: " + jobName);
            jenkinsServer.enableJob(jobName);
        }
    }


    private static void disableJobs(JenkinsServer jenkinsServer ,List<String> jobs) throws IOException {
        for (String jobName:jobs) {
            System.out.println("jobName: " + jobName);
            jenkinsServer.disableJob(jobName);
        }
    }

    private static void deleteJobs(JenkinsServer jenkinsServer ,List<String> jobs) throws IOException {
        for (String jobName:jobs) {
            System.out.println("jobName: " + jobName);
            jenkinsServer.deleteJob(jobName);
        }
    }

    private static String getJobExecuterLine(String job_config) throws SAXException, ParserConfigurationException, IOException {

        Document doc = toXmlDocument(job_config);
        NodeList nList = doc.getElementsByTagName("project");
        Element project = (Element) nList.item(0);
        Element builders = (Element) project.getElementsByTagName("builders").item(0);
        Element task = (Element)  builders.getElementsByTagName("hudson.tasks.Shell").item(0);
        Element commandElm = (Element)  task.getElementsByTagName("command").item(0);
        String[] command = commandElm.getTextContent().split("\n");
        return command[command.length-1];
    }

    private static List<String> getJobsList(JenkinsServer server,String dashboardName, String jobNamePattern) throws IOException {

        Map<String,Job> jobs = server.getJobs(dashboardName);
        List<String> job_list = new ArrayList<>();
        for (String jobName:jobs.keySet()) {

            if(jobName.equals("hub2-sanity-triggerSanityOnGitChanges"))
                continue;
            if(jobName.contains(jobNamePattern)){
                System.out.println("jobName: " + jobName);
                System.out.println("job_name_pattern: " + jobNamePattern);
                job_list.add(jobName);
            }

        }
        return job_list;
    }

    private static Document replaceCommand(String job_config, String new_execute_command) throws Exception {

        Document doc = toXmlDocument(job_config);
        NodeList nList = doc.getElementsByTagName("project");
        Element project = (Element) nList.item(0);
        //update description
        Element description = (Element) project.getElementsByTagName("description").item(0);
        description.setTextContent("RUN: " +  new_execute_command);

        //update git branch
        Element scm = (Element) project.getElementsByTagName("scm").item(0);
        Element branches = (Element)  scm.getElementsByTagName("branches").item(0);
        Element spec = (Element)  branches.getElementsByTagName("hudson.plugins.git.BranchSpec").item(0);
        Element name = (Element)  spec.getElementsByTagName("name").item(0);
        name.setTextContent("develop");

        //update scheduler
        Element triggers = (Element)project.getElementsByTagName("triggers").item(0);
        Element hudson = (Element) triggers.getElementsByTagName("hudson.triggers.TimerTrigger").item(0);
        Element specElm = (Element) hudson.getElementsByTagName("spec").item(0);
        specElm.setTextContent("TZ=Asia/Jerusalem\nH H(01-02) * * *");

        //update the command line to execute
//        Element builders = (Element) project.getElementsByTagName("builders").item(0);
//        Element task = (Element)  builders.getElementsByTagName("hudson.tasks.Shell").item(0);
//        Element command = (Element)  task.getElementsByTagName("command").item(0);
//        String[] commandLine = command.getTextContent().split("\n");
//        commandLine[commandLine.length-1] = new_execute_command;
//        System.out.println("new command: " + new_execute_command);

//        String newCommand = org.apache.commons.lang3.StringUtils.join(commandLine,"\n");
//        command.setTextContent(newCommand);
        return doc;
    }

    private static void createNewJob(String job_name, Document job_config, JenkinsServer server) throws IOException, TransformerException {
        String xml = toXmlString(job_config);
        server.createJob(job_name, xml);
    }

    private static void updateJob(String job_name, Document job_config, JenkinsServer server) throws IOException, TransformerException {
        String xml = toXmlString(job_config);
        server.updateJob(job_name, xml);
    }


    private static String toXmlString(Document document) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(document);
        StringWriter strWriter = new StringWriter();
        StreamResult result = new StreamResult(strWriter);

        transformer.transform(source, result);

        return strWriter.getBuffer().toString();

    }

    private static Document toXmlDocument(String str) throws ParserConfigurationException, SAXException, IOException{

        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
        Document document = docBuilder.parse(new InputSource(new StringReader(str)));

        return document;
    }
}
