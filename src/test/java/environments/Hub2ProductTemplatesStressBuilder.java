package environments;

import hub.base.BaseHubTest;
import hub.hub2_0.basePages.loyalty.templates.TemplateVerticals;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub2_0.utils.DateTimeUtility;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * This class creates ~50 rule product templates and 40 deal product templates.
 * This class should only trigger once for each environment!
 */
public class Hub2ProductTemplatesStressBuilder extends Hub2StressBuilder {

    public static final Logger log4j = Logger.getLogger(Hub2ProductTemplatesStressBuilder.class);

    @BeforeClass
    public static void createNewBox() throws Exception {
        init();
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }


    @Test//only run one time
    @ru.yandex.qatools.allure.annotations.Description("Create product templates with specific trigger for each vertical + 1 for All verticals")
    public void testCreateProductTemplatesWithRuleForNewBusiness() throws Exception {

        int random = DateTimeUtility.randomInt(1,100);
        int verticals = TemplateVerticals.values().length;
        for (int j=0; j< verticals ;j++) {
            TemplateVerticals vertical = TemplateVerticals.values()[j];
            createTemplateJoinClub(TemplateType.Product, vertical,j+random);
            createTemplateMadeAPurchase(TemplateType.Product, vertical,j+random);
            createTemplateRedeemsAnAsset(TemplateType.Product, vertical,j+random);
            createTemplateTagged(TemplateType.Product, vertical,j+random);
            createTemplateUpdatedMembershipDetails(TemplateType.Product, vertical,j+random);

        }

        random = DateTimeUtility.randomInt(1,100);
        //create template Make purchase + send asset action (asset name will be empty and later we will edit the activity
        ActivityAction activityAction = ActionsBuilder.buildSendBenefitAction("");
        LoyaltyService.createProductTemplateWithAllVerticals(box, ActivityTrigger.MadeAPurchase, activityAction, ActivityTrigger.MadeAPurchase.toString() + random);

    }

    @Test//only run one time
    @ru.yandex.qatools.allure.annotations.Description("Create product templates with specific deal for each vertical + 1 for All verticals")
    public void testCreateProductTemplatesWithDealForNewBusiness() throws Exception {

        int random = DateTimeUtility.randomInt(1,100);
        int verticals = TemplateVerticals.values().length;
        for (int j=0; j< verticals ;j++) {
            TemplateVerticals vertical = TemplateVerticals.values()[j];
            createTemplateEntireTicketDiscount10percentAndSendItemCode(TemplateType.Product, vertical,j+random);
            createTemplateSpecificItemsAmountOffAndPercentOffDiscounts(TemplateType.Product, vertical,j+random);
            createTemplateSpecificItemsDiscountsTripleDiscountActions(TemplateType.Product, vertical,j+random);
            createTemplateSpecificItemsFreeItemAndSpecialPriceWithCondition(TemplateType.Product, vertical,j+random);
        }

    }


    @AfterClass
    public static void tearDown(){

        log4j.info("close browser after finish test");
        cleanBase();
        BaseHubTest.cleanHub();
    }

}
