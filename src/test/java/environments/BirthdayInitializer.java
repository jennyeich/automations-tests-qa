package environments;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.BaseService;
import hub.common.objects.smarts.SmartBirthdayAndAnniversaryAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.common.objects.smarts.smarObjects.triggers.BirthdayAnniversaryAutomationTriggers;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.ApplyIf;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Condition;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Membership;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.TagOperator;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.BirthdayAnniversarySettings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.Navigator;
import hub.services.AcpService;
import hub.services.NewApplicationService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.StringUtils;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;

public class BirthdayInitializer extends BaseAutomationTest {

    private static final String IMPORT_FOLDER = "import";
    private static final String BIRTHDAY_FOLDER = "birthday";
    private static final String IMPORT_FILENAME_SUFFIX = "Import.csv";
    private static String BDayPrefix = "";


    @BeforeClass
    public static void setup() throws IOException {
        //the prefix is passed from command line as an argument and represent the prefix of the file to import
//        BDayPrefix = System.getProperty("BDayPrefix");
//        System.out.println("BDayPrefix: " + BDayPrefix);
//        NewApplicationService.setBDayPrefix(BDayPrefix);
        isNewApplication = false; //comment out when running 'createNewAppByFile' test or 'createNewAppWith4BDayAutomations' test
        init();
        log4j.info("HUB login succeeded...");
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {


        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new BirthdayAnniversarySettings();
            tag = "tag_" + timestamp;

        }
    };



    //@Test
    public void createNewAppByFile() throws Exception{
        String pathToFile = IMPORT_FOLDER + File.separator + BDayPrefix + IMPORT_FILENAME_SUFFIX;
        log4j.info("Starting import: " + pathToFile);
        AcpService.importMembers(pathToFile);
        log4j.info("Finish import");
        createBirthdayAnniversaryAutomationTagMemberAction(BirthdayAnniversaryAutomationTriggers.MatchingBirthdayMonthOnly,"BirthdayMonthOnly_");

    }




    //@Test
    public void createNewAppWith4BDayAutomations() throws Exception{
        log4j.info("Starting import");
        AcpService.importMembers(IMPORT_FOLDER + File.separator + BDayPrefix + IMPORT_FILENAME_SUFFIX);
        log4j.info("Finish import");
        createBirthdayAnniversaryAutomationTagMemberAction(BirthdayAnniversaryAutomationTriggers.MatchingBirthdayMonthOnly,"BirthdayMonthOnly_");
        createBirthdayAnniversaryAutomationTagMemberAction(BirthdayAnniversaryAutomationTriggers.MatchingBirthdayMonthAndDate,"BirthdayMonthAndDate_");
        createBirthdayAnniversaryAutomationTagMemberAction(BirthdayAnniversaryAutomationTriggers.MatchingAnniversaryMonthOnly,"AnniversaryMonthOnly_");
        createBirthdayAnniversaryAutomationTagMemberAction(BirthdayAnniversaryAutomationTriggers.MatchingAnniversaryMonthAndDate,"AnniversaryMonthAndDate_");


    }

    //@Test
    public void addNewSmartBirthdayAutomationsToExistingBusiness() throws Exception{
        int from = Integer.valueOf(System.getProperty("from")).intValue();
        int to = Integer.valueOf(System.getProperty("to")).intValue();

        String fileLocations = StringUtils.readFile(BaseService.resourcesDir + BIRTHDAY_FOLDER +  File.separator +"BirthdayLocationsActive.csv");
        String[] locations = fileLocations.split(",");

       for (int i = (from-1); i < to; i++) {
            String location = locations[i];
            String url2 = MessageFormat.format("https://qa.como.com/dashboard/#/operations/users-fields-automations?location_id={0}",location);
            Navigator.getDriver().get(url2);
            Navigator.getDriver().navigate().to(url2);
            smartGift = hubAssetsCreator.createSmartGift(timestamp);
            String url = MessageFormat.format("https://qa.como.com/dashboard/#/operations/users-fields-automations?location_id={0}",location);
            Navigator.getDriver().get(url);
            Navigator.getDriver().navigate().to(url);
            Navigator.waitForLoad();
            createBirthdayAnniversaryAutomationWithAction(BirthdayAnniversaryAutomationTriggers.MatchingBirthdayMonthOnly,"BirthdayMonthOnly_",Lists.newArrayList(SmartAutomationActions.SendAnAsset,SmartAutomationActions.SendAPushNotification));
            Navigator.waitForLoad();
        }
    }

    private void createBirthdayAnniversaryAutomationWithAction(BirthdayAnniversaryAutomationTriggers trigger,String name,List<SmartAutomationActions> actions) throws Exception {

        timestamp = String.valueOf(System.currentTimeMillis());
        tag = "tag_" + timestamp;
        //Create smart automation
        SmartBirthdayAndAnniversaryAutomation automation = hubAssetsCreator.createSmartBirthdayAnniversaryAutomation(trigger,name + timestamp,((BirthdayAnniversarySettings)settings));

        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtHours("10");
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtMinutes("30");
//        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
//        StringBuffer stringBuffer = new StringBuffer();
//        stringBuffer.append("b_1412");
        //Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(), AND, actions);
    }

    //@Test
    public void deactivateSmartBirthdayAutomations() throws Exception{

        String fileLocations = StringUtils.readFile(BaseService.resourcesDir + BIRTHDAY_FOLDER +  File.separator +"BirthdayLocationsActive.csv");
        String[] locations = fileLocations.split(",");
        for (int i = 0; i < locations.length; i++) {
            String location = locations[i];
            log4j.info("In location: " + location);
            String url = MessageFormat.format("https://qa.como.com/dashboard/#/operations/users-fields-automations?location_id={0}",location);
            Navigator.getDriver().get(url);
            Navigator.getDriver().navigate().to(url);
            Thread.sleep(2000);
            Navigator.refresh();
            List<WebElement> elemList = Navigator.getDriver().findElements(By.xpath("//input[contains(@class,\"ng-not-empty\") and starts-with(@id,\"active_automation_\")]"));
            log4j.info("Number of automations to deactivate: " + elemList.size());
            for (int j = 0; j <elemList.size() ; j++) {
                WebElement element = elemList.get(j);
                BaseService.executeJavascript("arguments[0].click();", element);
                Navigator.waitForLoad();
            }

        }
    }

    @Test
    public void activateSmartBirthdayAutomations() throws Exception{

        String fileLocations = StringUtils.readFile(BaseService.resourcesDir + BIRTHDAY_FOLDER +  File.separator +"BirthdayLocationsActive.csv");
        String[] locations = fileLocations.split(",");
        for (int i = 0; i < locations.length; i++) {
            String location = locations[i];
            log4j.info("In location: " + location);
            String url = MessageFormat.format("https://qa.como.com/dashboard/#/operations/users-fields-automations?location_id={0}",location);
            Navigator.getDriver().get(url);
            Navigator.getDriver().navigate().to(url);
            Thread.sleep(2000);
            Navigator.refresh();
            List<WebElement> elemList = Navigator.getDriver().findElements(By.xpath("//input[not(contains(@class,\"ng-not-empty\")) and starts-with(@id,\"active_automation_\")]"));
            log4j.info("Number of automations to activate: " + elemList.size());
            for (int j = 0; j <elemList.size() ; j++) {
                WebElement element = elemList.get(j);
                BaseService.executeJavascript("arguments[0].click();", element);
                Navigator.waitForLoad();
            }

        }
    }

    private void createBirthdayAnniversaryAutomationTagMemberAction(BirthdayAnniversaryAutomationTriggers trigger, String name) throws Exception {

        timestamp = String.valueOf(System.currentTimeMillis());
        tag = "tag_" + timestamp;
        //Create smart automation
        SmartBirthdayAndAnniversaryAutomation automation = hubAssetsCreator.createSmartBirthdayAnniversaryAutomation(trigger,name + timestamp,((BirthdayAnniversarySettings)settings));

        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtHours("15");
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtMinutes("45");
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("b_1412");
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
    }


    @Override
    public Triggers getTrigger() {
        return null;
    }

    @Override
    public String getAutomationName() {
        return null;
    }


}
