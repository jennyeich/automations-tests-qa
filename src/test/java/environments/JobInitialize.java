package environments;

import org.apache.log4j.Logger;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;

import static org.junit.Assert.assertTrue;

/**
* Created by: Sharon aruce
 * The purpose of this class is only to run dummy test in order to initialize the job to PASS for the first time
 * (this is due to jenkins issue which doesn't show the statistics until there is one PASS.)
 */

public class JobInitialize {
    public static final Logger log4j = Logger.getLogger(JobInitialize.class);
    @Description("Dummy test in order to make the Job PASS for the first time")
        @Test
        public void testInitJob() throws Exception{
            log4j.info("Going to run dummy test in order to initialize the job to Pass!");
            assertTrue("dummy test for job initialize failed!!!", true);

        }

    }
