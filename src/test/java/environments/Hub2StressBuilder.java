package environments;

import com.google.common.collect.Lists;
import hub.base.BaseHubTest;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.SmartPunchCard;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Deal;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.basePages.loyalty.cases.RuleCase;
import hub.hub2_0.basePages.loyalty.templates.SaveProductTemplate;
import hub.hub2_0.basePages.loyalty.templates.TemplateCategories;
import hub.hub2_0.basePages.loyalty.templates.TemplateVerticals;
import hub.hub2_0.basePages.loyalty.templates.TemplatesLibrary;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.dealActions.*;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.builders.DealBuilder;
import hub.hub2_0.common.builders.RuleBuilder;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.conditions.AssetCondition;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.PointsCondition;
import hub.hub2_0.common.conditions.cases.GeneralCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.limits.RollingPeriod;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.conditions.operators.ListOperators;
import hub.hub2_0.common.enums.ActivityActions;
import hub.hub2_0.common.enums.ActivityStatus;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.ActivityType;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.utils.HubAssetsCreator;
import hub.hub2_0.common.builders.CasesConditionsBuilder;
import integration.base.BaseHub2DealTest;
import integration.base.BaseIntegrationTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Arrays;

public class Hub2StressBuilder extends BaseIntegrationTest {

    public static final Logger log4j = Logger.getLogger(Hub2StressBuilder.class);
    private static final int NUM_OF_TEMPLATES_PER_TRIGGER = 10;

    private static String timestamp;
    private static SmartPunchCard smartPunchCard;
    private static String tag = "tag_";
    protected HubAssetsCreator hubAssetsCreator = new HubAssetsCreator();
    protected static RuleBuilder ruleBuilder = new RuleBuilder();
    protected static DealBuilder dealBuilder = new DealBuilder();
    protected static Box box;
    protected static String templateName;

    protected enum TemplateType{
        Product("Product"),
        Business("Business")
        ;
        private final String name;
        TemplateType(final String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    @BeforeClass
    public static void createNewBox() throws Exception {
        init();
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());

        }
    };

    @Test
    @ru.yandex.qatools.allure.annotations.Description("Create 10 business templates for each specific trigger - 5 different triggers (total 50)  + 10 business templates with 4 different deals (total 40)")
    public void testCreateBusinessTemplatesForNewBusiness() throws Exception {

        for (int i = 0; i < NUM_OF_TEMPLATES_PER_TRIGGER; i++) {
            createTemplateJoinClub(TemplateType.Business,null,i);
            createTemplateMadeAPurchase(TemplateType.Business,null,i);
            createTemplateRedeemsAnAsset(TemplateType.Business,null,i);
            createTemplateTagged(TemplateType.Business,null,i);
            createTemplateUpdatedMembershipDetails(TemplateType.Business,null,i);

            createTemplateEntireTicketDiscount10percentAndSendItemCode(TemplateType.Business, null,i);
            createTemplateSpecificItemsAmountOffAndPercentOffDiscounts(TemplateType.Business, null,i);
            createTemplateSpecificItemsDiscountsTripleDiscountActions(TemplateType.Business, null,i);
            createTemplateSpecificItemsFreeItemAndSpecialPriceWithCondition(TemplateType.Business, null,i);
        }
    }


    @Test
    @ru.yandex.qatools.allure.annotations.Description("Create Rule with 3 cases")
    public void testCreateRuleWith3Cases() throws Exception {

        final String BRANCH_ID_1 = "1";
        final String BRANCH_ID_2 = "2";
        final String BRANCH_ID_3 = "3";

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Draft, "RuleWith3Cases");

        /****case1****/
        GeneralCondition generalCondition = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID_1);
        Case aCase = new RuleCase();
        aCase.setConditions(Lists.newArrayList(generalCondition));
        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction("TAG_1");
        aCase.setActions(Lists.newArrayList(activityAction1));


        /****case2****/
        GeneralCondition generalCondition2 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID_2);
        Case aCase2 = new RuleCase();
        aCase2.setConditions(Lists.newArrayList(generalCondition2));
        ActivityAction activityAction2 = ActionsBuilder.buildTagMemberAction("TAG_2");
        aCase2.setActions(Lists.newArrayList(activityAction2));


        /****case3****/
        GeneralCondition generalCondition3 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID_3);
        Case aCase3 = new RuleCase();
        aCase3.setConditions(Lists.newArrayList(generalCondition3));
        ActivityAction activityAction3 = ActionsBuilder.buildTagMemberAction("TAG_3");
        aCase3.setActions(Lists.newArrayList(activityAction3));
        //create activity with 3 cases
        Activity activity = ruleBuilder.buildActivityWithCases(ActivityTrigger.MadeAPurchase.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(aCase,aCase2,aCase3));

        LoyaltyService.createActivityInBox(activity);
        log4j.info("rule name:" +rule.getActivityName());

    }

    @Test
    @ru.yandex.qatools.allure.annotations.Description("Create Rule with 10 actions")
    public void testCreateRuleWith10Actions() throws Exception {

        tag = tag + timestamp.toString();
        SmartGift smartGift = hubAssetsCreator.createSmartGift(timestamp);
        smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");
        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("20");


        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityStatus.Draft, ActivityTrigger.UsePoints.toString());
        ActivityAction action1 = ActionsBuilder.buildPunchAPunchCardAction(smartPunchCard.getTitle());
        ActivityAction action2 = ActionsBuilder.buildSendBenefitAction(smartGift.getTitle());
        ActivityAction action3 = ActionsBuilder.buildTagMemberAction(tag);
        ActivityAction action4 = ActionsBuilder.buildTagMemberAction("GOLD");
        ActivityAction action5 = ActionsBuilder.buildSendPushAction("send PUSH for TEST STRESS");
        ActivityAction action6 = ActionsBuilder.buildTagMemberAction("TEST STRESS");
        ActivityAction action7 = ActionsBuilder.buildExportEventAction("ExportAction_" + timestamp,"https://services-dot-comoqa.appspot.com/echo");
        ActivityAction action8 = ActionsBuilder.buildSendPushAction("send PUSH for testCreateRuleWith10Actions");
        ActivityAction action9 = ActionsBuilder.buildSendTextMessageAction("send text SMS for test testCreateRuleWith10Actions");
        ActivityAction action10 = ActionsBuilder.buildPointsAccumulationAction("222");
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.UsePoints.toString(), box, rule, Lists.newArrayList(pointsCondition),
                Lists.newArrayList(action1,action2,action3,action4,action5,action6,action7,action8,action9,action10));

        LoyaltyService.createActivityInBox(activity);

    }


    @Test
    @ru.yandex.qatools.allure.annotations.Description("Create 10 campaigns - each campaign with 40 rules and 40 deals")
    public void testCreate800RulesAndDealsForBusiness() throws Exception {

        SmartGift smartGift = hubAssetsCreator.createSmartGift(timestamp);
        Activity activity;

        for (int i = 1; i <= 10; i++) { //create 10 campaigns - each campaign with 40 rules and 40 deals

            box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
            LoyaltyService.createBox(box);


            for (int j = 1; j <= 20; j++) {

                if( (j%2) == 0){

                    //for even j - create UsePoints trigger
                    timestamp = String.valueOf(System.currentTimeMillis());
                    tag = tag + timestamp;
                    PointsCondition pointsCondition = new PointsCondition();
                    pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                            setOperatorKey(IntOperators.IS_GREATER_THAN)
                            .setConditionValue(String.valueOf(j*50));

                    Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Draft, ActivityTrigger.UsePoints.toString());
                    ActivityAction action = ActionsBuilder.buildTagMemberAction(tag);
                    activity = ruleBuilder.buildActivity(ActivityTrigger.UsePoints.toString(), box, rule, Lists.newArrayList(pointsCondition), Lists.newArrayList(action));
                    LoyaltyService.createActivityInBox(activity);


                    //for even j - create deal SpecificItemsAmountOffAndPercentOffDiscounts
                    activity = buildDealSpecificItemsAmountOffAndPercentOffDiscounts("SpecificItemsAmount_");
                    activity.clickPublishActivityBtn();
                    LoyaltyService.createActivityInBox(activity);

                }else{
                   //for odd j - create Redeem trigger
                    AssetCondition assetCondition = new AssetCondition();
                    assetCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                            .setConditionValue(smartGift.getTitle());

                    Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Draft, ActivityTrigger.RedeemsAnAsset.toString());
                    ActivityAction action = ActionsBuilder.buildSendTextMessageAction("Hello , you have redeemed your gift!");
                    activity = ruleBuilder.buildActivity(ActivityTrigger.RedeemsAnAsset.toString(), box, rule, Lists.newArrayList(assetCondition),Lists.newArrayList(action));
                    LoyaltyService.createActivityInBox(activity);


                    //for odd j - create deal SpecificItemsFreeItemAndSpecialPriceWithCondition
                    activity= buildDealSpecificItemsFreeItemAndSpecialPriceWithCondition("SpecificItemsFreeItem_");
                    activity.clickPublishActivityBtn();
                    LoyaltyService.createActivityInBox(activity);
                }


            }
        }
        activity = buildDealEntireTicketDiscount10percentAndSendItemCode("EntireTicketDiscount10percent_");
        activity.clickPublishActivityBtn();
        LoyaltyService.createActivityInBox(activity);

    }

    protected void createTemplateMadeAPurchase(TemplateType templateType, TemplateVerticals vertical,int suffixIndex) throws Exception {
        timestamp = String.valueOf(System.currentTimeMillis());
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.MadeAPurchase.toString() + "_" + timestamp, ActivityTrigger.MadeAPurchase.toString());

        templateName = ActivityTrigger.MadeAPurchase.toString() + "_" + suffixIndex;
        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(InputOperators.IS_ONE_OF).setConditionValue("1,2,3");

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(), box, rule, Arrays.asList(condition), Lists.newArrayList(ActivityActions.Tag), "tag by purchase_" + timestamp);

        saveTemplateByType(activity, templateType, vertical, TemplateCategories.None.toString());
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();
    }


    protected void createTemplateJoinClub(TemplateType templateType, TemplateVerticals vertical,int suffixIndex) throws Exception {
        timestamp = String.valueOf(System.currentTimeMillis());
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,  ActivityTrigger.JoinTheClub.toString() + "_" + timestamp,  ActivityTrigger.JoinTheClub.toString());

        templateName = ActivityTrigger.JoinTheClub.toString() + "_" + suffixIndex;
        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.AllowSMS.toString()).toggleBooleanValue(ActivityCondition.BOOLEAN_YES);
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.JoinTheClub.toString(), box, rule, Arrays.asList(globalCondition), Lists.newArrayList(ActivityActions.Tag), null);

        saveTemplateByType(activity, templateType, vertical, TemplateCategories.Winter.toString());
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();
    }

    protected void createTemplateRedeemsAnAsset(TemplateType templateType, TemplateVerticals vertical,int suffixIndex) throws Exception {
        timestamp = String.valueOf(System.currentTimeMillis());
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.RedeemsAnAsset.toString() + "_" + timestamp, ActivityTrigger.RedeemsAnAsset.toString());

        SmartGift smartGift1 = hubAssetsCreator.createSmartGift(String.valueOf(System.currentTimeMillis()));
        //Add condition
        AssetCondition assetCondition = new AssetCondition();
        assetCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setConditionValue(smartGift1.getTitle());

        templateName = ActivityTrigger.RedeemsAnAsset.toString() + "_" + suffixIndex;

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.RedeemsAnAsset.toString(), box, rule, Arrays.asList(assetCondition), Lists.newArrayList(ActivityActions.SendTextMessage), "");

        saveTemplateByType(activity, templateType, vertical, TemplateCategories.Winter.toString());
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();
    }

    protected void createTemplateUpdatedMembershipDetails(TemplateType templateType, TemplateVerticals vertical,int suffixIndex) throws Exception {
        timestamp = String.valueOf(System.currentTimeMillis());
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityTrigger.UpdatedMembershipDetails.toString() + "_" + timestamp,  ActivityTrigger.UpdatedMembershipDetails.toString() );

        templateName = ActivityTrigger.UpdatedMembershipDetails.toString() + "_" + suffixIndex;

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.UpdatedMembershipDetails.toString(), box, rule, Arrays.asList(), Lists.newArrayList( ActionsBuilder.buildPointsAccumulationAction("500")));

        saveTemplateByType(activity, templateType, vertical, TemplateCategories.Summer.toString());
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();
    }

    protected void createTemplateTagged(TemplateType templateType, TemplateVerticals vertical,int suffixIndex) throws Exception {
        timestamp = String.valueOf(System.currentTimeMillis());
        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("5").setUpToTimesByPeriod(true).setTimePerPeriodVal("2").setTimesByPeriod(RollingPeriod.Hour.toString());
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.Tagged.toString() + "_" + timestamp, ActivityTrigger.Tagged.toString());

        templateName = ActivityTrigger.Tagged.toString() + "_" + suffixIndex;

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.Tagged.toString(), box, rule, Arrays.asList(limitsGlobalCondition), Lists.newArrayList( ActionsBuilder.buildPointsAccumulationAction("300")));
        saveTemplateByType(activity, templateType, vertical, TemplateCategories.Summer.toString());
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();
    }

    protected void saveTemplateByType(Activity activity, TemplateType templateType, TemplateVerticals vertical, String templateCategory) {

        if (TemplateType.Business.name.equals(templateType.name())) {
            templateName =   "BT_" + templateName;
                    activity.clickSaveAsTemplateAndSave(templateName, "This is Make a purchase template with BranchID condition and tag action.");
            log4j.info("New business template created with name: " + templateName);
        } else {//save as product template

            templateName = TemplatesLibrary.PRODUCT_TEMPLATE_PREFIX + templateName + vertical.name();
            saveAsProductTemplate(activity, templateCategory, vertical);
            log4j.info("New product template created with name: " + templateName);
        }

    }

    protected void saveAsProductTemplate(Activity activity, String templateCategories, TemplateVerticals vertical ) {
        SaveProductTemplate productTemplate = new SaveProductTemplate();
        productTemplate.setName(templateName);
        productTemplate.setCategory(templateCategories);
        productTemplate.setDescription("description_" + vertical.name());
        productTemplate.setVerticals(vertical.getValue());
        activity.clickSaveAsProductTemplate(productTemplate);

    }

    protected void createTemplateEntireTicketDiscount10percentAndSendItemCode(TemplateType templateType, TemplateVerticals vertical,int suffixIndex) throws Exception {

        Activity activity = buildDealEntireTicketDiscount10percentAndSendItemCode("EntireTicketDiscount10percent_");

        templateName = "EntireTicketDiscount10percent_" + suffixIndex;
        saveTemplateByType(activity, templateType, vertical, TemplateCategories.Spring.toString());
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();
        System.out.println("Create deal " + activity.getActivityName() + " successfully!");
    }

    protected Activity buildDealEntireTicketDiscount10percentAndSendItemCode(String dealPrefix) throws Exception {

        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = BaseHub2DealTest.buildPercentOffDiscountOnEntireTicket("10");
        SendCodeToPOSAction sendCodeToPOSAction = BaseHub2DealTest.buildSendCodeToPOS(SendCodeToPOSAction.CodeType.ITEM_CODE, "1234");

        Deal deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ActivityStatus.Draft,dealPrefix );
        Activity activity = dealBuilder.buildEntireTicketDiscountActivity(box, deal, Lists.newArrayList(), Lists.newArrayList(percentOffDiscountEntireTicketAction, sendCodeToPOSAction));
        return activity;
    }

    protected void createTemplateSpecificItemsDiscountsTripleDiscountActions(TemplateType templateType, TemplateVerticals vertical,int suffixIndex) throws Exception {
        PercentOffDiscountAction percentOffDiscountAction = BaseHub2DealTest.buildPercentOffDiscount("10", BaseHub2DealTest.selectItemsByItemCode("111"));
        SetSpecialPriceAction setSpecialPriceAction = BaseHub2DealTest.buildSetSpecialPriceAction("10", BaseHub2DealTest.selectItemsByItemCode("666"));
        MakeItemsFreeAction makeItemsFreeAction = BaseHub2DealTest.buildMakeItemFreeAction(BaseHub2DealTest.selectItemsByItemCode("555"));

        templateName = "SpecificItemsDiscountsTriple_" + suffixIndex;
        Deal deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal,ActivityStatus.Draft,"specificItemDeal_");
        Activity activity = dealBuilder.buildSpecificItemsDiscountDeal(box, deal, Lists.newArrayList(), Lists.newArrayList(percentOffDiscountAction, setSpecialPriceAction,makeItemsFreeAction));

        saveTemplateByType(activity, templateType, vertical, TemplateCategories.None.toString());
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();
        System.out.println("Create deal " + deal.getActivityName() + " successfully!");
    }

    protected void createTemplateSpecificItemsAmountOffAndPercentOffDiscounts(TemplateType templateType, TemplateVerticals vertical,int suffixIndex) throws Exception {

        Activity activity = buildDealSpecificItemsAmountOffAndPercentOffDiscounts("SpecificItemsAmount_");

        templateName = "SpecificItemsAmount_" + suffixIndex;
        saveTemplateByType(activity, templateType, vertical, TemplateCategories.Spring.toString());
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();
        System.out.println("Create deal " + activity.getActivityName() + " successfully!");
    }

    protected Activity buildDealSpecificItemsAmountOffAndPercentOffDiscounts(String dealPrefix) throws Exception {

        AmountOffDiscountAction amountOffDiscountAction = BaseHub2DealTest.buildAmountOffDiscount("5", BaseHub2DealTest.selectItemsByDepartmentCode("Drink"));
        PercentOffDiscountAction percentOffDiscountAction = BaseHub2DealTest.buildPercentOffDiscount("10", BaseHub2DealTest.selectItemsByItemCode("111"));

        Deal deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal,ActivityStatus.Draft, dealPrefix);
        Activity activity = dealBuilder.buildSpecificItemsDiscountDeal(box, deal, Lists.newArrayList(), Lists.newArrayList(amountOffDiscountAction, percentOffDiscountAction));
        return activity;
    }

    protected void createTemplateSpecificItemsFreeItemAndSpecialPriceWithCondition(TemplateType templateType, TemplateVerticals vertical,int suffixIndex) throws Exception {

        Activity activity = buildDealSpecificItemsFreeItemAndSpecialPriceWithCondition("SpecificItemsFreeItem_");

        templateName = "SpecificItemsFreeItem_" + suffixIndex;
        saveTemplateByType(activity, templateType, vertical, TemplateCategories.Spring.toString());
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();
        System.out.println("Create deal " + activity.getActivityName() + " successfully!");
    }

    protected Activity buildDealSpecificItemsFreeItemAndSpecialPriceWithCondition(String dealPrefix) throws Exception {
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setConditionValue("4");

        MakeItemsFreeAction makeItemsFreeAction = BaseHub2DealTest.buildMakeItemFreeAction(BaseHub2DealTest.selectItemsByItemCode("555"));
        SetSpecialPriceAction setSpecialPriceAction = BaseHub2DealTest.buildSetSpecialPriceAction("10", BaseHub2DealTest.selectItemsByItemCode("666"));

        Deal deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ActivityStatus.Draft,dealPrefix);
        Activity activity = dealBuilder.buildSpecificItemsDiscountDeal(box, deal,  Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(makeItemsFreeAction, setSpecialPriceAction));
        return activity;
    }


    @AfterClass
    public static void tearDown(){

        log4j.info("close browser after finish test");
        cleanBase();
        BaseHubTest.cleanHub();
    }

}
