package integration;

import hub.base.BaseHubTest;
import integration.base.BaseIntegrationTest;
import integration.consent.ConsentByRegistrationTest;
import integration.consent.ConsentGracePeriodTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import server.base.BaseServerTest;

import java.io.IOException;

/**
 * Created by Jenny on 7/13/2017.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        ServerGiftCardTest.class,
        RedeemWith3RdCodeTypeTest.class,
        PointShopAssetSourceTest.class
})
public class IntegrationSuite {
    public static final Logger log4j = Logger.getLogger(IntegrationSuite.class);


    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseIntegrationTest.init();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Remove application from user");
        try {
            //AcpService.uncheckApplication(NewApplicationService.getEnvProperties().getLocationId());
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
        log4j.info("close browser after finish test");
        BaseServerTest.cleanBase();
        BaseHubTest.cleanHub();
    }

}
