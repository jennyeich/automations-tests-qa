package integration.base;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.common.objects.member.NewMember;
import hub.discounts.DiscountsConstants;
import hub.discounts.v4.GetBenefitsHelper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import server.common.IServerResponse;
import server.common.asset.ActionResponse;
import server.utils.V4Builder;
import server.v2_8.*;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Asset;
import server.v2_8.common.Customer;
import server.v2_8.common.Item;
import server.v2_8.common.RedeemItem;
import server.v4.GetBenefitsResponse;
import server.v4.common.models.Purchase;
import server.v4.common.models.PurchaseItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class BaseDiscount extends BaseIntegrationTest {

    public static final int EXPECT_NO_DISCOUNT = 0;
    public V4Builder v4Builder = new V4Builder(apiKey);
    public static CharSequence timestamp;

    public JSONObject getError(IServerResponse response) {
        return ((JSONObject) ((JSONArray) (response.getRawObject().get(ERRORS))).get(0));
    }

    public List<ActionResponse> getRedeemAssetWithItemsArrayGetDealCodes(String phoneNumber, String assetKey, ArrayList<Item> items, String totalSum) throws IOException, InterruptedException {

        Redeem redeem = createCustomerAndRedeemItems(phoneNumber, assetKey, items, totalSum);

        // Send the request
        IServerResponse response = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemResponse redeemResponse = (RedeemResponse) response;
        return redeemResponse.getDealCodes();
    }

    public List<ActionResponse> getRedeemAssetWithItemsArrayGetItemCodes(String phoneNumber, String assetKey, ArrayList<Item> items, String totalSum) throws IOException, InterruptedException {

        Redeem redeem = createCustomerAndRedeemItems(phoneNumber, assetKey, items, totalSum);

        // Send the request
        IServerResponse response = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemResponse redeemResponse = (RedeemResponse) response;
        return redeemResponse.getItemCodes();
    }


    private Redeem createCustomerAndRedeemItems(String phoneNumber, String assetKey, ArrayList<Item> items, String totalSum) {

        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        Redeem redeem = new Redeem();
        redeem.getCustomers().add(customer);
        redeem.setReturnExtendedItems(true);
        redeem.setBranchId("2");
        redeem.setPosId("12");
        redeem.setTotalSum(totalSum);
        redeem.setTransactionId(UUID.randomUUID().toString());
        redeem.setChainId("chainIDAutomation");
        redeem.setCashier("cashier1");

        RedeemItem redeemItemPurchase = new RedeemItem();
        redeemItemPurchase.setAssetKey(assetKey);
        ArrayList<RedeemItem> redeemItemForPOS = new ArrayList<>();
        redeemItemForPOS.add(redeemItemPurchase);
        redeem.setRedeemItems(redeemItemForPOS);
        redeem.setItems(items);

        return redeem;
    }

    private Redeem createRedeem(Customer customer,String transId, String totalSum) {

        Redeem redeem = new Redeem();
        redeem.getCustomers().add(customer);
        redeem.setReturnExtendedItems(true);
        redeem.setBranchId("2");
        redeem.setPosId("12");
        redeem.setTotalSum(totalSum);
        redeem.setTransactionId(transId);
        redeem.setChainId("chainIDAutomation");
        redeem.setCashier("cashier1");
        log4j.info("TransactionID: " + redeem.getTransactionId());

        return redeem;

    }

    private Redeem createRedeemWithMarkAsUsed(Customer customer,String transId, String totalSum) {

        Redeem redeem = new Redeem();
        redeem.getCustomers().add(customer);
        redeem.setReturnExtendedItems(true);
        redeem.setMarkAsUsed(true);
        redeem.setDetailedRedeemResponse(true);
        redeem.setBranchId("2");
        redeem.setPosId("12");
        redeem.setTotalSum(totalSum);
        redeem.setTransactionId(transId);
        redeem.setChainId("chainIDAutomation");
        redeem.setCashier("cashier1");
        log4j.info("TransactionID: " + redeem.getTransactionId());

        return redeem;

    }


    private Redeem createRedeem(Customer customer, String totalSum) {

        return createRedeem(customer,UUID.randomUUID().toString(),totalSum);


    }


    public String redeemAssetWithItemsArray(String phoneNumber, String assetKey, ArrayList<Item> items, String totalSum) throws IOException, InterruptedException {

        Thread.sleep(DiscountsConstants.WAIT_FOR_ASSETS_CACHE_UPDATE_MILLSEC);
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        Redeem redeem = createRedeem(customer, totalSum);


        RedeemItem redeemItemPurchase = new RedeemItem();
        redeemItemPurchase.setAssetKey(assetKey);
        ArrayList<RedeemItem> redeemItemForPOS = new ArrayList<>();
        redeemItemForPOS.add(redeemItemPurchase);
        redeem.setRedeemItems(redeemItemForPOS);

        redeem.setItems(items);

        // Send the request
        IServerResponse response = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            return ((ZappServerErrorResponse) response).getErrorMessage();
        }

        //Get the redeemAsset response
        RedeemResponse redeemResponse = (RedeemResponse) response;

        if (redeemResponse.getDiscounts() != null) {
            log4j.info("discount description :  " + redeemResponse.getDiscounts().get(0).getDescription());
            log4j.info("discount sum :  " + redeemResponse.getDiscounts().get(0).getSum());
            log4j.info("total discount sum :  " + redeemResponse.getTotalDiscountsSum());

        } else {
            Assert.fail("The discounts are null");
        }

        return redeemResponse.getTotalDiscountsSum();
    }

    public String redeemAsset(String phoneNumber,String transId, String assetKey, Item item, Item item2, String totalSum) throws Exception {

        // Send the request
        IServerResponse response = getRedeemResponse(phoneNumber, transId,assetKey, item, item2, totalSum);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            return ((ZappServerErrorResponse) response).getErrorMessage();
        }

        //Get the redeemAsset response
        RedeemResponse redeemResponse = (RedeemResponse) response;

        if (redeemResponse.getDiscounts() != null) {
            redeemResponse.getDiscounts().get(0).getDescription();
            log4j.info("discount description :  " + redeemResponse.getDiscounts().get(0).getDescription());
            log4j.info("discount sum :  " + redeemResponse.getDiscounts().get(0).getSum());
            log4j.info("total discount sum :  " + redeemResponse.getTotalDiscountsSum());

        } else {
            Assert.fail("The discounts are null");
        }

        return redeemResponse.getTotalDiscountsSum();
    }

    public String redeemAssetWithMarkAsUsed(String phoneNumber,String transId, String assetKey, Item item, Item item2, String totalSum) throws Exception {

        // Send the request
        IServerResponse response = getRedeemResponseWithMarkAsUsed(phoneNumber, transId,assetKey, item, item2, totalSum);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            return ((ZappServerErrorResponse) response).getErrorMessage();
        }

        //Get the redeemAsset response
        RedeemResponse redeemResponse = (RedeemResponse) response;

        if (redeemResponse.getDiscounts() != null) {
            redeemResponse.getDiscounts().get(0).getDescription();
            log4j.info("discount description :  " + redeemResponse.getDiscounts().get(0).getDescription());
            log4j.info("discount sum :  " + redeemResponse.getDiscounts().get(0).getSum());
            log4j.info("total discount sum :  " + redeemResponse.getTotalDiscountsSum());

        } else {
            Assert.fail("The discounts are null");
        }

        return redeemResponse.getTotalDiscountsSum();
    }

    public String redeemAssetWithMarkAsUsed(String phoneNumber, String assetKey, Item item, Item item2, String totalSum) throws Exception {

        return redeemAssetWithMarkAsUsed(phoneNumber, UUID.randomUUID().toString(),assetKey, item, item2, totalSum);
    }

    public String redeemAsset(String phoneNumber, String assetKey, Item item, Item item2, String totalSum) throws Exception {

        return redeemAsset(phoneNumber, UUID.randomUUID().toString(),assetKey, item, item2, totalSum);
    }

    public IServerResponse getRedeemResponse(String phoneNumber,String transId, String assetKey, Item item, Item item2, String totalSum) throws Exception {
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        Redeem redeem = createRedeem(customer, transId,totalSum);

        RedeemItem redeemItemPurchase = new RedeemItem();
        redeemItemPurchase.setAssetKey(assetKey);
        ArrayList<RedeemItem> redeemItemForPOS = new ArrayList<>();
        redeemItemForPOS.add(redeemItemPurchase);
        redeem.setRedeemItems(redeemItemForPOS);


        ArrayList<String> tags = new ArrayList<>();
        tags.add("DEP-1");
        tags.add("tag 2");
        item.setTags(tags);
        item2.setTags(tags);

        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        redeem.setItems(items);

        // Send the request
        IServerResponse response = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        return response;
    }

    public IServerResponse getRedeemResponseWithMarkAsUsed(String phoneNumber,String transId, String assetKey, Item item, Item item2, String totalSum) throws Exception {
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        Redeem redeem = createRedeemWithMarkAsUsed(customer, transId,totalSum);

        RedeemItem redeemItemPurchase = new RedeemItem();
        redeemItemPurchase.setAssetKey(assetKey);
        ArrayList<RedeemItem> redeemItemForPOS = new ArrayList<>();
        redeemItemForPOS.add(redeemItemPurchase);
        redeem.setRedeemItems(redeemItemForPOS);


        ArrayList<String> tags = new ArrayList<>();
        tags.add("DEP-1");
        tags.add("tag 2");
        item.setTags(tags);
        item2.setTags(tags);

        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        redeem.setItems(items);

        // Send the request
        IServerResponse response = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        return response;
    }


    public IServerResponse getRedeemResponse(String phoneNumber, String assetKey, Item item, Item item2, String totalSum) throws Exception {

        return getRedeemResponse(phoneNumber,UUID.randomUUID().toString(),assetKey,item,item2,totalSum);
    }


    public String getMemberDetails(String phoneNumber) throws Exception {

        Thread.sleep(DiscountsConstants.WAIT_FOR_ASSETS_CACHE_UPDATE_MILLSEC);
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.getCustomers().add(customer);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(false);

        //Send the request
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) getMemberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);

        ArrayList<server.v2_8.common.Membership> memberships = getMemberDetailsResponse.getMemberships();
        Assert.assertEquals("Membership must be one", 1, memberships.size());
        ArrayList<Asset> assets = memberships.get(0).getAssets();
        String key2 = assets.get(0).getKey();
        log4j.info("Memeber got the asset with the  :  " + key2);
        return key2;
    }

    public String getMemberBenefits(String phoneNumber, String totalSum, Item item, Item item2) throws Exception {

        Thread.sleep(DiscountsConstants.WAIT_FOR_ASSETS_CACHE_UPDATE_MILLSEC);
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        GetMemberBenefits getMemberBenefits = new GetMemberBenefits();
        getMemberBenefits.getCustomers().add(customer);
        getMemberBenefits.setReturnExtendedItems(true);
        getMemberBenefits.setTotalSum(totalSum);

        getMemberBenefits.getItems().add(item);
        getMemberBenefits.getItems().add(item2);

        //Send the request
        IServerResponse response = getMemberBenefits.SendRequestByApiKey(apiKey, GetMemberBenefitsResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            return ((ZappServerErrorResponse) response).getErrorMessage();
        }

        // Result true assertion
        GetMemberBenefitsResponse getMemberBenefitsResponse = (GetMemberBenefitsResponse) response;
        Assert.assertEquals("Result must be Y", "Y", getMemberBenefitsResponse.getResult());

        if (getMemberBenefitsResponse.getDiscounts().size() == 0) {
            return String.valueOf(EXPECT_NO_DISCOUNT);
        }

        return getMemberBenefitsResponse.getTotalSumDiscount();
    }

    public String getMemberBenefitsWithitemsList(String phoneNumber, String totalSum, ArrayList<Item> items) throws Exception {

        IServerResponse response = getMemberBenefitsWithItemsList(phoneNumber, totalSum, items);
        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            return ((ZappServerErrorResponse) response).getErrorMessage();
        }

        // Result true assertion
        GetMemberBenefitsResponse getMemberBenefitsResponse = (GetMemberBenefitsResponse) response;
        Assert.assertEquals("Result must be Y", "Y", getMemberBenefitsResponse.getResult());

        if (getMemberBenefitsResponse.getDiscounts().size() == 0) {
            return "0";
        }

        return getMemberBenefitsResponse.getTotalSumDiscount();

    }

    public static IServerResponse getMemberBenefitsWithItemsList(String phoneNumber, String totalSum, ArrayList<Item> items) throws Exception {

        Thread.sleep(DiscountsConstants.WAIT_FOR_ASSETS_CACHE_UPDATE_MILLSEC);
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        GetMemberBenefits getMemberBenefits = new GetMemberBenefits();
        getMemberBenefits.getCustomers().add(customer);
        getMemberBenefits.setReturnExtendedItems(true);
        getMemberBenefits.setTotalSum(totalSum);

        for (int i = 0; i < items.size(); i++) {
            getMemberBenefits.getItems().add(items.get(i));
        }

        //Send the request
        return getMemberBenefits.SendRequestByApiKey(apiKey, GetMemberBenefitsResponse.class);
    }

    public static GetMemberBenefitsResponse getMemberBenefits(String phoneNumber, String totalSum, ArrayList<Item> items) throws Exception {
        IServerResponse response = getMemberBenefitsWithItemsList(phoneNumber, totalSum, items);
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail("Get member benefits returns error: " + ((ZappServerErrorResponse) response).getErrorMessage());
        }
        // Result true assertion
        GetMemberBenefitsResponse getMemberBenefitsResponse = (GetMemberBenefitsResponse) response;
        Assert.assertEquals("Result must be Y", "Y", getMemberBenefitsResponse.getResult());
        return getMemberBenefitsResponse;
    }

    public Item createItem(String itemCode, String itemName, String depCode, String depName, Integer itemPrice, Integer quantity) {

        Item item = new Item();
        item.setItemCode(itemCode);
        item.setQuantity(quantity);
        item.setAmount(1.2415);
        item.setItemName(itemName);
        item.setDepartmentCode(depCode);
        item.setDepartmentName(depName);
        item.setPrice(itemPrice);
        item.setTags(Lists.newArrayList("DEP1"));
        return item;
    }

    public GetBenefitsResponse performPurchaseV4(List<PurchaseItem> items, int totalSum, int expected) throws Exception {
        //Create purchase
        Purchase purchase = v4Builder.buildPurchase(UUID.randomUUID().toString(), totalSum, items);

        //Send request & get response
        IServerResponse getBenefitsResponse = GetBenefitsHelper.getInstance().getBenefits(apiKey, hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX).getPhoneNumber(), purchase, null, null);
        checkResponseV4(getBenefitsResponse, expected);
        return (GetBenefitsResponse) getBenefitsResponse;
    }

    public GetBenefitsResponse performPurchaseV4(NewMember newMember, List<PurchaseItem> items, int totalSum, int expected) throws Exception {
        //Create purchase
        Purchase purchase = v4Builder.buildPurchase(UUID.randomUUID().toString(), totalSum, items);

        //Send request & get response
        IServerResponse getBenefitsResponse = GetBenefitsHelper.getInstance().getBenefits(apiKey, newMember.getPhoneNumber(), purchase, null, null);
        checkResponseV4(getBenefitsResponse, expected);
        return (GetBenefitsResponse) getBenefitsResponse;
    }


    public void checkResponseV4(IServerResponse getBenefitsResponse, int expected) {
        //Make sure the response is not Error response
        if (getBenefitsResponse instanceof server.v4.base.ZappServerErrorResponse) {
            Assert.fail(((server.v4.base.ZappServerErrorResponse) getBenefitsResponse).getErrorMessage());
        }

        if (!(((GetBenefitsResponse) getBenefitsResponse).getStatus().equals("ok")))
            Assert.fail(getError(getBenefitsResponse).get(MESSAGE).toString());
        //Make sure the response has status code 200
        Assert.assertEquals("Status code must be 200: ", httpStatusCode200, getBenefitsResponse.getStatusCode());

        if (expected == EXPECT_NO_DISCOUNT) {//no discount - returns only status ok
            Assert.assertNull("Total discount should be null - conditions not fulfilled ", ((GetBenefitsResponse) getBenefitsResponse).getTotalDiscountSum());
            log4j.info("Total discount should be empty - no discount");
        } else {
            //Check discount's sum
            Assert.assertEquals("Verify discount's sum is as expected", Integer.valueOf(expected), ((GetBenefitsResponse) getBenefitsResponse).getTotalDiscountSum());
            log4j.info("Total discount sum is as expected : " + ((GetBenefitsResponse) getBenefitsResponse).getTotalDiscountSum());
        }
    }

}
