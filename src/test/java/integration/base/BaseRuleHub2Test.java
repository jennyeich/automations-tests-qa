package integration.base;

import hub.common.objects.member.FindMember;
import hub.common.objects.member.NewMember;
import hub.hub2_0.common.builders.RuleBuilder;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.BeforeClass;

import java.util.Map;


public abstract class BaseRuleHub2Test extends BaseAutomationTest {

    public static RuleBuilder ruleBuilder = new RuleBuilder();


    @BeforeClass
    public static void setup() throws Exception {
        init();
    }


    protected NewMember createNewMember() throws Exception {
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        return newMember;
    }

    public String joinClubthroughFriendReferralCode () throws Exception {

        Map map = serverHelper.createAndValidateCode();
        String code = map.get(CODE).toString();

        String phoneNumber = String.valueOf(System.currentTimeMillis());
        String userToken = serverHelper.signON ();

        String membershipKey = serverHelper.joinClubWithUserTokenAndReferralCode(phoneNumber,userToken,"App",code);
        log4j.info("Created member through referral code with membershipKey: " + membershipKey);

        return membershipKey;
    }

    public String joinClubthroughFriendReferralCodeReturnMembershipKeyForAdvocate () throws Exception {

        Map map = serverHelper.createAndValidateCode();
        String code = map.get(CODE).toString();
        String membershipKey = map.get("membershipKey").toString();

        String phoneNumber = String.valueOf(System.currentTimeMillis());
        String userToken = serverHelper.signON ();
        serverHelper.joinClubWithUserTokenAndReferralCode(phoneNumber,userToken,"App",code);
        log4j.info("Created member through referral code with membershipKey of 'used my code': " + membershipKey);

        FindMember findMember = new FindMember();
        findMember.setMembershipKey(membershipKey);
        MembersService.findMember(findMember);
        MembersService.clickMember();
        return membershipKey;
    }


}
