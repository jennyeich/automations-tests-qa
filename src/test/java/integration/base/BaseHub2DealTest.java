package integration.base;

import com.beust.jcommander.internal.Lists;
import hub.common.objects.member.NewMember;
import hub.discounts.DiscountsConstants;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Deal;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.basePages.loyalty.templates.SaveProductTemplate;
import hub.hub2_0.basePages.loyalty.templates.TemplateCategories;
import hub.hub2_0.basePages.loyalty.templates.TemplateVerticals;
import hub.hub2_0.basePages.loyalty.templates.TemplatesLibrary;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.dealActions.*;
import hub.hub2_0.common.builders.DealBuilder;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.enums.ItemOperation;
import hub.hub2_0.common.enums.ActivityStatus;
import hub.hub2_0.common.enums.ActivityType;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.common.items.SelectPopulation;
import hub.hub2_0.services.loyalty.ActivityService;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.utils.HubMemberCreator;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import server.common.IServerResponse;
import server.v2_8.GetMemberBenefits;
import server.v2_8.GetMemberBenefitsResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Customer;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.List;

public class BaseHub2DealTest extends BaseDiscount {

    public static final String NO_DISCOUNT_AMOUNT_LIMIT = null;
    public static final String NO_DISCOUNT_TIMES_LIMIT = null;
    public static final SelectItems NO_ITEM_EXCLUDED = null;

    public static final Logger log4j = Logger.getLogger(BaseHub2DealTest.class);

    public static DealBuilder dealBuilder = new DealBuilder();
    public static String dealId;
    public static Deal deal;


    @BeforeClass
    public static void setup() throws Exception {
        init();
    }

    @After
    public void deactivateDeals() throws Exception {
        try {
            if(deal != null)
                ActivityService.executeOperation(deal.getActivityName(), ItemOperation.STOP);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // Entire Ticket Discount Builders
    public static PercentOffDiscountEntireTicketAction buildPercentOffDiscountOnEntireTicket(String percentage) {
        return buildPercentOffDiscountOnEntireTicket(percentage, null, null);
    }


    public static PercentOffDiscountEntireTicketAction buildPercentOffDiscountOnEntireTicket(String percentage, SelectItems excludeItems, String limitAmount) {
        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = new PercentOffDiscountEntireTicketAction();
        percentOffDiscountEntireTicketAction.clickPercentOffDiscount();
        percentOffDiscountEntireTicketAction.setDiscountPercentage(percentage);
        if (excludeItems != null) {
            percentOffDiscountEntireTicketAction.clickExcludeItemsExpand();
            percentOffDiscountEntireTicketAction.clickExcludeItemsCheckbox();
            percentOffDiscountEntireTicketAction.setSelectItemsToExclude(excludeItems);
        }
        if (limitAmount != null) {
            percentOffDiscountEntireTicketAction.clickLimitDiscountExpand();
            percentOffDiscountEntireTicketAction.clickLimitDiscountCheckbox();
            percentOffDiscountEntireTicketAction.setLimitDiscountValue(limitAmount);
        }
        return percentOffDiscountEntireTicketAction;
    }

    public static AmountOffDiscountEntireTicketAction buildAmountOffDiscountOnEntireTicket(String amount) {
        return buildAmountOffDiscountOnEntireTicket(amount, null);
    }


    public static AmountOffDiscountEntireTicketAction buildAmountOffDiscountOnEntireTicket(String amount, SelectItems excludeItems) {
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = new AmountOffDiscountEntireTicketAction();
        amountOffDiscountEntireTicketAction.clickAmountOffDiscount();
        amountOffDiscountEntireTicketAction.setDiscountAmount(amount);
        if (excludeItems != null) {
            amountOffDiscountEntireTicketAction.clickExcludeItemsExpand();
            amountOffDiscountEntireTicketAction.clickExcludeItemsCheckbox();
            amountOffDiscountEntireTicketAction.setSelectItemsToExclude(excludeItems);
        }
        return amountOffDiscountEntireTicketAction;
    }

    public static SendCodeToPOSAction buildSendCodeToPOS(SendCodeToPOSAction.CodeType codeType, String codeInput) {
        SendCodeToPOSAction sendCodeToPOSAction = new SendCodeToPOSAction();
        sendCodeToPOSAction.clickDealCodeToPOS();
        sendCodeToPOSAction.setCodeType(codeType);
        sendCodeToPOSAction.setCodeInput(codeInput);
        return sendCodeToPOSAction;
    }

    // ****** Specific items and advanced discount action builders ******//

    public static AmountOffDiscountAction buildAmountOffDiscount(String discountAmount, SelectPopulation selectItems) {
        return buildAmountOffDiscount(discountAmount, selectItems, NO_DISCOUNT_AMOUNT_LIMIT, NO_DISCOUNT_TIMES_LIMIT);
    }


    public static AmountOffDiscountAction buildAmountOffDiscount(String discountAmount, SelectPopulation selectItems, String limitAmount, String limitTimes) {
        AmountOffDiscountAction amountOffDiscountAction = new AmountOffDiscountAction();
        amountOffDiscountAction.clickAmountOffDiscount();
        amountOffDiscountAction.setDiscountAmount(discountAmount);
        amountOffDiscountAction.setSelectItems(selectItems);

        if (limitAmount != null || limitTimes != null) {
            amountOffDiscountAction.clickLimitDiscountExpand();
            if (limitAmount != null) {
                amountOffDiscountAction.clickLimitDiscountAmountCheckbox();
                amountOffDiscountAction.setLimitDiscountAmountValue(limitAmount);
            }
            if (limitTimes != null) {
                amountOffDiscountAction.clickLimitDiscountTimesCheckbox();
                amountOffDiscountAction.setLimitDiscountTimesValue(limitTimes);
            }
        }
        return amountOffDiscountAction;
    }

    public static PercentOffDiscountAction buildPercentOffDiscount(String percentage, SelectPopulation selectItems) {
        return buildPercentOffDiscount(percentage, selectItems, NO_DISCOUNT_AMOUNT_LIMIT, NO_DISCOUNT_TIMES_LIMIT);
    }

    public static PercentOffDiscountAction buildPercentOffDiscount(String percentage, SelectPopulation selectItems, String limitAmount, String limitTimes) {
        PercentOffDiscountAction percentOffDiscountAction = new PercentOffDiscountAction();
        percentOffDiscountAction.clickPercentOffDiscount();
        percentOffDiscountAction.setDiscountPercentage(percentage);
        percentOffDiscountAction.setSelectItems(selectItems);

        if (limitAmount != null || limitTimes != null) {
            percentOffDiscountAction.clickLimitDiscountExpand();
            if (limitAmount != null) {
                percentOffDiscountAction.clickLimitDiscountAmountCheckbox();
                percentOffDiscountAction.setLimitDiscountAmountValue(limitAmount);
            }
            if (limitTimes != null) {
                percentOffDiscountAction.clickLimitDiscountTimesCheckbox();
                percentOffDiscountAction.setLimitDiscountTimesValue(limitTimes);
            }
        }
        return percentOffDiscountAction;
    }

    public static MakeItemsFreeAction buildMakeItemFreeAction(SelectPopulation selectItems) {
        return buildMakeItemFreeAction(selectItems, NO_DISCOUNT_AMOUNT_LIMIT, NO_DISCOUNT_TIMES_LIMIT);
    }

    public static MakeItemsFreeAction buildMakeItemFreeAction(SelectPopulation selectItems, String limitAmount, String limitTimes) {
        MakeItemsFreeAction makeItemsFreeAction = new MakeItemsFreeAction();
        makeItemsFreeAction.clickMakeItemsFree();
        if (selectItems != null) {
            makeItemsFreeAction.setSelectItems(selectItems);
        }

        if (limitAmount != null || limitTimes != null) {
            makeItemsFreeAction.clickLimitDiscountExpand();
            if (limitAmount != null) {
                makeItemsFreeAction.clickLimitDiscountAmountCheckbox();
                makeItemsFreeAction.setLimitDiscountAmountValue(limitAmount);
            }
            if (limitTimes != null) {
                makeItemsFreeAction.clickLimitDiscountTimesCheckbox();
                makeItemsFreeAction.setLimitDiscountTimesValue(limitTimes);
            }
        }
        return makeItemsFreeAction;
    }

    public static SetSpecialPriceAction buildSetSpecialPriceAction(String specialPrice, SelectPopulation selectItems) {
        return buildSetSpecialPriceAction(specialPrice, selectItems, NO_DISCOUNT_AMOUNT_LIMIT, NO_DISCOUNT_TIMES_LIMIT);
    }

    public static SetSpecialPriceAction buildSetSpecialPriceAction(String specialPrice, SelectPopulation selectItems, String limitAmount, String limitTimes) {
        SetSpecialPriceAction setSpecialPriceAction = new SetSpecialPriceAction();
        setSpecialPriceAction.clickSpecialPrice();
        setSpecialPriceAction.setSpecialPriceValue(specialPrice);
        setSpecialPriceAction.setSelectItems(selectItems);

        if (limitAmount != null || limitTimes != null) {
            setSpecialPriceAction.clickLimitDiscountExpand();
            if (limitAmount != null) {
                setSpecialPriceAction.clickLimitDiscountAmountCheckbox();
                setSpecialPriceAction.setLimitDiscountAmountValue(limitAmount);
            }
            if (limitTimes != null) {
                setSpecialPriceAction.clickLimitDiscountTimesCheckbox();
                setSpecialPriceAction.setLimitDiscountTimesValue(limitTimes);
            }
        }
        return setSpecialPriceAction;
    }

    public static void createEntireTicketDiscountDeal(Box box, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ActivityStatus.Active, "entireTicketDeal_");
        Activity activity = dealBuilder.buildEntireTicketDiscountActivity(box, deal, conditions, activityActionList);

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        dealId = LoyaltyService.getDealID(activity.getActivityName());
        activity.clickCancel();
    }


    public static void createSpecificItemsDiscountDeal(Box box, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ActivityStatus.Active, "specificItemDeal_");
        Activity activity = dealBuilder.buildSpecificItemsDiscountDeal(box, deal, conditions, activityActionList);

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        dealId = LoyaltyService.getDealID(activity.getActivityName());
        activity.clickCancel();
    }

    public static void createMultipleCasesDeal(Box box, List<ApplyCondition> conditions, List<Case> cases) throws Exception {
        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ActivityStatus.Active, "MultipleCasesDeal_");
        Activity activity = dealBuilder.buildMultipleCasesDeal(box, deal, conditions, cases);

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        dealId = LoyaltyService.getDealID(activity.getActivityName());
        activity.clickCancel();
    }

    public static void createAdvancedDiscountDeal(Box box, List<ApplyCondition> conditions, List<OccurrencesCondition> occurrencesConditionList, List<ActivityAction> activityActionList) throws Exception {
        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ActivityStatus.Active, "advancedDeal_");
        Activity activity = dealBuilder.buildAdvancedDiscountDeal(box, deal, conditions, occurrencesConditionList, activityActionList);

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        dealId = LoyaltyService.getDealID(activity.getActivityName());
        activity.clickCancel();
    }

    /////**** Deal template creators****////

    public static void createEntireTicketDiscountDealTemplate(Box box, List<ApplyCondition> conditions, List<ActivityAction> activityActionList, String templateName) throws Exception {
        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, "entireTicketDeal_", "deal description");
        Activity activity = dealBuilder.buildEntireTicketDiscountActivity(box, deal, conditions, activityActionList);
        activity.clickSaveAsTemplateAndSave(templateName, "This is deal template.");
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();
    }

    public static void createSpecificItemsDiscountDealTemplate(Box box, List<ApplyCondition> conditions, List<ActivityAction> activityActionList, String templateName) throws Exception {
        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, "specificItemsDeal_", "deal description");
        Activity activity = dealBuilder.buildSpecificItemsDiscountDeal(box, deal, conditions, activityActionList);
        activity.clickSaveAsTemplateAndSave(templateName, "This is deal template.");
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();
    }

    public static void createAdvancedDiscountDealTemplate(Box box, List<ApplyCondition> conditions, List<OccurrencesCondition> occurrencesConditionList, List<ActivityAction> activityActionList, String templateName) throws Exception {
        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, "advancedDeal_", "deal description");
        Activity activity = dealBuilder.buildAdvancedDiscountDeal(box, deal, conditions, occurrencesConditionList, activityActionList);
        activity.clickSaveAsTemplateAndSave(templateName, "This is deal template.");
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();
    }

    public void createEntireTicketDealFromTemplate(Box box, String activityName, String templateName, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ActivityStatus.Active, activityName);
        Activity activity = dealBuilder.buildEntireTicketDiscountDealEditMode(box, deal, conditions, activityActionList);
        LoyaltyService.createDealFromTemplateInBox(activity, templateName);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        dealId = LoyaltyService.getDealID(activity.getActivityName());
        activity.clickCancel();
    }

    public void createSpecificItemsDealFromTemplate(Box box, String activityName, String templateName, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ActivityStatus.Active, activityName);
        Activity activity = dealBuilder.buildSpecificItemsDiscountDealEditMode(box, deal, conditions, activityActionList);
        LoyaltyService.createDealFromTemplateInBox(activity, templateName);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        dealId = LoyaltyService.getDealID(activity.getActivityName());
        activity.clickCancel();
    }

    public void createAdvancedDealFromTemplate(Box box, String activityName, String templateName, List<ApplyCondition> conditions, List<OccurrencesCondition> occurrencesConditionList, List<ActivityAction> activityActionList) throws Exception {
        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ActivityStatus.Active, activityName);
        Activity activity = dealBuilder.buildAdvancedDiscountDealEDitMode(box, deal, conditions, occurrencesConditionList, activityActionList);
        LoyaltyService.createDealFromTemplateInBox(activity, templateName);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        dealId = LoyaltyService.getDealID(activity.getActivityName());
        activity.clickCancel();
    }


    public static GetMemberBenefitsResponse getMemberBenefitsAndVerifyDiscount(ArrayList<Item> items, String totalSum, String expectedDiscount) throws Exception {
        NewMember newMember = HubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        return getMemberBenefitsAndVerifyDiscount(newMember, items, totalSum, expectedDiscount);
    }

    public static GetMemberBenefitsResponse getMemberBenefitsAndVerifyDiscount(NewMember newMember, ArrayList<Item> items, String totalSum, String expectedDiscount) throws Exception {
        GetMemberBenefitsResponse getMemberBenefitsResponse = getMemberBenefits(newMember.getPhoneNumber(), totalSum, items);
        Assert.assertEquals("Verify discount's sum is as expected", expectedDiscount, getMemberBenefitsResponse.getTotalSumDiscount());
        return getMemberBenefitsResponse;
    }


    public static List<Activity> buildProductTemplates(Box box) throws Exception {
        //verify before using the method that permission to create product templates
        int verticals = TemplateVerticals.values().length;
        List<Activity> productTemplates = new ArrayList<>();
        for (int i = 0; i < verticals; i++) {
            TemplateVerticals vertical = TemplateVerticals.values()[i];
            String timestamp = String.valueOf(System.currentTimeMillis());
            String tag = "tag_" + timestamp.toString();

            //build deal
            SendCodeToPOSAction sendCodeToPOSAction = buildSendCodeToPOS(SendCodeToPOSAction.CodeType.ITEM_CODE, tag);
            deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, "entireTicketDeal_", "product template with code " + tag);
            Activity activity = dealBuilder.buildEntireTicketDiscountActivity(box, deal, Lists.newArrayList(), Lists.newArrayList(sendCodeToPOSAction));

            SaveProductTemplate productTemplate = new SaveProductTemplate();
            productTemplate.setName(TemplatesLibrary.PRODUCT_TEMPLATE_PREFIX + vertical.name());
            if ((i % 2) == 0)
                productTemplate.setCategory(TemplateCategories.Summer.toString());
            else
                productTemplate.setCategory(TemplateCategories.Winter.toString());

            productTemplate.setDescription("description_" + vertical.name());
            productTemplate.setVerticals(vertical.getValue());
            activity.clickSaveAsProductTemplate(productTemplate);
            productTemplates.add(activity);
        }
        return productTemplates;
    }

    public static void getMemberBenefitsWithPodIdAndBranch(String phoneNumber, String totalSum, ArrayList<Item> items, String posId, String branchId, String expectedDiscount) throws Exception {

        Thread.sleep(DiscountsConstants.WAIT_FOR_ASSETS_CACHE_UPDATE_MILLSEC);
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        GetMemberBenefits getMemberBenefits = new GetMemberBenefits();
        getMemberBenefits.getCustomers().add(customer);
        getMemberBenefits.setReturnExtendedItems(true);
        getMemberBenefits.setTotalSum(totalSum);
        getMemberBenefits.setPosId(posId);
        getMemberBenefits.setBranchId(branchId);

        for (int i = 0; i < items.size(); i++) {
            getMemberBenefits.getItems().add(items.get(i));
        }

        //Send the request
        IServerResponse response = getMemberBenefits.SendRequestByApiKey(apiKey, GetMemberBenefitsResponse.class);

        if (response instanceof ZappServerErrorResponse) {
            Assert.fail("Get member benefits returns error: " + ((ZappServerErrorResponse) response).getErrorMessage());
        }
        // Result true assertion
        GetMemberBenefitsResponse getMemberBenefitsResponse = (GetMemberBenefitsResponse) response;
        Assert.assertEquals("Result must be Y", "Y", getMemberBenefitsResponse.getResult());

        Assert.assertEquals("Verify discount's sum is as expected", expectedDiscount, getMemberBenefitsResponse.getTotalSumDiscount());
    }
    public static SelectItems selectItemsByItemCode(String... itemCodes) {
        return dealBuilder.selectItemsByItemCode(itemCodes);
    }

    public static SelectItems selectItemsByDepartmentCode(String... departmentCodes) {
        return dealBuilder.selectItemsByDepartmentCode(departmentCodes);
    }


}
