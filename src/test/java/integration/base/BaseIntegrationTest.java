package integration.base;

import common.BaseTest;
import hub.base.BaseHubTest;
import hub.services.AcpService;
import hub.services.NewApplicationService;
import hub.services.member.MembersService;
import hub.utils.HubAssetsCreator;
import hub.utils.HubMemberCreator;
import hub.utils.ScreenShotTaker;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.base.BaseServerTest;
import server.utils.ServerHelper;
import utils.EnvProperties;
import utils.PropsReader;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

/**
 * Created by Jenny on 12/5/2016
 */
public class BaseIntegrationTest extends BaseTest{

    public static final String AUTO_USER_PREFIX = "auto_user_";
    public static final String AUTO_LAST_PREFIX = "auto_last_";

    public static final String IMPORT_FOLDER = "import";

    public static HubAssetsCreator hubAssetsCreator = new HubAssetsCreator();
    public static HubMemberCreator hubMemberCreator = new HubMemberCreator();

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
    }

    @Rule
    public TestWatcher screenShotWatcher = new TestWatcher() {

        @Override
        protected void failed(Throwable e, Description description) {
            ScreenShotTaker.takeScreenShot(description.getDisplayName(), log4j);
        }
    };


    public static void init(boolean isPermissionTagTest) throws IOException {
        logPrefix = "Integration-" + logPrefix;
        if(firstTime) {
            try {
                initBase();

                if (isNewApplication) {
                    BaseHubTest.initHubForNewApp(isPermissionTagTest);
                    firstTime = false;
                } else {
                    //run tests on exist application
                    BaseHubTest.initHub(isPermissionTagTest);
                }

            } catch (Exception e) {
                log4j.error(e);
            }

            MembersService.setLogger(log4j);
            BaseServerTest.initServer();
            initProperties();
            firstTime = false;
            if(isPermissionTagTest &&
                    (!PropsReader.getPropValuesForEnv("location_prefix").contains("qa_") &&
                    !PropsReader.getPropValuesForEnv("location_prefix").contains("hotfix_")&&
                    !PropsReader.getPropValuesForEnv("location_prefix").contains(""))){
                addPermissionTagUserToNewApp();
            }
        }
    }

    private static void addPermissionTagUserToNewApp() {
        try {
            AcpService.addPermissionTagUser();
        } catch (Exception e) {
            log4j.error("Failed to add user for permission tag tests - quit test");
            Assert.fail(e.getMessage());
        }
    }

    public static void init() throws IOException {
        init(false);

    }
    protected static void initProperties() throws IOException {
        if(isNewApplication) {
            EnvProperties envProperties = NewApplicationService.getEnvProperties();
            apiKey = envProperties.getApiKey();
            locationId = envProperties.getLocationId();
            serverToken = envProperties.getServerToken();
            applicationName = envProperties.getApplicationName();
        }else {
            //takes the properties data from the env.properties
            apiKey = PropsReader.getPropValuesForEnv("apiKey");
            locationId = PropsReader.getPropValuesForEnv("locationID");
            serverToken = PropsReader.getPropValuesForEnv("serverToken");
            applicationName = PropsReader.getPropValuesForEnv("appName");
            EnvProperties envProperties = NewApplicationService.getEnvProperties();
            envProperties.setLocationId(locationId);
            envProperties.setApiKey(apiKey);
            envProperties.setServerToken(serverToken);
        }

        log4j.info("ApiKey:" + apiKey);
        log4j.info("LocationId: " + locationId);
        serverHelper = new ServerHelper(locationId, apiKey, serverToken);
        setServerFlags();
    }

    public static void initSuite(boolean isSuiteRunning) {
        suiteRunning = isSuiteRunning;
    }

    protected static String resolveLocationId(String locationId) {
        // Support all kind of location id's (for production/hub2/qa etc) such as Dev_2,qa_3 etc.
        String currnetLocationId = locationId.substring(locationId.lastIndexOf("_")+1);
        return currnetLocationId;
    }
}
