package integration;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.mashape.unirest.http.HttpResponse;

import hub.common.objects.benefits.PointShopPage;
import hub.common.objects.common.Gender;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.PerformActionOnResults;
import hub.common.objects.member.PerformAnActionOnMember;
import hub.common.objects.member.performActionObjects.AddPointsAction;
import hub.common.objects.smarts.SmartGift;
import hub.services.NewApplicationService;
import hub.services.assets.AssetService;
import hub.services.member.MembersService;
import integration.base.BaseIntegrationTest;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.IServerResponse;
import server.utils.ServerHelper;
import server.v2_8.*;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.GiftShopItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static utils.StringUtils.StringToKey;



public class PointShopAssetSourceTest extends BaseIntegrationTest {

    private static String userToken;//the user token that we receive after sign on
    private static String assetTemplateKey; //the asset template key as returns from GetResource request
    private static String itemID; //the itemId as returns from GetResource request
    private static String userKey; //the userKey that returns from JoinClub request
    private static String phoneNumber; //the phone number that we use to find the user for all tests
    private static SmartGift smartGift;
    public Key key = null;
    public static final String MAIL_SUFFIX = "@como.com";


    @BeforeClass
    public static void setup() throws IOException {
        init();
    }

    @AfterClass
    public static void removeItemsFromPointShop() throws Exception{
        AssetService.removeItemsFromPointShop();
    }


    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            if(smartGift == null) {
                //create assets
                try {
                    phoneNumber = String.valueOf(System.currentTimeMillis());
                    smartGift = hubAssetsCreator.createSmartGift(phoneNumber);
                    String version = hubAssetsCreator.addItemToPointShopAndGetVersionAfterPublish(smartGift.getTitle(),locationId);
                    getDataForPurchaseAssetRequest(version);
                    createUserAndUpdateForTests();

                } catch (Exception e) {
                    log4j.error("error while creating asset",e);
                }
            }
            //each test should initialize the user key for the data store
            key = getKey();
        }
    };

    private void getDataForPurchaseAssetRequest(String version) {

        GiftShopItem item = serverHelper.getGiftShopItem(version);
        assetTemplateKey = item.getAssetKey();
        itemID = item.getItemID();
    }




    //3334 - check asset source for
    @Test
    public void testPointShopAssetSource() throws Exception {

        //purchase asset from gift shop
        performPurchaseAsset();
        checkDataStoreUpdatedWithAssetSource(StringToKey(userKey));

    }


    protected void performAnActionSendGift(SmartGift smartGift) throws Exception {

        Thread.sleep(4000);
        boolean isGiftExist = MembersService.performSmartActionSendAAsset(smartGift.getTitle());
        log4j.info("The gift was send to the member");
        Assert.assertTrue(isGiftExist);

    }



    private void checkDataStoreUpdatedWithAssetSource(Key userKey) throws Exception {

        //Validate userAction in datastore
        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, userKey),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"receivedasset")))));
        List<Entity> res = serverHelper.queryWithWait("Must find 1 UserAction: ", qUserAction, 1, dataStore);
        Text t = ((Text)res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());
        Assert.assertTrue("Data must contain the assetType: ", t.getValue().contains("AssetType=\"gift\""));
        Assert.assertTrue("Data must contain the price in points: ", t.getValue().contains("PriceInPoints=\"" + "200" + "\""));
        Assert.assertTrue("Data must contain the source: ", t.getValue().contains("Source=\"" + "PointsShop" + "\""));

        //check the source of the data:
        Assert.assertEquals("Source is hub","Client",res.get(0).getProperty("Source"));

    }


    //Create the user as a mobile client to simulate the purchase an assset trigger from point shop
    private void createUserAndUpdateForTests() throws Exception {
        //signOn and get the user token
        userToken = serverHelper.signOn();
        log4j.info("userToken: " + userToken);
        //join the club with the user token
        userKey = joinClubWithUserToken(phoneNumber);

        log4j.info("userKey: " + userKey);
        FindMember findMember = new FindMember();
        findMember.setUserKey(userKey);
        MembersService.findAndGetMember(findMember);
        performAnActionAddPoints("300000");
        log4j.info("Find member according to user key and give user 300000 points for using in all tests");
        performUpdateMemberDetails();
        log4j.info("Update user with relevant details(phone,name,birthday)");
        key = getKey();
    }


    //Returns the key for the data store according to the userKey in the hub
    private Key getKey(){
        DataStoreKeyUtility uKey = new DataStoreKeyUtility();
        HttpResponse response = uKey.SendRequest(userKey);
        return uKey.userKey((String)response.getBody());

    }

    //Update member details with all relevant fields for the tests
    void performUpdateMemberDetails() throws Exception {
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setPhoneNumber(phoneNumber);
        memberDetails.setFirstName(AUTO_USER_PREFIX + phoneNumber);
        memberDetails.setLastname(AUTO_LAST_PREFIX + phoneNumber);
        memberDetails.setGender(Gender.MALE.toString());
        memberDetails.setEmail(AUTO_USER_PREFIX + phoneNumber + MAIL_SUFFIX);
        Calendar birthday = Calendar.getInstance();
        birthday.add(Calendar.YEAR, -20);
        memberDetails.setBirhday(hubAssetsCreator.getDate(birthday));
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);
        memberDetails.setMembershipExpiration(hubAssetsCreator.getDate(calNextYear));
        MembersService.updateMember(memberDetails);
    }

    //Add the user points to use in all tests when purchasing the asset
    private void performAnActionAddPoints(String numOfPoints) throws Exception {

        log4j.info("perform action add points");
        MembersService.performSmartActionAddPoints(numOfPoints,String.valueOf(System.currentTimeMillis()));

    }


    private void performPurchaseAsset() {

        PurchaseAsset purchaseAsset = new PurchaseAsset();
        GiftShopItem giftShopItem = new GiftShopItem();
        giftShopItem.setAssetKey(assetTemplateKey);
        giftShopItem.setPrice("2");
        giftShopItem.setItemID(itemID);
        purchaseAsset.setItem(giftShopItem);

        IServerResponse response = purchaseAsset.sendRequestByTokenAndLocation(userToken,locationId, PurchaseAssetResponse.class);
        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }
    }

    //Simulates the user join the club from the client application using the token that was received from signOn
    private String joinClubWithUserToken(String phoneNumber) {

        //Join Club
        JoinClub joinClub = new JoinClub();
        joinClub.setPhoneNumber(phoneNumber);
        joinClub.setLocationID(locationId);
        joinClub.setOrigin(ServerHelper.APP);
        IServerResponse response = joinClub.sendRequestByTokenAndLocation(userToken, locationId, JoinClubResponse.class);
        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }
        String userKey = (String) ((JoinClubResponse)response).getRawObject().get("UserKey");
        log4j.info("Join club and get user key...");
        return userKey;
    }



    private String getRandomId() {
        return String.valueOf(System.currentTimeMillis());
    }




}
