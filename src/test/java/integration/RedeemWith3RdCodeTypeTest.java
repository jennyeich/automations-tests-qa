package integration;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.base.categories.serverCategories.ServerRegression;
import hub.common.objects.benefits.redeemCode.RedeemCodeType;
import hub.common.objects.benefits.redeemCode.ThirdPartyCodes;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartGift;
import hub.services.assets.AssetService;
import hub.services.member.MembersService;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.v2_8.RedeemAsset;
import server.v2_8.RedeemAssetResponse;
import server.v2_8.base.ZappServerErrorResponse;

import java.io.IOException;

import static utils.StringUtils.StringToKey;

/**
 * Created by Jenny on 6/14/2017.
 */
public class RedeemWith3RdCodeTypeTest extends BaseIntegrationTest {


    @BeforeClass
    public static void setup() throws IOException {
        init();
        serverHelper.addPOSRedeemCodeBulk();
    }



    /**
     * C5740 - Redeem with Code Type = 3'rd Code (Auto Mark Redeem As Code Use = NO) )
     */
    @Test
    @Category({Hub1Regression.class,ServerRegression.class})
    public void testRedeemPurchaseWith3RdCodeTypeAutoMarkRedeemAsCodeUseNo() throws Exception {

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);
        String timestamp = String.valueOf(System.currentTimeMillis());
        SmartGift smartGift = createSmartGiftWithThirdPartyCode(timestamp);
        ThirdPartyCodes thirdPartyCodes = generate3PartyCodes (smartGift.getTitle());

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String membershipKey = MembersService.getMembershipKey();

        //Send gift to member
        performAnActionSendGift(smartGift);
        String assetKey = serverHelper.getAssetKey(member.getPhoneNumber());
        Key dataStoreAssetKey = StringToKey(assetKey);

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(assetKey);
        redeemAsset.setMembershipKey(membershipKey);
        redeemAsset.setToken(serverToken);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);

        //Get the redeem response
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) response;
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Verify Asset statues with DataStore;
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Redemeed","Redeemed",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Burned","Burned",result2.getProperty("RedeemState"));

    }




    protected void performAnActionSendGift(SmartGift smartGift) throws Exception {

        Thread.sleep(4000);
        boolean isGiftExist = MembersService.performSmartActionSendAAsset(smartGift.getTitle());
        log4j.info( "The gift was send to the member");
        Assert.assertTrue("The gift doesn't exist", isGiftExist);

    }


    public SmartGift createSmartGiftWithThirdPartyCode(CharSequence timestamp) throws Exception{
        log4j.info("creating smart gift");
        SmartGift smartGift = new SmartGift();
        smartGift.setDescription("Sample description");
        smartGift.setShortName("sample short name " + timestamp);
        smartGift.setTitle("testGift" + timestamp);
        //Create with redeem code a3rd party
        smartGift.setRedeemCodeType(RedeemCodeType.THIRD_PARTY_CODES);
        AssetService.createSmartGift(smartGift);
        log4j.info("gift was created succesfully. gift's title is:  " + smartGift.getTitle());
        return smartGift;
    }

    public ThirdPartyCodes generate3PartyCodes (String giftName) throws Exception {

        ThirdPartyCodes thirdPartyCodes = new ThirdPartyCodes();
        thirdPartyCodes.setCodeName(giftName + "_thirdPatyCodes");
        thirdPartyCodes.setChooseAsset(giftName);
        thirdPartyCodes.setBulk_name(giftName + "_Bulk");
        thirdPartyCodes.setBulk_tag(giftName + "_bulkTag");
        thirdPartyCodes.setCodes("10001,10021,10003,10004,10010,11223344");
        AssetService.createThirdPartCodes(thirdPartyCodes);
        return thirdPartyCodes;
    }

}
