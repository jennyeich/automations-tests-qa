package integration;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import common.BaseTest;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import hub.utils.HubCreatorBase;
import hub.utils.HubMemberCreator;
import integration.base.BaseIntegrationTest;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.common.LocationBackendFields;
import server.v2_8.PayWithBudget;
import server.v2_8.PayWithBudgetResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Customer;

import java.io.IOException;
import java.util.*;

/**
 * Created by Jenny on 6/4/2017.
 */
public class ServerGiftCardTest extends BaseIntegrationTest {

    String  timestamp;
    public HubMemberCreator hubMemberCreator = new HubMemberCreator();
    @BeforeClass
    public static void setup() throws IOException {
        init();
        Map<String, Object> props = new HashMap<>();
        props.put(LocationBackendFields.ALLOW_PAY_WITH_BUDGET_CHARGE.key(), "true");
        props.put(LocationBackendFields.CLIENT_VERIFICATION_CODE_EXPIRATION_SECONDS.key(), 180);
        serverHelper.updateBusinessBackend(props);
        serverHelper.saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), true);

    }


    //Test 3832 Pay With Budget - Add Budget To gift Card
    @Test
    @Category(Hub1Regression.class)
    public void testAddBudgetToGiftCard () throws Exception {

        timestamp = String.valueOf(System.currentTimeMillis());

        NewMember newMember = hubMemberCreator.createMember("first", "last");
        hubAssetsCreator.createGiftCard(timestamp,"100","200");
        Thread.sleep(2000);
        String randomCodes = generateRandomCodes();
        String codeToUse = String.valueOf(BaseTest.getRandomInt());
        String codes = randomCodes + codeToUse;
        hubAssetsCreator.createGiftCardBulkCodes("bulkName" + timestamp,"tag" + timestamp,"description" + timestamp,codes);
        Thread.sleep(20000);
        PayWithBudget payWithBudget = payWithBudget(newMember.getPhoneNumber(),"-100", codeToUse);
        IServerResponse response = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        String actualCharge = "100";
        Long sum = new Long (100);

        PayWithBudgetResponse payWithBudgetResponse = (PayWithBudgetResponse) response;
        verifyDataStore(payWithBudgetResponse, payWithBudget, sum,actualCharge);
    }

    //Test 3833 Pay With Budget - Add Budget To gift Card
    @Test
    public void testAddBudgetToGiftCardPartialPayment () throws Exception {

        timestamp = String.valueOf(System.currentTimeMillis());
        NewMember newMember = hubMemberCreator.createMember("first", "last");
        hubAssetsCreator.createGiftCard(timestamp,"100","200");
        Thread.sleep(2000);
        //generate random codes for bulk
        String randomCodes = generateRandomCodes();
        String codeToUse = String.valueOf(BaseTest.getRandomInt());
        String codes = randomCodes + codeToUse;
        hubAssetsCreator.createGiftCardBulkCodes("bulkName" + timestamp,"tag" + timestamp,"description" + timestamp,codes);

        Thread.sleep(20000);
        PayWithBudget payWithBudget = payWithBudget(newMember.getPhoneNumber(),"-100", codeToUse);
        IServerResponse response = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        String actualCharge = "100";
        Long sum = new Long (100);

        PayWithBudgetResponse payWithBudgetResponse = (PayWithBudgetResponse) response;
        verifyDataStore(payWithBudgetResponse, payWithBudget, sum,actualCharge);

        Thread.sleep(10000);
        //create second call for payWithBudget
         payWithBudget = payWithBudget(newMember.getPhoneNumber(),"-20", codeToUse);
         response = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

         actualCharge = "20";
         sum = new Long (20);

         payWithBudgetResponse = (PayWithBudgetResponse) response;
         verifyDataStore(payWithBudgetResponse, payWithBudget, sum,actualCharge);

    }

    private String generateRandomCodes() {

        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < 5; i++) {
            String code = String.valueOf(BaseTest.getRandomInt());
            buffer.append(code + ",");
        }
        return buffer.toString();
    }

    private void verifyDataStore (PayWithBudgetResponse payWithBudgetResponse, PayWithBudget payWithBudget, Long sum, String actualCharge) throws Exception{

        Assert.assertEquals("status code must be 200", (Integer) 200,payWithBudgetResponse.getStatusCode());
        Assert.assertEquals("Result = Y ","Y",payWithBudgetResponse.getResult());
        Assert.assertFalse("Payment UID must not be empty!", StringUtils.isEmpty(payWithBudgetResponse.getPaymentUID()));
        Assert.assertEquals("Response Total Sum equals", actualCharge,payWithBudgetResponse.getActualCharge());

        Query qUserAction = new Query("UserAction");
        ArrayList<Query.Filter> filters = new ArrayList<>();
        filters.add(new Query.FilterPredicate("BudgetTransactionUID",Query.FilterOperator.EQUAL,payWithBudgetResponse.getPaymentUID()));
        filters.add(new Query.FilterPredicate("Action",Query.FilterOperator.EQUAL, "budgettransaction"));
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, filters));

        List<Entity> resultUserAction = serverHelper.queryWithWait("Must find UserAction with the BudgetTransactionUID: " + payWithBudgetResponse.getPaymentUID(),
                qUserAction, 1, dataStore);
        Assert.assertTrue("Transaction ID must be in User Action dta", ((Text)resultUserAction.get(0).getProperty("Data")).getValue().contains(payWithBudget.getTransactionId()));
        Assert.assertTrue("User Action must contains Actual Charge ",serverHelper.dataContainsConfirmation(resultUserAction,payWithBudgetResponse.getActualCharge()));


        //Verify with DataStore
        Query q = new Query("BudgetTransaction");
        q.setFilter(new Query.FilterPredicate("UID",Query.FilterOperator.EQUAL, payWithBudgetResponse.getPaymentUID()));
        List<Entity> result = serverHelper.queryWithWait("Must find one budgetTransaction with the UID: "+ payWithBudgetResponse.getPaymentUID(),
                q, 1, dataStore);
        Assert.assertEquals("ApiKey is equal",apiKey, result.get(0).getProperty("ApiKey"));
        Assert.assertEquals("BranchID is equal",payWithBudget.getBranchId(), result.get(0).getProperty("BranchID"));
        Assert.assertEquals("BusinessID is equal",locationId, result.get(0).getProperty("LocationID"));
        Assert.assertEquals("TransactionID is equal",payWithBudget.getTransactionId(), result.get(0).getProperty("TransactionID"));
        Assert.assertEquals("Sum is equal",sum, result.get(0).getProperty("Sum"));
        Assert.assertEquals("SumForTransaction is equal",sum, result.get(0).getProperty("SumForTransaction"));
        Assert.assertEquals("RequestedSum is equal",sum, result.get(0).getProperty("RequestedSum"));
        Assert.assertEquals("POSID is equal",payWithBudget.getPosId(), result.get(0).getProperty("PosID"));
        Assert.assertEquals("BudgetType is equal","GiftCard", result.get(0).getProperty("BudgetType"));
    }

    private PayWithBudget payWithBudget (String phonenumber, String totalSum, String verificationCode){

        Customer customer = new Customer();
        customer.setPhoneNumber(phonenumber);

        PayWithBudget payWithBudget = new PayWithBudget();
        payWithBudget.getCustomers().add(customer);
        payWithBudget.setAllowPartialPayment(true);
        payWithBudget.setTotalSum(totalSum);
        payWithBudget.setverificationCode(verificationCode);
        payWithBudget.setTimeStamp(String.valueOf(System.currentTimeMillis()));
        payWithBudget.setTransactionId(UUID.randomUUID().toString());
        payWithBudget.setBranchId(UUID.randomUUID().toString());
        payWithBudget.setPosId(UUID.randomUUID().toString());

        return payWithBudget;
    }


}
