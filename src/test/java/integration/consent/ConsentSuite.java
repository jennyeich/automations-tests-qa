package integration.consent;

import hub.base.BaseHubTest;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import integration.base.BaseIntegrationTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import server.base.BaseServerTest;
import server.v2_8.Consent;
import utils.PropsReader;

import java.io.IOException;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ConsentByRegistrationTest.class,
        ConsentGracePeriodTest.class,
        ConsentPostGraceTest.class,
        UpdateMemberConsentTest.class,
        AllowSmsAndEmailByRegistrationTest.class,
        FilterMembersByConsentTest.class
})
public class ConsentSuite {
    public static final Logger log4j = Logger.getLogger(ConsentSuite.class);


    @BeforeClass
    public static void setup() throws IOException {

        /*when this flag = false - no need to run this suite*/
        if(!Boolean.valueOf(PropsReader.getPropValuesForEnv(Consent.REGISTER_MEMBERS_BY_CONSENT)).booleanValue()) {
            log4j.info("registerMembersByConsent is false - No need to run this suite");
            System.exit(0);
        }
        // common initialization done once for all tests
        BaseIntegrationTest.init();
    }

    @AfterClass
    public static void tearDown(){

        try {
            BaseAutomationTest.deactivateAutomation();
            MembersService.deactivateSmartClubDeals();
        } catch (Exception e) {
            log4j.error(e);
        }
        log4j.info("close browser after finish test");
        BaseServerTest.cleanBase();
        BaseHubTest.cleanHub();
    }

}
