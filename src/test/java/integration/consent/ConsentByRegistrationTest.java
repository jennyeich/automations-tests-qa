package integration.consent;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.base.categories.serverCategories.ServerRegression;
import hub.base.categories.serverCategories.serverV2_8;
import hub.services.AcpService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.yandex.qatools.allure.annotations.Description;
import server.v2_8.Consent;
import server.v2_8.JoinClubResponse;
import server.v2_8.RegisterResponse;
import server.v4.RegisterMemberV4Response;
import utils.PropsReader;

import java.io.File;

public class ConsentByRegistrationTest extends BaseConsentTest {


    public static final String REG_IMPORT_MEMBER_PHONE_1 = "91919191919";
    public static final String REG_IMPORT_MEMBER_PHONE_2 = "2228882228";
    public static final String REG_IMPORT_MEMBER_PHONE_3 = "2828282846";
    public static final String REG_IMPORT_MEMBER_PHONE_4 ="2823822846";
    private static final String IMPORT_CONSENT_FILENAME = "testImportForConsent.csv";
    private static final String IMPORT_CONSENT_TRUE_FILENAME = "testImportForConsentImportTrue.csv";
    private static final String IMPORT2_CONSENT_FILENAME = "testImportForConsent2.csv";
    private static final String IMPORT2_UPDATE_CONSENT_FILENAME = "testImportUpdateForConsent2.csv";
    private static final String IMPORT3_CONSENT_FILENAME = "testImportForConsent3.csv";
    private static final String IMPORT3_UPDATE_CONSENT_FILENAME = "testImportUpdateForConsent3.csv";





    @Test
    @Description("Register member from JoinClub (app registration) to check consent")
    @Category({Hub1Regression.class,ServerRegression.class})
    public void testRegisterFromAppConsentIsYes() throws Exception {
        //register member from JoinClub (app registration)
        JoinClubResponse joinClubResponse = serverHelper.createMember();
        Assert.assertEquals(joinClubResponse.getRegistrationSource(),RegistrationSource.App.source);
        checkDataStoreWithConsentData(joinClubResponse.getUserKey(),joinClubResponse.getPhoneNumber(),ConsentStatus.Yes.status,RegistrationSource.App.source,ConsentStatus.Yes.status);
        checkMemberProfileLogs(joinClubResponse.getPhoneNumber(),JOINED_MESSAGE,RegistrationSource.App.toString());
        checkMemberProfileLogs(joinClubResponse.getPhoneNumber(),GAVE_CONSENT_MESSAGE,RegistrationSource.App.toString());
    }



    @Test
    @Description("Register member from AutoRegister (submit purchase)to check consent")
    public void testRegisterFromAutoRegisterConsentIsNo() throws Exception {

        String customerPhone = String.valueOf(System.currentTimeMillis());
        getAutoRegisterResponse(customerPhone);

        checkDataStoreWithConsentDataByPhoneOnly(customerPhone,ConsentStatus.No.status,RegistrationSource.AutoRegistration.source);
        checkMemberProfileLogs(customerPhone,JOINED_MESSAGE,RegistrationSource.POS.toString());

    }



    @Test
    @Description("Register member from Register through API V1 (old) to check consent")
    @Category({Hub1Regression.class,ServerRegression.class})
    public void testRegisterFromRegisterationV1ConsentIsNo() throws Exception {

        String customerPhone = String.valueOf(System.currentTimeMillis());
        RegisterResponse registerResponse = getRegisterV1Response(customerPhone);
        Assert.assertEquals(registerResponse.getRegistrationSource(),RegistrationSource.POS.source);
        Assert.assertEquals(registerResponse.getConsent(),ConsentStatus.No.status);
        Assert.assertEquals(registerResponse.getConsentForHub(),ConsentStatus.No.status);
        checkDataStoreWithConsentData(registerResponse.getUserKey(),registerResponse.getPhoneNumber(),ConsentStatus.No.status,RegistrationSource.POS.source,ConsentStatus.No.status);
        checkMemberProfileLogs(customerPhone,JOINED_MESSAGE,RegistrationSource.POS.toString());
    }

    @Test
    @Description("Register member from Register through API V1 (old) to check consent with consentIsOn = true")
    public void testRegisterFromRegisterationV1ConsentIsNoConsentIsOnTrue() throws Exception {

        serverHelper.updateConsentFlag(true);

        String customerPhone = String.valueOf(System.currentTimeMillis());
        RegisterResponse registerResponse = getRegisterV1Response(customerPhone);
        Assert.assertEquals(registerResponse.getRegistrationSource(),RegistrationSource.POS.source);
        Assert.assertEquals(registerResponse.getConsent(),ConsentStatus.No.status);
        Assert.assertEquals(registerResponse.getConsentForHub(),ConsentStatus.No.status);
        checkDataStoreWithConsentData(registerResponse.getUserKey(),registerResponse.getPhoneNumber(),ConsentStatus.No.status,RegistrationSource.POS.source,ConsentStatus.No.status);
        checkMemberProfileLogs(customerPhone,JOINED_MESSAGE,RegistrationSource.POS.toString());

        serverHelper.updateConsentFlag(Boolean.valueOf(PropsReader.getPropValuesForEnv(Consent.REGISTER_MEMBERS_BY_CONSENT)).booleanValue());
    }


    @Test
    @Description("Register member from Register through API V1 (old) to check consent with consentIsOn = false")
    public void testRegisterFromRegisterationV1ConsentIsNoConsentIsOnFalse() throws Exception {

        serverHelper.updateConsentFlag(false);

        String customerPhone = String.valueOf(System.currentTimeMillis());
        RegisterResponse registerResponse = getRegisterV1Response(customerPhone);
        Assert.assertEquals(registerResponse.getRegistrationSource(),RegistrationSource.POS.source);
        Assert.assertEquals(registerResponse.getConsent(),ConsentStatus.Existing.status);
        Assert.assertEquals(registerResponse.getConsentForHub(),ConsentStatus.Previous.status);
        checkDataStoreWithConsentData(registerResponse.getUserKey(),registerResponse.getPhoneNumber(),ConsentStatus.Existing.status,RegistrationSource.POS.source,ConsentStatus.Previous.status);
        checkMemberProfileLogs(customerPhone,JOINED_MESSAGE,RegistrationSource.POS.toString());

        serverHelper.updateConsentFlag(Boolean.valueOf(PropsReader.getPropValuesForEnv(Consent.REGISTER_MEMBERS_BY_CONSENT)).booleanValue());
    }



    @Test
    @Description("Register member from Register through API V4 (new) to check consent")
    @Category({Hub1Regression.class,ServerRegression.class})
    public void testRegisterFromRegisterationV4ConsentIsNo() throws Exception {
        String customerPhone = String.valueOf(System.currentTimeMillis());
        RegisterMemberV4Response registerMemberV4Response = serverHelper.registerMemberV4WithoutConsent(customerPhone,true);
        Assert.assertNotNull(registerMemberV4Response);

        checkDataStoreWithConsentDataByPhoneOnly(customerPhone,ConsentStatus.No.status,RegistrationSource.POS.source);
        checkMemberProfileLogs(customerPhone,JOINED_MESSAGE,RegistrationSource.POS.toString());
    }

    @Test
    @Description("Register member from Hub to check consent")
    public void testRegisterFromHubConsentIsNo() throws Exception {
        String customerPhone = String.valueOf(System.currentTimeMillis());
        //Create member
        hubMemberCreator.createMemberDefaultConsent(customerPhone, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        checkDataStoreWithConsentDataByPhoneOnly(customerPhone,ConsentStatus.No.status,RegistrationSource.Hub.source);

        checkMemberProfileLogs(customerPhone, JOINED_MESSAGE,RegistrationSource.Hub.toString());

    }

    @Test
    @Description("Register member from Import - GiveConsentOnImport flag is false ")
    public void testRegisterFromImportGiveConsentOnImportFalseConsentIsNo() throws Exception {

        serverHelper.updateBusinessBackend("GiveConsentOnImport", false);
        serverHelper.setRegistrationConfigurationV2();

        //import member
        AcpService.importMembers(IMPORT_FOLDER + File.separator + IMPORT_CONSENT_FILENAME);
        Thread.sleep(10000);
        String userKey = checkConsent(REG_IMPORT_MEMBER_PHONE_1,"No");

        checkDataStoreWithConsentData(userKey,REG_IMPORT_MEMBER_PHONE_1,ConsentStatus.Import_no.status,RegistrationSource.Import.source,ConsentStatus.No.status);

        checkMemberProfileLogs(REG_IMPORT_MEMBER_PHONE_1,JOINED_MESSAGE,RegistrationSource.Import.toString());

        //back to default settings of the 'GiveConsentOnImport' flag
        serverHelper.updateBusinessBackend("GiveConsentOnImport",PropsReader.getPropValuesForEnv("GiveConsentOnImport"));

    }
    @Test
    @Description("Register member from Import on Update - GiveConsentOnImport flag is true")
    public void testRegisterFromImportGiveConsentOnImportTrueConsentIsYesUpdate() throws Exception {

        serverHelper.updateBusinessBackend("GiveConsentOnImport", true);
        serverHelper.setRegistrationConfigurationV2();

        //import member
        AcpService.importMembers(IMPORT_FOLDER + File.separator + IMPORT3_CONSENT_FILENAME);
        Thread.sleep(10000);
        String userKey = checkConsent(REG_IMPORT_MEMBER_PHONE_4,"Previous business approval");
        checkDataStoreWithConsentData(userKey,REG_IMPORT_MEMBER_PHONE_4,ConsentStatus.Import_yes.status,RegistrationSource.Import.source,ConsentStatus.Previous.status);

        //Run import again with updated member
        AcpService.importMembers(IMPORT_FOLDER + File.separator + IMPORT3_UPDATE_CONSENT_FILENAME);
        Thread.sleep(10000);
        userKey = checkConsent(REG_IMPORT_MEMBER_PHONE_4,"Previous business approval");
        checkDataStoreWithConsentData(userKey,REG_IMPORT_MEMBER_PHONE_4,ConsentStatus.Import_yes.status,RegistrationSource.Import.source,ConsentStatus.Previous.status);


        checkMemberProfileLogs(REG_IMPORT_MEMBER_PHONE_4,UPDATE_MESSAGE,RegistrationSource.Import.toString());
        //back to default settings of the 'GiveConsentOnImport' flag
        serverHelper.updateBusinessBackend("GiveConsentOnImport",PropsReader.getPropValuesForEnv("GiveConsentOnImport"));

    }

    @Test
    @Description("Register member from Import on Update - GiveConsentOnImport flag is false ")
    @Category({Hub1Regression.class,ServerRegression.class})
    public void testRegisterFromImportGiveConsentOnImportFalseConsentIsNoUpdate() throws Exception {

        serverHelper.updateBusinessBackend("GiveConsentOnImport", false);
        serverHelper.setRegistrationConfigurationV2();

        //import member
        AcpService.importMembers(IMPORT_FOLDER + File.separator + IMPORT2_CONSENT_FILENAME);
        Thread.sleep(10000);
        String userKey = checkConsent(REG_IMPORT_MEMBER_PHONE_3,"No");
        checkDataStoreWithConsentData(userKey,REG_IMPORT_MEMBER_PHONE_3,ConsentStatus.Import_no.status,RegistrationSource.Import.source,ConsentStatus.No.status);

        //Run import again with updated member
        AcpService.importMembers(IMPORT_FOLDER + File.separator + IMPORT2_UPDATE_CONSENT_FILENAME);
        Thread.sleep(10000);
        userKey = checkConsent(REG_IMPORT_MEMBER_PHONE_3,"No");
        checkDataStoreWithConsentData(userKey,REG_IMPORT_MEMBER_PHONE_3,ConsentStatus.Import_no.status,RegistrationSource.Import.source,ConsentStatus.No.status);


        checkMemberProfileLogs(REG_IMPORT_MEMBER_PHONE_3,UPDATE_MESSAGE,RegistrationSource.Import.toString());

        //back to default settings of the 'GiveConsentOnImport' flag
        serverHelper.updateBusinessBackend("GiveConsentOnImport",PropsReader.getPropValuesForEnv("GiveConsentOnImport"));

    }



    @Test
    @Description("Register member from Import - GiveConsentOnImport flag is true ")
    public void testRegisterFromImportGiveConsentOnImportTrueConsentIsYes() throws Exception {

        serverHelper.updateBusinessBackend("GiveConsentOnImport",true);
        serverHelper.setRegistrationConfigurationV2();

        AcpService.importMembers(IMPORT_FOLDER + File.separator + IMPORT_CONSENT_TRUE_FILENAME);
        Thread.sleep(10000);
        String userKey = checkConsent(REG_IMPORT_MEMBER_PHONE_2,"Previous business approval");

        checkDataStoreWithConsentData(userKey,REG_IMPORT_MEMBER_PHONE_2,ConsentStatus.Import_yes.status,RegistrationSource.Import.source,ConsentStatus.Previous.status);

        checkMemberProfileLogs(REG_IMPORT_MEMBER_PHONE_2,JOINED_MESSAGE,RegistrationSource.Import.toString());

    }





}
