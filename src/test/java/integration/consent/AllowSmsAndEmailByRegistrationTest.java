package integration.consent;

import hub.common.objects.common.YesNoListNumber;
import org.junit.Assert;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import server.utils.Initializer;
import server.v2_8.JoinClubResponse;
import server.v2_8.UpdatedFields;
import server.v4.RegisterMemberV4Response;

import java.util.Map;

public class AllowSmsAndEmailByRegistrationTest extends BaseConsentTest {



    @Test
    @Description("Register member from JoinClub (app registration) to check allowEmail and allowSMS")
    public void testRegisterFromAppAllowSMSEmailInRegistration() throws Exception {

        removeAllowSMSAllowEmailgistrationForm();
        //Add fields to registration form
        addAllowSMSAllowEmailgistrationForm(YesNoListNumber.NO.toString());
        JoinClubResponse joinClubResponse = joinClub( true,  true);

        //find member and check the logs
        String phoneNumber = joinClubResponse.getPhoneNumber();
        checkDataStoreWithAllowSMSandEmail(joinClubResponse.getUserKey(),phoneNumber,true, true);
        checkAllowSMSAllowEmailYes(phoneNumber);
    }


    @Test
    @Description("Register member from JoinClub (app registration) to check allowEmail and allowSMS")
    public void testRegisterFromAppAllowSMSEmailInRegistration2() throws Exception {

        removeAllowSMSAllowEmailgistrationForm();

        //register member from JoinClub (app registration)
        JoinClubResponse joinClubResponse = joinClub(true,true );

        //find member and check the logs
        String phoneNumber = joinClubResponse.getPhoneNumber();
        checkDataStoreWithAllowSMSandEmail(joinClubResponse.getUserKey(),phoneNumber,true, true);
        checkAllowSMSAllowEmailYes(phoneNumber);

    }

    @Test
    @Description("Register member from JoinClub (app registration) to check allowEmail and allowSMS")
    public void testRegisterFromAppAllowSMSEmailInREgistration3() throws Exception {

        removeAllowSMSAllowEmailgistrationForm();
        //Add fields to registration form
        addAllowSMSAllowEmailgistrationForm(YesNoListNumber.NO.toString());

        //register member from JoinClub (app registration)
        JoinClubResponse joinClubResponse = joinClub( false,  false);

        //find member and check the logs
        String phoneNumber = joinClubResponse.getPhoneNumber();
        checkDataStoreWithAllowSMSandEmail(joinClubResponse.getUserKey(),phoneNumber,false, false);
        checkAllowSMSAllowEmailNo(phoneNumber);

    }



    @Test
    @Description("Register member from Hub to check AllowSMS and AllowEmail")
    public void testRegisterFromHubAllowEmailAllowSMSFalse() throws Exception {

        removeAllowSMSAllowEmailgistrationForm();
        addAllowSMSAllowEmailgistrationForm(YesNoListNumber.NO.toString());

        String customerPhone = String.valueOf(System.currentTimeMillis());
        //Create member
        hubMemberCreator.createMemberWithEmailSMS(customerPhone, AUTO_USER_PREFIX, AUTO_LAST_PREFIX,"", "");

        checkDataStoreWithConsentDataByPhoneOnlyEmailSMS(customerPhone,false, false);
        checkAllowSMSAllowEmailNo(customerPhone);

    }


    @Test
    @Description("Register member from Hub to check AllowSMS and AllowEmail")
    public void testRegisterFromHubAllowEmailAllowSMSTrue() throws Exception {

        removeAllowSMSAllowEmailgistrationForm();
        addAllowSMSAllowEmailgistrationForm(YesNoListNumber.YES.toString());

        String customerPhone = String.valueOf(System.currentTimeMillis());
        //Create member
        hubMemberCreator.createMemberWithEmailSMS(customerPhone, AUTO_USER_PREFIX, AUTO_LAST_PREFIX,"", "");

        checkDataStoreWithConsentDataByPhoneOnlyEmailSMS(customerPhone,true, true);
        checkAllowSMSAllowEmailYes(customerPhone);

    }


   @Test
    @Description("Register member from Hub to check AllowSMS and AllowEmail")
    public void testRegisterFromHubAllowEmailAllowSMSIsNotPartOfTheRegistrationForm() throws Exception {

        removeAllowSMSAllowEmailgistrationForm();

        String customerPhone = String.valueOf(System.currentTimeMillis());
        //Create member
        hubMemberCreator.createMemberWithEmailSMS(customerPhone, AUTO_USER_PREFIX, AUTO_LAST_PREFIX,"", "");

        checkAllowSMSAllowEmailNone(customerPhone);
    }



    @Test
    @Description("Register member from AutoRegister (submit purchase)to check allowSMS and allowEmail")
    public void testRegisterFromAutoRegisterAllowSMSandAllowEmailAreNone() throws Exception {

        removeAllowSMSAllowEmailgistrationForm();
        String customerPhone = String.valueOf(System.currentTimeMillis());
        getAutoRegisterResponse(customerPhone);

        checkAllowSMSAllowEmailNone(customerPhone);
    }


    @Test
    @Description("Register member from AutoRegister (submit purchase)to check allowSMS and allowEmail - defaults are Yes")
    public void testRegisterFromAutoRegisterAllowSMSandAllowEmailAreNone2DefaultsAreYes() throws Exception {

        removeAllowSMSAllowEmailgistrationForm();
        addAllowSMSAllowEmailgistrationForm(YesNoListNumber.YES.toString());
        String customerPhone = String.valueOf(System.currentTimeMillis());
        getAutoRegisterResponse(customerPhone);

        checkAllowSMSAllowEmailNone(customerPhone);
    }


    @Test
    @Description("Register member from AutoRegister (submit purchase)to check allowSMS and allowEmail - defaults are No")
    public void testRegisterFromAutoRegisterAllowSMSandAllowEmailAreNone2DefaultsAreNo() throws Exception {

        removeAllowSMSAllowEmailgistrationForm();
        addAllowSMSAllowEmailgistrationForm(YesNoListNumber.NO.toString());
        String customerPhone = String.valueOf(System.currentTimeMillis());
        getAutoRegisterResponse(customerPhone);

        checkAllowSMSAllowEmailNone(customerPhone);
    }



    @Test
    @Description("Register member from Register through API V1 (old) to check allowEmail and allowSMS is not part of the registration form")
    public void testRegisterFromRegisterationV1AllowSMSAllowEmail() throws Exception {

        Map<String,String> configFieldMap =Initializer.initConfigRegistrationNoEmailSMS();
        serverHelper.initConfiguration(locationId,configFieldMap);

       // serverHelper.initConfiguration(locationId,configFieldMap);
        String customerPhone = String.valueOf(System.currentTimeMillis());
        getRegisterV1Response(customerPhone);
        checkAllowSMSAllowEmailNone(customerPhone);

    }


    @Test
    @Description("Register member from Register through API V1 (old) to check allowEmail and allowSMS is part of the registration form but no value provided")
    public void testRegisterFromRegistrationV1AllowSMSAllowEmailIsNotPartOfTheRegistrationForm() throws Exception {

        serverHelper.updateConsentFlag(true);

        Map<String,String> configFieldMap =Initializer.initUnsubscribeConfigRegistration();
        serverHelper.initConfiguration(locationId,configFieldMap);

        String customerPhone = String.valueOf(System.currentTimeMillis());
        getRegisterV1ResponseNoAllowEmailAllowSMS(customerPhone,"true",false);
        checkAllowSMSAllowEmailNone(customerPhone);

    }



    @Test
    @Description("Register member from Register through API V1 (old) to check allowEmail and allowSMS is part of the registration form and the value is True")
    public void testRegisterFromRegisterationV1AllowSMSAllowEmailIsPartOfTheRegistrationForm() throws Exception {

        serverHelper.updateConsentFlag(true);

        Map<String,String> configFieldMap =Initializer.initUnsubscribeConfigRegistration();
        serverHelper.initConfiguration(locationId,configFieldMap);

        String customerPhone = String.valueOf(System.currentTimeMillis());
        getRegisterV1ResponseWithAllowEmailAllowSMS(customerPhone,"true",false);
        checkAllowSMSAllowEmailYes(customerPhone);

    }




    @Test
    @Description("Register member from Register through API V1 (old) to check allowEmail and allowSMS is part of the registration form and the value is False")
    public void testRegisterFromRegisterationV1AllowSMSAllowEmailIsPartOfTheRegistrationFormValueNo() throws Exception {

        serverHelper.updateConsentFlag(true);
        Map<String,String> configFieldMap =Initializer.initUnsubscribeConfigRegistration();
        serverHelper.initConfiguration(locationId,configFieldMap);

        String customerPhone = String.valueOf(System.currentTimeMillis());
        getRegisterV1ResponseWithAllowEmailAllowSMS(customerPhone,"false", false);
        checkAllowSMSAllowEmailNo(customerPhone);

    }



    @Test
    @Description("Register member from Register through API V4 (new) to check Allow SMS and Allow Email")
    public void testRegisterFromRegistrationV4AllowSMSAllowEmailIsNotPartOfTheRegistrationForm() throws Exception {

        String customerPhone = String.valueOf(System.currentTimeMillis());
        removeAllowSMSAllowEmailgistrationForm();
        RegisterMemberV4Response registerMemberV4Response = serverHelper.registerMemberV4WithoutConsent(customerPhone,true);
        Assert.assertNotNull(registerMemberV4Response);

        checkAllowSMSAllowEmailNone(customerPhone);
    }



    @Test
    @Description("Register member from Register through API V4 (new) to check Allow SMS and Allow Email")
    public void testRegisterFromRegistrationV4AllowSMSAllowEmailIsPartOfTheRegistrationForm() throws Exception {

        String customerPhone = String.valueOf(System.currentTimeMillis());

        removeAllowSMSAllowEmailgistrationForm();
        addAllowSMSAllowEmailgistrationForm(YesNoListNumber.NO.toString());

        RegisterMemberV4Response registerMemberV4Response = serverHelper.registerMemberV4WithAllowSMSAllowEmail(customerPhone,false,true, true);
        Assert.assertNotNull(registerMemberV4Response);

        checkAllowSMSAllowEmailYes(customerPhone);
    }


    private JoinClubResponse joinClub (boolean allowEmail, boolean allowSMS)throws Exception{

        //register member from JoinClub (app registration)
        String phoneMember = String.valueOf(System.currentTimeMillis());
        UpdatedFields updatedFields = new UpdatedFields();
        updatedFields.setAllowEmail(allowEmail);
        updatedFields.setAllowSMS(allowSMS);
        updatedFields.setPhoneNumber(phoneMember);
        JoinClubResponse joinClubResponse = serverHelper.createMember(updatedFields);
        return joinClubResponse;


    }






}
