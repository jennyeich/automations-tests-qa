package integration.consent;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.base.categories.serverCategories.ServerRegression;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.services.member.MembersService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Issue;
import server.utils.ServerHelper;
import server.v2_8.JoinClubResponse;
import server.v2_8.UpdatedFields;

public class UpdateMemberConsentTest extends BaseConsentTest{

    @Test
    @Description("Register member from Hub , Update membership through the app, consent turn to YES")
    @Category({Hub1Regression.class,ServerRegression.class})
    public void testRegisterFromHubConsentIsNo() throws Exception {
        String customerPhone = String.valueOf(System.currentTimeMillis());
        //Create member from hub
        NewMember newMember = hubMemberCreator.createMemberDefaultConsent(customerPhone, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String membershipKey = MembersService.getMembershipKey();
        checkDataStoreWithConsentDataByPhoneOnly(customerPhone,ConsentStatus.No.status,RegistrationSource.Hub.source);

        checkMemberProfileLogs(customerPhone,JOINED_MESSAGE,RegistrationSource.Hub.toString());

        UpdatedFields updatedFields = new UpdatedFields();
        updatedFields.setLastName(newMember.getLastName() + "_update");
        log4j.info("Update member from app: " + newMember.getPhoneNumber());
        serverHelper.updateMembership(membershipKey,updatedFields, RegistrationSource.App.source);

        log4j.info("Check source and log as expected");
        checkMemberProfileLogs(customerPhone,UPDATE_MESSAGE,RegistrationSource.App.toString());
        checkMemberProfileLogs(customerPhone,GAVE_CONSENT_MESSAGE,RegistrationSource.App.toString());

        log4j.info("Check data store updated to : " + ConsentStatus.Yes.status);
        checkDataStoreWithConsentDataByPhoneOnly(customerPhone,ConsentStatus.Yes.status,RegistrationSource.Hub.source);
    }

    @Test
    @Description("Register member from app , Update membership through the V1")
    public void testUpdateMembershipV1RegisterMemberFromApp() throws Exception {

        JoinClubResponse joinClubResponse = serverHelper.createMember();
        String customerPhone = joinClubResponse.getPhoneNumber();

        Assert.assertEquals(joinClubResponse.getRegistrationSource(),RegistrationSource.App.source);
        checkDataStoreWithConsentData(joinClubResponse.getUserKey(),joinClubResponse.getPhoneNumber(),ConsentStatus.Yes.status,RegistrationSource.App.source,ConsentStatus.Yes.status);
        checkMemberProfileLogs(customerPhone,JOINED_MESSAGE,RegistrationSource.App.toString());
        checkMemberProfileLogs(customerPhone,GAVE_CONSENT_MESSAGE,RegistrationSource.App.toString());


        log4j.info("Update member from V1: " + customerPhone);
        serverHelper.updateMemberExternal(customerPhone,customerPhone + "_update");

        log4j.info("Check source updated and log as expected");
        checkMemberProfileLogs(customerPhone,UPDATE_MESSAGE,RegistrationSource.POS.toString());

        log4j.info("Check data store updated to : " + ConsentStatus.Yes.status);
        checkDataStoreWithConsentDataByPhoneOnly(customerPhone,ConsentStatus.Yes.status,RegistrationSource.App.source);
    }

    @Test
    @Description("Register member from Hub , Update membership through the V4")
    @Category({Hub1Regression.class,ServerRegression.class})
    public void testUpdateMembershipV4RegisterMemberFromHub() throws Exception {

        String customerPhone = String.valueOf(System.currentTimeMillis());
        //Create member from hub
        NewMember newMember = hubMemberCreator.createMemberDefaultConsent(customerPhone, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String membershipKey = MembersService.getMembershipKey();
        checkDataStoreWithConsentDataByPhoneOnly(customerPhone,ConsentStatus.No.status,RegistrationSource.Hub.source);

        checkMemberProfileLogs(customerPhone,JOINED_MESSAGE,RegistrationSource.Hub.toString());

        log4j.info("Update member from V4: " + customerPhone);
        serverHelper.updateMemberV4(customerPhone, Boolean.TRUE, Boolean.TRUE);

        log4j.info("Check source updated and log as expected");
        checkMemberProfileLogs(customerPhone,UPDATE_MESSAGE,RegistrationSource.POS.toString());

        log4j.info("Check data store updated to : " + ConsentStatus.No.status);
        checkDataStoreWithConsentDataByPhoneOnly(customerPhone,ConsentStatus.No.status,RegistrationSource.Hub.source);
    }

    @Test
    @Description("Register member from Hub , Update membership through the hub")
    public void testUpdateMemberFromHub() throws Exception {

        String customerPhone = String.valueOf(System.currentTimeMillis());
        //Create member from hub
        NewMember newMember = hubMemberCreator.createMemberDefaultConsent(customerPhone, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String membershipKey = MembersService.getMembershipKey();
        checkDataStoreWithConsentDataByPhoneOnly(customerPhone,ConsentStatus.No.status,RegistrationSource.Hub.source);

        checkMemberProfileLogs(customerPhone,JOINED_MESSAGE,RegistrationSource.Hub.toString());
        log4j.info("Update member from hub: " + customerPhone);
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setPhoneNumber(customerPhone);
        memberDetails.setFirstName(newMember.getFirstName() + "_update");
        MembersService.updateMemberDetails(memberDetails);

        log4j.info("Check source updated and log as expected");
        checkMemberProfileLogs(customerPhone,UPDATE_MESSAGE,RegistrationSource.Hub.toString());

        log4j.info("Check data store updated to : " + ConsentStatus.No.status);
        checkDataStoreWithConsentDataByPhoneOnly(customerPhone,ConsentStatus.No.status,RegistrationSource.Hub.source);
    }

    @Test
    @Description("Register member from Hub , Update membership and give consent from landing page, consent turn to YES")
    public void testRegisterFromHubGiveConsentFromLandingPage() throws Exception {

        String customerPhone = String.valueOf(System.currentTimeMillis());
        //Create member from hub
        NewMember newMember = hubMemberCreator.createMemberDefaultConsent(customerPhone, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String membershipKey = MembersService.getMembershipKey();
        checkDataStoreWithConsentDataByPhoneOnly(customerPhone,ConsentStatus.No.status,RegistrationSource.Hub.source);

        checkMemberProfileLogs(customerPhone,JOINED_MESSAGE,RegistrationSource.Hub.toString());

        UpdatedFields updatedFields = new UpdatedFields();
        updatedFields.setLastName(newMember.getLastName() + "_update");
        log4j.info("Update member from app: " + newMember.getPhoneNumber());
        serverHelper.updateMembership(membershipKey,updatedFields, RegistrationSource.LandingPage.source);

        log4j.info("Check source updated to :  " + RegistrationSource.LandingPage.source + " and log as expected");
        checkMemberProfileLogs(customerPhone,UPDATE_MESSAGE,RegistrationSource.LandingPage.toString());
        checkMemberProfileLogs(customerPhone,GAVE_CONSENT_MESSAGE,RegistrationSource.LandingPage.toString());

        log4j.info("Check data store updated to : " + ConsentStatus.Yes.status);
        checkDataStoreWithConsentDataByPhoneOnly(customerPhone,ConsentStatus.Yes.status,RegistrationSource.Hub.source);
    }
}
