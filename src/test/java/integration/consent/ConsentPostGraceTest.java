package integration.consent;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.base.categories.serverCategories.ServerRegression;
import hub.common.objects.member.FindMember;
import hub.services.member.MembersService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.yandex.qatools.allure.annotations.Description;
import server.common.AnonymousPurchaseTypes;
import server.common.ApiClientBuilder;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.utils.PosErrorsEnum;
import server.v2_8.*;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Customer;
import server.v4.GetBenefitsResponse;
import server.v4.PaymentAPI;
import server.v4.PaymentAPIResponse;
import server.v4.common.enums.Mode;
import server.v4.common.enums.PaymentMethod;
import server.v4.common.models.Membership;
import server.v4.common.models.PaymentQueryParams;
import server.v4.common.models.Purchase;

import java.util.UUID;

import static utils.StringUtils.StringToKey;

public class ConsentPostGraceTest extends BaseConsentTest{

    private static final String POST_GRACE_FIRST_LAST_NAME ="No consent";

    /*************************************PostGraceWithoutConsentRegisterationV4****************************************************/

    @Test
    @Description("Member without grace (Post grace period) - register API V4 (new),test GMD")
    @Category({Hub1Regression.class,ServerRegression.class})
    public void testMemberPostGraceWithoutConsentRegisterationV4_GMD() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()));
        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone1,true);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone2,true);


        GetMemberDetailsResponse getMemberDetailsResponse1 = getMemberDetailsV2_8Response(customerPhone1);
        String membershipKey1 = getMemberDetailsResponse1.getMemberships().get(0).getKey();
        String userKey1 = getMemberDetailsResponse1.getMemberships().get(0).getUserKey();


        String membershipKey2 = getMemberDetailsV2_8Response(customerPhone2).getMemberships().get(0).getKey();
        String userKey2 = getMemberDetailsV2_8Response(customerPhone2).getMemberships().get(0).getUserKey();


        //update members to be after grace period (32 days)
        updateUserAtLocationToPostGracePeriod(customerPhone1);
        updateUserAtLocationToPostGracePeriod(customerPhone2);

        Thread.sleep(20000);
        log4j.info("Checking GMD response for Member without grace (Post grace period)");
        //check GMD v2.8
        getMemberDetailsResponse1 = getMemberDetailsV2_8Response(customerPhone1);
        checkGMDResponseForPostGrace(getMemberDetailsResponse1,POST_GRACE_FIRST_LAST_NAME,POST_GRACE_FIRST_LAST_NAME,ConsentStatus.No.status,REGISTERED);

        Membership membership2 = getMemberDetailsV4Response(customerPhone2);
        //check GMD v4
        checkGMDV4ResponseForPostGrace(membership2,POST_GRACE_FIRST_LAST_NAME,POST_GRACE_FIRST_LAST_NAME,INACTIVE);
    }

    @Test
    @Description("Member without grace (Post grace period) - register API V4 (new),test GetBenefits ")
    public void testMemberPostGraceWithoutConsentRegisterationV4_GetBenefits() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()));
        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone1,true);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone2,true);


        GetMemberDetailsResponse getMemberDetailsResponse1 = getMemberDetailsV2_8Response(customerPhone1);
        String membershipKey1 = getMemberDetailsResponse1.getMemberships().get(0).getKey();
        String userKey1 = getMemberDetailsResponse1.getMemberships().get(0).getUserKey();


        String membershipKey2 = getMemberDetailsV2_8Response(customerPhone2).getMemberships().get(0).getKey();
        String userKey2 = getMemberDetailsV2_8Response(customerPhone2).getMemberships().get(0).getUserKey();


        //update members to be after grace period (32 days)
        updateUserAtLocationToPostGracePeriod(customerPhone1);
        updateUserAtLocationToPostGracePeriod(customerPhone2);

        Thread.sleep(20000);



        IServerResponse getMemberBenefitsResponse = getBenefitsv2_8PostGraceResponse(customerPhone1);
        if (getMemberBenefitsResponse instanceof ZappServerErrorResponse) {
            Assert.assertEquals( "One or more customers not found",((ZappServerErrorResponse) getMemberBenefitsResponse).getErrorMessage());
            Assert.assertEquals( "1012",((ZappServerErrorResponse) getMemberBenefitsResponse).getErrorCode());
        }

        IServerResponse getBenefitsResponse = getBenefitsV4PostGraceResponse(customerPhone2);
        checkV4ResponsePostGrace(getBenefitsResponse,PosErrorsEnum.CUSTOMER_S_NOT_FOUND);
    }

    @Test
    @Description("Member without grace (Post grace period) - register API V4 (new),test payment ")
    @Category({Hub1Regression.class,ServerRegression.class})
    public void testMemberPostGraceWithoutConsentRegisterationV4_Payment() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()));
        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone1,true);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone2,true);


        GetMemberDetailsResponse getMemberDetailsResponse1 = getMemberDetailsV2_8Response(customerPhone1);
        String membershipKey1 = getMemberDetailsResponse1.getMemberships().get(0).getKey();
        String userKey1 = getMemberDetailsResponse1.getMemberships().get(0).getUserKey();


        String membershipKey2 = getMemberDetailsV2_8Response(customerPhone2).getMemberships().get(0).getKey();
        String userKey2 = getMemberDetailsV2_8Response(customerPhone2).getMemberships().get(0).getUserKey();


        //update members to be after grace period (32 days)
        updateUserAtLocationToPostGracePeriod(customerPhone1);
        updateUserAtLocationToPostGracePeriod(customerPhone2);

        Thread.sleep(20000);


        log4j.info("Checking pay with budget response for Member without grace (Post grace period)");
        //check payWithBudget
        IServerResponse responsePayWithBudget = getPayWithBudgetResponse(userKey1,membershipKey1,customerPhone1,"1000");
        if (responsePayWithBudget instanceof ZappServerErrorResponse) {
            Assert.assertEquals( "One or more customers not found",((ZappServerErrorResponse) responsePayWithBudget).getErrorMessage());
            Assert.assertEquals( "1012",((ZappServerErrorResponse) responsePayWithBudget).getErrorCode());
        }

        log4j.info("Checking payment response for Member without grace (Post grace period)");
        //check payment
        IServerResponse responsePayment =  getPaymentResponse(userKey2,membershipKey2,customerPhone2,1000);
        checkV4ResponsePostGrace(responsePayment,PosErrorsEnum.CUSTOMER_S_NOT_FOUND);

    }



    @Test
    @Description("Member without grace (Post grace period) - register API V4 (new),test submit purchase and smart automation ")
    public void testMemberPostGraceWithoutConsentRegisterationV4_SubmitPurchase() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()));
        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone1,true);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone2,true);



        //update members to be after grace period (32 days)
        updateUserAtLocationToPostGracePeriod(customerPhone1);
        updateUserAtLocationToPostGracePeriod(customerPhone2);

        Thread.sleep(20000);

        log4j.info("Checking Submit purchase response for Member without grace (Post grace period)");
        IServerResponse responseV2_8 = submitPurchaseV2_8PostGrace(customerPhone1);
        if (responseV2_8 instanceof ZappServerErrorResponse) {
            Assert.assertEquals( "One or more customers not found",((ZappServerErrorResponse) responseV2_8).getErrorMessage());
            Assert.assertEquals( "1012",((ZappServerErrorResponse) responseV2_8).getErrorCode());
        }

        IServerResponse responseV4 = submitPurchaseV4_0PostGrace(customerPhone2);
        checkV4ResponsePostGrace(responseV4,PosErrorsEnum.PROPRIETARY_CUSTOMER_NOT_FOUND);


        log4j.info("Checking smart automation run for Member without grace (Post grace period)");
        //check submit purchase on anonymous member
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()));
        submitPurchaseV2_8(customerPhone1);

        try {
            FindMember findMember = new FindMember();
            findMember.setPhoneNumber(customerPhone1);
            MembersService.findAndGetMember(findMember);
            if(MembersService.isGiftExists(smartGift.getTitle()))
                Assert.assertTrue("Member with phone : " + customerPhone1 + " should not have the gift by automation",false);
        } catch (Exception e) {
            Assert.assertTrue(true);
        }
        //check submit purchase V4
        submitPurchaseV4_0(customerPhone2);
        try {
            FindMember findMember = new FindMember();
            findMember.setPhoneNumber(customerPhone2);
            MembersService.findAndGetMember(findMember);
            if(MembersService.isGiftExists(smartGift.getTitle()))
                Assert.assertTrue("Member with phone : " + customerPhone2 + "  should not have the gift by automation",false);
        } catch (Exception e) {
            Assert.assertTrue(true);
        }
    }


    /*************************************PostGraceWithConsentRegisterationV4****************************************************/

    @Test
    @Description("Member with consent (Post grace period ) - register API V4 (new),test GMD")
    public void testMemberPostGraceWithConsentRegisterationV4_GMD() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()));

        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone1,true);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone2,true);

        //update members to be after grace period (32 days)
        updateUserAtLocationToPostGracePeriod(customerPhone1);
        updateUserAtLocationToPostGracePeriod(customerPhone2);


        Thread.sleep(10000);
        log4j.info("Checking GMD response for Member with consent (Post grace period )");
        //update consent  = YES
        GetMemberDetailsResponse getMemberDetailsResponse1 = getMemberDetailsV2_8Response(customerPhone1);
        String membershipKey1 = getMemberDetailsResponse1.getMemberships().get(0).getKey();
        serverHelper.updateMembership(membershipKey1,new UpdatedFields(), RegistrationSource.App.source);

        //check GMD V2.8
        getMemberDetailsResponse1 = getMemberDetailsV2_8Response(customerPhone1);
        checkGMDResponse(getMemberDetailsResponse1,RegistrationSource.POS.source,ConsentStatus.Yes.status);

        //update consent  = YES
        GetMemberDetailsResponse getMemberDetailsResponse2 = getMemberDetailsV2_8Response(customerPhone2);
        String membershipKey2 = getMemberDetailsResponse2.getMemberships().get(0).getKey();
        serverHelper.updateMembership(membershipKey2,new UpdatedFields(), RegistrationSource.App.source);

        //check GMD V4
        Membership membership2 = getMemberDetailsV4Response(customerPhone2);
        checkGMDV4Response(membership2,customerPhone2);

    }

    @Test
    @Description("Member with consent (Post grace period ) - register API V4 (new),testGetBenefits ")
    public void testMemberPostGraceWithConsentRegisterationV4_GetBenefits() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()));

        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone1,true);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone2,true);

        //update members to be after grace period (32 days)
        updateUserAtLocationToPostGracePeriod(customerPhone1);
        updateUserAtLocationToPostGracePeriod(customerPhone2);


        Thread.sleep(10000);
        log4j.info("Checking GMD response for Member with consent (Post grace period )");
        //update consent  = YES
        GetMemberDetailsResponse getMemberDetailsResponse1 = getMemberDetailsV2_8Response(customerPhone1);
        String membershipKey1 = getMemberDetailsResponse1.getMemberships().get(0).getKey();
        serverHelper.updateMembership(membershipKey1,new UpdatedFields(), RegistrationSource.App.source);

        //update consent  = YES
        GetMemberDetailsResponse getMemberDetailsResponse2 = getMemberDetailsV2_8Response(customerPhone2);
        String membershipKey2 = getMemberDetailsResponse2.getMemberships().get(0).getKey();
        serverHelper.updateMembership(membershipKey2,new UpdatedFields(), RegistrationSource.App.source);


        log4j.info("Checking get benefits response for Member with consent (Post grace period )");
        //check getBenefits V2.8
        GetMemberBenefitsResponse getMemberBenefitsResponse = getBenefitsv2_8Response(customerPhone1);
        Assert.assertEquals("-100",getMemberBenefitsResponse.getTotalSumDiscount());

        //check getBenefits V4
        GetBenefitsResponse getBenefitsResponse = getBenefitsV4Response(customerPhone2);
        Assert.assertEquals(Integer.valueOf(-100),getBenefitsResponse.getTotalDiscountSum());

    }

    @Test
    @Description("Member with consent (Post grace period ) - register API V4 (new),test submit purchase")
    @Category({Hub1Regression.class,ServerRegression.class})
    public void testMemberPostGraceWithConsentRegisterationV4_SubmitPurchase() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()));

        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone1,true);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone2,true);

        //update members to be after grace period (32 days)
        updateUserAtLocationToPostGracePeriod(customerPhone1);
        updateUserAtLocationToPostGracePeriod(customerPhone2);


        Thread.sleep(10000);
        log4j.info("Checking GMD response for Member with consent (Post grace period )");
        //update consent  = YES
        GetMemberDetailsResponse getMemberDetailsResponse1 = getMemberDetailsV2_8Response(customerPhone1);
        String membershipKey1 = getMemberDetailsResponse1.getMemberships().get(0).getKey();
        serverHelper.updateMembership(membershipKey1,new UpdatedFields(), RegistrationSource.App.source);

        //update consent  = YES
        GetMemberDetailsResponse getMemberDetailsResponse2 = getMemberDetailsV2_8Response(customerPhone2);
        String membershipKey2 = getMemberDetailsResponse2.getMemberships().get(0).getKey();
        serverHelper.updateMembership(membershipKey2,new UpdatedFields(), RegistrationSource.App.source);

        log4j.info("Checking Submit purchase response for Member with consent (Post grace period )");
        //check submit purchase V2.8
        submitPurchaseV2_8(customerPhone1);
        checkMemberHasTheGiftBySmartAutomation(customerPhone1);
        //check submit purchase V4
        submitPurchaseV4_0(customerPhone2);
        checkMemberHasTheGiftBySmartAutomation(customerPhone2);


    }


    /*************************************UserNoMembership****************************************************/


    @Test
    @Description("User without membership - GMD")
    public void testUserNoMembership_GMD() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()));
        String userPhone1 = "1010101010"; //user1 not register for testing 2.8 POS
        String userPhone2 = "2020202020";//user2 not register for testing 4.0 POS

        //check GMD v2.8
        log4j.info("Checking GMD response for no membership user");
        IServerResponse response1 = getMemberDetailsV2_8ResponseNoValidation(userPhone1);
        if (response1 instanceof ZappServerErrorResponse) {
            Assert.assertEquals( "One or more customers not found",((ZappServerErrorResponse) response1).getErrorMessage());
            Assert.assertEquals( "1012",((ZappServerErrorResponse) response1).getErrorCode());
        }


        IServerResponse response2 = getMemberDetailsV4ResponseNoMmembership(userPhone2);
        //check GMD v4
        checkV4ResponsePostGrace(response2,PosErrorsEnum.SINGLE_CUSTOMER_NOT_FOUND);
    }

    @Test
    @Description("User without membership - GetBenefits")
    public void testUserNoMembership_GetBenefits() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()));
        String userPhone1 = "1010101010"; //user1 not register for testing 2.8 POS

        String userPhone2 = "2020202020";//user2 not register for testing 4.0 POS

        log4j.info("Checking getBenefits response for no membership user");

        IServerResponse getMemberBenefitsResponse = getBenefitsv2_8PostGraceResponse(userPhone1);
        if (getMemberBenefitsResponse instanceof ZappServerErrorResponse) {
            Assert.assertEquals( "One or more customers not found",((ZappServerErrorResponse) getMemberBenefitsResponse).getErrorMessage());
            Assert.assertEquals( "1012",((ZappServerErrorResponse) getMemberBenefitsResponse).getErrorCode());
        }

        IServerResponse getBenefitsResponse = getBenefitsV4PostGraceResponse(userPhone2);
        checkV4ResponsePostGrace(getBenefitsResponse,PosErrorsEnum.CUSTOMER_S_NOT_FOUND);
    }

    @Test
    @Description("User without membership - submitPurchase")
    public void testUserNoMembership_SubmitPurchase() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()));
        String userPhone1 = "1010101010"; //user1 not register for testing 2.8 POS

        String userPhone2 = "2020202020";//user2 not register for testing 4.0 POS

        log4j.info("Checking Submit purchase response for no membership user");
        //check submit purchase
        IServerResponse responseV2_8 = submitPurchaseV2_8PostGrace(userPhone1);
        if (responseV2_8 instanceof ZappServerErrorResponse) {
            Assert.assertEquals( "One or more customers not found",((ZappServerErrorResponse) responseV2_8).getErrorMessage());
            Assert.assertEquals( "1012",((ZappServerErrorResponse) responseV2_8).getErrorCode());
        }

        IServerResponse responseV4 = submitPurchaseV4_0PostGrace(userPhone2);
        checkV4ResponsePostGrace(responseV4,PosErrorsEnum.PROPRIETARY_CUSTOMER_NOT_FOUND);


    }

    private void checkGMDV4ResponseForPostGrace( Membership membership,String fName,String lName,String memberStatus) {
        Assert.assertEquals(membership.getStatus(),memberStatus);
        Assert.assertEquals(membership.getFirstName(),fName);
        Assert.assertEquals(membership.getLastName(),lName);
    }

    private void checkGMDResponseForPostGrace(GetMemberDetailsResponse gmdResponse ,String fName,String lName,String consent,String memberStatus) {
        server.v2_8.common.Membership membership = gmdResponse.getMemberships().get(0);
        Assert.assertEquals(membership.getFirstName(),fName);
        Assert.assertEquals(membership.getLastName(),lName);
        Assert.assertEquals(membership.getConsent(),consent);
        Assert.assertEquals(membership.getMembershipStatus(),memberStatus);
    }

    private void checkV4ResponsePostGrace(IServerResponse response,PosErrorsEnum errorsEnum) {
        //Make sure the response is not Error response
        if (response instanceof server.v4.base.ZappServerErrorResponse) {
            Assert.fail(((server.v4.base.ZappServerErrorResponse) response).getErrorMessage());
        }

        Assert.assertEquals(errorsEnum.getMessage(),serverHelper.getError(response).get(MESSAGE).toString());
        Assert.assertEquals(errorsEnum.getCode(),serverHelper.getError(response).get(CODE).toString());
    }


   private IServerResponse getPayWithBudgetResponse(String userKey,String membershipKey ,String customerPhone,String totalSum) throws Exception{

       Customer customer = new Customer();
       customer.setPhoneNumber(customerPhone);
       serverHelper.addPointsToMember("300000",membershipKey,userKey);

       //Set pay with budget
       PayWithBudget payWithBudget = v2_8Builder.buildPayWithBudget(customer, totalSum);

       // Send the request
       IServerResponse response = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);
       return response;
   }


    private IServerResponse getPaymentResponse(String userKey,String membershipKey ,String customerPhone,int totalSum) throws Exception{

        server.v4.common.models.Customer customerv4 = new server.v4.common.models.Customer();
        customerv4.setPhoneNumber(customerPhone);

        serverHelper.addPointsToMember("300000",membershipKey,userKey);
        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.DISCOUNT;
        PaymentQueryParams queryParams = new PaymentQueryParams();
        queryParams.setAllowedPaymentMethod(paymentMethod);
        queryParams.setMode(Mode.PAY);


        //build purchase
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customerv4,totalSum,purchase);

        // Send the request
        IServerResponse response = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        return response;
    }

    private IServerResponse getRedeemResponse(String membershipKey,String assetKey){

        Key dataStoreAssetKey = StringToKey(assetKey);

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned", "Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused", "Unused", result.getProperty("RedeemState"));

        RedeemAsset redeemAsset =v2_8Builder.buildRedeemAsset(membershipKey, assetKey,locationId,serverToken);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);
        return  response;
    }

}
