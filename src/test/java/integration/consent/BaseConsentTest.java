package integration.consent;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.common.objects.common.RegistrationField;
import hub.common.objects.common.YesNoListNumber;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.operations.GeneralPOSSettingsPage;
import hub.common.objects.operations.POSMembershipPurchaseFlow;
import hub.common.objects.operations.PurchaseRegistrationOptions;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.SmartClubDeal;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.smarObjects.Scenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftActions;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.AddDiscountAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.AmountOffNumber;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.DiscountTypeOperation;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.PercentOff;
import hub.discounts.BaseHub1Discount;
import hub.discounts.v4.GetBenefitsHelper;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartClubDealAction;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.ApplyIf;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Condition;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.TagOperator;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.OperationService;
import hub.services.assets.AssetService;
import hub.services.member.MembersService;
import hub.utils.SmartAutomationsHelper;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.common.LocationBackendFields;
import server.common.PaymentType;
import server.utils.Initializer;
import server.utils.ServerHelper;
import server.utils.V2_8Builder;
import server.utils.V4Builder;
import server.v2_8.*;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Item;
import server.v4.GetBenefitsResponse;
import server.v4.SubmitPurchase;
import server.v4.SubmitPurchaseResponse;
import server.v4.common.models.Customer;
import server.v4.common.models.Membership;
import server.v4.common.models.Purchase;
import server.v4.common.models.PurchaseItem;
import utils.StringUtils;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.text.MessageFormat;
import java.util.*;

public class BaseConsentTest extends BaseIntegrationTest {

    public static final String REG_ITEM_CODE = "1111";
    public static final String REG_ITEM_TAG ="AUTO_REGISTER";
    public static final String REGISTERED = "registered";
    public static final String INACTIVE = "Inactive";
    public static final String ACTIVE = "Active";
    protected static SmartGift smartGift;

    public static final String JOINED_MESSAGE = "Joined the program from {0}";
    public static final String GAVE_CONSENT_MESSAGE = "Consented to T&C from {0}";
    public static final String UPDATE_MESSAGE = "Updated membership details from {0}";
    public static final String SUBSCRIBED_SMS = "Subscribed to receive marketing SMS";
    public static final String SUBSCRIBED_EMAIL = "Subscribed receiving email";
    public static final String UNSUBSCRIBED_SMS = "Unsubscribed receiving sms";
    public static final String UNSUBSCRIBED_EMAIL = "Unsubscribed receiving email";

    /*flag is needed when using suite and we don't want to init some data for every class in the suite,only one time*/
    public static boolean initFinished = false;


    protected V4Builder v4Builder = new V4Builder(apiKey);
    protected V2_8Builder v2_8Builder = new V2_8Builder(apiKey);
    protected static SmartAutomationsHelper automationsHelper = new SmartAutomationsHelper(locationId, apiKey, serverToken);


    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();

        init();
        if (!initFinished) {
            setPaymentConfiguration();
            try {
                updateAutoRegisterCode();
            } catch (Exception e) {
                log4j.error("Failed updating registration code", e);
            }
            try {
                createSmartAutomationForTest();
                createSmartClubDealWithDiscount10PercentOff();
            } catch (Exception e) {
                log4j.error("Failed creating smart automation", e);
            }
            initFinished = true;
        }



    }

    private static void setPaymentConfiguration(){
        serverHelper.saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);

        Map<String, Object> props = new HashMap<>();
        props.put(LocationBackendFields.PAY_WITH_BUDGET_MIN_SUM.key(), "-1");
        props.put(LocationBackendFields.PAY_WITH_BUDGET_MAX_SUM.key(), "-1");
        props.put(LocationBackendFields.PAY_WITH_BUDGET_TYPE.key(), PaymentType.POINTS.type());
        serverHelper.updateBusinessBackend(props);
    }



    private static void createSmartAutomationForTest() throws Exception {

        String timestamp = String.valueOf(System.currentTimeMillis());
        smartGift = hubAssetsCreator.createSmartGift(timestamp);
        Settings settings = new Settings();
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(Triggers.SubmitAPurchase, "SubmitPurchaseGrace_" + timestamp, settings);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.DoesntContainAny);
        String tagForItem = REG_ITEM_TAG;
        Condition condition = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.PURCHASE, hub.hub1_0.common.objects.smarts.smarObjects.conditions.Purchase.Tags, oper, tagForItem);
        automationsHelper.createSmartAutomationScenario(automation, Lists.newArrayList(condition), false, Lists.newArrayList(SmartAutomationActions.SendAnAsset), smartGift.getTitle(), null);

    }

    private static SmartClubDeal createSmartClubDealWithDiscount10PercentOff() throws Exception {
        SmartClubDeal smartClubDeal = hubAssetsCreator.buildSmartClubDeal("10 Percent", "10 Percent");
        Scenario scenario = hubAssetsCreator.createSmartAssetScenario(smartClubDeal, "10_PERCENT");

        //Create  action
        SmartClubDealAction clubDealAction = GetBenefitsHelper.createSmartClubDealAction(SmartGiftActions.AddDiscount.toString(), "10 Percent to all club members");
        //create discount smartGiftAction
        AddDiscountAction discount = GetBenefitsHelper.createAddDiscountAction(smartClubDeal, "10 Percent to all club members", DiscountTypeOperation.PercentOff.toString());

        //percent off smartGiftAction
        PercentOff percentOff = new PercentOff(smartClubDeal);
        AmountOffNumber amountOffNumber1 = new AmountOffNumber(smartClubDeal);
        amountOffNumber1.setAmountOffNumber("10");
        percentOff.setAmountOffNumber(amountOffNumber1);

        GetBenefitsHelper.setDiscountType(clubDealAction, percentOff, discount);

        scenario.addAction(clubDealAction, smartClubDeal);

        AssetService.createSmartClubDeal(smartClubDeal);

        return smartClubDeal;
    }

    protected void addAllowSMSAllowEmailgistrationForm (String defaultValue) throws Exception {

        List<RegistrationField> fields = Arrays.asList(RegistrationField.AllowEmail, RegistrationField.AllowSMS);
        OperationService.addRequiredFieldsToRegistrationForm(fields, YesNoListNumber.NO.toString(), defaultValue);

    }

    protected void removeAllowSMSAllowEmailgistrationForm () throws Exception {

        OperationService.removeFieldsFromRegistrationForm(Arrays.asList(RegistrationField.AllowSMS, RegistrationField.AllowEmail));

    }

    private static void updateAutoRegisterCode() throws Exception {

        GeneralPOSSettingsPage generalPOSSettingsPage = new GeneralPOSSettingsPage();
        POSMembershipPurchaseFlow purchaseFlow = new POSMembershipPurchaseFlow();
        purchaseFlow.setAllowPosRegistration(PurchaseRegistrationOptions.AUTO_REGISTER.toString());
        purchaseFlow.setRegItemCode(REG_ITEM_CODE);
        generalPOSSettingsPage.setPurchaseFlow(purchaseFlow);
        OperationService.updatePOSSettings(generalPOSSettingsPage);
    }

    protected static RegisterResponse getRegisterV1Response(String customerPhone) {
        Map<String,String> fieldMap = new HashMap<>();
        fieldMap.put(Initializer.FIRST_NAME,Initializer.FIRST_NAME + "_" +customerPhone);
        fieldMap.put(Initializer.LAST_NAME,Initializer.LAST_NAME + "_" +customerPhone);
        fieldMap.put(Initializer.PHONE_NUMBER, customerPhone);
        return (RegisterResponse) serverHelper.registerMemberWithoutConsent(fieldMap,true);
    }

    protected static RegisterResponse getRegisterV1ResponseWithAllowEmailAllowSMS(String customerPhone, String value, boolean initConfigRegistration) {
        Map<String,String> fieldMap = new HashMap<>();
        fieldMap.put(Initializer.FIRST_NAME,Initializer.FIRST_NAME + "_" +customerPhone);
        fieldMap.put(Initializer.LAST_NAME,Initializer.LAST_NAME + "_" +customerPhone);
        fieldMap.put(Initializer.PHONE_NUMBER, customerPhone);
        fieldMap.put(Initializer.ALLOW_EMAIL,value);
        fieldMap.put(Initializer.ALLOW_SMS,value);
        return (RegisterResponse) serverHelper.registerMemberWithoutConsent(fieldMap,initConfigRegistration);
    }

    protected static RegisterResponse getRegisterV1ResponseNoAllowEmailAllowSMS(String customerPhone, String value, boolean initConfigRegistration) {
        Map<String,String> fieldMap = new HashMap<>();
        fieldMap.put(Initializer.FIRST_NAME,Initializer.FIRST_NAME + "_" +customerPhone);
        fieldMap.put(Initializer.LAST_NAME,Initializer.LAST_NAME + "_" +customerPhone);
        fieldMap.put(Initializer.PHONE_NUMBER, customerPhone);
        return (RegisterResponse) serverHelper.registerMemberWithoutConsent(fieldMap,initConfigRegistration);
    }

    protected GetMemberDetailsResponse getMemberDetailsV2_8Response(String phoneNumber){
        server.v2_8.common.Customer customer = new server.v2_8.common.Customer();
        customer.setPhoneNumber(phoneNumber);
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.getCustomers().add(customer);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(false);

        //Send the request
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse)getMemberDetails.SendRequestByApiKeyAndValidateResponse(apiKey, GetMemberDetailsResponse.class);
        return getMemberDetailsResponse;
    }

    protected IServerResponse getMemberDetailsV2_8ResponseNoValidation(String phoneNumber){
        server.v2_8.common.Customer customer = new server.v2_8.common.Customer();
        customer.setPhoneNumber(phoneNumber);
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.getCustomers().add(customer);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(false);

        //Send the request
        IServerResponse getMemberDetailsResponse = getMemberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);
        return getMemberDetailsResponse;
    }

    protected Membership getMemberDetailsV4Response(String phoneNumber){

        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        Membership membership = v4Builder.getMember(customer);

        return membership;
    }

    protected IServerResponse getMemberDetailsV4ResponseNoMmembership(String phoneNumber){

        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        server.v4.GetMemberDetails memberDetails = v4Builder.buildMemberDetailsRequest(customer);
        IServerResponse response = memberDetails.SendRequestByApiKey(apiKey, server.v4.GetMemberDetailsResponse.class);

        return response;
    }

    protected SubmitPurchaseResponse getAutoRegisterResponse(String customerPhone) {

        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem = v4Builder.buildPurchaseItem(REG_ITEM_CODE);
        Purchase purchase = v4Builder.buildPurchase(uuid,100, Lists.newArrayList(purchaseItem)) ;
        purchase.setTags(Lists.newArrayList(REG_ITEM_TAG));
        SubmitPurchase submitPurchase = v4Builder.buildSubmitPurchase(customerPhone, purchase);

        IServerResponse response =   submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        if(!(((SubmitPurchaseResponse) response).getStatus().equals("ok")))
            Assert.fail(response.getRawObject().toString());

        return (SubmitPurchaseResponse)response;
    }





    protected GetBenefitsResponse getBenefitsV4Response(String customerPhone) throws Exception {
        //Create list of items
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem(BaseHub1Discount.ITEM_CODE_111,1,100,"depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem(BaseHub1Discount.ITEM_CODE_222,1,900, "depCode", "depName"));

        //Create purchase
        Purchase purchase = v4Builder.buildPurchase(UUID.randomUUID().toString(),1000,items);

        //Send request & get response
        IServerResponse getBenefitsResponse = GetBenefitsHelper.getInstance().getBenefits(apiKey,customerPhone, purchase, null, null);
        return (GetBenefitsResponse) getBenefitsResponse;
    }

    protected IServerResponse getBenefitsV4PostGraceResponse(String customerPhone) throws Exception {
        //Create list of items
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem(BaseHub1Discount.ITEM_CODE_111,1,100,"depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem(BaseHub1Discount.ITEM_CODE_222,1,900, "depCode", "depName"));

        //Create purchase
        Purchase purchase = v4Builder.buildPurchase(UUID.randomUUID().toString(),1000,items);

        //Send request & get response
        IServerResponse getBenefitsResponse = GetBenefitsHelper.getInstance().getBenefits(apiKey,customerPhone, purchase, null, null);
        return getBenefitsResponse;
    }


    protected GetMemberBenefitsResponse getBenefitsv2_8Response(String phone) throws Exception{

        Item item = v2_8Builder.buildItem(BaseHub1Discount.ITEM_CODE_111, "Prod1", "123", "Dep1", 100, 1);
        Item item2 = v2_8Builder.buildItem(BaseHub1Discount.ITEM_CODE_222, "Prod1", "123", "Dep1", 900, 3);
        IServerResponse response = getMemberBenefits(phone, "1000",item, item2);
        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail( ((ZappServerErrorResponse) response).getErrorMessage());
        }

        // Result true assertion
        GetMemberBenefitsResponse getMemberBenefitsResponse = (GetMemberBenefitsResponse) response;
        return getMemberBenefitsResponse;
    }

    protected IServerResponse getBenefitsv2_8PostGraceResponse(String phone) throws Exception{

        Item item = v2_8Builder.buildItem(BaseHub1Discount.ITEM_CODE_111, "Prod1", "123", "Dep1", 100, 1);
        Item item2 = v2_8Builder.buildItem(BaseHub1Discount.ITEM_CODE_222, "Prod1", "123", "Dep1", 900, 3);
        IServerResponse getMemberBenefitsResponse = getMemberBenefits(phone, "1000",item, item2);

        return getMemberBenefitsResponse;
    }


    private IServerResponse getMemberBenefits(String phoneNumber, String totalSum, Item item, Item item2) throws Exception {

        server.v2_8.common.Customer customer = v2_8Builder.buildCustomer(phoneNumber);
        GetMemberBenefits getMemberBenefits = v2_8Builder.buildGetMemberBenefits(customer,totalSum,item,item2);

        //Send the request
        IServerResponse response = getMemberBenefits.SendRequestByApiKey(apiKey, GetMemberBenefitsResponse.class);

        // Result true assertion
        return  response;

    }


    protected IServerResponse submitPurchaseV2_8PostGrace(String customerPhone){

        server.v2_8.SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(customerPhone);
        submitPurchase.setTotalSum("1000");

        //Send the request
        IServerResponse submitPurchaseResponse = submitPurchase.SendRequestByApiKey(apiKey, server.v2_8.SubmitPurchaseResponse.class);

        return submitPurchaseResponse;
    }

    protected server.v2_8.SubmitPurchaseResponse submitPurchaseV2_8(String customerPhone){

        server.v2_8.SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(customerPhone);
        submitPurchase.setTotalSum("1000");

        //Send the request
        server.v2_8.SubmitPurchaseResponse submitPurchaseResponse = (server.v2_8.SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, server.v2_8.SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());
        return submitPurchaseResponse;
    }

    protected SubmitPurchaseResponse submitPurchaseV4_0(String customerPhone){

        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,100, Lists.newArrayList()) ;
        SubmitPurchase submitPurchase = v4Builder.buildSubmitPurchase(customerPhone, purchase);
        IServerResponse response =  submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);

        return (SubmitPurchaseResponse) response;
    }

    protected IServerResponse submitPurchaseV4_0PostGrace(String customerPhone){

        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,100, Lists.newArrayList()) ;
        SubmitPurchase submitPurchase = v4Builder.buildSubmitPurchase(customerPhone, purchase);
        IServerResponse response =  submitPurchase.SendRequestByApiKey(apiKey,SubmitPurchaseResponse.class);

        return  response;
    }



    protected void checkMemberHasTheGiftBySmartAutomation(String customerPhone) throws Exception {

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(customerPhone);
        MembersService.findAndGetMember(findMember);
        Assert.assertTrue("Member did not recieve the gift from automation",MembersService.isGiftExists(smartGift.getTitle()));

    }

    protected void checkGMDResponse(GetMemberDetailsResponse gmdResponse,String registrationSource,String consent ) {
        Assert.assertEquals(gmdResponse.getMemberships().get(0).getRegistrationSource(),registrationSource);
        Assert.assertEquals(gmdResponse.getMemberships().get(0).getConsent(),consent);
        Assert.assertEquals(gmdResponse.getMemberships().get(0).getMembershipStatus(),REGISTERED);
    }

    protected void checkGMDV4Response( Membership membership,String phone) {
        Assert.assertEquals(membership.getPhoneNumber(),phone);
        Assert.assertEquals(membership.getStatus(), ACTIVE);
    }

    protected void updateUserAtLocationToMay072018 (String phoneNumber) throws Exception{

        Date monthBack = hubAssetsCreator.getMay012018Date();

        log4j.info("get user by phone : " + phoneNumber);

        log4j.info("check Data Store with consent data in UserAtLocation");
        List<Query.FilterPredicate> predicates = Arrays.asList(new Query.FilterPredicate("PhoneNumber", Query.FilterOperator.EQUAL,phoneNumber));

        List<Entity>result = serverHelper.queryWithWait("Must find User with the phoneNumber: " + phoneNumber, serverHelper.buildQuery("UserAtLocation",predicates), 1, dataStore);
        Entity e = result.get(0);

        //set created date to be month earlier - post grace period
        serverHelper.updateEntity(dataStore,e,"CreatedOn", monthBack);

    }

    protected void updateUserAtLocationToPostGracePeriod (String phoneNumber) throws Exception{

        Date monthBack = hubAssetsCreator.dateHoursBack(25);

        log4j.info("get user by phone : " + phoneNumber);

        log4j.info("check Data Store with consent data in UserAtLocation");
        List<Query.FilterPredicate> predicates = Arrays.asList(new Query.FilterPredicate("PhoneNumber", Query.FilterOperator.EQUAL,phoneNumber));

        List<Entity>result = serverHelper.queryWithWait("Must find User with the phoneNumber: " + phoneNumber, serverHelper.buildQuery("UserAtLocation",predicates), 1, dataStore);
        Entity e = result.get(0);

        //set created date to be month earlier - post grace period
        serverHelper.updateEntity(dataStore,e,"CreatedOn", monthBack);

    }

    protected String checkConsent(String phoneNumber,String expectedConsent) throws Exception {
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MemberDetails memberDetails = MembersService.findAndGetMember(findMember);
        Assert.assertEquals("Consent is not as expected" ,expectedConsent,memberDetails.getConsentForHub());
        return memberDetails.getUserKey();
    }

    protected void checkMemberProfileLogs(String phoneNumber,String pattern,String source) throws Exception{
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        String expectedMessage = MessageFormat.format(pattern,source);
        String log =  MembersService.checkUserLog(expectedMessage);
        Assert.assertEquals("User log is not as expected: " + log,log,expectedMessage);
    }


    protected void checkDataStoreWithConsentData(String uKey,String phoneNumber,String expectedConsent,String expectedRegistrationSource,String expectedConsentForHub) throws Exception{

        Key userKey = StringUtils.StringToKey(uKey);
        log4j.info("get user key : " + uKey);
        log4j.info("User phone : " + phoneNumber);

        log4j.info("check Data Store with consent data in UserAtLocation");
        List<Query.FilterPredicate> predicates = Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, userKey),
                new Query.FilterPredicate("PhoneNumber", Query.FilterOperator.EQUAL,phoneNumber));

        List<Entity> res = ServerHelper.queryWithWait("Must find User with the userKey: " + userKey,
                serverHelper.buildQuery("UserAtLocation",predicates) , 1, dataStore);

        checkData(expectedConsent, expectedRegistrationSource,expectedConsentForHub, res);
    }

    protected void checkDataStoreWithConsentDataByPhoneOnly(String phoneNumber,String expectedConsent,String expectedRegistrationSource) throws Exception{

        log4j.info("get user with phone : " + phoneNumber);

        log4j.info("check Data Store with consent data in UserAtLocation");
        List<Query.FilterPredicate> predicates = Arrays.asList(new Query.FilterPredicate("PhoneNumber", Query.FilterOperator.EQUAL,phoneNumber));

        List<Entity> res = ServerHelper.queryWithWait("Must find User with the phone: " + phoneNumber,
                serverHelper.buildQuery("UserAtLocation",predicates) , 1, dataStore);

        checkData(expectedConsent, expectedRegistrationSource,expectedConsent,res);
    }

    private void checkData(String expectedConsent, String expectedRegistrationSource,String expectedConsentForHub, List<Entity> res) {
        String consent = (String) res.get(0).getProperty("Consent");
        Assert.assertEquals("Consent should be <" + expectedConsent + ">" ,expectedConsent,consent);
        String registrationSource = (String) res.get(0).getProperty("RegistrationSource");
        Assert.assertEquals("RegistrationSource should be <" +expectedRegistrationSource + ">",expectedRegistrationSource,registrationSource);
        String consentForHub = (String) res.get(0).getProperty("ConsentForHub");
        Assert.assertEquals("ConsentStatus should be <" + expectedConsentForHub + ">",expectedConsentForHub,consentForHub);
    }


    protected void checkDataStoreWithConsentDataByPhoneOnlyEmailSMS(String phoneNumber,boolean expectedAllowSMS, boolean  expectedAllowEmail) throws Exception{

        log4j.info("get user with phone : " + phoneNumber);

        log4j.info("check Data Store with consent data in UserAtLocation");
        List<Query.FilterPredicate> predicates = Arrays.asList(new Query.FilterPredicate("PhoneNumber", Query.FilterOperator.EQUAL,phoneNumber));

        List<Entity> res = ServerHelper.queryWithWait("Must find User with the phone: " + phoneNumber,
                serverHelper.buildQuery("UserAtLocation",predicates) , 1, dataStore);

        checkDataAllowEmailSMS(expectedAllowEmail, expectedAllowSMS, res);
    }


    protected void checkDataStoreWithConsentDataByPhoneOnlyEmailSMSNoResults(String phoneNumber) throws Exception{

        log4j.info("get user with phone : " + phoneNumber);

        log4j.info("check Data Store with consent data in UserAtLocation");
        List<Query.FilterPredicate> predicates = Arrays.asList(new Query.FilterPredicate("PhoneNumber", Query.FilterOperator.EQUAL,phoneNumber));

        List<Entity> res = ServerHelper.queryWithWait("Must find User with the phone: " + phoneNumber,
                serverHelper.buildQuery("UserAtLocation",predicates) , 0, dataStore);

        checkDataAllowEmailSMS(res);

    }


    protected void checkDataStoreWithAllowSMSandEmail(String uKey,String phoneNumber,boolean expectedAllowSMS, boolean  expectedAllowEmail) throws Exception{

        Key userKey = StringUtils.StringToKey(uKey);
        log4j.info("get user key : " + uKey);
        log4j.info("User phone : " + phoneNumber);

        log4j.info("check Data Store with consent data in UserAtLocation");
        List<Query.FilterPredicate> predicates = Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, userKey),
                new Query.FilterPredicate("PhoneNumber", Query.FilterOperator.EQUAL,phoneNumber));

        List<Entity> res = ServerHelper.queryWithWait("Must find User with the userKey: " + userKey,
                serverHelper.buildQuery("UserAtLocation",predicates) , 1, dataStore);

        checkDataAllowEmailSMS(expectedAllowEmail, expectedAllowSMS, res);
    }

    private void checkDataAllowEmailSMS(boolean expectedAllowEmail, boolean expectedAllowSMS, List<Entity> res) {
        try {
            boolean allowEmail = (Boolean) res.get(0).getProperty("AllowEmail");
            Assert.assertEquals("AllowEmail should be <" + expectedAllowEmail + ">", expectedAllowEmail, allowEmail);
            boolean allowSMS = (Boolean) res.get(0).getProperty("AllowSMS");
            Assert.assertEquals("AllowSMS should be <" + expectedAllowSMS + ">", expectedAllowSMS, allowSMS);
        }catch (Exception e) {

            log4j.error("Should not be null");
        }

    }

    protected void checkAllowSMSAllowEmailNone (String customerPhone) throws Exception{

        MembersService.findAndGetMember(customerPhone);
        Assert.assertEquals("", "None",MembersService.getMemberAllowSMS());
        Assert.assertEquals("", "None",MembersService.getMemberAllowEmail());


    }

    protected void checkAllowSMSAllowEmailYes (String customerPhone) throws Exception {

        MembersService.findAndGetMember(customerPhone);
        Assert.assertEquals("", "Yes",MembersService.getMemberAllowSMS());
        Assert.assertEquals("", "Yes",MembersService.getMemberAllowEmail());

        String log =  MembersService.checkUserLog(SUBSCRIBED_EMAIL);
        Assert.assertEquals("User log is not as expected: " + log,log,SUBSCRIBED_EMAIL);

        log =  MembersService.checkUserLog(SUBSCRIBED_SMS);
        Assert.assertEquals("User log is not as expected: " + log,log,SUBSCRIBED_SMS);
    }

    protected void checkAllowSMSAllowEmailNo (String customerPhone) throws Exception{

        MembersService.findAndGetMember(customerPhone);
        Assert.assertEquals("", "No",MembersService.getMemberAllowSMS());
        Assert.assertEquals("", "No",MembersService.getMemberAllowEmail());

        String log =  MembersService.checkUserLog(UNSUBSCRIBED_EMAIL);
        Assert.assertEquals("User log is not as expected: " + log,log,UNSUBSCRIBED_EMAIL);

        log =  MembersService.checkUserLog(UNSUBSCRIBED_SMS);
        Assert.assertEquals("User log is not as expected: " + log,log,UNSUBSCRIBED_SMS);

    }

    private void checkDataAllowEmailSMS(List<Entity> res) {

            boolean allowEmail = (Boolean) res.get(0).getProperty("AllowEmail");

            boolean allowSMS = (Boolean) res.get(0).getProperty("AllowSMS");


    }


    public enum ConsentStatus{
        Yes("yes"),
        No("no"),
        Previous("previous"),
        Import_yes("import_yes"),
        Existing("existing"),
        Import_no("import_no");


        protected final String status;
        ConsentStatus(final String name) {
            this.status = name;
        }

    }

    public enum RegistrationSource{
        Hub("Hub"),
        App("App"),
        POS("Api"),
        AutoRegistration("AutoRegistration"),
        Import("Import"),
        LandingPage("LandingPage");


        protected final String source;
        RegistrationSource(final String name) {
            this.source = name;
        }

    }

}
