package integration.consent;

import org.junit.Assert;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import server.v2_8.GetMemberBenefitsResponse;
import server.v2_8.GetMemberDetailsResponse;
import server.v4.GetBenefitsResponse;
import server.v4.common.models.Membership;

public class ConsentGracePeriodTest extends BaseConsentTest {

/********************************************AutoRegisteration**********************/

    @Test
    @Description("Member with grace (In grace period) - Auto-register GMD")
    public void testMemberWithGraceAutoRegisterationGMD() throws Exception {

        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        getAutoRegisterResponse(customerPhone1);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        getAutoRegisterResponse(customerPhone2);

        GetMemberDetailsResponse getMemberDetailsResponse1 = getMemberDetailsV2_8Response(customerPhone1);
        checkGMDResponse(getMemberDetailsResponse1,RegistrationSource.AutoRegistration.source,ConsentStatus.No.status);

        Membership membership2 = getMemberDetailsV4Response(customerPhone2);
        checkGMDV4Response(membership2,customerPhone2);

    }

    @Test
    @Description("Member with grace (In grace period) - Auto-register GetBenefits")
    public void testMemberWithGraceAutoRegisterationGetBenefits() throws Exception {

        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        getAutoRegisterResponse(customerPhone1);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        getAutoRegisterResponse(customerPhone2);



        GetMemberBenefitsResponse getMemberBenefitsResponse = getBenefitsv2_8Response(customerPhone1);
        Assert.assertEquals("-100",getMemberBenefitsResponse.getTotalSumDiscount());

        GetBenefitsResponse getBenefitsResponse = getBenefitsV4Response(customerPhone2);
        Assert.assertEquals(Integer.valueOf(-100),getBenefitsResponse.getTotalDiscountSum());


    }

    @Test
    @Description("Member with grace (In grace period) - Auto-register SubmitPurchaseSmartAutomation")
    public void testMemberWithGraceAutoRegisterationSubmitPurchaseSmartAutomation() throws Exception {

        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        getAutoRegisterResponse(customerPhone1);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        getAutoRegisterResponse(customerPhone2);

        submitPurchaseV2_8(customerPhone1);
        checkMemberHasTheGiftBySmartAutomation(customerPhone1);

        submitPurchaseV4_0(customerPhone2);

        checkMemberHasTheGiftBySmartAutomation(customerPhone2);
    }

  /********************************RegisterationV1**************************/


    @Test
    @Description("Member with grace (In grace period) - API V1 (old) - GMD")
    public void testMemberWithGraceRegisterationV1GMD() throws Exception{

        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        getRegisterV1Response(customerPhone1);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        getRegisterV1Response(customerPhone2);

        GetMemberDetailsResponse getMemberDetailsResponse1 = getMemberDetailsV2_8Response(customerPhone1);
        checkGMDResponse(getMemberDetailsResponse1,RegistrationSource.POS.source,ConsentStatus.No.status);

        Membership membership2 = getMemberDetailsV4Response(customerPhone2);
        checkGMDV4Response(membership2,customerPhone2);

    }

    @Test
    @Description("Member with grace (In grace period) - API V1 (old) - GetBenefits")
    public void testMemberWithGraceRegisterationV1GetBenefits() throws Exception{

        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        getRegisterV1Response(customerPhone1);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        getRegisterV1Response(customerPhone2);

        GetMemberBenefitsResponse getMemberBenefitsResponse = getBenefitsv2_8Response(customerPhone1);
        Assert.assertEquals("-100",getMemberBenefitsResponse.getTotalSumDiscount());

        GetBenefitsResponse getBenefitsResponse = getBenefitsV4Response(customerPhone2);
        Assert.assertEquals(Integer.valueOf(-100),getBenefitsResponse.getTotalDiscountSum());

    }

    @Test
    @Description("Member with grace (In grace period) - API V1 (old) - SubmitPurchaseSmartAutomation")
    public void testMemberWithGraceRegisterationV1SubmitPurchaseSmartAutomation() throws Exception{

        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        getRegisterV1Response(customerPhone1);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        getRegisterV1Response(customerPhone2);


        submitPurchaseV2_8(customerPhone1);
        checkMemberHasTheGiftBySmartAutomation(customerPhone1);

        submitPurchaseV4_0(customerPhone2);
        checkMemberHasTheGiftBySmartAutomation(customerPhone2);

    }


    /***********************************RegisterationV4*****************************************/
    @Test
    @Description("Member with grace (In grace period) - API V4 (new) - GMD")
    public void testMemberWithGraceRegisterationV4GMD() throws Exception {
        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone1,true);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone2,true);

        GetMemberDetailsResponse getMemberDetailsResponse1 = getMemberDetailsV2_8Response(customerPhone1);
        checkGMDResponse(getMemberDetailsResponse1,RegistrationSource.POS.source,ConsentStatus.No.status);

        Membership membership2 = getMemberDetailsV4Response(customerPhone2);
        checkGMDV4Response(membership2,customerPhone2);

    }

    @Test
    @Description("Member with grace (In grace period) - API V4 (new) -GetBenefits ")
    public void testMemberWithGraceRegisterationV4GetBenefits() throws Exception {
        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone1,true);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone2,true);

        GetMemberBenefitsResponse getMemberBenefitsResponse = getBenefitsv2_8Response(customerPhone1);
        Assert.assertEquals("-100",getMemberBenefitsResponse.getTotalSumDiscount());

        GetBenefitsResponse getBenefitsResponse = getBenefitsV4Response(customerPhone2);
        Assert.assertEquals(Integer.valueOf(-100),getBenefitsResponse.getTotalDiscountSum());

    }

    @Test
    @Description("Member with grace (In grace period) - API V4 (new)  - SubmitPurchaseSmartAutomation")
    public void testMemberWithGraceRegisterationV4SubmitPurchaseSmartAutomation() throws Exception {
        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone1,true);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        serverHelper.registerMemberV4WithoutConsent(customerPhone2,true);

        submitPurchaseV2_8(customerPhone1);
        checkMemberHasTheGiftBySmartAutomation(customerPhone1);

        submitPurchaseV4_0(customerPhone2);
        checkMemberHasTheGiftBySmartAutomation(customerPhone2);

    }

    /***********************************RegisterationFromHub*************************************************/

    @Test
    @Description("Member with grace (In grace period) - Hub - GMD")
    public void testMemberWithGraceRegisterationFromHubGMD() throws Exception {
        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        //Create member
        hubMemberCreator.createMemberDefaultConsent(customerPhone1, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        //Create member
        hubMemberCreator.createMemberDefaultConsent(customerPhone2, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        GetMemberDetailsResponse getMemberDetailsResponse1 = getMemberDetailsV2_8Response(customerPhone1);
        checkGMDResponse(getMemberDetailsResponse1,RegistrationSource.Hub.source,ConsentStatus.No.status);

        Membership membership2 = getMemberDetailsV4Response(customerPhone2);
        checkGMDV4Response(membership2,customerPhone2);

    }

    @Test
    @Description("Member with grace (In grace period) - Hub - GetBenefits")
    public void testMemberWithGraceRegisterationFromHubGetBenefits() throws Exception {
        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        //Create member
        hubMemberCreator.createMemberDefaultConsent(customerPhone1, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        //Create member
        hubMemberCreator.createMemberDefaultConsent(customerPhone2, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        GetMemberBenefitsResponse getMemberBenefitsResponse = getBenefitsv2_8Response(customerPhone1);
        Assert.assertEquals("-100",getMemberBenefitsResponse.getTotalSumDiscount());

        GetBenefitsResponse getBenefitsResponse = getBenefitsV4Response(customerPhone2);
        Assert.assertEquals(Integer.valueOf(-100),getBenefitsResponse.getTotalDiscountSum());

    }

    @Test
    @Description("Member with grace (In grace period) - Hub - SubmitPurchaseSmartAutomation")
    public void testMemberWithGraceRegisterationFromHubSubmitPurchaseSmartAutomation() throws Exception {
        String customerPhone1 = String.valueOf(System.currentTimeMillis());
        //Create member
        hubMemberCreator.createMemberDefaultConsent(customerPhone1, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        String customerPhone2 = String.valueOf(System.currentTimeMillis());
        //Create member
        hubMemberCreator.createMemberDefaultConsent(customerPhone2, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        submitPurchaseV2_8(customerPhone1);
        checkMemberHasTheGiftBySmartAutomation(customerPhone1);

        submitPurchaseV4_0(customerPhone2);
        checkMemberHasTheGiftBySmartAutomation(customerPhone2);

    }



}
