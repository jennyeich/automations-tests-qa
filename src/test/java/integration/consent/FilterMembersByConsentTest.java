package integration.consent;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.basePages.BaseDataAndBIPage;
import hub.common.objects.common.DateAndTimeInFilter;
import hub.common.objects.common.FilterActionTypes;
import hub.common.objects.common.FilterMembersChooseDates;
import hub.common.objects.common.FilterMembersSource;
import hub.common.objects.member.FilterMember;
import hub.common.objects.member.MemberDetails;
import hub.hub1_0.services.Navigator;
import hub.services.member.MembersService;
import org.junit.*;
import org.junit.rules.TestName;
import org.openqa.selenium.WebElement;
import server.common.IServerResponse;
import server.utils.UserActionsConstants;
import server.utils.V4Builder;
import server.v2_8.Consent;
import server.v2_8.JoinClubResponse;
import server.v2_8.RegisterResponse;
import server.v4.RegisterMemberV4Response;
import server.v4.SubmitPurchase;
import server.v4.SubmitPurchaseResponse;
import server.v4.common.models.Purchase;
import server.v4.common.models.PurchaseItem;
import utils.AutomationException;
import utils.EnvProperties;
import utils.PropsReader;

import java.io.*;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import static hub.base.BaseService.getDriver;

public class FilterMembersByConsentTest extends BaseConsentTest {


    @Rule
    public TestName name = new TestName();

    public static final String TEST_FILE_NAME = "FilterMembersTestData.txt";
    private static final String IMPORT_500_FILENAME = "500Import.csv";


    @Before
    public  void createStatment() throws Exception {
        sqlDB.createStatment();
    }

    @BeforeClass
    public static void setup() throws Exception {

        if(Boolean.valueOf(PropsReader.getPropValuesForEnv(Consent.REGISTER_MEMBERS_BY_CONSENT)).booleanValue()) {
            init();
            createMembersFromDifferentSources();

            File timeFile = new File(TEST_FILE_NAME);
            if (timeFile.exists()) {
                timeFile.delete();
            }
        }

    }

    private static void createMembersFromDifferentSources() throws Exception{

        String autoRegisterCustomerPhone = String.valueOf(System.currentTimeMillis());
        autoRegisterMember(autoRegisterCustomerPhone);
        log4j.info("create member from auto register(submit purchase): " + autoRegisterCustomerPhone);

        Thread.sleep(1000);
        String registerV1CustomerPhone = String.valueOf(System.currentTimeMillis());
        RegisterResponse registerResponse = getRegisterV1Response(registerV1CustomerPhone);
        log4j.info("create member from api v1: " + registerV1CustomerPhone);

        Thread.sleep(1000);
        String registerHubCustomerPhone = String.valueOf(System.currentTimeMillis());
        hubMemberCreator.createMemberDefaultConsent(registerHubCustomerPhone, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        log4j.info("create member from Hub: " + registerHubCustomerPhone);

        Thread.sleep(1000);
        String registerV4CustomerPhone = String.valueOf(System.currentTimeMillis());
        RegisterMemberV4Response registerMemberV4Response = serverHelper.registerMemberV4WithoutConsent(registerV4CustomerPhone,true);
        log4j.info("create member from api v4: " + registerV4CustomerPhone);

        Thread.sleep(1000);
        JoinClubResponse joinClubResponse = serverHelper.createMember();
        log4j.info("create member from app: " + joinClubResponse.getPhoneNumber());
    }

    private static void autoRegisterMember(String autoRegisterCustomerPhone) {
        V4Builder v4Builder = new V4Builder(EnvProperties.getInstance().getApiKey());
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem = v4Builder.buildPurchaseItem(REG_ITEM_CODE);
        Purchase purchase = v4Builder.buildPurchase(uuid,100, Lists.newArrayList(purchaseItem)) ;
        purchase.setTags(Lists.newArrayList(REG_ITEM_TAG));
        SubmitPurchase submitPurchase = v4Builder.buildSubmitPurchase(autoRegisterCustomerPhone, purchase);

        IServerResponse response =   submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        if(!(((SubmitPurchaseResponse) response).getStatus().equals("ok")))
            Assert.fail(response.getRawObject().toString());

    }

    @Test
    public void testFilterMembersJoinedTheClubFromSourcePOS() throws Exception {


        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setSource(FilterMembersSource.POS.source);
        setDateFilterDayAgo(filterMember);

        startFilterByBQ(filterMember);
        assertNoErrors();
        checkQuery(UserActionsConstants.JOINED_CLUB,FilterMembersSource.POS.toString());
    }

    @Test
    public void testFilterMembersJoinedTheClubFromSourceApp() throws Exception {


        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setSource(FilterMembersSource.App.source);
        setDateFilterDayAgo(filterMember);

        startFilterByBQ(filterMember);
        assertNoErrors();
        checkQuery(UserActionsConstants.JOINED_CLUB,FilterMembersSource.App.toString());
    }

    @Test
    public void testFilterMembersJoinedTheClubFromSourceAutomation() throws Exception {


        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setSource(FilterMembersSource.Automation.source);
        setDateFilterDayAgo(filterMember);

        startFilterByBQ(filterMember);
        assertNoErrors();
        checkQuery(UserActionsConstants.JOINED_CLUB,FilterMembersSource.Automation.toString());

    }

    @Test
    public void testFilterMembersJoinedTheClubFromSourceHub() throws Exception {


        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setSource(FilterMembersSource.Hub.source);
        setDateFilterDayAgo(filterMember);

        startFilterByBQ(filterMember);
        assertNoErrors();
        checkQuery(UserActionsConstants.JOINED_CLUB,FilterMembersSource.Hub.toString());

    }

    @Test
    public void testFilterMembersJoinedTheClubFromSourceImport() throws Exception {


        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setSource(FilterMembersSource.Import.source);
        setDateFilterDayAgo(filterMember);

        startFilterByBQ(filterMember);
        assertNoErrors();
        checkQuery(UserActionsConstants.JOINED_CLUB,FilterMembersSource.Import.toString());

    }

    @Test
    public void testFilterMembersMemberAcceptedTAndC_CheckNO() throws Exception {

        Navigator.refresh();
        FilterMember filterMember = new FilterMember();
        filterMember.checkNoConsent();

        startFilterByBQ(filterMember);
        assertNoErrors();
        MemberDetails memberDetails = MembersService.clickOnFoundMember();
        Assert.assertEquals("Consent is not as expected" ,ConsentStatus.No.toString(),memberDetails.getConsentForHub());

    }

    @Test
    public void testFilterMembersMemberAcceptedTAndC_CheckYes() throws Exception {

        Navigator.refresh();
        FilterMember filterMember = new FilterMember();
        filterMember.checkHasConsent();

        startFilterByBQ(filterMember);
        assertNoErrors();
        MemberDetails memberDetails = MembersService.clickOnFoundMember();
        Assert.assertEquals("Consent is not as expected" ,ConsentStatus.Yes.toString(),memberDetails.getConsentForHub());

    }

    @Test
    public void testFilterMembersMemberAcceptedTAndC_CheckPrevious() throws Exception {

        Navigator.refresh();
        FilterMember filterMember = new FilterMember();
        filterMember.checkPreviousConsent();

        startFilterByBQ(filterMember);
        assertNoErrors();
        MemberDetails memberDetails = MembersService.clickOnFoundMember();
        Assert.assertEquals("Consent is not as expected" ,"Previous business approval",memberDetails.getConsentForHub());

    }

    @Test
    public void testFilterMembersGaveConsentFromSourceApp() throws Exception {


        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.CONSENTED_TC.toString());
        filterMember.setSource(FilterMembersSource.App.source);
        setDateFilterDayAgo(filterMember);

        startFilterByBQ(filterMember);
        assertNoErrors();
        checkQuery(UserActionsConstants.GAVE_CONSENT,FilterMembersSource.App.toString());
    }

    @Test
    public void testFilterMembersGaveConsentFromSourceLandingPage() throws Exception {


        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.CONSENTED_TC.toString());
        filterMember.setSource(FilterMembersSource.LandingPage.source);
        setDateFilterDayAgo(filterMember);

        startFilterByBQ(filterMember);
        assertNoErrors();
        checkQuery(UserActionsConstants.GAVE_CONSENT,FilterMembersSource.LandingPage.toString());
    }

    private void setDateFilterDayAgo(FilterMember filterMember) {
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar cal1DayAgo  = Calendar.getInstance();
        cal1DayAgo.add(Calendar.DAY_OF_WEEK, -1);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(hubMemberCreator.getDate(cal1DayAgo));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);
    }

    private void checkQuery(String actionName,String source) throws SQLException, ClassNotFoundException {
        String query = getQueryAndValidate(locationId);
        assertQueryContains(query, "action = '" + actionName  + "'");
        assertQueryContains(query,  "source ='" + source  + "'");
    }


    private void assertQueryContains(String query, String value){
        Assert.assertTrue("Query must contain the value: " + value, query.contains(value));
    }


    private void assertNoErrors() throws Exception{

        List<WebElement> notes = getDriver().findElements(BaseDataAndBIPage.FILTER_ERROR);
        int size = notes.size();
        Assert.assertTrue("Filter error is shown", size==0);
    }

    private void startFilterByBQ(FilterMember filterMember) throws InterruptedException, AutomationException {

        //Navigator.refresh();
        MembersService.filterMemberByBigQuery(filterMember);
        int numOfMembersBQ = getNumOfMembers("BQ");
    }
    private int getNumOfMembers(String filterType){
        long startTime = System.currentTimeMillis();
        String numOfMembers = MembersService.numberOfMembers();
        long timeForFilter = System.currentTimeMillis()-startTime;
        writeFilterTimeAndNumOfMembersToFile(numOfMembers, timeForFilter, filterType);
        return Integer.valueOf(numOfMembers).intValue();
    }



    private void writeFilterTimeAndNumOfMembersToFile(String numOfMembers, long timeForFilter, String filterType){
        log4j.info("Will write to file:");
        log4j.info("Number of members - "  + filterType + " "  +numOfMembers);
        log4j.info("Time for filter "+ filterType + " " +timeForFilter);
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(TEST_FILE_NAME, true), "utf-8"))) {
            writer.write(name.getMethodName() + '\n');
            writer.write("Number of members - " + filterType + " " + numOfMembers + '\n');
            writer.write("Time for filter "  + filterType + " " + timeForFilter + '\n'+ '\n');
        } catch (Exception e) {
            log4j.error("error while creating file",e);
        }
    }

    private String getQueryAndValidate(String locationId) throws SQLException, ClassNotFoundException {
        String query = sqlDB.getLastBigQueryForBusiness(locationId);
        log4j.info("The sql query:" + query);
        Assert.assertTrue("Location must contain the location id ", query.contains("location_id = '" + locationId + "'"));

        return query;
    }


}
