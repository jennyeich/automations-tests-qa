package hub.webTests.regression;

import hub.common.objects.member.FindMember;
import hub.services.member.MembersService;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Map;

/**
 * Created by Jenny on 18/07/2018.
 */
public class FriendReferralCodeTest extends BaseIntegrationTest {


    private static CharSequence timestamp;
    private final String MEMBERSHIP_KEY = "membershipKey";
    private final String USER_TOKEN = "userToken";


    @BeforeClass
    public static void setup() throws Exception {
        init();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());

        }
    };

    @Test
    public void testCodeIsNotValidWithDeletedMember() throws Exception {


        Map map = serverHelper.createAndValidateCode();
        String advocateMembership = map.get(MEMBERSHIP_KEY).toString();

        FindMember findMember = new FindMember();
        findMember.setMembershipKey(advocateMembership);
        MembersService.findMember(findMember);
        MembersService.clickMember();
        MembersService.deleteExistingMember ();

        String code = map.get(CODE).toString();

        String phoneNumber = String.valueOf(System.currentTimeMillis());
        String userToken = serverHelper.signON ();


        try {
           String membershipKey =  serverHelper.joinClubWithUserTokenAndReferralCode(phoneNumber,userToken,"App",code);
           log4j.info("Member with MembershipKey was created without the code: " + membershipKey);
           serverHelper.checkDataStoreUpdatedMembersRelation(advocateMembership,membershipKey,0,dataStore);
        }catch (Exception e) {

           log4j.info("Member with code from deleted member was not created");
        }

        String userToken1 = map.get(USER_TOKEN).toString();
        String validate = serverHelper.validateReferralCode(userToken1,code);
        Assert.assertEquals("The token is not NOT_FOUND: " ,"NOT_FOUND", validate);


    }


    @Test
    public void testFriendReferralCodeFromMemberProfile() throws Exception {


        Map map = serverHelper.createAndValidateCode();
        String advocateMembership = map.get(MEMBERSHIP_KEY).toString();

        FindMember findMember = new FindMember();
        findMember.setMembershipKey(advocateMembership);
        MembersService.findMember(findMember);
        MembersService.clickMember();
        String referralCode = MembersService.getMemberFriendReferralCode();

        String code = map.get(CODE).toString();
        Assert.assertEquals("The code is not as expected: " ,referralCode, code);


    }

    @Test
    public void testFriendReferralCodeFromMemberProfileWhenTheCodeIsEmpty() throws Exception {


        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String referralCode = MembersService.getMemberFriendReferralCode();
        Assert.assertEquals("The code is not as expected: " ,"", referralCode);


    }



}
