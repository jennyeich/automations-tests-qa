package hub.webTests.regression;

import hub.common.objects.common.RegistrationField;
import hub.common.objects.common.YesNoListNumber;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.MemberDetails;
import hub.hub1_0.services.OperationService;
import hub.services.NewApplicationService;
import hub.services.member.MembersService;
import integration.base.BaseIntegrationTest;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import server.common.IServerResponse;
import server.utils.Initializer;
import server.utils.ServerHelper;
import server.v2_8.RegisterResponse;
import server.v2_8.base.ServicesErrorResponse;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class RegistrationFormTest extends BaseIntegrationTest {

    public static final String PHONE_NUMBER = "";// will be set later by timestamp
    public static final String ERROR_CODE = "4000000";
    public static final String REQUIRED_ERROR_MSG = "One of the required field is missing or empty";

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }


    @Test
    public void testCreateRegistrationFormRequiredField() throws Exception {
        Map <String, Object> fieldsHashMap = new HashMap<>();
        String timestamp = String.valueOf(System.currentTimeMillis());
        log4j.info("member phone number: " + timestamp);
        OperationService.addRequiredFieldsToRegistrationForm(Arrays.asList(RegistrationField.zipCode), YesNoListNumber.YES.toString());
        log4j.info("try to Add member via API call with the missing required field - expect it to failed");
        fieldsHashMap.put(Initializer.PHONE_NUMBER, PHONE_NUMBER + timestamp);
        fieldsHashMap.put(Initializer.FIRST_NAME, AUTO_USER_PREFIX + timestamp);
        fieldsHashMap.put(Initializer.LAST_NAME, AUTO_LAST_PREFIX  + timestamp);
        fieldsHashMap.put(Initializer.EMAIL, NewApplicationService.HUB_USER);

        IServerResponse response = serverHelper.getRegisterMemberV4Response(fieldsHashMap);
        //Make sure the API call failed with status code 400
        String errorMessage = serverHelper.getError(response).get(MESSAGE).toString();
        assertEquals("Verify HTTP response Status code:" , httpStatusCode400, response.getStatusCode());
        assertEquals( "Verify HTTP error type:", ERROR_CODE,serverHelper.getError(response).get(CODE).toString());
        assertEquals( "Verify HTTP error message:", REQUIRED_ERROR_MSG,errorMessage);
        log4j.info("try to Add member via API including the required field  - expect it to pass");
        fieldsHashMap.put(Initializer.ZIP_CODE, timestamp);
        response = serverHelper.getRegisterMemberV4Response(fieldsHashMap);
        if (response instanceof ServicesErrorResponse)
            Assert.fail("API call expected to return Non error response");

        assertEquals("Verify HTTP response Status code:" , httpStatusCode200, response.getStatusCode());


        // verify that the new member indeed registered
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(timestamp);
        MemberDetails memberDetails  = MembersService.findAndGetMember(findMember);
        log4j.info("check if the new:" + memberDetails.getFirstName() +  " member exists.");
        assertEquals("verify that new member added",AUTO_USER_PREFIX + timestamp,memberDetails.getFirstName());
    }

    @AfterClass
    public static void removeRequiredFields() throws Exception{
        OperationService.removeFieldsFromRegistrationForm(Arrays.asList(RegistrationField.zipCode));
    }

}
