package hub.webTests.regression;

import hub.common.objects.common.RegistrationField;
import hub.common.objects.common.YesNoListNumber;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.MemberDetails;
import hub.hub1_0.services.OperationService;
import hub.services.AcpService;
import hub.services.member.MembersService;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class UnsubscribeTest extends BaseIntegrationTest {

    public static final String ALLOW_EMAIL_TRUE = "Yes";
    public static final String ALLOW_EMAIL_FALSE = "No";
    public static final String ALLOW_EMAIL_NONE = "None";
    public static final String ALLOW_SMS_TRUE = "Yes";
    public static final String ALLOW_SMS_FALSE = "No";
    public static final String ALLOW_SMS_NONE = "None";
    public static final boolean UNSUBSCRIBE_SMS_ENABLED = true;
    public static final boolean UNSUBSCRIBE_SMS_DISABLED = false;
    public static final boolean UNSUBSCRIBE_EMAIL_ENABLED = true;
    public static final boolean UNSUBSCRIBE_EMAIL_DISABLED = false;

    @BeforeClass
    public static void setup() throws IOException {
        init();
    }

    @Test
    public void testImportExportMembers() throws Exception{
        OperationService.removeFieldsFromRegistrationForm(Arrays.asList(RegistrationField.AllowSMS, RegistrationField.AllowEmail));
        AcpService.importMembers(IMPORT_FOLDER + File.separator + "testImportWOallowFields.csv");
        OperationService.addRequiredFieldsToRegistrationForm(Arrays.asList(RegistrationField.AllowSMS, RegistrationField.AllowEmail), YesNoListNumber.NO.toString(), YesNoListNumber.NO.toString());
        AcpService.importMembers(IMPORT_FOLDER + File.separator + "testImportWithAllowFields.csv");
        Thread.sleep(60000);
        //Check creating new members when allow fields are true OR false
        checkAllowEmail("testUnsubscribe1", "test", ALLOW_SMS_NONE, ALLOW_EMAIL_NONE, UNSUBSCRIBE_SMS_ENABLED, UNSUBSCRIBE_EMAIL_ENABLED);
        checkAllowEmail("testUnsubscribe2", "test", ALLOW_SMS_TRUE, ALLOW_EMAIL_TRUE, UNSUBSCRIBE_SMS_ENABLED, UNSUBSCRIBE_EMAIL_ENABLED);
        checkAllowEmail("testUnsubscribe4", "test", ALLOW_SMS_FALSE, ALLOW_EMAIL_FALSE, UNSUBSCRIBE_SMS_DISABLED, UNSUBSCRIBE_EMAIL_DISABLED);
//        // Check create with empty allow email and allow sms. (server doesnt know the default value in registration form, so value will be null)
        checkAllowEmail("testUnsubscribe6", "test", ALLOW_EMAIL_NONE, ALLOW_EMAIL_NONE, UNSUBSCRIBE_SMS_ENABLED, UNSUBSCRIBE_EMAIL_ENABLED);

        //Try update allow fields.
        AcpService.importMembers(IMPORT_FOLDER + File.separator + "testUpdateAllowFields.csv");
        Thread.sleep(60000);
        checkAllowEmail("testUnsubscribe2", "test_update", ALLOW_SMS_FALSE, ALLOW_EMAIL_TRUE, UNSUBSCRIBE_SMS_DISABLED, UNSUBSCRIBE_EMAIL_ENABLED);
        checkAllowEmail("testUnsubscribe3", "test_update", ALLOW_SMS_TRUE, ALLOW_EMAIL_FALSE, UNSUBSCRIBE_SMS_ENABLED, UNSUBSCRIBE_EMAIL_DISABLED);
        checkAllowEmail("testUnsubscribe4", "test", ALLOW_SMS_FALSE, ALLOW_EMAIL_FALSE, UNSUBSCRIBE_SMS_DISABLED, UNSUBSCRIBE_EMAIL_DISABLED);
        checkAllowEmail("testUnsubscribe5", "test", ALLOW_SMS_FALSE, ALLOW_EMAIL_FALSE, UNSUBSCRIBE_SMS_DISABLED, UNSUBSCRIBE_EMAIL_DISABLED);
    }

    private void checkAllowEmail(String firstName, String lastName, String isAllowSMSChecked, String isAllowEmailChecked, boolean isUnsubscribeSMSEnabled, boolean isUnsubscribeEmailEnabled  ) throws Exception {
        FindMember findMember = new FindMember();
        findMember.setFirstName(firstName);
        findMember.setLastname(lastName);
        MemberDetails memberDetails = MembersService.findAndGetMember(findMember);
        Assert.assertTrue(String.format("AllowSMS checkbox expected to be %s, but actually was %s. ", isAllowSMSChecked, memberDetails.getAllowSMS()), isAllowSMSChecked.equals(memberDetails.getAllowSMS()));
        Assert.assertTrue(String.format("AllowEmail checkbox expected to be %s, but actually was %s. ", isAllowEmailChecked, memberDetails.getAllowEmail()), isAllowEmailChecked.equals(memberDetails.getAllowEmail()));
        Assert.assertTrue("Unsubscribe sms button state is not as expected.", isUnsubscribeSMSEnabled == memberDetails.isUnsubscribeSMSEnabled());
        Assert.assertTrue("Unsubscribe email button state is not as expected.", isUnsubscribeEmailEnabled == memberDetails.isUnsubscribeEmailEnabled());
    }
}
