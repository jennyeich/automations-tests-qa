package hub.webTests.regression;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.common.objects.common.ActionsToPerform;
import hub.common.objects.common.Gender;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.common.objects.oldObjects.*;
import hub.common.objects.smarts.smarObjects.triggers.OldAutomationTriggers;
import hub.services.automations.OldAutomationService;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import hub.utils.HubMemberCreator;
import hub.utils.HubOldAutomationCreator;
import integration.base.BaseIntegrationTest;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.SubmitPurchase;
import server.v2_8.common.Customer;
import server.v2_8.common.Item;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Goni on 6/7/2017.
 */
public class OldAutomationsTest extends BaseIntegrationTest {

    public static CharSequence timestamp;

    public static final Integer STATUS_CODE_OK = 200;
    public static final String INACTIVE = "inactive";

    public static final String MOCK_EMAIL = "mockemail@coco.com";
    public static final String MAIL_SUFFIX = "@como.com";
    public static final String MEMBER_TAG_VIP= "VIP";
    public static final String MEMBER_TAG_ANYUSER= "ANY_USER";
    public static final boolean AND = false;
    public static final boolean OR = true;
    public static final String EMPLOYEE = "Employee";
    public static final String BRANCH_ID = "1";
    public static final String DEPARTMENT_CODE ="123";
    public static final String ITEM_NAME_PREFIX = "item_";

    //UserActions action name
    public static final String TAG_OPERATION = "tag_operation";


    @BeforeClass
    public static void setup() throws IOException {
        init();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){
        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    //C2346
    @Test
    public void testTagAutomationOnUpdateMembership() throws Exception {

        NewMember member = HubMemberCreator.createMemberWithGovId(AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String tagName = "Auto" + timestamp;

        createAutomation(tagName,OldAutomationTriggers.UpdatesMembershipDetails,null,ActionsToPerform.TAG_UNTAG_MEMBER);

        MembersService.tryFindAndGetMemberByPhoneNumber(member);
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setGender(Gender.MALE.toString());
        MembersService.updateMember(memberDetails);
        Assert.assertTrue("The member did not get the tag sent, so the trigger did not started", MembersService.isTagExists(tagName));

    }

    //C2323
    @Test
    public void testTagAutomationOnMakeAPurchaseOnRange() throws Exception {

        String tagName = "Auto" + timestamp;
        //set which trigger to select
        createAutomation(tagName,OldAutomationTriggers.MakesPurchase,ConditionTypes.PurchaseTotalInRange,ActionsToPerform.TAG_UNTAG_MEMBER);

        NewMember member = HubMemberCreator.createMemberWithGovId(AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        Key key = hubMemberCreator.getMemberKeyToUserKey();

        //make the purchase
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member), "20000", transactionId);

        serverHelper.checkAutomationRunAndTagMemberByTransaction(dataStore,key,1,transactionId,tagName);
        Assert.assertTrue("The member did not get the tag sent, so the trigger did not started", MembersService.isTagExists(tagName));

    }

    //C2323
    @Test
    public void testTagAutomationOnMakeAPurchaseNotOnRange() throws Exception {

        String tagName = "Auto" + timestamp;
        createAutomation(tagName,OldAutomationTriggers.MakesPurchase,ConditionTypes.PurchaseTotalInRange,ActionsToPerform.TAG_UNTAG_MEMBER);


        NewMember member = HubMemberCreator.createMemberWithGovId(AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        Key key = hubMemberCreator.getMemberKeyToUserKey();

        //make the purchase
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member), "4000", transactionId);

        serverHelper.checkAutomationRunNotRunByTransaction(dataStore,key,transactionId);



    }

    private void createAutomation(String tagName,OldAutomationTriggers trigger,ConditionTypes conditionTypes,ActionsToPerform tagActionToPerform) throws Exception {

        //set which trigger to select
        OldAutomation oldAutomation = new OldAutomation();
        oldAutomation.setAutomationName(trigger.toString()+"_" + timestamp);
        oldAutomation.setTrigger(trigger);

        if(conditionTypes != null) {
            OldCondition condition = new OldCondition();
            condition.setConditionType(conditionTypes.toString());
            condition = HubOldAutomationCreator.createConditionPurchaseInRange(condition, "100", "400");
            oldAutomation.addCondition(condition, oldAutomation);
        }

        //set which action to perform
        OldAction action = HubOldAutomationCreator.buildTagMemberOldAction(tagName,oldAutomation, TagOptions.Tag);
        action.setPerformTheAction(tagActionToPerform.toString());

        oldAutomation.addAction(action,oldAutomation);
        OldAutomationService.createAutomation(oldAutomation);
    }


    private void performAnActionSubmitPurchase(ArrayList<NewMember> members, String totalSum, String transactionId) throws Exception {
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setPosId("1");
        submitPurchase.setBranchId(BRANCH_ID);
        submitPurchase.setTotalSum(totalSum);
        submitPurchase.setTransactionId(transactionId);
        submitPurchase.setChainId("chain123");
        submitPurchase.setCashier(EMPLOYEE + timestamp.toString());
        log4j.info("perform action submit purchase with TransactionID: " + transactionId);

        for (int i = 0; i < members.size(); i++) {
            Customer customer = new Customer();
            customer.setPhoneNumber(members.get(i).getPhoneNumber());
            submitPurchase.getCustomers().add(customer);
        }

        Item item = new Item();
        item.setItemCode(timestamp.toString());
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName(ITEM_NAME_PREFIX + timestamp.toString());
        item.setDepartmentCode(DEPARTMENT_CODE);
        item.setDepartmentName("Dep1");
        item.setPrice(Integer.valueOf(totalSum));
        item.setTags(Lists.newArrayList("DEP1"));
        submitPurchase.setItems(Lists.newArrayList(item));

        serverHelper.performAnActionSubmitPurchase(submitPurchase);
    }

    @After
    public void cleanData() {
        log4j.info("deactivate automation from APIs");
        BaseAutomationTest.deactivateAutomation();
    }
}
