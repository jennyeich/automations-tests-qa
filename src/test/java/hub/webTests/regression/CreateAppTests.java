package hub.webTests.regression;

import hub.hub1_0.services.Navigator;
import hub.services.NewApplicationService;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

/**
 * Created by doron on 23/05/2017.
 */
public class CreateAppTests extends BaseIntegrationTest {

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        isNewApplication = false;
        init();
    }

    //C2319
    @Test
    public void testCreateAppWithFacebook() throws Exception {

        Navigator.selectCreateNewApplication();
        NewApplicationService.createAppWithFacebook();
        NewApplicationService.publish();
        int version = NewApplicationService.getVersion();
        log4j.info("version: " + version);
        Assert.assertEquals("application version is not as expected- version: " + version, version ,1);
    }

    //C2343
    @Test
    public void testCreateAppFromScratch() throws Exception {
        Navigator.selectCreateNewApplication();
        NewApplicationService.createAppFromScratch();
        int version = NewApplicationService.getVersion();
        log4j.info("version: " + version);
        Assert.assertEquals("application version is not as expected- version: " + version, version ,1);
    }
}
