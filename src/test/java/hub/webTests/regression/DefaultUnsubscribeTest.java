package hub.webTests.regression;

import hub.hub1_0.common.objects.operation.CountryCode;
import hub.hub1_0.services.OperationService;
import hub.services.NewApplicationService;
import integration.base.BaseIntegrationTest;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;


/**
 * Created by Jenny on 26/02/2018.
 */


public class DefaultUnsubscribeTest extends BaseIntegrationTest {

    private static final String LINK = "link";
    private static final String REPLY = "reply";


    private static CharSequence timestamp;

    @BeforeClass
    public static void setup() throws Exception {
        init();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());

        }
    };


    @Test
    public void testCheckDefaultUnsubscribeMethodIsrael() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.Israel.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",REPLY, unsubscribe);

    }


    @Test
    public void testCheckDefaultUnsubscribeMethodBrazil() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.Brazil.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",LINK, unsubscribe);

    }

    @Test
    public void testCheckDefaultUnsubscribeMethodChile() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.Chile.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",LINK, unsubscribe);

    }

    @Test
    public void testCheckDefaultUnsubscribeMethodColombia() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.Colombila.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",LINK, unsubscribe);

    }


    @Test
    public void testCheckDefaultUnsubscribeMethodDenmark() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.Denmark.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",LINK, unsubscribe);

    }


    @Test
    public void testCheckDefaultUnsubscribeMethodItaly() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.Italy.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",LINK, unsubscribe);

    }


    @Test
    public void testCheckDefaultUnsubscribeMethodKazakstan() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.Kazakstan.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",LINK, unsubscribe);

    }


    @Test
    public void testCheckDefaultUnsubscribeMethodMexico() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.Mexico.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",LINK, unsubscribe);

    }

    @Test
    public void testCheckDefaultUnsubscribeMethodPanama() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.Panama.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",LINK, unsubscribe);

    }


    @Test
    public void testCheckDefaultUnsubscribeMethodSouthAfrika() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.SouthAfrika.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",LINK, unsubscribe);

    }

    @Test
    public void testCheckDefaultUnsubscribeMethodSweden() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.Sweden.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",LINK, unsubscribe);

    }

    @Test
    public void testCheckDefaultUnsubscribeMethodUnitedEmirates() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.UnitedArabEmirates.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",LINK, unsubscribe);

    }


    @Test
    public void testCheckDefaultUnsubscribeMethodUnitedKingdom() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.UnitedKingdom.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",LINK, unsubscribe);

    }

    @Test
    public void testCheckDefaultUnsubscribeMethodUnitedStates() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.UnitedStates.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",REPLY, unsubscribe);

    }

    @Test
    public void testCheckDefaultUnsubscribeMethodTurkey() throws Exception {

        NewApplicationService.createNewAppRegisterPageWithCountrySelection(CountryCode.Turkey.toString());
        String unsubscribe  = OperationService.checkUnsubscribeMethod();
        Assert.assertEquals("The unsubscribe method is not as expected",LINK, unsubscribe);

    }





}
