package hub.webTests.regression;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.benefits.LotteryReward;
import hub.common.objects.common.Gender;
import hub.common.objects.common.RewardTypes;
import hub.common.objects.common.TextAreaParameters;
import hub.common.objects.common.YesNoList;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.ApplyIf;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Condition;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Membership;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.TagOperator;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.services.AcpService;
import hub.services.DataBIService;
import hub.services.assets.AssetService;
import hub.services.member.MembersService;
import hub.utils.SmartAutomationsHelper;
import integration.base.BaseIntegrationTest;
import org.apache.commons.collections.map.HashedMap;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import ru.yandex.qatools.allure.annotations.Issue;
import server.internal.QueryUsers;
import server.utils.ServerHelper;
import server.utils.UserActionsConstants;
import server.v2_8.base.ZappServerErrorResponse;

import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Goni on 10/3/2017.
 */

public class LotteryRewardLimitationTest extends BaseIntegrationTest {


    private static final String IMPORT_550_FILENAME = "550Import.csv";
    private static LotteryReward lotteryReward;
    private static SmartGift smartGift;
    private static String smartGiftTitle;
    private static CharSequence timestamp;
    private static String lotteryId;
    private static String limit;
    private static String memberLimit = "100";
    private static String tag;
    private Key key = null;
    private static List<Key> membersKeys = new ArrayList<>();

    private SmartAutomationsHelper automationsHelper = new SmartAutomationsHelper(locationId, apiKey, serverToken);

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = "lottery_" +timestamp;
            try {
                if(smartGift == null) {
                    smartGift = hubAssetsCreator.createSmartGiftWithPushMessage("SmartGift_" + timestamp,"@" + TextAreaParameters.Membership_FirstName + " , you got new gift" );
                    smartGiftTitle = smartGift.getTitle();
                }

            } catch (Exception e) {
                log4j.error("error while creating asset",e);
            }
        }
    };

    @BeforeClass
    public static void setup() throws IOException {
       init();
        try {
            AcpService.importMembers(IMPORT_FOLDER + File.separator + IMPORT_550_FILENAME);
            Thread.sleep(30000);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //C7034
    //40% probablity to assign asset #2 with limit of 3 assets
    //40% probablity to assign asset #3 with limit of 2 assets
    //20% probablity to send push notification: "No asset, only push"
    //Define push notification in asset #2: "asset 2 test"
    //Define push notification in asset #3: "asset 3 test
    @Test
    public void testLotteryRewardWithLimitThroughFilterMemberDifferentProbabilities() throws Exception {

        SmartGift smartGift1 = hubAssetsCreator.createSmartGiftWithPushMessage("asset #2","asset #2 test" );
        SmartGift smartGift2 = hubAssetsCreator.createSmartGiftWithPushMessage("asset #3","asset #3 test" );

        Map<Integer,String> rewardCounterMap = new HashedMap();
        rewardCounterMap.put(0,"3");
        rewardCounterMap.put(1,"2");

        LotteryReward lotteryReward1 = hubAssetsCreator.buildLotteryReward(timestamp,memberLimit,Lists.newArrayList(tag));

        hubAssetsCreator.buildLotteryRewardOptionWithProbability(lotteryReward1,"60",smartGift1.getTitle(),"3", RewardTypes.GIVE_ASSET);
        hubAssetsCreator.buildLotteryRewardOptionWithProbability(lotteryReward1,"40",smartGift2.getTitle(),"2", RewardTypes.GIVE_ASSET);

        AssetService.createLotteryReward(lotteryReward1);
        log4j.info("Lottery rewards was created successfully. Title is:  " + lotteryReward1.getTitle());
        lotteryId = getLotteryActionID(lotteryReward1.getTitle());

        checkDataStoreWithRewardForActionRule(lotteryId,limit,memberLimit,tag);
        //send lottery to all mmembers
        findMembersAndSendLottery(lotteryReward1);

        TimeUnit.MINUTES.sleep(2);//need to wait enough time untile data store will be update

        //check that RewardRecordCounters updated with correct data (probability option,limit)
        checkRewardRecordCounters(lotteryId,rewardCounterMap);
        //check that only 5 actions performed according to the total limit of the lottery
        checkDataStoreRewardForActionPerformedStatus(lotteryId,true,5);

    }

    //C7033
    //Define a lottery reward with 100% probablity to assign asset #1 with limit of 3 assets
    @Test
    public void testLotteryRewardWithLimit() throws Exception {

        limit = "3";
        lotteryReward = hubAssetsCreator.createLotteryRewardWithPush(smartGiftTitle,memberLimit,limit,RewardTypes.GIVE_ASSET, Lists.newArrayList(tag), YesNoList.YES);

        lotteryId = getLotteryActionID(lotteryReward.getTitle());

        checkDataStoreWithRewardForActionRule(lotteryId,limit,memberLimit,tag);
        //send lottery to 3 mmembers
        for (int i = 0; i < Integer.valueOf(limit).intValue(); i++) {
            createNewMemberAndSendLottery();
        }

        checkDataStoreWithRewardRecordCounters(lotteryId,"3");
        checkDataStoreRewardForActionPerformedStatus(lotteryId,true,Integer.valueOf(limit).intValue());
        checkDataStoreUpdatedWithActionByLastUserKey(UserActionsConstants.PUSH_FAILED);
        checkDataStoreUpdatedWithActionByLastUserKey(UserActionsConstants.RECEIVEDASSET);

        //send lottery to 1 more members
        String tag = createNewMemberAndSendLottery();
        //check the RewardForAction that was taged is not performed
        checkDataStoreRewardForActionPerformedStatus(lotteryId,false,1);
        checkDataStoreNotUpdatedWithActionByLastUserKey(UserActionsConstants.PUSH_FAILED);
        checkDataStoreNotUpdatedWithActionByLastUserKey(UserActionsConstants.RECEIVEDASSET);
    }



    //C7035
    //Define a lottery reward with 100% probablity to assign asset #1 with limit of 2 assets
    //Trigger the automation 3 times and see that asset sent only 3 times
    @Test
    public void testLotteryRewardThroughAutomationWithLimit() throws Exception {

        limit = "2";
        lotteryReward = hubAssetsCreator.createLotteryRewardWithPush(smartGiftTitle,memberLimit,limit,RewardTypes.GIVE_ASSET, Lists.newArrayList(tag), YesNoList.YES);

        lotteryId = getLotteryActionID(lotteryReward.getTitle());

        checkDataStoreWithRewardForActionRule(lotteryId,limit,memberLimit,tag);

        NewMember member = hubMemberCreator.createMember(String.valueOf(System.currentTimeMillis()),AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        MembersService.performTagMemberSmartAction(tag,"TagMember" +timestamp);

        createSmartAutomation(tag);

        updateMemberToRunAutomation(2,member.getPhoneNumber());

        int numOfActionsByLimit = Integer.valueOf(limit).intValue();
        checkDataStoreWithRewardRecordCounters(lotteryId,"2");//one member rewarded
        checkDataStoreRewardForActionPerformedStatus(lotteryId,true,numOfActionsByLimit);
        checkDataStoreUpdatedWithActionByLastUserKey(UserActionsConstants.PUSH_FAILED,numOfActionsByLimit);
        checkDataStoreUpdatedWithActionByLastUserKey(UserActionsConstants.RECEIVEDASSET,numOfActionsByLimit);

        //trigger automation again
        performUpdateMemberGender(member.getPhoneNumber(),Gender.FEMALE);

        //check the RewardForAction that was invoke after limit ended is not performed
        checkDataStoreRewardForActionPerformedStatus(lotteryId,false,1);
    }


    //C7037
    @Test
    public void testLotteryRewardWithLimitBBBLike() throws Exception {

        SmartGift smartGift1 = hubAssetsCreator.createSmartGiftWithPushMessage("asset 1 30%","asset #1 test" );
        SmartGift smartGift2 = hubAssetsCreator.createSmartGiftWithPushMessage("asset 2 30%","asset #2 test" );
        SmartGift smartGift3 = hubAssetsCreator.createSmartGiftWithPushMessage("asset 3 40%","asset #3 test" );

        LotteryReward lotteryReward1 = hubAssetsCreator.buildLotteryReward(timestamp,memberLimit,Lists.newArrayList(tag));

        hubAssetsCreator.buildLotteryRewardOptionWithProbability(lotteryReward1,"30",smartGift1.getTitle(),"2", RewardTypes.GIVE_ASSET);
        hubAssetsCreator.buildLotteryRewardOptionWithProbability(lotteryReward1,"30",smartGift2.getTitle(),"3", RewardTypes.GIVE_ASSET);
        hubAssetsCreator.buildLotteryRewardOptionWithProbability(lotteryReward1,"40",smartGift3.getTitle(),"5", RewardTypes.GIVE_ASSET);

        AssetService.createLotteryReward(lotteryReward1);
        log4j.info("Lottery rewards was created successfully. Title is:  " + lotteryReward1.getTitle());
        lotteryId = getLotteryActionID(lotteryReward1.getTitle());

        Map<Integer,String> rewardCounterMap = new HashedMap();
        rewardCounterMap.put(0,"2");
        rewardCounterMap.put(1,"3");
        rewardCounterMap.put(2,"5");

        checkDataStoreWithRewardForActionRule(lotteryId,limit,memberLimit,tag);
        //send lottery to all mmembers
        findMembersAndSendLottery(lotteryReward1);

        //check that RewardRecordCounters updated with correct data (probability option,limit)
        checkRewardRecordCounters(lotteryId,rewardCounterMap);
        //check that only 10 actions performed according to the total limit of the lottery
        checkDataStoreRewardForActionPerformedStatus(lotteryId,true,10);

    }

    private void findMembersAndSendLottery(LotteryReward lotteryReward1) throws Exception {
        MembersService.findMembers();
        MembersService.performSmartActionSendLotteryReward(lotteryReward1,timestamp);
    }

    //C7038
    //Define a lottery reward with 100% probablity to assign asset #1 with  no limit
    @Test
    public void testLotteryRewardWithNoLimit() throws Exception {

        limit = null;
        lotteryReward = hubAssetsCreator.createLotteryRewardWithPush(smartGiftTitle,memberLimit,limit,RewardTypes.GIVE_ASSET, Lists.newArrayList(tag), YesNoList.YES);;

        lotteryId = getLotteryActionID(lotteryReward.getTitle());

        checkDataStoreWithRewardForActionRule(lotteryId,limit,memberLimit,tag);
        //send lottery to 3 mmembers
        createNewMemberAndSendLottery();
        createNewMemberAndSendLottery();
        createNewMemberAndSendLottery();

        TimeUnit.MINUTES.sleep(1);//need to wait enough time untile data store will be update

        checkDataStoreRewardForActionPerformedStatus(lotteryId,true,3);
        checkDataStoreUpdatedWithActionByLastUserKey(UserActionsConstants.PUSH_FAILED);

        //send lottery to 1 more members
        createNewMemberAndSendLottery();
        TimeUnit.MINUTES.sleep(1);//need to wait enough time untile data store will be update
        //check All actions performed
        checkDataStoreRewardForActionPerformedStatus(lotteryId,true,4);
        checkDataStoreUpdatedWithActionByLastUserKey(UserActionsConstants.PUSH_FAILED);
        checkDataStoreUpdatedWithActionByLastUserKey(UserActionsConstants.RECEIVEDASSET);
    }

    //C7039
    //Send lottery from import user keys to the list of 20 users (with lottery that is limited to 3 100% probability) - only 3 will recieve
    @Test
    public void testLotteryRewardWithLimitFromImportUserKeys() throws Exception {

        limit = "2";
        lotteryReward = hubAssetsCreator.createLotteryRewardProbability100(timestamp,smartGiftTitle,memberLimit,limit, Lists.newArrayList(tag));

        lotteryId = getLotteryActionID(lotteryReward.getTitle());

        checkDataStoreWithRewardForActionRule(lotteryId,limit,memberLimit,tag);
        import10UsersKeys();

        //send lottery to members
        MembersService.performSmartActionSendLotteryReward(lotteryReward,timestamp);

        checkDataStoreWithRewardRecordCounters(lotteryId,"2");
        checkDataStoreRewardForActionPerformedStatus(lotteryId,true,Integer.valueOf(limit).intValue());

    }

    //C7041:test action tags and asset source
    @Test
    @Category(Hub1Regression.class)
    public void testLotteryRewardWithLimitAndActionTag() throws Exception {

        limit = "2";
        lotteryReward = hubAssetsCreator.createLotteryRewardProbability100(timestamp,smartGiftTitle,memberLimit,limit, Lists.newArrayList(tag));

        lotteryId = getLotteryActionID(lotteryReward.getTitle());

        checkDataStoreWithRewardForActionRule(lotteryId,limit,memberLimit,tag);
        String tag1 = createNewMemberAndSendLottery();

        checkDataStoreUpdatedWithActionByActionTag(tag1);
    }

    public  void checkDataStoreUpdatedWithActionByActionTag(String actionTag) throws Exception {

        log4j.info("check Data Store Updated with action receive asset");
        log4j.info("ActionTag: " + actionTag);
        Query q = new Query("UserAction");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("LocationID", Query.FilterOperator.EQUAL, locationId),
                new Query.FilterPredicate("ActionTags", Query.FilterOperator.EQUAL, actionTag))));
        List<Entity> res = ServerHelper.queryWithWait("Must find 1 UserAction with the ActionTags: " + actionTag,
                q,1, dataStore);

        Assert.assertEquals("Number of actions with actionTag " + actionTag + " must be 1", 1, res.size());
    }

    private void import10UsersKeys() throws Exception{
        QueryUsers queryUsers = new QueryUsers(serverToken, locationId);
        Object response = queryUsers.sendRequestByClass(Object.class);

        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        StringBuffer userKeys = new StringBuffer();
        List<Object> members = (List<Object>) response;
        if (members.isEmpty()) {
            log4j.info("No members found.... check if import succeeded ");
            return;
        }
        int size = members.size();
        if (size > 10)
            size = 10;
        for (int i = 0; i < size; i++) {
            userKeys.append(((LinkedHashMap) members.get(i)).get("UserKey")).append(',');
        }

        DataBIService.importUserKeys(userKeys);
        DataBIService.clickLastCreatedUsersKeysList();
    }

    private void checkRewardRecordCounters(String rewardId, Map<Integer,String> rewardCounterMap) throws Exception {

        Thread.sleep(80000);//wait until the RewardRecordCounters will update
        Query q = new Query("RewardForActionRule");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("LocationID", Query.FilterOperator.EQUAL, locationId),
                new Query.FilterPredicate("ActionID", Query.FilterOperator.EQUAL, rewardId))));
        List<Entity> res = ServerHelper.queryWithWait("Must find 1 RewardForActionRule with the ActionID: " + rewardId,
                q,1, dataStore);

        Text t = (Text) res.get(0).getProperty("RandomRewardData");
        log4j.info("RandomRewardData contains the following data : " + t.getValue());
        Text counters = (Text) res.get(0).getProperty("RewardRecordCounters");
        int retry = 0;
        if(counters == null && retry < 3) {
            res = ServerHelper.queryWithWait("Must find 1 RewardForActionRule with the ActionID: " + rewardId,
                    q, 1, dataStore);
            counters = (Text) res.get(0).getProperty("RewardRecordCounters");
            retry++;
        }

        //check that all probabilities have reached their limit and did not passed it
        for (Integer index:rewardCounterMap.keySet()) {
            String limit = rewardCounterMap.get(index);
            String indexStr = String.valueOf(index);
            Assert.assertTrue("RewardRecordCounters must contain: the correct data", counters.getValue().contains("\"" + indexStr+ "\":\"" +  limit + "\""));
        }

    }

    private void updateMemberToRunAutomation(int iterations,String memberPhone) throws Exception{

        for (int i = 1; i <= iterations; i++) {
            if(i%2 == 0)
                performUpdateMemberGender(memberPhone,Gender.FEMALE);
            else
                performUpdateMemberGender(memberPhone,Gender.MALE);
        }

    }

    private void performUpdateMemberGender(String memberPhone,Gender gender) throws Exception{
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setPhoneNumber(memberPhone);
        memberDetails.setGender(gender.toString());
        MembersService.updateMemberDetails(memberDetails);

    }


    private void createSmartAutomation(String tag) throws Exception {
        timestamp = String.valueOf(System.currentTimeMillis());

        Settings settings = new Settings();
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(Triggers.UpdatesMembershipDetails,Triggers.UpdatesMembershipDetails.name() + "_" + timestamp, settings);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsAll);
        Condition cond = hubAssetsCreator.createConditionTextArea(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, tag);
        automationsHelper.createSmartAutomationScenario(automation, Lists.newArrayList(cond), false, Lists.newArrayList(SmartAutomationActions.SendALotteryReward), lotteryReward.getTitle(),tag);
    }

    private void checkDataStoreUpdatedWithActionByLastUserKey(String actionName) throws Exception {
        checkDataStoreUpdatedWithActionByLastUserKey(actionName,1);
    }


    private void checkDataStoreUpdatedWithActionByLastUserKey(String actionName,int numOfActions) throws Exception {
        log4j.info("check Data Store Updated with Tag");
        Query q = new Query("UserAction");
        Query.CompositeFilter filter = new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
        new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, actionName),
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL,key)));
        q.setFilter(filter);
        List<Entity> res = ServerHelper.queryWithWait("Must find 1 UserAction with the action: " + actionName,
               q, numOfActions, dataStore);

        Assert.assertTrue("Must find 1 UserAction with the action: " + actionName,res.size() == numOfActions);
    }

    private void checkDataStoreNotUpdatedWithActionByLastUserKey(String actionName) throws Exception {
        log4j.info("check Data Store Updated with Tag");
        Query q = new Query("UserAction");
        Query.CompositeFilter filter = new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, actionName),
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL,key)));
        q.setFilter(filter);
        List<Entity> res = dataStore.findByQuery(q);
        Assert.assertTrue("Must find 1 UserAction with the action: " + actionName,res.isEmpty());
    }

    private void createNewMembers(int numOfMembers) throws Exception{

        //create member
        for (int i = 0; i < numOfMembers; i++) {
            hubMemberCreator.createMember(String.valueOf(System.currentTimeMillis()),AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
            //Get member userKey and turn into User_KEY
            key = hubMemberCreator.getMemberKeyToUserKey();
            membersKeys.add(key);
        }

    }


    private String createNewMemberAndSendLottery() throws Exception {
        //Create member
        String timeMillis = String.valueOf(System.currentTimeMillis());
        hubMemberCreator.createMember(timeMillis,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String tag = MembersService.performSmartActionSendLotteryReward(lotteryReward, timeMillis);
        return tag;
    }

    public String getLotteryActionID(String lotteryName) throws Exception {
        String id = AssetService.getLotteryRewardId(lotteryName);
        log4j.info("ActionID : "+ id);
        return id;
    }


    private void checkDataStoreWithRewardForActionRule(String rewardId,String limit,String memberLimit,String tags) throws Exception{
        TimeUnit.MINUTES.sleep(3);//wait until the RewardRecordCounters will update
        Query q = new Query("RewardForActionRule");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("LocationID", Query.FilterOperator.EQUAL, locationId),
                new Query.FilterPredicate("ActionID", Query.FilterOperator.EQUAL, rewardId))));
        List<Entity> res = ServerHelper.queryWithWait("Must find 1 RewardForActionRule with the ActionID: " + rewardId,
                q,1, dataStore);

        Text t = (Text) res.get(0).getProperty("RandomRewardData");
        log4j.info("RandomRewardData contains the following data : " + t.getValue());
        if(limit != null)
            Assert.assertTrue("RandomRewardData must contain Limit: " + limit, t.getValue().contains("Limit=\"" + limit  + "\""));

        Assert.assertTrue("RandomRewardData must contain MaxRewardsForActionAndTag: " + memberLimit, t.getValue().contains("MaxRewardsForActionAndTag=\"" + memberLimit + "\""));
        Assert.assertTrue("RandomRewardData must contain Tags: " + tags, t.getValue().contains("Tags text=\"" + tags + "\""));
    }

    private void checkDataStoreWithRewardRecordCounters(String rewardId,String numOfMembers) throws Exception{
        TimeUnit.MINUTES.sleep(3);//wait until the RewardRecordCounters will update
        Query q = new Query("RewardForActionRule");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("LocationID", Query.FilterOperator.EQUAL, locationId),
                new Query.FilterPredicate("ActionID", Query.FilterOperator.EQUAL, rewardId))));
        List<Entity> res = ServerHelper.queryWithWait("Must find 1 RewardForActionRule with the ActionID: " + rewardId,
                q,1, dataStore);

        Text t = (Text) res.get(0).getProperty("RandomRewardData");
        log4j.info("RandomRewardData contains the following data : " + t.getValue());
        Text counters = (Text) res.get(0).getProperty("RewardRecordCounters");
        int retry = 3;
        if(counters == null &&  retry >= 0) {
            res = ServerHelper.queryWithWait("Must find 1 RewardForActionRule with the ActionID: " + rewardId,
                    q, 1, dataStore);
            counters = (Text) res.get(0).getProperty("RewardRecordCounters");
            retry--;
        }
        Assert.assertTrue("RewardRecordCounters must contain: {\"0\":\"" +numOfMembers + "\"}", counters.getValue().contains("\"0\":\"" +  numOfMembers + "\""));

    }

    private void checkDataStoreRewardForActionPerformedStatus(String rewardId,boolean expect, int numberOfActions) throws Exception{
        Thread.sleep(20000);//wait until the datastore will update
        Query q = new Query("RewardForAction");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("LocationID", Query.FilterOperator.EQUAL, locationId),
                new Query.FilterPredicate("ActionID", Query.FilterOperator.EQUAL, rewardId))));
        List<Entity> res = dataStore.findByQuery(q);

        int counter = 0;
        for (int i = 0; i < res.size(); i++) {
            Boolean performed =(Boolean)res.get(i).getProperty("ActionPerformed");
            if( (performed != null) && (performed.booleanValue() == expect) )
                counter++;
        }

        Assert.assertEquals("RewardForAction by performed status not as expected : " + numberOfActions,numberOfActions,counter );

    }

}

