package hub.webTests.regression;

import com.google.appengine.api.datastore.Query;
import hub.common.objects.content.information.CatalogItemPage;
import hub.common.objects.content.information.CatalogPage;
import hub.common.objects.content.information.CategoryPage;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.common.objects.model.ExportItemModel;
import hub.common.objects.smarts.SmartPunchCard;
import hub.hub1_0.services.Navigator;
import hub.services.catalog.CatalogService;
import hub.services.member.MembersService;
import hub.utils.HubCatalogCreator;
import hub.utils.HubMemberCreator;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import server.utils.ServerHelper;

import java.util.Arrays;

/**
 * Created by doron on 16/05/2017.
 */
public class WebSanityTests extends BaseIntegrationTest {


    public static CharSequence timestamp;
    private static NewMember member;


    @BeforeClass
    public static void setup() throws Exception {
        init();
        member = HubMemberCreator.createMemberWithGovId(AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
    }


    //C2350
    @Test
    public void testDeleteMembership() throws Exception {
        //Create member
        timestamp = String.valueOf(System.currentTimeMillis());
        NewMember member = hubMemberCreator.createMember(timestamp, "delete" + AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //find and delete member
        MemberDetails memberDetails = MembersService.deleteMember(member);
        //find member if removed return null
        MemberDetails memberDetails2 = MembersService.tryFindAndGetMemberByPhoneNumber(member);

        Assert.assertNull("member <" + memberDetails2 + "> was not deleted", memberDetails2);
    }

    //C2351
    @Test
    public void testExportMembersFromFindMembers() throws Exception {
        //find all members
        MembersService.findMembers();
        Thread.sleep(3000);
        int numOfMembers = MembersService.getNumberOfMembersOnFindMember();
        Assert.assertTrue("find members did not found any member", numOfMembers > 0);
        MembersService.exportFindMembers();
        ExportItemModel exportItem = MembersService.exportFindMembers().get(0);//get the last export report
        log4j.info("MembersService.getFindMemberExportCount(): " + MembersService.getFindMemberExportCount());
        Assert.assertTrue("export did not show in exports list, num of members to export <" + numOfMembers + "> and last export num of members <" + exportItem.getMembersCount(), numOfMembers == exportItem.getMembersCount());
        // TODO: 15/05/2017  implement check csv file for row count
    }

    //C2321
    @Test
    public void testCreateNewCatalogItem() throws Exception {
        timestamp = String.valueOf(System.currentTimeMillis());
        CatalogPage catalogPage = HubCatalogCreator.buildCatalog("catalog" + timestamp);
        CategoryPage categoryPage = HubCatalogCreator.buildCategory("category" + timestamp);
        CatalogItemPage catalogItemPage = HubCatalogCreator.buildCatalogItem("catalogItem"+timestamp,categoryPage.getTitle(),true);
        catalogPage.addCategoryPage(categoryPage);
        catalogPage.addCatalogItemPage(catalogItemPage);
        CatalogService.createNewCatalog(catalogPage);
        CatalogService.selectCatalog(catalogPage.getTitle());
        Assert.assertTrue(CatalogService.isItemExist(catalogItemPage));

    }

    //C2321
    @Test
    public void testCreateAndEditCatalogItem() throws Exception {
        timestamp = String.valueOf(System.currentTimeMillis());
        CatalogPage catalogPage = HubCatalogCreator.buildCatalog("catalog" + timestamp);
        CategoryPage categoryPage = HubCatalogCreator.buildCategory("category" + timestamp);
        CatalogItemPage catalogItemPage = HubCatalogCreator.buildCatalogItem("catalogItem"+timestamp,categoryPage.getTitle(),true);
        catalogPage.addCategoryPage(categoryPage);
        catalogPage.addCatalogItemPage(catalogItemPage);
        CatalogService.createNewCatalog(catalogPage);
        CatalogService.selectCatalog(catalogPage.getTitle());
        CatalogService.selectCatalogItemFromList(catalogItemPage);
        CatalogItemPage editedPage = CatalogService.editCatalogItem("editedCatalogItem" + timestamp,categoryPage.getTitle());
        Assert.assertTrue("could not find catalogItem <" + editedPage.getTitle() + ">in current catalog", CatalogService.isItemExist(editedPage));
    }

    //C2330
    @Test
    public void testFindMemberByPhoneNumber() throws Exception {
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MemberDetails memberDetails = MembersService.tryFindAndGetMember(findMember, member.getFirstName());
        log4j.info("member.getFirstName(): " + member.getFirstName());
        log4j.info("findMember.getPhoneNumber(): " + findMember.getPhoneNumber());
        log4j.info("memberDetails.getFirstName(): " + memberDetails.getFirstName());
        Assert.assertNotNull("could not found member " + member.getFirstName() + " by phone number: " + findMember.getPhoneNumber(), memberDetails);
        Assert.assertTrue("could not found member " + member.getFirstName() + " by phone number: " + findMember.getPhoneNumber(), member.getFirstName().contentEquals(memberDetails.getFirstName()));
    }

    //C2330
    @Test
    public void testFindMemberByEmail() throws Exception {
        FindMember findMember = new FindMember();
        findMember.setEmail(member.getEmail());
        MemberDetails memberDetails = MembersService.findAndGetMember(findMember);
        log4j.info("member.getFirstName(): " + member.getFirstName());
        log4j.info("findMember.getEmail(): " + findMember.getEmail());
        log4j.info("memberDetails.getFirstName(): " + memberDetails.getFirstName());
        Assert.assertNotNull("could not found member " + member.getFirstName() + " by Email: " + findMember.getEmail(), memberDetails);
        Assert.assertTrue("could not found member " + member.getFirstName() + " by Email: " + findMember.getEmail(), member.getFirstName().contentEquals(memberDetails.getFirstName()));
    }

    //C2330
    @Test
    public void testFindMemberByBirthDay() throws Exception {
        FindMember findMember = new FindMember();
        findMember.setBirthday(member.getBirthday());
        MemberDetails memberDetails = MembersService.tryFindAndGetMember(findMember, member.getFirstName());
        log4j.info("member.getFirstName(): " + member.getFirstName());
        log4j.info("findMember.getBirthday(): " + findMember.getBirthday());
        log4j.info("memberDetails.getFirstName(): " + memberDetails.getFirstName());
        Assert.assertNotNull("could not found member " + member.getFirstName() + " by birthday: " + findMember.getBirthday(), memberDetails.getFirstName());
        Assert.assertTrue("could not found member " + member.getFirstName() + " by birthday: " + findMember.getBirthday(), member.getFirstName().contentEquals(memberDetails.getFirstName()));

    }

    //C2347
    @Test
    public void testFindMemberAndSendAsset() throws Exception {
        timestamp = String.valueOf(System.currentTimeMillis());
        SmartPunchCard punchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");
        MemberDetails memberDetails = MembersService.tryFindAndGetMemberByPhoneNumber(member);
        log4j.info("member.getFirstName(): " + member.getFirstName());
        log4j.info("memberDetails.getFirstName(): " + memberDetails.getFirstName());
        Assert.assertNotNull("could not found member " + member.getFirstName(), memberDetails.getFirstName());
        MembersService.performSmartActionSendAPunchCard(punchCard, timestamp);
        Navigator.refresh();
        log4j.info("punchCard.getTitle(): " + punchCard.getTitle());
        Assert.assertTrue("member: " + memberDetails.getFirstName() + " dose not have ths current punchCard: " + punchCard.getTitle(), MembersService.isGiftExists(punchCard.getTitle()));
    }


    //C2330
    @Test
    public void testFindMemberByGovId() throws Exception {
        FindMember findMember = new FindMember();
        findMember.setGovId(member.getGovId());
        MemberDetails memberDetails = MembersService.tryFindAndGetMember(findMember, member.getFirstName());
        Assert.assertNotNull("could not found member " + member.getFirstName() + " by birthday: " + findMember.getBirthday(), memberDetails.getFirstName());
        Assert.assertTrue("could not found member " + member.getFirstName() + " by birthday: " + findMember.getBirthday(), member.getFirstName().contentEquals(memberDetails.getFirstName()));
    }

    private Query buildQuerySearchGiftByTitle(String title) {
        return ServerHelper.buildQuery("Asset", Arrays.asList(
                new Query.FilterPredicate("Type", Query.FilterOperator.EQUAL, "gift"),
                new Query.FilterPredicate("Name", Query.FilterOperator.EQUAL, title)));
    }


}
