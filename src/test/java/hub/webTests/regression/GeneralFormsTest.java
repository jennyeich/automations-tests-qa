package hub.webTests.regression;

import com.google.appengine.repackaged.com.google.common.collect.Lists;
import hub.common.objects.common.FormFieldAnswerType;
import hub.common.objects.common.YesNoList;
import hub.common.objects.content.forms.FormFieldPage;
import hub.common.objects.content.forms.FormFieldsConstants;
import hub.common.objects.content.forms.FormPage;
import hub.common.objects.content.forms.GeneralFormPage;
import hub.hub1_0.services.Navigator;
import hub.services.ContentService;
import hub.utils.HubFormsCreator;
import integration.base.BaseIntegrationTest;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by taya.ashkenazi on 6/5/17.
 */
public class GeneralFormsTest extends BaseIntegrationTest {

    public CharSequence timestamp;

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());

        }
    };

    @BeforeClass
    public static void setup() throws IOException {
       init();
    }


    //C2333
    @Test
    public void testCreateNewForm() throws Exception {
        String formTitle = createForm();
        Assert.assertTrue("Form with title: " + formTitle + " doesn't exist", ContentService.isFormCreated(formTitle,GeneralFormPage.FORM_TITLE));

    }

    @Test
    public void testCreateNewFormCheckUrl() throws Exception {

        String formTitle = createForm();
        Assert.assertTrue("Form with title: " + formTitle + " doesn't exist", ContentService.isFormCreated(formTitle, GeneralFormPage.FORM_TITLE));

        try {
            String url = ContentService.extractFormUrl(formTitle);

            Assert.assertTrue("Web form was not opened", ContentService.openFormInWeb(url));

            Assert.assertTrue("Incorrect form was opened", ContentService.isCorrectFormOpeninWeb(HubFormsCreator.AUTO_DESCRIPTION_PREFIX));

            closeFormTabAndDeleteForm(formTitle);
        } catch (Exception e) {
            recoverFromException(e);
        }
    }


    @Test
    public void testFieldTextInWebLinkForm() throws Exception {

        //create form
        String formTitle = createForm();

        //add field
        FormFieldPage fieldPage = addFormField(FormFieldAnswerType.FREE_TEXT_LINE,YesNoList.YES.toString());

        ContentService.updateForm(formTitle,fieldPage);
        Navigator.contentTabGeneralForms();
        try {
            String url = ContentService.extractFormUrl(formTitle);
            Assert.assertTrue("Web form was not opened", ContentService.openFormInWeb(url));

            checkField(fieldPage,FormFieldPage.TEXT);

            closeFormTabAndDeleteForm(formTitle);
        } catch (Exception e) {
            recoverFromException(e);
        }
    }

    @Test
    public void testFieldTextNotMandatoryInWebLinkForm() throws Exception {

        //create form
        String formTitle = createForm();

        //add field
        FormFieldPage fieldPage = addFormField(FormFieldAnswerType.FREE_TEXT_LINE,YesNoList.NO.toString());

        ContentService.updateForm(formTitle,fieldPage);
        Navigator.contentTabGeneralForms();
        try {
            String url = ContentService.extractFormUrl(formTitle);
            Assert.assertTrue("Web form was not opened", ContentService.openFormInWeb(url));

            checkField(fieldPage,FormFieldPage.TEXT);

            closeFormTabAndDeleteForm(formTitle);
        } catch (Exception e) {
            recoverFromException(e);
        }
    }

    @Test
    public void testFieldTextMandatoryIsEmptyInWebLinkFormNotSubmitted() throws Exception {

        //create form
        FormPage formPage = HubFormsCreator.createForm(timestamp,HubFormsCreator.AUTO_TITLE_PREFIX, HubFormsCreator.AUTO_RECIPIENTS, HubFormsCreator.AUTO_THANK_YOU_TITLE_PREFIX, HubFormsCreator.AUTO_THANK_YOU_CONTENT_PREFIX, HubFormsCreator.AUTO_DESCRIPTION_PREFIX);
        String formTitle = formPage.getTitle();

        //add field
        FormFieldPage fieldPage = addFormField(FormFieldAnswerType.FREE_TEXT_LINE,YesNoList.YES.toString());

        ContentService.updateForm(formTitle,fieldPage);
        Navigator.contentTabGeneralForms();
        try {
            String url = ContentService.extractFormUrl(formTitle);
            Assert.assertTrue("Web form was not opened", ContentService.openFormInWeb(url));

            checkField(fieldPage,FormFieldPage.TEXT);

            //check if submit performed
            Assert.assertFalse("Form is missing required field and should not be submitted",ContentService.submitForm(formPage));

            closeFormTabAndDeleteForm(formTitle);
        } catch (Exception e) {
            recoverFromException(e);
        }
    }

    @Test
    public void testFieldTextNotMandatoryIsEmptyInWebLinkFormSubmitted() throws Exception {

        //create form
        FormPage formPage = HubFormsCreator.createForm(timestamp,HubFormsCreator.AUTO_TITLE_PREFIX, HubFormsCreator.AUTO_RECIPIENTS, HubFormsCreator.AUTO_THANK_YOU_TITLE_PREFIX, HubFormsCreator.AUTO_THANK_YOU_CONTENT_PREFIX, HubFormsCreator.AUTO_DESCRIPTION_PREFIX);
        String formTitle = formPage.getTitle();

        //add field
        FormFieldPage fieldPage = addFormField(FormFieldAnswerType.FREE_TEXT_LINE,YesNoList.NO.toString());

        ContentService.updateForm(formTitle,fieldPage);
        Navigator.contentTabGeneralForms();
        try {
            String url = ContentService.extractFormUrl(formTitle);
            Assert.assertTrue("Web form was not opened", ContentService.openFormInWeb(url));

            checkField(fieldPage,FormFieldPage.TEXT);

            //check if submit performed
            Assert.assertTrue("Form is missing non-mandatory field but should be submitted",ContentService.submitForm(formPage));

            closeFormTabAndDeleteForm(formTitle);
        } catch (Exception e) {
            recoverFromException(e);
        }
    }

    @Test
    public void testFieldTextBoxInWebLinkForm() throws Exception {

        //create form
        String formTitle = createForm();

        //add field
        FormFieldPage fieldPage = addFormField(FormFieldAnswerType.FREE_TEXT_BOX,YesNoList.YES.toString());

        ContentService.updateForm(formTitle,fieldPage);
        Navigator.contentTabGeneralForms();
        try {
            String url = ContentService.extractFormUrl(formTitle);
            Assert.assertTrue("Web form was not opened", ContentService.openFormInWeb(url));

            checkField(fieldPage,FormFieldPage.TEXT_BOX);

            closeFormTabAndDeleteForm(formTitle);
        } catch (Exception e) {
            recoverFromException(e);
        }
    }

    @Test
    public void testFieldCheckboxInWebLinkForm() throws Exception {

        //create form
        String formTitle = createForm();

        //add field
        FormFieldPage fieldPage = addFormField(FormFieldAnswerType.CHECK_BOX,YesNoList.YES.toString());

        ContentService.updateForm(formTitle,fieldPage);
        Navigator.contentTabGeneralForms();
        try {
            String url = ContentService.extractFormUrl(formTitle);
            Assert.assertTrue("Web form was not opened", ContentService.openFormInWeb(url));

            checkField(fieldPage,FormFieldPage.CHECKBOX);

            closeFormTabAndDeleteForm(formTitle);
        } catch (Exception e) {
            recoverFromException(e);
        }
    }

    @Test
    public void testFieldMultiSelectInWebLinkForm() throws Exception {

        //create form
        String formTitle = createForm();

        //add field
        FormFieldPage fieldPage = addFormField(FormFieldAnswerType.MULTIPLE_ANSWERS,YesNoList.YES.toString());

        ContentService.updateForm(formTitle,fieldPage);
        Navigator.contentTabGeneralForms();
        try {
            String url = ContentService.extractFormUrl(formTitle);
            Assert.assertTrue("Web form was not opened", ContentService.openFormInWeb(url));

            checkField(fieldPage,FormFieldPage.MULT_SELECT);

            closeFormTabAndDeleteForm(formTitle);
        } catch (Exception e) {
            recoverFromException(e);
        }
    }
    @Test
    public void testFieldMultiSelectOneChoiceInWebLinkForm() throws Exception {

        //create form
        String formTitle = createForm();

        //add field
        FormFieldPage fieldPage = addFormField(FormFieldAnswerType.MULTIPLE_CHOICE_ONE_ANSWER,YesNoList.YES.toString());

        ContentService.updateForm(formTitle,fieldPage);
        Navigator.contentTabGeneralForms();
        try {
            String url = ContentService.extractFormUrl(formTitle);
            Assert.assertTrue("Web form was not opened", ContentService.openFormInWeb(url));

            checkField(fieldPage,FormFieldPage.MULTI_SELECT_ONE_CHOICE);

            closeFormTabAndDeleteForm(formTitle);
        } catch (Exception e) {
            recoverFromException(e);
        }
    }

    private String createForm() throws Exception {
        FormPage formPage = HubFormsCreator.createForm(timestamp,HubFormsCreator.AUTO_TITLE_PREFIX, HubFormsCreator.AUTO_RECIPIENTS, HubFormsCreator.AUTO_THANK_YOU_TITLE_PREFIX, HubFormsCreator.AUTO_THANK_YOU_CONTENT_PREFIX, HubFormsCreator.AUTO_DESCRIPTION_PREFIX);
        return formPage.getTitle();
    }

    @Test
    public void testFieldTimeInWebLinkForm() throws Exception {

        //create form
        String formTitle = createForm();

        //add field
        FormFieldPage fieldPage = addFormField(FormFieldAnswerType.TIME,YesNoList.YES.toString());

        ContentService.updateForm(formTitle,fieldPage);
        Navigator.contentTabGeneralForms();
        try {
            String url = ContentService.extractFormUrl(formTitle);
            Assert.assertTrue("Web form was not opened", ContentService.openFormInWeb(url));

            checkField(fieldPage,FormFieldPage.TIME);

            closeFormTabAndDeleteForm(formTitle);
        } catch (Exception e) {
            recoverFromException(e);
        }
    }

    @Test
    public void testFieldDateInWebLinkForm() throws Exception {

        //create form
        String formTitle = createForm();

        //add field
        FormFieldPage fieldPage = addFormField(FormFieldAnswerType.DATE,YesNoList.YES.toString());

        ContentService.updateForm(formTitle,fieldPage);
        Navigator.contentTabGeneralForms();
        try {
            String url = ContentService.extractFormUrl(formTitle);
            Assert.assertTrue("Web form was not opened", ContentService.openFormInWeb(url));

            checkField(fieldPage,FormFieldPage.DATE);

            closeFormTabAndDeleteForm(formTitle);
        } catch (Exception e) {
            recoverFromException(e);
        }
    }

    @Test
    public void testCheckMailWebLinkForm() throws Exception {

        //create form
        FormPage formPage = HubFormsCreator.createForm(timestamp,HubFormsCreator.AUTO_TITLE_PREFIX, HubFormsCreator.AUTO_RECIPIENTS, HubFormsCreator.AUTO_THANK_YOU_TITLE_PREFIX, HubFormsCreator.AUTO_THANK_YOU_CONTENT_PREFIX, HubFormsCreator.AUTO_DESCRIPTION_PREFIX);
        String formTitle = formPage.getTitle();

        //add field
        FormFieldPage fieldPage = addFormField(FormFieldAnswerType.FREE_TEXT_LINE,YesNoList.YES.toString());

        ContentService.updateForm(formTitle,fieldPage);
        Navigator.contentTabGeneralForms();
        String input = null;
        try {
            String url = ContentService.extractFormUrl(formTitle);
            Assert.assertTrue("Web form was not opened", ContentService.openFormInWeb(url));

            input = "This is a test for answet type:" + fieldPage.getAnswerType();
            ContentService.fillForm(input ,fieldPage.getAnswerType());
            ContentService.submitForm(formPage);
            closeFormTabAndDeleteForm(formTitle);
            Assert.assertTrue("Recipient did not get the mail sent by the form",ContentService.checkMail(Arrays.asList(input),formTitle));
        } catch (Exception e) {
            recoverFromException(e);
        }


    }



    @Test
    public void testCheckExportForm() throws Exception {

        //create form
        FormPage formPage = HubFormsCreator.createForm(timestamp,HubFormsCreator.AUTO_TITLE_PREFIX, HubFormsCreator.AUTO_RECIPIENTS,
                HubFormsCreator.AUTO_THANK_YOU_TITLE_PREFIX, HubFormsCreator.AUTO_THANK_YOU_CONTENT_PREFIX, HubFormsCreator.AUTO_DESCRIPTION_PREFIX);
        String formTitle = formPage.getTitle();

        //add field
        FormFieldPage fieldPage1 = addFormField(FormFieldAnswerType.FREE_TEXT_LINE,YesNoList.YES.toString());
        ContentService.updateForm(formTitle,fieldPage1);
        FormFieldPage fieldPage2 = addFormField(FormFieldAnswerType.FREE_TEXT_BOX,YesNoList.YES.toString());
        ContentService.addFormField(fieldPage2);
        FormFieldPage fieldPage3 = addFormField(FormFieldAnswerType.CHECK_BOX,YesNoList.YES.toString());
        ContentService.addFormField(fieldPage3);


        try {
            Navigator.contentTabGeneralForms();
            String url = ContentService.extractFormUrl(formTitle);
            Assert.assertTrue("Web form was not opened", ContentService.openFormInWeb(url));

            String input1 = "This is a test for answer type:" + fieldPage1.getAnswerType();
            String input2 = "This is a test for answer type:" + fieldPage2.getAnswerType();
            String input3 = "Y";
            ContentService.fillForm(input1 ,fieldPage1.getAnswerType());
            ContentService.fillForm(input2 ,fieldPage2.getAnswerType());
            ContentService.fillForm(input3 ,fieldPage3.getAnswerType());

            ContentService.submitForm(formPage);

            ContentService.closeFormTab();
            String resultsUrl = ContentService.exportResults(formTitle);
            Assert.assertTrue("Export results failed",  ContentService.readResults(resultsUrl, Lists.newArrayList(input1,input2,input3)));

            ContentService.deleteForm(formTitle);
        } catch (Exception e) {
            recoverFromException(e);
        }
    }

    private void recoverFromException(Exception e) {
        log4j.error("Error while open form");
        ContentService.closeFormTab();
        Assert.fail(e.getMessage());
    }


    //Check the given field according to the field type - check that was created as expected in the form
    private void checkField(FormFieldPage fieldPage,String fieldType) {
        Assert.assertTrue("Field " + fieldPage.getTitle() + " not as expected", ContentService.checkFormField(FormFieldsConstants.FIELD_TITLE_PATH,fieldPage.getTitle(),null));
        Assert.assertTrue("Field " + fieldPage.getDescription() + " not as expected",ContentService.checkFormField(FormFieldsConstants.FIELD_DESCRIPTION_PATH,fieldPage.getDescription(),null));
        Assert.assertTrue("Field " + fieldPage.getIsMandatory() + " not as expected",ContentService.checkFormField(FormFieldsConstants.FIELD_MANDATORY_PATH,fieldPage.getIsMandatory(),null));
        Assert.assertTrue("Field " + fieldPage.getAnswerType() + " not as expected",ContentService.checkFormField(FormFieldsConstants.FIELD_ANSWER_TYPE,fieldPage.getAnswerType(),fieldType));
    }

    //Add the given field to the form (according to the given Field type
    private FormFieldPage addFormField(FormFieldAnswerType fieldType,String mandatory) {
        return HubFormsCreator.addFormField(timestamp.toString(),fieldType,mandatory);
    }

    //Close the form url tab and delete the form from the general form page
    private void closeFormTabAndDeleteForm(String formTitle) throws Exception {
        ContentService.closeFormTab();

        ContentService.deleteForm(formTitle);
    }

    @AfterClass
    public static void cleanResources(){
        ContentService.deleteGeneratedFiles();
    }


}
