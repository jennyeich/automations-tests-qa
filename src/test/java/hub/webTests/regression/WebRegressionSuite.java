package hub.webTests.regression;

import hub.base.BaseHubTest;
import hub.services.AcpService;
import hub.services.NewApplicationService;
import integration.base.BaseIntegrationTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.IOException;

/**
 * Created by Goni on 6/12/2017.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        WebSanityTests.class,
        OldAutomationsTest.class,
        HubLogsTest.class,
        GeneralFormsTest.class,
        SurveyTest.class,
        RegistrationFormTest.class,
        SendBulkEmailTest.class,
        RemovePIIDataTest.class,
        DefaultUnsubscribeTest.class,
        UnsubscribeTest.class,
        FriendReferralCodeTest.class

})

public class WebRegressionSuite {
    public static final Logger log4j = Logger.getLogger(WebRegressionSuite.class);


    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseIntegrationTest.init();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Remove application from user");
        try {
            AcpService.uncheckApplication(NewApplicationService.getEnvProperties().getLocationId());
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
        log4j.info("close browser after finish test");
        BaseIntegrationTest.cleanBase();
        BaseHubTest.cleanHub();
    }

}
