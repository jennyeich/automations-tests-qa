package hub.webTests.regression;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import hub.common.objects.benefits.LotteryReward;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.SmartPunchCard;
import hub.hub1_0.services.Navigator;
import hub.services.member.MembersService;
import hub.utils.HubMemberCreator;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.utils.UserActionsConstants;

import java.text.MessageFormat;
import java.util.Arrays;

/**
 * Created by Goni on 6/11/2017.
 */
public class HubLogsTest extends BaseIntegrationTest {


    private static CharSequence timestamp;
    private static NewMember member;
    private static Key key;
    private static SmartGift smartGift;


    @BeforeClass
    public static void setup() throws Exception {
        init();
        //Create member
        member = HubMemberCreator.createMemberWithGovId(AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        log4j.info("member with phone: " + member.getPhoneNumber());
        //Get member userKey and turn into User_KEY
        key = HubMemberCreator.getMemberKeyToUserKey();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            if(smartGift == null) {
                //create assets
                try {
                    smartGift = hubAssetsCreator.createSmartGift(timestamp);
                } catch (Exception e) {
                    log4j.error("error while creating asset",e);
                }
            }
        }
    };

    //C4711
    @Test
    public void testAssetSourceInLogWhenSendingAssetFromHub() throws Exception {

        MembersService.tryFindAndGetMemberByPhoneNumber(member);
        MembersService.performSmartActionSendAnAsset(smartGift, timestamp);

        //Verify the gift was sent
        Assert.assertTrue("The member did not get the gift in the HUB", MembersService.isGiftExists(smartGift.getTitle()));
        log4j.info("The member got the gift sent by the HUB");


        String expectedMsg = "from Hub";
        String returnedLog = MembersService.getLogAppears(expectedMsg);
        Assert.assertTrue("Log is not as expected", contains(returnedLog, expectedMsg));

    }


    //C4714
    @Test
    public void testAssetNameInLogWhenSendingAssetFromHub() throws Exception {

        MembersService.tryFindAndGetMemberByPhoneNumber(member);
        MembersService.performSmartActionSendAnAsset(smartGift, timestamp);

        //Verify the gift was sent
        Assert.assertTrue("The member did not get the gift in the HUB", MembersService.isGiftExists(smartGift.getTitle()));
        log4j.info("The member got the gift sent by the HUB");

        String expectedMsg = MessageFormat.format("Received a new gift \"{0}\"",smartGift.getTitle());
        String returnedLog = MembersService.getLogAppears(expectedMsg);
        Assert.assertTrue("Log is not as expected", contains(returnedLog, expectedMsg));
    }


    @Test
    public void testPointsAmountInLogWhenPointAddedFromHub() throws Exception {

        member = HubMemberCreator.createMemberWithGovId(AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        log4j.info("member with phone: " + member.getPhoneNumber());
        //Get member userKey and turn into User_KEY
        key = HubMemberCreator.getMemberKeyToUserKey();
        String actionTag = MembersService.performSmartActionAddPoints("3333", timestamp);

        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);
        Navigator.refresh();
        log4j.info("The member got the points sent by the HUB");

        String expectedMsg = MessageFormat.format("Point transaction performed using the operation \"AddPoints\" (amount:{0})","3,333.00");
        String returnedLog = MembersService.getLogAppears(expectedMsg);
        Assert.assertTrue("Add points through the hub log is not as expected returnedLog: " + returnedLog + " ,expectedMsg: " + expectedMsg, contains(returnedLog, expectedMsg));
    }

    @Test
    public void testPointsAmountInLogWhenPointReducedFromHub() throws Exception {

        member = HubMemberCreator.createMemberWithGovId(AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        log4j.info("member with phone: " + member.getPhoneNumber());
        String actionTag = MembersService.performSmartActionAddPoints("3333", timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);
        timestamp = String.valueOf(System.currentTimeMillis());
        actionTag = MembersService.performSmartActionAddPoints("-1111", timestamp);
        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);
        Navigator.refresh();

        String expectedMsg = MessageFormat.format("Point transaction performed using the operation \"AddPoints\" (amount:{0})","-1,111.00");
        String returnedLog = MembersService.getLogAppears(expectedMsg);
        Assert.assertTrue("Add points through the hub log is not as expected returnedLog: " + returnedLog + " ,expectedMsg: " + expectedMsg, contains(returnedLog, expectedMsg));

    }

    //C4712
    @Test
    public void testAssetNameInLogWhenRedeemGiftFromHub() throws Exception {

        MembersService.tryFindAndGetMemberByPhoneNumber(member);
        MembersService.performSmartActionSendAnAsset(smartGift, timestamp);

        //Verify the gift was sent
        Assert.assertTrue("The member did not get the gift in the HUB", MembersService.isGiftExists(smartGift.getTitle()));
        log4j.info("The member got the gift sent by the HUB");

        MembersService.performAnActionReedem();
        MembersService.refreshHubPage();
        String expectedMsg = MessageFormat.format("Redeemed the gift \"{0}\"" ,smartGift.getTitle());
        //wait for update logs
        Thread.sleep(5000);
        String returnedLog = MembersService.getLogAppears(expectedMsg);
        Assert.assertTrue("Log is not as expected", contains(returnedLog, expectedMsg));

    }


    @Test
    public void testAddCreditFromHub() throws Exception{

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String giveCredit = "100";
        String actionTag = MembersService.performSmartActionAddCredit(giveCredit,timestamp);

        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        MembersService.refreshHubPage();
        String expectedMsg = MessageFormat.format("Point transaction performed using the operation \"AddPoints\" (amount:{0})",giveCredit + ".00");
        String returnedLog = MembersService.getLogAppears(expectedMsg);
        Assert.assertTrue("Add points through the hub log is not as expected returnedLog: " + returnedLog + " ,expectedMsg: " + expectedMsg, contains(returnedLog, expectedMsg));
    }


    @Test
    public void testTagMemberFromHub() throws Exception{

        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String [] array = MembersService.performSmartActionTagMember(timestamp);
        MembersService.refreshHubPage();

        String expectedMsg = MessageFormat.format("Tag operation \"Tag\" with tag \"{0}\"",array[0]);
        String returnedLog = MembersService.getLogAppears(expectedMsg);
        Assert.assertTrue("The hub log must contains tag member", contains(returnedLog, expectedMsg));

    }


    @Test
    public void testUpdateExpirationDateActionFromHub() throws Exception{

        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        MembersService.performSmartActionUpdateExpirationDate(timestamp);

        MembersService.refreshHubPage();
        String expectedMsg = "Updated membership details";
        String returnedLog = MembersService.getLogAppears(expectedMsg);
        log4j.info("Member log contains: " + returnedLog);
        Assert.assertTrue("The hub log must contains Updated Membership Details", contains(returnedLog, expectedMsg));

    }



    @Test
    public void testSendLotteryRewardFromHub() throws Exception{

        SmartGift smartGift = hubAssetsCreator.createSmartGift(timestamp);
        LotteryReward lotteryReward = hubAssetsCreator.createLotteryReward("temp" + timestamp,smartGift.getTitle(),"1",null);
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        MembersService.performSmartActionSendLotteryReward(lotteryReward,timestamp);

        MembersService.refreshHubPage();

        String expectedMsg = MessageFormat.format("Received a new gift \"{0}\" from Hub",smartGift.getTitle());
        String returnedLog = MembersService.getLogAppears(expectedMsg);
        Assert.assertTrue("The hub log must contains Received a new gift from hub", contains(returnedLog, expectedMsg));

    }


    @Test
    public void testPunchAPunchCardFromHub() throws Exception{

        SmartPunchCard punchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "4");
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String actionTag = MembersService.performSmartActionSendAPunchCard(punchCard,timestamp);
        timestamp = String.valueOf(System.currentTimeMillis());
        actionTag = MembersService.performSmartActionPunchAPunchCard(punchCard,timestamp);
        checkUserAction(UserActionsConstants.PUNCH,actionTag);

        MembersService.refreshHubPage();
        String lastLog = MembersService.getLastLog();
        String expectedMsg = "Punched 1 punches in \""+ punchCard.getTitle() +"\" (Total punches is 1)";
        log4j.info("Member log contains: " + lastLog);
        Assert.assertTrue("The hub log must contains the punch card action", contains(lastLog, expectedMsg));

    }

    private void checkUserAction(String action,String actionTag) throws Exception {

        Query q = new Query("UserAction");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, action),
                new Query.FilterPredicate("ActionTags", Query.FilterOperator.EQUAL,actionTag))));
        log4j.info("Check member get the point transaction in datastore: ActionTags =" +actionTag );
        serverHelper.queryWithWait("Member did not get the points ",q,1,dataStore);
    }


    private boolean contains(String returnedLog, String expectedMsg){
        return  returnedLog.replace(" ","").contains(expectedMsg.replace(" ",""));
    }
}
