package hub.webTests.regression;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.NewMember;
import hub.common.objects.operations.ExternalServicesPage;
import hub.hub1_0.services.OperationService;
import hub.services.ContentService;
import hub.services.DataBIService;
import hub.services.member.MembersLog;
import hub.services.member.MembersService;
import hub.utils.HubTemplateCreator;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import utils.PropsReader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SendBulkEmailTest extends BaseIntegrationTest{

    public static final String THIS_IS_AN_ADVERTISEMENT = "This is an advertisement";
    public static final String BUSSINESS_EMAIL_ADDRESS = PropsReader.getPropValuesForEnv("user");
    public static final String UNSUBSCRIBE_TEXT = "If you have received this email in error or would like to stop receiving emails like this, press";
    public static final String UNSUBSCRIBE_TEXT_2 = String.format("or email %s to", locationId);



    public static final String downloadsDir = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" +
            File.separator + "downloads" + File.separator;

    String timestamp;
    String templateName;
    String tag;
    NewMember member;

    @BeforeClass
    public static void setup() throws IOException {
       init();
    }


    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){
        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = "sendEmail_" +timestamp;
            templateName = "template_" + timestamp;
            member = hubMemberCreator.buildMember(timestamp, "testBulkFN", "testBulkLN");
            member.setEmail("automationcomo+" + timestamp + "@gmail.com");
            member.setPhoneNumber(timestamp);
            try {
                MembersService.createNewMember(member);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Test
    @Category(Hub1Regression.class)
    public void testSendEmailFromFindMember() throws Exception{
        HubTemplateCreator.createImageTemplate(templateName);
        MembersService.findMembersByFirstName(member.getFirstName());
        MembersService.performSmartActionSendEmail(templateName);
        FindMember findMember = new FindMember();
        findMember.setFirstName(member.getFirstName());
        MembersService.findAndGetMember(findMember);
        Assert.assertTrue("Email sent log not found in member profile.", MembersService.isLogAppears(MembersLog.EMAIL_SENT_MESSAGE(templateName, "Hub")));
        Assert.assertTrue("Email reached inbox log not found in member profile.", MembersService.isLogAppears(MembersLog.EMAIL_REACHED_MESSAGE(templateName)));
        checkEmail(Collections.emptyList(), String.format(HubTemplateCreator.DEFAULT_MAIL_SUBJECT, member.getFirstName(), member.getLastName()));
    }

    @Test
    public void testSendEmailImportUserKeys() throws Exception{
        HubTemplateCreator.createImageTemplate(templateName);
        MembersService.tryFindAndGetMemberByPhoneNumber(member);
        String membershipKey = MembersService.getUserKey();
        StringBuffer stringBuffer = new StringBuffer(membershipKey);
        DataBIService.importUserKeys(stringBuffer);
        DataBIService.clickLastCreatedUsersKeysList();

        MembersService.performSmartActionSendEmail(templateName);
        Thread.sleep(20000);
        checkEmail(Collections.emptyList(), String.format(HubTemplateCreator.DEFAULT_MAIL_SUBJECT, member.getFirstName(), member.getLastName()));
    }

    @Test
    public void testSendEmailFooter() throws Exception{
        HubTemplateCreator.createCodeTemplate(templateName);
        findMemberAndSendMail();

        List<String> footerTexts = Arrays.asList(THIS_IS_AN_ADVERTISEMENT, ">" + applicationName + "<", BUSSINESS_EMAIL_ADDRESS,
                UNSUBSCRIBE_TEXT, UNSUBSCRIBE_TEXT_2);
        checkEmail(footerTexts, String.format(HubTemplateCreator.DEFAULT_MAIL_SUBJECT, member.getFirstName(), member.getLastName()));


//        The "Marketing email" checkbox was removed until "Operational email" will be developed.
//        This part of the test is not relevant till then

//        templateName = templateName + "_";
//        HubTemplateCreator.createCodeTemplate(templateName, HubTemplateCreator.SUBJECT, HubTemplateCreator.HTML_FILE_PATH, false);
//        findMemberAndSendMail();
//
//        boolean mailPassed = ContentService.checkMail(Collections.emptyList(), String.format(HubTemplateCreator.DEFAULT_MAIL_SUBJECT, member.getFirstName(), member.getLastName()));
//        if(mailPassed)
//            Assert.fail("Email contained the subject prefix 'Advertisement' but expected not to.");
//
//        mailPassed = ContentService.checkMail(footerTexts, String.format("Hi %s %s !", member.getFirstName(), member.getLastName()), false);
//        if(!mailPassed)
//            Assert.fail("Email contained the footer text but expected not to.");
    }

    @Test
    @Category(Hub1Regression.class)
    public void testSendEmailSettings() throws Exception {
        HubTemplateCreator.createImageTemplate(templateName);

        String fromName = "Kipi-Ben-Kipod";
        String address = "21 Sesame street Rehovot";
        ExternalServicesPage externalServicesPage = new ExternalServicesPage();
        externalServicesPage.setFromName(fromName);
        externalServicesPage.setBusinessAddress(address);
        externalServicesPage.setComoDomainPrefix("no-reply-kipi");
        externalServicesPage.clickSave();
        OperationService.setExternalSettings(externalServicesPage);

        findMemberAndSendMail();

        //Change "from name" back to application name.
        ExternalServicesPage externalServicesPage2 = new ExternalServicesPage();
        externalServicesPage2.setFromName(applicationName);
        externalServicesPage2.setComoDomainPrefix("no-reply-" + locationId);
        externalServicesPage2.clickSave();
        OperationService.setExternalSettings(externalServicesPage2);

        List<String> footerTexts = Arrays.asList("From: \"Kipi-Ben-Kipod\" <no-reply-kipi@qa-como.com>", THIS_IS_AN_ADVERTISEMENT, ">" + fromName + "<", address, BUSSINESS_EMAIL_ADDRESS,
                UNSUBSCRIBE_TEXT, UNSUBSCRIBE_TEXT_2);
        checkEmail(footerTexts, String.format(HubTemplateCreator.DEFAULT_MAIL_SUBJECT, member.getFirstName(), member.getLastName()));
    }

    @Test
    public void testStatistics() throws Exception{
        List<String> statisticsInputs = new ArrayList<>();
        statisticsInputs.add("Event Type\",\"Total");
        statisticsInputs.add("\"Total Filtered Members\",1");
        statisticsInputs.add("\"Sent\",1");
        statisticsInputs.add("\"Delivered\",1");
        statisticsInputs.add("\"Delivered Rate\",\"100%\"");
        statisticsInputs.add("\"Spam Complaint\",0");
        statisticsInputs.add("\"Click\",0");
        statisticsInputs.add("\"Click Rate\",\"0%\"");
        statisticsInputs.add("\"Open\",0");
        statisticsInputs.add("\"Open Rate\",\"0%\"");
        statisticsInputs.add("\"Unsubscribe\",0");
        statisticsInputs.add("\"Bounce\",0");
        statisticsInputs.add("\"Rejected by email provider\",0");

        HubTemplateCreator.createImageTemplate(templateName);
        findMemberAndSendMail();
        File file = new File(downloadsDir + "Report.csv");
        ContentService.downloadTemplateStatisticsFile(templateName, file);
        //TODO: enable the following assertion when file can be read from file system in linux
//        Assert.assertTrue(ContentService.checkTemplateStatistics(file, statisticsInputs));
    }


    private void findMemberAndSendMail()  throws Exception{
        MembersService.findMembersByFirstName(member.getFirstName());
        MembersService.performSmartActionSendEmail(templateName);
        Thread.sleep(25000);
    }


    private void checkEmail(List contents, String subject) throws Exception {
        boolean result = ContentService.checkMail(contents, subject);
        if (!result) {
            Assert.fail("EMail didnt contain the expected subject and/or content.");
        }
    }
}
