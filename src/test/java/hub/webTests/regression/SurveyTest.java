package hub.webTests.regression;

import hub.base.basePages.BaseContentPage;
import hub.common.objects.common.FormFieldAnswerType;
import hub.common.objects.content.forms.MainSurveyPage;
import hub.common.objects.content.forms.SurveyPage;
import hub.common.objects.content.forms.SurveyQuestionPage;
import hub.services.ContentService;
import hub.utils.HubFormsCreator;
import integration.base.BaseIntegrationTest;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.io.IOException;

/**
 * Created by Goni on 6/18/2017.
 */
public class SurveyTest extends BaseIntegrationTest {

    public static final String AUTO_TITLE_PREFIX = "auto_title";
    public static final String AUTO_DESCRIPTION_PREFIX = "auto_description";
    public static final String AUTO_RECIPIENTS = "automationcomo@gmail.com";
    public static final String AUTO_THANK_YOU_TITLE_PREFIX = "auto_thank_you_title";
    public static final String AUTO_THANK_YOU_CONTENT_PREFIX = "auto_thank_you_message";

    public CharSequence timestamp;

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());

        }
    };

    @BeforeClass
    public static void setup() throws IOException {
        init();
    }


    //C2353
    @Test
    public void testCreateNewSurvey() throws Exception {
        SurveyPage surveyPage =  HubFormsCreator.createSurvey(timestamp.toString(),AUTO_TITLE_PREFIX, AUTO_RECIPIENTS, AUTO_THANK_YOU_TITLE_PREFIX, AUTO_THANK_YOU_CONTENT_PREFIX, AUTO_DESCRIPTION_PREFIX);
        String surveyTitle = surveyPage.getTitle();
        Assert.assertTrue("Survey with title: " + surveyTitle + " doesn't exist", ContentService.isFormCreated(surveyTitle, MainSurveyPage.SURVEY_TITLE));

    }

    @Test
    public void testCreateNewSurveyCheckUrl() throws Exception {

        SurveyPage surveyPage = HubFormsCreator.createSurvey(timestamp.toString(),AUTO_TITLE_PREFIX, AUTO_RECIPIENTS, AUTO_THANK_YOU_TITLE_PREFIX, AUTO_THANK_YOU_CONTENT_PREFIX, AUTO_DESCRIPTION_PREFIX);
        String title = surveyPage.getTitle();
        Assert.assertTrue("Survey with title: " + title + " doesn't exist", ContentService.isFormCreated(title,MainSurveyPage.SURVEY_TITLE));

        String url = ContentService.extractFormUrl(title,MainSurveyPage.SURVEY_TITLE,MainSurveyPage.SURVEY_URL);

        Assert.assertTrue("Web survey was not opened", ContentService.openFormInWeb(url, BaseContentPage.SurveyStartButton));

        Assert.assertTrue("Incorrect survey was opened", ContentService.isCorrectSurveyOpenInWeb(title));

        closeFormTabAndDeleteForm(title);
    }

//TODO - finish tests
    /*
    @Test
    public void testQuestionTextInWebLinkSurvey() throws Exception {

        //create survey
        SurveyPage surveyPage = HubFormsCreator.createSurvey(timestamp.toString(),AUTO_TITLE_PREFIX, AUTO_RECIPIENTS, AUTO_THANK_YOU_TITLE_PREFIX, AUTO_THANK_YOU_CONTENT_PREFIX, AUTO_DESCRIPTION_PREFIX);
        String title = surveyPage.getTitle();

        //add question
        SurveyQuestionPage questionPage = addQuestion(FormFieldAnswerType.FREE_TEXT_LINE, YesNoList.YES.toString());
        ContentService.updateSurvey(title,questionPage);

        Navigator.contentTabSurvey();
        String url = ContentService.extractFormUrl(title,MainSurveyPage.SURVEY_TITLE,MainSurveyPage.SURVEY_URL);

        Assert.assertTrue("Web survey was not opened", ContentService.openFormInWeb(url,BaseContentPage.SurveyStartButton));
        ContentService.startSurvey();

        checkQuestion(questionPage,SurveyQuestionPage.TEXT);

        closeFormTabAndDeleteForm(title);
    }

    @Test
    public void testQuestionCheckBoxInWebLinkSurvey() throws Exception {

        //create survey
        SurveyPage surveyPage = HubFormsCreator.createSurvey(timestamp.toString(),AUTO_TITLE_PREFIX, AUTO_RECIPIENTS, AUTO_THANK_YOU_TITLE_PREFIX, AUTO_THANK_YOU_CONTENT_PREFIX, AUTO_DESCRIPTION_PREFIX);
        String title = surveyPage.getTitle();

        //add question
        SurveyQuestionPage questionPage = addQuestion(FormFieldAnswerType.CHECK_BOX, YesNoList.YES.toString());
        ContentService.updateSurvey(title,questionPage);

        Navigator.contentTabSurvey();
        String url = ContentService.extractFormUrl(title,MainSurveyPage.SURVEY_TITLE,MainSurveyPage.SURVEY_URL);

        Assert.assertTrue("Web survey was not opened", ContentService.openFormInWeb(url,BaseContentPage.SurveyStartButton));
        ContentService.startSurvey();

        checkQuestion(questionPage, SurveyQuestionPage.CHECKBOX);

        closeFormTabAndDeleteForm(title);
    }

    @Test
    public void testQuestionMultiSelectOneChoiceInWebLinkSurvey() throws Exception {

        //create survey
        SurveyPage surveyPage = HubFormsCreator.createSurvey(timestamp.toString(),AUTO_TITLE_PREFIX, AUTO_RECIPIENTS, AUTO_THANK_YOU_TITLE_PREFIX, AUTO_THANK_YOU_CONTENT_PREFIX, AUTO_DESCRIPTION_PREFIX);
        String title = surveyPage.getTitle();

        //add question
        SurveyQuestionPage questionPage = addQuestion(FormFieldAnswerType.MULTIPLE_CHOICE_ONE_ANSWER, YesNoList.NO.toString());
        ContentService.updateSurvey(title,questionPage);

        Navigator.contentTabSurvey();
        String url = ContentService.extractFormUrl(title,MainSurveyPage.SURVEY_TITLE,MainSurveyPage.SURVEY_URL);

        Assert.assertTrue("Web survey was not opened", ContentService.openFormInWeb(url,BaseContentPage.SurveyStartButton));
        ContentService.startSurvey();

        checkQuestion(questionPage,SurveyQuestionPage.MULTI_SELECT_ONE_CHOICE);

        closeFormTabAndDeleteForm(title);
    }

    @Test
    public void testCheckMailWebLinkForm() throws Exception {

        //create survey
        SurveyPage surveyPage = HubFormsCreator.createSurvey(timestamp.toString(),AUTO_TITLE_PREFIX, AUTO_RECIPIENTS, AUTO_THANK_YOU_TITLE_PREFIX, AUTO_THANK_YOU_CONTENT_PREFIX, AUTO_DESCRIPTION_PREFIX);
        String title = surveyPage.getTitle();

        //add question
        SurveyQuestionPage questionPage = addQuestion(FormFieldAnswerType.FREE_TEXT_LINE, YesNoList.YES.toString());
        ContentService.updateSurvey(title,questionPage);

        ContentService.updateSurvey(title,questionPage);

        Navigator.contentTabSurvey();
        String url = ContentService.extractFormUrl(title,MainSurveyPage.SURVEY_TITLE,MainSurveyPage.SURVEY_URL);

        Assert.assertTrue("Web survey was not opened", ContentService.openFormInWeb(url,BaseContentPage.SurveyStartButton));
        ContentService.startSurvey();

        String input = "This is a test for answer type:" + questionPage.getAnswerType();
        ContentService.fillForm(input ,questionPage.getAnswerType());
        if(ContentService.checkIfSurveyAnswered(surveyPage))
            Assert.assertTrue("Recipient did not get the mail sent by the form",ContentService.checkMail(input,title));

        closeFormTabAndDeleteForm(title);
    }
    @Test
    public void testCheckExportForm() throws Exception {

        //create survey
        SurveyPage surveyPage = HubFormsCreator.createSurvey(timestamp.toString(),AUTO_TITLE_PREFIX, AUTO_RECIPIENTS, AUTO_THANK_YOU_TITLE_PREFIX, AUTO_THANK_YOU_CONTENT_PREFIX, AUTO_DESCRIPTION_PREFIX);
        String title = surveyPage.getTitle();

        //add questions
        SurveyQuestionPage questionPage1 = addQuestion(FormFieldAnswerType.FREE_TEXT_LINE, YesNoList.YES.toString());
        ContentService.updateSurvey(title,questionPage1);
        SurveyQuestionPage questionPage2 = addQuestion(FormFieldAnswerType.FREE_TEXT_BOX,YesNoList.YES.toString());
        ContentService.createQuestion(questionPage2);
        SurveyQuestionPage questionPage3 = addQuestion(FormFieldAnswerType.CHECK_BOX,YesNoList.YES.toString());
        ContentService.createQuestion(questionPage3);


        Navigator.contentTabSurvey();
        String url = ContentService.extractFormUrl(title,MainSurveyPage.SURVEY_TITLE,MainSurveyPage.SURVEY_URL);

        Assert.assertTrue("Web survey was not opened", ContentService.openFormInWeb(url,BaseContentPage.SurveyStartButton));
        ContentService.startSurvey();

        String input1 = "This is a test for answer type:" + questionPage1.getAnswerType();
        String input2 = "This is a test for answer type:" + questionPage2.getAnswerType();
        String input3 = "Y";
        ContentService.updateSurvey(input1 ,questionPage1);
        ContentService.updateSurvey(input2 ,questionPage2);
        ContentService.updateSurvey(input3 ,questionPage3);

        Assert.assertTrue("Survey was not completely fullfilled", ContentService.checkIfSurveyAnswered(surveyPage));

        ContentService.closeFormTab();
        String resultsUrl = ContentService.exportResults(title,MainSurveyPage.SURVEY_TITLE);
        Assert.assertTrue("Export results failed",  ContentService.readResults(resultsUrl, Lists.newArrayList(input1,input2,input3)));

        ContentService.deleteForm(title,MainSurveyPage.SURVEY_TITLE);
    }

*/

    //Check the given question according to the field type - check that was created as expected in the survey
    private void checkQuestion(SurveyQuestionPage questionPage,String fieldType) throws Exception{
        Assert.assertTrue("Question " + questionPage.getAnswerType() + " not as expected",ContentService.checkQuestion(questionPage,fieldType));
    }


    //Add the given question to the survey (according to the given Field type)
    private SurveyQuestionPage addQuestion(FormFieldAnswerType fieldType,String mandatory) {
        return HubFormsCreator.addQuestion(timestamp.toString(),fieldType,mandatory);
    }

    //Close the form url tab and delete the form from the general form page
    private void closeFormTabAndDeleteForm(String formTitle) throws Exception {
        ContentService.closeFormTab();

        ContentService.deleteFormFromSurvey(formTitle, MainSurveyPage.SURVEY_TITLE);
    }

    @AfterClass
    public static void cleanResources(){
        ContentService.deleteGeneratedFiles();
    }

}
