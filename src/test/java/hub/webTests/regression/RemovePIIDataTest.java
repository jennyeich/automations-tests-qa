package hub.webTests.regression;

import hub.common.objects.common.RegistrationField;
import hub.common.objects.common.YesNoListNumber;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.hub1_0.services.OperationService;
import hub.services.member.MembersService;
import hub.utils.HubMemberCreator;
import integration.base.BaseIntegrationTest;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Jenny on 31/01/2018.
 */
public class RemovePIIDataTest extends BaseIntegrationTest {


    private static CharSequence timestamp;

    @BeforeClass
    public static void setup() throws Exception {
        init();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());

        }
    };

    //C9470 Remove PII data (Personally identifiable information) - CNP 9501 - Automated
    //Also, CNP - 9502 - Remove PII data - CP - UX improvements - CNP - 9502 - Automated
    @Test
    public void testRemovePIIData() throws Exception {

        createMemberWithAllFields ();
        MembersService.deleteExistingMember ();
        NewMember existingMember = MembersService.getMemberFields();
        checkDeletedMember(existingMember);

        //check the log
        String expectedMsg = "Member has been deleted";
        String returnedLog = MembersService.getLogAppears(expectedMsg);
        Assert.assertTrue("Log is not as expected",returnedLog.contains(expectedMsg));

        expectedMsg = "Logged out";
        returnedLog = MembersService.getLogAppears(expectedMsg);
        Assert.assertTrue("Log is not as expected",returnedLog.contains(expectedMsg));

        expectedMsg = "Unsubscribed receiving email";
        returnedLog = MembersService.getLogAppears(expectedMsg);
        Assert.assertTrue("Log is not as expected",returnedLog.contains(expectedMsg));

        expectedMsg = "Unsubscribed receiving sms";
        returnedLog = MembersService.getLogAppears(expectedMsg);
        Assert.assertTrue("Log is not as expected",returnedLog.contains(expectedMsg));

        checkButtonsDisabled();

    }


    private void checkDeletedMember (NewMember existingMember) {

        Assert.assertTrue("Member firstName was not cleared",existingMember.getFirstName().startsWith("deleted"));
        Assert.assertTrue("Member lastName was not cleared",existingMember.getLastName().startsWith("deleted"));
        Assert.assertTrue("Member phone number was not deleted" ,  existingMember.getPhoneNumber().startsWith("deleted"));
        Assert.assertTrue("Member official ID Number was not deleted" ,  existingMember.getGovId().startsWith("deleted"));
        Assert.assertTrue("External ID Number was not deleted" ,  existingMember.getExtClubID().startsWith("deleted"));
        Assert.assertTrue("Address floor number was not deleted" ,  existingMember.getAddressFloor().startsWith("deleted"));
        Assert.assertTrue("Address floor home was not deleted" ,  existingMember.getAddressHome().startsWith("deleted"));
        Assert.assertTrue("Email was not deleted" ,  existingMember.getEmail().startsWith("deleted"));
        Assert.assertTrue("Address street was not deleted" ,  existingMember.getAddressStreet().startsWith("deleted"));
        Assert.assertTrue("Address line1 was not deleted" ,  existingMember.getAddressLine1().startsWith("deleted"));
        Assert.assertTrue("Address line2 was not deleted" ,  existingMember.getAddressLine2().startsWith("deleted"));
        Assert.assertTrue("Generic String1 was not deleted" ,  existingMember.getGenericString1().startsWith("deleted"));
    }

    private NewMember createMemberWithAllFields () throws Exception {

        NewMember member;
        List<RegistrationField> fields = Arrays.asList(RegistrationField.OfficialIDNumber, RegistrationField.extClubID, RegistrationField.addressFloor, RegistrationField.addressHome, RegistrationField.addressStreet, RegistrationField.addressLine1, RegistrationField.addressLine2,RegistrationField.CustomText1);
        OperationService.addRequiredFieldsToRegistrationForm(fields, YesNoListNumber.NO.toString());

        //Create member
        member = HubMemberCreator.createMemberWithAllPIIFields(AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        log4j.info("member with phone: " + member.getPhoneNumber());

        return member;

    }

    private void checkButtonsDisabled () throws Exception {

        MemberDetails memberDetails = MembersService.getDeletedMemberDetails();

        Assert.assertTrue(String.format("AllowSMS expected to be %s, but actually was %s. ", "No", memberDetails.getAllowSMS()), "No".equals(memberDetails.getAllowSMS()));
        Assert.assertTrue(String.format("AllowEmail checkbox expected to be %s, but actually was %s. ", "No", memberDetails.getAllowEmail()), "No".equals(memberDetails.getAllowEmail()));
        Assert.assertEquals("Unsubscribe sms button state is not as expected.","",MembersService.getMemberUnsubscribeSMS());
        Assert.assertEquals("Unsubscribe email button state is not as expected.","",MembersService.getMemberUnsubscribeEmail());
        Assert.assertEquals("Disconnect user button state is not as expected.","",MembersService.getMemberDisconnectUser());
        Assert.assertEquals("Delete user button state is not as expected.","",MembersService.getMemberDeleteUser());
        Assert.assertEquals("Update button state is not as expected.","",MembersService.getMemberUpdateButton());
    }

}
