package hub.webTests.features;

import hub.base.BaseHubTest;
import hub.base.basePages.BaseContentPage;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.PermissionLevel;
import hub.common.objects.common.Tags;
import hub.common.objects.member.FilterMember;
import hub.common.objects.member.FindMember;
import hub.common.objects.operations.UserPermissions;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.common.pages.DashboardPage;
import hub.common.pages.HomeScreenPage;
import hub.hub1_0.services.Navigator;
import hub.hub1_0.services.OperationService;
import hub.services.AcpService;
import hub.services.DashboardService;
import hub.services.FilterMembersService;
import hub.services.HomeScreenService;
import integration.base.BaseIntegrationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;

import java.io.IOException;


/**
 * Created by lior on 7/4/17.
 */
public class PermissionTagsTest extends BaseIntegrationTest {

    @BeforeClass
    public static void setup() throws IOException {
        init(true);
        log4j.info("HUB login succeeded...");
    }




    /******************************************
     CNP-6321 - Find members
     /****************************************/

    @Test
    @Category(Hub1Regression.class)
    public void testPermissionTagForExtraIdentifiersInFindMembers() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.FIND_USERS_FIELD_DELETE);
        Navigator.dataAndBIPageFindMember();
        Assert.assertTrue("Element not exist " + FindMember.EXTRA_IDENTIFIERS, AcpService.isElementExist(FindMember.EXTRA_IDENTIFIERS));
        AcpService.setPermissionTag(Tags.EXTRA_IDENTIFIERS.toString(),PermissionLevel.DISALLOW.toString());
        Navigator.dataAndBIPageFindMember();
        Assert.assertFalse("Element exist " + FindMember.EXTRA_IDENTIFIERS, AcpService.isElementExist(FindMember.EXTRA_IDENTIFIERS));
        Navigator.acpTabManageComoHubUsersUpdateUserCurrentUser();
        AcpService.setPermissionTag(Tags.EXTRA_IDENTIFIERS.toString(),PermissionLevel.ALLOW.toString());
        Navigator.refresh();
        Navigator.dataAndBIPageFindMember();
        Assert.assertTrue("Element not exist " + FindMember.EXTRA_IDENTIFIERS, AcpService.isElementExist(FindMember.EXTRA_IDENTIFIERS));
        AcpService.setPermissionTag(Tags.EXTRA_IDENTIFIERS.toString(), PermissionLevel.DISALLOW.toString());
        Navigator.refresh();
        Navigator.dataAndBIPageFindMember();
        Assert.assertFalse("Element exist " + FindMember.EXTRA_IDENTIFIERS, AcpService.isElementExist(FindMember.EXTRA_IDENTIFIERS));

    }

    @Test
    public void testPermissionTagForMobileAppUserInFindMembers() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.FIND_USERS_FIELD_DELETE);
        Navigator.dataAndBIPageFindMember();
        Assert.assertTrue("Element not exist " + FindMember.MOBILE_APP_USER, AcpService.isElementExist(FindMember.MOBILE_APP_USER));
        AcpService.setPermissionTag(Tags.MOBILE_APP_USER.toString(), PermissionLevel.DISALLOW.toString());
        Navigator.dataAndBIPageFindMember();
        Assert.assertFalse("Element exist " + FindMember.MOBILE_APP_USER, AcpService.isElementExist(FindMember.MOBILE_APP_USER));
        Navigator.acpTabManageComoHubUsersUpdateUserCurrentUser();
        AcpService.setPermissionTag(Tags.MOBILE_APP_USER.toString(), PermissionLevel.ALLOW.toString());
        Navigator.refresh();
        Navigator.dataAndBIPageFindMember();
        Assert.assertTrue("Element not exist " + FindMember.MOBILE_APP_USER, AcpService.isElementExist(FindMember.MOBILE_APP_USER));
        AcpService.setPermissionTag(Tags.MOBILE_APP_USER.toString(), PermissionLevel.DISALLOW.toString());
        Navigator.refresh();
        Navigator.dataAndBIPageFindMember();
        Assert.assertFalse("Element exist " + FindMember.MOBILE_APP_USER, AcpService.isElementExist(FindMember.MOBILE_APP_USER));

    }

    @Test
    public void testPermissionTagForLocationEnabledInFindMembers() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.FIND_USERS_FIELD_DELETE);
        Navigator.dataAndBIPageFindMember();
        Assert.assertTrue("Element not exist " + FindMember.LOCATION_ENABLED, AcpService.isElementExist(FindMember.LOCATION_ENABLED));
        AcpService.setPermissionTag(Tags.LOCATION_ENABLED.toString(), PermissionLevel.DISALLOW.toString());
        Navigator.dataAndBIPageFindMember();
        Assert.assertFalse("Element exist " + FindMember.LOCATION_ENABLED, AcpService.isElementExist(FindMember.LOCATION_ENABLED));
        Navigator.acpTabManageComoHubUsersUpdateUserCurrentUser();
        AcpService.setPermissionTag(Tags.LOCATION_ENABLED.toString(), PermissionLevel.ALLOW.toString());
        Navigator.refresh();
        Navigator.dataAndBIPageFindMember();
        Assert.assertTrue("Element not exist " + FindMember.LOCATION_ENABLED, AcpService.isElementExist(FindMember.LOCATION_ENABLED));

        AcpService.setPermissionTag(Tags.LOCATION_ENABLED.toString(), PermissionLevel.DISALLOW.toString());
        Navigator.refresh();
        Navigator.dataAndBIPageFindMember();
        Assert.assertFalse("Element exist " + FindMember.LOCATION_ENABLED, AcpService.isElementExist(FindMember.LOCATION_ENABLED));

    }

    @Test
    @Category(Hub1Regression.class)
    public void testPermissionTagForPushNotificationEnabledInFindMembers() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.FIND_USERS_FIELD_DELETE);
        AcpService.openFindCustomersInFilterMembers();
        Assert.assertTrue("Element not exist " + FilterMember.PUSH_NOTIFICATION_ENABLED, AcpService.isElementExist(FilterMember.PUSH_NOTIFICATION_ENABLED));
        AcpService.setPermissionTag(Tags.PUSH_NOTIFICATION_ENABLED.toString(), PermissionLevel.DISALLOW.toString());
        AcpService.openFindCustomersInFilterMembers();
        Assert.assertFalse("Element exist " + FilterMember.PUSH_NOTIFICATION_ENABLED, AcpService.isElementExist(FilterMember.PUSH_NOTIFICATION_ENABLED));
        Navigator.acpTabManageComoHubUsersUpdateUserCurrentUser();
        AcpService.setPermissionTag(Tags.PUSH_NOTIFICATION_ENABLED.toString(), PermissionLevel.ALLOW.toString());
        Navigator.refresh();
        AcpService.openFindCustomersInFilterMembers();
        Assert.assertTrue("Element not exist " + FilterMember.PUSH_NOTIFICATION_ENABLED,AcpService.isElementExist(FilterMember.PUSH_NOTIFICATION_ENABLED));
        AcpService.setPermissionTag(Tags.PUSH_NOTIFICATION_ENABLED.toString(), PermissionLevel.DISALLOW.toString());
        Navigator.refresh();
        AcpService.openFindCustomersInFilterMembers();
        Assert.assertFalse("Element exist " + FilterMember.PUSH_NOTIFICATION_ENABLED, AcpService.isElementExist(FilterMember.PUSH_NOTIFICATION_ENABLED));

    }

    @Test
    public void testPermissionTagWhenFatherPermissionTagIsDisallowAndExtraIdentifiersIsAllowInFindMembers() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.FIND_USERS_FIELD_DELETE);
        Navigator.refresh();
        Navigator.dataAndBIPageFindMember();
        Assert.assertTrue("Element not exist " + FindMember.EXTRA_IDENTIFIERS, AcpService.isElementExist(FindMember.EXTRA_IDENTIFIERS));
        Assert.assertTrue("Element not exist " + FindMember.MOBILE_APP_USER, AcpService.isElementExist(FindMember.MOBILE_APP_USER));
        Assert.assertTrue("Element not exist " + FindMember.LOCATION_ENABLED, AcpService.isElementExist(FindMember.LOCATION_ENABLED));
        AcpService.openFindCustomersInFilterMembers();
        Assert.assertTrue("Element not exist " + FilterMember.PUSH_NOTIFICATION_ENABLED, AcpService.isElementExist(FilterMember.PUSH_NOTIFICATION_ENABLED));
        AcpService.setPermissionTag(Tags.FIND_USERS_FIELD.toString(), PermissionLevel.DISALLOW.toString());

        AcpService.setPermissionTag(Tags.EXTRA_IDENTIFIERS.toString(), PermissionLevel.ALLOW.toString());

        Navigator.dataAndBIPageFindMember();
        Assert.assertTrue("Element exist " + FindMember.EXTRA_IDENTIFIERS, AcpService.isElementExist(FindMember.EXTRA_IDENTIFIERS));
        Assert.assertFalse("Element exist " + FindMember.MOBILE_APP_USER, AcpService.isElementExist(FindMember.MOBILE_APP_USER));
        Assert.assertFalse("Element exist " + FindMember.LOCATION_ENABLED, AcpService.isElementExist(FindMember.LOCATION_ENABLED));
        AcpService.openFindCustomersInFilterMembers();
        Assert.assertFalse("Element exist " + FilterMember.PUSH_NOTIFICATION_ENABLED, AcpService.isElementExist(FilterMember.PUSH_NOTIFICATION_ENABLED));
        Navigator.acpTabManageComoHubUsersUpdateUserCurrentUser();
        AcpService.setPermissionTag(Tags.FIND_USERS_FIELD.toString(), PermissionLevel.ALLOW.toString());
        Navigator.refresh();
        Navigator.dataAndBIPageFindMember();
        Assert.assertTrue("Element not exist " + FindMember.EXTRA_IDENTIFIERS, AcpService.isElementExist(FindMember.EXTRA_IDENTIFIERS));
        Assert.assertTrue("Element not exist " + FindMember.MOBILE_APP_USER, AcpService.isElementExist(FindMember.MOBILE_APP_USER));
        Assert.assertTrue("Element not exist " + FindMember.LOCATION_ENABLED, AcpService.isElementExist(FindMember.LOCATION_ENABLED));
        AcpService.openFindCustomersInFilterMembers();
        Assert.assertTrue("Element not exist " + FilterMember.PUSH_NOTIFICATION_ENABLED, AcpService.isElementExist(FilterMember.PUSH_NOTIFICATION_ENABLED));

    }

    /*******************************************
     CNP-6322 - Membership status in find/filter
     /*****************************************/

    @Test
    public void testPermissionTagForMembershipStatusInFindAndFilterMembers() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.FIND_USERS_SECTION_DELETE);
        Navigator.refresh();
        Navigator.dataAndBIPageFindMember();
        Assert.assertTrue("Element not exist " + FindMember.MEMBERSHIP_STATUS, AcpService.isElementExist(FindMember.MEMBERSHIP_STATUS));
        Navigator.dataAndBIPageFilterMember();
        Assert.assertTrue("Element not exist " + FindMember.MEMBERSHIP_STATUS, AcpService.isElementExist(FindMember.MEMBERSHIP_STATUS));
        AcpService.setPermissionTag(Tags.MEMBERSHIP_STATUS.toString(), PermissionLevel.DISALLOW.toString());
        Navigator.dataAndBIPageFindMember();
        Assert.assertFalse("Element exist " + FindMember.MEMBERSHIP_STATUS, AcpService.isElementExist(FindMember.MEMBERSHIP_STATUS));
        Navigator.dataAndBIPageFilterMember();
        Assert.assertFalse("Element exist " + FindMember.MEMBERSHIP_STATUS, AcpService.isElementExist(FindMember.MEMBERSHIP_STATUS));
        AcpService.setPermissionTag(Tags.MEMBERSHIP_STATUS.toString(), PermissionLevel.ALLOW.toString());
        Navigator.dataAndBIPageFindMember();
        Assert.assertTrue("Element not exist " + FindMember.MEMBERSHIP_STATUS, AcpService.isElementExist(FindMember.MEMBERSHIP_STATUS));
        Navigator.dataAndBIPageFilterMember();
        Assert.assertTrue("Element not exist " + FindMember.MEMBERSHIP_STATUS, AcpService.isElementExist(FindMember.MEMBERSHIP_STATUS));
        AcpService.setPermissionTag(Tags.MEMBERSHIP_STATUS.toString(), PermissionLevel.DISALLOW.toString());
        Navigator.dataAndBIPageFindMember();
        Assert.assertFalse("Element exist " + FindMember.MEMBERSHIP_STATUS, AcpService.isElementExist(FindMember.MEMBERSHIP_STATUS));


    }

    /******************************************
     CNP-6323 - Dashboard KPI's
     /****************************************/


    @Test
    @Ignore
    public void testPermissionTagForDashboardKpiConnectWithFacebook() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.DASHBOARD_WIDGET_DELETE);
        DashboardService.openDashboard();
        validateDashboardKpiPermissionsExists(DashboardPage.DASHBOARD_KPI_CONNECT_WITH_FACEBOOK);

        AcpService.setPermissionTag(Tags.CONNECT_WITH_FACEBOOK.toString(), PermissionLevel.DISALLOW.toString());
        DashboardService.openDashboard();
        validateDashboardKpiPermissionsNotExists(DashboardPage.DASHBOARD_KPI_CONNECT_WITH_FACEBOOK);

        AcpService.setPermissionTag(Tags.CONNECT_WITH_FACEBOOK.toString(), PermissionLevel.ALLOW.toString());
        DashboardService.openDashboard();
        validateDashboardKpiPermissionsExists(DashboardPage.DASHBOARD_KPI_CONNECT_WITH_FACEBOOK);

        AcpService.setPermissionTag(Tags.CONNECT_WITH_FACEBOOK.toString(), PermissionLevel.DISALLOW.toString());
        DashboardService.openDashboard();
        validateDashboardKpiPermissionsNotExists(DashboardPage.DASHBOARD_KPI_CONNECT_WITH_FACEBOOK);

    }


    @Test
    @Ignore
    public void testPermissionTagForDashboardKpiShareOnFacebook() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.DASHBOARD_WIDGET_DELETE);
        Navigator.refresh();
        DashboardService.openDashboard();
        validateDashboardKpiPermissionsExists(DashboardPage.DASHBOARD_KPI_SHARE_ON_FACEBOOK);

        AcpService.setPermissionTag(Tags.SHARE_ON_FACEBOOK.toString(), PermissionLevel.DISALLOW.toString());
        DashboardService.openDashboard();
        validateDashboardKpiPermissionsNotExists(DashboardPage.DASHBOARD_KPI_SHARE_ON_FACEBOOK);

        AcpService.setPermissionTag(Tags.SHARE_ON_FACEBOOK.toString(), PermissionLevel.ALLOW.toString());
        DashboardService.openDashboard();
        validateDashboardKpiPermissionsExists(DashboardPage.DASHBOARD_KPI_SHARE_ON_FACEBOOK);

        AcpService.setPermissionTag(Tags.SHARE_ON_FACEBOOK.toString(), PermissionLevel.DISALLOW.toString());
        DashboardService.openDashboard();
        validateDashboardKpiPermissionsNotExists(DashboardPage.DASHBOARD_KPI_SHARE_ON_FACEBOOK);

    }

    @Test
    @Ignore
    public void testPermissionTagForDashboardKpiFilledAForm() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.DASHBOARD_WIDGET_DELETE);
        DashboardService.openDashboard();
        validateDashboardKpiPermissionsExists(DashboardPage.DASHBOARD_KPI_FILLED_OUT_A_FORM);

        AcpService.setPermissionTag(Tags.FILLED_A_FORM.toString(), PermissionLevel.DISALLOW.toString());
        DashboardService.openDashboard();
        validateDashboardKpiPermissionsNotExists(DashboardPage.DASHBOARD_KPI_FILLED_OUT_A_FORM);

        AcpService.setPermissionTag(Tags.FILLED_A_FORM.toString(), PermissionLevel.ALLOW.toString());
        DashboardService.openDashboard();
        validateDashboardKpiPermissionsExists(DashboardPage.DASHBOARD_KPI_FILLED_OUT_A_FORM);

        AcpService.setPermissionTag(Tags.FILLED_A_FORM.toString(), PermissionLevel.DISALLOW.toString());
        DashboardService.openDashboard();
        validateDashboardKpiPermissionsNotExists(DashboardPage.DASHBOARD_KPI_FILLED_OUT_A_FORM);
    }

    @Test
    @Ignore
    public void testPermissionTagWhenFatherPermissionTagIsDisallowAndFilledAFormIsAllowInDashboardKpis() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.DASHBOARD_WIDGET_DELETE);
        Navigator.refresh();
        DashboardService.openDashboard();
        validateDashboardKpiPermissionsExists(DashboardPage.DASHBOARD_KPI_CONNECT_WITH_FACEBOOK);

        AcpService.setPermissionTag(Tags.DASHBOARD_WIDGET.toString(), PermissionLevel.DISALLOW.toString());
        AcpService.setPermissionTag(Tags.FILLED_A_FORM.toString(), PermissionLevel.ALLOW.toString());
        DashboardService.openDashboard();

        DashboardService.openKpiDropdown("1");
        Assert.assertFalse("Element exist " + DashboardPage.DASHBOARD_KPI_CONNECT_WITH_FACEBOOK, AcpService.isElementExist(DashboardPage.DASHBOARD_KPI_CONNECT_WITH_FACEBOOK));
        Assert.assertFalse("Element exist " + DashboardPage.DASHBOARD_KPI_SHARE_ON_FACEBOOK, AcpService.isElementExist(DashboardPage.DASHBOARD_KPI_SHARE_ON_FACEBOOK));
        Assert.assertTrue("Element not exist " + DashboardPage.DASHBOARD_KPI_FILLED_OUT_A_FORM, AcpService.isElementExist(DashboardPage.DASHBOARD_KPI_FILLED_OUT_A_FORM));
        DashboardService.openKpiDropdown("2");
        Assert.assertFalse("Element exist " + DashboardPage.DASHBOARD_KPI_CONNECT_WITH_FACEBOOK, AcpService.isElementExist(DashboardPage.DASHBOARD_KPI_CONNECT_WITH_FACEBOOK));
        Assert.assertFalse("Element exist " + DashboardPage.DASHBOARD_KPI_SHARE_ON_FACEBOOK, AcpService.isElementExist(DashboardPage.DASHBOARD_KPI_SHARE_ON_FACEBOOK));
        Assert.assertTrue("Element not exist " + DashboardPage.DASHBOARD_KPI_FILLED_OUT_A_FORM, AcpService.isElementExist(DashboardPage.DASHBOARD_KPI_FILLED_OUT_A_FORM));
        DashboardService.openKpiDropdown("3");
        Assert.assertFalse("Element exist " + DashboardPage.DASHBOARD_KPI_CONNECT_WITH_FACEBOOK, AcpService.isElementExist(DashboardPage.DASHBOARD_KPI_CONNECT_WITH_FACEBOOK));
        Assert.assertFalse("Element exist " + DashboardPage.DASHBOARD_KPI_SHARE_ON_FACEBOOK, AcpService.isElementExist(DashboardPage.DASHBOARD_KPI_SHARE_ON_FACEBOOK));
        Assert.assertTrue("Element not exist " + DashboardPage.DASHBOARD_KPI_FILLED_OUT_A_FORM, AcpService.isElementExist(DashboardPage.DASHBOARD_KPI_FILLED_OUT_A_FORM));
        DashboardService.openKpiDropdown("4");
        Assert.assertFalse("Element exist " + DashboardPage.DASHBOARD_KPI_CONNECT_WITH_FACEBOOK, AcpService.isElementExist(DashboardPage.DASHBOARD_KPI_CONNECT_WITH_FACEBOOK));
        Assert.assertFalse("Element exist " + DashboardPage.DASHBOARD_KPI_SHARE_ON_FACEBOOK, AcpService.isElementExist(DashboardPage.DASHBOARD_KPI_SHARE_ON_FACEBOOK));
        Assert.assertTrue("Element not exist " + DashboardPage.DASHBOARD_KPI_FILLED_OUT_A_FORM, AcpService.isElementExist(DashboardPage.DASHBOARD_KPI_FILLED_OUT_A_FORM));

        AcpService.setPermissionTag(Tags.DASHBOARD_WIDGET.toString(),PermissionLevel.ALLOW.toString());
        DashboardService.openDashboard();
        validateDashboardKpiPermissionsExists(DashboardPage.DASHBOARD_KPI_CONNECT_WITH_FACEBOOK);

    }


    /*******************************************
     CNP-5995 - Permission Tags for App Layouts
     /*****************************************/

    @Test
    public void testPermissionTagForAppLayoutsSimple() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.HOME_SCREEN_LAYOUT_DELET);
        Navigator.refresh();
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.SIMPLE_LAYOUT, AcpService.isElementExist(HomeScreenPage.SIMPLE_LAYOUT));
        AcpService.setPermissionTag(Tags.SIMPLE_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.SIMPLE_LAYOUT, AcpService.isElementExist(HomeScreenPage.SIMPLE_LAYOUT));
        AcpService.setPermissionTag(Tags.SIMPLE_LAYOUT.toString(), PermissionLevel.ALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.SIMPLE_LAYOUT, AcpService.isElementExist(HomeScreenPage.SIMPLE_LAYOUT));
        AcpService.setPermissionTag(Tags.SIMPLE_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.SIMPLE_LAYOUT, AcpService.isElementExist(HomeScreenPage.SIMPLE_LAYOUT));

    }

    @Test
    public void testPermissionTagForAppLayoutsStylist() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.HOME_SCREEN_LAYOUT_DELET);
        Navigator.refresh();
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.STYLIST_LAYOUT, AcpService.isElementExist(HomeScreenPage.STYLIST_LAYOUT));
        AcpService.setPermissionTag(Tags.STYLIST_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.STYLIST_LAYOUT, AcpService.isElementExist(HomeScreenPage.STYLIST_LAYOUT));
        AcpService.setPermissionTag(Tags.STYLIST_LAYOUT.toString(), PermissionLevel.ALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.STYLIST_LAYOUT, AcpService.isElementExist(HomeScreenPage.STYLIST_LAYOUT));
        AcpService.setPermissionTag(Tags.STYLIST_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.STYLIST_LAYOUT, AcpService.isElementExist(HomeScreenPage.STYLIST_LAYOUT));

    }

    @Test
    public void testPermissionTagForAppLayoutsSlim() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.HOME_SCREEN_LAYOUT_DELET);
        Navigator.refresh();
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.SLIM_LAYOUT, AcpService.isElementExist(HomeScreenPage.SLIM_LAYOUT));
        AcpService.setPermissionTag(Tags.SLIM_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.SLIM_LAYOUT, AcpService.isElementExist(HomeScreenPage.SLIM_LAYOUT));
        AcpService.setPermissionTag(Tags.SLIM_LAYOUT.toString(), PermissionLevel.ALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.SLIM_LAYOUT, AcpService.isElementExist(HomeScreenPage.SLIM_LAYOUT));
        AcpService.setPermissionTag(Tags.SLIM_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.SLIM_LAYOUT, AcpService.isElementExist(HomeScreenPage.SLIM_LAYOUT));

    }

    @Test
    public void testPermissionTagForAppLayoutsStudio() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.HOME_SCREEN_LAYOUT_DELET);
        Navigator.refresh();
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.STUDIO_LAYOUT, AcpService.isElementExist(HomeScreenPage.STUDIO_LAYOUT));
        AcpService.setPermissionTag(Tags.STUDIO_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.STUDIO_LAYOUT, AcpService.isElementExist(HomeScreenPage.STUDIO_LAYOUT));
        AcpService.setPermissionTag(Tags.STUDIO_LAYOUT.toString(), PermissionLevel.ALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.STUDIO_LAYOUT, AcpService.isElementExist(HomeScreenPage.STUDIO_LAYOUT));
        AcpService.setPermissionTag(Tags.STUDIO_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.STUDIO_LAYOUT, AcpService.isElementExist(HomeScreenPage.STUDIO_LAYOUT));

    }

    @Test
    public void testPermissionTagForAppLayoutsThin() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.HOME_SCREEN_LAYOUT_DELET);
        Navigator.refresh();
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.THIN_LAYOUT, AcpService.isElementExist(HomeScreenPage.THIN_LAYOUT));
        AcpService.setPermissionTag(Tags.THIN_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.THIN_LAYOUT, AcpService.isElementExist(HomeScreenPage.THIN_LAYOUT));
        AcpService.setPermissionTag(Tags.THIN_LAYOUT.toString(), PermissionLevel.ALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.THIN_LAYOUT, AcpService.isElementExist(HomeScreenPage.THIN_LAYOUT));
        AcpService.setPermissionTag(Tags.THIN_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.THIN_LAYOUT, AcpService.isElementExist(HomeScreenPage.THIN_LAYOUT));

    }

    /*****************************************************************
     CNP-6751 - Permission tags for layouts with "Rotating Background"
     /***************************************************************/

    @Test
    public void testPermissionTagForHomeScreenLayoutMainLocationTCFullScreen0x3() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.LAYOUT_MAIN_LOCATION_TC_DELETE);
        Navigator.refresh();
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3));
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x3_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3));
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x3_LAYOUT.toString(), PermissionLevel.ALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3));
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x3_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3));

    }

    @Test
    public void testPermissionTagForHomeScreenLayoutMainLocationTCFullScreen0x4() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.LAYOUT_MAIN_LOCATION_TC_DELETE);
        Navigator.refresh();
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x4, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x4));
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x4_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x4, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x4));
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x4_LAYOUT.toString(), PermissionLevel.ALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x4, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x4));
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x4_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x4, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x4));

    }

    @Test
    public void testPermissionTagForHomeScreenLayoutMainLocationTCFullScreen0x2x2() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.LAYOUT_MAIN_LOCATION_TC_DELETE);
        Navigator.refresh();
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2));
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2));
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2_LAYOUT.toString(), PermissionLevel.ALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2));
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2));

    }

    @Test
    public void testPermissionTagForHomeScreenLayoutMainLocationTCFullScreen0x3x3() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.LAYOUT_MAIN_LOCATION_TC_DELETE);
        Navigator.refresh();
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3));
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3));
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3_LAYOUT.toString(), PermissionLevel.ALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3));
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3_LAYOUT.toString(), PermissionLevel.DISALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertFalse("Element exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3));

    }

    @Test
    public void testPermissionTagWhenFatherPermissionTagIsDisallowAndMainLocationTCFullScreensAllowed() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.LAYOUT_MAIN_LOCATION_TC_DELETE);
        Navigator.refresh();
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3));
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x4, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x4));
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2));
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3));
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC.toString(), PermissionLevel.DISALLOW.toString());
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x3_LAYOUT.toString(), PermissionLevel.ALLOW.toString());
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x4_LAYOUT.toString(), PermissionLevel.ALLOW.toString());
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2_LAYOUT.toString(), PermissionLevel.ALLOW.toString());
        AcpService.setPermissionTag(Tags.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3_LAYOUT.toString(), PermissionLevel.ALLOW.toString());
        HomeScreenService.openHomeScreenAndShowLayouts();
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3));
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x4, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x4));
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x2x2));
        Assert.assertTrue("Element not exist " + HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3, AcpService.isElementExist(HomeScreenPage.MAIN_LOCATION_TC_FULL_SCREEN_0x3x3));
        Assert.assertFalse("Element exist " + HomeScreenPage.HANDY_BARISTA_LAYOUT, AcpService.isElementExist(HomeScreenPage.HANDY_BARISTA_LAYOUT));

    }




    /*******************************************************************
     CNP-6209 - Permission Tags for Smart Actions (Automations + Filter)
     /*****************************************************************/

    @Test
    @Category(Hub1Regression.class)
    public void testPermissionTagForSmartActionSendAnAsset() throws Exception {

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        Navigator.refresh();
        FilterMembersService.openFilterMembersPageAndFilterAndOpenSmartActionDropDown();
        Assert.assertTrue("Element not exist " + FilterMember.SMART_ACTION_SEND_AN_ASSET, AcpService.isElementExist(FilterMember.SMART_ACTION_SEND_AN_ASSET));

        OperationService.openNewSmartAutomationAddTriggerAndOpenActionsDropDown();
        Assert.assertTrue("Element not exist " + SmartAutomationActions.SendAnAsset, AcpService.isElementExistInSelectOptionByLabel(String.valueOf(SmartAutomationActions.SendAnAsset)));

        AcpService.setPermissionTag(Tags.SMART_ACTION_ASSIGN_ASSET.toString(), PermissionLevel.DISALLOW.toString());
        FilterMembersService.openFilterMembersPageAndFilterAndOpenSmartActionDropDown();
        Assert.assertFalse("Element exist " + FilterMember.SMART_ACTION_SEND_AN_ASSET, AcpService.isElementExist(FilterMember.SMART_ACTION_SEND_AN_ASSET));
        OperationService.openNewSmartAutomationAddTriggerAndOpenActionsDropDown();
        Assert.assertFalse("Element exist " + SmartAutomationActions.SendAnAsset, AcpService.isElementExistInSelectOptionByLabel(String.valueOf(SmartAutomationActions.SendAnAsset)));

        AcpService.setPermissionTag(Tags.SMART_ACTION_ASSIGN_ASSET.toString(), PermissionLevel.ALLOW.toString());
        FilterMembersService.openFilterMembersPageAndFilterAndOpenSmartActionDropDown();
        Assert.assertTrue("Element not exist " + FilterMember.SMART_ACTION_SEND_AN_ASSET, AcpService.isElementExist(FilterMember.SMART_ACTION_SEND_AN_ASSET));
        OperationService.openNewSmartAutomationAddTriggerAndOpenActionsDropDown();
        Assert.assertTrue("Element not exist " + SmartAutomationActions.SendAnAsset, AcpService.isElementExistInSelectOptionByLabel(String.valueOf(SmartAutomationActions.SendAnAsset)));

        AcpService.setPermissionTag(Tags.SMART_ACTION_ASSIGN_ASSET.toString(), PermissionLevel.DISALLOW.toString());
        FilterMembersService.openFilterMembersPageAndFilterAndOpenSmartActionDropDown();
        Assert.assertFalse("Element exist " + FilterMember.SMART_ACTION_SEND_AN_ASSET, AcpService.isElementExist(FilterMember.SMART_ACTION_SEND_AN_ASSET));
        OperationService.openNewSmartAutomationAddTriggerAndOpenActionsDropDown();
        Assert.assertFalse("Element exist " + SmartAutomationActions.SendAnAsset, AcpService.isElementExistInSelectOptionByLabel(String.valueOf(SmartAutomationActions.SendAnAsset)));


    }

    @Test
    public void testPermissionTagForSmartActionSendEmail() throws Exception {
        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        Navigator.refresh();
        FilterMembersService.openFilterMembersPageAndFilterAndOpenSmartActionDropDown();
        Assert.assertTrue("Element not exist " + FilterMember.SMART_ACTION_SEND_AN_EMAIL, AcpService.isElementExist(FilterMember.SMART_ACTION_SEND_AN_EMAIL));

        AcpService.setPermissionTag(Tags.SEND_MEMBER_EMAIL_SMART_ACTION.toString(), PermissionLevel.DISALLOW.toString());
        FilterMembersService.openFilterMembersPageAndFilterAndOpenSmartActionDropDown();
        Assert.assertFalse("Element exist " + FilterMember.SMART_ACTION_SEND_AN_EMAIL, AcpService.isElementExist(FilterMember.SMART_ACTION_SEND_AN_EMAIL));

        AcpService.setPermissionTag(Tags.SEND_MEMBER_EMAIL_SMART_ACTION.toString(), PermissionLevel.ALLOW.toString());
        FilterMembersService.openFilterMembersPageAndFilterAndOpenSmartActionDropDown();
        Assert.assertTrue("Element not exist " + FilterMember.SMART_ACTION_SEND_AN_EMAIL, AcpService.isElementExist(FilterMember.SMART_ACTION_SEND_AN_EMAIL));
    }

    @Test
    @Category(Hub1Regression.class)
    public void testPermissionTagForTemplatePage() throws Exception {
        AcpService.checkUserPermissionTags(UserPermissions.EMAIL_TEMPLATE_PAGE_DELETE);
        Navigator.refresh();
        Navigator.contentTab();
        Assert.assertTrue("Element not exist " + BaseContentPage.EmailTemplatesManager.toString(), AcpService.isElementExist(BaseContentPage.EmailTemplatesManager));


        AcpService.setPermissionTag(Tags.EMAIL_TEMPLATE.toString(), PermissionLevel.DISALLOW.toString());
        Navigator.contentTab();
        Assert.assertFalse("Element exist " +  BaseContentPage.EmailTemplatesManager.toString(), AcpService.isElementExist(BaseContentPage.EmailTemplatesManager));

        AcpService.setPermissionTag(Tags.EMAIL_TEMPLATE.toString(), PermissionLevel.ALLOW.toString());
        Navigator.contentTab();
        Assert.assertTrue("Element not exist " +  BaseContentPage.EmailTemplatesManager.toString(), AcpService.isElementExist( BaseContentPage.EmailTemplatesManager));

    }


    //TODO - add test for CNP-6209



    private void validateDashboardKpiPermissionsExists(String elementName) throws Exception {

        boolean tryAgain = false;
        int i = 0;

        while (!tryAgain && i< 5) {

            DashboardService.openKpiDropdown("1");

            if (AcpService.isElementExist(DashboardPage.DASHBOARD_KPI_JOINED_THE_CLUB)) {

                Assert.assertTrue("Element not exist " + elementName, AcpService.isElementExist(elementName));
                DashboardService.openKpiDropdown("2");
                Assert.assertTrue("Element not exist " + elementName, AcpService.isElementExist(elementName));
                DashboardService.openKpiDropdown("3");
                Assert.assertTrue("Element not exist " + elementName, AcpService.isElementExist(elementName));
                DashboardService.openKpiDropdown("4");
                Assert.assertTrue("Element not exist " + elementName, AcpService.isElementExist(elementName));
                tryAgain = true;
                log4j.info("if " +i);

            } else {
                Navigator.dataAndBIPageFindMember();
                DashboardService.openDashboard();
                //Navigator.refresh();
                i++;
                log4j.info("else " +i);
            }
        }

    }

    private void validateDashboardKpiPermissionsNotExists(String elementName) throws Exception {
        DashboardService.openKpiDropdown("1");
        Assert.assertFalse("Element exist " + elementName, AcpService.isElementExist(elementName));
        DashboardService.openKpiDropdown("2");
        Assert.assertFalse("Element exist " + elementName, AcpService.isElementExist(elementName));
        DashboardService.openKpiDropdown("3");
        Assert.assertFalse("Element exist " + elementName, AcpService.isElementExist(elementName));
        DashboardService.openKpiDropdown("4");
        Assert.assertFalse("Element exist " + elementName, AcpService.isElementExist(elementName));
    }



    @AfterClass
    public static void closeBrowser(){
        cleanBase();
        BaseHubTest.cleanHub();
    }



}
