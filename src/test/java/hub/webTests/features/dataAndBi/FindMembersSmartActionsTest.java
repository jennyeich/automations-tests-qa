package hub.webTests.features.dataAndBi;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.benefits.LotteryReward;
import hub.common.objects.operations.UserPermissions;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.SmartPunchCard;
import hub.services.AcpService;
import hub.services.member.MembersService;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.ArrayList;
import java.util.List;

import static server.utils.ServerHelper.queryWithWait;

public class FindMembersSmartActionsTest extends BaseIntegrationTest {


    public CharSequence timestamp;

    @BeforeClass
    public static void setup() throws Exception {
        init();
        log4j.info("HUB login succeeded...");
    }
    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {


        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    @Test
    @Category(Hub1Regression.class)
    public void testSendAnAsset() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        SmartGift smartGift = hubAssetsCreator.createSmartGift(timestamp);
        MembersService.findMembers();
        int numOfMembers = Integer.parseInt(MembersService.numberOfMembers());
        String actionTag = MembersService.performSmartActionSendAnAsset(smartGift,timestamp);

        userActionPerformedDataStoreCheck(actionTag, numOfMembers);

    }

    @Test
    public void testAddPoints() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        MembersService.findMembers();
        int numOfMembers = Integer.parseInt(MembersService.numberOfMembers());
        String givePoints = "100";
        String actionTag = MembersService.performSmartActionAddPoints(givePoints,timestamp);

        userActionPerformedDataStoreCheck(actionTag, numOfMembers);

    }

    @Test
    public void testAddCredit() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        MembersService.findMembers();
        int numOfMembers = Integer.parseInt(MembersService.numberOfMembers());
        String giveCredit = "100";
        String actionTag = MembersService.performSmartActionAddCredit(giveCredit,timestamp);

        userActionPerformedDataStoreCheck(actionTag, numOfMembers);

    }


    @Test
    @Category(Hub1Regression.class)
    public void testTagMember() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        MembersService.findMembers();
        int numOfMembers = Integer.parseInt(MembersService.numberOfMembers());
        String [] array = MembersService.performSmartActionTagMember(timestamp);

        userActionPerformedDataStoreCheck(array[1], numOfMembers);

    }


    @Test
    public void testUpdateExpirationDateAction() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        MembersService.findMembers();
        int numOfMembers = Integer.parseInt(MembersService.numberOfMembers());
        String actionTag = MembersService.performSmartActionUpdateExpirationDate(timestamp);

        userActionPerformedDataStoreCheck(actionTag, numOfMembers);

    }


    @Test
    public void testSendLotteryRewardAction() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        SmartGift smartGift = hubAssetsCreator.createSmartGift(timestamp);
        LotteryReward lotteryReward = hubAssetsCreator.createLotteryReward("temp" + timestamp,smartGift.getTitle(),"1",null);
        MembersService.findMembers();
        int numOfMembers = Integer.parseInt(MembersService.numberOfMembers());

        String actionTag = MembersService.performSmartActionSendLotteryReward(lotteryReward,timestamp);

        userActionPerformedDataStoreCheck(actionTag, numOfMembers);

    }


    @Test
    public void testPunchAPunchCardAction() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        SmartPunchCard punchCard = hubAssetsCreator.createSmartPunchCard(timestamp,"4");
        MembersService.findMembers();
        MembersService.performSmartActionSendAPunchCard(punchCard,timestamp);
        MembersService.findMembers();
        int numOfMembers = Integer.parseInt(MembersService.numberOfMembers());

        String actionTag = MembersService.performSmartActionPunchAPunchCard(punchCard,timestamp);

        userActionPerformedDataStoreCheck(actionTag, numOfMembers);
    }






    private void userActionPerformedDataStoreCheck(String actionTag, int numOfMembers) throws InterruptedException {

        log4j.info("start DataStore check");
        Query qAction = new Query("UserAction");
        ArrayList<Query.Filter> filters = new ArrayList<>();
        filters.add(new Query.FilterPredicate("LocationID",Query.FilterOperator.EQUAL, locationId));
        filters.add(new Query.FilterPredicate("ActionTags",Query.FilterOperator.EQUAL, actionTag));
        qAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, filters));
        List<Entity> numOfMembersInDataStore = queryWithWait("Member did not get the action ",qAction,numOfMembers,dataStore);
        log4j.info("Number of members for the action: " +numOfMembersInDataStore.size());
        log4j.info("ActionTag: " +actionTag);
        Assert.assertEquals("Number of members must be equal ", numOfMembers, numOfMembersInDataStore.size());
    }

}
