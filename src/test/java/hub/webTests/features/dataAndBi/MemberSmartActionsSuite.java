package hub.webTests.features.dataAndBi;

import common.BaseTest;
import hub.base.BaseHubTest;
import hub.webTests.regression.WebRegressionSuite;
import integration.base.BaseIntegrationTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import utils.PropsReader;

import java.io.IOException;

/**
 * Created by Goni on 11/9/2017.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        FindMembersSmartActionsTest.class,
        MemberProfileSmartActionsTest.class
})
public class MemberSmartActionsSuite {
    public static final Logger log4j = Logger.getLogger(WebRegressionSuite.class);



    @BeforeClass
    public static void setup() throws IOException {
        // for feature env create new application
        if(!PropsReader.getPropValuesForEnv("location_prefix").contains("qa_") &&
                !PropsReader.getPropValuesForEnv("location_prefix").contains("hotfix_")&&
                !PropsReader.getPropValuesForEnv("location_prefix").contains(""))  {
            BaseIntegrationTest.isNewApplication = true;
        } else {
            BaseIntegrationTest.isNewApplication = false;

        }
        BaseIntegrationTest.init();

    }

    @AfterClass
    public static void tearDown(){
        log4j.info("close browser after finish test");
        BaseIntegrationTest.cleanBase();
        BaseHubTest.cleanHub();
    }

}

