package hub.webTests.features.dataAndBi;


import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.benefits.LotteryReward;
import hub.common.objects.member.FilterMember;
import hub.common.objects.model.ExportItemModel;
import hub.common.objects.operations.UserPermissions;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.SmartPunchCard;
import hub.hub1_0.services.Navigator;
import hub.services.AcpService;
import hub.services.ContentService;
import hub.services.member.MembersService;
import hub.smartAutomations.smartBirthdayAndAnniversaryAutomations.SmartBirthdayAndAnniversaryTest;
import hub.utils.HubMemberCreator;
import hub.utils.HubTemplateCreator;
import integration.base.BaseIntegrationTest;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.Consent;
import utils.PropsReader;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static server.utils.ServerHelper.queryWithWait;

public class FilterMembersSmartActionsTest extends BaseIntegrationTest {

    public CharSequence timestamp;
    public static final String DEFAULT_CODE_TEMPLATE_NAME = "DefaultCodeTemplate";
    public static final String DEFAULT_IMAGE_WITH_TEXT_TEMPLATE_NAME = "DefaultImageWithTextTemplate";
    public static final String EMAIL_MEMBER_FIRST_NAME = "testBulkEmailFN";
    public static final String EMAIL_MEMBER_LAST_NAME = "testBulkEmailLN";
    private static final String IMPORT_FOLDER = "import";
    private static final String IMPORT_2K_FILENAME = "2004KImport.csv";
    public static HubMemberCreator hubMemberCreator = new HubMemberCreator();

    public static final Logger log4j = Logger.getLogger(FilterMembersSmartActionsTest.class);

    private static String birthdayLocationId = PropsReader.getPropValuesForHub("birthday.anniversary.automation.locationId");
    @BeforeClass
    public static void setup() throws Exception {
        //for feature environments we start with new business
        if(!PropsReader.getPropValuesForEnv("location_prefix").contains("qa_") &&
                !PropsReader.getPropValuesForEnv("location_prefix").contains("hotfix_")&&
                !PropsReader.getPropValuesForEnv("location_prefix").contains("")) {
            init();
            try {
                AcpService.importMembers(IMPORT_FOLDER + File.separator + IMPORT_2K_FILENAME);
                Thread.sleep(10000);
                hubMemberCreator.createMemberWithEmailAddress("", "testBulkEmailFN", "testBulkEmailLN", "automationcomo@gmail.com");
                TimeUnit.MINUTES.sleep(25);
            } catch (Exception e) {
                log4j.error("Failed to import members: " + e.getMessage(),e);
            }

         //for qa\ hotfix env we run tests on existing business
        }else {
            isNewApplication = false;
            init();
            Navigator.selectApplicationByLocationId(birthdayLocationId);
            log4j.info("Moved to BDay app: " + birthdayLocationId);
            serverHelper.updateConsentFlag(birthdayLocationId,Boolean.valueOf(PropsReader.getPropValuesForEnv(Consent.REGISTER_MEMBERS_BY_CONSENT)).booleanValue());
        }

    }


    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {


        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    @Test
    public void testSendAnAsset() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        SmartGift smartGift = hubAssetsCreator.createSmartGift(timestamp);
        FilterMember filterMember = new FilterMember();
        MembersService.filterMemberByBigQuery(filterMember);
        int numOfMembers = Integer.parseInt(MembersService.numberOfMembers());
        String actionTag = MembersService.performSmartActionSendAnAsset(smartGift,timestamp);

        userActionPerformedDataStoreCheck(actionTag, numOfMembers);

    }


    @Test
    public void testAddPoints() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        FilterMember filterMember = new FilterMember();
        MembersService.filterMemberByBigQuery(filterMember);
        int numOfMembers = Integer.parseInt(MembersService.numberOfMembers());
        String givePoints = "100";
        String actionTag = MembersService.performSmartActionAddPoints(givePoints,timestamp);

        userActionPerformedDataStoreCheck(actionTag, numOfMembers);

    }


    @Test
    public void testAddCredit() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        FilterMember filterMember = new FilterMember();
        MembersService.filterMemberByBigQuery(filterMember);
        int numOfMembers = Integer.parseInt(MembersService.numberOfMembers());
        String giveCredit = "100";
        String actionTag = MembersService.performSmartActionAddCredit(giveCredit,timestamp);

        userActionPerformedDataStoreCheck(actionTag, numOfMembers);

    }


    @Test
    public void testTagMember() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        FilterMember filterMember = new FilterMember();
        MembersService.filterMemberByBigQuery(filterMember);
        Thread.sleep(5000);
        int numOfMembers = Integer.parseInt(MembersService.numberOfMembers());
        String [] array = MembersService.performSmartActionTagMember(timestamp);

        userActionPerformedDataStoreCheck(array[1], numOfMembers);

    }


    @Test
    @Category(Hub1Regression.class)
    public void testUpdateExpirationDateAction() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        FilterMember filterMember = new FilterMember();
        MembersService.filterMemberByBigQuery(filterMember);
        int numOfMembers = Integer.parseInt(MembersService.numberOfMembers());
        String actionTag = MembersService.performSmartActionUpdateExpirationDate(timestamp);

        userActionPerformedDataStoreCheck(actionTag, numOfMembers);

    }


    @Test
    public void testSendLotteryRewardAction() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        SmartGift smartGift = hubAssetsCreator.createSmartGift(timestamp);
        LotteryReward lotteryReward = hubAssetsCreator.createLotteryReward("temp" + timestamp,smartGift.getTitle(),"1",null);
        FilterMember filterMember = new FilterMember();
        MembersService.filterMemberByBigQuery(filterMember);
        int numOfMembers = Integer.parseInt(MembersService.numberOfMembers());

        String actionTag = MembersService.performSmartActionSendLotteryReward(lotteryReward,timestamp);

        userActionPerformedDataStoreCheck(actionTag, numOfMembers);

    }


    @Test
    public void testPunchAPunchCardAction() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        SmartPunchCard punchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "4");
        FilterMember filterMember = new FilterMember();
        MembersService.filterMemberByBigQuery(filterMember);
        MembersService.performSmartActionSendAPunchCard(punchCard,timestamp);
        MembersService.filterMemberByBigQuery(filterMember);
        int numOfMembers = Integer.parseInt(MembersService.numberOfMembers());

        String actionTag = MembersService.performSmartActionPunchAPunchCard(punchCard,timestamp);

        userActionPerformedDataStoreCheck(actionTag, numOfMembers);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testSendEmailFromFilter_CodeTemplate() throws Exception{
        if( !ContentService.isTemplateExist(DEFAULT_CODE_TEMPLATE_NAME)){
            HubTemplateCreator.createCodeTemplate(DEFAULT_CODE_TEMPLATE_NAME);
        }
        filterMemberAndSendEmail(DEFAULT_CODE_TEMPLATE_NAME);
        checkEmail(Arrays.asList("Click the gift to get your special surprise!"), String.format(HubTemplateCreator.DEFAULT_MAIL_SUBJECT, EMAIL_MEMBER_FIRST_NAME, EMAIL_MEMBER_LAST_NAME));
    }

    @Test
    public void testSendEmailFromFilter_imageWithTextTemplate() throws Exception{
        if( !ContentService.isTemplateExist(DEFAULT_IMAGE_WITH_TEXT_TEMPLATE_NAME)){
            HubTemplateCreator.createImageWithTextTemplate(DEFAULT_IMAGE_WITH_TEXT_TEMPLATE_NAME);
        }
        filterMemberAndSendEmail(DEFAULT_IMAGE_WITH_TEXT_TEMPLATE_NAME);
        checkEmail(Arrays.asList(String.format("%s , click here to get your special gift", EMAIL_MEMBER_FIRST_NAME), "This is the email text."),
                String.format(HubTemplateCreator.DEFAULT_MAIL_SUBJECT,  EMAIL_MEMBER_FIRST_NAME, EMAIL_MEMBER_LAST_NAME));
    }

    @Test
    public void testExportFilterMembers() throws Exception {
        FilterMember filterMember = new FilterMember();
        MembersService.filterMemberByBigQuery(filterMember);
        Thread.sleep(3000);
        int resCount = MembersService.getFilterCountMembersResult();
        List<ExportItemModel> exportItemModels = MembersService.exportFilterMemberBySql(filterMember);
        Assert.assertTrue(!exportItemModels.isEmpty());
        ExportItemModel exportItem = exportItemModels.get(0);//get the last export report
        Assert.assertTrue("export did not show in exports list, num of members to export <" + resCount + "> and last export num of members <" + exportItem.getMembersCount(), resCount == exportItem.getMembersCount());
    }



    private void filterMemberAndSendEmail(String templateName) throws Exception {
        FilterMember filterMember = new FilterMember();
        filterMember.setFindCustomers("Y");
        filterMember.setFirstName(EMAIL_MEMBER_FIRST_NAME);
        MembersService.filterMemberByBigQuery(filterMember);
        MembersService.performSmartActionSendEmail(templateName);
        Thread.sleep(20000);
    }

    private void checkEmail(List<String> contents, String subject) throws Exception {
        boolean result = ContentService.checkMail(contents, subject);
        if (!result) {
            Assert.fail("EMail didnt contain the expected subject and/or content.");
        }
    }

    private void userActionPerformedDataStoreCheck(String actionTag, int numOfMembers) throws InterruptedException {

        log4j.info("start DataStore check");
        Query qAction = new Query("UserAction");
        ArrayList<Query.Filter> filters = new ArrayList<>();
        filters.add(new Query.FilterPredicate("LocationID",Query.FilterOperator.EQUAL, birthdayLocationId));
        filters.add(new Query.FilterPredicate("ActionTags",Query.FilterOperator.EQUAL, actionTag));
        qAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, filters));
        List<Entity> numOfMembersInDataStore = queryWithWait("Member did not get the action ",qAction,numOfMembers,dataStore);
        log4j.info("Number of members for the action: " +numOfMembersInDataStore.size());
        log4j.info("ActionTag: " +actionTag);
        Assert.assertEquals("Number of members must be equal ", numOfMembers, numOfMembersInDataStore.size());
    }


}
