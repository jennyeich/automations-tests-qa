package hub.webTests.features.dataAndBi;

import com.google.appengine.api.datastore.Key;
import hub.base.BaseHubTest;
import hub.base.basePages.BaseDataAndBIPage;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.*;
import hub.common.objects.member.FilterMember;
import hub.hub1_0.services.Navigator;
import hub.services.ContentService;
import hub.services.member.MembersService;
import hub.utils.HubAssetsCreator;
import hub.utils.HubTemplateCreator;
import integration.base.BaseIntegrationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestName;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.AutomationException;
import utils.PropsReader;

import java.awt.*;
import java.beans.IntrospectionException;
import java.io.*;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.List;

import static hub.base.BaseService.getDriver;


/**
 * Created by lior on 5/8/17.
 */
public class FilterMembersTest extends BaseIntegrationTest {

    public static CharSequence timestamp;

    private static final String filterLocationId = PropsReader.getPropValuesForHub("filter.member.locationId");

    public static Key key = null;

    private HubAssetsCreator assetsCreator = new HubAssetsCreator();

    @Rule
    public TestName name = new TestName();

    public static final String TEST_FILE_NAME = "FilterMembersTestData.txt";

    @Before
    public  void createStatment() throws Exception {
        sqlDB.createStatment();
    }

    @BeforeClass
    public static void setup() throws Exception {

        isNewApplication = false;
        init();
        log4j.info("HUB login succeeded...");
        File timeFile = new File(TEST_FILE_NAME);
        if(timeFile.exists()){
            timeFile.delete();

        }
        Navigator.selectApplicationByLocationId(filterLocationId);

    }



    /****************************************************************************
     Preset Tests
     /**************************************************************************/

    @Test
    public void testFilterMemberPresetDidntOpenedTheAppInTheLast60Days() throws Exception {
        
        
        FilterMember filterMember = new FilterMember();
        filterMember.setChoosePresetFilter(FilterPreset.DIDNT_OPEN_THE_APP_IN_LAST_60_DAYS.toString());
        
        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'openedclub'");

    }



    @Test
    public void testFilterMemberPresetUsersWhoSharedOnFacebook() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setChoosePresetFilter(FilterPreset.USERS_WHO_SHARED_ON_FACEBOOK.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'FBPost'");

    }

    @Test
    public void testFilterMemberPresetOpenedTheAppMoreThanTwiceInLast60Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setChoosePresetFilter(FilterPreset.OPENED_THE_APP_MORE_THAN_TWICE_IN_THE_LAST_60_DAYS.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'openedclub'");
        assertQueryContains(query, "COUNT(*) BETWEEN 2 AND 999999");

    }

    @Test
    public void testFilterMemberPresetJoinedTheClubInLast30Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setChoosePresetFilter(FilterPreset.JOIND_THE_CLUB_IN_THE_LAST_30_DAYS.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'joinedclub'");

    }

    @Test
    public void testFilterMemberPresetPunchedAPunchCardInLast30Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setChoosePresetFilter(FilterPreset.PUNCHED_A_PUNCH_CARD_IN_THE_LAST_30_DAYS.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'punch'");
        assertQueryContains(query, "user_action.punch.asset_template = '865'");

    }

    @Test
    public void testFilterMemberPresetRedeemedAGiftInLast90Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setChoosePresetFilter(FilterPreset.REDEEMED_A_GIFT_IN_THE_LAST_90_DAYS.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'redeem'");
        assertQueryContains(query, "COUNT(*) BETWEEN 1 AND 999999");

    }


    /****************************************************************************
     Action Type Tests
     /**************************************************************************/

    @Test
    public void testFilterMembersActionTypeJoinedTheClub() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'joinedclub'");

    }

    @Test
    public void testFilterMembersActionTypeLaunchedTheAppForTheFirstTime() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.LAUNCHED_THE_APP_FOR_THE_FIRST_TIME.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'joinedapp'");
    }

    @Test
    public void testFilterMembersActionTypeOpenedTheApp() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.OPENED_THE_APP.toString());
        
        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'openedclub'");

    }

    @Test
    @Category(Hub1Regression.class)
    public void testFilterMembersActionTypePunchedAPunchCard() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.PUNCHED_A_PUNCH_CARD.toString());
        
        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'punch'");
    }

    @Test
    public void testFilterMembersActionTypeRedeemedAGift() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.REDEEMED_A_GIFT.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'redeem'");
    }

    @Test
    public void testFilterMembersActionTypeRedeemAttempt() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.ATTEMPTED_TO_REDEEM_A_GIFT.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'RedeemAttempt'");
    }

    @Test
    public void testFilterMembersActionTypeReceivedAnAsset() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_AN_ASSET.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'receivedasset'");
    }

    @Test
    public void testFilterMembersActionTypeReceivedOrUsedPoints() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_OR_USED_POINTS.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");

    }

    @Test
    public void testFilterMembersActionTypeTappedClaim() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.TAPPED_CLAIM.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'Claim'");
    }

    @Test
    public void testFilterMembersActionTypeUnregistered() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.UNREGISTERED.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'unregister'");
    }

    @Test
    public void testFilterMembersActionTypeEventExported() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.EVENT_EXPORTED.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'event_exported'");
    }

    @Test
    public void testFilterMembersActionTypeUsedCredit() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.USED_CREDIT.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");
        assertQueryContains(query, "SAFE_CAST(user_action.point_transaction.amounts.budget_weighted AS INT64) < 0");
    }

    @Test
    @Category(Hub1Regression.class)
    public void testFilterMembersActionTypeReceivedCredit() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_CREDIT.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");
        assertQueryContains(query, "SAFE_CAST(user_action.point_transaction.amounts.budget_weighted AS INT64) > 0");
    }

    @Test
    public void testFilterMembersActionTypeMadeAPurchase() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.MADE_A_PURCHASE.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
    }

    @Test
    public void testFilterMembersActionTypePurchasedASpecificItem() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.PURCHASED_A_SPECIFIC_ITEM.toString());
        filterMember.setItemCode("111");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);

    }


    @Test
    public void testFilterMembersActionTypeConnectedToFacebookThroughTheApp() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.CONNECTED_TO_FB_THROUGH_THE_APP.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'connectedwithfb'");
        assertQueryContains(query, "COUNT(*) > 0");
    }

    @Test
    public void testFilterMembersActionTypeShareAnItemToFacebook() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.SHARED_AN_ITEM_TO_FB.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'FBPost'");
    }

    @Test
    public void testFilterMembersActionTypeSendSMS() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.SEND_MEMBER_SMS.toString());
        
        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'MemberSMS'");
    }

    @Test
    public void testFilterMembersActionTypeBeaconSignal() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.BEACON_SIGNAL.toString());

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'ExternalEvent'");
    }


    /****************************************************************************
     different permutations to the action types
     /**************************************************************************/

    @Test
    public void testFilterMembersMadeAPurchaseWithNumberOfActionMoreThenOrEqualToZero() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.MADE_A_PURCHASE.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.MORE_THAN_OR_EQUAL_TO.toString());
        filterMember.setNumberOfActionsData("0");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);

    }

    @Test
    public void testFilterMembersMadeAPurchaseWithNumOfActionsMoreThanOrEqualTo4AndTotalIsGreaterThan10() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.MADE_A_PURCHASE.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.MORE_THAN_OR_EQUAL_TO.toString());
        filterMember.setNumberOfActionsData("4");
        filterMember.setPurchaseTotalIsGreaterThan("10");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "total_sum >= 1000");
        assertQueryContains(query, "COUNT(*) BETWEEN 4 AND 999999");

    }

    @Test
    public void testFilterMembersMadeAPurchaseWithNumOfActionsWithinARangeFrom1To5() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.MADE_A_PURCHASE.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.WITHIN_A_RANGE_AND_EQUAL_TO.toString());

        filterMember.setNumberOfActionsFrom("1");
        filterMember.setNumberOfActionsTo("5");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "COUNT(*) BETWEEN 1 AND 5");
    }

    @Test
    public void testFilterMembersMadeAPurchaseWithNumOfActionsLessThanOrEqualTo22() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.MADE_A_PURCHASE.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.LESS_THAN_OR_EQUAL_TO_INCLUDES_THE_VALUE_ZERO.toString());
        filterMember.setLessThenOrEqualToNumberOfActionData("22");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "membership_key NOT IN");
        assertQueryContains(query, "COUNT(*) > 22");
    }

    @Test
    public void testFilterMembersMadeAPurchaseInTheLast33Days() throws Exception {
        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.MADE_A_PURCHASE.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("33");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
    }

    @Test
    public void testFilterMembersMadeAPurchaseInThePastTwoMonthsWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar calPastTwoMonth  = Calendar.getInstance();
        calPastTwoMonth.add(Calendar.MONTH, -2);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calPastTwoMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();
    }

    @Test
    public void testFilterMembersJoinedTheClubFromTheLast40Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("40");

        startFilterByBQ(filterMember);
        assertNoErrors();

    }

    @Test
    public void testFilterMembersJoinedTheClubInTheLastMonthWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar calLastMonth  = Calendar.getInstance();
        calLastMonth.add(Calendar.MONTH, -1);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calLastMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'joinedclub'");

    }

    @Test
    public void testFilterMembersLaunchedTheAppForThe1stTimeFromTheLast40Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.LAUNCHED_THE_APP_FOR_THE_FIRST_TIME.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("40");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'joinedapp'");
    }

    @Test
    public void testFilterMembersLaunchedTheAppForThe1stTimeInTheLastMonthWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.LAUNCHED_THE_APP_FOR_THE_FIRST_TIME.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar calLastMonth  = Calendar.getInstance();
        calLastMonth.add(Calendar.MONTH, -1);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calLastMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'joinedapp'");
    }

    @Test
    public void testFilterMembersOpenedTheAppWithNumOfActionsWithinARangeFrom1To50() throws Exception {
        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.OPENED_THE_APP.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.WITHIN_A_RANGE_AND_EQUAL_TO.toString());
        filterMember.setNumberOfActionsFrom("1");
        filterMember.setNumberOfActionsTo("50");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'openedclub'");
        assertQueryContains(query, "COUNT(*) BETWEEN 1 AND 50");

    }

    @Test
    public void testFilterMembersOpenedTheAppWithMoreThanOrEqualTo12() throws Exception {
        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.OPENED_THE_APP.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.MORE_THAN_OR_EQUAL_TO.toString());
        filterMember.setNumberOfActionsData("12");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'openedclub'");
        assertQueryContains(query, "COUNT(*) BETWEEN 12 AND 999999");
    }

    @Test
    public void testFilterMembersOpenedTheAppWithMoreThanOrEqualToZero() throws Exception {
        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.OPENED_THE_APP.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.MORE_THAN_OR_EQUAL_TO.toString());
        filterMember.setNumberOfActionsData("0");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'openedclub'");
        assertQueryContains(query, "COUNT(*) > 999999");
    }

    @Test
    public void testFilterMembersOpenedTheAppWithLessThanOrEqualTo10() throws Exception {
        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.OPENED_THE_APP.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.LESS_THAN_OR_EQUAL_TO_INCLUDES_THE_VALUE_ZERO.toString());
        filterMember.setLessThenOrEqualToNumberOfActionData("10");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'openedclub'");
        assertQueryContains(query, "membership_key NOT IN");
        assertQueryContains(query, "COUNT(*) > 10");
    }

    @Test
    public void testFilterMembersOpenedTheAppInTheLastMonthWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.OPENED_THE_APP.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar calLastMonth  = Calendar.getInstance();
        calLastMonth.add(Calendar.MONTH, -1);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calLastMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'openedclub'");
    }

    @Test
    public void testFilterMembersOpenedTheAppFromTheLast32Days() throws  Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.OPENED_THE_APP.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("32");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'openedclub'");

    }

    /*@Test
    public void testFilterMembersPunchedAPunchCardWithSpecificPunchCard() throws Exception {
        
        FilterMember filterMember = new FilterMember();
        filterMember.setMemberActionType(FilterActionTypes.PUNCHED_A_PUNCH_CARD.toString());
        //TODO - Add specific punch card
        //filterMember.setPunchCardList(FilterMembersPunchCardsList.PUNCH_CARD_1.);
        MembersService.filterMemberBySql(filterMember);
        //MembersService.filterMemberByBigQuery(filterMember);
        long startTime = System.currentTimeMillis();
        String numOfMembersSql = MembersService.numberOfMembers();
        long endTime = System.currentTimeMillis();
        long timeForFilter = endTime - startTime;
        log4j.info("Number of members - SQL " +numOfMembersSql);
        log4j.info("Time for filter " +timeForFilter);

        writeFilterTimeAndNumOfMembersToFile(numOfMembersSql, timeForFilter, "SQL");
    }*/

    @Test
    public void testFilterMembersPunchedAPunchCardWithNumOfActionWithinARangeOf1To10() throws Exception {
        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.PUNCHED_A_PUNCH_CARD.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.WITHIN_A_RANGE_AND_EQUAL_TO.toString());
        filterMember.setNumberOfActionsFrom("1");
        filterMember.setNumberOfActionsTo("10");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'punch'");
        assertQueryContains(query, "COUNT(*) BETWEEN 1 AND 10");

    }

    @Test
    public void testFilterMembersPunchedAPunchCardWithNumOfActionMoreThanOrEqualTo7() throws Exception {
        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.PUNCHED_A_PUNCH_CARD.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.MORE_THAN_OR_EQUAL_TO.toString());
        filterMember.setNumberOfActionsData("7");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'punch'");
        assertQueryContains(query, "COUNT(*) BETWEEN 7 AND 999999");

    }

    @Test
    public void testFilterMembersPunchedAPunchCardWithNumberOfActionLessThanOrEqualTo5() throws Exception {
        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.PUNCHED_A_PUNCH_CARD.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.LESS_THAN_OR_EQUAL_TO_INCLUDES_THE_VALUE_ZERO.toString());
        filterMember.setLessThenOrEqualToNumberOfActionData("5");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'punch'");
        assertQueryContains(query, "membership_key NOT IN");
        assertQueryContains(query, "COUNT(*) > 5");
    }

    @Test
    public void testFilterMembersPunchedAPunchCardInThePastTwoMonthsWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.PUNCHED_A_PUNCH_CARD.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar calPastTwoMonths  = Calendar.getInstance();
        calPastTwoMonths.add(Calendar.MONTH, -2);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calPastTwoMonths));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'punch'");
    }

    @Test
    public void testFilterMembersPunchedAPunchCardFromTheLast20Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.PUNCHED_A_PUNCH_CARD.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("32");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'punch'");
    }

    @Test
    public void testFilterMembersRedeemedAGiftWithNumberOfActionWithinARangeOf1To30() throws Exception {
        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.REDEEMED_A_GIFT.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.WITHIN_A_RANGE_AND_EQUAL_TO.toString());
        filterMember.setNumberOfActionsFrom("1");
        filterMember.setNumberOfActionsTo("30");
        
        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'redeem'");
        assertQueryContains(query, "COUNT(*) BETWEEN 1 AND 30");
    }


    @Test
    public void testFilterMembersRedeemedAGiftWithNumOfActionsMoreThanOrEqualTo15() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.REDEEMED_A_GIFT.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.MORE_THAN_OR_EQUAL_TO.toString());
        filterMember.setNumberOfActionsData("15");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'redeem'");
        assertQueryContains(query, "COUNT(*) BETWEEN 15 AND 999999");
    }

    @Test
    public void testFilterMembersRedeemedAGiftWithNumOfActionsLessThanOrEqualTo17() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.REDEEMED_A_GIFT.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.LESS_THAN_OR_EQUAL_TO_INCLUDES_THE_VALUE_ZERO.toString());
        filterMember.setLessThenOrEqualToNumberOfActionData("17");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'redeem'");
        assertQueryContains(query, "membership_key NOT IN");
        assertQueryContains(query, "COUNT(*) > 17");
    }

    @Test
    public void testFilterMembersRedeemedAGiftWithTotalPerTransactionIsGreaterThan10() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.REDEEMED_A_GIFT.toString());
        filterMember.setTotalIsGreaterThan("10");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'redeem'");
        assertQueryContains(query, "p.total_sum >= 1000");
    }

    @Test
    public void testFilterMembersRedeemedAGiftWithTotalPerTransactionIsLessThan100() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.REDEEMED_A_GIFT.toString());
        filterMember.setTotalIsLessThan("100");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'redeem'");
        assertQueryContains(query, "p.total_sum <= 10000");
    }

    @Test
    public void testFilterMembersRedeemedAGiftWithinARangeOf1To20AndWithTotalPerTransactionIsGreaterThan10() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.REDEEMED_A_GIFT.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.WITHIN_A_RANGE_AND_EQUAL_TO.toString());
        filterMember.setNumberOfActionsFrom("1");
        filterMember.setNumberOfActionsTo("20");
        filterMember.setTotalIsGreaterThan("10");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'redeem'");
        assertQueryContains(query, "p.total_sum >= 1000");
        assertQueryContains(query, "COUNT(*) BETWEEN 1 AND 20");


    }

    @Test
    public void testFilterMembersRedeemedAGiftWithNumOfActionsLessThan7AndWithTotalPerTransactionIsLessThan100() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.REDEEMED_A_GIFT.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.LESS_THAN_OR_EQUAL_TO_INCLUDES_THE_VALUE_ZERO.toString());
        filterMember.setLessThenOrEqualToNumberOfActionData("7");
        filterMember.setTotalIsLessThan("100");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'redeem'");
        assertQueryContains(query, "p.total_sum <= 10000");
        assertQueryContains(query, "membership_key NOT IN");
        assertQueryContains(query, "COUNT(*) > 7");

    }


    @Test
    public void testFilterMembersRedeemedAGiftInThePastTwoMonthsWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.REDEEMED_A_GIFT.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar calPastTwoMonth  = Calendar.getInstance();
        calPastTwoMonth.add(Calendar.MONTH, -2);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calPastTwoMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'redeem'");

    }


    @Test
    public void testFilterMembersRedeemedAGiftInTheLast50Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.REDEEMED_A_GIFT.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("50");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'redeem'");
    }

    @Test
    public void testFilterMembersRedeemedAGiftWithinARangeOf1To20AndWithTotalPerTransactionIsGreaterThan10AndInThePastTwoMonthsWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.REDEEMED_A_GIFT.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.WITHIN_A_RANGE_AND_EQUAL_TO.toString());
        filterMember.setNumberOfActionsFrom("1");
        filterMember.setNumberOfActionsTo("20");
        filterMember.setTotalIsGreaterThan("10");
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar calPastTwoMonth  = Calendar.getInstance();
        calPastTwoMonth.add(Calendar.MONTH, -2);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calPastTwoMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'redeem'");

    }

    @Test
    public void testFilterMembersRedeemAttemptWithNumOfActionsWithinARangeFrom1To10() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.ATTEMPTED_TO_REDEEM_A_GIFT.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.WITHIN_A_RANGE_AND_EQUAL_TO.toString());
        filterMember.setNumberOfActionsFrom("1");
        filterMember.setNumberOfActionsTo("10");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'RedeemAttempt'");
        assertQueryContains(query, "COUNT(*) BETWEEN 1 AND 10");

    }

    @Test
    public void testFilterMembersRedeemAttemptWithNumOfActionsMoreThanOrEqualTo7() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.ATTEMPTED_TO_REDEEM_A_GIFT.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.MORE_THAN_OR_EQUAL_TO.toString());
        filterMember.setNumberOfActionsData("7");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'RedeemAttempt'");
        assertQueryContains(query, "COUNT(*) BETWEEN 7 AND 999999");
    }

    @Test
    public void testFilterMembersRedeemAttemptWithNumOfActionsLessThanOrEqualTo10() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.ATTEMPTED_TO_REDEEM_A_GIFT.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.LESS_THAN_OR_EQUAL_TO_INCLUDES_THE_VALUE_ZERO.toString());
        filterMember.setLessThenOrEqualToNumberOfActionData("10");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'RedeemAttempt'");
        assertQueryContains(query, "membership_key NOT IN");
        assertQueryContains(query, "COUNT(*) > 10");

    }

    @Test
    public void testFilterMembersRedeemAttemptInTheLastMonthWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.ATTEMPTED_TO_REDEEM_A_GIFT.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar calLastMonth  = Calendar.getInstance();
        calLastMonth.add(Calendar.MONTH, -1);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calLastMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'RedeemAttempt'");

    }

    @Test
    public void testFilterMembersRedeemAttemptFromTheLast30Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.ATTEMPTED_TO_REDEEM_A_GIFT.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("30");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'RedeemAttempt'");

    }

    @Test
    public void testFilterMembersReceivedAnAssetWithNumOfActionsWithinARangeOf2To8() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_AN_ASSET.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.WITHIN_A_RANGE_AND_EQUAL_TO.toString());
        filterMember.setNumberOfActionsFrom("2");
        filterMember.setNumberOfActionsTo("8");
        
        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'receivedasset'");
        assertQueryContains(query, "COUNT(*) BETWEEN 2 AND 8");
    }

    @Test
    public void testFilterMembersReceivedAnAssetWithNumOfActionsMoreThanOrEqualTo10() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_AN_ASSET.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.MORE_THAN_OR_EQUAL_TO.toString());
        filterMember.setNumberOfActionsData("10");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'receivedasset'");
        assertQueryContains(query, "COUNT(*) BETWEEN 10 AND 999999");
    }

    @Test
    public void testFilterMembersReceivedAnAssetWithNumOfActionsLessThanOrEqualTo8() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_AN_ASSET.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.LESS_THAN_OR_EQUAL_TO_INCLUDES_THE_VALUE_ZERO.toString());
        filterMember.setLessThenOrEqualToNumberOfActionData("8");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'receivedasset'");
        assertQueryContains(query, "membership_key NOT IN");
        assertQueryContains(query, "COUNT(*) > 8");

    }

    @Test
    public void testFilterMembersReceivedAnAssetInTheLastMonthWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_AN_ASSET.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());

        Calendar calLastMonth  = Calendar.getInstance();
        calLastMonth.add(Calendar.MONTH, -1);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calLastMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'receivedasset'");

    }

    @Test
    public void testFilterMembersReceivedAnAssetFromTheLast25Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_AN_ASSET.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("25");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'receivedasset'");

    }

    @Test
    public void testFilterMembersReceivedAnAssetWithNumOfActionsMoreThanOrEqualTo10AndInTheLastMonthWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_AN_ASSET.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.MORE_THAN_OR_EQUAL_TO.toString());
        filterMember.setNumberOfActionsData("10");

        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar calLastMonth  = Calendar.getInstance();
        calLastMonth.add(Calendar.MONTH, -1);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calLastMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'receivedasset'");

    }

    @Test
    public void testFilterMembersReceivedOrUsedPointsWithNumOfActionsWithinARangeOf3To15() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_OR_USED_POINTS.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.WITHIN_A_RANGE_AND_EQUAL_TO.toString());
        filterMember.setNumberOfActionsFrom("3");
        filterMember.setNumberOfActionsTo("15");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");
        assertQueryContains(query, "COUNT(*) BETWEEN 3 AND 15");
    }

    @Test
    public void testFilterMembersReceivedOrUsedPointsWithNumOfActionsMoreThanOrEqualTo9() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_OR_USED_POINTS.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.MORE_THAN_OR_EQUAL_TO.toString());
        filterMember.setNumberOfActionsData("9");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");
        assertQueryContains(query, "COUNT(*) BETWEEN 9 AND 999999");

    }

    @Test
    public void testFilterMembersReceivedOrUsedPointsWithNumOfActionsLessThanOrEqualTo7() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_OR_USED_POINTS.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.LESS_THAN_OR_EQUAL_TO_INCLUDES_THE_VALUE_ZERO.toString());
        filterMember.setLessThenOrEqualToNumberOfActionData("7");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");
        assertQueryContains(query, "membership_key NOT IN");
        assertQueryContains(query, "COUNT(*) > 7");
    }

    @Test
    public void testFilterMembersReceivedOrUsedPointsInTheLastMonthWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_OR_USED_POINTS.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());

        Calendar calLastMonth  = Calendar.getInstance();
        calLastMonth.add(Calendar.MONTH, -1);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calLastMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");

    }

    @Test
    public void testFilterMembersReceivedOrUsedPointsFromTheLast15Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_OR_USED_POINTS.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("15");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");

    }

    @Test
    public void testFilterMembersReceivedOrUsedPointsWithNumOfActionsLessThanOrEqualTo7AndFromTheLast15Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.RECEIVED_OR_USED_POINTS.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.LESS_THAN_OR_EQUAL_TO_INCLUDES_THE_VALUE_ZERO.toString());
        filterMember.setLessThenOrEqualToNumberOfActionData("7");
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("15");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");
        assertQueryContains(query, "membership_key NOT IN");
        assertQueryContains(query, "COUNT(*) > 7");
    }

    @Test
    public void testFilterMembersTappedClaimTriggerButtonInThePastTwoMonthsWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.TAPPED_CLAIM.toString());

        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar calPastTwoMonth  = Calendar.getInstance();
        calPastTwoMonth.add(Calendar.MONTH, -2);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calPastTwoMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'Claim'");

    }

    @Test
    public void testFilterMembersTappedClaimTriggerButtonFromTheLast40Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.TAPPED_CLAIM.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("40");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'Claim'");
    }

    @Test
    public void testFilterMembersUnregisteredInThePastTwoMonthsWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.UNREGISTERED.toString());

        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar calPastTwoMonth  = Calendar.getInstance();
        calPastTwoMonth.add(Calendar.MONTH, -2);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calPastTwoMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'unregister'");

    }

    @Test
    public void testFilterMembersUnregisteredFromTheLast35Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.UNREGISTERED.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("35");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'unregister'");

    }

    @Test
    public void testFilterMembersEventExportedInTheLastMonthWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.EVENT_EXPORTED.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());

        Calendar calLastMonth  = Calendar.getInstance();
        calLastMonth.add(Calendar.MONTH, -1);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calLastMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'event_exported'");

    }

    @Test
    public void testFilterMembersEventExportedFromTheLast55Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.EVENT_EXPORTED.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("55");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'event_exported'");

    }

    @Test
    public void testFilterMembersUsedCreditPerActionWithinARangeOf2To10() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.USED_CREDIT.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.WITHIN_A_RANGE_AND_EQUAL_TO.toString());
        filterMember.setNumberOfActionsFrom("2");
        filterMember.setNumberOfActionsTo("10");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");
        assertQueryContains(query, "SAFE_CAST(user_action.point_transaction.amounts.budget_weighted AS INT64) < 0");
        assertQueryContains(query, "(membership_key_int) BETWEEN 2 AND 10");


    }

    @Test
    public void testFilterMembersUsedCreditPerActionMoreThanOrEqualTo9() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.USED_CREDIT.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.MORE_THAN_OR_EQUAL_TO.toString());
        filterMember.setNumberOfActionsData("9");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");
        assertQueryContains(query, "SAFE_CAST(user_action.point_transaction.amounts.budget_weighted AS INT64) < 0");
        assertQueryContains(query, "(membership_key_int) >= 9 ");

    }

    @Test
    public void testFilterMembersUsedCreditPerActionLessThanOrEqualTo7() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.USED_CREDIT.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.LESS_THAN_OR_EQUAL_TO_INCLUDES_THE_VALUE_ZERO.toString());
        filterMember.setLessThenOrEqualToNumberOfActionData("7");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");
        assertQueryContains(query, "membership_key NOT IN");
        assertQueryContains(query, "SAFE_CAST(user_action.point_transaction.amounts.budget_weighted AS INT64) < 0");
        assertQueryContains(query, "(membership_key_int) > 7 ");

    }


    @Test
    public void testFilterMembersUsedCreditAggregateTotalOfAllActionsWithTotalValueWithinARangeOf1to10() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.USED_CREDIT.toString());
        filterMember.setApplyFieldsPerActionOrTotal(FilterMembersApplyFieldsPerActionOrTotal.TOTAL.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.WITHIN_A_RANGE_AND_EQUAL_TO.toString());
        filterMember.setTotalAmountRangeFrom("1");
        filterMember.setTotalAmountRangeTo("10");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");
        assertQueryContains(query, "SAFE_CAST(user_action.point_transaction.amounts.budget_weighted AS INT64) < 0");
        assertQueryContains(query, "SUM(charge) BETWEEN -1000 AND -1");

    }

    @Test
    public void testFilterMembersUsedCreditAggregateTotalOfAllActionsWithTotalValueMoreThanOrEqualTo5() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.USED_CREDIT.toString());
        filterMember.setApplyFieldsPerActionOrTotal(FilterMembersApplyFieldsPerActionOrTotal.TOTAL.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.MORE_THAN_OR_EQUAL_TO.toString());
        filterMember.setTotalValueMoreThanOrEqualToData("5");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");
        assertQueryContains(query, "SAFE_CAST(user_action.point_transaction.amounts.budget_weighted AS INT64) < 0");
        assertQueryContains(query, "SUM(charge) <=-500 ");

    }

    @Test
    public void testFilterMembersUsedCreditAggregateTotalOfAllActionsWithTotalValueLessThanOrEqualTo10() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.USED_CREDIT.toString());
        filterMember.setApplyFieldsPerActionOrTotal(FilterMembersApplyFieldsPerActionOrTotal.TOTAL.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.LESS_THAN_OR_EQUAL_TO_INCLUDES_THE_VALUE_ZERO.toString());
        filterMember.setTotalValueLessThanOrEqualToData("10");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");
        assertQueryContains(query, "SAFE_CAST(user_action.point_transaction.amounts.budget_weighted AS INT64) < 0");
        assertQueryContains(query, "SUM(charge) >=-1000 ");

    }

    @Test
    public void testFilterMembersUsedCreditPerActionLessThanOrEqualTo7AndInTheLastMonthWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.USED_CREDIT.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.LESS_THAN_OR_EQUAL_TO_INCLUDES_THE_VALUE_ZERO.toString());
        filterMember.setLessThenOrEqualToNumberOfActionData("7");

        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar calLastMonth  = Calendar.getInstance();
        calLastMonth.add(Calendar.MONTH, -1);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calLastMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");
        assertQueryContains(query, "SAFE_CAST(user_action.point_transaction.amounts.budget_weighted AS INT64) < 0");

    }

    @Test
    public void testFilterMembersUsedCreditPerActionMoreThanOrEqualTo9AndFromTheLast30Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.USED_CREDIT.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.MORE_THAN_OR_EQUAL_TO.toString());
        filterMember.setNumberOfActionsData("9");

        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("30");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'PointTransaction'");
        assertQueryContains(query, "SAFE_CAST(user_action.point_transaction.amounts.budget_weighted AS INT64) < 0");
        assertQueryContains(query, "Count(membership_key_int) >= 9 ");

    }

    @Test
    public void testFilterMembersActionTypeSendSMSFromTheLast40Days() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.SEND_MEMBER_SMS.toString());

        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        filterMember.setFromTheLastXDays("40");

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'MemberSMS'");
    }

    @Test
    public void testFilterMembersActionTypeSendSMSInTheLastMonthWithCalendar() throws Exception {

        
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.SEND_MEMBER_SMS.toString());

        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar calLastMonth  = Calendar.getInstance();
        calLastMonth.add(Calendar.MONTH, -1);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(calLastMonth));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();

        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'MemberSMS'");

    }


    @Test
    public void testFilterMembersActionTypeSendEmailWithCalendar() throws Exception{
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.SEND_MEMBER_EMAIL.toString());

        filterMember.setChooseDate(FilterMembersChooseDates.CHOOSE_FROM_CALENDAR.toString());
        Calendar cal3MonthsAgo  = Calendar.getInstance();
        cal3MonthsAgo.add(Calendar.MONTH, -3);
        DateAndTimeInFilter dateAndTimeInFilter = new DateAndTimeInFilter();
        dateAndTimeInFilter.setFromDate(assetsCreator.getDate(cal3MonthsAgo));
        dateAndTimeInFilter.setUntilDate(assetsCreator.getDate(Calendar.getInstance()));
        filterMember.setDateAndTimeInFilter(dateAndTimeInFilter);

        startFilterByBQ(filterMember);
        assertNoErrors();
        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'MemberEmail'");

    }

    @Test
    public void testFilterMembersActionTypeSendEmailWithSourceAndTemplate() throws Exception{
        String templateName = "template1";
        if( !ContentService.isTemplateExist(templateName)) {
            HubTemplateCreator.createImageTemplate(templateName);
        }
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.SEND_MEMBER_EMAIL.toString());
        filterMember.setTemplateName(templateName);
        filterMember.setSentFrom(FilterMembersEmailSentFrom.AUTOMATION);
        startFilterByBQ(filterMember);

        //get template id from ui
        String templateId = getDriver().findElement(By.xpath("//*[contains(@label, '" + templateName + "')]")).getAttribute("value");
        templateId = templateId.replace("string:", "");

        assertNoErrors();
        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'MemberEmail'");
        assertQueryContains(query, "user_action.member_email.source ='Automation'");
        assertQueryContains(query, "user_action.member_email.template_id = '" + templateId + "'");
    }

    @Test
    public void testFilterMembersActionTypeSendEmailWithSourceAndEvent() throws Exception{
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.SEND_MEMBER_EMAIL.toString());
        filterMember.setSentFrom(FilterMembersEmailSentFrom.HUB);
        filterMember.setEmailEvents(FilterMembersEmailEvents.EMAIL_WAS_OPENED);
        startFilterByBQ(filterMember);

        assertNoErrors();
        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'MemberEmail'");
        assertQueryContains(query, "user_action.member_email.source ='Hub'");
        assertQueryContains(query, "user_action.member_email.event = 'opened'");
    }

    @Test
    public void testFilterMembersActionTypeSendEmailWithNumberOfActions() throws Exception{
        FilterMember filterMember = new FilterMember();
        filterMember.setAddSearchFilter("Y");
        filterMember.setMemberActionType(FilterActionTypes.SEND_MEMBER_EMAIL.toString());
        filterMember.setNumberOfActionsLabel(FilterMembersNumberOfActions.MORE_THAN_OR_EQUAL_TO.toString());
        filterMember.setNumberOfActionsData("2");
        startFilterByBQ(filterMember);

        assertNoErrors();
        String query = getQueryAndValidate(filterLocationId);
        assertQueryContains(query, "action = 'MemberEmail'");
        assertQueryContains(query, "COUNT(*) BETWEEN 2 AND 999999");
    }

    private void startFilterByBQ(FilterMember filterMember) throws InterruptedException, AutomationException {

        //Navigator.refresh();
        MembersService.filterMemberByBigQuery(filterMember);
        int numOfMembersBQ = getNumOfMembers("BQ");
    }



    private int getNumOfMembers(String filterType){
        long startTime = System.currentTimeMillis();
        String numOfMembers = MembersService.numberOfMembers();
        long timeForFilter = System.currentTimeMillis()-startTime;
        writeFilterTimeAndNumOfMembersToFile(numOfMembers, timeForFilter, filterType);
        return Integer.valueOf(numOfMembers).intValue();
    }



    private void writeFilterTimeAndNumOfMembersToFile(String numOfMembers, long timeForFilter, String filterType){
        log4j.info("Will write to file:");
        log4j.info("Number of members - "  + filterType + " "  +numOfMembers);
        log4j.info("Time for filter "+ filterType + " " +timeForFilter);
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(TEST_FILE_NAME, true), "utf-8"))) {
            writer.write(name.getMethodName() + '\n');
            writer.write("Number of members - " + filterType + " " + numOfMembers + '\n');
            writer.write("Time for filter "  + filterType + " " + timeForFilter + '\n'+ '\n');
        } catch (Exception e) {
            log4j.error("error while creating file",e);
        }
    }

    private String getQueryAndValidate(String locationId) throws SQLException, ClassNotFoundException {
        String query = sqlDB.getLastBigQueryForBusiness(locationId);
        log4j.info("The sql query:" + query);
        Assert.assertTrue("Location must contain the location id ", query.contains("location_id = '" + locationId + "'"));

        return query;
    }

    private void assertQueryContains(String query, String value){
        Assert.assertTrue("Query must contain the value: " + value, query.contains(value));
    }

    private void assertNoErrors() throws Exception{

        List<WebElement> notes = getDriver().findElements(BaseDataAndBIPage.FILTER_ERROR);
        int size = notes.size();
        Assert.assertTrue("Filter error is shown", size==0);
    }




    @After
    public void closeStatment() throws Exception {
        sqlDB.closeStatment();
    }



    @AfterClass
    public static void closeBrowser(){
        cleanBase();
        BaseHubTest.cleanHub();
    }

}
