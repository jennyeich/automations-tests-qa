package hub.webTests.features.dataAndBi.sms;

import hub.common.objects.operations.AppSettingsPage;
import hub.common.objects.operations.CountryCode_APP_SETTINGS;
import hub.common.objects.operations.SMSConfigurationProvider;
import hub.hub1_0.services.OperationService;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class SendSMSILTest extends SendSMSBase {


    public CharSequence timestamp;

    @BeforeClass
    public static void setup() throws Exception {
        init();

        AppSettingsPage appSettingsPage = new AppSettingsPage();
        appSettingsPage.setCountryCode(CountryCode_APP_SETTINGS.IL.toString());
        OperationService.updateAppSettings(appSettingsPage);
        setUnsubscribeMethod(LINK);
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {


        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    @Test
    public void testSendSMSTwilio() throws Exception {

        //Set provider
        setSMSProvider(SMSConfigurationProvider.COMO_TWILIO.toString());
        String customerPhone = createPhoneNumber();

        //Create member
        hubMemberCreator.createMemberWithEmailSMS(customerPhone, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, "Yes", "Yes");
        createAndCheckSMS (customerPhone, 3);
    }


    @Test
    public void testSendSMSNexmo() throws Exception {

        //Set provider
        setSMSProvider(SMSConfigurationProvider.COMO_NEXMO.toString());
        String customerPhone = createPhoneNumber();

        //Create member
        hubMemberCreator.createMemberWithEmailSMS(customerPhone, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, "Yes", "Yes");

        createAndCheckSMS (customerPhone, 3);


    }

    @Test
    public void testSendSMSPlivo() throws Exception {

        //Set provider
        setSMSProvider(SMSConfigurationProvider.COMO_PLIVO.toString());
        String customerPhone = createPhoneNumber();

        //Create member
        hubMemberCreator.createMemberWithEmailSMS(customerPhone, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, "Yes", "Yes");

        createAndCheckSMS (customerPhone, 3);

    }


    @Test
    public void testSendSMSUnicellOperational() throws Exception {

        //Set provider
        setSMSProvider(SMSConfigurationProvider.COMO_UNICELL_MARKETING.toString());
        String customerPhone = "0544671785"; //Doesn't work with invalid phoneNumber

        //Create member
        hubMemberCreator.createMemberWithEmailSMS(customerPhone, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, "Yes", "Yes");
        createAndCheckSMS (customerPhone, 2);

    }

}




