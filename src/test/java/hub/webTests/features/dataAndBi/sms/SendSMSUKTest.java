package hub.webTests.features.dataAndBi.sms;


import hub.common.objects.operations.AppSettingsPage;
import hub.common.objects.operations.CountryCode_APP_SETTINGS;
import hub.common.objects.operations.SMSConfigurationProvider;
import hub.hub1_0.services.OperationService;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class SendSMSUKTest extends SendSMSBase {


    public  final String CUSTOMER_PHONE= "7481358337";
    public final String CUSTOMER_PHONE_FROM = "+44" + CUSTOMER_PHONE;
    public final String UNSUBSCRIBE_MSG = "You are unsubscribed from all alerts,  no more messages will be sent";


    @BeforeClass
    public static void setup() throws Exception {

        init();
        AppSettingsPage appSettingsPage = new AppSettingsPage();
        appSettingsPage.setCountryCode(CountryCode_APP_SETTINGS.UK.toString());
        OperationService.updateAppSettings(appSettingsPage);
        setUnsubscribeMethod("reply");
    }
    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {


        @Override
        protected void starting(Description description) {

            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };


    @Test
    public void testSendSMSTwilioUKwithunsubscribe() throws Exception{

        sendSMSandVerifyUnsubscribe(SMSConfigurationProvider.COMO_TWILIO.toString(),CUSTOMER_PHONE, CUSTOMER_PHONE_FROM,UNSUBSCRIBE_MSG, locationId);

    }


    @Test
    public void testSendSMSNexmoUKUnsubscribe() throws Exception{

        sendSMSandVerifyUnsubscribe(SMSConfigurationProvider.NEXMO_SHORTCODE.toString(),CUSTOMER_PHONE, CUSTOMER_PHONE_FROM,UNSUBSCRIBE_MSG,locationId);

    }

    @Test
    public void testSendSMSPlivoUKWithUnsubscribe() throws Exception{

        sendSMSandVerifyUnsubscribe(SMSConfigurationProvider.COMO_PLIVO.toString(),CUSTOMER_PHONE, CUSTOMER_PHONE_FROM,UNSUBSCRIBE_MSG,locationId);

    }





}
