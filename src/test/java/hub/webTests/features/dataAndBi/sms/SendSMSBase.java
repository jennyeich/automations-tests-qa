package hub.webTests.features.dataAndBi.sms;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.operations.ExternalServicesPage;
import hub.hub1_0.services.OperationService;
import hub.services.ContentService;
import hub.services.member.MembersService;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static server.utils.ServerHelper.queryWithWait;

public class SendSMSBase extends BaseIntegrationTest {

    public static final String UNSUBSCRIBED_SMS = "Unsubscribed receiving sms";
    public static final String HELP = "HELP";
    public CharSequence timestamp;
    public static final String LINK = "link";



    protected void userActionPerformedDataStoreCheck(String actionTag, int numOfMembers, String message) throws InterruptedException {

        //Wait for datastore to be updated
        Thread.sleep(50000);
        log4j.info("start DataStore check");
        Query qAction = new Query("UserAction");
        ArrayList<Query.Filter> filters = new ArrayList<>();
        filters.add(new Query.FilterPredicate("LocationID",Query.FilterOperator.EQUAL, locationId));
        filters.add(new Query.FilterPredicate("ActionTags",Query.FilterOperator.EQUAL, actionTag));
        qAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, filters));
        List<Entity> numOfMembersInDataStore = queryWithWait("Member did not get the action ",qAction,numOfMembers,dataStore);

        for (int i=0; i<numOfMembersInDataStore.size(); i++) {

            Text t = ((Text) numOfMembersInDataStore.get(i).getProperty("Data"));
            log4j.info("Data contains the following data : " + t.getValue());
            Assert.assertTrue("Data must contain the message status accepted", t.getValue().contains("status=\"" + "accepted" + "\""));

            if (t.getValue().contains("smsGateway"))
                Assert.assertTrue("Data must contain the message content: ", t.getValue().contains("messageContent=\"" + message));

        }
        Assert.assertEquals("Number of members must be equal ", numOfMembers, numOfMembersInDataStore.size());
        log4j.info("Number of members for the action: " + numOfMembersInDataStore.size());
        log4j.info("ActionTag: " + actionTag);

    }



    public String createPhoneNumber() {

        int leftLimit = 1000000;
        int rightLimit = 10000000;
        int generatedInteger = 31 * (leftLimit + (int) (new Random().nextFloat() * (rightLimit - leftLimit)));
        String phoneNumber = Integer.toString(generatedInteger);
        System.out.println(phoneNumber);
        return "051" + phoneNumber;
    }

    protected String createAndCheckSMS (String customerPhone, int numOfMessages) throws Exception{

        String smsMessage = "Welcome to: " + customerPhone;
        String actionsTag = MembersService.performSmartActionSendSMS(smsMessage,customerPhone);
        userActionPerformedDataStoreCheck(actionsTag,numOfMessages,smsMessage);
        return smsMessage;
    }


    protected void setSMSProvider (String provider) throws Exception{

        ExternalServicesPage externalServicesPage = new ExternalServicesPage();
        externalServicesPage.setSmsConfiguration(provider);
        OperationService.setExternalSettings(externalServicesPage);
    }

    protected static void setUnsubscribeMethod (String linkOrReply) {

        ExternalServicesPage externalServicesPage = new ExternalServicesPage();
        if (linkOrReply.equals(LINK))
            externalServicesPage.setUnsubscribe_link("Y");
        else
            externalServicesPage.setUnsubscribe_reply("Y");
    }


    protected void setSMSProviderANDUnsubscribeLink (String provider) throws Exception {

        ExternalServicesPage externalServicesPage = new ExternalServicesPage();
        externalServicesPage.setSmsConfiguration(provider);
        externalServicesPage.setUnsubscribe_link("Y");
        OperationService.setExternalSettings(externalServicesPage);
    }


    protected void updateMemberDetails () throws Exception{

        String timestamp = String.valueOf(System.currentTimeMillis());
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setPhoneNumber(timestamp);
        memberDetails.setFirstName(timestamp);
        memberDetails.setLastname(timestamp);
        memberDetails.setEmail(timestamp + "@gmail.com");
        MembersService.updateMember(memberDetails);

    }

    protected void verifyMemberDoesntExist (String phoneNumber) throws Exception {

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        try {

            MembersService.findMember(findMember);
            MembersService.clickMember();
            updateMemberDetails ();

        }catch (Exception e){
            log4j.info("The member was not found");
        }

    }


    public void sendUnsubscribeSMSAndVerify(String smsMessage, long mills, String CUSTOMER_PHONE_FROM, String MSG, String unsubscribeMsg) throws Exception {


        String fromNumber = TwilioHelper.getFromNumberFromMessage(CUSTOMER_PHONE_FROM,mills,smsMessage);
        log4j.info("SMS was send. From number: " + fromNumber);

        mills = TwilioHelper.sendSMS(CUSTOMER_PHONE_FROM,fromNumber, unsubscribeMsg);
        log4j.info("Unsubscribe SMS sent: STOP " + locationId);

        fromNumber = TwilioHelper.getFromNumberFromMessage(CUSTOMER_PHONE_FROM,mills,MSG);
        Assert.assertNotEquals("The member didn't get the unsubscribe message", "not_found", fromNumber); //not empty means was found and got the message
        log4j.info("Member got unsubscribe SMS: " + MSG);

        //Check the user action log
        MembersService.refreshHubPage();
        String log =  MembersService.checkUserLog(UNSUBSCRIBED_SMS);
        Assert.assertEquals("User log is not as expected: " + log,log,UNSUBSCRIBED_SMS);

        //Verify AllowSMS = No
        Assert.assertEquals("Expected AllowSMS=No", "No" , MembersService.getMemberAllowSMS());
    }


    public void sendSMSandVerifyUnsubscribe (String provider, String CUSTOMER_PHONE, String CUSTOMER_PHONE_FROM, String UNSUBSCRIBE_MSG, String unsubscribe) throws Exception {

        //Set provider
        setSMSProvider(provider);

        //Verify that the member doesn't exist. If exists, one would be renamed before the test
        verifyMemberDoesntExist(CUSTOMER_PHONE);

        //Create member
        hubMemberCreator.createMemberWithEmailSMS(CUSTOMER_PHONE, AUTO_USER_PREFIX, AUTO_LAST_PREFIX,"Yes", "Yes");

        //Send SMS
        long milliss = System.currentTimeMillis();
        String smsMessage = createAndCheckSMS (CUSTOMER_PHONE, 3);

        sendUnsubscribeSMSAndVerify(smsMessage, milliss,CUSTOMER_PHONE_FROM,UNSUBSCRIBE_MSG, unsubscribe);
        updateMemberDetails ();

    }


    public void sendSMSandVerifyUnsubscribeByLink (String provider, String CUSTOMER_PHONE, String CUSTOMER_PHONE_FROM, int numOfMessages) throws Exception {

        //Set provider
        setSMSProviderANDUnsubscribeLink(provider);

        //Verify that the member doesn't exist. If exists, one would be renamed before the test
        verifyMemberDoesntExist(CUSTOMER_PHONE);

        //Create member
        hubMemberCreator.createMemberWithEmailSMS(CUSTOMER_PHONE, AUTO_USER_PREFIX, AUTO_LAST_PREFIX,"Yes", "Yes");

        //Send SMS
        long milliss = System.currentTimeMillis();
        String smsMessage = createAndCheckSMS (CUSTOMER_PHONE, numOfMessages);

        sendUnsubscribeSMSAndVerifyByLinkHelper(smsMessage, milliss,CUSTOMER_PHONE_FROM,CUSTOMER_PHONE);
        updateMemberDetails ();

    }

    public void sendUnsubscribeSMSAndVerifyByLinkHelper(String smsMessage, long mills, String CUSTOMER_PHONE_FROM,String CUSTOMER_PHONE) throws Exception {


        String body = TwilioHelper.getFromBodyFromMessage(CUSTOMER_PHONE_FROM,mills,smsMessage);
        String link = extractLink(body,smsMessage);
        log4j.info("Unsubscribe link: " + link);

        //Click on unsubscribe link
        fillUnsubscribeLink(link,CUSTOMER_PHONE);

        //Check the user action log
        MembersService.refreshHubPage();
        String log =  MembersService.checkUserLog(UNSUBSCRIBED_SMS);
        Assert.assertEquals("User log is not as expected: " + log,log,UNSUBSCRIBED_SMS);

        //Verify AllowSMS = No
        Assert.assertEquals("Expected AllowSMS=No", "No" , MembersService.getMemberAllowSMS());
    }


    public String extractLink (String body, String smsMessage) {


        body = body.replace(smsMessage + "\n Unsbscribe: " ,"");
        return body;



    }


    private void fillUnsubscribeLink (String link, String phone) throws Exception {

        ContentService.switchToNewTabNavigateLink("http://" + link);
        ContentService.fillUnsubscribePhoneNumber(phone);
        ContentService.closeFormTab();

    }




}
