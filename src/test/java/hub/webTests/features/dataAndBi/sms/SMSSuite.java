package hub.webTests.features.dataAndBi.sms;

import hub.base.BaseHubTest;
import hub.services.AcpService;
import hub.services.NewApplicationService;
import integration.base.BaseIntegrationTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.IOException;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SendSMSILTest.class,
        SendSMSUSTest.class,
        SendSMSUKTest.class,
        SendSMSUSUnsubscribeByLinkTest.class
})
public class SMSSuite {
    public static final Logger log4j = Logger.getLogger(SMSSuite.class);


    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseIntegrationTest.init();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Remove application from user");
        try {
            AcpService.uncheckApplication(NewApplicationService.getEnvProperties().getLocationId());
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
        log4j.info("close browser after finish test");
        BaseIntegrationTest.cleanBase();
        BaseHubTest.cleanHub();
    }
}
