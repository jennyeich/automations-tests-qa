package hub.webTests.features.dataAndBi.sms;


import hub.common.objects.operations.AppSettingsPage;
import hub.common.objects.operations.CountryCode_APP_SETTINGS;
import hub.common.objects.operations.SMSConfigurationProvider;
import hub.hub1_0.services.OperationService;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class SendSMSUSTest extends SendSMSBase {


    private final String CUSTOMER_PHONE = "3377069992";
    private final String CUSTOMER_PHONE_FROM = "+1" + CUSTOMER_PHONE;
    private final String UNSUBSCRIBE_MSG = "You are unsubscribed from all alerts,  no more messages will be sent";
    private final String HELP_REPLY = "COMO Sense Alerts: For help email support@como.com Reply STOP to end. Msg&date rates may apply ";


    @BeforeClass
    public static void setup() throws Exception {

        init();
        AppSettingsPage  appSettingsPage = new AppSettingsPage();
        appSettingsPage.setCountryCode(CountryCode_APP_SETTINGS.US.toString());
        OperationService.updateAppSettings(appSettingsPage);
        setUnsubscribeMethod("reply");
    }


    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {


        @Override
        protected void starting(Description description) {

            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };


    @Test
    public void testSendSMSTwilioUSwithunsubscribe() throws Exception{

        sendSMSandVerifyUnsubscribe(SMSConfigurationProvider.COMO_TWILIO.toString(),CUSTOMER_PHONE, CUSTOMER_PHONE_FROM,UNSUBSCRIBE_MSG,"STOP " + locationId);

    }


    @Test
    public void testSendSMSTwilioUSwithHELPSMS() throws Exception{


        //Set provider
        setSMSProvider(SMSConfigurationProvider.COMO_TWILIO.toString());

        //Verify that the member doesn't exist. If exists, one would be renamed before the test
        verifyMemberDoesntExist(CUSTOMER_PHONE);

        //Create member
        hubMemberCreator.createMemberWithEmailSMS(CUSTOMER_PHONE, AUTO_USER_PREFIX, AUTO_LAST_PREFIX,"Yes", "Yes");

        //Send SMS
        long mills = System.currentTimeMillis();
        String smsMessage = createAndCheckSMS (CUSTOMER_PHONE, 3);

        sendHELPAndVerifyReply(smsMessage, mills);
        updateMemberDetails ();

    }



    @Test
    public void testSendSMSNexmoUSUnsubscribe() throws Exception{

        sendSMSandVerifyUnsubscribe(SMSConfigurationProvider.NEXMO_SHORTCODE.toString(),CUSTOMER_PHONE, CUSTOMER_PHONE_FROM,UNSUBSCRIBE_MSG,"STOP " + locationId);

    }

    @Test
    public void testSendSMSPlivoUSWithUnsubscribe() throws Exception{

        sendSMSandVerifyUnsubscribe(SMSConfigurationProvider.COMO_PLIVO.toString(),CUSTOMER_PHONE, CUSTOMER_PHONE_FROM,UNSUBSCRIBE_MSG,
                "STOP " + locationId);

    }


    private void sendHELPAndVerifyReply(String smsMessage, long mills) throws Exception {


        String fromNumber = TwilioHelper.getFromNumberFromMessage(CUSTOMER_PHONE_FROM,mills,smsMessage);
        log4j.info("SMS was send. From number: " + fromNumber);

        mills = TwilioHelper.sendSMS(CUSTOMER_PHONE_FROM,fromNumber,HELP);
        log4j.info("HELP SMS sent: " + HELP);

        fromNumber = TwilioHelper.getFromNumberFromMessage(CUSTOMER_PHONE_FROM,mills,HELP_REPLY);
        Assert.assertNotEquals("The member didn't get the HELP message", "not_found", fromNumber); //not empty means was found and got the message
        log4j.info("Member got HELP SMS: " + HELP_REPLY);


    }




}
