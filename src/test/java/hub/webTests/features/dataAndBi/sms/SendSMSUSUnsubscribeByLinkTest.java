package hub.webTests.features.dataAndBi.sms;


import hub.common.objects.operations.AppSettingsPage;
import hub.common.objects.operations.CountryCode_APP_SETTINGS;
import hub.common.objects.operations.SMSConfigurationProvider;
import hub.hub1_0.services.OperationService;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class SendSMSUSUnsubscribeByLinkTest extends SendSMSBase {


    private final String CUSTOMER_PHONE = "3377069992";
    private final String CUSTOMER_PHONE_FROM = "+1" + CUSTOMER_PHONE;

    @BeforeClass
    public static void setup() throws Exception {

        init();
        AppSettingsPage  appSettingsPage = new AppSettingsPage();
        appSettingsPage.setCountryCode(CountryCode_APP_SETTINGS.US.toString());
        OperationService.updateAppSettings(appSettingsPage);
        setUnsubscribeMethod(LINK);

    }


    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {


        @Override
        protected void starting(Description description) {

            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };


    @Test
    public void testSendSMSTwilioUSwithunsubscribeLink() throws Exception{

        sendSMSandVerifyUnsubscribeByLink(SMSConfigurationProvider.COMO_TWILIO.toString(),CUSTOMER_PHONE, CUSTOMER_PHONE_FROM,3);

    }


    @Test
    public void testSendSMSNexmoUSUnsubscribeLink() throws Exception{

        sendSMSandVerifyUnsubscribeByLink(SMSConfigurationProvider.NEXMO_SHORTCODE.toString(),CUSTOMER_PHONE, CUSTOMER_PHONE_FROM,2);

    }

    @Test
    public void testSendSMSPlivoUSWithUnsubscribeLink() throws Exception{

        sendSMSandVerifyUnsubscribeByLink(SMSConfigurationProvider.COMO_PLIVO.toString(),CUSTOMER_PHONE, CUSTOMER_PHONE_FROM,3);

    }



}
