
package hub.webTests.features.dataAndBi.sms;

import com.twilio.Twilio;
import com.twilio.base.ResourceSet;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.apache.log4j.Logger;

import java.time.Instant;


public class TwilioHelper {

    private static final String ACCOUNT_SID = "AC92e49fc4e98adda5d79d51f43d60e974";
    private static final String AUTH_TOKEN = "de863a121001539ed06edf080063a4e4";
    private static final Logger log4j = Logger.getLogger(TwilioHelper.class);


    public static String getFromNumberFromMessage (String toNumber, long mills, String body) throws Exception{

        Thread.sleep(10000);
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        ResourceSet<Message> messages = Message.reader().read().setLimit(5);

        for(Message record : messages) {
            if (record.getTo().contains(toNumber) && record.getBody().contains(body)) {
                if (mills != 0 && record.getDateSent().isAfter(mills)) {
                    log4j.info("the message is: " + record.getBody() + " from:" + record.getFrom() + " to: " + record.getTo());
                    return record.getFrom().toString();
                }
            }
        }
        return "not_found";
    }



    public static long sendSMS (String fromPhoneNumber, String toPhoneNumber, String body) {


        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        long mills = Instant.now().toEpochMilli();
        Message message = Message.creator(new PhoneNumber(toPhoneNumber),
                new PhoneNumber(fromPhoneNumber),
                body).create();

        return mills;

    }

    public static String getFromBodyFromMessage (String toNumber, long mills, String body) throws Exception{

        Thread.sleep(10000);
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        ResourceSet<Message> messages = Message.reader().read().setLimit(5);

        for(Message record : messages) {
            if (record.getTo().contains(toNumber) && record.getBody().contains(body)) {
                if (mills != 0 && record.getDateSent().isAfter(mills)) {
                    log4j.info("the message is: " + record.getBody() + " from:" + record.getFrom() + " to: " + record.getTo());
                    return record.getBody().toString();
                }
            }
        }
        return "not_found";
    }



}