package hub.webTests.features.dataAndBi;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.base.categories.hub1Categories.hub1Sanity;
import hub.common.objects.benefits.LotteryReward;
import hub.common.objects.member.NewMember;
import hub.common.objects.operations.UserPermissions;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.SmartPunchCard;
import hub.services.AcpService;
import hub.services.member.MembersService;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

import static hub.base.BaseHubTest.driver;
import static server.utils.ServerHelper.queryWithWait;

public class MemberProfileSmartActionsTest extends BaseIntegrationTest {

    public CharSequence timestamp;

    @BeforeClass
    public static void setup() throws Exception {
        init();
        log4j.info("HUB login succeeded...");

    }
    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {


        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };


    @Test
    @Category(hub1Sanity.class)
    public void testSendAnAsset() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        //Create asset
        SmartGift smartGift = hubAssetsCreator.createSmartGift(timestamp);

        //Create member
       hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        String actionTag = MembersService.performSmartActionSendAnAsset(smartGift,timestamp);

        userActionPerformedDataStoreCheck(actionTag);

    }

    @Test
    @Category(Hub1Regression.class)
    public void testAddPoints() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String givePoints = "100";
        String actionTag = MembersService.performSmartActionAddPoints(givePoints,timestamp);

        userActionPerformedDataStoreCheck(actionTag);

    }


    @Test
    public void testAddCredit() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String giveCredit = "100";
        String actionTag = MembersService.performSmartActionAddCredit(giveCredit,timestamp);

        userActionPerformedDataStoreCheck(actionTag);
    }


    @Test
    @Category(hub1Sanity.class)
    public void testTagMember() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String [] array = MembersService.performSmartActionTagMember(timestamp);

        userActionPerformedDataStoreCheck(array[1]);

    }


    @Test
    public void testUpdateExpirationDateAction() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String actionTag = MembersService.performSmartActionUpdateExpirationDate(timestamp);

        userActionPerformedDataStoreCheck(actionTag);

    }


    @Test
    public void testSendLotteryRewardAction() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        SmartGift smartGift = hubAssetsCreator.createSmartGift(timestamp);
        LotteryReward lotteryReward = hubAssetsCreator.createLotteryReward("temp" + timestamp,smartGift.getTitle(),"1",null);
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        String actionTag = MembersService.performSmartActionSendLotteryReward(lotteryReward,timestamp);

        userActionPerformedDataStoreCheck(actionTag);
    }


    @Test
    @Category(Hub1Regression.class)
    public void testPunchAPunchCardAction() throws Exception{

        AcpService.checkUserPermissionTags(UserPermissions.SMART_ACTIONS_LAYOUT_DELET);
        SmartPunchCard punchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "4");
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        MembersService.performSmartActionSendAPunchCard(punchCard,timestamp);
        String actionTag = MembersService.performSmartActionPunchAPunchCard(punchCard,timestamp);

        userActionPerformedDataStoreCheck(actionTag);

    }

    @Test
    @Category(Hub1Regression.class)
    public void testSendAssetDropDownContainsOnlyValidAssets() throws Exception{

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //create invalid smart gift
        SmartGift invalidSmartGift = hubAssetsCreator.createSmartGiftWithInvalidDates("invalid"+timestamp);
        //create valid smart gift
        SmartGift validSmartGift = hubAssetsCreator.createSmartGift("valid"+timestamp);

        //create valid punch card
        SmartPunchCard validPunchCard = hubAssetsCreator.createSmartPunchCard("valid"+timestamp, "4");
        //create invalid punch card
        SmartPunchCard invalidPunchCard = hubAssetsCreator.createSmartPunchCardWithInvalidDates("invalid"+timestamp, "4");


        int validCounter=0;
        int validSmartGiftNumber = 1;
        int validPunchCardNumber = 1;
        MembersService.tryFindAndGetMemberByPhoneNumber(newMember);
        MembersService.openSmartActionSendAssetDropDown();

        List <WebElement> myElements = driver.findElements(By.xpath("//select[@ng-model=\"fieldParent[fieldName]\"]"));
        for (WebElement e: myElements){
            validCounter =0;
            System.out.print("the elements in the list: "+e.getText());
            if (e.getText().contains(validSmartGift.getTitle()))
                validCounter ++;


        }
        Assert.assertEquals("The number of gifts in the list is not as expected",validCounter, validSmartGiftNumber);


        for (WebElement e: myElements){
            validCounter=0;
            if (e.getText().contains(validPunchCard.getTitle()))
                validCounter++;
        }
        Assert.assertEquals("The number of gifts in the list is not as expected",validCounter, validPunchCardNumber);

    }



    private void userActionPerformedDataStoreCheck(String actionTag) throws InterruptedException {

        log4j.info("start DataStore check");
        Query qAction = new Query("UserAction");
        ArrayList<Query.Filter> filters = new ArrayList<>();
        filters.add(new Query.FilterPredicate("LocationID",Query.FilterOperator.EQUAL, locationId));
        filters.add(new Query.FilterPredicate("ActionTags",Query.FilterOperator.EQUAL, actionTag));
        qAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, filters));
        List<Entity> numOfMembersInDataStore = queryWithWait("Member did not get the action ",qAction,1,dataStore);
        log4j.info("Number of members for the action: " +numOfMembersInDataStore.size());
        log4j.info("ActionTag: " +actionTag);
        Assert.assertEquals("Number of members must be equal to 1", 1, numOfMembersInDataStore.size());
    }


}
