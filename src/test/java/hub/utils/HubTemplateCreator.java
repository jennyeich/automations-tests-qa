package hub.utils;

import hub.common.objects.content.Template;
import hub.services.ContentService;

import java.io.File;

public class HubTemplateCreator extends HubCreatorBase {

    public static final String SUBJECT = "Hi @Membership.FirstName @Membership.LastName !";
    public static final String resourcesDir = "src" + File.separator + "main"+ File.separator +"resources"+ File.separator;
    public static final String HTML_FILE_PATH = resourcesDir + "email_template.html";
    public static final boolean IS_MARKETING_EMAIL_TRUE = true;
    public static final String DEFAULT_MAIL_SUBJECT = "[Advertisement] Hi %s %s !";





    public static Template createImageTemplate(String title, String subject, String link, boolean isMarketingEmail) throws Exception {
        Template template = new Template();
        template.setTitle(title);
        template.setSubject(subject);
        template.selectImageLayout();
        template.setAddLinkToImage(link);
        if(!isMarketingEmail) {
            template.setMarketingEmailOff();
        }
        ContentService.createTemplate(template);
        return template;
    }



    public static Template createImageTemplate(String title) throws Exception {
        Template template = createImageTemplate(title, SUBJECT, null, IS_MARKETING_EMAIL_TRUE);
        return template;
    }

    public static Template createImageWithTextTemplate(String title, String subject, String link, String emailTitle, String emailText, boolean isMarketingEmail) throws Exception {
        Template template = new Template();
        template.setTitle(title);
        template.setSubject(subject);
        template.selectImageWithTextLayout();
        template.setEmailTitle(emailTitle);
        template.setEmailText(emailText);
        template.setAddLinkToImage(link);
        if(!isMarketingEmail) {
            template.setMarketingEmailOff();
        }
        ContentService.createTemplate(template);
        return  template;
    }

    public static Template createImageWithTextTemplate(String title) throws Exception{
        return createImageWithTextTemplate(title, SUBJECT, null, "@Membership.FirstName , click here to get your special gift", "This is the email text.", IS_MARKETING_EMAIL_TRUE);
    }

    public static Template createCodeTemplate(String title, String subject, String filePath, boolean isMarketingEmail) throws Exception {
        Template template = new Template();
        template.setTitle(title);
        template.setSubject(subject);
        template.selectCodeLayout(filePath);
        if(!isMarketingEmail) {
            template.setMarketingEmailOff();
        }
        ContentService.createTemplate(template);
        return template;
    }

    public static Template createCodeTemplate(String title) throws Exception{
        Template template =  createCodeTemplate(title, SUBJECT, HTML_FILE_PATH, IS_MARKETING_EMAIL_TRUE);
        return template;
    }


}
