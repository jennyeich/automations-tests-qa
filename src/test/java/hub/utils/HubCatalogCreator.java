package hub.utils;

import hub.common.objects.content.information.CatalogItemPage;
import hub.common.objects.content.information.CatalogPage;
import hub.common.objects.content.information.CategoryPage;

/**
 * Created by doron on 13/05/2017.
 */
public class HubCatalogCreator extends HubCreatorBase{

    public static CatalogPage buildCatalog(String title) {

        CatalogPage catalog = new CatalogPage();
        catalog.setTitle(title);
        return catalog;
    }

    public static CategoryPage buildCategory(String title) {
        CategoryPage category = new CategoryPage();
        category.setTitle(title);
        return category;
    }

    public static CatalogItemPage buildCatalogItem(String itemTitle, String category,boolean isNew) {
        CatalogItemPage catalogItemPage = new CatalogItemPage();
        catalogItemPage.setTitle(itemTitle);
        catalogItemPage.setCategory(category);
        catalogItemPage.setAddItem(isNew);
        return catalogItemPage;
    }
}
