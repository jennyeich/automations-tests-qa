package hub.utils;

import hub.common.objects.operations.GeneralPOSSettingsPage;
import hub.common.objects.operations.MemberNotes;
import hub.hub1_0.services.OperationService;

/**
 * Created by Ariel on 9/08/2017.
 */
public class HubMemberNotesCreator extends HubCreatorBase{

    private static HubMemberNotesCreator instance;

    private HubMemberNotesCreator(){}

    public static HubMemberNotesCreator getInstance(){
        if(instance == null){
            instance = new HubMemberNotesCreator();
        }
        return instance;
    }

    public void createMemberNotes(String noteText, String target, String type) throws Exception {
        GeneralPOSSettingsPage generalPOSSettingsPage = new GeneralPOSSettingsPage();
        MemberNotes memberNotes = new MemberNotes();
        memberNotes.setMemberNotes(noteText);
        memberNotes.setTarget(target);
        memberNotes.setType(type);
        generalPOSSettingsPage.addMemberNotes(memberNotes, generalPOSSettingsPage);
    }

    public void createMutipleMemberNotes (String noteText,String target, String type, int count) throws Exception {

        GeneralPOSSettingsPage generalPOSSettingsPage = new GeneralPOSSettingsPage();

        for (int i=0; i<count; i++) {

            MemberNotes memberNotes = new MemberNotes();
            memberNotes.setMemberNotes(noteText +  " " + i);
            memberNotes.setTarget(target);
            memberNotes.setType(type);
            generalPOSSettingsPage.addMemberNotes(memberNotes, generalPOSSettingsPage);
        }

        OperationService.updatePOSSettings(generalPOSSettingsPage);
    }


}
