package hub.utils;

import hub.common.objects.common.ActionsToPerform;
import hub.common.objects.oldObjects.*;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;

/**
 * Created by Goni on 6/8/2017.
 */
public class HubOldAutomationCreator extends HubCreatorBase{

    public static OldAction buildTagMemberOldAction(String text, OldAutomation automation, TagOptions tagOptions) throws Exception {
        OldAction action = new OldAction();
        action.setPerformTheAction(SmartAutomationActions.TagMember.toString());
        TagMemberOldAction tagMember = new TagMemberOldAction(automation);
        tagMember.setTagText(text);
        tagMember.setAction(tagOptions.toString());
        action.setTagMember(tagMember);
        return action;
    }

    public static OldAction buildPointsCreditAccumulationOldAction(OldAutomation automation,String defaultMultiplier,String defaultConstant, BudgetTypeOldAction budgetType) throws Exception {

        OldAction action = new OldAction();
        action.setPerformTheAction(ActionsToPerform.POINT_CREDIT_ACCUMULATION.toString());
        CreditPointsAccumulationAction accumulationAction = new CreditPointsAccumulationAction(automation);
        accumulationAction.setDefaultMultiplier(defaultMultiplier);
        accumulationAction.setDefaultConstant(defaultConstant);
        accumulationAction.setBudgetType(budgetType.name());
        action.setAccumulationOldAction(accumulationAction);
        return action;
    }

    public static OldCondition createConditionPurchaseInRange(OldCondition oldCondition, String min, String max) throws Exception{
        ConditionPurchaseInRange cond = new ConditionPurchaseInRange();
        cond.setSumMin(min);
        cond.setSumMax(max);
        oldCondition.setPurchaseInRange(cond);
        return oldCondition;
    }
}
