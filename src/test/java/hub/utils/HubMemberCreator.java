package hub.utils;

import com.google.appengine.api.datastore.Key;
import com.mashape.unirest.http.HttpResponse;
import hub.common.objects.common.Gender;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.services.member.MembersService;
import server.v2_8.DataStoreKeyUtility;
import utils.AutomationException;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * Created by doron on 11/05/2017.
 */
public class HubMemberCreator extends HubCreatorBase{

    public NewMember createMember(String firstName,String lastName) throws InterruptedException, AutomationException {
        NewMember newMember = buildMember(String.valueOf(System.currentTimeMillis()), firstName, lastName);
        MembersService.createNewMember(newMember);
        log4j.info("New member with phone number was created :  "+newMember.getPhoneNumber());
        return newMember;
    }
    public static NewMember createMember(CharSequence timestamp, String userFName, String userLName) throws Exception {

        NewMember newMember = buildMember(timestamp, userFName, userLName);
        MembersService.createNewMember(newMember);
        log4j.info("New member with phone number was created :  "+newMember.getPhoneNumber());
        return newMember;
    }

    public static NewMember createMemberWithGenericInteger(CharSequence timestamp, String userFName, String userLName,int genericInt) throws Exception {

        NewMember newMember = buildMember(timestamp, userFName, userLName);
        newMember.setGenericInteger1(String.valueOf(genericInt));
        MembersService.createNewMember(newMember);
        log4j.info("New member with phone number was created :  "+newMember.getPhoneNumber());
        return newMember;
    }

    public static NewMember createMemberDefaultConsent(CharSequence timestamp, String userFName, String userLName) throws Exception {

        NewMember newMember = buildMember(timestamp, userFName, userLName);
        MembersService.createMemberDefaultConsent(newMember);
        log4j.info("New member with phone number was created :  "+newMember.getPhoneNumber());
        return newMember;
    }

    public static NewMember createMemberWithGovId(String firstName, String lastName) throws InterruptedException, AutomationException{
        NewMember newMember = buildMember(String.valueOf(System.currentTimeMillis()), firstName, lastName);
        newMember.setGovId(String.valueOf(System.currentTimeMillis()));
        MembersService.createNewMember(newMember);
        log4j.info("New member with phone number was created :  "+newMember.getPhoneNumber());
        return newMember;
    }

    public static NewMember createMemberWithAllPIIFields(String firstName, String lastName) throws InterruptedException, AutomationException{


        NewMember newMember = buildMember(String.valueOf(System.currentTimeMillis()), firstName, lastName);
        newMember.setGovId(String.valueOf(System.currentTimeMillis()));
        newMember.setAddressFloor(String.valueOf(System.currentTimeMillis()));
        newMember.setAddressLine1(String.valueOf(System.currentTimeMillis()));
        newMember.setAddressLine2(String.valueOf(System.currentTimeMillis()));
        newMember.setAddressHome(String.valueOf(System.currentTimeMillis()));
        newMember.setAddressStreet(String.valueOf(System.currentTimeMillis()));
        newMember.setExtClubID(String.valueOf(System.currentTimeMillis()));
        newMember.setGenericString1(String.valueOf(System.currentTimeMillis()));

        MembersService.createNewMember(newMember);
        log4j.info("New member with phone number was created :  "+newMember.getPhoneNumber());
        return newMember;


    }

    public static NewMember buildMember(CharSequence timestamp, String userFName, String userLName) {
        NewMember newMember = new NewMember();
        newMember.setFirstName(userFName+timestamp);
        newMember.setLastname(userLName+timestamp);
        newMember.setPhoneNumber(timestamp.toString());
        newMember.setGender(Gender.FEMALE.toString());
        newMember.setBirthday(getRandomDate());
        log4j.info("creating new member: " + newMember.getFirstName() + " " + newMember.getLastName());
        newMember.setEmail(newMember.getFirstName() + timestamp + MAIL_SUFFIX );
        return newMember;
    }
    public NewMember createMemberWithFixedName(String userFName, String userLName) throws Exception {
        NewMember newMember = buildMember(userFName, userLName);
        MembersService.createNewMember(newMember);
        log4j.info("New member with phone number was created :  "+newMember.getPhoneNumber());
        return newMember;
    }
    private NewMember buildMember(String userFName, String userLName) {
        NewMember newMember = new NewMember();
        newMember.setFirstName(userFName);
        newMember.setLastname(userLName);
        newMember.setPhoneNumber(String.valueOf(System.currentTimeMillis()));
        newMember.setGender(Gender.FEMALE.toString());
        newMember.setBirthday(getRandomDate());
        log4j.info("creating new member: " + newMember.getFirstName() + " " + newMember.getLastName());
        newMember.setEmail(newMember.getFirstName() + MAIL_SUFFIX);
        return newMember;
    }
    public static Key getMemberKeyToUserKey() throws InterruptedException {
        //wait until datastore updated
        TimeUnit.SECONDS.sleep(10);
        DataStoreKeyUtility dataStoreKeyUtility = new DataStoreKeyUtility();
        String uKey = MembersService.getUserKey();
        log4j.info("get user key : " + uKey);
        HttpResponse response = dataStoreKeyUtility.SendRequest(uKey);
        return dataStoreKeyUtility.userKey((String)response.getBody());
    }


    public static Key convertUSerKeyToDataStoreKey (String userKey) {

        DataStoreKeyUtility dataStoreKeyUtility = new DataStoreKeyUtility();
        HttpResponse response = dataStoreKeyUtility.SendRequest(userKey);
        return dataStoreKeyUtility.userKey((String)response.getBody());

    }
    public NewMember createMemberWithBirthday(CharSequence timestamp,String userFName,String userLName,Calendar birthday) throws Exception {

        NewMember newMember = buildMember(timestamp, userFName, userLName);
        newMember.setBirthday(getDate(birthday));
        MembersService.createNewMember(newMember);
        log4j.info("New member with phone number was created :  "+newMember.getPhoneNumber());
        log4j.info("New member with birthday date :  "+newMember.getBirthday());
        return newMember;
    }

    public NewMember createMemberWithEmailAddress(CharSequence timestamp, String userFName, String userLName, String emailAddress) throws Exception{
        NewMember newMember = buildMember(timestamp, userFName, userLName);
        newMember.setEmail(emailAddress);
        MembersService.createNewMember(newMember);
        log4j.info("New member with phone number was created :  "+newMember.getPhoneNumber());
        log4j.info("New member with email address :  "+newMember.getEmail());
        return newMember;
    }

    public NewMember createMemberWithExpirationDate(CharSequence timestamp,String userFName,String userLName,Calendar expiration) throws Exception {

        NewMember newMember = buildMember(timestamp, userFName, userLName);
        newMember.setMembershipExpiration(getDate(expiration));
        MembersService.createNewMember(newMember);
        log4j.info("New member with phone number was created :  "+newMember.getPhoneNumber());
        log4j.info("New member with expiration date :  "+newMember.getMembershipExpiration());
        return newMember;
    }

    public NewMember createMemberWithEmailSMS (CharSequence timestamp,String userFName,String userLName,String allowEmail, String allowSMS) throws Exception {

        NewMember newMember = buildMember(timestamp, userFName, userLName);
        if (allowEmail!="")
            newMember.setAllowEmail(allowEmail);
        if (allowSMS!="")
            newMember.setAllowSMS(allowSMS);
        MembersService.createNewMember(newMember);
        log4j.info("New member with phone number was created :  "+newMember.getPhoneNumber());
        return newMember;
    }

    public void deleteMember (MemberDetails memberDetails) throws Exception{

        MembersService.deleteMember(memberDetails);
    }


    public void createMembers(int numOfMembers, String userName, String lastName) throws Exception {
        for (int i=0;i<numOfMembers;i++){
            createMemberWithFixedName(userName,lastName+String.valueOf(System.currentTimeMillis()));
        }
    }
}

