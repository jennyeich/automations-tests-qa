package hub.utils;

import hub.common.objects.common.Days;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.DaysTime;
import org.apache.log4j.Logger;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by doron on 11/05/2017.
 */
public class HubCreatorBase {
    public final static Logger log4j = Logger.getLogger(HubAssetsCreator.class);
    public static final String MAIL_SUFFIX = "@como.com";
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyyHH:mm:ss z");
    /**
     * Generate DaysTime object with default hours (12:00 AM ,11:59:59 PM)
     */
    public DaysTime generateSingleDay(Calendar day) {
        DaysTime daysTime = new DaysTime();
        daysTime.setIs(getDayOfWeek(day));
        return daysTime;
    }

    public String getDayOfWeek(Calendar day) {
        String[] strDays = new String[] {Days.SUNDAY.toString(), Days.MONDAY.toString(), Days.TUESDAY.toString(),
                Days.WEDNESDAY.toString(), Days.THURSDAY.toString(),
                Days.FRIDAY.toString(), Days.SATURDAY.toString() };
        System.out.println("day of week by calander: " + day.get(Calendar.DAY_OF_WEEK));
        return strDays[day.get(Calendar.DAY_OF_WEEK) - 1];
    }


    public static String getDate(Calendar cal) {
        String day = dateFormat.format(cal.getTime());
        String date = day.substring(0,8);
        return date;
    }

    public String getTime(Calendar cal){
        TimeZone timeZone = TimeZone.getTimeZone(System.getProperty("user.timezone"));
        dateFormat.setTimeZone(timeZone);
        String day = dateFormat.format(cal.getTime());
        log4j.info("day date: " + day);
        String time = day.substring(8,day.indexOf(" "));
        return time;
    }
    public static String getRandomDate(){
        TimeZone timeZone = TimeZone.getTimeZone(System.getProperty("user.timezone"));
        Calendar randomDate = Calendar.getInstance(timeZone);
        randomDate.add(Calendar.YEAR, -20);
        log4j.info("Time zone: " + timeZone);
        String date = getDate(randomDate);
        log4j.info("generate random date: " +date);
        return date;
    }

    public Date dateDaysBack(int days){

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, -24*days);
        Date date = cal.getTime();
        return date;

    }

    public Date dateYearBack (){

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -1);
        Date date = cal.getTime();
        return date;

    }

    public Date dateHoursBack(int hours){

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, -hours);
        Date date = cal.getTime();
        return date;

    }

    public Date date1WeekBack (){

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, - (24*7));
        Date date = cal.getTime();
        return date;

    }

    public Date getMay012018Date(){

        Calendar cal = Calendar.getInstance();
        cal.set(2018,4,01);
        Date date = cal.getTime();
        return date;

    }

    public Date dateMinutesBack (int minutes){

        Calendar cal = Calendar.getInstance();
        log4j.info("get current Time: " + cal.getTime());
        cal.add(Calendar.MINUTE, - minutes);
        Date date = cal.getTime();
        return date;
    }

}
