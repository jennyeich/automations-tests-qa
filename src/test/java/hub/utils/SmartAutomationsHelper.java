package hub.utils;

import com.google.appengine.api.datastore.Key;
import com.mashape.unirest.http.HttpResponse;
import hub.common.objects.common.Gender;
import hub.common.objects.common.TextAreaParameters;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.common.objects.member.PerformActionOnResults;
import hub.common.objects.member.PerformAnActionOnMember;
import hub.common.objects.member.performActionObjects.AddPointsAction;
import hub.common.objects.member.performActionObjects.TagUntagOperation;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.*;
import hub.common.objects.smarts.smarObjects.settings.ISettings;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Condition;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.apache.commons.collections.map.HashedMap;
import org.apache.log4j.Logger;
import org.junit.Assert;
import server.common.IServerResponse;
import server.common.InvokeEventBasedActionRuleBuilder;
import server.common.Response;
import server.utils.ServerHelper;
import server.v2_8.*;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.GiftShopItem;
import utils.PropsReader;

import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by Goni on 9/27/2017.
 */

public class SmartAutomationsHelper {


    public static String apiKey;
    public static String locationId;
    public static String serverToken;

    public static final String AUTO_USER_PREFIX = "auto_user_";
    public static final String AUTO_LAST_PREFIX = "auto_last_";
    public static final String AUTOMATION_RULE_PREFIX = "automation_";
    public static final String MAIL_SUFFIX = "@como.com";


    public static HubAssetsCreator hubAssetsCreator = new HubAssetsCreator();
    public static HubMemberCreator hubMemberCreator = new HubMemberCreator();

    public static final Logger log4j = Logger.getLogger(SmartAutomationsHelper.class);

    public Settings settings;
    public CharSequence timestamp;
    public static SmartGift smartGift;


    public SmartAutomationsHelper(String locationId, String apiKey, String serverToken) {
        this.locationId = locationId;
        this.apiKey = apiKey;
        this.serverToken = serverToken;

    }

    public String getAutomationRuleID(ISettings settings) throws Exception {
        String autoURL = SmartAutomationService.getAutomationURL(settings);
        String ruleId = AUTOMATION_RULE_PREFIX + autoURL;
        log4j.info("AutomationRuleID : "+ ruleId);
        return ruleId;
    }

    /**
     * Method creates smart automation scenario with names for scenario ,actions and conditions
     */
    public void createSmartAutomationScenario(String scenarionName, SmartAutomation automation, List<Condition> condList, boolean isOR, Map<SmartAutomationActions,String> actionMap,String assetTitle,String tag) throws Exception {

        buildSmartAutomationScenario(scenarionName,automation,condList,isOR,actionMap,assetTitle,tag);
        SmartAutomationService.createSmartAutomation(automation);

    }


    public void buildSmartAutomationScenario(String scenarionName, SmartAutomation automation, List<Condition> condList, boolean isOR, Map<SmartAutomationActions,String> actionMap,String assetTitle,String tag) throws Exception {
        log4j.info("creating scenario");
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        scenario.setScenarioName(scenarionName);
        automation.addScenario(scenario);

        for (Condition cond : condList) {
            scenario.addCondition(cond, automation);
        }
        if (isOR)
            scenario.setOrOperator();

        for (SmartAutomationActions action : actionMap.keySet()) {

            SmartAutomationAction smartAction = new SmartAutomationAction();
            SmartAutomationAction updateSmartAction = getAction(action.toString(), smartAction, automation,assetTitle,tag);
            String actionName = actionMap.get(action);
            updateSmartAction.setActionName(actionName);
            scenario.addAction(updateSmartAction, automation);
        }

    }



    public void createSmartAutomationScenario(SmartAutomation automation, java.util.List<Condition> condList, boolean isOR, java.util.List<SmartAutomationActions> actionList,String assetTitle,String tag) throws Exception {

        Map<SmartAutomationActions,String> actionMap = new HashedMap();
        for (SmartAutomationActions actionName : actionList) {

            actionMap.put(actionName, null);
        }

        buildSmartAutomationScenario(null, automation,condList,isOR, actionMap,assetTitle,tag);
        SmartAutomationService.createSmartAutomation(automation);
    }


    public SmartAutomationScenario createScenarioWithTagAction(ISmartAutomationCounter automation, String tag) throws Exception {
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getAction(SmartAutomationActions.TagMember.toString(), smartAction, automation,"",tag);
        scenario.addAction(updateSmartAction, automation);
        return scenario;
    }


    public SmartAutomationAction getAction(String actionName, SmartAutomationAction smartAction, ISmartAutomationCounter automation, String objectName,String tag) throws Exception {

        SmartAutomationAction action = new SmartAutomationAction();

        if (SmartAutomationActions.SendAnAsset.toString().equals(actionName)) {
            action = hubAssetsCreator.getSendAssetAction(smartAction, automation, objectName);

        } else if(SmartAutomationActions.SendALotteryReward.toString().equals(actionName)) {
            action =  hubAssetsCreator.getSendLotteryRewardAction(smartAction, automation,objectName);

        }else if (SmartAutomationActions.TagMember.toString().equals(actionName)) {
            action = hubAssetsCreator.getTagMemberAction(smartAction, automation, tag, TagUntagOperation.Tag);

        }  else if (SmartAutomationActions.SendALotteryReward.toString().equals(actionName)) {
            action = hubAssetsCreator.getSendLotteryRewardAction(smartAction, automation, objectName);

        }else if (SmartAutomationActions.AddSetNumberofPoints.toString().equals(actionName)) {
            action = hubAssetsCreator.getAddPointsAction(smartAction, automation, AmountType.Credit);

        } else if (SmartAutomationActions.PunchThePunchCard.toString().equals(actionName)) {
            action = hubAssetsCreator.getPunchThePunchCardAction(smartAction, automation, objectName, "1");

        }else if (SmartAutomationActions.SendAPushNotification.toString().equals(actionName)) {
            String txtMessage =  "@" + TextAreaParameters.Membership_FirstName + " hello. You got push message.";
            action = hubAssetsCreator.getSendPushNotificationAction(smartAction, automation,txtMessage, "Y");

        }else if (SmartAutomationActions.SendTextSMS.toString().equals(actionName)) {
            String txtMessage =  "@" + TextAreaParameters.Membership_FirstName + " hello. You got sms.";
            action = hubAssetsCreator.getSendSMSAction(smartAction, automation,txtMessage);

        }else if (SmartAutomationActions.SendPopUPMessage.toString().equals(actionName)) {
            String txtMessage =  "@" + TextAreaParameters.Membership_FirstName + " hello. You got popup message.";
            action = hubAssetsCreator.getSendPopUpMessageAction(smartAction, automation,"test message",txtMessage);

        }else if(SmartAutomationActions.SendEmail.toString().equals(actionName)) {
            action = hubAssetsCreator.getSendEmailAction(smartAction, automation, objectName);
        }else if(SmartAutomationActions.RegisterMembership.toString().equals(actionName)) {
            action = hubAssetsCreator.getRegisterAction(smartAction, automation);
        }else if (SmartAutomationActions.ExportEvent.toString().equals(actionName)) {
            String DESTINATION_URL = "https://services-dot-comoqa.appspot.com/echo";
            action = hubAssetsCreator.getExportEventAction(smartAction, automation, smartAction.getActionName(), DESTINATION_URL);
            action.setActionName(actionName);
        }
        return action;
    }

    public CustomOccurrences setCustomOccurences(SmartAutomation automation, ISmartAction smartAction, String occurancesTimes, int forEach, ForEachTotalType totalType, Integer limitTimes){
        Occurrences occurrences = new Occurrences(automation);
        occurrences.setOccurrences_type(OccurrencesType.custom.toString());
        occurrences.setOccurrences_times(occurancesTimes);
        CustomOccurrences customOccurrences = new CustomOccurrences(automation);
        customOccurrences.setCustomOccurrencesForEach(String.valueOf(forEach));
        customOccurrences.setCustomOccurrencesForEachTotal(totalType.toString());

        if(limitTimes != null){//limit is on the result of the divided result(total/foreach)
            customOccurrences.setLimitActionOccurrencesPerAutomation("YES");
            customOccurrences.setLimitToTimes(limitTimes.toString());
        }
        occurrences.setCustomOccurrences(customOccurrences);
        smartAction.setOccurrences(occurrences);
        return customOccurrences;
    }



    //Update member details with all relevant fields for the tests
    public void performUpdateMemberDetails(String phoneNumber) throws Exception {
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setPhoneNumber(phoneNumber);
        memberDetails.setFirstName(AUTO_USER_PREFIX + phoneNumber);
        memberDetails.setLastname(AUTO_LAST_PREFIX + phoneNumber);
        memberDetails.setGender(Gender.MALE.toString());
        memberDetails.setEmail(AUTO_USER_PREFIX + phoneNumber + BaseAutomationTest.MAIL_SUFFIX);
        Calendar birthday = Calendar.getInstance();
        birthday.add(Calendar.YEAR, -20);
        memberDetails.setBirhday(hubAssetsCreator.getDate(birthday));
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);
        memberDetails.setMembershipExpiration(hubAssetsCreator.getDate(calNextYear));
        MembersService.updateMember(memberDetails);
    }

    //Returns the key for the data store according to the userKey in the hub
    public Key getKey(String userKey){
        log4j.info("userKey from hub: " + userKey);
        DataStoreKeyUtility uKey = new DataStoreKeyUtility();
        HttpResponse response = uKey.SendRequest(userKey);
        return uKey.userKey((String)response.getBody());

    }

    //Simulates the user join the club from the client application using the token that was received from signOn
    public String joinClubWithUserToken(String phoneNumber, String userToken) {

        //Join Club
        JoinClub joinClub = new JoinClub();
        joinClub.setPhoneNumber(phoneNumber);
        joinClub.setLocationID(locationId);
        if(Boolean.valueOf(PropsReader.getPropValuesForEnv(Consent.REGISTER_MEMBERS_BY_CONSENT)).booleanValue())
            joinClub.setOrigin(ServerHelper.APP);
        IServerResponse response = joinClub.sendRequestByTokenAndLocation(userToken, locationId, JoinClubResponse.class);
        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }
        String userKey = (String) ((JoinClubResponse)response).getRawObject().get("UserKey");
        log4j.info("Join club and get user key...");
        return userKey;
    }


    public void performPurchaseAsset(String assetTemplateKey, String itemID, String userToken) {
        log4j.info("perform purchase asset");
        PurchaseAsset purchaseAsset = new PurchaseAsset();
        GiftShopItem giftShopItem = new GiftShopItem();
        giftShopItem.setAssetKey(assetTemplateKey);
        giftShopItem.setPrice("2");
        giftShopItem.setItemID(itemID);
        purchaseAsset.setItem(giftShopItem);

        IServerResponse response = purchaseAsset.sendRequestByTokenAndLocation(userToken,locationId, PurchaseAssetResponse.class);
        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }
    }

    public IServerResponse getPerformPurchaseAssetRequest(String assetTemplateKey, String itemID, String userToken) {
        log4j.info("perform purchase asset");
        PurchaseAsset purchaseAsset = new PurchaseAsset();
        GiftShopItem giftShopItem = new GiftShopItem();
        giftShopItem.setAssetKey(assetTemplateKey);
        giftShopItem.setPrice("2");
        giftShopItem.setItemID(itemID);
        purchaseAsset.setItem(giftShopItem);

        return purchaseAsset.sendRequestByTokenAndLocation(userToken,locationId, PurchaseAssetResponse.class);
    }

    //unregister member which created and then when we update him,
    // the trigger is activate and should run the automation for register again
        public void unregisterMemberAndUpdate(NewMember member) throws Exception {

        //unregister member and send the tag to him
        unregisterMember();
        MemberDetails memberDetails = new MemberDetails();
        member.setFirstName(member.getFirstName() +"_updated");
        MembersService.updateMember(memberDetails);
    }

    public void unregisterMember() {
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();
        performUnregistered(userKey,membershipKey);
    }

    private void performUnregistered(String userKey,String membershipKey) {
        try {
            InvokeEventBasedActionRuleBuilder invokeEventBasedActionRuleBuilder = new InvokeEventBasedActionRuleBuilder();
            server.common.InvokeEventBasedActionRule invokeEventBasedActionRule = invokeEventBasedActionRuleBuilder.buildUnregisterRequest(membershipKey, userKey, locationId);
            IServerResponse response = invokeEventBasedActionRule.SendRequestByToken(serverToken, Response.class);

            // make sure the response is not Error response
            if (response instanceof ZappServerErrorResponse) {
                Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
            }
            log4j.info("User with userKey: " + userKey + " was unregistered.");
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);

        }

    }

}

