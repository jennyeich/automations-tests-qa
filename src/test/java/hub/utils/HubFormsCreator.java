package hub.utils;

import hub.common.objects.common.FormFieldAnswerType;
import hub.common.objects.content.forms.*;
import hub.services.ContentService;

/**
 * Created by taya.ashkenazi on 6/6/17.
 */
public class HubFormsCreator extends HubCreatorBase{

    public static final String AUTO_TITLE_PREFIX = "auto_title";
    public static final String AUTO_DESCRIPTION_PREFIX = "auto_description";
    public static final String AUTO_RECIPIENTS = "automationcomo@gmail.com";
    public static final String AUTO_THANK_YOU_TITLE_PREFIX = "auto_thank_you_title";
    public static final String AUTO_THANK_YOU_CONTENT_PREFIX = "auto_thank_you_message";

    private static FormPage buildForm(String title, String recipients, String messageContent, String messageTitle, String description) {
        FormPage formPage = new FormPage();
        formPage.setTitle(title);
        formPage.setRecipientsOfCompletedForms(recipients);
        formPage.setThankYouMessageContent(messageContent);
        formPage.setThankYouMessageTitle(messageTitle);
        formPage.setDescription(description);
        log4j.info("creating new form: " + formPage.getTitle());
        return formPage;
    }

    //Add the given field to the form (according to the given Field type
    public static FormFieldPage addFormField(String timestamp, FormFieldAnswerType fieldType, String mandatory) {
        FormFieldPage fieldPage = new FormFieldPage();
        fieldPage.setTitle(fieldType.toString() + timestamp);
        fieldPage.setDescription(fieldType.toString());
        fieldPage.setIsMandatory(mandatory);
        fieldPage.setAnswerType(fieldType.toString());
        return fieldPage;
    }

    public static FormPage createForm(CharSequence timestamp,String title, String recipients, String messageContent, String messageTitle, String description) throws Exception {
        FormPage page = new FormPage();
        page = (FormPage)buildPage(page,title + timestamp, recipients, messageContent, messageTitle, description);
        ContentService.createNewForm((FormPage)page);
        log4j.info("New form was created : " + page.getTitle());
        return page;
    }


    private static IForm buildPage(IForm page, String title, String recipients, String messageContent, String messageTitle, String description) {
        page.setTitle(title);
        page.setRecipientsOfCompletedForms(recipients);
        page.setThankYouMessageContent(messageContent);
        page.setThankYouMessageTitle(messageTitle);
        page.setDescription(description);
        log4j.info("creating new page: " + page.getTitle());
        return page;
    }

    public static SurveyPage createSurvey(String timestamp, String title, String recipients, String messageContent, String messageTitle, String description) throws Exception {
        SurveyPage page = new SurveyPage();
        page = (SurveyPage)buildPage(page,title + timestamp, recipients, messageContent, messageTitle, description);
        ContentService.createNewSurvey((SurveyPage)page);
        log4j.info("New Survey was created : " + page.getTitle());
        return page;
    }

    //Add the given question to the survey (according to the given Field type)
    public static SurveyQuestionPage addQuestion(String timestamp, FormFieldAnswerType fieldType, String mandatory) {
        SurveyQuestionPage questionPage = new SurveyQuestionPage();
        questionPage.setTitle(fieldType.toString() + timestamp);
        questionPage.setDescription(fieldType.toString());
        questionPage.setIsMandatory(mandatory);
        questionPage.setAnswerType(fieldType.toString());
        return questionPage;
    }


}
