package hub.utils;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.ISmartAutomationTest;
import hub.common.objects.benefits.LotteryReward;
import hub.common.objects.benefits.PointShopItem;
import hub.common.objects.benefits.PointShopPage;
import hub.common.objects.benefits.RewardOptions;
import hub.common.objects.benefits.giftCard.GiftCard;
import hub.common.objects.benefits.giftCard.GiftCardBulkCode;
import hub.common.objects.benefits.redeemCode.*;
import hub.common.objects.common.*;
import hub.common.objects.member.performActionObjects.TagUntagOperation;
import hub.common.objects.operations.JoiningCode;
import hub.common.objects.smarts.*;
import hub.common.objects.smarts.smarObjects.Scenario;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.*;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftActions;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.dealItemCode.AddItemCode;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.AddDiscountAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.AmountOffNumber;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.PercentOff;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.SetDiscountOnItems;
import hub.common.objects.smarts.smarObjects.settings.ISettings;
import hub.common.objects.smarts.smarObjects.triggers.BirthdayAnniversaryAutomationTriggers;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.common.objects.benefits.smartGiftActions.ClubDeal;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.BirthdayAnniversarySettings;
import hub.services.NewApplicationService;
import hub.services.assets.AssetService;
import hub.services.member.MembersService;
import org.junit.Assert;
import server.common.asset.AssignAsset;
import server.internal.GetLocationState;
import server.internal.GetLocationStateResponse;

import java.util.*;

/**
 * Created by goni on 2/6/2017.
 */
public class HubAssetsCreator extends HubCreatorBase{

    public LotteryReward createLotteryReward(LotteryReward lotteryReward,String assetTitle,CharSequence timestamp) throws Exception {
        if(lotteryReward == null)
            lotteryReward =createLotteryReward(timestamp,assetTitle,"5","5");
        if(lotteryReward.getMaxRewards().equals("1")){
            //create lottery with max use to 5 for all other tests
            lotteryReward = createLotteryReward(timestamp,assetTitle,"5","5");
        }
        return lotteryReward;
    }

    public SmartBirthdayAndAnniversaryAutomation createSmartBirthdayAnniversaryAutomation(BirthdayAnniversaryAutomationTriggers triggers,String automationName,BirthdayAnniversarySettings birthdayAnniversarySettings) throws Exception {

        return createSmartBirthdayAutomation(triggers,automationName,birthdayAnniversarySettings);
    }

    public String addItemToPointShopAndGetVersionAfterPublish(String assetName,String locationId) throws Exception {
        PointShopPage pointShop = new PointShopPage();
        AssetService.addItemToPointShop(pointShop,assetName);
        NewApplicationService.publishApp();
        GetLocationState getLocationState = new GetLocationState(locationId);
        GetLocationStateResponse response = (GetLocationStateResponse) getLocationState.sendRequest(GetLocationStateResponse.class);
        ArrayList<Map<String, String>> resource =  response.getMainResources().getResource();
        for (int i = 0; i < resource.size(); i++) {
            Map map = resource.get(i);
            if(map.get("id").equals("Menu")) {
                String version = (String) map.get("Version");
                return version;
            }
        }
        return "0";
    }


    public SmartAutomation createSmartAutomation(ISmartAutomationTest test, ISettings settings) throws Exception {

        return createSmartAutomation(test.getTrigger(), test.getAutomationName(), settings);
    }

    public SmartAutomationScenario createSmartAutomationScenario(SmartAutomation automation, java.util.List<Condition> condList, boolean isOR ) throws Exception{

        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        for (Condition cond : condList) {
            scenario.addCondition(cond, automation);
        }

        if(isOR)
            scenario.setOrOperator();

        return scenario;
    }

    public SmartAutomationAction getTagMemberAction(SmartAutomationAction smartAction, ISmartAutomationCounter automation, String tag, TagUntagOperation operation) throws Exception{

        smartAction.setPerformTheAction(SmartAutomationActions.TagMember.toString());
        TagMember tagMember = new TagMember(automation);
        tagMember.setTagText(tag);
        if(TagUntagOperation.Tag.equals(operation))
            tagMember.setTag_member("Y");
        else
            tagMember.setUntag_member("Y");
        smartAction.setTagMember(tagMember);
        return smartAction;
    }


    public SmartAutomationAction getSendAssetAction(SmartAutomationAction smartAction,ISmartAutomationCounter automation,String giftTitle) throws Exception{
        smartAction.setPerformTheAction(SmartAutomationActions.SendAnAsset.toString());
        SendAsset action = new SendAsset(automation);
        action.setAssetName(giftTitle);
        smartAction.setSendAsset(action);
        return smartAction;
    }

    public SmartAutomationAction getPunchThePunchCardAction(SmartAutomationAction smartAction,ISmartAutomationCounter automation,String punchTitle,String numOfPunches) throws Exception{
        smartAction.setPerformTheAction(SmartAutomationActions.PunchThePunchCard.toString());
        PunchAPunchCard action = new PunchAPunchCard(automation);
        action.setNumOfPunches(numOfPunches);
        action.setAssetName(punchTitle);
        smartAction.setPunchAPunchCard(action);
        return smartAction;
    }

    public SmartAutomationAction getPunchThePunchCardActionWithTiming(SmartAutomationAction smartAction,ISmartAutomationCounter automation,String punchTitle,String numOfPunches) throws Exception{
        smartAction.setPerformTheAction(SmartAutomationActions.PunchThePunchCard.toString());
        PunchAPunchCard action = new PunchAPunchCard(automation);
        action.setNumOfPunches(numOfPunches);
        action.setAssetName(punchTitle);
       // action.setTiming
        Timing timing = new Timing(automation);
        timing.setTimingTypes(TimingTypes.DELAYED.toString());
        timing.setValueInput("40");
        timing.setTimingUnits(TimingUnit.SECONDS);
        action.setTiming(timing);
        smartAction.setPunchAPunchCard(action);
        return smartAction;
    }

    public SmartAutomationAction getAddPointsAction(SmartAutomationAction smartAction,ISmartAutomationCounter automation,AmountType type) throws Exception{
        return getAddPointsAction(smartAction,automation, type,"10");
    }

    public SmartAutomationAction getAddPointsAction(SmartAutomationAction smartAction,ISmartAutomationCounter automation,AmountType type,String points) throws Exception{
        smartAction.setPerformTheAction(SmartAutomationActions.AddSetNumberofPoints.toString());
        AddPoints action = new AddPoints(automation);
        action.setNumOfPoints(points);
        action.setAmountType(type.toString());//e.g: AmountType.Credit
        smartAction.setAddPoints(action);
        return smartAction;
    }

    public SmartAutomationAction getSendLotteryRewardAction(SmartAutomationAction smartAction, ISmartAutomationCounter automation, String lotteryTitle) throws Exception{
        smartAction.setPerformTheAction(SmartAutomationActions.SendALotteryReward.toString());
        SendLotteryReward action = new SendLotteryReward(automation);
        action.setAssetName(lotteryTitle);
        smartAction.setLotteryReward(action);
        return smartAction;
    }

    public SmartAutomation createSmartAutomation(ITrigger triggers, String automationName, ISettings settings) throws Exception{

        SmartAutomation automation = new SmartAutomation();
        settings.setAutomationName(automationName); //Automation name e.g:"Receives or Uses Points_"+ timestamp
        settings.setTrigger(triggers);
        automation.setSettings(settings);
        log4j.info("New smart automation was created :  "+ automationName);
        Thread.sleep(4000);
        return automation;
    }


    public SmartBirthdayAndAnniversaryAutomation createSmartBirthdayAutomation(BirthdayAnniversaryAutomationTriggers triggers, String automationName, BirthdayAnniversarySettings settings) throws Exception{

        SmartBirthdayAndAnniversaryAutomation automation = new SmartBirthdayAndAnniversaryAutomation();
        settings.setAutomationName(automationName); //Automation name e.g:"Receives or Uses Points_"+ timestamp
        settings.setTrigger(triggers);
        automation.setBirthdayAnniversarySettings(settings);
        log4j.info("New smart automation was created :  "+ automationName);
        return automation;
    }

    public StringOperator getStringOperator(SmartAutomation automation, StringOperator.String_Equals_enum operation) {
        StringOperator equal = new StringOperator();
        equal.setOperator(automation,operation.toString());
        return equal;
    }

    public StringOperator getStringBirthdayAnniversaryOperator(SmartBirthdayAndAnniversaryAutomation smartBirthdayAndAnniversaryAutomation, TagOperator.Tags_Equals_enum operation) {
        StringOperator equal = new StringOperator();
        equal.setBirthdayAnniversaryOperator(smartBirthdayAndAnniversaryAutomation,operation.toString());
        return equal;
    }

    public StringOperator getStringAssetOperator(ISmartAssetCounter smartAsset, StringOperator.String_Equals_enum operation) {
        StringOperator equal = new StringOperator();
        equal.setAssetOperator(smartAsset,operation.toString());
        return equal;
    }


    public IntOperator getIntOperator(SmartAutomation automation, IntOperator.Int_Equals_enum operation) {
        IntOperator equal = new IntOperator();
        equal.setOperator(automation,operation.toString());
        return equal;
    }

    public IntOperator getIntAssetOperator(ISmartAssetCounter smartAsset, IntOperator.Int_Equals_enum operation) {
        IntOperator equal = new IntOperator();
        equal.setAssetOperator(smartAsset,operation.toString());
        return equal;
    }


    public DateOperator getDateOperator(SmartAutomation automation, DateOperator.Date_Equals_enum operation) {
        DateOperator equal = new DateOperator();
        equal.setOperator(automation,operation.toString());
        return equal;
    }

    public DateOperator getDateAssetOperator(ISmartAssetCounter smartAsset, DateOperator.Date_Equals_enum operation) {
        DateOperator equal = new DateOperator();
        equal.setAssetOperator(smartAsset,operation.toString());
        return equal;
    }

    public TagOperator getTagOperator(ISmartAutomationCounter automation, TagOperator.Tags_Equals_enum operation) {
        TagOperator equal = new TagOperator();
        equal.setOperator(automation,operation.toString());
        return equal;
    }

    public TagOperator getTagAssetOperator(ISmartAssetCounter smartAsset, TagOperator.Tags_Equals_enum operation) {
        TagOperator equal = new TagOperator();
        equal.setAssetOperator(smartAsset,operation.toString());
        return equal;
    }

    public Condition createConditionTagValue(ISmartAutomationCounter automation, ApplyIf applyIf, IApplyField field, IOperator operator, String value) throws Exception{
        ConditionTagValue conditionTagValue = new ConditionTagValue(automation);
        conditionTagValue.setValue(value);
        ConditionFieldOprValue cond = generateCondition(applyIf, field, operator.getOperator(), conditionTagValue);
        cond.setConditionTagValue(conditionTagValue);
        return cond;
    }
    public Condition createConditionDateValue(ICounter automation, ApplyIf applyIf, IApplyField field, IOperator operator, Calendar date) throws Exception{
        ConditionDateValue conditionDateValue = new ConditionDateValue(automation);
        conditionDateValue.setValue(getDate(date));
        ConditionFieldOprValue cond = generateCondition(applyIf, field, operator.getOperator(), conditionDateValue);
        cond.setConditionDateValue(conditionDateValue);
        return cond;
    }

    public Condition createConditionAssetDateValue(ISmartAssetCounter smartAsset, ApplyIf applyIf, IApplyField field, IOperator operator, Calendar date) throws Exception{
        ConditionDateValue conditionDateValue = new ConditionDateValue(smartAsset);
        conditionDateValue.setValue(getDate(date));
        ConditionFieldOprValue cond = generateCondition(applyIf, field, operator.getOperator(), conditionDateValue);
        cond.setConditionDateValue(conditionDateValue);
        return cond;
    }

    //this function fits both usages of smartAction:artAutomation and smartAsset
    public  Condition createCondition(ICounter automation, ApplyIf applyIf, IApplyField field, IOperator operator, String value) throws Exception{
        ConditionValue conditionValue = new ConditionValue(automation);
        conditionValue.setValue(value);
        ConditionFieldOprValue cond = generateCondition(applyIf, field, operator.getOperator(),conditionValue);
        cond.setConditionValue(conditionValue);
        return cond;
    }

    public Condition createConditionTextArea(ICounter automation, ApplyIf applyIf, IApplyField field, IOperator operator, String value) throws Exception{
        ConditionTextAreaValue conditionValue = new ConditionTextAreaValue(automation);
        conditionValue.setValue(value);
        ConditionFieldOprValue cond = generateCondition(applyIf, field, operator.getOperator(),conditionValue);
        cond.setConditionTextAreaValue(conditionValue);
        return cond;
    }


    public Condition createConditionSelectValue(SmartAutomation automation, ApplyIf applyIf, IApplyField field, IOperator operator, String value) throws Exception{
        ConditionSelectValue conditionSelectValue = new ConditionSelectValue(automation);
        conditionSelectValue.setValue(value);
        ConditionFieldOprValue cond = generateCondition(applyIf, field, operator.getOperator(), conditionSelectValue);
        cond.setConditionSelectValue(conditionSelectValue);
        return cond;
    }

    public Condition createConditionWithGroup(ICounter automation, ApplyIf applyIf, IApplyField field, IOperator operator, String value, Group group) throws Exception{
        ConditionValue conditionValue = new ConditionValue(automation);
        conditionValue.setValue(value);
        ConditionFieldOprValue cond = generateCondition(applyIf, field, operator.getOperator(),conditionValue);
        cond.setConditionValue(conditionValue);
        cond.setGroup(group);
        return cond;
    }

    public ConditionFieldOprValue createConditionNoGroup(ICounter automation, ApplyIf applyIf, IApplyField field, IOperator operator, String value) throws Exception{
        ConditionValue conditionValue = new ConditionValue(automation);
        conditionValue.setValue(value);
        ConditionFieldOprValue cond = generateCondition(applyIf, field, operator.getOperator(),conditionValue);
        cond.setConditionValue(conditionValue);
        return cond;
    }
    private ConditionFieldOprValue generateCondition(ApplyIf applyIf, IApplyField field, String operator, IApplyValue inputValue) {
        log4j.info("creating condition: " + applyIf.toString() + " " + field + " " + operator.toString() + " " + inputValue.getValue());
        ConditionFieldOprValue cond = new ConditionFieldOprValue();
        cond.setApplyIf(applyIf);
        cond.setField(field.toString());
        cond.setOperator(operator);
        return cond;
    }

    public IGroupCondition createGroupCondition (ICounter automation, GroupField field, String operator, String value, Group group) {

        IGroupCondition groupCondition = new GroupCondition();
        setGroupConditionData(automation, field, operator, value, group, groupCondition);
        return groupCondition;

    }

    public IGroupCondition createGroupConditionItemTag(ICounter automation, GroupField field, String operator, String value, Group group){
        IGroupCondition groupCondition = new GroupConditionTags();
        setGroupConditionData(automation, field, operator, value, group, groupCondition);
        return groupCondition;
    }

    private void setGroupConditionData(ICounter automation, GroupField field, String operator, String value, Group group, IGroupCondition groupCondition) {
        groupCondition.setField(field.toString());
        groupCondition.setEquals(operator);
        groupCondition.setValue(value);
        group.addCondition(automation, groupCondition);
    }


    public Group createGroup (ICounter automation, String groupName){
        log4j.info("creating group: " + groupName);
        Group group = new Group(automation);
        group.setGroupName(groupName);
        return group;
    }


    public Group createSmartAssetGroupWithConds(String groupName, ISmartAssetCounter smartAsset, SetDiscountOnItems d) {
        //Create new group
        Group group = createGroup(smartAsset, groupName);
        d.setGroup(group, smartAsset);

        return group;

    }


    public  SmartGift createSmartGift(CharSequence timestamp) throws Exception{
        log4j.info("creating smart gift");
        SmartGift smartGift = buildSmartGift(timestamp);
        AssetService.createSmartGift(smartGift);
        log4j.info("gift was created successfully. smart gift title is:  " + smartGift.getTitle());
        return smartGift;
    }

    public SmartGift createSmartGiftWithInvalidDates(CharSequence timestamp) throws Exception{
        log4j.info("creating smart gift");
        SmartGift smartGift = buildSmartGift(timestamp);
        Calendar calPastOneMonth = Calendar.getInstance();
        calPastOneMonth.add(Calendar.MONTH, -1);
        Calendar calPastTwoMonth  = Calendar.getInstance();
        calPastTwoMonth.add(Calendar.MONTH, -2);
        smartGift.setValidFrom(getDate(calPastOneMonth));
        smartGift.setValidUntil(getDate(calPastTwoMonth));
        AssetService.createSmartGift(smartGift);
        log4j.info("Smart gift was created successfully. smart gift title is:  " + smartGift.getTitle());


        return smartGift;
    }



    public  SmartGift createSmartGift(CharSequence timestamp,AssetStatus assetStatus) throws Exception{
        log4j.info("creating smart gift with status: " + assetStatus);
        SmartGift smartGift = buildSmartGift(timestamp);
        switch ( assetStatus) {
            case ACTIVE:
                log4j.info("creating smart gift with stock, valid and status: " + assetStatus);
                AssetService.createSmartGift(smartGift);
                break;
            case EXPIRED:
                Calendar date = Calendar.getInstance();
                date.add(Calendar.YEAR, -1);
                smartGift.setValidUntil(getDate(date));
                AssetService.createSmartGift(smartGift);
                break;
            case DEACTIVATED:
                smartGift.setDeactivateAfterNum("1");
                smartGift.setDeactivateAfterUnits(TimeUnits.MINUTES);
                AssetService.createSmartGift(smartGift);
                break;
        }

        log4j.info("gift was created successfully. gift's title is:  " + smartGift.getTitle());
        return smartGift;
    }



    public  SmartGift createSmartGiftWithAutoGenerateCodes(CharSequence timestamp) throws Exception{
        log4j.info("creating smart gift");
        SmartGift smartGift = buildSmartGift(timestamp);
        RedeemCodeAutoGenerated redeemCodeAutoGenerated = new RedeemCodeAutoGenerated();
        redeemCodeAutoGenerated.setRedeemCodeExpirasAfterUnits(TimeUnits.DAYS);
        redeemCodeAutoGenerated.setRedeemCodeExpiresAfter("10");
        smartGift.setRedeemcodeAutoGenerated(redeemCodeAutoGenerated);
        smartGift.setRedeemCodeType(RedeemCodeType.AUTOGENERATED_CODES);

        Scenario scenario = new Scenario();
        SmartGiftAction action = new SmartGiftAction();
        action.setPerformTheAction(SmartGiftActions.AddDiscount.toString());
        AddDiscountAction addDiscountAction = new AddDiscountAction(smartGift);
        addDiscountAction.setDescritpion("add discount");
        PercentOff percentOff = new PercentOff(smartGift);
        AmountOffNumber amountOffNumber1 = new AmountOffNumber(smartGift);
        amountOffNumber1.setAmountOffNumber("10");
        smartGift.addScenario(scenario);
        scenario.addAction(action,smartGift);
        action.setDiscount(addDiscountAction);
        addDiscountAction.setPercentOff(percentOff);
        percentOff.setAmountOffNumber(amountOffNumber1);
        AssetService.createSmartGift(smartGift);
        log4j.info("gift was created successfully. gift's title is:  " + smartGift.getTitle());
        return smartGift;
    }


    public SmartGift buildSmartGift(CharSequence timestamp) {
        return buildSmartGift("testGift" + timestamp, "sample short name " + timestamp, "Sample description");
    }

    public SmartGift buildSmartGift(String namePrefix) {
        return buildSmartGift(namePrefix, "sample short name " + namePrefix, "Sample description");
    }

    public SmartGift buildSmartGift(String title, String shortName, String description) {
        SmartGift smartGift = new SmartGift();
        smartGift.setTitle(title);
        smartGift.setShortName(shortName);
        smartGift.setDescription(description);
        setAutogeneratedCodes(smartGift);
        return smartGift;
    }


    public SmartClubDeal buildSmartClubDeal(CharSequence timestamp) {
        SmartClubDeal smartClubDeal = new SmartClubDeal();
        smartClubDeal.setDescription("Sample description");
        smartClubDeal.setTitle("testGift" + timestamp);
        return smartClubDeal;
    }

    public SmartClubDeal buildSmartClubDeal(String namePrefix) {
        return buildSmartClubDeal(namePrefix, "Sample description");
    }

    public SmartClubDeal buildSmartClubDeal(String name, String description) {
        SmartClubDeal smartClubDeal = new SmartClubDeal();
        smartClubDeal.setDescription(description);
        smartClubDeal.setTitle(name);
        return smartClubDeal;
    }

    public ClubDeal buildOldClubDeal(CharSequence timestamp) {
        ClubDeal clubDeal = new ClubDeal();
        clubDeal.setDescription("Sample description");
        clubDeal.setTitle("testClubdeal" + timestamp);
        return clubDeal;
    }

    public ClubDeal buildOldClubDeal(String namePrefix) {
        ClubDeal clubDeal = new ClubDeal();
        clubDeal.setDescription("Sample description");
        clubDeal.setTitle(namePrefix);
        return clubDeal;
    }

    public  SmartClubDeal createClubDeal(CharSequence timestamp) throws Exception{
        log4j.info("creating smart club deal");
        SmartClubDeal smartGift = buildSmartClubDeal(timestamp);
        AssetService.createSmartClubDeal(smartGift);
        log4j.info("smart club deal was created successfully. smart club deal's title is:  " + smartGift.getTitle());
        return smartGift;
    }

    public  SmartClubDeal createSmartClubDeal(String namePrefix,AssetStatus assetStatus) throws Exception{

        log4j.info("creating smart club deal");
        SmartClubDeal smartClubDeal = buildSmartClubDeal(namePrefix);

        switch ( assetStatus) {
            case ACTIVE:
                log4j.info("creating smart club deal with stock, valid and status: " + assetStatus);
                AssetService.createSmartClubDeal(smartClubDeal);
                break;
            case EXPIRED:
                Calendar date = Calendar.getInstance();
                date.add(Calendar.YEAR, -1);
                smartClubDeal.setValidUntil(getDate(date));
                AssetService.createSmartClubDeal(smartClubDeal);
                break;
            case DEACTIVATED:
                AssetService.createSmartClubDeal(smartClubDeal);
                MembersService.deactivateSmartClubDeal(smartClubDeal.getTitle());
                break;

        }

        log4j.info("smart club deal was created successfully. smart club deal's title is:  " + smartClubDeal.getTitle());
        return smartClubDeal;
    }


    public ClubDeal createOldClubDeal(String namePrefix,AssetStatus assetStatus) throws Exception{

        log4j.info("creating smart club deal");
        ClubDeal clubDeal = buildOldClubDeal(namePrefix);

        switch ( assetStatus) {
            case ACTIVE:
                log4j.info("creating smart club deal with stock, valid and status: " + assetStatus);
                AssetService.createOldClubDeal(clubDeal);
                break;
            case EXPIRED:
                Calendar date = Calendar.getInstance();
                date.add(Calendar.YEAR, -1);
                clubDeal.setValidUntil(getDate(date));
                AssetService.createOldClubDeal(clubDeal);
                break;
            case DEACTIVATED:
                AssetService.createOldClubDeal(clubDeal);
                MembersService.deactivateOldClubDeal(clubDeal.getTitle());
                break;

        }

        log4j.info("smart club deal was created successfully. smart club deal's title is:  " + clubDeal.getTitle());
        return clubDeal;
    }

    public  LotteryReward createLotteryReward(CharSequence timestamp,String giftTitle,String maxRewards,String limit) throws Exception{
        log4j.info("creating lottery reward");
        LotteryReward lotteryReward = buildLotteryReward(giftTitle, limit);
        lotteryReward.setTitle("LotteryReward" + timestamp);
        lotteryReward.setMaxRewards(maxRewards);
        AssetService.createLotteryReward(lotteryReward);
        log4j.info("Lottery rewards was created successfully. Title is:  " + lotteryReward.getTitle());
        return lotteryReward;
    }

    public  LotteryReward createLotteryRewardWithProbability(String probability,CharSequence timestamp,String giftTitle,String maxRewards,String limit,List<String> tags) throws Exception{
        log4j.info("creating lottery reward");
        LotteryReward lotteryReward = buildLotteryRewardWithProbability(probability,timestamp, giftTitle, limit);
        lotteryReward.setMaxRewards(maxRewards);
        AssetService.createLotteryReward(lotteryReward);
        log4j.info("Lottery rewards was created successfully. Title is:  " + lotteryReward.getTitle());
        return lotteryReward;
    }

    public  LotteryReward buildLotteryRewardOptionWithProbability(LotteryReward lotteryReward,String probability,String giftTitle,String limit,RewardTypes rewardTypes) throws Exception{
        log4j.info("creating lottery reward option : probability-" + probability + " rewardTypes-" + rewardTypes.toString());
        return buildLotteryRewardOption(lotteryReward,probability,giftTitle,limit,rewardTypes);
    }

    public  LotteryReward createLotteryRewardWithPush(String giftTitle,String maxRewards,String limit,RewardTypes rewardTypes,List<String> tags,YesNoList notify) throws Exception{
        log4j.info("creating lottery reward probability 100% with push");
        LotteryReward lotteryReward = new LotteryReward();
        buildLotteryRewardOption(lotteryReward,"100",giftTitle,limit,rewardTypes,notify);
        lotteryReward.setTitle("Lottery_prob100_" +System.currentTimeMillis());
        lotteryReward.setMaxRewards(maxRewards);
        lotteryReward.setTags(tags);
        AssetService.createLotteryReward(lotteryReward);
        log4j.info("Lottery rewards was created successfully. Title is:  " + lotteryReward.getTitle());
        return lotteryReward;
    }

    public  LotteryReward createLotteryRewardProbability100(CharSequence timestamp,String giftTitle,String maxRewards,String limit,List<String> tags) throws Exception{
        log4j.info("creating lottery reward probability 100%");
        LotteryReward lotteryReward = buildLotteryRewardWithProbability("100",timestamp, giftTitle, limit);
        lotteryReward.setMaxRewards(maxRewards);
        lotteryReward.setTags(tags);
        AssetService.createLotteryReward(lotteryReward);
        log4j.info("Lottery rewards was created successfully. Title is:  " + lotteryReward.getTitle());
        return lotteryReward;
    }

    private LotteryReward buildLotteryRewardWithProbability(String probability,CharSequence timestamp, String giftTitle, String limit) {
        LotteryReward lotteryReward = buildLotteryReward(probability,giftTitle,limit,RewardTypes.GIVE_ASSET);
        lotteryReward.setTitle("LotteryReward" + timestamp);
        return lotteryReward;
    }

    private LotteryReward buildLotteryReward( String giftTitle, String limit) {
        LotteryReward lotteryReward = buildLotteryReward("100",giftTitle,limit,RewardTypes.GIVE_ASSET);
        return lotteryReward;
    }

    public LotteryReward buildLotteryReward(String probability, String giftTitle, String limit,RewardTypes rewardTypes) {
        LotteryReward lotteryReward = new LotteryReward();
        lotteryReward.setTitle("Lottery_probability" + probability + rewardTypes.toString());
        buildLotteryRewardOption(lotteryReward,probability,giftTitle,limit,rewardTypes);
        return lotteryReward;
    }
    public LotteryReward buildLotteryReward(CharSequence timestamp, String memberLimit,List<String> tags) {
        LotteryReward lotteryReward = new LotteryReward();
        lotteryReward.setTitle("Lottery_" + timestamp);
        lotteryReward.setMaxRewards(memberLimit);
        lotteryReward.setTags(tags);
        return lotteryReward;
    }

    public LotteryReward buildLotteryRewardOption(LotteryReward lotteryReward,String probability, String giftTitle, String limit,RewardTypes rewardTypes,YesNoList notify) {
        RewardOptions rewardOptions = new RewardOptions();

        rewardOptions.setProbability(probability + "%");
        rewardOptions.setRewardType(rewardTypes.toString());
        if(rewardTypes.equals(RewardTypes.GIVE_ASSET)) {
            rewardOptions.setLimit(limit);
            rewardOptions.setReward(giftTitle);
            rewardOptions.setNotify(notify.toString());
        }else if(rewardTypes.equals(RewardTypes.SEND_PUSH)) {
            rewardOptions.setPushMessage("Only push from lottery reward!");
        }
        lotteryReward.addRewardOptions(rewardOptions);
        return lotteryReward;
    }

    public LotteryReward buildLotteryRewardOption(LotteryReward lotteryReward,String probability, String giftTitle, String limit,RewardTypes rewardTypes) {
        return buildLotteryRewardOption(lotteryReward,probability,giftTitle,limit,rewardTypes,YesNoList.NO);
    }

    public SmartGift createSmartGiftWithRedeemCode(CharSequence timestamp) throws Exception{
        log4j.info("creating smart gift with redeem code");
        SmartGift smartGift = buildSmartGift(timestamp);
        //Create with redeem code auto generated
        smartGift.setRedeemCodeType(RedeemCodeType.AUTOGENERATED_CODES);
        smartGift.setRedeemcodeAutoGenerated(new RedeemCodeAutoGenerated("12",TimeUnits.HOURS));
        AssetService.createSmartGift(smartGift);
        log4j.info("gift was created successfully. gift's title is:  " + smartGift.getTitle());
        return smartGift;
    }


    public SmartPunchCard createSmartPunchCard(CharSequence timestamp,String numOfPunches) throws Exception{
        log4j.info("creating punch card");
        SmartPunchCard punchCard = buildBasicSmartPunchCard(timestamp, numOfPunches);
        AssetService.createSmartPunchCard(punchCard);
        log4j.info("Punch card was created successfully. Punch card title is:  " + punchCard.getTitle());
        return punchCard;
    }

    public SmartPunchCard createSmartPunchCardWithInvalidDates(CharSequence timestamp,String numOfPunches) throws Exception{
        log4j.info("creating invalid punch card");
        SmartPunchCard punchCard = buildBasicSmartPunchCard(timestamp, numOfPunches);
        Calendar calPastOneMonth = Calendar.getInstance();
        calPastOneMonth.add(Calendar.MONTH, -1);
        Calendar calPastTwoMonth  = Calendar.getInstance();
        calPastTwoMonth.add(Calendar.MONTH, -2);
        punchCard.setValidFrom(getDate(calPastOneMonth));
        punchCard.setValidUntil(getDate(calPastTwoMonth));
        AssetService.createSmartPunchCard(punchCard);
        log4j.info("Punch card was created successfully. Punch card title is:  " + punchCard.getTitle());
        return punchCard;
    }

    public SmartPunchCard buildBasicSmartPunchCard(CharSequence timestamp, String numOfPunches) {
        return buildBasicSmartPunchCard("testPunch" + timestamp, "sample short name " + timestamp, "Sample description", numOfPunches);
    }

    public SmartPunchCard buildBasicSmartPunchCard(String name, String shortName, String description, String numOfPunches) {
        SmartPunchCard punchCard = new SmartPunchCard();
        punchCard.setDescription(description);
        punchCard.setShortName(shortName);
        punchCard.setTitle(name);
        punchCard.setNumOfPunches(numOfPunches);
        punchCard.setAllowMultiPunchesYesNo(YesNoList.YES.toString());
        setAutogeneratedCodes(punchCard);
        return punchCard;
    }


    public GiftCard createGiftCard (String timestamp,String value,String cost) throws Exception{

        log4j.info("creating gift card");
        GiftCard giftCard = new GiftCard();
        giftCard.setTitle("giftCard" + timestamp);
        giftCard.setDescription("Sample description");
        giftCard.setValue(value);
        giftCard.setCost(cost);
        giftCard.setCanReloaded(YesNoList.YES.toString());
        AssetService.createGiftCard(giftCard);
        log4j.info("gift card was created successfully. gift card title is:  " + giftCard.getTitle());
        return giftCard;
    }

    public GiftCard createGiftCard (String title,String description, Calendar experationDate , boolean canReloaded) throws Exception{

        log4j.info("creating gift card");

        GiftCard giftCard = new GiftCard();
        giftCard.setTitle(title);
        giftCard.setDescription(description);
        if (canReloaded)
            giftCard.setCanReloaded(YesNoList.YES.toString());
        giftCard.setValidUntil(getDate(experationDate));
        AssetService.createGiftCard(giftCard);
        log4j.info("gift card was created succesfully. gift card title is:  " + giftCard.getTitle());
        return giftCard;
    }

    public GiftCardBulkCode createGiftCardBulkCodes (String bulkName, String bulkTag, String description, String customCodes) throws Exception {

        log4j.info("creating gift card codes");
        GiftCardBulkCode giftCardBulkCode = new GiftCardBulkCode();
        giftCardBulkCode.setDescription(description);
        giftCardBulkCode.setBulk_name(bulkName);
        giftCardBulkCode.setBulk_tag(bulkTag);
        giftCardBulkCode.setCodes(customCodes);
        giftCardBulkCode.clickGenerateCode();
        AssetService.generateGiftCardCodes(giftCardBulkCode);
        log4j.info("Generate codes " + giftCardBulkCode.getCodes() + " succesfully. gift card codes bulk name is:  " + giftCardBulkCode.getBulk_name());
        return giftCardBulkCode;
    }


    //this function fits both usages of smartAction:artAutomation and smartAsset
    public Condition createConditionDateAndTime(ICounter smartAction, ApplyIf applyIf, Calendar fromDateTime, Calendar untilDateTime) throws Exception{
        ConditionDateAndTime cond = new ConditionDateAndTime(smartAction);
        cond.setApplyIf(applyIf);
        DateAndTime dateAndTime = new DateAndTime(smartAction);
        dateAndTime.setFromDate(getDate(fromDateTime));
        dateAndTime.setFromTime(getTime(fromDateTime));
        dateAndTime.setUntilDate(getDate(untilDateTime));
        dateAndTime.setUntilTime(getTime(untilDateTime));
        dateAndTime.setUntilAM();
        cond.setDateAndTime(dateAndTime);
        log4j.info("creating condition: " + applyIf.toString() + " Date&Time: "  + dateAndTime.getFromDate() + "-> " + dateAndTime.getFromTime() +" "+ dateAndTime.getFromAM());
        log4j.info( " " + dateAndTime.getUntilDate() + "-> " + dateAndTime.getUntilTime() + " " + dateAndTime.getUntilAM());
        return cond;
    }


    public Condition createConditionDaysTime(SmartAutomation automation, ApplyIf applyIf,Calendar day, Calendar fromDateTime, Calendar untilDateTime) throws Exception{

        ConditionDaysTime cond = new ConditionDaysTime(automation);
        cond.setApplyIf(applyIf);
        DaysTime daysTime = generateSingleDay(day, fromDateTime, untilDateTime);
        cond.addDaysTime(daysTime,automation);
        printDayTime(daysTime,applyIf);

        return cond;
    }



    //this function fits both usages of smartAction:artAutomation and smartAsset
    public  Condition createConditionDaysTime(ICounter smartAction, List<DaysTime> days) throws Exception{

        ConditionDaysTime cond = new ConditionDaysTime(smartAction);
        cond.setApplyIf(ApplyIf.DAYSTIME);
        for (DaysTime day : days) {
            printDayTime(day,ApplyIf.DAYSTIME);
            cond.addDaysTime(day,smartAction);
        }
        return cond;
    }


    private static void printDayTime(DaysTime day,ApplyIf applyIf) {
        log4j.info("creating condition: " + applyIf.toString() + " DaysTime:\n " +
                "From: "  + day.getFromTime() + " " + ((day.getFromTimeAMPM()==null)? "AM":"PM")
                + " To:" + day.getToTime() + " " + (( day.getToTimeAM()==null)? "PM":"AM"));
    }


    public DaysTime generateSingleDay(Calendar day, Calendar fromDateTime, Calendar untilDateTime) {
        DaysTime daysTime = new DaysTime();
        daysTime.setIs(getDayOfWeek(day));
        daysTime.setFromTime(getTime(fromDateTime));
        daysTime.setToTime(getTime(untilDateTime));
        return daysTime;
    }

    /**
     * Generate DaysTime object with default hours (12:00 AM ,11:59:59 PM)
     */
    public DaysTime generateSingleDay(Calendar day) {
        DaysTime daysTime = new DaysTime();
        daysTime.setIs(getDayOfWeek(day));
        return daysTime;
    }

    public String getDayOfWeek(Calendar day) {
        day.setTimeZone(TimeZone.getTimeZone(System.getProperty("user.timezone")));
        day.setFirstDayOfWeek(Calendar.SUNDAY);
        String[] strDays = new String[] {Days.SUNDAY.toString(), Days.MONDAY.toString(), Days.TUESDAY.toString(),
                Days.WEDNESDAY.toString(), Days.THURSDAY.toString(),
                Days.FRIDAY.toString(), Days.SATURDAY.toString() };
        System.out.println("day of week by calander: " + day.get(Calendar.DAY_OF_WEEK));
        return strDays[day.get(Calendar.DAY_OF_WEEK) - 1];
    }

    public SmartAutomationAction getUnregisterAction(SmartAutomationAction smartAction, ISmartAutomationCounter automation) throws Exception{

        smartAction.setPerformTheAction(SmartAutomationActions.UnregisterMembership.toString());
        Unregister unregister = new Unregister(automation);
        smartAction.setUnregister(unregister);
        return smartAction;
    }

    public SmartAutomationAction getRegisterAction(SmartAutomationAction smartAction, ISmartAutomationCounter automation) throws Exception{

        smartAction.setPerformTheAction(SmartAutomationActions.RegisterMembership.toString());
        Register register = new Register(automation);
        smartAction.setRegister(register);
        return smartAction;
    }

    public SmartAutomationAction getExportEventAction(SmartAutomationAction smartAction, ISmartAutomationCounter automation,String exportName,String url) throws Exception{

        smartAction.setPerformTheAction(SmartAutomationActions.ExportEvent.toString());
        ExportEvent exportEvent = new ExportEvent(automation);
        exportEvent.setName(exportName);
        exportEvent.setUrl(url);
        smartAction.setExportEvent(exportEvent);
        return smartAction;
    }

    public SmartAutomationAction getSendPushNotificationAction(SmartAutomationAction smartAction, ISmartAutomationCounter automation, String textMessage, String refreshAssetList) {

        smartAction.setPerformTheAction(SmartAutomationActions.SendAPushNotification.toString());
        SendPushNotification sendPushNotification = new SendPushNotification(automation);
        sendPushNotification.setTxtMessage(textMessage);
        sendPushNotification.setRefreshAssetChk(refreshAssetList);
        smartAction.setPushNotification(sendPushNotification);
        return smartAction;
    }

    public SmartAutomationAction getSendSMSAction(SmartAutomationAction smartAction, ISmartAutomationCounter automation, String textMessage) {

        smartAction.setPerformTheAction(SmartAutomationActions.SendTextSMS.toString());
        SendSMS sendSMS = new SendSMS(automation);
        sendSMS.setTxtMessage(textMessage);
        smartAction.setSendSMS(sendSMS);
        return smartAction;
    }

    public SmartAutomationAction getSendEmailAction(SmartAutomationAction smartAction, ISmartAutomationCounter automation, String templateName) {
        smartAction.setPerformTheAction(SmartAutomationActions.SendEmail.toString());
        SendEmail sendEmail = new SendEmail(automation);
        sendEmail.setTemplateName(templateName);
        smartAction.setSendEmail(sendEmail);
        return smartAction;
    }

    public SmartAutomationAction getSendPopUpMessageAction(SmartAutomationAction smartAction, ISmartAutomationCounter automation, String title, String textMessage) {

        smartAction.setPerformTheAction(SmartAutomationActions.SendPopUPMessage.toString());
        SendPopUpMessage sendPopUpMessage = new SendPopUpMessage(automation);
        sendPopUpMessage.setTitle(title);
        sendPopUpMessage.setTxtMessage(textMessage);
        smartAction.setSendPopUpMessage(sendPopUpMessage);
        return smartAction;
    }

    public SmartGift createSmartGiftWithPushMessage(CharSequence timestamp,String pushMessage) throws Exception{
        log4j.info("creating smart gift with push message");
        SmartGift smartGift = buildSmartGift(timestamp + "pushMessage");
        //Create with push message
        smartGift.setPushNotificationMessage(pushMessage);
        AssetService.createSmartGift(smartGift);
        log4j.info("gift was created successfully. gift's title is:  " + smartGift.getTitle());
        return smartGift;
    }

    public void performAnActionSendGift(SmartGift smartGift) throws Exception {

        MembersService.performSmartActionSendAnAsset(smartGift,String.valueOf(System.currentTimeMillis()));
        Assert.assertTrue(MembersService.isGiftExists(smartGift.getTitle()));

    }



    public void performAnActionSendSmartPunch(SmartPunchCard punchCard) throws Exception {

        performAnActionSendPunch(punchCard.getTitle());

    }

    public void performAnActionSendPunch(String punchCardTitle) throws Exception {

        MembersService.performSmartActionSendAnAsset(punchCardTitle,String.valueOf(System.currentTimeMillis()));
        Assert.assertTrue(MembersService.isGiftExists(punchCardTitle));

    }
    public void deactivateSmartgift (String name) throws Exception {

        MembersService.deactivateGift(name);
        log4j.info("The gift is deactivated: " + name);

    }


    public void deactivateSmartPunchCard (String name) throws Exception {

        MembersService.deactivateSmartPunchCard(name);
        log4j.info("The punch is deactivated: " + name);

    }

    public SmartGift createSmartGiftByRedeemCodeType(CharSequence timestamp, RedeemCodeType redeemCodeType) {

        ArrayList<String> itemCodes = Lists.newArrayList("222334");

        SmartGift smartGift = new SmartGift();
        smartGift.setDescription("Sample description" + timestamp);
        smartGift.setShortName("sample short name " + timestamp);
        smartGift.setTitle("testTitle" + timestamp);

        switch(redeemCodeType){

            case AUTOGENERATED_CODES:
                setAutogeneratedCodes(smartGift);
                break;

            case PREDEFINED_CODE_BULK:
                setBulkCodes(smartGift, AssignAsset.DEFAULT_DEAL_CODE);
                break;

            case THIRD_PARTY_CODES:
                set3rdpartyCodes(smartGift);
                break;

        }

        //Create smartGift action
        Scenario scenario = createSmartGiftScenario(smartGift);
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddItemCode.toString());
        AddItemCode itemCode = new AddItemCode();
        itemCode.setItemCodes(itemCodes);
        smartGiftAction.setItem(itemCode);
        scenario.addAction(smartGiftAction, smartGift);
        return smartGift;

    }

    public SmartGiftAction createSmartGiftAction(String action) {

        SmartGiftAction smartGiftAction = new SmartGiftAction();
        smartGiftAction.setPerformTheAction(action);
        return smartGiftAction;

    }

    public Scenario createSmartGiftScenario(SmartClubDeal smartClubDeal) {

        Scenario scenario = new Scenario();
        smartClubDeal.addScenario(scenario);
        return scenario;

    }

    public void setAutogeneratedCodes(SmartGift smartGift){

        smartGift.setRedeemCodeType(RedeemCodeType.AUTOGENERATED_CODES);
        smartGift.setRedeemcodeAutoGenerated(new RedeemCodeAutoGenerated("12", TimeUnits.HOURS));

    }

    public void setAutogeneratedCodes(SmartPunchCard smartPunchCard){

        smartPunchCard.setRedeemCodeType(RedeemCodeType.AUTOGENERATED_CODES);
        smartPunchCard.setRedeemcodeAutoGenerated(new RedeemCodeAutoGenerated("12", TimeUnits.HOURS));

    }

    public void setBulkCodes(SmartGift smartGift,String SKUCode){

        smartGift.setRedeemCodeType(RedeemCodeType.PREDEFINED_CODE_BULK);
        RedeemCodeBulk codeBulk = new RedeemCodeBulk();
        codeBulk.setRewardSKU(SKUCode);
        smartGift.setCodeBulk(codeBulk);

    }

    public void set3rdpartyCodes(SmartGift smartGift){

        smartGift.setRedeemCodeType(RedeemCodeType.THIRD_PARTY_CODES);

    }

    public ThirdPartyCodes generate3PartyCodes(String smartGiftTitle) throws Exception{

        ThirdPartyCodes thirdPartyCodes = new ThirdPartyCodes();
        thirdPartyCodes.setCodeName(smartGiftTitle + "_thirdPatyCodes");
        thirdPartyCodes.setChooseAsset(smartGiftTitle);
        thirdPartyCodes.setBulk_name(smartGiftTitle + "_Bulk");
        thirdPartyCodes.setBulk_tag(smartGiftTitle + "_bulkTag");
        thirdPartyCodes.setCodes("10001,10021,10003,10004,10010,23344");

        return thirdPartyCodes;
    }

    public static JoiningCode generateJoiningCodes(String codeName,String bulkName,String bulkTag, String codes) throws Exception{

        JoiningCode joiningCode = new JoiningCode();
        joiningCode.setCodeName(codeName);
        joiningCode.setBulkName(bulkName);
        joiningCode.setBulkTag(bulkTag);
        joiningCode.setCodes(codes);
        return joiningCode;
    }


    private static String getCodes(String code) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i <5 ; i++) {
            String uuid = UUID.randomUUID().toString();
            buffer.append(uuid.substring(0,uuid.indexOf("-"))).append(",");
        }
        return buffer.append(code).toString();
    }


    public PointShopItem buildPointShopItem(PointShopPage pointShop, String giftTitle) {

        PointShopItem item = new PointShopItem(pointShop);
        item.setTitle(giftTitle);
        item.setDescription(giftTitle);
        item.setPrice("2");
        item.setChooseGift(giftTitle);
        pointShop.addItem(item);
        return item;
    }


    public String createSmartGiftPreset (String name) throws Exception {

        //Create smart automation preset
        SmartGift smartGift = buildSmartGift(name);
        smartGift.setSaveAsPreset("Yes");
        AssetService.createSmartGift(smartGift);
        String presetName = smartGift.getTitle();
        log4j.info("Created smart gift preset : " + presetName);
        return presetName;

    }

    public String createSmartClubDealPreset (String name) throws Exception {

        //Create smart club deal preset
        SmartClubDeal smartClubDeal = buildSmartClubDeal(name);
        smartClubDeal.setSaveAsPreset("Yes");
        AssetService.createSmartClubDeal(smartClubDeal);
        String presetName = smartClubDeal.getTitle();
        log4j.info("Created smart club deal preset : " + presetName);
        return presetName;

    }

    public Scenario createSmartAssetScenario(ISmartAssetCounter smartAsset,String name) {

        Scenario scenario = createSmartAssetScenario(smartAsset);
        scenario.setScenarioName(name);
        return scenario;

    }


    public Scenario createSmartAssetScenario(ISmartAssetCounter smartAsset) {

        Scenario scenario = new Scenario();
        smartAsset.addScenario(scenario);
        return scenario;

    }


    public void createCouponCodeBulkCodes(String codeName,String ruleName, String bulkName, String bulkTag, String codes) throws Exception {

        CouponCode couponCode = new CouponCode();
        couponCode.setCodeName(codeName);
        couponCode.setChooseAutomation(ruleName);
        couponCode.setBulkName(bulkName);
        couponCode.setBulkTag(bulkTag);
        couponCode.setCodes(codes);
        AssetService.createCouponCodeBulkCodes(couponCode);
    }
}
