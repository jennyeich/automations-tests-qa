package hub.utils;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.logging.LogEntries;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.logging.LogType;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;

import static hub.base.BaseHubTest.driver;

/**
 * Created by Jenny on 11/22/2016.
 */
public class ScreenShotTaker{

    public static void takeScreenShot(String fileName, Logger log4j){
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        // Now you can do whatever you need to do with it, for example copy somewhere
        try {
            File folder = new File(System.getProperty("user.dir") + File.separator + "Screenshots");
            if(!folder.exists())
                folder.mkdir();
            File targetFile = new File("Screenshots" + File.separator + fileName + ".png");
            FileUtils.copyFile(scrFile, targetFile);
            attachScreenshotToAllureReport(fileName, targetFile);

            //log console errors (js error mostly for debug purpose)
            if(log4j.isDebugEnabled()) {
                LogEntries logEntries = driver.manage().logs().get(LogType.BROWSER);
                for (LogEntry entry : logEntries) {
                    if (entry.getLevel().equals(Level.SEVERE))
                        System.out.println(new Date(entry.getTimestamp()) + " " + entry.getLevel() + " " + entry.getMessage());

                }
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    @Attachment("screenshot_{0}")
    public static byte[] attachScreenshotToAllureReport(String testName, File screenshot) {
        try {
            return FileUtils.readFileToByteArray(screenshot);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}


