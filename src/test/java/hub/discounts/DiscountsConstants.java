package hub.discounts;

/**
 * Created by Goni on 10/2/2017.
 */

public class DiscountsConstants {

    //The timeout in millisec that need to wait after creating member and before checking benefits
    public static final int WAIT_FOR_ASSETS_CACHE_UPDATE_MILLSEC = 100000;

    public static final String DEFAULT_DISCOUNT_STRATEGY = "DefaultDiscountStrategy";
    public static final String DETAILED_REDEEM_RESPONSE = "DetailedRedeemResponse";

    public static final String ITEM_CODE_111 = "111";
    public static final String ITEM_CODE_222 = "222";
    public static final String ITEM_CODE_333 = "333";
    public static final String DEP_CODE_111 = "111";
    public static final String DEP_CODE_222 = "222";
    public static final String DISCOUNT_ON = "";
    public static final String DISCOUNT_OFF = "off";
    public static final String DISCOUNTS_CHECK_BOX_ON = "yes";
    public static final String DISCOUNTS_CHECK_BOX_OFF = "";
    public static final String IS_LIMIT_CHECKED = "yes";
    public static final String IS_LIMIT_OFF = "";
    public static final String IS_LIMIT_OFFER_OFF = "";
    public static final String IS_LIMIT_OFFER_CHECKED = "yes";
}

