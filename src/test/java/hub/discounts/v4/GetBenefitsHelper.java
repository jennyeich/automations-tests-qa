package hub.discounts.v4;


import com.google.common.collect.Lists;
import hub.common.objects.smarts.ISmartAssetCounter;
import hub.common.objects.smarts.SmartClubDeal;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.SmartPunchCard;
import hub.common.objects.smarts.smarObjects.Scenario;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftActions;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.dealItemCode.AddDealCode;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.dealItemCode.AddItemCode;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.dealItemCode.RelatedDealCode;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.*;
import hub.discounts.BaseHub1Discount;
import hub.discounts.DiscountsConstants;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartGiftActions.ISmartAssetAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartClubDealAction;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.services.assets.AssetService;
import hub.utils.HubAssetsCreator;
import org.apache.log4j.Logger;
import server.common.IServerResponse;
import server.v4.GetBenefits;
import server.v4.GetBenefitsResponse;
import server.v4.common.models.Customer;
import server.v4.common.models.Purchase;
import server.v4.common.models.RedeemAsset;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by anastasiya on 02/07/2017.
 */
public class GetBenefitsHelper {


    private static Logger log4j = Logger.getLogger(GetBenefitsHelper.class);;
    private static GetBenefitsHelper instance;

    public static HubAssetsCreator hubAssetsCreator = new HubAssetsCreator();

    public static final String ITEM_CODE_111 = DiscountsConstants.ITEM_CODE_111;
    public static final String ITEM_CODE_222 = DiscountsConstants.ITEM_CODE_222;
    public static final String ITEM_CODE_333 = DiscountsConstants.ITEM_CODE_333;
    public static final String DISCOUNTS_CHECK_BOX_ON = DiscountsConstants.DISCOUNTS_CHECK_BOX_ON;
    public static final String DISCOUNTS_CHECK_BOX_OFF= DiscountsConstants.DISCOUNTS_CHECK_BOX_OFF;
    public static final String DISCOUNT_ON = DiscountsConstants.DISCOUNT_ON;
    public static final String DISCOUNT_OFF = DiscountsConstants.DISCOUNT_OFF;
    public static final String IS_LIMIT_CHECKED = DiscountsConstants.IS_LIMIT_CHECKED;
    public static final String IS_LIMIT_OFF = DiscountsConstants.IS_LIMIT_OFF;

    //SINGELTON CLASS
    public static GetBenefitsHelper getInstance() {
        if (instance == null)
            instance = new GetBenefitsHelper();
        return instance;
    }

    public static void setLogger(Logger logger) {
        log4j = logger;
    }

    /////////////////////////////////////////////
    ///////   GET  MEMBER  BENEFITS    //////////
    /////////////////////////////////////////////

    public GetBenefits getBenefitsRequest(String apiKey, String phoneNumber, Purchase purchase, ArrayList<RedeemAsset> assets, Map<String, String> queryParams) throws Exception {

        Thread.sleep(DiscountsConstants.WAIT_FOR_ASSETS_CACHE_UPDATE_MILLSEC);

        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        List<Customer> customersLst = new ArrayList<>();
        customersLst.add(customer);

        GetBenefits getBenefits = new GetBenefits(apiKey);
        getBenefits.setCustomers(customersLst);
        getBenefits.setPurchase(purchase);

        if(assets!=null && assets.size()!=0){
            getBenefits.setRedeemAssets(assets);
        }

        if(queryParams!=null && queryParams.size()!=0) {
            getBenefits.setQueryParameters(queryParams);
        }
        return getBenefits;
    }

    public GetBenefitsResponse getBenefits(String apiKey, String phoneNumber, Purchase purchase, ArrayList<RedeemAsset> assets, Map<String, String> queryParams) throws Exception {

        //Send the request
        GetBenefits getBenefits = getBenefitsRequest(apiKey, phoneNumber, purchase, assets, queryParams);
        IServerResponse response = getBenefits.SendRequestByApiKey(apiKey, GetBenefitsResponse.class);
        return (GetBenefitsResponse) response;
    }

    /////////////////////////////////////////////
    ////////// H E L P E R   F U N C S //////////
    /////////////////////////////////////////////

    public static AddDiscountAction createAddDiscountAction(ISmartAssetCounter smartAsset, String description, String discountType) {

        //create discount smartGiftAction
        AddDiscountAction discount = new AddDiscountAction(smartAsset);
        discount.setDescritpion(description);
        discount.setDiscountType(discountType);
        return discount;

    }

    public static SmartGiftAction createSmartGiftAction(String action) {
        // Create smart gift action without setting action name.
        return createSmartGiftAction(action, null);

    }

    public static SmartGiftAction createSmartGiftAction(String action, String actionName) {
        SmartGiftAction smartGiftAction = new SmartGiftAction();
        smartGiftAction.setPerformTheAction(action);
        smartGiftAction.setActionName(actionName);
        return smartGiftAction;
    }

    public static SmartClubDealAction createSmartClubDealAction(String action) {
        SmartClubDealAction clubDealAction = new SmartClubDealAction();
        clubDealAction.setPerformTheAction(action);
        return clubDealAction;
    }

    public static SmartClubDealAction createSmartClubDealAction(String action, String actionName) {
        SmartClubDealAction clubDealAction = new SmartClubDealAction();
        clubDealAction.setActionName(actionName);
        clubDealAction.setPerformTheAction(action);
        return clubDealAction;
    }


    public static void setDiscountType(ISmartAssetAction smartGiftAction, IDiscountType iDiscountType, AddDiscountAction discount) {
        if (iDiscountType instanceof PercentOff)
            discount.setPercentOff((PercentOff) iDiscountType);
        else if (iDiscountType instanceof AmountOff)
            discount.setAmountOff((AmountOff) iDiscountType);
        else if (iDiscountType instanceof FreeItem)
            discount.setFreeItem((FreeItem) iDiscountType);
        if (discount != null)
            smartGiftAction.setDiscount(discount);
    }

    public static SmartGift createSmartGiftWithAddDealCode(ArrayList dealCodes, String timestamp, boolean addCondition) throws Exception {

        SmartGift smartGift = BaseHub1Discount.createSmartGift(SmartGiftActions.AddDealCode, timestamp);
        setDealCode(dealCodes, addCondition, smartGift);
        AssetService.createSmartGift(smartGift);
        log4j.info("gift was created succesfully. gift's title is:  " + smartGift.getTitle());

        return smartGift;
    }



    public static SmartGift createSmartGiftWithAddItemCode(ArrayList itemCodes, String timestamp, boolean addCondition) throws Exception {

        SmartGift smartGift = BaseHub1Discount.createSmartGift(SmartGiftActions.AddItemCode, timestamp);

        setItemCode(itemCodes, addCondition, smartGift);
        AssetService.createSmartGift(smartGift);
        return smartGift;
    }

    public static SmartPunchCard createSmartPunchCardWithAddDealCode(ArrayList dealCodes, String timestamp, boolean addCondition,String numOfPunches) throws Exception {

        SmartPunchCard smartPunchCard = BaseHub1Discount.createSmartPunchCard(SmartGiftActions.AddDealCode, timestamp,numOfPunches);
        setDealCode(dealCodes, addCondition, smartPunchCard);
        AssetService.createSmartPunchCard(smartPunchCard);
        log4j.info("gift was created succesfully. gift's title is:  " + smartPunchCard.getTitle());

        return smartPunchCard;
    }



    public static SmartPunchCard createSmartPunchCardWithAddItemCode(ArrayList itemCodes, String timestamp, boolean addCondition,String numOfPunches) throws Exception {

        SmartPunchCard smartGift = BaseHub1Discount.createSmartPunchCard(SmartGiftActions.AddItemCode, timestamp,numOfPunches);

        setItemCode(itemCodes, addCondition, smartGift);
        AssetService.createSmartPunchCard(smartGift);
        return smartGift;
    }

    private static void setDealCode(ArrayList dealCodes, boolean addCondition, SmartClubDeal smartGift) throws Exception {
        AddDealCode addDealCode = new AddDealCode();
        addDealCode.setDealCodes(dealCodes);
        ((SmartGiftAction)smartGift.getScenarios().get(0).getSmartGiftActions().get(0)).setCode(addDealCode);
        if(addCondition){
            createSmartGiftCondition(smartGift);
        }
    }

    private static void setItemCode(ArrayList itemCodes, boolean addCondition, SmartClubDeal smartGift) throws Exception {
        AddItemCode itemCode = new AddItemCode();
        itemCode.setItemCodes(itemCodes);
        ((SmartGiftAction)(smartGift.getScenarios().get(0).getSmartGiftActions().get(0))).setItem(itemCode);
        if(addCondition){
            createSmartGiftCondition(smartGift);
        }
    }

    public static SmartGift createSmartGiftWithAddDiscount(String discountAmount, String isLimit, String limitNum, String timestamp, String setDiscountsCheckBox, String isOfferLimit, String limitOfferNum,
                                                           List<DiscountOnItem> discountOnItems, DiscountTypeOperation discountTypeOperation) throws Exception {
        SmartGift smartGift = BaseHub1Discount.createSmartGift(timestamp);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartGift);

        //create discount smartGiftAction
        AddDiscountAction discount = BaseHub1Discount.createAddDiscountAction(smartGift, "discount description " + timestamp, discountTypeOperation.toString());

        //percent off smartGiftAction
        PercentOff percentOff = BaseHub1Discount.createPercentOffDiscount(smartGift, discountAmount, isLimit, limitNum, setDiscountsCheckBox, isOfferLimit, limitOfferNum);
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        if(discountOnItems != null){
            createDiscountOnItems(discountOnItems, percentOff, smartGift);
        }
        BaseHub1Discount.createDiscount(smartGift, smartGiftAction, scenario, percentOff, discount);

        return smartGift;
    }

    public static SmartPunchCard createSmartPunchCardWithAddDiscount(String numPunches,String discountAmount, String isLimit, String limitNum, String timestamp, String setDiscountsCheckBox, String isOfferLimit, String limitOfferNum,
                                                           List<DiscountOnItem> discountOnItems, DiscountTypeOperation discountTypeOperation) throws Exception {
        SmartPunchCard smartPunchCard = BaseHub1Discount.createSmartPunchCard(timestamp,numPunches);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartPunchCard);

        //create discount smartGiftAction
        AddDiscountAction discount = BaseHub1Discount.createAddDiscountAction(smartPunchCard, "discount description " + timestamp, discountTypeOperation.toString());

        //percent off smartGiftAction
        PercentOff percentOff = BaseHub1Discount.createPercentOffDiscount(smartPunchCard, discountAmount, isLimit, limitNum, setDiscountsCheckBox, isOfferLimit, limitOfferNum);
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        if(discountOnItems != null){
            createDiscountOnItems(discountOnItems, percentOff, smartPunchCard);
        }
        BaseHub1Discount.createDiscount(smartPunchCard, smartGiftAction, scenario, percentOff, discount);

        return smartPunchCard;
    }


    public static SmartGift createSmartGiftWithDiscountsWithOfferLimit(String discountNum, String isOfferLimit, String limitOfferNum, String timestamp) throws Exception{

        DiscountOnItem item1 = buildDiscountItem(timestamp, DiscountsConstants.DISCOUNT_ON, ITEM_CODE_111, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "2");
        Thread.sleep(500);
        DiscountOnItem item2 = buildDiscountItem(String.valueOf(System.currentTimeMillis()), DiscountsConstants.DISCOUNT_ON, ITEM_CODE_222, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "3");
        return createSmartGiftWithAddDiscount(discountNum, IS_LIMIT_OFF, "", timestamp, DISCOUNTS_CHECK_BOX_ON, isOfferLimit, limitOfferNum, Lists.newArrayList(item1, item2), DiscountTypeOperation.PercentOff);
    }

    public static SmartPunchCard createSmartPunchCardWithDiscountsWithOfferLimit(String discountNum, String isOfferLimit, String limitOfferNum, String timestamp,String numPunches) throws Exception{

        DiscountOnItem item1 = buildDiscountItem(timestamp, DiscountsConstants.DISCOUNT_ON, ITEM_CODE_111, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "2");
        Thread.sleep(500);
        DiscountOnItem item2 = buildDiscountItem(String.valueOf(System.currentTimeMillis()), DiscountsConstants.DISCOUNT_ON, ITEM_CODE_222, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "3");
        return createSmartPunchCardWithAddDiscount(numPunches,discountNum, IS_LIMIT_OFF, "", timestamp, DISCOUNTS_CHECK_BOX_ON, isOfferLimit, limitOfferNum, Lists.newArrayList(item1, item2), DiscountTypeOperation.PercentOff);
    }


    public static SmartGift createSmartGiftWithDiscountsWithOfferLimit3itemsGroup (String discountNum, String isOfferLimit, String limitOfferNum, String timestamp) throws Exception{

        DiscountOnItem item1 = buildDiscountItem(timestamp,DiscountsConstants.DISCOUNT_ON, "111,222,333", DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Isoneof, "2");
        Thread.sleep(500);
        DiscountOnItem item2 = buildDiscountItem(String.valueOf(System.currentTimeMillis()), DiscountsConstants.DISCOUNT_ON, "444,555,666", DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Isoneof, "3");
        return createSmartGiftWithAddDiscount(discountNum,  "", "", timestamp, DISCOUNTS_CHECK_BOX_ON, isOfferLimit, limitOfferNum, Lists.newArrayList(item1, item2), DiscountTypeOperation.PercentOff);
    }

    public static SmartGift createSmartGiftWithDiscountsWithOfferLimit2discountsOneIsOff(String discountNum, String isOfferLimit, String limitOfferNum, String timestamp) throws Exception{

        DiscountOnItem item1 = buildDiscountItem(timestamp, DiscountsConstants.DISCOUNT_ON, ITEM_CODE_111, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "2");
        Thread.sleep(500);
        DiscountOnItem item2 = buildDiscountItem(String.valueOf(System.currentTimeMillis()), DiscountsConstants.DISCOUNT_OFF, ITEM_CODE_222, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "3");
        return createSmartGiftWithAddDiscount(discountNum,"", "", timestamp, DISCOUNTS_CHECK_BOX_ON, isOfferLimit, limitOfferNum, Lists.newArrayList(item1, item2), DiscountTypeOperation.PercentOff);

    }

    public static SmartGift createSmartGiftWithDiscountsWithOfferLimit3discountsOneIsOff(String discountNum, String isOfferLimit, String limitOfferNum, String timestamp) throws Exception{

        DiscountOnItem item1 = buildDiscountItem(timestamp, DiscountsConstants.DISCOUNT_ON, ITEM_CODE_111, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "2");
        Thread.sleep(500);
        DiscountOnItem item2 = buildDiscountItem(String.valueOf(System.currentTimeMillis()), DiscountsConstants.DISCOUNT_ON, ITEM_CODE_222, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "3");
        Thread.sleep(500);
        DiscountOnItem item3 = buildDiscountItem(String.valueOf(System.currentTimeMillis()), DiscountsConstants.DISCOUNT_OFF, ITEM_CODE_222, DiscountItems_ForEach.spend, GroupField.ItemCode, GroupOperators.Equals, "2");

        return createSmartGiftWithAddDiscount(discountNum, "", "", timestamp, DISCOUNTS_CHECK_BOX_ON, isOfferLimit, limitOfferNum, Lists.newArrayList(item1, item2, item3), DiscountTypeOperation.PercentOff);
    }

    public static SmartGift createSmartGiftWithDiscountsWithOfferLimit3differentdiscountsOneIsOff(String discountNum, String isOfferLimit, String limitOfferNum, String timestamp) throws Exception{

        DiscountOnItem item1 = buildDiscountItem(timestamp, DiscountsConstants.DISCOUNT_ON, ITEM_CODE_111, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "2");
        Thread.sleep(500);
        DiscountOnItem item2 = buildDiscountItem(String.valueOf(System.currentTimeMillis()), DiscountsConstants.DISCOUNT_OFF, ITEM_CODE_222, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "3");
        Thread.sleep(500);
        DiscountOnItem item3 = buildDiscountItem(String.valueOf(System.currentTimeMillis()), DiscountsConstants.DISCOUNT_OFF, ITEM_CODE_333, DiscountItems_ForEach.spend, GroupField.ItemCode, GroupOperators.Equals, "2");

        return createSmartGiftWithAddDiscount(discountNum,"", "", timestamp, DISCOUNTS_CHECK_BOX_ON, isOfferLimit, limitOfferNum, Lists.newArrayList(item1, item2, item3), DiscountTypeOperation.PercentOff);
    }

    public static SmartGift createSmartGiftWithDiscountBothBusketsEnabled(String amountOffNumber, String isLimit, String limitNum, String timestamp) throws Exception {

        DiscountOnItem item1 = buildDiscountItem(timestamp, DiscountsConstants.DISCOUNT_ON, ITEM_CODE_111, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "2");
        Thread.sleep(500);
        DiscountOnItem item2 = buildDiscountItem(String.valueOf(System.currentTimeMillis()), DiscountsConstants.DISCOUNT_ON, ITEM_CODE_222, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "3");
        return createSmartGiftWithAddDiscount(amountOffNumber, isLimit, limitNum, timestamp,DISCOUNTS_CHECK_BOX_ON, "", "", Lists.newArrayList(item1, item2), DiscountTypeOperation.AmountOff);

    }

    public static SmartGift createSmartGiftWithDiscountOneBusketsOff(String amountOffNumber, String isLimit, String limitNum, String timestamp) throws Exception {

        DiscountOnItem item1 = buildDiscountItem(timestamp, DiscountsConstants.DISCOUNT_OFF, ITEM_CODE_111, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "2");
        Thread.sleep(500);
        DiscountOnItem item2 = buildDiscountItem(String.valueOf(System.currentTimeMillis()), DiscountsConstants.DISCOUNT_ON, ITEM_CODE_222, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "3");
        return createSmartGiftWithAddDiscount(amountOffNumber, isLimit, limitNum, timestamp,DISCOUNTS_CHECK_BOX_ON, "", "", Lists.newArrayList(item1, item2), DiscountTypeOperation.AmountOff);

    }

    public static SmartPunchCard createSmartPunchCardWithDiscountOneBusketsOff(String amountOffNumber, String isLimit, String limitNum, String timestamp,String numPunches) throws Exception {

        DiscountOnItem item1 = buildDiscountItem(timestamp, DiscountsConstants.DISCOUNT_OFF, ITEM_CODE_111, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "2");
        Thread.sleep(500);
        DiscountOnItem item2 = buildDiscountItem(String.valueOf(System.currentTimeMillis()), DiscountsConstants.DISCOUNT_ON, ITEM_CODE_222, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "3");
        return createSmartPunchCardWithAddDiscount(numPunches,amountOffNumber, isLimit, limitNum, timestamp,DISCOUNTS_CHECK_BOX_ON, "", "", Lists.newArrayList(item1, item2), DiscountTypeOperation.AmountOff);

    }

    public static SmartGift createSmartGiftWithAddDealCodeWithConditionWithBasket(ArrayList dealCodes, String timestamp) throws Exception {

        SmartGift smartGift = BaseHub1Discount.createSmartGift(timestamp);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartGift);


        //Create smartGiftCondition
        IntOperator equal = hubAssetsCreator.getIntAssetOperator(smartGift, IntOperator.Int_Equals_enum.Greaterthan);
        ConditionFieldOprValue cond1 = hubAssetsCreator.createConditionNoGroup(smartGift, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "2");
        scenario.addCondition(cond1,smartGift);

        Group group = hubAssetsCreator.createGroup(smartGift,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(smartGift,GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_111,group);
        hubAssetsCreator.createGroupCondition(smartGift,GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_222,group);
        cond1.setGroup(group);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDealCode.toString());
        AddDealCode addDealCode = new AddDealCode();
        addDealCode.setDealCodes(dealCodes);
        smartGiftAction.setCode(addDealCode);
        scenario.addAction(smartGiftAction, smartGift);


        AssetService.createSmartGift(smartGift);

        return smartGift;

    }

    public static SmartPunchCard createSmartPunchCardWithAddDealCodeWithCondition(ArrayList dealCodes, String timestamp,String numOfPunches) throws Exception {

        SmartPunchCard smartPunchCard = BaseHub1Discount.createSmartPunchCard(timestamp,numOfPunches);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartPunchCard);

        //Create smartGiftCondition
        IntOperator equal = hubAssetsCreator.getIntAssetOperator(smartPunchCard, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(smartPunchCard, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "2");
        scenario.addCondition(cond1,smartPunchCard);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDealCode.toString());
        AddDealCode addDealCode = new AddDealCode();
        addDealCode.setDealCodes(dealCodes);
        smartGiftAction.setCode(addDealCode);
        scenario.addAction(smartGiftAction, smartPunchCard);



        AssetService.createSmartPunchCard(smartPunchCard);
        log4j.info("Punch card was created succesfully.Title is:  " + smartPunchCard.getTitle());

        return smartPunchCard;

    }

    public static SmartPunchCard createSmartPunchCardWithAddDealCodeWithConditionWithBasket(ArrayList dealCodes, String timestamp,String numOfPunches) throws Exception {

        SmartPunchCard smartPunchCard = BaseHub1Discount.createSmartPunchCard(timestamp,numOfPunches);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartPunchCard);


        //Create smartGiftCondition
        IntOperator equal = hubAssetsCreator.getIntAssetOperator(smartPunchCard, IntOperator.Int_Equals_enum.Greaterthan);
        ConditionFieldOprValue cond1 = hubAssetsCreator.createConditionNoGroup(smartPunchCard, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "2");
        scenario.addCondition(cond1,smartPunchCard);

        Group group = hubAssetsCreator.createGroup(smartPunchCard,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(smartPunchCard,GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_111,group);
        hubAssetsCreator.createGroupCondition(smartPunchCard,GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_222,group);
        cond1.setGroup(group);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDealCode.toString());
        AddDealCode addDealCode = new AddDealCode();
        addDealCode.setDealCodes(dealCodes);
        smartGiftAction.setCode(addDealCode);
        scenario.addAction(smartGiftAction, smartPunchCard);


        AssetService.createSmartPunchCard(smartPunchCard);

        return smartPunchCard;

    }

    public static SmartPunchCard createSmartPunchCardWithAddItemCodeWithCondition(ArrayList itemCodes,String timestamp,String numOfPunches) throws Exception {

        SmartPunchCard smartPunchCard = BaseHub1Discount.createSmartPunchCard(timestamp,numOfPunches);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartPunchCard);

        //Create smartGiftCondition
        IntOperator equal = hubAssetsCreator.getIntAssetOperator(smartPunchCard, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(smartPunchCard, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "2");
        scenario.addCondition(cond1,smartPunchCard);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddItemCode.toString());
        AddItemCode itemCode = new AddItemCode();
        itemCode.setItemCodes(itemCodes);
        smartGiftAction.setItem(itemCode);
        scenario.addAction(smartGiftAction, smartPunchCard);

        AssetService.createSmartPunchCard(smartPunchCard);
        log4j.info("Punch card was created succesfully. Title is:  " + smartPunchCard.getTitle());

        return smartPunchCard;

    }

    public static SmartPunchCard createSmartPunchCardWithAddItemCodeWithConditionWithBasket(ArrayList itemCodes,String timestamp,String numOfPunches) throws Exception {

        SmartPunchCard smartPunchCard = BaseHub1Discount.createSmartPunchCard(timestamp,numOfPunches);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartPunchCard);
        //Create smartGiftCondition
        IntOperator equal = hubAssetsCreator.getIntAssetOperator(smartPunchCard, IntOperator.Int_Equals_enum.Greaterthan);
        ConditionFieldOprValue cond1 = hubAssetsCreator.createConditionNoGroup(smartPunchCard, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "2");
        scenario.addCondition(cond1,smartPunchCard);

        Group group = hubAssetsCreator.createGroup(smartPunchCard,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(smartPunchCard, GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_111,group);
        hubAssetsCreator.createGroupCondition(smartPunchCard,GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_222,group);
        cond1.setGroup(group);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddItemCode.toString());
        AddItemCode itemCode = new AddItemCode();
        itemCode.setItemCodes(itemCodes);
        smartGiftAction.setItem(itemCode);
        scenario.addAction(smartGiftAction, smartPunchCard);


        AssetService.createSmartPunchCard(smartPunchCard);
        log4j.info("Punch card was created succesfully. Title is:  " + smartPunchCard.getTitle());

        return smartPunchCard;

    }




    public static SmartGift createSmartGiftWithDiscountsWithOfferLimit3differentdiscountsOneIsOffOld(String discountNum, String isLimit, String limitNum, String timestamp) throws Exception{

        SmartGift smartGift = BaseHub1Discount.createSmartGift(timestamp);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartGift);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());
        //create discount smartGiftAction
        AddDiscountAction discount = BaseHub1Discount.createAddDiscountAction(smartGift, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());
        //percent off smartGiftAction
        PercentOff percentOff = BaseHub1Discount.createPercentOffDiscount(smartGift, discountNum, BaseHub1Discount.IS_LIMIT_OFF, "", DISCOUNTS_CHECK_BOX_ON, isLimit, limitNum);

        //Create new group
        Group group = createGroupWithConds(timestamp, smartGift, createDiscountWithGroup(percentOff,DiscountItems_ForEach.quantity.toString(),"2", DISCOUNT_ON, smartGift));
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartGift, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);
        Thread.sleep(1000);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartGift, createDiscountWithGroup(percentOff,DiscountItems_ForEach.quantity.toString(),"3", DISCOUNT_ON, smartGift));
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartGift, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);

        Thread.sleep(1000);
        //Create new group3
        Group group3 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartGift, createDiscountWithGroup(percentOff,DiscountItems_ForEach.spend.toString(),"2", DISCOUNT_OFF, smartGift));
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartGift, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_333, group3);

        BaseHub1Discount.createDiscount(smartGift, smartGiftAction, scenario, percentOff, discount);
        return smartGift;
    }

    public static SmartGift createSmartGiftWithAddItemCodeWithConditionWithBasketWithRelatedDealCodes(ArrayList dealCodes, String timestamp) throws Exception {

        SmartGift smartGift = BaseHub1Discount.createSmartGift(timestamp);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartGift);

        //Create smartGiftCondition
        IntOperator equal = hubAssetsCreator.getIntAssetOperator(smartGift, IntOperator.Int_Equals_enum.Greaterthan);
        ConditionFieldOprValue cond1 = hubAssetsCreator.createConditionNoGroup(smartGift, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "2");
        scenario.addCondition(cond1,smartGift);

        Group group = hubAssetsCreator.createGroup(smartGift,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(smartGift,GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_111,group);
        hubAssetsCreator.createGroupCondition(smartGift,GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_222,group);
        cond1.setGroup(group);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDealCode.toString());
        AddDealCode addDealCode = new AddDealCode();
        addDealCode.setDealCodes(dealCodes);
        smartGiftAction.setCode(addDealCode);
        addDealCode.setDefineRelatedItem("y");

        RelatedDealCode relatedDealCode = new RelatedDealCode();
        relatedDealCode.setDealCodeValue("222334");
        relatedDealCode.setDealCodes(com.google.appengine.labs.repackaged.com.google.common.collect.Lists.newArrayList("1234"));
        addDealCode.setRelatedRelatedDealCodes(com.google.appengine.labs.repackaged.com.google.common.collect.Lists.newArrayList(relatedDealCode));
        scenario.addAction(smartGiftAction, smartGift);

        AssetService.createSmartGift(smartGift);
        return smartGift;

    }

    public static void createSmartClubDealWithDiscountWith12and1items(String isLimit, String limitNum, String isLimitOffer, String limiitOfferNum, String timestamp) throws Exception {

        SmartClubDeal smartClubDeal = BaseHub1Discount.createSmartClubDeal(timestamp);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.FreeItem.toString());

        //percent off smartGiftAction
        FreeItem freeItem = BaseHub1Discount.createFreeItemDiscount(smartClubDeal, isLimit, limitNum, DISCOUNTS_CHECK_BOX_ON, isLimitOffer, limiitOfferNum);

        SetDiscountOnItems d = createSetDiscountOnItemFreeItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "12", DISCOUNT_OFF, freeItem);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnItemFreeItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_ON, freeItem);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        BaseHub1Discount.createDiscountFreeItem(smartClubDeal, smartGiftAction, scenario, freeItem, discount);

    }

    public static void createSmartClubDealWithDiscountBothBasketsEnabled(String amountOffNumber, String isLimit, String limitNum, String timestamp,
                                                                                  String basketQuantity1, String basketQuantity2) throws Exception {
        createSmartClubDealWithDiscount(amountOffNumber, isLimit, limitNum, timestamp,basketQuantity1, basketQuantity2, DISCOUNT_ON, DISCOUNT_ON);
    }

    public static SmartClubDeal createSmartClubDealWithDiscount(String amountOffNumber,  String isLimitOffer, String limitOfferNum, String timestamp,
                                                                                  String basketQuantity1, String basketQuantity2, String onOff1, String onOff2) throws Exception {

        SmartClubDeal smartClubDeal = BaseHub1Discount.createSmartClubDeal(timestamp);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.AmountOff.toString());

        //percent off smartGiftAction
        AmountOff amountOff = BaseHub1Discount.createAmountOffDiscount(smartClubDeal, amountOffNumber,DISCOUNTS_CHECK_BOX_ON, isLimitOffer, limitOfferNum);

        SetDiscountOnItems d = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), basketQuantity1, onOff1, amountOff);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), basketQuantity2, onOff2, amountOff);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        createDiscount(smartClubDeal, smartGiftAction, scenario, amountOff, discount);

        return smartClubDeal;
    }



    public static void createSmartClubDealWithDiscountFreeItem(String isLimit, String limitNum, String isLimitOffer, String limiitOfferNum, String timestamp) throws Exception {

        SmartClubDeal smartClubDeal = BaseHub1Discount.createSmartClubDeal(timestamp);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.FreeItem.toString());

        //percent off smartGiftAction
        FreeItem freeItem = BaseHub1Discount.createFreeItemDiscount(smartClubDeal, isLimit, limitNum, DISCOUNTS_CHECK_BOX_ON, isLimitOffer, limiitOfferNum);

        SetDiscountOnItems d = createSetDiscountOnItemFreeItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_ON, freeItem);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal,  d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnItemFreeItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_OFF, freeItem);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        BaseHub1Discount.createDiscountFreeItem(smartClubDeal, smartGiftAction, scenario, freeItem, discount);
    }

    public static SmartClubDeal createSmartClubDealWithDiscount(String percentOffNumber, String isLimit, String limitNum, String timestamp) throws Exception {

        SmartClubDeal smartClubDeal = BaseHub1Discount.createSmartClubDeal(timestamp);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());

        //percent off smartGiftAction
        PercentOff percentOff = BaseHub1Discount.createPercentOffDiscount(smartClubDeal, percentOffNumber, isLimit, limitNum, DISCOUNTS_CHECK_BOX_ON, BaseHub1Discount.IS_LIMIT_OFFER_OFF, "");

        SetDiscountOnItems d = createSetDiscountOnItemPercent(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_ON, percentOff);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnItemPercent(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_OFF, percentOff);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        BaseHub1Discount.createDiscount(smartClubDeal, smartGiftAction, scenario, percentOff, discount);

        return smartClubDeal;

    }

    public static void createSmartClubDealWithDiscountNoGroup(String percentOffNumber, String isLimit, String limitNum, String timestamp) throws Exception {

        SmartClubDeal smartClubDeal = BaseHub1Discount.createSmartClubDeal(timestamp);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());

        //percent off smartGiftAction
        PercentOff percentOff = BaseHub1Discount.createPercentOffDiscount(smartClubDeal, percentOffNumber, isLimit, limitNum, DISCOUNTS_CHECK_BOX_OFF, BaseHub1Discount.IS_LIMIT_OFFER_OFF, "");
        BaseHub1Discount.createDiscount(smartClubDeal, smartGiftAction, scenario, percentOff, discount);
    }

    public static SmartClubDeal createSmartClubDealWithDiscountsWithOfferLimit(String discountNum, String isOfferLimit, String limitOfferNum, String timestamp) throws Exception {

        SmartClubDeal smartClubDeal = BaseHub1Discount.createSmartClubDeal(timestamp);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartClubDeal);


        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());

        //percent off smartGiftAction
        PercentOff percentOff = BaseHub1Discount.createPercentOffDiscount(smartClubDeal, discountNum, BaseHub1Discount.IS_LIMIT_OFF, "", DISCOUNTS_CHECK_BOX_ON, isOfferLimit, limitOfferNum);

        SetDiscountOnItems d = BaseHub1Discount.createSetDiscountOnTimes(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "2", DISCOUNT_ON);
        percentOff.addSetDiscountOnItems(d, smartClubDeal);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = BaseHub1Discount.createSetDiscountOnTimes(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "3",DISCOUNT_ON);
        percentOff.addSetDiscountOnItems(d2, smartClubDeal);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        BaseHub1Discount.createDiscount(smartClubDeal, smartGiftAction, scenario, percentOff, discount);

        return smartClubDeal;
    }

    public static void createSmartGiftWithDiscountsWithOfferLimit3discountsTwoAreOff(String discountNum, String isOfferLimit, String limitOfferNum, String timestamp) throws Exception {

        SmartClubDeal smartClubDeal = BaseHub1Discount.createSmartClubDeal(timestamp);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());
        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());
        //percent off smartGiftAction
        PercentOff percentOff = BaseHub1Discount.createPercentOffDiscount(smartClubDeal, discountNum, BaseHub1Discount.IS_LIMIT_OFF, "", DISCOUNTS_CHECK_BOX_ON, isOfferLimit, limitOfferNum);

        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal, createDiscountWithGroup(percentOff, DiscountItems_ForEach.quantity.toString(), "2", DISCOUNT_ON, smartClubDeal));
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Isoneof.toString(), "668773,525561", group);

        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, createDiscountWithGroup(percentOff, DiscountItems_ForEach.quantity.toString(), "3", DISCOUNT_OFF, smartClubDeal));
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Isoneof.toString(), "325242,526251", group2);

        Thread.sleep(1000);
        //Create new group3
        Group group3 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, createDiscountWithGroup(percentOff, DiscountItems_ForEach.spend.toString(), "2", DISCOUNT_OFF, smartClubDeal));
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Isoneof.toString(), "322122,821172", group3);

        BaseHub1Discount.createDiscount(smartClubDeal, smartGiftAction, scenario, percentOff, discount);
    }

    public static DiscountOnItem buildDiscountItem(String timestamp, String onOff, String itemName, DiscountItems_ForEach discountItemType, GroupField groupField,
                                                   GroupOperators groupOperators, String count) {
        DiscountOnItem item1 = new DiscountOnItem();
        item1.setTimeStamp(timestamp);
        item1.setOnOff(onOff);
        item1.setItemName(itemName);
        item1.setDiscountItems_forEach(discountItemType);
        item1.setGroupField(groupField);
        item1.setGroupOperators(groupOperators);
        item1.setCount(count);
        return item1;
    }

    private static SetDiscountOnItems createSetDiscountOnItem(ISmartAssetCounter smartAsset, String quanityType, String count, String onnOff, AmountOff amountOff) {
        SetDiscountOnItems d = BaseHub1Discount.createSetDiscountOnTimes(smartAsset, quanityType, count, onnOff);
        amountOff.addSetDiscountOnItems(d, smartAsset);
        return d;
    }


    private static SetDiscountOnItems createSetDiscountOnItemFreeItem(SmartClubDeal smartGift, String quanityType, String count, String onnOff,  FreeItem freeItem) {
        SetDiscountOnItems d = BaseHub1Discount.createSetDiscountOnTimes(smartGift, quanityType, count, onnOff);
        freeItem.addSetDiscountOnItems(d, smartGift);

        return d;
    }

    private static SetDiscountOnItems createSetDiscountOnItemPercent(ISmartAssetCounter smartAsset, String quanityType, String count, String onnOff, IDiscountType discountType){

        SetDiscountOnItems d = BaseHub1Discount.createSetDiscountOnTimes(smartAsset, quanityType, count, onnOff);
        discountType.addSetDiscountOnItems(d, smartAsset);
        return d;
    }

    private static Group createGroupWithConds(String timestamp, ISmartAssetCounter smartGift, SetDiscountOnItems d) {
        //Create new group
        Group group = hubAssetsCreator.createGroup(smartGift, "new group" + timestamp);
        d.setGroup(group, smartGift);
        return group;
    }

    private static void createDiscount(SmartClubDeal smartClubDeal, SmartGiftAction smartGiftAction, Scenario scenario, AmountOff amountOff, AddDiscountAction discount) throws Exception {
        if (amountOff != null)
            discount.setAmountOff(amountOff);
        if (discount != null)
            smartGiftAction.setDiscount(discount);
        scenario.addAction(smartGiftAction, smartClubDeal);
        AssetService.createSmartClubDeal(smartClubDeal);
        log4j.info("Gift was created succesfully. Gift's title is:  " + smartClubDeal.getTitle());
    }

    private static SetDiscountOnItems createDiscountWithGroup (IDiscountType iDiscountType, String discountType, String discountNum, String discountOffOn, SmartClubDeal smartClubDeal){

        SetDiscountOnItems d = BaseHub1Discount.createSetDiscountOnTimes(smartClubDeal, discountType, discountNum , discountOffOn);
        iDiscountType.addSetDiscountOnItems(d, smartClubDeal);

        return d;
    }

    private static void createSmartGiftCondition(SmartClubDeal smartGift) throws Exception {
        IntOperator equal = hubAssetsCreator.getIntAssetOperator(smartGift, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(smartGift, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "2");
        smartGift.getScenarios().get(0).addCondition(cond1,smartGift);
    }

    private static void createDiscountOnItem(String timestamp, ISmartAssetCounter smartAsset, IDiscountType discountType, String onnOff, String value, String count,
                                             DiscountItems_ForEach discountItems_forEach, GroupField groupField, GroupOperators groupOperator) {
        SetDiscountOnItems d = createSetDiscountOnItemPercent(smartAsset, discountItems_forEach.toString(), count, onnOff, discountType);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartAsset, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartAsset, groupField, groupOperator.toString(), value, group);
    }

    private static void createDiscountOnItems(List<DiscountOnItem> discountOnItems,IDiscountType discountType, ISmartAssetCounter smartAsset){
        for(DiscountOnItem discountOnItem : discountOnItems){
            createDiscountOnItem(discountOnItem.getTimeStamp(), smartAsset, discountType, discountOnItem.getOnOff(), discountOnItem.getItemName(),
                    discountOnItem.getCount(), discountOnItem.getDiscountItems_forEach(), discountOnItem.getGroupField(), discountOnItem.getGroupOperators());
        }
    }

}
