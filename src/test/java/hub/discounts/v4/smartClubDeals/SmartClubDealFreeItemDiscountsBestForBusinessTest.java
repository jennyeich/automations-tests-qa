package hub.discounts.v4.smartClubDeals;

import hub.base.categories.hub1Categories.Hub1Regression;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Anastasiya on 11/7/2017.
 */

public class SmartClubDealFreeItemDiscountsBestForBusinessTest extends BaseHub1SmartClubDealBestForBusinessTest {


    /////////////////////////////////////////////
    //////////////   T E S T S   ////////////////
    /////////////////////////////////////////////


    @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroup1() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "",1,100000, 100000,EXPECT_NO_DISCOUNT);
    }


    @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroup2() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "",1,10000, 3,90000,100000,-10000);
    }


    @Test
    @Category(Hub1Regression.class)
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroup3() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "",2,20000, 2,60000,80000,-20000);
    }

    @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroup4() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "",5,50000, 3,90000,140000,-30000);
    }


    @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroup5() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "",12,12000, 3,90000,102000,-30000, true);
    }

    @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroup6() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "",24,24000, 3,90000,114000,-60000,true);
    }

    @Test
    public void testSmartClubDealFreeItemBestForBusinessLimitAmount100test7() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_CHECKED,"100",IS_LIMIT_OFFER_OFF, "",1,10000, 10000,EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartClubDealFreeItemBestForBusinessLimitAmount100test8() throws Exception {
       runSmartClubDealTestWithParams(IS_LIMIT_CHECKED,"100",IS_LIMIT_OFFER_OFF, "",1,10000, 3,90000,100000,-10000);
    }

    @Test
    public void testSmartClubDealFreeItemBestForBusinessLimitAmount100test9() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_CHECKED,"100",IS_LIMIT_OFFER_OFF, "",1,20000, 2,60000,80000,-10000);
    }

    @Test
    public void testSmartClubDealFreeItemBestForBusinessLimitAmount100test10() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_CHECKED,"100",IS_LIMIT_OFFER_OFF, "",5,50000, 3,90000,140000,-10000);
    }

    @Test
    public void testSmartClubDealFreeItemBestForBusinessLimitOfer1test11() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_CHECKED, "1",5,50000, 3,90000,140000,-10000);
    }



    @Test
    public void testSmartClubDealFreeItemBestForBusinessLimitOfer1test12() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_CHECKED, "1",1,10000, 1,30000,40000,-10000);

    }








}



