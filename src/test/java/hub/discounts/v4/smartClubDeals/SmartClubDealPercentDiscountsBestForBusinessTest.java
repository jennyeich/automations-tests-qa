package hub.discounts.v4.smartClubDeals;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.discounts.v4.GetBenefitsHelper;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Goni on 7/18/2017.
 */

public class SmartClubDealPercentDiscountsBestForBusinessTest extends BaseHub1SmartClubDealBestForBusinessTest {

	@Test
	public void testSmartClubDeal33PercentWithGroupBestForBusiness1() throws Exception {
		runSmartClubDealTestWithParamsPercentDiscount("33",IS_LIMIT_OFF,"", 1,3000,1, 4000,7000,-990);
	}

	@Test
	public void testSmartClubDeal20PercentWithoutGroupNoLimitBestForBusiness2() throws Exception {
		runSmartClubDealTestWithParamsPercentDiscountNoGroup("20",IS_LIMIT_OFF,"", 1,3000,1, 4000,7000,-1400);
	}

	@Test
	public void testSmartClubDeal0PercentWithoutGroupNoLimitBestForBusiness3() throws Exception {
		runSmartClubDealTestWithParamsPercentDiscountNoGroup("0",IS_LIMIT_OFF,"", 1,3000,1, 4000,7000,EXPECT_NO_DISCOUNT);
	}

	@Test
	@Category(Hub1Regression.class)
	public void testSmartClubDeal100PercentWithoutGroupNoLimitBestForBusiness4() throws Exception {
		runSmartClubDealTestWithParamsPercentDiscountNoGroup("100",IS_LIMIT_OFF,"", 1,3000,1, 4000,7000,-7000);
	}

	@Test
	public void testSmartClubDeal50PercentWithoutGroup200LimitBestForBusiness5() throws Exception {
		runSmartClubDealTestWithParamsPercentDiscountNoGroup("50",IS_LIMIT_CHECKED,"200", 1,15000,1, 25000,40000,-20000);
	}

	@Test
	public void testSmartClubDeal50PercentWithoutGroup100LimitBestForBusiness6() throws Exception {
		runSmartClubDealTestWithParamsPercentDiscountNoGroup("50",IS_LIMIT_CHECKED,"100", 1,15000,1, 25000,40000,-10000);
	}

	@Test
	public void testSmartClubDeal50PercentWithoutGroup300LimitBestForBusiness7() throws Exception {
		runSmartClubDealTestWithParamsPercentDiscountNoGroup("50",IS_LIMIT_CHECKED,"300", 1,25000,1, 15000,40000,-20000);
	}

	@Test
	public void testSmartClubDeal0PercentWithoutGroup0LimitBestForBusiness8() throws Exception {
		runSmartClubDealTestWithParamsPercentDiscountNoGroup("0",IS_LIMIT_CHECKED,"0", 1,25000,1, 15000,40000,EXPECT_NO_DISCOUNT);
	}

	@Test
	public void testSmartClubDeal0PercentWithoutGroup100LimitBestForBusiness9() throws Exception {
		runSmartClubDealTestWithParamsPercentDiscountNoGroup("0",IS_LIMIT_CHECKED,"100", 1,25000,1, 15000,40000,EXPECT_NO_DISCOUNT);
	}

	@Test
	public void testSmartClubDeal100PercentWithoutGroup100LimitBestForBusiness10() throws Exception {
		runSmartClubDealTestWithParamsPercentDiscountNoGroup("100",IS_LIMIT_CHECKED,"100", 1,25000,1, 15000,40000,-10000);
	}

	@Test
	public void testSmartClubDeal100PercentWithoutGroup500LimitBestForBusiness11() throws Exception {
		runSmartClubDealTestWithParamsPercentDiscountNoGroup("100",IS_LIMIT_CHECKED,"500", 1,25000,1, 15000,40000,-40000);
	}

	@Test
	public void testSmartClubDeal40PercentWithoutLimitOfer3Buskets2disabledBestForBusiness11() throws Exception {
		runSmartClubDealTestWithDiscountsWithOfferLimit3discountsTwoAreOff("40",IS_LIMIT_OFFER_OFF,"", 1,1200,1, 1700,2900,EXPECT_NO_DISCOUNT);
	}

	@Test
	public void testSmartClubDeal40PercentWithout3Buskets2disabledBestForBusiness12() throws Exception {
		runSmartClubDealTestWithDiscountsWithOfferLimit3discountsTwoAreOff("40",IS_LIMIT_OFFER_OFF,"", 2,2400,2, 3400, 2, 3000,8800,EXPECT_NO_DISCOUNT);
	}

	@Test
	public void testSmartClubDeal40PercentWithout3Buskets2disabledBestForBusiness13() throws Exception {
		runSmartClubDealTestWithDiscountsWithOfferLimit3discountsTwoAreOff("40",IS_LIMIT_OFFER_OFF,"", 2,2400,2, 5100, 3, 3000,10500,-960);
	}

	@Test
	public void testSmartClubDeal40PercentWithout3Buskets2disabledBestForBusiness14() throws Exception {
		runSmartClubDealTestWithDiscountsWithOfferLimit3discountsTwoAreOff("40",IS_LIMIT_OFFER_OFF,"", 4,4800,5, 8500, 4, 3000,16300,-960);
	}

	@Test
	public void testSmartClubDeal40PercentWithout3Buskets2disabledBestForBusiness15() throws Exception {
		runSmartClubDealTestWithDiscountsWithOfferLimit3discountsTwoAreOff("40",IS_LIMIT_OFFER_OFF,"", 4,4800,5, 8500, 7, 3000,16300,-1920);
	}

	@Test
	public void testSmartClubDeal25PercentWith2EnabledBusketsNoLimitBestForBusiness16() throws Exception {
		runSmartClubDealTestWithDiscountsWithOfferLimit("25",IS_LIMIT_OFFER_CHECKED,"1", 5,15000,3, 12000,27000,-4500);
	}

	@Test
	public void testSmartClubDeal25PercentWith2EnabledBusketsNoLimitBestForBusiness17() throws Exception {
		runSmartClubDealTestWithDiscountsWithOfferLimit("25",IS_LIMIT_OFFER_CHECKED,"1", 2,6000,8, 32000,38000,-4500);
	}

	@Test
	public void testSmartClubDeal25PercentWith2EnabledBusketsNoLimitBestForBusiness18() throws Exception {
		runSmartClubDealTestWithDiscountsWithOfferLimit("25",IS_LIMIT_OFFER_CHECKED,"1", 7,21000,10, 40000,61000,-4500);
	}

	private void runSmartClubDealTestWithParamsPercentDiscount(String percentOffNumber, String isLimit, String limitNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
		GetBenefitsHelper.createSmartClubDealWithDiscount(percentOffNumber, isLimit, limitNum, timestamp);
		performPurchase(quantity1, price1, quantity2, price2, totalSum, expected);
	}

	private void runSmartClubDealTestWithParamsPercentDiscountNoGroup(String percentOffNumber, String isLimit, String limitNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
		GetBenefitsHelper.createSmartClubDealWithDiscountNoGroup(percentOffNumber, isLimit, limitNum, timestamp);
		performPurchase(quantity1, price1, quantity2, price2, totalSum, expected);
	}

	private void runSmartClubDealTestWithDiscountsWithOfferLimit3discountsTwoAreOff(String percentOffNumber, String isLimitOffer, String limitOfferNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
		GetBenefitsHelper.createSmartGiftWithDiscountsWithOfferLimit3discountsTwoAreOff(percentOffNumber, isLimitOffer, limitOfferNum, timestamp);
		performPurchase(quantity1, price1, quantity2, price2, totalSum, expected);
	}

	private void runSmartClubDealTestWithDiscountsWithOfferLimit3discountsTwoAreOff(String percentOffNumber,  String isOfferLimit, String limitOfferNum, int quantity1, int price1, int quantity2, int price2, int quantity3, int price3, int totalSum, int expected) throws Exception{
		GetBenefitsHelper.createSmartGiftWithDiscountsWithOfferLimit3discountsTwoAreOff(percentOffNumber, isOfferLimit, limitOfferNum, timestamp);
		performPurchase(quantity1, price1, quantity2, price2, quantity3, price3, totalSum, expected);
	}

	private void runSmartClubDealTestWithDiscountsWithOfferLimit(String percentOffNumber, String isLimitOffer, String limitOfferNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
		GetBenefitsHelper.createSmartClubDealWithDiscountsWithOfferLimit(percentOffNumber, isLimitOffer, limitOfferNum, timestamp);
		performPurchase(quantity1, price1, quantity2, price2, totalSum, expected);
	}
}

