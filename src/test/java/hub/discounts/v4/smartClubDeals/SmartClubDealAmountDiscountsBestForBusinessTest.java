package hub.discounts.v4.smartClubDeals;


import hub.base.categories.hub1Categories.Hub1Regression;
import hub.discounts.v4.GetBenefitsHelper;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Anastasiya on 11/7/2017.
 */

public class SmartClubDealAmountDiscountsBestForBusinessTest extends BaseHub1SmartClubDealBestForBusinessTest {


    /////////////////////////////////////////////
    //////////////   T E S T S   ////////////////
    /////////////////////////////////////////////

    @Test
    public void testSmartClubDealAmountOff50WithGroup1() throws Exception {
        runSmartClubDealTestWithParams("50", IS_LIMIT_OFFER_OFF,"",1,10000, 2,90000,100000,EXPECT_NO_DISCOUNT, "2", "3");
    }


    @Test
    public void testSmartClubDealAmountOff50WithGroup2() throws Exception {
        runSmartClubDealTestWithParams("50", IS_LIMIT_OFFER_OFF,"",2,20000, 2,60000,80000,EXPECT_NO_DISCOUNT, "2", "3");
    }

    @Test
    @Category(Hub1Regression.class)
    public void testSmartClubDealAmountOff50WithGroup3() throws Exception {
        runSmartClubDealTestWithParams("50", IS_LIMIT_OFFER_OFF, "", 2, 20000, 3, 90000, 110000, -5000, "2", "3");
    }


    @Test
    public void testSmartClubDealAmountOff50WithGroup4() throws Exception {
        runSmartClubDealTestWithParams("50", IS_LIMIT_OFFER_OFF,"",5,50000, 3,90000,140000,-5000, "2", "3");
    }

    @Test
    public void testSmartClubDealAmountOff50WithGroup5() throws Exception {
        runSmartClubDealTestWithParams("50", IS_LIMIT_OFFER_OFF,"",2,20000, 8,240000,260000,-5000, "2", "3");
    }


    @Test
    public void testSmartClubDealAmountOff80WithGroup6() throws Exception {
        runSmartClubDealTestWithParams("80", IS_LIMIT_CHECKED,"1",5,50000, 3,90000,140000,-8000, "2", "3");
    }

    @Test
    public void testSmartClubDealAmountOff80WithGroup7() throws Exception {
        runSmartClubDealTestWithParams("80", IS_LIMIT_CHECKED,"1",2,20000, 8,240000,260000,-8000, "2", "3");
    }

    @Test
    public void testSmartClubDealAmountOffBestForBusiness80WithGroup8() throws Exception {
        runSmartClubDealTestWithParams("80", IS_LIMIT_CHECKED,"4",20,200000, 15,450000,650000,-32000, "2", "3");

    }

    @Test
    public void testSmartClubDealAmountOffBestForBusiness80WithGroup9() throws Exception {
        runSmartClubDealTestWithParams("80", IS_LIMIT_CHECKED,"4",7,70000, 7,210000,280000,-16000, "2", "3");
    }

    @Test
    public void testSmartClubDealAmountOffBestForBusiness120WithGroup10() throws Exception {
        runSmartClubDealTestWithParams("120",IS_LIMIT_OFFER_OFF,"",1,120000, 15,450000,570000,-12000, "1", "3");
    }

    @Test
    public void testSmartClubDealAmountOffBestForBusiness120WithGroup11() throws Exception {
        runSmartClubDealTestWithParams("120", IS_LIMIT_OFFER_OFF,"",9,1080000, 15,450000,1530000,-24000, "4", "3");
    }

    @Test
    public void testSmartClubDealAmountOff50WithGroup12() throws Exception {
        runSmartClubDealTestWith2BusketsOneOff("50", IS_LIMIT_OFFER_OFF,"",2,0, 3,90000,90000,-5000, "2", "3",DISCOUNT_OFF,DISCOUNT_ON);
    }

    @Test
    public void testSmartClubDealAmountOff50WithGroup13() throws Exception {
        runSmartClubDealTestWithParams("50", IS_LIMIT_OFFER_OFF,"",2,0, 3,90000,90000,EXPECT_NO_DISCOUNT, "2", "3");
    }

    @Test
    public void testSmartClubDealAmountOff50WithGroup14() throws Exception {
        runSmartClubDealTestWithParams("50", IS_LIMIT_OFFER_OFF,"",2,90000, 1,0,90000,EXPECT_NO_DISCOUNT, "2", "1");
    }

    @Test
    public void testSmartClubDealAmountOff50WithGroup15() throws Exception {
        runSmartClubDealTestWith2BusketsOneOff("50", IS_LIMIT_OFFER_OFF,"",2,90000, 1,0,90000,-5000, "2", "1",DISCOUNT_ON,DISCOUNT_OFF);
    }
    /////////////////////////////////////////////
    ////////// H E L P E R   F U N C S //////////
    /////////////////////////////////////////////


    private void runSmartClubDealTestWithParams(String amountOffNumber, String isLimit, String limitNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected, String basketQuantity1, String basketQuantity2) throws Exception{
        GetBenefitsHelper.createSmartClubDealWithDiscountBothBasketsEnabled(amountOffNumber, isLimit, limitNum, timestamp, basketQuantity1, basketQuantity2);
        performPurchase(quantity1, price1, quantity2, price2, totalSum, expected);

    }

    private void runSmartClubDealTestWith2BusketsOneOff(String amountOffNumber, String isLimit, String limitNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected, String basketQuantity1, String basketQuantity2,String onOff1, String onOff2) throws Exception{
        GetBenefitsHelper.createSmartClubDealWithDiscount(amountOffNumber, isLimit, limitNum, timestamp,basketQuantity1, basketQuantity2, onOff1, onOff2);
        performPurchase(quantity1, price1, quantity2, price2, totalSum, expected);

    }

}



