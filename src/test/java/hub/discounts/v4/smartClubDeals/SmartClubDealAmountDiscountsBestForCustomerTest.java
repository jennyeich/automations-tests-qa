package hub.discounts.v4.smartClubDeals;

import hub.base.categories.hub1Categories.Hub1Regression;
import org.junit.*;
import org.junit.experimental.categories.Category;

/**
 * Created by anastasiya on 11/07/2017.
 */
public class SmartClubDealAmountDiscountsBestForCustomerTest extends BaseHub1SmartClubDealBestForCustomerTest {


    /////////////////////////////////////////////
    //////////////   T E S T S   ////////////////
    /////////////////////////////////////////////


    @Test
    public void testSmartClubDealAmountOffBestForCustomer50WithGroup1() throws Exception {
        runSmartClubDealTestWithParams("50", IS_LIMIT_OFFER_OFF,"",1,10000, 3,90000,100000,EXPECT_NO_DISCOUNT, "2", "3",DISCOUNT_ON, DISCOUNT_OFF);
    }

    @Test
    public void testSmartClubDealAmountOffBestForCustomer50WithGroup2() throws Exception {
        runSmartClubDealTestWithParams("50", IS_LIMIT_OFFER_OFF,"",2,20000, 2,60000,80000,EXPECT_NO_DISCOUNT, "2", "3", DISCOUNT_ON, DISCOUNT_OFF);
    }

    @Test
    public void testSmartClubDealAmountOffBestForCustomer50WithGroup3() throws Exception {
        runSmartClubDealTestWithParams("50", IS_LIMIT_OFFER_OFF,"",2,20000, 3,90000,110000,-5000, "2", "3", DISCOUNT_ON, DISCOUNT_OFF);
    }


    @Test
    public void testSmartClubDealAmountOffBestForCustomer50WithGroup4() throws Exception {
        runSmartClubDealTestWithParams("50",IS_LIMIT_OFFER_OFF,"",5,50000, 3,90000,140000,-5000, "2", "3",DISCOUNT_ON, DISCOUNT_OFF);
    }

    @Test
    public void testSmartClubDealAmountOffBestForCustomer50WithGroup5() throws Exception {
        runSmartClubDealTestWithParams("50", IS_LIMIT_OFFER_OFF,"",2,20000, 8,240000,260000,-5000, "2", "3",DISCOUNT_ON, DISCOUNT_ON);
    }


    @Test
    public void testSmartClubDealAmountOffBestForCustomer80WithGroup6() throws Exception {
        runSmartClubDealTestWithParams("80", IS_LIMIT_OFFER_OFF,"",5,50000, 3,90000,140000,-8000, "2", "3",DISCOUNT_ON, DISCOUNT_ON);
    }

    @Test
    public void testSmartClubDealAmountOffBestForCustomer80WithGroup7() throws Exception {
        runSmartClubDealTestWithParams("80",IS_LIMIT_OFFER_OFF,"",2,20000, 8,240000,260000,-8000, "2", "3",DISCOUNT_ON, DISCOUNT_ON);
    }


    @Test
    public void testSmartClubDealAmountOffBestForCustomer80WithGroup8() throws Exception {
        runSmartClubDealTestWithParams("80", IS_LIMIT_OFFER_CHECKED,"4",20,200000, 15,450000,650000,-32000, "2", "3",DISCOUNT_ON, DISCOUNT_ON);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testSmartClubDealAmountOffBestForCustomer80WithGroup9() throws Exception {
        runSmartClubDealTestWithParams("80", IS_LIMIT_OFFER_CHECKED,"4",7,70000, 7,210000,280000,-16000, "2", "3",DISCOUNT_ON, DISCOUNT_ON);
    }

    @Test
    public void testSmartClubDealAmountOffBestForCustomer120WithGroup10() throws Exception {
        runSmartClubDealTestWithParams("120", IS_LIMIT_OFFER_OFF,"",1,120000, 15,450000,570000,-12000, "1", "3",DISCOUNT_ON, DISCOUNT_ON);
    }



    @Test
    public void testSmartClubDealAmountOffBestForCustomer120WithGroup11() throws Exception {
        runSmartClubDealTestWithParams("120", IS_LIMIT_OFFER_OFF,"",9,1080000, 15,450000,1530000,-24000, "4", "3",DISCOUNT_ON, DISCOUNT_ON);
    }

}

