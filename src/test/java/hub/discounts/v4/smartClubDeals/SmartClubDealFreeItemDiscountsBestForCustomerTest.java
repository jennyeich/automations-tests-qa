package hub.discounts.v4.smartClubDeals;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.discounts.v4.GetBenefitsHelper;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by anastasiya on 12/07/2017.
 */

public class SmartClubDealFreeItemDiscountsBestForCustomerTest extends BaseHub1SmartClubDealBestForCustomerTest {


    /////////////////////////////////////////////
    //////////////   T E S T S   ////////////////
    /////////////////////////////////////////////


    @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroupBestForCusotmer1() throws Exception {
      runSmartClubDealTestWithParams(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "",1,100000, 100000,EXPECT_NO_DISCOUNT);
    }


    @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroupBestForCustomer2() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "",1,10000, 3,90000,100000,-10000);
    }


    @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroupBestForCustomer3() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "",2,20000, 2,60000,80000,-20000);
    }

    @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroupBestForCustomer4() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "",5,50000, 3,90000,140000,-30000);
    }



    @Test
    @Category(Hub1Regression.class)
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroupBestForCustomer5() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "",12,12000, 3,90000,102000,-30000,true);

    }

    @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroupBestForCustomer6() throws Exception {
        runSmartClubDealTestWithParams(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "",24,24000, 3,9000,33000,-6000,true);
    }



    private void runSmartClubDealTestWithParams(String isLimit, String limitNum, String isLimitOffer, String limiitOfferNum, int quantity1, int netAmount, int quantity2, int netAmount2, int totalSum, int expected) throws Exception{
        runSmartClubDealTestWithParams( isLimit,  limitNum,  isLimitOffer,  limiitOfferNum,quantity1,netAmount, quantity2,netAmount2,totalSum,expected,false);    }


    private void runSmartClubDealTestWithParams(String isLimit, String limitNum, String isLimitOffer, String limiitOfferNum, int quantity1, int netAmount1, int totalSum, int expected) throws Exception{
        runSmartClubDealTestWithParams( isLimit,  limitNum,  isLimitOffer,  limiitOfferNum,quantity1,netAmount1, totalSum,expected,false);
    }


    private void runSmartClubDealTestWithParams(String isLimit, String limitNum, String isLimitOffer, String limiitOfferNum, int quantity1, int netAmount1, int quantity2, int netAmount2, int totalSum, int expected, boolean createSmartClubDealWithDiscountWith12and1items) throws Exception{
        if(createSmartClubDealWithDiscountWith12and1items) {
            GetBenefitsHelper.createSmartClubDealWithDiscountWith12and1items( isLimit,  limitNum,  isLimitOffer,  limiitOfferNum , timestamp);
        }else{
            GetBenefitsHelper.createSmartClubDealWithDiscountFreeItem( isLimit, limitNum, isLimitOffer,  limiitOfferNum, timestamp);
        }


        performPurchase(quantity1, netAmount1, quantity2, netAmount2, totalSum, expected);
    }

    private void runSmartClubDealTestWithParams(String isLimit, String limitNum, String isLimitOffer, String limiitOfferNum, int quantity1, int netAmount, int totalSum, int expected, boolean createSmartClubDealWithDiscountWith12and1items) throws Exception{
        if(createSmartClubDealWithDiscountWith12and1items) {
            GetBenefitsHelper.createSmartClubDealWithDiscountWith12and1items( isLimit,  limitNum,  isLimitOffer,  limiitOfferNum , timestamp);
        }else{
            GetBenefitsHelper.createSmartClubDealWithDiscountFreeItem( isLimit, limitNum, isLimitOffer,  limiitOfferNum, timestamp);
        }


        performPurchase(quantity1, netAmount, totalSum, expected);
    }
}



