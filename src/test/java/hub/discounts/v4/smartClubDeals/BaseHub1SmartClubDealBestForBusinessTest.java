package hub.discounts.v4.smartClubDeals;

import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.discounts.BaseHub1Discount;
import hub.discounts.v4.GetBenefitsHelper;
import hub.services.member.MembersService;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.IServerResponse;
import server.common.asset.actions.DiscountStrategy;
import server.utils.V4Builder;
import server.v4.GetBenefitsResponse;
import server.v4.common.models.Purchase;
import server.v4.common.models.PurchaseItem;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.*;

/**
 * Created by Goni on 7/18/2017.
 */

public class BaseHub1SmartClubDealBestForBusinessTest extends BaseHub1Discount {

    public Settings settings;
    public String timestamp;
    public V4Builder v4Builder = new V4Builder(apiKey);

    /////////////////////////////////////////////
    //////////////// I  N  I  T  ////////////////
    /////////////////////////////////////////////

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();

        //update the server with best for business
        Map<String, Object> props = new HashMap<>();
        props.put(BaseHub1Discount.DEFAULT_DISCOUNT_STRATEGY, DiscountStrategy.BEST_FOR_BUISNESS.strategy());
        props.put(BaseHub1Discount.DETAILED_REDEEM_RESPONSE, true);
        serverHelper.saveApiClient(props);

        GetBenefitsHelper.setLogger(log4j);
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            //checkClubDealsInActive();
        }

    };

    @After
    public void cleanAssets() throws Exception {
        MembersService.deactivateSmartClubDeals();
    }

    /////////////////////////////////////////////
    ////////// H E L P E R   F U N C S //////////
    /////////////////////////////////////////////

    public void checkResponse(IServerResponse getBenefitsResponse, int expected) {
        //Make sure the response is not Error response
        if (getBenefitsResponse instanceof server.v4.base.ZappServerErrorResponse) {
            Assert.fail(((server.v4.base.ZappServerErrorResponse) getBenefitsResponse).getErrorMessage());
        }

        if(!(((GetBenefitsResponse) getBenefitsResponse).getStatus().equals("ok")))
            Assert.fail(getError(getBenefitsResponse).get(MESSAGE).toString());
        //Make sure the response has status code 200
        Assert.assertEquals("Status code must be 200: ", httpStatusCode200, getBenefitsResponse.getStatusCode());

        if(expected == EXPECT_NO_DISCOUNT){//no discount - returns only status ok
            Assert.assertNull("Total discount should be null - conditions not fulfilled " ,((GetBenefitsResponse) getBenefitsResponse).getTotalDiscountSum());
            log4j.info("Total discount should be empty - no discount");
        }else {
            //Check discount's sum
            Assert.assertEquals("Verify discount's sum is as expected", Integer.valueOf(expected), ((GetBenefitsResponse) getBenefitsResponse).getTotalDiscountSum());
            log4j.info("Total discount sum is as expected : " + ((GetBenefitsResponse) getBenefitsResponse).getTotalDiscountSum());
        }
    }

    public void runSmartClubDealTestWithParams(String isLimit, String limitNum, String isLimitOffer, String limiitOfferNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
        runSmartClubDealTestWithParams( isLimit,  limitNum,  isLimitOffer,  limiitOfferNum,quantity1,price1, quantity2,price2,totalSum,expected,false);
    }

    public void runSmartClubDealTestWithParams(String isLimit, String limitNum, String isLimitOffer, String limiitOfferNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected, boolean createSmartClubDealWithDiscountWith12and1items) throws Exception{
        if(createSmartClubDealWithDiscountWith12and1items) {
            GetBenefitsHelper.createSmartClubDealWithDiscountWith12and1items( isLimit,  limitNum,  isLimitOffer,  limiitOfferNum , timestamp);
        }else{
            GetBenefitsHelper.createSmartClubDealWithDiscountFreeItem( isLimit, limitNum, isLimitOffer,  limiitOfferNum, timestamp);
        }

        performPurchase(quantity1, price1, quantity2, price2, totalSum, expected);

    }
    public void runSmartClubDealTestWithParams(String isLimit, String limitNum, String isLimitOffer, String limiitOfferNum, int quantity1, int netAmount1, int totalSum, int expected) throws Exception{
        runSmartClubDealTestWithParams( isLimit,  limitNum,  isLimitOffer,  limiitOfferNum,quantity1,netAmount1, totalSum,expected,false);
    }

    public void runSmartClubDealTestWithParams(String isLimit, String limitNum, String isLimitOffer, String limiitOfferNum, int quantity1, int price1, int totalSum, int expected, boolean createSmartClubDealWithDiscountWith12and1items) throws Exception{
        if(createSmartClubDealWithDiscountWith12and1items) {
            GetBenefitsHelper.createSmartClubDealWithDiscountWith12and1items( isLimit,  limitNum,  isLimitOffer,  limiitOfferNum , timestamp);
        }else{
            GetBenefitsHelper.createSmartClubDealWithDiscountFreeItem( isLimit, limitNum, isLimitOffer,  limiitOfferNum, timestamp);
        }

        performPurchase(quantity1, price1, totalSum, expected);

    }

    public void performPurchase(int quantity1, int price1, int totalSum, int expected) throws Exception {
        //Create list of items
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem(ITEM_CODE_111,quantity1,price1,"depCode", "depName"));

        //Create purchase
        Purchase purchase = v4Builder.buildPurchase(UUID.randomUUID().toString(),totalSum,items);

        //Send request & get response
        IServerResponse getBenefitsResponse = GetBenefitsHelper.getInstance().getBenefits(apiKey,hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX).getPhoneNumber(), purchase, null, null);
        checkResponse(getBenefitsResponse,expected);
    }

    public void performPurchase(int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception {
        //Create list of items
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem(ITEM_CODE_111,quantity1,price1,"depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem(ITEM_CODE_222,quantity2,price2, "depCode", "depName"));

        //Create purchase
        Purchase purchase = v4Builder.buildPurchase(UUID.randomUUID().toString(),totalSum,items);

        //Send request & get response
        IServerResponse getBenefitsResponse = GetBenefitsHelper.getInstance().getBenefits(apiKey,hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX).getPhoneNumber(), purchase, null, null);
        checkResponse(getBenefitsResponse,expected);
    }

    public void performPurchase(int quantity1, int price1, int quantity2, int price2,int quantity3, int price3, int totalSum, int expected) throws Exception {
        //Create list of items
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("668773", quantity1, price1,"123", "Dep1"));
        items.add(v4Builder.buildPurchaseItem("821172", quantity2, price2, "123", "Dep1"));
        items.add(v4Builder.buildPurchaseItem("325242", quantity3, price3, "123", "Dep1"));

        //Create purchase
        Purchase purchase = v4Builder.buildPurchase(UUID.randomUUID().toString(),totalSum,items);

        //Send request & get response
        IServerResponse getBenefitsResponse = GetBenefitsHelper.getInstance().getBenefits(apiKey,hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX).getPhoneNumber(), purchase, null, null);
        checkResponse(getBenefitsResponse,expected);
    }
}

