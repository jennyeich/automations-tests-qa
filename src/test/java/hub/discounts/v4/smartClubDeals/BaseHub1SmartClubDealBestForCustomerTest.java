package hub.discounts.v4.smartClubDeals;

import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.discounts.BaseHub1Discount;
import hub.discounts.v4.GetBenefitsHelper;
import hub.services.member.MembersService;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.IServerResponse;
import server.common.asset.actions.DiscountStrategy;
import server.utils.V4Builder;
import server.v4.GetBenefitsResponse;
import server.v4.common.models.Purchase;
import server.v4.common.models.PurchaseItem;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.*;

/**
 * Created by Goni on 7/18/2017.
 */

public class BaseHub1SmartClubDealBestForCustomerTest extends BaseHub1Discount {


    public Settings settings;
    public String timestamp;
    public V4Builder v4Builder = new V4Builder(apiKey);

    /////////////////////////////////////////////
    //////////////// I  N  I  T  ////////////////
    /////////////////////////////////////////////

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
        //update the server with best for custome
        Map<String, Object> props = new HashMap<>();
        props.put(BaseHub1Discount.DEFAULT_DISCOUNT_STRATEGY, DiscountStrategy.BEST_FOR_CUSTOMER.strategy());
        props.put(BaseHub1Discount.DETAILED_REDEEM_RESPONSE, true);
        serverHelper.saveApiClient(props);

        GetBenefitsHelper.setLogger(log4j);
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            //checkClubDealsInActive();
        }
    };

    @After
    public void cleanAssets() throws Exception {
        MembersService.deactivateSmartClubDeals();
    }

    /////////////////////////////////////////////
    ////////// H E L P E R   F U N C S //////////
    /////////////////////////////////////////////

    public void runSmartClubDealTestWithParams(String amountOffNumber,  String isLimitOffer, String limitOfferNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected,
                                               String basketQuantity1, String basketQuantity2, String onOff1, String onOff2) throws Exception{
        GetBenefitsHelper.createSmartClubDealWithDiscount(amountOffNumber, isLimitOffer, limitOfferNum, timestamp, basketQuantity1, basketQuantity2, onOff1, onOff2);
        performPurchase(quantity1, price1, quantity2, price2, totalSum, expected);


    }

    public void performPurchase(int quantity1, int netAmount, int totalSum, int expected) throws Exception {
        //Create list of items
        List<PurchaseItem> items = new ArrayList<PurchaseItem>();
        items.add(v4Builder.buildPurchaseItem(ITEM_CODE_111,quantity1,netAmount,"depCode", "depName"));

        //Create purchase
        Purchase purchase = v4Builder.buildPurchase(UUID.randomUUID().toString(),totalSum,items);

        //Send request & get response
        IServerResponse getBenefitsResponse = GetBenefitsHelper.getInstance().getBenefits(apiKey,hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX).getPhoneNumber(), purchase, null, null);
        checkResponse(getBenefitsResponse,expected);
    }

    public void performPurchase(int quantity1, int netAmount1, int quantity2, int netAmount2, int totalSum, int expected) throws Exception {
        //Create list of items
        List<PurchaseItem> items = new ArrayList<PurchaseItem>();
        items.add(v4Builder.buildPurchaseItem(ITEM_CODE_111,quantity1,netAmount1,"depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem(ITEM_CODE_222,quantity2,netAmount2,"depCode", "depName"));

        //Create purchase
        Purchase purchase = v4Builder.buildPurchase(UUID.randomUUID().toString(),totalSum,items);

        //Send request & get response
        IServerResponse getBenefitsResponse = GetBenefitsHelper.getInstance().getBenefits(apiKey,hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX).getPhoneNumber(), purchase, null, null);
        checkResponse(getBenefitsResponse,expected);
    }


    public void checkResponse(IServerResponse getBenefitsResponse, int expected) {
        //Make sure the response is not Error response
        if (getBenefitsResponse instanceof server.v4.base.ZappServerErrorResponse) {
            Assert.fail(((server.v4.base.ZappServerErrorResponse) getBenefitsResponse).getErrorMessage());
        }

        //Make sure the response has status code 200
        Assert.assertEquals("Status code must be 200: ", httpStatusCode200, getBenefitsResponse.getStatusCode());

        if(expected == EXPECT_NO_DISCOUNT){//no discount - returns only status ok
            Assert.assertNull("Total discount should be empty - conditions not fulfilled - no discount" ,((GetBenefitsResponse) getBenefitsResponse).getTotalDiscountSum());
            log4j.info("Total discount should be empty - conditions not fulfilled - no discount");
        }else {

            //Check discount's sum
            Assert.assertEquals("Verify discount's sum is as expected", Integer.valueOf(expected), ((GetBenefitsResponse) getBenefitsResponse).getTotalDiscountSum());
            log4j.info("Total discount sum is as expected : " + ((GetBenefitsResponse) getBenefitsResponse).getTotalDiscountSum());
        }
    }

}

