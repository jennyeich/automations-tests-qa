package hub.discounts.v4.smartGifts;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;

/**
 * Created by Jenny on 18/5/2017.
 */

public class SmartAssetDiscountsDealCodeBestForBusinessTest extends BaseHub1SmartAssetBestForBusinessTest {


    @Test
    public void testSmartGiftDiscountaddDealCodesBestforBusiness1() throws Exception {
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        runSmartAssetTesWithAddItemCode(dealCodesToCreate, 1, 3000, 3, 12000, 15000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscountaddDealCodesBestforBusiness2() throws Exception {
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        runSmartAssetTesWithAddItemCode(dealCodesToCreate, 1, 3000, 3, 12000, 3, 12000, 27000, EXPECT_NO_DISCOUNT);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testSmartGiftDiscountaddDealCodesBestforBusiness3() throws Exception {
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        runSmartAssetTesWithDealCodesWithCondition(dealCodesToCreate,1, 3000, 3, 12000, 15000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscountaddDealCodesBestforBusiness4() throws Exception {
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        runSmartAssetTesWithDealCodesWithCondition(dealCodesToCreate,1, 3000, 3, 12000, 3, 15000, 30000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscountaddDealCodesBestforBusiness5() throws Exception {
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        runSmartAssetTestWithAddItemCodeWithConditionWithBasket(dealCodesToCreate, 1, 3000, 3, 12000, 3, 12000, 27000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscountAddDealCodesBestForBusiness6() throws Exception {
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        runSmartAssetTesWithDealCodesWithCondition3Items(dealCodesToCreate, 1, 3000, 3, 12000, 3, 12000, 27000, EXPECT_NO_DISCOUNT);
    }

}



