package hub.discounts.v4.smartGifts;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.smarts.SmartPunchCard;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;

/**
 * Created by Goni on 1/8/2018.
 */

public class SmartPunchCardDiscountsBestForBusinessTest  extends BaseHub1SmartAssetBestForBusinessTest {

    public static SmartPunchCard smartPunchCard;
    private final String NUM_OF_PUNCHES = "5";

    @Test
    public void testSmartPunchCardDiscountaddDealCodes1() throws Exception {
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        runSmartPunchCardWithDealCodesWithCondition(dealCodesToCreate,1, 3000, 3, 12000, 15000, EXPECT_NO_DISCOUNT,NUM_OF_PUNCHES);
    }


    @Test
    public void testSmartPunchCardDiscountaddItemCodes2() throws Exception {
        ArrayList<String> codesToCreate = Lists.newArrayList("222334","553344");
        runSmartPunchCardWithAddItemCodeWithConditionWithBasket(codesToCreate, 1, 3000, 3, 12000, 3, 12000, 27000, EXPECT_NO_DISCOUNT,NUM_OF_PUNCHES);
    }

    @Test
    public void testSmartPunchCardDiscount25PercentWithGroup1LimitOffer3() throws Exception {
        runSmartPunchCardWithOfferLimit("25", IS_LIMIT_OFFER_CHECKED, "1", 5, 15000, 3, 12000, 27000, -4500,NUM_OF_PUNCHES);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testSmartPunchCardDiscountAmountOff50WithGroupWith2DifferentItemsPrice0_4() throws Exception {
        runSmartPunchCardWithDiscountOneBusketsOff("50",IS_LIMIT_OFF, "", 5, 0, 8, 32000, 32000,-10000,NUM_OF_PUNCHES);
    }

    @Test
    public void testSmartPunchCardDiscountAmountOff50WithGroupWith2DifferentItemsItemPrice0BusketOff5() throws Exception {
        runSmartPunchCardWithDiscountOneBusketsOff("50", IS_LIMIT_OFF, "", 2, 0, 3, 12000, 12000, -5000,NUM_OF_PUNCHES);
    }

}

