package hub.discounts.v4.smartGifts;

import hub.base.categories.hub1Categories.Hub1Regression;
import org.junit.Test;
import org.junit.experimental.categories.Category;

/**
 * Created by Jenny on 18/5/2017.
 */

public class SmartAssetDiscountsBestForBusinessTest extends BaseHub1SmartAssetBestForBusinessTest {


    @Test
    public void testSmartGiftDiscount33PercentWithGroupBestforBusiness1() throws Exception {
        runSmartAssetTestWithParams("33", IS_LIMIT_OFF, "", 1, 3000, 1, 4000, 7000, -990);
    }

    @Test
    public void testSmartGiftDiscount20PercentNoGroupBestforBusiness2() throws Exception {
        runSmartAssetTestWithParamsNoGroup("20", IS_LIMIT_OFF, "", 1, 3000, 1, 4000, 7000, -1400);
    }

    @Test
    public void testSmartGiftDiscount0PercentNoGroupBestforBusiness3() throws Exception {
        runSmartAssetTestWithParamsNoGroup("0", IS_LIMIT_OFF, "", 1, 3000, 1, 4000, 7000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscount100PercentNoGroupBestforBusiness4() throws Exception {
        runSmartAssetTestWithParamsNoGroup("100", IS_LIMIT_OFF, "", 1, 3000, 1, 4000, 7000, -7000);
    }

    @Test
    public void testSmartGiftDiscount50PercentWith200LimitBestforBusiness5() throws Exception {
        runSmartAssetTestWithParamsNoGroup("50", IS_LIMIT_CHECKED, "200", 1, 15000, 1, 25000, 40000, -20000);
    }

    @Test
    public void testSmartGiftDiscount50PercentNoGroupWith100LimittBestforBusiness6() throws Exception {
        runSmartAssetTestWithParamsNoGroup("50",IS_LIMIT_CHECKED, "100", 1, 15000, 1, 25000, 40000, -10000);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testSmartGiftDiscount50PercentNoGroupWith300LimitBestforBusiness7() throws Exception {
        runSmartAssetTestWithParamsNoGroup("50", IS_LIMIT_CHECKED, "100", 1, 15000, 1, 25000, 40000, -10000);
    }

    @Test
    public void testSmartGiftDiscount0PercentNoGroupWith0LimitBestforBusiness8() throws Exception {
        runSmartAssetTestWithParamsNoGroup("0", IS_LIMIT_CHECKED, "0", 1, 15000, 1, 25000, 40000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscount0PercentNoGroupWith100LimitBestforBusiness9() throws Exception {
        runSmartAssetTestWithParamsNoGroup("0",IS_LIMIT_CHECKED, "100", 1, 15000, 1, 25000, 40000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscount100PercentNoGroupWith100LimitBestforBusiness10() throws Exception {
        runSmartAssetTestWithParamsNoGroup("100", IS_LIMIT_CHECKED, "100", 1, 15000, 1, 25000, 40000, -10000);
    }

    @Test
    public void testSmartGiftDiscount100PercentNoGroupWith500LimitBestforBusiness11() throws Exception {
        runSmartAssetTestWithParamsNoGroup("100", IS_LIMIT_CHECKED, "500", 1, 15000, 1, 25000, 40000, -40000);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupNoLimitOfferBestforBusiness12() throws Exception {
        runSmartAssetTestWithOfferLimit("25", IS_LIMIT_OFFER_OFF, "", 1, 3000, 3, 12000, 15000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupNoLimitOfferBestforBusiness13() throws Exception {
        runSmartAssetTestWithOfferLimit("25", IS_LIMIT_OFFER_OFF, "", 2, 6000, 2, 8000, 14000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupNoLimitOfferBestforBusiness14() throws Exception {
        runSmartAssetTestWithOfferLimit("25", IS_LIMIT_OFFER_OFF, "", 2, 6000, 3, 12000, 18000, -4500);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupNoLimitOfferBestforBusiness15() throws Exception {
        runSmartAssetTestWithOfferLimit("25", IS_LIMIT_OFFER_OFF, "", 5, 15000, 3, 12000, 27000, -4500);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupNoLimitOfferBestforBusiness16() throws Exception {
        runSmartAssetTestWithOfferLimit("25", IS_LIMIT_OFFER_OFF, "", 5, 15000, 8, 32000, 47000, -9000);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroup1LimitOfferBestforBusiness17() throws Exception {
        runSmartAssetTestWithOfferLimit("25", IS_LIMIT_OFFER_CHECKED, "1", 5, 15000, 3, 12000, 27000, -4500);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroup1LimitOfferBestforBusiness18() throws Exception {
        runSmartAssetTestWithOfferLimit("25", IS_LIMIT_OFFER_CHECKED, "1", 2, 6000, 8, 32000, 38000, -4500);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroup1LimitOfferBestforBusiness19() throws Exception {
        runSmartAssetTestWithOfferLimit("25", IS_LIMIT_OFFER_CHECKED, "1", 2, 21000, 8, 40000, 61000, -9000);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroup4LimitOfferBestForBusiness20() throws Exception {
        runSmartAssetTestWithOfferLimit("25", IS_LIMIT_OFFER_CHECKED, "4", 20, 60000, 15, 60000, 120000, -18000);
    }
}



