package hub.discounts.v4.smartGifts;

import com.google.common.collect.Lists;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.ISmartAssetCounter;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.SmartPunchCard;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.DiscountOnItem;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.DiscountTypeOperation;
import hub.discounts.BaseHub1Discount;
import hub.discounts.DiscountsConstants;
import hub.discounts.v4.GetBenefitsHelper;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.DiscountItems_ForEach;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.GroupField;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.GroupOperators;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.services.member.MembersService;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.IServerResponse;
import server.common.asset.actions.DiscountStrategy;
import server.utils.RedeemErrorCodesMap;
import server.utils.V4Builder;
import server.v4.GetBenefitsResponse;
import server.v4.common.models.*;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class BaseHub1SmartAssetBestForBusinessTest extends BaseHub1Discount {

    public Settings settings;
    public String timestamp;
    public V4Builder v4Builder = new V4Builder(apiKey);

    /////////////////////////////////////////////
    //////////////// I  N  I  T  ////////////////
    /////////////////////////////////////////////

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();

        //update the server with best for business
        serverHelper.saveApiClient(DiscountsConstants.DEFAULT_DISCOUNT_STRATEGY, DiscountStrategy.BEST_FOR_BUISNESS.strategy());
        serverHelper.saveApiClient(DiscountsConstants.DETAILED_REDEEM_RESPONSE, true);
        GetBenefitsHelper.setLogger(log4j);
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
        }
    };

    /////////////////////////////////////////////
    ////////// H E L P E R   F U N C S //////////
    /////////////////////////////////////////////


    public void runSmartAssetTestWithParams(String percentOffNumber, String isLimit, String limitNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
        DiscountOnItem item1 = GetBenefitsHelper.buildDiscountItem(timestamp, DISCOUNT_ON, ITEM_CODE_111, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "1");
        DiscountOnItem item2 = GetBenefitsHelper.buildDiscountItem(String.valueOf(System.currentTimeMillis()), DiscountsConstants.DISCOUNT_OFF, ITEM_CODE_222, DiscountItems_ForEach.quantity, GroupField.ItemCode, GroupOperators.Equals, "1");
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithAddDiscount(percentOffNumber, isLimit, limitNum, timestamp, DiscountsConstants.DISCOUNTS_CHECK_BOX_ON, "", "", Lists.newArrayList(item1, item2), DiscountTypeOperation.PercentOff);
        performPurchase(quantity1, price1, quantity2, price2, totalSum, expected, smartGift);

    }

    public void runSmartAssetTestWithParamsNoGroup(String percentOffNumber, String isLimit, String limitNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithAddDiscount(percentOffNumber, isLimit, limitNum, timestamp,DiscountsConstants.DISCOUNTS_CHECK_BOX_OFF, "", "", null, DiscountTypeOperation.PercentOff);
        performPurchase(quantity1, price1, quantity2, price2, totalSum, expected, smartGift);
    }

    public void runSmartAssetTestWithOfferLimit(String percentOffNumber, String isOfferLimit, String limitOfferNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithDiscountsWithOfferLimit(percentOffNumber, isOfferLimit, limitOfferNum, timestamp);
        performPurchase(quantity1, price1, quantity2, price2, totalSum, expected, smartGift);
    }
    public void runSmartPunchCardWithOfferLimit(String percentOffNumber, String isOfferLimit, String limitOfferNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected,String numPunches) throws Exception{
        SmartPunchCard smartPunchCard = GetBenefitsHelper.createSmartPunchCardWithDiscountsWithOfferLimit(percentOffNumber, isOfferLimit, limitOfferNum, timestamp,numPunches);
        performPurchase(quantity1, price1, quantity2, price2, totalSum, expected, smartPunchCard);
    }


    public void runSmartAssetTestWithOfferLimit3itemsGroup(String percentOffNumber, String isLimit, String limitNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithDiscountsWithOfferLimit3itemsGroup(percentOffNumber, isLimit, limitNum, timestamp);
        performPurchase(quantity1, price1, quantity2, price2, totalSum, expected, smartGift);
    }

    public void runSmartAssetTestWithOfferLimit3itemsGroupWithGivenCodes(String percentOffNumber, String isLimit, String limitNum, int quantity1, int price1,String itemcode1, int quantity2, int price2,String itemcode2, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithDiscountsWithOfferLimit3itemsGroup(percentOffNumber, isLimit, limitNum, timestamp);
        performPurchaseWithItemCodes(quantity1, price1,itemcode1, quantity2, price2,itemcode2, totalSum, expected, smartGift);
    }

    public void runSmartAssetTestWithOfferLimit3itemsGroup(String percentOffNumber,  String isOfferLimit, String limitOfferNum, int quantity1, int price1, int quantity2, int price2, int quantity3, int price3, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithDiscountsWithOfferLimit3itemsGroup(percentOffNumber, isOfferLimit, limitOfferNum, timestamp);
        //Create list of items
        List<PurchaseItem> items = new ArrayList<PurchaseItem>();
        items.add(v4Builder.buildPurchaseItem(ITEM_CODE_333,quantity1,price1,"123", "Dep1"));
        items.add(v4Builder.buildPurchaseItem("555",quantity2,price2, "123", "Dep1"));
        items.add(v4Builder.buildPurchaseItem("666",quantity3,price3, "123", "Dep1"));

        performPurchase(items, totalSum, expected, smartGift);
    }

    public void runSmartAssetTestWithOfferLimit2discountsOneIsOff(String percentOffNumber, String isLimit, String limitNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithDiscountsWithOfferLimit2discountsOneIsOff(percentOffNumber, isLimit, limitNum, timestamp);
        performPurchase(quantity1, price1, quantity2, price2, totalSum, expected, smartGift);
    }

    public void runSmartAssetTestWithOfferLimit3discountsOneIsOff(String percentOffNumber, String isLimit, String limitNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithDiscountsWithOfferLimit3discountsOneIsOff(percentOffNumber, isLimit, limitNum, timestamp);
        performPurchase(quantity1, price1, quantity2, price2, totalSum, expected, smartGift);
    }


    public void runSmartAssetTestWithDiscountsWithOfferLimit3differentdiscountsOneIsOffOld(String percentOffNumber, String isLimit, String limitNum, int quantity1, int price1, int quantity2, int price2, int quantity3, int price3, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithDiscountsWithOfferLimit3differentdiscountsOneIsOffOld(percentOffNumber, isLimit, limitNum, timestamp);
        performPurchase(quantity1, price1, quantity2, price2, quantity3, price3, totalSum, expected, smartGift);
    }

    public void runSmartAssetTestWithDiscountBothBusketsEnabled(String amountOffNumber, String isLimit, String limitNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithDiscountBothBusketsEnabled(amountOffNumber, isLimit, limitNum, timestamp);
        performPurchase(quantity1, price1, quantity2, price2, totalSum, expected, smartGift);
    }

    public void runSmartAssetTestWithDiscountOneBusketsOff(String amountOffNumber, String isLimit, String limitNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithDiscountOneBusketsOff(amountOffNumber, isLimit, limitNum, timestamp);
        performPurchase(quantity1, price1, quantity2, price2, totalSum, expected, smartGift);
    }
    public void runSmartAssetTesWithDealCodes(ArrayList<String> dealCodes,int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithAddDealCode(dealCodes, timestamp, false);
        performPurchaseWithDealCodes(quantity1, price1, quantity2, price2, totalSum, expected, smartGift, dealCodes);
    }

    public void runSmartPunchCardWithDiscountOneBusketsOff(String amountOffNumber, String isLimit, String limitNum, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected,String numPunches) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithDiscountOneBusketsOff(amountOffNumber, isLimit, limitNum, timestamp);
        performPurchase(quantity1, price1, quantity2, price2, totalSum, expected, smartGift);
    }

    public void runSmartAssetTesWithDealCodesWithCondition(ArrayList<String> dealCodes, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithAddDealCode(dealCodes, timestamp, true);
        performPurchaseWithDealCodes(quantity1, price1, quantity2, price2, totalSum, expected, smartGift, dealCodes);
    }

    public void runSmartPunchCardWithDealCodesWithCondition(ArrayList<String> dealCodes, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected,String numOfPunches) throws Exception{
        SmartPunchCard smartPunchCard = GetBenefitsHelper.createSmartPunchCardWithAddDealCode(dealCodes, timestamp, true,numOfPunches);
        performPurchaseWithDealCodes(quantity1, price1, quantity2, price2, totalSum, expected, smartPunchCard, dealCodes);
    }

    public void runSmartAssetTesWithDealCodesWithCondition(ArrayList<String> dealCodes, int quantity1, int price1, int quantity2, int price2, int quantity3, int price3, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithAddDealCode(dealCodes, timestamp, true);
        performPurchaseWithDealCodes3Items(quantity1, price1, quantity2, price2, quantity3, price3, totalSum, expected, smartGift, dealCodes);
    }

    public void runSmartAssetTesWithDealCodesWithCondition3Items(ArrayList<String> dealCodes,  int quantity1, int price1, int quantity2, int price2, int quantity3, int price3, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithAddItemCodeWithConditionWithBasketWithRelatedDealCodes(dealCodes, timestamp);
        performPurchaseWithDealCodes3Items(quantity1, price1, quantity2, price2, quantity3, price3, totalSum, expected, smartGift, dealCodes);
    }

    public void runSmartAssetTesWithAddItemCode(ArrayList<String> dealCodes, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithAddItemCode(dealCodes, timestamp, false);
        performPurchaseWithDealCodes(quantity1, price1, quantity2, price2, totalSum, expected, smartGift, dealCodes);
    }

    public void runSmartAssetTesWithAddItemCode(ArrayList<String> dealCodes, int quantity1, int price1, int quantity2, int price2, int quantity3, int price3, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithAddItemCode(dealCodes, timestamp, false);
        performPurchaseWithDealCodes3Items(quantity1, price1, quantity2, price2, quantity3, price3, totalSum, expected, smartGift, dealCodes);
    }

    public void runSmartAssetTesWithAddItemCode3Items(ArrayList<String> dealCodes, int quantity1, int price1, int quantity2, int price2, int quantity3, int price3, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithAddItemCode(dealCodes, timestamp, false);
        performPurchaseWithDealCodes3Items(quantity1, price1, quantity2, price2, quantity3, price3, totalSum, expected, smartGift, dealCodes);
    }

    public void runSmartAssetTesWithAddItemCodeWithCondition(ArrayList<String> dealCodes, int quantity1, int price1, int quantity2, int price2, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithAddItemCode(dealCodes, timestamp, true);
        performPurchaseWithDealCodes(quantity1, price1, quantity2, price2, totalSum, expected, smartGift, dealCodes);
    }

    public void runSmartAssetTesWithAddItemCodeWithCondition3Items(ArrayList<String> dealCodes, int quantity1, int price1, int quantity2, int price2, int quantity3, int price3, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithAddItemCode(dealCodes, timestamp, true);
        performPurchaseWithDealCodes3Items(quantity1, price1, quantity2, price2, quantity3, price3, totalSum, expected, smartGift, dealCodes);
    }

    public void runSmartAssetTestWithAddItemCodeWithConditionWithBasket(ArrayList<String> dealCodes, int quantity1, int price1, int quantity2, int price2, int quantity3, int price3, int totalSum, int expected) throws Exception{
        SmartGift smartGift = GetBenefitsHelper.createSmartGiftWithAddDealCodeWithConditionWithBasket(dealCodes, timestamp);
        performPurchaseWithDealCodes3Items(quantity1, price1, quantity2, price2, quantity3, price3, totalSum, expected, smartGift, dealCodes);
    }

    public void runSmartPunchCardWithAddItemCodeWithConditionWithBasket(ArrayList<String> dealCodes, int quantity1, int price1, int quantity2, int price2, int quantity3, int price3, int totalSum, int expected,String numOfPunches) throws Exception{
        SmartPunchCard smartPunchCard = GetBenefitsHelper.createSmartPunchCardWithAddDealCodeWithConditionWithBasket(dealCodes, timestamp,numOfPunches);
        performPurchaseWithDealCodes3Items(quantity1, price1, quantity2, price2, quantity3, price3, totalSum, expected, smartPunchCard, dealCodes);
    }

    public GetBenefitsResponse performPurchase(int quantity1, int price1, int quantity2, int price2, int totalSum, int expected, ISmartAssetCounter smartAsset) throws Exception {
        //Create list of items
        List<PurchaseItem> items = new ArrayList<PurchaseItem>();
        items.add(v4Builder.buildPurchaseItem(ITEM_CODE_111,quantity1,price1,"depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem(ITEM_CODE_222,quantity2,price2, "depCode", "depName"));

        return performPurchase(items, totalSum, expected, smartAsset);
    }

    public GetBenefitsResponse performPurchaseWithItemCodes(int quantity1, int price1,String itemcode1, int quantity2, int price2,String itemcode2, int totalSum, int expected, SmartGift smartGift) throws Exception {
        //Create list of items
        List<PurchaseItem> items = new ArrayList<PurchaseItem>();
        items.add(v4Builder.buildPurchaseItem(itemcode1,quantity1,price1,"depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem(itemcode2,quantity2,price2, "depCode", "depName"));

        return performPurchase(items, totalSum, expected, smartGift);
    }

    public GetBenefitsResponse performPurchase(int quantity1, int price1, int quantity2, int price2, int quantity3, int price3, int totalSum, int expected, ISmartAssetCounter smartAsset) throws Exception {
        //Create list of items
        List<PurchaseItem> items = new ArrayList<PurchaseItem>();
        items.add(v4Builder.buildPurchaseItem(ITEM_CODE_111,quantity1,price1,"depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem(ITEM_CODE_222,quantity2,price2, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem(ITEM_CODE_333,quantity3,price3, "depCode", "depName"));

        return performPurchase(items, totalSum, expected, smartAsset);
    }

    public GetBenefitsResponse performPurchase(List<PurchaseItem> purchaseItems, int totalSum, int expected, ISmartAssetCounter smartAsset) throws Exception {
        //Create purchase
        Purchase purchase = v4Builder.buildPurchase(UUID.randomUUID().toString(),totalSum, purchaseItems);

        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        if(smartAsset instanceof SmartGift) {
            performAnActionSendAsset(smartAsset.getTitle());
        }else if(smartAsset instanceof SmartPunchCard){
            //Send punch to member
            performAnActionSendAsset(smartAsset.getTitle());
            //punch the punchcard
            performAnActionPunchAPunchCard(smartAsset.getTitle(),((SmartPunchCard) smartAsset).getNumOfPunches());
        }
        //Get gift's asset key
        String assetKey = "";

        //wait for cache to be updated
        Thread.sleep(DiscountsConstants.WAIT_FOR_ASSETS_CACHE_UPDATE_MILLSEC);

        assetKey = serverHelper.getMemberAssetKey(smartAsset.getTitle(), member.getPhoneNumber());

        RedeemAssetOfSubmitPurchase redeemAsset = new RedeemAssetOfSubmitPurchase();
        redeemAsset.setKey(assetKey);

        //Send request & get response
        RedeemAsset redeemAsset1 = new RedeemAsset();
        redeemAsset1.setKey(assetKey);
        ArrayList<RedeemAsset> redeemAssetList = Lists.newArrayList(redeemAsset1);
        IServerResponse getBenefitsResponse = GetBenefitsHelper.getInstance().getBenefits(apiKey,member.getPhoneNumber(), purchase, redeemAssetList, null);
        checkResponse(getBenefitsResponse,expected);
        return (GetBenefitsResponse)getBenefitsResponse;
    }

    public void performPurchaseWithDealCodes(int quantity1, int price1, int quantity2, int price2, int totalSum, int expected, ISmartAssetCounter smartAsset, ArrayList<String> expectedDealCodes) throws Exception {
        GetBenefitsResponse purchaseResponse = performPurchase(quantity1, price1, quantity2, price2, totalSum, expected, smartAsset);
        checkDealCodes(purchaseResponse, expectedDealCodes);
    }
    public void performPurchaseWithDealCodes3Items(int quantity1, int price1, int quantity2, int price2, int quantity3, int price3, int totalSum, int expected, ISmartAssetCounter smartAsset, ArrayList<String> expectedDealCodes) throws Exception {
        GetBenefitsResponse purchaseResponse = performPurchase(quantity1, price1, quantity2, price2, quantity3, price3, totalSum, expected, smartAsset);
        checkDealCodes(purchaseResponse, expectedDealCodes);
    }

    private void checkDealCodes(GetBenefitsResponse purchaseResponse, ArrayList<String> expectedDealCodes) {
        Assert.assertNotNull("RedeenAssets should not be null", purchaseResponse.getRedeemAssets());
        Assert.assertEquals("RedeenAssets size should be 1", 1, purchaseResponse.getRedeemAssets().size());
        Assert.assertNotNull("RedeenAssets content should not be null", purchaseResponse.getRedeemAssets().get(0));
        Assert.assertNotNull("RedeenAssets benefits should not be null", purchaseResponse.getRedeemAssets().get(0).getBenefits());
        for (int i=0; i<purchaseResponse.getRedeemAssets().get(0).getBenefits().size();i++)
            Assert.assertTrue("Not expected deal code", expectedDealCodes.contains(purchaseResponse.getRedeemAssets().get(0).getBenefits().get(i).getCode()));
        log4j.info("all deal codes are as expected");
    }

    public void checkResponse(IServerResponse getBenefitsResponse, int expected) {

        //Make sure the response is not Error response
        if (getBenefitsResponse instanceof server.v4.base.ZappServerErrorResponse) {
            Assert.fail(((server.v4.base.ZappServerErrorResponse) getBenefitsResponse).getErrorMessage());
        }
        if (!(((GetBenefitsResponse) getBenefitsResponse).getStatus().equals("ok")))
            Assert.fail(getError(getBenefitsResponse).get(MESSAGE).toString());

        //Make sure the response has status code 200
        Assert.assertEquals("Status code must be 200: ", httpStatusCode200, getBenefitsResponse.getStatusCode());

        if (expected == EXPECT_NO_DISCOUNT) {
            //expect an error
            if (((GetBenefitsResponse) getBenefitsResponse).getStatus().equals("ok")) {
                GetBenefitsResponse response = (GetBenefitsResponse) getBenefitsResponse;
                ReturnedRedeemAsset redeemAsset = response.getRedeemAssets().get(0);
                NonRedeemableCause nonRedeemableCause = redeemAsset.getNonRedeemableCause();
                if(nonRedeemableCause != null) {
                    Assert.assertEquals("getBenefits should fail ", RedeemErrorCodesMap.getRedeemErrorCodesMap().get(RedeemErrorCodesMap.VIOLATION_OF_ASSET_NO_BENEFITS), nonRedeemableCause.getMessage());
                    Assert.assertEquals("getBenefits should fail ", RedeemErrorCodesMap.VIOLATION_OF_ASSET_NO_BENEFITS, nonRedeemableCause.getCode());
                }else{
                    //Make sure the response has status code 200
                    Assert.assertNull("Total discount should be null - this is dealCode/itemCode or no discount" ,((GetBenefitsResponse) getBenefitsResponse).getTotalDiscountSum());
                }
            }
        } else {

            //Check discount's sum
            Assert.assertEquals("Verify discount's sum is as expected", Integer.valueOf(expected), ((GetBenefitsResponse) getBenefitsResponse).getTotalDiscountSum());
            log4j.info("Total discount sum is as expected : " + ((GetBenefitsResponse) getBenefitsResponse).getTotalDiscountSum());

        }
    }
    public void performAnActionSendGift(SmartGift smartGift) throws Exception {

        Thread.sleep(4000);
        boolean isGiftExist = MembersService.performSmartActionSendAAsset(smartGift.getTitle());
        log4j.info("The gift was send to the member");
        Assert.assertTrue(isGiftExist);

    }

}

