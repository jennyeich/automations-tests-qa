package hub.discounts.v4.smartGifts;

import hub.base.categories.hub1Categories.Hub1Regression;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class SmartAssetDiscountsBestForBusiness2Test extends BaseHub1SmartAssetBestForBusinessTest {




    @Test
    public void testSmartGiftDiscount25PercentWithGroup4LimitOfferBestForBusiness21() throws Exception {
        runSmartAssetTestWithOfferLimit("25", IS_LIMIT_OFFER_CHECKED, "4", 7, 21000, 7, 28000, 49000, -9000);
    }

    @Test
    public void testSmartGiftDiscount50PercentWithGroupWith3ItemsNoLimitOfferBestForBusiness22() throws Exception {
        runSmartAssetTestWithOfferLimit3itemsGroup("50", IS_LIMIT_OFFER_OFF, "", 1, 2500, 3, 10500, 13000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscount50PercentWithGroupWith3ItemsNoLimitOfferBestForBusiness23() throws Exception {
        runSmartAssetTestWithOfferLimit3itemsGroup("50",IS_LIMIT_OFFER_OFF, "", 1, 2500, 3, 10500, 13000, EXPECT_NO_DISCOUNT);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testSmartGiftDiscount50PercentWithGroupWith3ItemsNoLimitOfferBestForBusiness24() throws Exception {
        runSmartAssetTestWithOfferLimit3itemsGroup("50", IS_LIMIT_OFFER_OFF, "", 1, 5000, 3, 2500, 7500, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscount50PercentWithGroupWith3ItemsNoLimitOfferBestForBusiness25() throws Exception {
        runSmartAssetTestWithOfferLimit3itemsGroup("50", IS_LIMIT_OFFER_OFF, "", 2, 15000, 2, 2500, 1, 3000, 20500, -10250);
    }

    @Test
    public void testSmartGiftDiscount50PercentWithGroupWith3ItemsNoLimitOfferBestForBusiness26() throws Exception {
        runSmartAssetTestWithOfferLimit3itemsGroup("50", IS_LIMIT_OFFER_OFF, "", 1, 3500, 1, 7500, 2, 2500, 13500, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscount50PercentWithGroupWith3ItemsNoLimitOfferBestForBusiness27() throws Exception {
        runSmartAssetTestWithOfferLimit3itemsGroupWithGivenCodes("50", IS_LIMIT_OFFER_OFF, "", 5, 12500,ITEM_CODE_111, 6, 18000,"666", 30500, -14000);
    }

    @Test
    public void testSmartGiftDiscount50PercentWithGroupWith3ItemsNoLimitOfferBestForBusiness28() throws Exception {
        runSmartAssetTestWithOfferLimit3itemsGroupWithGivenCodes("50", IS_LIMIT_OFFER_OFF, "", 2, 5000,ITEM_CODE_111, 8, 10000,"555", 15000, -4375);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupWith3ItemsWith4LimitOfferBestForBusiness29() throws Exception {
        runSmartAssetTestWithOfferLimit2discountsOneIsOff("25",IS_LIMIT_OFFER_CHECKED, "4", 20, 24000, 15, 25500, 49500, -2400);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupWith3ItemsWith4LimitOfferBestForBusiness30() throws Exception {
        runSmartAssetTestWithOfferLimit2discountsOneIsOff("25",IS_LIMIT_OFFER_CHECKED, "4", 7, 8400, 7, 11900, 20300, -1200);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupWith2Items3discountsNoLimitOfferBestForBusiness31() throws Exception {
        runSmartAssetTestWithOfferLimit3discountsOneIsOff("25", IS_LIMIT_OFFER_OFF, "", 1, 1200, 3, 5100, 6300, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupWith2ItemsNoLimitOfferBestForBusiness32() throws Exception {
        runSmartAssetTestWithOfferLimit3discountsOneIsOff("25", IS_LIMIT_OFFER_OFF, "", 2, 2400, 2, 3400, 5800, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupWith2ItemsNoLimitOfferBestForBusiness33() throws Exception {
        runSmartAssetTestWithOfferLimit3discountsOneIsOff("25",IS_LIMIT_OFFER_OFF, "", 2, 2400, 3, 5100, 7500, -1875);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupWith2ItemsNoLimitOfferBestforBusiness34() throws Exception {
        runSmartAssetTestWithOfferLimit3discountsOneIsOff("25", IS_LIMIT_OFFER_OFF, "", 5, 6000, 3, 5100, 11100, -1875);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupWith2ItemsNoLimitOfferBestForBusiness35() throws Exception {
        runSmartAssetTestWithOfferLimit3discountsOneIsOff("25", IS_LIMIT_OFFER_OFF, "", 2, 2400, 8, 13600, 16000, -1875);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupWith3DifferentItemsItemsNoLimitOfferBestForBusiness36() throws Exception {
        runSmartAssetTestWithDiscountsWithOfferLimit3differentdiscountsOneIsOffOld("25",IS_LIMIT_OFFER_OFF, "", 1, 1200, 3, 5100, 2, 2666, 8966, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupWith3DifferentItemsItemsNoLimitOfferBestForBusiness37() throws Exception {
        runSmartAssetTestWithDiscountsWithOfferLimit3differentdiscountsOneIsOffOld("25", IS_LIMIT_OFFER_OFF, "", 2, 2400, 2, 3400, 2, 2666, 8466, EXPECT_NO_DISCOUNT);

    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupWith3DifferentItemsItemsNoLimitOfferBestForBusiness38() throws Exception {
        runSmartAssetTestWithDiscountsWithOfferLimit3differentdiscountsOneIsOffOld("25", IS_LIMIT_OFFER_OFF, "", 2, 2400, 3, 5100, 2, 2666, 10166, -1875);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupWith3DifferentItemsItemsNoLimitOfferBestForBusiness39() throws Exception {
        runSmartAssetTestWithDiscountsWithOfferLimit3differentdiscountsOneIsOffOld("25", IS_LIMIT_OFFER_OFF, "", 5, 6000, 3, 5100, 2, 2666, 13766, -1875);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupWith3DifferentItemsItemsNoLimitOfferBestForBusiness40() throws Exception {
        runSmartAssetTestWithDiscountsWithOfferLimit3differentdiscountsOneIsOffOld("25", IS_LIMIT_OFFER_OFF, "", 2, 2400, 8, 13600, 1, 10, 16010, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscountAmountOff50WithGroupWith2DifferentItemsItemsNoLimitOfferBestForBusiness41() throws Exception {
        runSmartAssetTestWithDiscountBothBusketsEnabled("50", IS_LIMIT_OFF, "", 1, 0, 3, 12000, 12000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscountAmountOff50WithGroupWith2DifferentItemsItemsNoLimitOfferBestForBusiness42() throws Exception {
        runSmartAssetTestWithDiscountBothBusketsEnabled("50", IS_LIMIT_OFF, "", 2, 0, 2, 8000, 8000, EXPECT_NO_DISCOUNT);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testSmartGiftDiscountAmountOff50WithGroupWith2DifferentItemsItemPrice0BestForBusiness43() throws Exception {
        runSmartAssetTestWithDiscountBothBusketsEnabled("50", IS_LIMIT_OFF, "", 2, 0, 3, 12000, 12000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscountAmountOff50WithGroupWith2DifferentItemsPrice0BestForBusiness44() throws Exception {
        runSmartAssetTestWithDiscountBothBusketsEnabled("50", IS_LIMIT_OFF, "", 5, 0, 3, 12000, 12000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscountAmountOff50WithGroupWith2DifferentItemsPrice0BestForBusiness45() throws Exception {
        runSmartAssetTestWithDiscountOneBusketsOff("50",IS_LIMIT_OFF, "", 5, 0, 8, 32000, 32000,-10000);
    }

    @Test
    public void testSmartGiftDiscountAmountOff50WithGroupWith2DifferentItemsItemPrice0BusketOffBestForBusiness46() throws Exception {
        runSmartAssetTestWithDiscountOneBusketsOff("50", IS_LIMIT_OFF, "", 2, 0, 3, 12000, 12000, -5000);
    }

}



