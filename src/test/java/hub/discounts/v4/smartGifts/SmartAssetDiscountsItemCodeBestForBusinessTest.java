package hub.discounts.v4.smartGifts;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;

/**
 * Created by Jenny on 18/5/2017.
 */

public class SmartAssetDiscountsItemCodeBestForBusinessTest extends BaseHub1SmartAssetBestForBusinessTest {


    @Test
    public void testSmartGiftDiscountAddItemCodesBestForBusiness1() throws Exception {
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        runSmartAssetTesWithDealCodes(dealCodesToCreate,1, 3000, 3, 12000, 15000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscountaddItemCodesBestforBusiness2() throws Exception {
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        runSmartAssetTesWithAddItemCode3Items(dealCodesToCreate,  1, 3000, 3, 12000, 3, 12000, 27000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscountaddItemCodesBestforBusiness3() throws Exception {
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        runSmartAssetTesWithAddItemCodeWithCondition(dealCodesToCreate, 1, 3000, 3, 12000, 15000, EXPECT_NO_DISCOUNT);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testSmartGiftDiscountaddItemCodesBestforBusiness4() throws Exception {
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        runSmartAssetTesWithAddItemCodeWithCondition3Items(dealCodesToCreate, 1, 3000, 3, 12000, 3, 12000, 27000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscountaddItemCodesBestforBusiness5() throws Exception {
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        runSmartAssetTestWithAddItemCodeWithConditionWithBasket(dealCodesToCreate, 1, 3000, 3, 12000, 3, 12000, 27000, EXPECT_NO_DISCOUNT);
    }

    @Test
    public void testSmartGiftDiscountaddItemCodesBestforBusiness6() throws Exception {
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        runSmartAssetTestWithAddItemCodeWithConditionWithBasket(dealCodesToCreate, 3, 3000, 4, 12000, 5, 12000, 27000, EXPECT_NO_DISCOUNT);
    }

}



