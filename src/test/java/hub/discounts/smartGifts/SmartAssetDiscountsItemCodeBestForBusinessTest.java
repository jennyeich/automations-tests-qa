package hub.discounts.smartGifts;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.smarObjects.Scenario;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftActions;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.dealItemCode.AddItemCode;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.dealItemCode.RelatedItemCode;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.services.assets.AssetService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.common.IServerResponse;
import server.common.asset.ActionResponse;
import server.v2_8.Redeem;
import server.v2_8.RedeemResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Customer;
import server.v2_8.common.Item;
import server.v2_8.common.RedeemItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Jenny on 18/5/2017.
 */

public class SmartAssetDiscountsItemCodeBestForBusinessTest extends BaseHub1SmartAssetBestForBusinessTest {


    @Test
    public void testSmartGiftDiscountAddItemCodesBestForBusiness1() throws Exception {

        String assetKey = "";
        ArrayList<String> itemCodesToCreate = Lists.newArrayList("222334","553344");
        smartGift = createSmartGiftWithAddItemCode(Lists.newArrayList(itemCodesToCreate));

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send gift to member
        performAnActionSendGift(smartGift);
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> itemCodes = getRedeemAssetWithItemsArrayGetItemCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2), "15000");
        checkRedeemResults(itemCodesToCreate, itemCodes);

    }

    @Test
    public void testSmartGiftDiscountaddItemCodesBestforBusiness2() throws Exception {

        String assetKey = "";
        ArrayList<String> itemCodesToCreate = Lists.newArrayList("222334","553344");
        smartGift = createSmartGiftWithAddItemCode(Lists.newArrayList(itemCodesToCreate));

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send gift to member
        performAnActionSendGift(smartGift);
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);
        Item item3 = createItem(ITEM_CODE_333, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> itemCodes = getRedeemAssetWithItemsArrayGetItemCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2, item3), "15000");
        checkRedeemResults(itemCodesToCreate, itemCodes);

    }

    @Test
    public void testSmartGiftDiscountaddItemCodesBestforBusiness3() throws Exception {

        String assetKey = "";
        ArrayList<String> itemCodesToCreate = Lists.newArrayList("222334","553344");
        smartGift = createSmartGiftWithAddItemCodeWithCondition(Lists.newArrayList(itemCodesToCreate));

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send gift to member
        performAnActionSendGift(smartGift);
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> itemCodes = getRedeemAssetWithItemsArrayGetItemCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2), "15000");
        checkRedeemResults(itemCodesToCreate, itemCodes);
    }

    @Test
    public void testSmartGiftDiscountaddItemCodesBestforBusiness4() throws Exception {

        String assetKey = "";
        ArrayList<String> itemCodesToCreate = Lists.newArrayList("222334","553344");
        smartGift = createSmartGiftWithAddItemCodeWithCondition(Lists.newArrayList(itemCodesToCreate));

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send gift to member
        performAnActionSendGift(smartGift);
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);
        Item item3 = createItem(ITEM_CODE_333, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> itemCodes = getRedeemAssetWithItemsArrayGetItemCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2,item3), "27000");
        checkRedeemResults(itemCodesToCreate, itemCodes);
    }


    @Test
    @Category(Hub1Regression.class)
    public void testSmartGiftDiscountaddItemCodesBestforBusiness5() throws Exception {

        String assetKey = "";
        ArrayList<String> itemCodesToCreate = Lists.newArrayList("222334","553344");
        smartGift = createSmartGiftWithAddItemCodeWithConditionWithBasket(Lists.newArrayList(itemCodesToCreate));

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send gift to member
        performAnActionSendGift(smartGift);
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);
        Item item3 = createItem(ITEM_CODE_333, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> itemCodes = getRedeemAssetWithItemsArrayGetItemCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2,item3), "27000");
        checkRedeemResults(itemCodesToCreate, itemCodes);
    }


    @Test
    public void testSmartGiftDiscountaddItemCodesBestforBusiness6() throws Exception {

        String assetKey = "";
        ArrayList<String> itemCodesToCreate = Lists.newArrayList("222334","553344");
        smartGift = createSmartGiftWithAddItemCodeWithConditionWithBasketWithRelatedItemCodes(Lists.newArrayList(itemCodesToCreate));

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send gift to member
        performAnActionSendGift(smartGift);
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 3);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 4);
        Item item3 = createItem(ITEM_CODE_333, "Prod1", "123", "Dep1", 12000, 5);

        //Redeem the asset with asset's key
        List<ActionResponse> itemCodes = getRedeemAssetWithItemsArrayGetItemCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2,item3), "27000");
        checkRedeemResults(itemCodesToCreate, itemCodes);
        Assert.assertEquals("The code is as expected : ", "222334",itemCodes.get(2).getCode());
        Assert.assertEquals("Related code is as expected: ", "1234", itemCodes.get(2).getRequiredItems().get(0));

    }


    public String redeemAssetWithItemsArrayNegative(String phoneNumber, String assetKey, ArrayList<Item> items, String totalSum) throws IOException, InterruptedException {

        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        Redeem redeem = new Redeem();
        redeem.getCustomers().add(customer);
        redeem.setReturnExtendedItems(true);
        redeem.setBranchId("2");
        redeem.setPosId("12");
        redeem.setTotalSum(totalSum);
        redeem.setTransactionId(UUID.randomUUID().toString());
        redeem.setChainId("chainIDAutomation");
        redeem.setCashier("cashier1");

        RedeemItem redeemItemPurchase = new RedeemItem();
        redeemItemPurchase.setAssetKey(assetKey);
        ArrayList<RedeemItem> redeemItemForPOS = new ArrayList<>();
        redeemItemForPOS.add(redeemItemPurchase);
        redeem.setRedeemItems(redeemItemForPOS);

        redeem.setItems(items);

        // Send the request
        IServerResponse response = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            return ((ZappServerErrorResponse) response).getErrorMessage();
        }
        else
            return "Passed";
    }


    public SmartGift createSmartGiftWithAddItemCode(ArrayList itemCodes) throws Exception {

        smartGift = createSmartGift(timestamp);
        Scenario scenario = createSmartGiftScenario(smartGift);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddItemCode.toString());
        AddItemCode itemCode = new AddItemCode();
        itemCode.setItemCodes(itemCodes);
        smartGiftAction.setItem(itemCode);
        scenario.addAction(smartGiftAction, smartGift);

        AssetService.createSmartGift(smartGift);
        log4j.info("gift was created succesfully. gift's title is:  " + smartGift.getTitle());

        return smartGift;

    }




    public SmartGift createSmartGiftWithAddItemCodeWithCondition(ArrayList itemCodes) throws Exception {

        smartGift = createSmartGift(timestamp);
        Scenario scenario = createSmartGiftScenario(smartGift);

        //Create smartGiftCondition
        IntOperator equal = hubAssetsCreator.getIntAssetOperator(smartGift, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(smartGift, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "2");
        scenario.addCondition(cond1,smartGift);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddItemCode.toString());
        AddItemCode itemCode = new AddItemCode();
        itemCode.setItemCodes(itemCodes);
        smartGiftAction.setItem(itemCode);
        scenario.addAction(smartGiftAction, smartGift);

        AssetService.createSmartGift(smartGift);
        log4j.info("gift was created succesfully. gift's title is:  " + smartGift.getTitle());

        return smartGift;

    }


    public SmartGift createSmartGiftWithAddItemCodeWithConditionWithBasket(ArrayList itemCodes) throws Exception {

        smartGift = createSmartGift(timestamp);
        Scenario scenario = createSmartGiftScenario(smartGift);

        //Create smartGiftCondition
        IntOperator equal = hubAssetsCreator.getIntAssetOperator(smartGift, IntOperator.Int_Equals_enum.Greaterthan);
        ConditionFieldOprValue cond1 = hubAssetsCreator.createConditionNoGroup(smartGift, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "2");
        scenario.addCondition(cond1,smartGift);

        Group group = hubAssetsCreator.createGroup(smartGift,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(smartGift, GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_111,group);
        hubAssetsCreator.createGroupCondition(smartGift,GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_222,group);
        cond1.setGroup(group);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddItemCode.toString());
        AddItemCode itemCode = new AddItemCode();
        itemCode.setItemCodes(itemCodes);
        smartGiftAction.setItem(itemCode);
        scenario.addAction(smartGiftAction, smartGift);


        AssetService.createSmartGift(smartGift);
        log4j.info("gift was created succesfully. gift's title is:  " + smartGift.getTitle());

        return smartGift;

    }


    public SmartGift createSmartGiftWithAddItemCodeWithConditionWithBasketWithRelatedItemCodes(ArrayList itemCodes) throws Exception {

        smartGift = createSmartGift(timestamp);
        Scenario scenario = createSmartGiftScenario(smartGift);

        //Create smartGiftCondition
        IntOperator equal = hubAssetsCreator.getIntAssetOperator(smartGift, IntOperator.Int_Equals_enum.Greaterthan);
        ConditionFieldOprValue cond1 = hubAssetsCreator.createConditionNoGroup(smartGift, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "2");
        scenario.addCondition(cond1,smartGift);

        Group group = hubAssetsCreator.createGroup(smartGift,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(smartGift,GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_111,group);
        hubAssetsCreator.createGroupCondition(smartGift,GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_222,group);
        cond1.setGroup(group);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddItemCode.toString());
        AddItemCode itemCode = new AddItemCode();
        itemCode.setItemCodes(itemCodes);
        smartGiftAction.setItem(itemCode);

        itemCode.setDefineRelatedItem("y");

        RelatedItemCode relatedItemCode = new RelatedItemCode();
        relatedItemCode.setItemCodeValue("222334");
        relatedItemCode.setItemCodes(Lists.newArrayList("1234"));

        itemCode.setRelatedRelatedItemCodes(Lists.newArrayList(relatedItemCode));
        scenario.addAction(smartGiftAction, smartGift);


        AssetService.createSmartGift(smartGift);
        log4j.info( "gift was created succesfully. gift's title is:  " + smartGift.getTitle());

        return smartGift;

    }


}



