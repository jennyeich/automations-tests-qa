package hub.discounts.smartGifts;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.smarObjects.Scenario;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftActions;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.*;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.DiscountItems_ForEach;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Group;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.GroupField;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.GroupOperators;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.utils.RedeemErrorCodesMap;
import server.v2_8.common.Item;

/**
 * Created by Jenny on 18/5/2017.
 */

public class SmartAssetDiscountsBestForBusinessTest extends BaseHub1SmartAssetBestForBusinessTest {


    @Test
    public void testSmartGiftDiscount33PercentWithGroupBestforBusiness1() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscount("33",IS_LIMIT_OFF,"");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 4000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "70000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-990", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartGiftDiscount20PercentNoGroupBestforBusiness2() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscountNoGroup ("20", IS_LIMIT_OFF, "");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 4000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "7000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-1400", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @Test
    public void testSmartGiftDiscount0PercentNoGroupBestforBusiness3() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscountNoGroup ("0", IS_LIMIT_OFF, "");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 4000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "7000");

        //Check discount's sum
        Assert.assertEquals("Violation of asset is expected ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION), discountSum);
        log4j.info("Violation of asset is expected : " + discountSum);

    }


    @Test
    public void testSmartGiftDiscount100PercentNoGroupBestforBusiness4() throws Exception {

        String assetKey = "";

        smartGift = createSmartGiftWithDiscountNoGroup ("100", IS_LIMIT_OFF, "");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 4000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "7000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-7000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @Test
    public void testSmartGiftDiscount50PercentWith200LimitBestforBusiness5() throws Exception {

        String assetKey = "";

        smartGift = createSmartGiftWithDiscountNoGroup ("50", IS_LIMIT_CHECKED, "200");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 15000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 25000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "40000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-20000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @Test
    public void testSmartGiftDiscount50PercentNoGroupWith100LimittBestforBusiness6() throws Exception {

        String assetKey = "";

        smartGift = createSmartGiftWithDiscountNoGroup ("50", IS_LIMIT_CHECKED, "100");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 15000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 25000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "40000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-10000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @Test
    public void testSmartGiftDiscount50PercentNoGroupWith300LimitBestforBusiness7() throws Exception {

        String assetKey = "";

        smartGift = createSmartGiftWithDiscountNoGroup ("50", IS_LIMIT_CHECKED, "300");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 15000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 25000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "40000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-20000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @Test
    public void testSmartGiftDiscount0PercentNoGroupWith0LimitBestforBusiness8() throws Exception {

        String assetKey = "";

        smartGift = createSmartGiftWithDiscountNoGroup ("0", IS_LIMIT_CHECKED, "0");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 15000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 25000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "40000");

        //Check discount's sum
        Assert.assertEquals("Violation of asset is expected ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION), discountSum);
        log4j.info("Violation of asset is expected : " + discountSum);

    }

    @Test
    public void testSmartGiftDiscount0PercentNoGroupWith100LimitBestforBusiness9() throws Exception {

        String assetKey = "";

        smartGift = createSmartGiftWithDiscountNoGroup ("0",IS_LIMIT_CHECKED, "100");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 15000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 25000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "40000");

        //Check discount's sum
        Assert.assertEquals("Violation of asset is expected ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION), discountSum);
        log4j.info("Violation of asset is expected : " + discountSum);

    }


    @Test
    public void testSmartGiftDiscount100PercentNoGroupWith100LimitBestforBusiness10() throws Exception {

        String assetKey = "";

        smartGift = createSmartGiftWithDiscountNoGroup ("100", IS_LIMIT_CHECKED, "100");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 15000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 25000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "40000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-10000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @Test
    public void testSmartGiftDiscount100PercentNoGroupWith500LimitBestforBusiness11() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscountNoGroup ("100",IS_LIMIT_CHECKED, "500");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 15000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 25000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "40000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-40000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }


    @Test
    public void testSmartGiftDiscount25PercentWithGroupNoLimitOfferBestforBusiness12() throws Exception {

        String assetKey = "";


        smartGift = createSmartGiftWithDiscountsWithOfferLimit ("25", IS_LIMIT_OFF, "");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "15000");

        //Check discount's sum
        Assert.assertEquals("Violation of asset is expected ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION), discountSum);
        log4j.info("Violation of asset is expected : " + discountSum);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupNoLimitOfferBestforBusiness13() throws Exception {

        String assetKey = "";

        smartGift = createSmartGiftWithDiscountsWithOfferLimit ("25",IS_LIMIT_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 6000, 2);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 8000, 2);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "14000");

        //Check discount's sum
        Assert.assertEquals("Violation of asset is expected ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION), discountSum);
        log4j.info("Violation of asset is expected : " + discountSum);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroupNoLimitOfferBestforBusiness14() throws Exception {

        String assetKey = "";

        smartGift = createSmartGiftWithDiscountsWithOfferLimit ("25",IS_LIMIT_OFF, "");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 6000, 2);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "18000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-4500", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartGiftDiscount25PercentWithGroupNoLimitOfferBestforBusiness15() throws Exception {

        String assetKey = "";


        smartGift = createSmartGiftWithDiscountsWithOfferLimit ("25",IS_LIMIT_OFF, "");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 15000, 5);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "27000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-4500", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartGiftDiscount25PercentWithGroupNoLimitOfferBestforBusiness16() throws Exception {

        String assetKey = "";


        smartGift = createSmartGiftWithDiscountsWithOfferLimit ("25",IS_LIMIT_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 15000, 5);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 32000, 8);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "47000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-9000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroup1LimitOfferBestforBusiness17() throws Exception {

        String assetKey = "";


        smartGift = createSmartGiftWithDiscountsWithOfferLimit ("25",IS_LIMIT_CHECKED, "1");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 15000, 5);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "27000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-4500", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    @Category(Hub1Regression.class)
    public void testSmartGiftDiscount25PercentWithGroup1LimitOfferBestforBusiness18() throws Exception {

        String assetKey = "";


        smartGift = createSmartGiftWithDiscountsWithOfferLimit ("25",IS_LIMIT_CHECKED, "1");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 6000, 2);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 32000, 8);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "38000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-4500", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroup1LimitOfferBestforBusiness19() throws Exception {

        String assetKey = "";


        smartGift = createSmartGiftWithDiscountsWithOfferLimit ("25",IS_LIMIT_CHECKED, "1");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 21000, 2);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 40000, 8);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "61000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-9000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartGiftDiscount25PercentWithGroup4LimitOfferBestForBusiness20() throws Exception {

        String assetKey = "";


        smartGift = createSmartGiftWithDiscountsWithOfferLimit ("25",IS_LIMIT_CHECKED, "4");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 60000, 20);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 60000, 15);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "120000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-18000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }






  //  private void performAnActionSendGift(SmartGift smartGift) throws Exception {

  //      Thread.sleep(4000);
 //       boolean isGiftExist = MembersService.performAnActionSendGift(smartGift);
 //       logger.log(LogStatus.INFO, "The gift was send to the member");
 //       Assert.assertTrue(isGiftExist);

  //  }



    public SmartGift createSmartGiftWithDiscount(String percentOffNumber, String isLimit, String limitNum) throws Exception {

        smartGift = createSmartGift(timestamp);
        Scenario scenario = createSmartGiftScenario(smartGift);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartGift, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());

        //percent off smartGiftAction
        PercentOff percentOff = createPercentOffDiscount(smartGift, percentOffNumber, isLimit, limitNum, DISCOUNTS_CHECK_BOX_ON, "", "");

        SetDiscountOnItems d = createSetDiscountOnItem(smartGift, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_ON, percentOff);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartGift, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartGift, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnItem(smartGift, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_OFF, percentOff);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartGift, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartGift, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        createDiscount(smartGift, smartGiftAction, scenario, percentOff, discount);

        return smartGift;

    }

    public SmartGift createSmartGiftWithDiscountsWithOfferLimit (String discountNum, String isLimitOffer, String limitOfferNum) throws Exception{

        smartGift = createSmartGift(timestamp);
        Scenario scenario = createSmartGiftScenario(smartGift);


        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartGift, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());

        //percent off smartGiftAction
        PercentOff percentOff = createPercentOffDiscount(smartGift, discountNum, IS_LIMIT_OFF, "", DISCOUNTS_CHECK_BOX_ON, isLimitOffer, limitOfferNum);

        SetDiscountOnItems d = createSetDiscountOnTimes(smartGift, DiscountItems_ForEach.quantity.toString(), "2", "");
        percentOff.addSetDiscountOnItems(d, smartGift);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartGift, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartGift, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnTimes(smartGift, DiscountItems_ForEach.quantity.toString(), "3", DISCOUNT_ON);
        percentOff.addSetDiscountOnItems(d2, smartGift);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartGift, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartGift, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        createDiscount(smartGift, smartGiftAction, scenario, percentOff, discount);

        return smartGift;
    }



    public SetDiscountOnItems createDiscountWithGroup (PercentOff percentOff, String discountType, String discountNum, String discountOffOn ){

        SetDiscountOnItems d = createDiscountWithGroup(discountType, discountNum , discountOffOn);
        percentOff.addSetDiscountOnItems(d, smartGift);

        return d;
    }

    public SetDiscountOnItems createDiscountWithGroup (String discountType, String discountNum, String discountOffOn ){

        SetDiscountOnItems d = createSetDiscountOnTimes(smartGift, discountType, discountNum , discountOffOn);
        return d;
    }





    public SetDiscountOnItems createSetDiscountOnItem(SmartGift smartGift, String quanityType, String count, String onnOff, PercentOff percentOff){

        SetDiscountOnItems d = createSetDiscountOnTimes(smartGift, quanityType, count, onnOff);
        percentOff.addSetDiscountOnItems(d, smartGift);

        return d;


    }

    public SmartGift createSmartGiftWithDiscountNoGroup (String percentOffNumber, String isLimit, String limitNum) throws Exception{


        smartGift = createSmartGift(timestamp);
        Scenario scenario = createSmartGiftScenario(smartGift);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartGift, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());

        //percent off smartGiftAction
        PercentOff percentOff = createPercentOffDiscount(smartGift, percentOffNumber, isLimit, limitNum, DISCOUNTS_CHECK_BOX_OFF, "", "");
        createDiscount(smartGift, smartGiftAction, scenario, percentOff, discount);

        return smartGift;
    }


    public SetDiscountOnItems createSetDiscountOnItem(SmartGift smartGift, String quanityType, String count, String onnOff, AmountOff amountOff){

        SetDiscountOnItems d = createSetDiscountOnTimes(smartGift, quanityType, count, onnOff);
        amountOff.addSetDiscountOnItems(d, smartGift);

        return d;


    }


    public SmartGift createSmartGiftWithDiscountsWithOfferLimit3differentdiscountsOneIsOff(String discountNum, String isLimitOffer, String limitOfferNum) throws Exception{

        smartGift = createSmartGift(timestamp);
        Scenario scenario = createSmartGiftScenario(smartGift);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());
        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartGift, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());
        //percent off smartGiftAction
        PercentOff percentOff = createPercentOffDiscount(smartGift, discountNum, IS_LIMIT_OFF, "", DISCOUNTS_CHECK_BOX_ON, isLimitOffer, limitOfferNum);

        //Create new group
        Group group = createGroupWithConds(timestamp, smartGift, createDiscountWithGroup(percentOff,DiscountItems_ForEach.quantity.toString(),"2", DISCOUNT_ON));
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartGift, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartGift, createDiscountWithGroup(percentOff,DiscountItems_ForEach.quantity.toString(),"3", DISCOUNT_ON));
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartGift, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);

        Thread.sleep(1000);
        //Create new group3
        Group group3 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartGift, createDiscountWithGroup(percentOff,DiscountItems_ForEach.spend.toString(),"2", DISCOUNT_OFF));
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartGift, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_333, group3);

        createDiscount(smartGift, smartGiftAction, scenario, percentOff, discount);
        return smartGift;
    }


    public SetDiscountOnItems createDiscountWithGroup (IDiscountType iDiscountType, String discountType, String discountNum, String discountOffOn ){

        SetDiscountOnItems d = createDiscountWithGroup(discountType, discountNum , discountOffOn);
        iDiscountType.addSetDiscountOnItems(d, smartGift);

        return d;
    }




}



