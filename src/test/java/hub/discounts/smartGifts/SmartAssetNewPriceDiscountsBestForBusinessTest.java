package hub.discounts.smartGifts;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.utils.RedeemErrorCodesMap;
import server.v2_8.common.Item;

/**
 * Created by Jenny on 5/12/2017.
 */

public class SmartAssetNewPriceDiscountsBestForBusinessTest extends BaseHub1SmartAssetBestForBusinessTest {


    @Test
    public void testSmartGiftDiscountNEWPrice5111On222OFFBestforBusiness1() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscountNewPriceItemCode111ONItemCode222OF("5",IS_LIMIT_OFF,"", "1",DISCOUNT_ON,"1",DISCOUNT_OFF, "","");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
       performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod2", "123", "Dep1", 5000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod3", "123", "Dep1", 5000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "10000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-4500", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartGiftDiscountNEWPrice5111On222ONBestforBusiness2() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscountNewPriceItemCode111ONItemCode222OF("5",IS_LIMIT_OFF,"","1",DISCOUNT_ON,"1",DISCOUNT_ON, "", "");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 5000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 5000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "10000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-9500", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }



    @Test
    public void testSmartGiftDiscountNEWPrice5111On222ONBestforBusinessNegative3() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscountNewPriceItemCode111ONItemCode222OF("5",IS_LIMIT_OFF,"","1",DISCOUNT_ON,"1",DISCOUNT_ON, "", "");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 200, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 300, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "500");

        //Check discount's sum
        Assert.assertEquals("Violation of asset is expected ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION), discountSum);
        log4j.info("Violation of asset is expected : " + discountSum);
    }


    /**
    * Configuration: set a Club Deal with the following settings:
        Action: Add Discount -  New Price $5.00
        Apply Discount on Specific Items: Department X
        Limit Times Discount Applied per Purchase - Limit to 2 times
        Buy Item A $8.00 and verify that the discount is $3.00.
    **/

    @Test
    public void testSmartGiftDiscountNEWPrice5Dep111OnBestforBusinessLimit24() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscountNewPriceDEPCODE111("5",IS_LIMIT_CHECKED,"3",DISCOUNT_ON, "1", "", "");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", DEP_CODE_111, "111", 800, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAssetWithItemsArray(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item), "800");
        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-300", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    //The same as test 4, just with 3 items. only 2 will get the discount

    @Test
    public void testSmartGiftDiscountNEWPrice5Dep111OnBestForBusinessLimit25() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscountNewPriceDEPCODE111("5",IS_LIMIT_CHECKED,"6",DISCOUNT_ON, "1", "", "");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", DEP_CODE_111, "111", 2400, 3);

        //Redeem the asset with asset's key
        String discountSum = redeemAssetWithItemsArray(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item), "2400");
        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-600", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }


    /**
    * Configuration: set a Gift with the following settings:
        Action: Add Discount -  New Price $20.00
        Apply Discount on Specific Items: Department X
        Buy 3 items:
        Item A $8.00
        Item B $5.00
        Item C $ 10.00
        Verify that the discount is $3.00 and allocate on the items properly.
    */
    @Test
    public void testSmartGiftDiscountNEWPrice5Dep111OnBestForBusiness6() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscountNewPriceDEPCODE111("20",IS_LIMIT_OFF,"",DISCOUNT_ON, "3", "", "");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", DEP_CODE_111, "111", 800, 1);
        Item item2 = createItem(ITEM_CODE_111, "Prod1", DEP_CODE_111, "111", 500, 1);
        Item item3 = createItem(ITEM_CODE_111, "Prod1", DEP_CODE_111, "111", 1000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAssetWithItemsArray(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item,item2, item3), "2400");
        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-300", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }


    /**
     *  New Price per Item in Group - Buy 2 Item A, get B for New Price of $5
     Configuration: set a Gift with the following settings:
     Action: Add Discount -  New Price $5.00
     Apply Discount on Specific Items:
     Qty= 2 of Item A; Discount = False
     Qty=1 of Item B; Discount = True
     Buy 3 items:
     Item A $15.00
     Item A $15.00
     Item B $ 10.00
     Verify that the discount is $5.00.
    * */

    @Test
    @Category(Hub1Regression.class)
    public void testSmartGiftDiscountNEWPriceBuy2Get1forPrice5BestforBusiness7() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscountNewPriceItemCode111ONItemCode222OF("5",IS_LIMIT_OFF,"", "2",DISCOUNT_OFF,"1",DISCOUNT_ON, IS_LIMIT_OFFER_OFF, IS_LIMIT_OFFER_OFF);

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod111", "123", "Dep1", 3000, 2);
        Item item2 = createItem(ITEM_CODE_222, "Prod12", "123", "Dep1", 1000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "4000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-500", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    /**
     * New Price per Item - Buy 3 Item A, get each A for New Price of $5
     Configuration: set a Gift with the following settings:
     Action: Add Discount -  New Price $5.00 [per item]
     Apply Discount on Specific Items: Qty= 3 of Item A; Discount = True
     Buy 3 Item A, each $8.00. Verify that the total discount is $9.00.
     * */

    @Test
    public void testSmartGiftDiscountNEWPriceBuy3getEachOneForNewPriceBestForBusiness8() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscountNewPriceDEPCODE111("15",IS_LIMIT_OFF,"",DISCOUNT_ON, "3",IS_LIMIT_OFFER_OFF, IS_LIMIT_OFFER_OFF);

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod123", DEP_CODE_111, "111", 2400, 3);

        //Redeem the asset with asset's key
        String discountSum = redeemAssetWithItemsArray(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item), "2400");
        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-900", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    /**
     * New Price per Item - Buy 3 Item A, get each A for New Price of $5
     Configuration: set a Gift with the following settings:
     Action: Add Discount -  New Price $5.00 [per item]
     Apply Discount on Specific Items: Qty= 3 of Item A; Discount = True
     Buy 4 Item A, each $8.00. Verify that the total discount is $9.00.
     * */

    @Test
    public void testSmartGiftDiscountNEWPriceBuy3getEachOneForNewPriceBestForBusiness9() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscountNewPriceDEPCODE111("15",IS_LIMIT_OFF,"",DISCOUNT_ON, "3", IS_LIMIT_OFFER_OFF, IS_LIMIT_OFFER_OFF);

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod123", DEP_CODE_111, "111", 3200, 4);

        //Redeem the asset with asset's key
        String discountSum = redeemAssetWithItemsArray(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item), "2400");
        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-900", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }


    /**
     * New Price per Item - Buy 3 Item A, get each A for New Price of $5
     Configuration: set a Gift with the following settings:
     Action: Add Discount -  New Price $5.00 [per item]
     Apply Discount on Specific Items: Qty= 3 of Item A; Discount = True
     Buy 6 Item A, each $8.00. Verify that the total discount is $18.00.
     * */

    @Test
    public void testSmartGiftDiscountNEWPriceBuy3getEachOneForNewPriceBestForBusiness10() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscountNewPriceDEPCODE111("15",IS_LIMIT_OFF,"",DISCOUNT_ON, "3", IS_LIMIT_OFFER_OFF, IS_LIMIT_OFFER_OFF);

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod121", DEP_CODE_111, "111", 4800, 6);

        //Redeem the asset with asset's key
        String discountSum = redeemAssetWithItemsArray(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item), "2400");
        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-1800", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    /**
     * New Price per Item - Buy 3 Item A, get each A for New Price of $5
     Configuration: set a Gift with the following settings:
     Action: Add Discount -  New Price $5.00 [per item]
     Apply Discount on Specific Items: Qty= 3 of Item A; Discount = True
     Buy 6 Item A, each $8.00. Verify that the total discount is $18.00.
     With Limit offer =3
     * */

    @Test
    public void testSmartGiftDiscountNEWPriceBuy3getEachOneForNewPriceBestForBusiness11() throws Exception {

        smartGift = createSmartGiftWithDiscountNewPriceDEPCODE111("15",IS_LIMIT_OFF,"",DISCOUNT_ON, "3", IS_LIMIT_OFFER_CHECKED, "1");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        String assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod12", DEP_CODE_111, "111", 4800, 6);

        //Redeem the asset with asset's key
        String discountSum = redeemAssetWithItemsArray(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item), "2400");
        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-900", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }



    /**
     * Scenario 1: New Price per Item + Limit - Buy any item from Department X for new price $5
     1. Configuration: set a Club Deal with the following settings:
     Action: Add Discount -  New Price $5.00
     Apply Discount on Specific Items: Department X
     Limit Times Discount Applied per Purchase - Limit to 1 time
     2.Buy 2 Items:
     Item A  $9.00
     Item B  $8.00
     Best for the Business: Verify that the new price is apply to Item B, total discount is $3.00.
     * */

    @Test
    @Category(Hub1Regression.class)
    public void testSmartGiftDiscountSpcificDepartmentForNewPriceBestForBusiness12() throws Exception {

        smartGift = createSmartGiftWithDiscountNewPriceDEPCODE111("5",IS_LIMIT_OFF,"",DISCOUNT_ON, "1", IS_LIMIT_OFFER_CHECKED, "1");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        String assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_222, "Prod1", DEP_CODE_111, "1112", 900, 1);
        Item item2 = createItem(ITEM_CODE_333, "Prod1", DEP_CODE_111, "1113", 800, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAssetWithItemsArray(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item,item2), "1700");
        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-300", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }
    /**
     * Scenario 1: New Price per Item + Limit - Buy any item from Department X for new price $5
     1. Configuration: set a Club Deal with the following settings:
     Action: Add Discount -  New Price $5.00
     Apply Discount on Specific Items: Department X
     Limit Times Discount Applied per Purchase - Limit to 2 time
     2.Buy 2 Items:
     Item A  $9.00
     Item B  $8.00
     Best for the Business: Verify that the new price is apply to Item B, total discount is $3.00.
     * */

    @Test
    public void testSmartGiftDiscountSpcificDepartmentForNewPriceBestForBusiness13() throws Exception {

        smartGift = createSmartGiftWithDiscountNewPriceDEPCODE111("5",IS_LIMIT_OFF,"",DISCOUNT_ON, "1", IS_LIMIT_OFFER_CHECKED, "2");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        String assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod11", DEP_CODE_111, "1112", 900, 1);
        Item item2 = createItem(ITEM_CODE_333, "Prod21", DEP_CODE_111, "1113", 800, 1);
        Item item3 = createItem(ITEM_CODE_222, "Prod13", DEP_CODE_111, "1112", 700, 1);
        Item item4 = createItem("1232", "Prod14", DEP_CODE_111, "1113", 600, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAssetWithItemsArray(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item,item2,item3,item4), "1700");
        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-300", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    /**
     * Scenario 1: New Price per Item + Limit - Buy any item from Department X for new price $5
     1. Configuration: set a Club Deal with the following settings:
     Action: Add Discount -  New Price $5.00
     Apply Discount on Specific Items: Department X
     Limit Times Discount Applied per Purchase - Limit to 2 time
     2.Buy 2 Items:
     Item A  $9.00
     Item B  $8.00
     Best for the Business: Verify that no dicount is returned.
     * */

    @Test
    public void testSmartGiftDiscountSpcificDepartmentForNewPriceBestForBusiness14() throws Exception {

        smartGift = createSmartGiftWithDiscountNewPriceDEPCODE111("5",IS_LIMIT_OFF,"",DISCOUNT_ON, "1", IS_LIMIT_OFFER_CHECKED, "2");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        String assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod11", DEP_CODE_111, "1112", 900, 1);
        Item item2 = createItem(ITEM_CODE_333, "Prod21", DEP_CODE_111, "1113", 800, 1);
        Item item3 = createItem(ITEM_CODE_222, "Prod13", DEP_CODE_111, "1112", 500, 1);
        Item item4 = createItem("1232", "Prod14", DEP_CODE_111, "1113", 300, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAssetWithItemsArray(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item,item2,item3,item4), "1700");

        //Check discount's sum
        Assert.assertEquals("Violation of asset is expected ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION), discountSum);
        log4j.info("Violation of asset is expected : " + discountSum);

    }


    /**
    * Scenario 2: New Price per Group of Items - Buy A, B and C and get the three of them in $15
     1. Configuration: set a Gift with the following settings:
     a. Action: Add Discount -  New Price $15.00
     b. Apply Discount on Specific Items: Item A, Item B, Item C.
     c. Limit Times Discount Applied per Purchase - Limit to 1 time
    * */

    @Test
    public void testSmartGiftDiscountSpcificDepartmentForNewPriceBestForCustomer15() throws Exception {

        smartGift = createSmartGiftWithDiscountNewPriceItemCode111ONItemCode222ONItemCode333ON("15",IS_LIMIT_OFF,"","1", DISCOUNT_ON,"1",DISCOUNT_ON, "1",DISCOUNT_ON, IS_LIMIT_OFFER_CHECKED, "1");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        String assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod112", DEP_CODE_111, "1112", 1000, 1);
        Item item2 = createItem(ITEM_CODE_333, "Prod221", DEP_CODE_111, "1113", 900, 1);
        Item item3 = createItem(ITEM_CODE_222, "Prod123", DEP_CODE_111, "1112", 800, 1);
        Item item4 = createItem(ITEM_CODE_111, "Prod112", DEP_CODE_111, "1112", 700, 1);
        Item item5 = createItem(ITEM_CODE_333, "Prod221", DEP_CODE_111, "1113", 600, 1);
        Item item6 = createItem(ITEM_CODE_222, "Prod123", DEP_CODE_111, "1112", 500, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAssetWithItemsArray(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item,item2,item3,item4,item5,item6), "4500");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-300", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }


    /**
     * Scenario 3: New Price per Group of Items - Buy 3 items from {A, B, C} and get the three of them in $15
     Buy 6 items:
     Item A $10.00
     Item A $10.00
     Item B $5.00
     Item B $5.00
     Item B $5.00
     Item C $3.00
     a. Best for the Business: Verify that no discount is returned.
     b. Best for the Customer: Verify that the discount is allocated on Item A, A and B.Total discount is $10.00.
     */
    @Test
    public void testSmartGiftDiscountNEWPrice5Dep111OnBestForBusiness16() throws Exception {

        String assetKey = "";
        smartGift = createSmartGiftWithDiscountNewPriceDEPCODE111("15",IS_LIMIT_OFF,"",DISCOUNT_ON, "3", "", "1");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", DEP_CODE_111, "111", 1000, 2);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", DEP_CODE_111, "111", 500, 3);
        Item item3 = createItem(ITEM_CODE_333, "Prod1", DEP_CODE_111, "111", 300, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAssetWithItemsArray(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item,item2, item3), "2400");

        //Check discount's sum
        Assert.assertEquals("Violation of asset is expected ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION), discountSum);
        log4j.info("Violation of asset is expected : " + discountSum);

    }


    /**
     * Scenario 2: New Price per Group of Items - Buy A, B and C and get the three of them in $15
     1. Configuration: set a Gift with the following settings:
     a. Action: Add Discount -  New Price $10.00
     b. Apply Discount on Specific Items: Item A, Item B, Item C.
     c. Limit Times Discount Applied per Purchase - Limit to 1 time
     * */

    @Test
    public void testSmartGiftDiscountSpcificDepartmentForNewPriceBestForBusiness17() throws Exception {

        smartGift = createSmartGiftWithDiscountNewPriceItemCode111ONItemCode222ONItemCode333ON("10",IS_LIMIT_OFF,"","1", DISCOUNT_ON,"1",DISCOUNT_ON, "1",DISCOUNT_ON, IS_LIMIT_OFFER_CHECKED, "1");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        String assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item4 = createItem(ITEM_CODE_111, "Prod112", DEP_CODE_111, "1112", 700, 1);
        Item item5 = createItem(ITEM_CODE_333, "Prod221", DEP_CODE_111, "1113", 600, 1);
        Item item6 = createItem(ITEM_CODE_222, "Prod123", DEP_CODE_111, "1112", 500, 1);


        //Redeem the asset with asset's key
        String discountSum = redeemAssetWithItemsArray(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item4,item5,item6), "1800");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-800", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

}



