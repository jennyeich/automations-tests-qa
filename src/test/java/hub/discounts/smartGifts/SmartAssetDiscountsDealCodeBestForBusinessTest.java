package hub.discounts.smartGifts;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.benefits.redeemCode.RedeemCodeAutoGenerated;
import hub.common.objects.benefits.redeemCode.RedeemCodeType;
import hub.common.objects.common.TimeUnits;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.smarObjects.Scenario;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftActions;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.dealItemCode.AddDealCode;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.dealItemCode.RelatedDealCode;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.services.assets.AssetService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.common.asset.ActionResponse;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jenny on 18/5/2017.
 */

public class SmartAssetDiscountsDealCodeBestForBusinessTest extends BaseHub1SmartAssetBestForBusinessTest {


    @Test
    public void testSmartGiftDiscountaddDealCodesBestforBusiness1() throws Exception {

        String assetKey = "";
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        smartGift = createSmartGiftWithAddDealCode(dealCodesToCreate);
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send gift to member
        performAnActionSendGift(smartGift);
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> dealCodes = getRedeemAssetWithItemsArrayGetDealCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2), "15000");
        verifyTheDealCodesresponse(dealCodesToCreate, dealCodes);
    }




    @Test
    @Category(Hub1Regression.class)
    public void testSmartGiftDiscountaddDealCodesBestforBusiness2() throws Exception {

        String assetKey = "";
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        smartGift = createSmartGiftWithAddDealCode(Lists.newArrayList(dealCodesToCreate));

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send gift to member
        performAnActionSendGift(smartGift);
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);
        Item item3 = createItem(ITEM_CODE_333, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> dealCodes = getRedeemAssetWithItemsArrayGetDealCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2, item3), "15000");

        verifyTheDealCodesresponse(dealCodesToCreate, dealCodes);
    }

    @Test
    public void testSmartGiftDiscountaddDealCodesBestforBusiness3() throws Exception {

        String assetKey = "";
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        smartGift = createSmartGiftWithAddDealCodeWithCondition(Lists.newArrayList(dealCodesToCreate));

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> dealCodes = getRedeemAssetWithItemsArrayGetDealCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2), "15000");

        verifyTheDealCodesresponse(dealCodesToCreate, dealCodes);
    }

    @Test
    public void testSmartGiftDiscountaddDealCodesBestforBusiness4() throws Exception {

        String assetKey = "";
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        smartGift = createSmartGiftWithAddDealCodeWithCondition(Lists.newArrayList(dealCodesToCreate));

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send gift to member
        performAnActionSendGift(smartGift);
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);
        Item item3 = createItem(ITEM_CODE_333, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> dealCodes = getRedeemAssetWithItemsArrayGetDealCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2,item3), "27000");
        verifyTheDealCodesresponse(dealCodesToCreate, dealCodes);
    }


    @Test
    public void testSmartGiftDiscountaddDealCodesBestforBusiness5() throws Exception {

        String assetKey = "";
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        smartGift = createSmartGiftWithAddDealCodeWithConditionWithBasket(Lists.newArrayList(dealCodesToCreate));

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send gift to member
        performAnActionSendGift(smartGift);
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);
        Item item3 = createItem(ITEM_CODE_333, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> dealCodes = getRedeemAssetWithItemsArrayGetDealCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2,item3), "27000");
        verifyTheDealCodesresponse(dealCodesToCreate, dealCodes);
    }

    @Test
    public void testSmartGiftDiscountAddDealCodesBestForBusiness6() throws Exception {

        String assetKey = "";
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        smartGift = createSmartGiftWithAddItemCodeWithConditionWithBasketWithRelatedDealCodes(Lists.newArrayList(dealCodesToCreate));

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send gift to member
        performAnActionSendGift(smartGift);
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);
        Item item3 = createItem(ITEM_CODE_333, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> dealCodes = getRedeemAssetWithItemsArrayGetDealCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2,item3), "27000");
        verifyTheDealCodesresponse(dealCodesToCreate, dealCodes);
        Assert.assertEquals("The code is as expected : ", "222334",dealCodes.get(2).getCode());
        Assert.assertEquals("Related code is as expected: ", "1234", dealCodes.get(2).getRequiredItems().get(0));


    }


    public SmartGift createSmartGiftWithAddItemCodeWithConditionWithBasketWithRelatedDealCodes(ArrayList dealCodes) throws Exception {

        smartGift = createSmartGift(timestamp);
        Scenario scenario = createSmartGiftScenario(smartGift);

        //Create smartGiftCondition
        IntOperator equal = hubAssetsCreator.getIntAssetOperator(smartGift, IntOperator.Int_Equals_enum.Greaterthan);
        ConditionFieldOprValue cond1 = hubAssetsCreator.createConditionNoGroup(smartGift, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "2");
        scenario.addCondition(cond1,smartGift);

        Group group = hubAssetsCreator.createGroup(smartGift,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(smartGift, GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_111,group);
        hubAssetsCreator.createGroupCondition(smartGift,GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_222,group);
        cond1.setGroup(group);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDealCode.toString());
        AddDealCode addDealCode = new AddDealCode();
        addDealCode.setDealCodes(dealCodes);
        smartGiftAction.setCode(addDealCode);
        addDealCode.setDefineRelatedItem("y");

        RelatedDealCode relatedDealCode = new RelatedDealCode();
        relatedDealCode.setDealCodeValue("222334");
        relatedDealCode.setDealCodes(Lists.newArrayList("1234"));
        addDealCode.setRelatedRelatedDealCodes(Lists.newArrayList(relatedDealCode));
        scenario.addAction(smartGiftAction, smartGift);


        AssetService.createSmartGift(smartGift);
        log4j.info("gift was created succesfully. gift's title is:  " + smartGift.getTitle());

        return smartGift;

    }



    public static SmartGift createSmartGift(CharSequence timestamp) {

        SmartGift smartGift = new SmartGift();
        smartGift.setDescription("Sample description" + timestamp);
        smartGift.setShortName("sample short name " + timestamp);
        smartGift.setTitle("testTitle" + timestamp);
        //Create with redeem code auto generated
        smartGift.setRedeemCodeType(RedeemCodeType.AUTOGENERATED_CODES);
        smartGift.setRedeemcodeAutoGenerated(new RedeemCodeAutoGenerated("12", TimeUnits.HOURS));
        log4j.info("gift was created succesfully. gift's title is:  " + smartGift.getTitle());

        return smartGift;

    }



    public static Scenario createSmartGiftScenario(SmartGift smartGift) {

        Scenario scenario = new Scenario();
        smartGift.addScenario(scenario);
        return scenario;

    }


    public static SmartGiftAction createSmartGiftAction(String action) {

        SmartGiftAction smartGiftAction = new SmartGiftAction();
        smartGiftAction.setPerformTheAction(action);
        return smartGiftAction;

    }


    public static Group createGroup(SmartGift gift, String groupName) {

        Group group = new Group(gift);
        group.setGroupName(groupName);
        return group;

    }



  //  private void performAnActionSendGift(SmartGift smartGift) throws Exception {

  //      Thread.sleep(4000);
  //      boolean isGiftExist = MembersService.performAnActionSendGift(smartGift);
   //     logger.log(LogStatus.INFO, "The gift was send to the member");
   //     Assert.assertTrue(isGiftExist);

  //  }



    public SmartGift createSmartGiftWithAddDealCode(ArrayList dealCodes) throws Exception {

        smartGift = createSmartGift(timestamp);
        Scenario scenario = createSmartGiftScenario(smartGift);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDealCode.toString());
        AddDealCode addDealCode = new AddDealCode();
        addDealCode.setDealCodes(dealCodes);
        smartGiftAction.setCode(addDealCode);
        scenario.addAction(smartGiftAction, smartGift);

        AssetService.createSmartGift(smartGift);
        log4j.info("gift was created succesfully. gift's title is:  " + smartGift.getTitle());

        return smartGift;

    }




    public SmartGift createSmartGiftWithAddDealCodeWithCondition(ArrayList dealCodes) throws Exception {

        smartGift = createSmartGift(timestamp);
        Scenario scenario = createSmartGiftScenario(smartGift);

        //Create smartGiftCondition
        IntOperator equal = hubAssetsCreator.getIntAssetOperator(smartGift, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(smartGift, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "2");
        scenario.addCondition(cond1,smartGift);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDealCode.toString());
        AddDealCode addDealCode = new AddDealCode();
        addDealCode.setDealCodes(dealCodes);
        smartGiftAction.setCode(addDealCode);
        scenario.addAction(smartGiftAction, smartGift);



        AssetService.createSmartGift(smartGift);
        log4j.info("gift was created succesfully. gift's title is:  " + smartGift.getTitle());

        return smartGift;

    }


    public SmartGift createSmartGiftWithAddDealCodeWithConditionWithBasket(ArrayList dealCodes) throws Exception {

        smartGift = createSmartGift(timestamp);
        Scenario scenario = createSmartGiftScenario(smartGift);


        //Create smartGiftCondition
        IntOperator equal = hubAssetsCreator.getIntAssetOperator(smartGift, IntOperator.Int_Equals_enum.Greaterthan);
        ConditionFieldOprValue cond1 = hubAssetsCreator.createConditionNoGroup(smartGift, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "2");
        scenario.addCondition(cond1,smartGift);

        Group group = hubAssetsCreator.createGroup(smartGift,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(smartGift,GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_111,group);
        hubAssetsCreator.createGroupCondition(smartGift,GroupField.ItemCode,GroupOperators.Equals.toString(),ITEM_CODE_222,group);
        cond1.setGroup(group);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDealCode.toString());
        AddDealCode addDealCode = new AddDealCode();
        addDealCode.setDealCodes(dealCodes);
        smartGiftAction.setCode(addDealCode);
        scenario.addAction(smartGiftAction, smartGift);


        AssetService.createSmartGift(smartGift);
        log4j.info("gift was created succesfully. gift's title is:  " + smartGift.getTitle());

        return smartGift;

    }


}



