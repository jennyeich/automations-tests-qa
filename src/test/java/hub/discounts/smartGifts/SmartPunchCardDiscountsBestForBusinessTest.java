package hub.discounts.smartGifts;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartPunchCard;
import hub.common.objects.smarts.smarObjects.Scenario;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftActions;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.*;
import hub.discounts.BaseHub1Discount;
import hub.discounts.v4.GetBenefitsHelper;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.DiscountItems_ForEach;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Group;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.GroupField;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.GroupOperators;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.common.asset.ActionResponse;
import server.utils.RedeemErrorCodesMap;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 1/7/2018.
 */

public class SmartPunchCardDiscountsBestForBusinessTest extends BaseHub1SmartAssetBestForBusinessTest {

    public static SmartPunchCard smartPunchCard;
    private final String NUM_OF_PUNCHES = "5";

    @Test
    public void testSmartPunchCardDiscount33PercentWithGroup1() throws Exception {

        String assetKey = "";
        smartPunchCard = createSmartPunchCardWithDiscount("33",IS_LIMIT_OFF,"",NUM_OF_PUNCHES);

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send punch to member
        sendPunchAndPuch();

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 4000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "70000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-990", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartPunchCardDiscount20PercentNoGroup2() throws Exception {

        String assetKey = "";
        smartPunchCard = createSmartPunchCardWithDiscountNoGroup("20", IS_LIMIT_OFF, "",NUM_OF_PUNCHES);

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send punch to member
        sendPunchAndPuch();

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 4000, 1);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "7000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-1400", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @Test
    public void testSmartPunchCardDiscount25PercentWithGroup4LimitOffer3() throws Exception {

        String assetKey = "";

        smartPunchCard = createSmartPunchCardWithDiscountsWithOfferLimit ("25",IS_LIMIT_CHECKED, "4",NUM_OF_PUNCHES);
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send punch to member
        sendPunchAndPuch();

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 60000, 20);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 60000, 15);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "120000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-18000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    @Category(Hub1Regression.class)
    public void testSmartPunchCardDiscountAddDealCodes4() throws Exception {

        String assetKey = "";
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        smartPunchCard = GetBenefitsHelper.createSmartPunchCardWithAddDealCode(dealCodesToCreate,timestamp.toString(),false,NUM_OF_PUNCHES);
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send punch to member
        sendPunchAndPuch();
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> dealCodes = getRedeemAssetWithItemsArrayGetDealCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2), "15000");
        verifyTheDealCodesresponse(dealCodesToCreate, dealCodes);
    }

    @Test
    public void testSmartPunchCardDiscountAddDealCodesWithCondition5() throws Exception {

        String assetKey = "";
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        smartPunchCard = GetBenefitsHelper.createSmartPunchCardWithAddDealCodeWithCondition(Lists.newArrayList(dealCodesToCreate),timestamp.toString(),NUM_OF_PUNCHES);

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send punch to member
        sendPunchAndPuch();

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> dealCodes = getRedeemAssetWithItemsArrayGetDealCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2), "15000");

        verifyTheDealCodesresponse(dealCodesToCreate, dealCodes);
    }
    @Test
    public void testSmartPunchCardDiscountAddDealCodesBestforBusiness6() throws Exception {

        String assetKey = "";
        ArrayList<String> dealCodesToCreate = Lists.newArrayList("222334","553344");
        smartPunchCard = GetBenefitsHelper.createSmartPunchCardWithAddDealCodeWithConditionWithBasket(Lists.newArrayList(dealCodesToCreate),timestamp.toString(),NUM_OF_PUNCHES);

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send punch to member
        sendPunchAndPuch();
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);
        Item item3 = createItem(ITEM_CODE_333, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> dealCodes = getRedeemAssetWithItemsArrayGetDealCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2,item3), "27000");
        verifyTheDealCodesresponse(dealCodesToCreate, dealCodes);
    }


    @Test
    public void testSmartPunchCardDiscountAddItemCodes7() throws Exception {

        String assetKey = "";
        ArrayList<String> itemCodesToCreate = Lists.newArrayList("222334","553344");
        smartPunchCard = GetBenefitsHelper.createSmartPunchCardWithAddItemCode(itemCodesToCreate,timestamp.toString(),false,NUM_OF_PUNCHES);

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        sendPunchAndPuch();

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);
        Item item3 = createItem(ITEM_CODE_333, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> itemCodes = getRedeemAssetWithItemsArrayGetItemCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2, item3), "15000");
        checkRedeemResults(itemCodesToCreate, itemCodes);

    }



    @Test
    public void testSmartPunchCardDiscountAddItemCodesWithCondition8() throws Exception {

        String assetKey = "";
        ArrayList<String> itemCodesToCreate = Lists.newArrayList("222334","553344");
        smartPunchCard = GetBenefitsHelper.createSmartPunchCardWithAddItemCodeWithCondition(Lists.newArrayList(itemCodesToCreate),timestamp.toString(),NUM_OF_PUNCHES);

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send punch to member
        sendPunchAndPuch();
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> itemCodes = getRedeemAssetWithItemsArrayGetItemCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2), "15000");
        checkRedeemResults(itemCodesToCreate, itemCodes);
    }

    @Test
    public void testSmartPunchCardDiscountaddItemCodesWithConditionWithBasket9() throws Exception {

        String assetKey = "";
        ArrayList<String> itemCodesToCreate = Lists.newArrayList("222334","553344");
        smartPunchCard = GetBenefitsHelper.createSmartPunchCardWithAddItemCodeWithConditionWithBasket(Lists.newArrayList(itemCodesToCreate),timestamp.toString(),NUM_OF_PUNCHES);

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send punch to member
        sendPunchAndPuch();
        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);
        Item item3 = createItem(ITEM_CODE_333, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        List<ActionResponse> itemCodes = getRedeemAssetWithItemsArrayGetItemCodes(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item, item2,item3), "27000");
        checkRedeemResults(itemCodesToCreate, itemCodes);
    }

    /**
     * New Price per Item - Buy 3 Item A, get each A for New Price of $5
     Configuration: set a Gift with the following settings:
     Action: Add Discount -  New Price $5.00 [per item]
     Apply Discount on Specific Items: Qty= 3 of Item A; Discount = True
     Buy 4 Item A, each $8.00. Verify that the total discount is $9.00.
     * */

    @Test
    @Category(Hub1Regression.class)
    public void testSmartPunchCardDiscountNEWPriceBuy3getEachOneForNewPrice10() throws Exception {

        String assetKey = "";
        smartPunchCard = createSmartPunchCardWithDiscountNewPriceDEPCODE111("15",IS_LIMIT_OFF,"",DISCOUNT_ON, "3", IS_LIMIT_OFFER_OFF, IS_LIMIT_OFFER_OFF,timestamp.toString(),NUM_OF_PUNCHES);

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send punch to member
        sendPunchAndPuch();

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod123", DEP_CODE_111, "111", 3200, 4);

        //Redeem the asset with asset's key
        String discountSum = redeemAssetWithItemsArray(newMember.getPhoneNumber(), assetKey, Lists.newArrayList(item), "2400");
        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-900", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @Test
    @Category(Hub1Regression.class)
    public void testSmartPunchCardDiscountAmountOff50WithGroupWith2DifferentItemsItemsNoLimitOffer11() throws Exception {

        String assetKey = "";

        smartPunchCard = createSmartPunchCardWithDiscountAmountOff2Buskets("50",IS_LIMIT_OFFER_OFF, "",DISCOUNT_ON,DISCOUNT_ON,NUM_OF_PUNCHES);
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send punch to member
        sendPunchAndPuch();

        //Get gift's asset key
        assetKey = getMemberDetails(newMember.getPhoneNumber());

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 0, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);

        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "12000");

        //Check discount's sum
        Assert.assertEquals("Violation of asset is expected ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION), discountSum);
        log4j.info("Violation of asset is expected : " + discountSum);

    }

    private SmartPunchCard createSmartPunchCardWithDiscountAmountOff2Buskets(String amountOffNumber, String isLimitOffer, String limitOfferNum,String discount1State,String discount2State,String numOfPunches) throws Exception {

        SmartPunchCard smartPunchCard = BaseHub1Discount.createSmartPunchCard(timestamp,numOfPunches);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartPunchCard);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartPunchCard, "discount description " + timestamp, DiscountTypeOperation.AmountOff.toString());

        //percent off smartGiftAction
        AmountOff amountOff = createAmountOffDiscount(smartPunchCard, amountOffNumber, DISCOUNTS_CHECK_BOX_ON, isLimitOffer, limitOfferNum);

        SetDiscountOnItems d = createSetDiscountOnItem(smartPunchCard, DiscountItems_ForEach.quantity.toString(), "2", discount1State, amountOff);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartPunchCard, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartPunchCard, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnItem(smartPunchCard, DiscountItems_ForEach.quantity.toString(), "3", discount2State, amountOff);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartPunchCard, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartPunchCard, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        createDiscount(smartPunchCard, smartGiftAction, scenario, amountOff, discount);

        return smartPunchCard;

    }

    private void sendPunchAndPuch() throws Exception {
        //Send punch to member
        performAnActionSendAsset(smartPunchCard.getTitle());
        //punch the punchcard
        performAnActionPunchAPunchCard(smartPunchCard.getTitle(),NUM_OF_PUNCHES);
    }

    public SmartPunchCard createSmartPunchCardWithDiscountNewPriceDEPCODE111(String newPriceNumber, String isLimit, String limitNum,String firstDiscountONOFF, String numOfItems, String isLimitOffer, String limitOfferTimes,String timestamp,String numOfPunches) throws Exception {

        SmartPunchCard smartPunchCard = BaseHub1Discount.createSmartPunchCard(timestamp,numOfPunches);
        Scenario scenario = BaseHub1Discount.createSmartGiftScenario(smartPunchCard);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartPunchCard, "discount description " + timestamp, DiscountTypeOperation.NewPrice.toString());

        //percent off smartGiftAction
        NewPrice newPrice = BaseHub1Discount.createNewPriceDiscount(smartPunchCard, newPriceNumber, isLimit, limitNum, DISCOUNTS_CHECK_BOX_ON, isLimitOffer, limitOfferTimes);

        SetDiscountOnItems d =  createSetDiscountOnItem(smartPunchCard, DiscountItems_ForEach.quantity.toString(), numOfItems, firstDiscountONOFF, newPrice);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartPunchCard, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartPunchCard, GroupField.DepartmentCode, GroupOperators.Equals.toString(), DEP_CODE_111, group);
        createDiscount(smartPunchCard, smartGiftAction, scenario, newPrice, discount);

        return smartPunchCard;

    }

    private SmartPunchCard createSmartPunchCardWithDiscount(String percentOffNumber, String isLimit, String limitNum,String numOfPunches) throws Exception {


        smartPunchCard = createSmartPunchCard(timestamp,numOfPunches);
        Scenario scenario = createSmartGiftScenario(smartPunchCard);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartPunchCard, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());

        //percent off smartGiftAction
        PercentOff percentOff = createPercentOffDiscount(smartPunchCard, percentOffNumber, isLimit, limitNum, DISCOUNTS_CHECK_BOX_ON, "", "");

        SetDiscountOnItems d = createSetDiscountOnItem(smartPunchCard, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_ON, percentOff);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartPunchCard, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartPunchCard, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnItem(smartPunchCard, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_OFF, percentOff);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartPunchCard, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartPunchCard, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        createDiscount(smartPunchCard, smartGiftAction, scenario, percentOff, discount);

        return smartPunchCard;

    }

    public SmartPunchCard createSmartPunchCardWithDiscountNoGroup (String percentOffNumber, String isLimit, String limitNum,String numOfPunches) throws Exception{


        smartPunchCard = createSmartPunchCard(timestamp,numOfPunches);
        Scenario scenario = createSmartGiftScenario(smartPunchCard);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartPunchCard, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());

        //percent off smartGiftAction
        PercentOff percentOff = createPercentOffDiscount(smartPunchCard, percentOffNumber, isLimit, limitNum, DISCOUNTS_CHECK_BOX_OFF, "", "");
        createDiscount(smartPunchCard, smartGiftAction, scenario, percentOff, discount);

        return smartPunchCard;
    }


    public SmartPunchCard createSmartPunchCardWithDiscountsWithOfferLimit (String discountNum, String isLimitOffer, String limitOfferNum,String numOfPunches) throws Exception{

        smartPunchCard = createSmartPunchCard(timestamp,numOfPunches);
        Scenario scenario = createSmartGiftScenario(smartPunchCard);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartPunchCard, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());

        //percent off smartGiftAction
        PercentOff percentOff = createPercentOffDiscount(smartPunchCard, discountNum, IS_LIMIT_OFF, "", DISCOUNTS_CHECK_BOX_ON, isLimitOffer, limitOfferNum);

        SetDiscountOnItems d = createSetDiscountOnTimes(smartPunchCard, DiscountItems_ForEach.quantity.toString(), "2", "");
        percentOff.addSetDiscountOnItems(d, smartPunchCard);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartPunchCard, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartPunchCard, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnTimes(smartPunchCard, DiscountItems_ForEach.quantity.toString(), "3", DISCOUNT_ON);
        percentOff.addSetDiscountOnItems(d2, smartPunchCard);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartPunchCard, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartPunchCard, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        createDiscount(smartPunchCard, smartGiftAction, scenario, percentOff, discount);

        return smartPunchCard;
    }


}

