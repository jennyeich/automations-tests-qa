package hub.discounts.smartGifts;

import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.smarObjects.Scenario;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.*;
import hub.discounts.BaseHub1Discount;
import hub.discounts.DiscountsConstants;
import hub.services.assets.AssetService;
import org.junit.BeforeClass;
import server.common.asset.actions.DiscountStrategy;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Goni on 7/18/2017.
 */

public class BaseHub1SmartAssetBestForCustomerTest extends BaseHub1Discount {

    public static SmartGift smartGift;

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();

        //update the server with best for customer
        Map<String, Object> props = new HashMap<>();
        props.put(DiscountsConstants.DEFAULT_DISCOUNT_STRATEGY,  DiscountStrategy.BEST_FOR_CUSTOMER.strategy());
        props.put(DiscountsConstants.DETAILED_REDEEM_RESPONSE, true);
        serverHelper.saveApiClient(props);

    }

    public static PercentOff createPercentOffDiscount(SmartGift gift, String amountOffNumber, String setLimitAmount, String setAmountNumberLimit, String setDiscountCheckBox, String setDiscountsLimitOffer, String setDiscountsLimitOfferTimes) {

        PercentOff percentOff = new PercentOff(gift);

        if (!amountOffNumber.equals("")) {

            AmountOffNumber amountOffNumber1 = new AmountOffNumber(gift);
            amountOffNumber1.setAmountOffNumber(amountOffNumber);
            percentOff.setAmountOffNumber(amountOffNumber1);
        }

        if (!setLimitAmount.equals("")) {

            LimitAmount limitAmount = new LimitAmount(gift);
            limitAmount.setLimitAMount(setLimitAmount);
            if (!setAmountNumberLimit.equals(""))
                limitAmount.setAmountNumberLimit(setAmountNumberLimit);
            percentOff.setLimitAmount(limitAmount);
        }

        if (setDiscountCheckBox.equals(DISCOUNTS_CHECK_BOX_ON)) {

            SetDiscountOnItemsCheckBox checkBox = new SetDiscountOnItemsCheckBox(gift);
            percentOff.setSetDiscountOnItemsCheckBox(checkBox);

            //Set Limit Offer
            if (!setDiscountsLimitOffer.equals("")) {
                checkBox.setDiscountsLimitOffer(setDiscountsLimitOffer);
                if (!setDiscountsLimitOfferTimes.equals("")) {
                    LimitOfferLimitedToTimes limitOfferTimes = new LimitOfferLimitedToTimes();
                    limitOfferTimes.setDiscountsLimitOfferTimes(setDiscountsLimitOfferTimes, gift);
                    percentOff.setLimitOfferLimitedToTimes(limitOfferTimes);
                }
            }
        }


        return percentOff;

    }

    public static void createDiscount(SmartGift smartGift, SmartGiftAction smartGiftAction, Scenario scenario, PercentOff percentOff, AddDiscountAction discount) throws Exception {

        if (percentOff != null)
            discount.setPercentOff(percentOff);
        if (discount != null)
            smartGiftAction.setDiscount(discount);
        scenario.addAction(smartGiftAction, smartGift);

        AssetService.createSmartGift(smartGift);
        log4j.info("gift was created succesfully. gift's title is:  " + smartGift.getTitle());
    }


}

