package hub.discounts.smartClubDeals;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartClubDeal;
import hub.common.objects.smarts.smarObjects.Scenario;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftActions;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.AddDiscountAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.AmountOff;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.DiscountTypeOperation;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.SetDiscountOnItems;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.DiscountItems_ForEach;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Group;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.GroupField;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.GroupOperators;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.v2_8.common.Item;

/**
 * Created by Jenny on 12/4/2016.
 */

public class SmartClubDealAmountDiscountsBestForBusinessTest extends BaseHub1SmartClubDealBestForBusinessTest {




    @Test
    public void testSmartClubDealAmountOff50WithGroup1() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscount("50",IS_LIMIT_OFFER_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 90000, 3);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "100000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected",String.valueOf(EXPECT_NO_DISCOUNT), discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testSmartClubDealAmountOff50WithGroup2() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscount("50",IS_LIMIT_OFFER_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 20000, 2);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 60000, 2);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "80000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", String.valueOf(EXPECT_NO_DISCOUNT), discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartClubDealAmountOff50WithGroup3() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscount("50",IS_LIMIT_OFFER_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 20000, 2);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 90000, 3);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "110000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-5000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartClubDealAmountOff50WithGroup4() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscount("50",IS_LIMIT_OFFER_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 50000, 5);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 90000, 3);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "140000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-5000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartClubDealAmountOff50WithGroup5() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscount("50",IS_LIMIT_OFFER_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 20000, 2);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 240000, 8);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "260000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-5000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartClubDealAmountOff80WithGroup6() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscount("80",IS_LIMIT_OFFER_CHECKED,"1");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 50000, 5);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 90000, 3);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "140000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-8000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartClubDealAmountOff80WithGroup7() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscount("80",IS_LIMIT_OFFER_CHECKED,"1");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 20000, 2);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 240000, 8);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "260000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-8000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartClubDealAmountOffBestForBusiness80WithGroup8() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscountBothBusketsEnabled("80",IS_LIMIT_OFFER_CHECKED,"4");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 200000, 20);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 450000, 15);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "650000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-32000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartClubDealAmountOffBestForBusiness80WithGroup9() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscountBothBusketsEnabled("80",IS_LIMIT_OFFER_CHECKED,"4");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 70000, 7);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 210000, 7);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "280000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-16000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartClubDealAmountOffBestForBusiness120WithGroup10() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscountBothBusketsEnabledBuslet1Busket3("120",IS_LIMIT_OFFER_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 120000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 450000, 15);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "570000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-12000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }



    @Test
    public void testSmartClubDealAmountOffBestForBusiness120WithGroup11() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscountBothBusketsEnabledBusket4Busket3("120",IS_LIMIT_OFFER_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 1080000, 9);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 450000, 15);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "1530000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-24000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartClubDealAmountOff50WithGroup12() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscount("50",IS_LIMIT_OFFER_OFF,"",DISCOUNT_ON,DISCOUNT_ON);
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 0, 2);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 240000, 8);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "240000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", String.valueOf(EXPECT_NO_DISCOUNT), discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartClubDealAmountOff50WithGroup13() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscount("50",IS_LIMIT_OFFER_OFF,"",DISCOUNT_OFF,DISCOUNT_ON);
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 0, 2);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 240000, 8);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "240000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-5000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    public SmartClubDeal createSmartClubDealWithDiscount(String amountOffNumber, String isLimitOffer, String limitOfferNum) throws Exception {

        return createSmartClubDealWithDiscount(amountOffNumber,isLimitOffer,limitOfferNum,DISCOUNT_ON,DISCOUNT_ON);

    }

    public SmartClubDeal createSmartClubDealWithDiscount(String amountOffNumber, String isLimitOffer, String limitOfferNum,String discount1State,String discount2State) throws Exception {

        smartClubDeal = createSmartClubDeal(timestamp);
        Scenario scenario = createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.AmountOff.toString());

        //percent off smartGiftAction
        AmountOff amountOff = createAmountOffDiscount(smartClubDeal, amountOffNumber, DISCOUNTS_CHECK_BOX_ON, isLimitOffer,limitOfferNum);

        SetDiscountOnItems d = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "2", discount1State, amountOff);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "3", discount2State, amountOff);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        createDiscount(smartClubDeal, smartGiftAction, scenario, amountOff, discount);

        return smartClubDeal;

    }


    public SmartClubDeal createSmartClubDealWithDiscountBothBusketsEnabled(String amountOffNumber, String isLimitOffer, String limitOfferNum) throws Exception {

        smartClubDeal = createSmartClubDeal(timestamp);
        Scenario scenario = createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.AmountOff.toString());

        //percent off smartGiftAction
        AmountOff amountOff = createAmountOffDiscount(smartClubDeal, amountOffNumber, DISCOUNTS_CHECK_BOX_ON, isLimitOffer, limitOfferNum);

        SetDiscountOnItems d = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "2", DISCOUNT_ON, amountOff);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "3", DISCOUNT_ON, amountOff);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        createDiscount(smartClubDeal, smartGiftAction, scenario, amountOff, discount);

        return smartClubDeal;

    }

    public SmartClubDeal createSmartClubDealWithDiscountBothBusketsEnabledBusket4Busket3(String amountOffNumber, String isLimitOffer, String limitOfferNum) throws Exception {

        smartClubDeal = createSmartClubDeal(timestamp);
        Scenario scenario = createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.AmountOff.toString());

        //percent off smartGiftAction
        AmountOff amountOff = createAmountOffDiscount(smartClubDeal, amountOffNumber, DISCOUNTS_CHECK_BOX_ON, isLimitOffer, limitOfferNum);

        SetDiscountOnItems d = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "4",DISCOUNT_ON, amountOff);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "3", DISCOUNT_ON, amountOff);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        createDiscount(smartClubDeal, smartGiftAction, scenario, amountOff, discount);

        return smartClubDeal;

    }

    public SmartClubDeal createSmartClubDealWithDiscountBothBusketsEnabledBuslet1Busket3(String amountOffNumber, String isLimitOffer, String limitOfferNum) throws Exception {

        smartClubDeal = createSmartClubDeal(timestamp);
        Scenario scenario = createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.AmountOff.toString());

        //percent off smartGiftAction
        AmountOff amountOff = createAmountOffDiscount(smartClubDeal, amountOffNumber, DISCOUNTS_CHECK_BOX_ON, isLimitOffer, limitOfferNum);

        SetDiscountOnItems d = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_ON, amountOff);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "3", DISCOUNT_ON, amountOff);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        createDiscount(smartClubDeal, smartGiftAction, scenario, amountOff, discount);

        return smartClubDeal;

    }

}



