package hub.discounts.smartClubDeals;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartClubDeal;
import hub.common.objects.smarts.smarObjects.Scenario;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftActions;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.*;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.DiscountItems_ForEach;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Group;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.GroupField;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.GroupOperators;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.v2_8.common.Item;

/**
 * Created by Jenny on 17/5/2017.
 */

public class SmartClubDealPercentDiscountsBestForBusinessTest extends BaseHub1SmartClubDealBestForBusinessTest {


    @Test
    public void testSmartClubDeal33PercentWithGroupBestForBusiness1() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscount("33",IS_LIMIT_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 4000, 1);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "7000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-990", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartClubDeal20PercentWithoutGroupNoLimitBestForBusiness2() throws Exception {

        //create smart club deal
        smartClubDeal = createSmartClubDealWithDiscountNoGroup("20",IS_LIMIT_OFF,"");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 4000, 1);

        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "7000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-1400", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void  testSmartClubDeal0PercentWithoutGroupNoLimitBestForBusiness3() throws Exception {

        //create smart club deal
        smartClubDeal = createSmartClubDealWithDiscountNoGroup("0",IS_LIMIT_OFF,"");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 4000, 1);

        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "7000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected",String.valueOf(EXPECT_NO_DISCOUNT), discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void  testSmartClubDeal100PercentWithoutGroupNoLimitBestForBusiness4() throws Exception {

        //create smart club deal
        smartClubDeal = createSmartClubDealWithDiscountNoGroup("100",IS_LIMIT_OFF,"");

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 4000, 1);

        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "7000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-7000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }



    @Test
    public void  testSmartClubDeal50PercentWithoutGroup200LimitBestForBusiness5() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscountNoGroup("50",IS_LIMIT_CHECKED,"200");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 15000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 25000, 1);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "40000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-20000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testSmartClubDeal50PercentWithoutGroup100LimitBestForBusiness6() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscountNoGroup("50",IS_LIMIT_CHECKED,"100");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 15000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 25000, 1);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "40000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-10000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartClubDeal50PercentWithoutGroup300LimitBestForBusiness7() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscountNoGroup("50",IS_LIMIT_CHECKED,"300");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 25000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 15000, 1);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "40000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-20000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartClubDeal0PercentWithoutGroup0LimitBestForBusiness8() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscountNoGroup("0",IS_LIMIT_CHECKED,"0");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 25000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 15000, 1);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "40000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", String.valueOf(EXPECT_NO_DISCOUNT), discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartClubDeal0PercentWithoutGroup100LimitBestForBusiness9() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscountNoGroup("0",IS_LIMIT_CHECKED,"100");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 25000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 15000, 1);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "40000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", String.valueOf(EXPECT_NO_DISCOUNT), discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartClubDeal100PercentWithoutGroup100LimitBestForBusiness10() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscountNoGroup("100",IS_LIMIT_CHECKED,"100");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 25000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 15000, 1);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "40000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-10000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartClubDeal100PercentWithoutGroup500LimitBestForBusiness11() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscountNoGroup("100",IS_LIMIT_CHECKED,"500");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 25000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 15000, 1);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "40000",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-40000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartClubDeal40PercentWithoutLimitOfer3Buskets2disabledBestForBusiness11() throws Exception {

        smartClubDeal = createSmartGiftWithDiscountsWithOfferLimit3discountsTwoAreOff("40",IS_LIMIT_OFFER_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem("668773", "Prod1", "123", "Dep1", 1200, 1);
        Item item2 = createItem("821172", "Prod1", "123", "Dep1", 1700, 1);
        String discountSum = getMemberBenefits(newMember.getPhoneNumber(), "2900",item, item2);

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", String.valueOf(EXPECT_NO_DISCOUNT), discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartClubDeal40PercentWithout3Buskets2disabledBestForBusiness12() throws Exception {

        smartClubDeal = createSmartGiftWithDiscountsWithOfferLimit3discountsTwoAreOff("40",IS_LIMIT_OFFER_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem("668773", "Prod1", "123", "Dep1", 2400, 2);
        Item item2 = createItem("821172", "Prod1", "123", "Dep1", 3400, 2);
        Item item3 = createItem("325242", "Prod1", "123", "Dep1", 3000, 2);

        String discountSum = getMemberBenefitsWithitemsList(newMember.getPhoneNumber(), "8800",Lists.newArrayList(item,item2,item3));

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", String.valueOf(EXPECT_NO_DISCOUNT), discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartClubDeal40PercentWithout3Buskets2disabledBestForBusiness13() throws Exception {

        smartClubDeal = createSmartGiftWithDiscountsWithOfferLimit3discountsTwoAreOff("40",IS_LIMIT_OFFER_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem("668773", "Prod1", "123", "Dep1", 2400, 2);
        Item item2 = createItem("821172", "Prod1", "123", "Dep1", 5100, 2);
        Item item3 = createItem("325242", "Prod1", "123", "Dep1", 3000, 3);

        String discountSum = getMemberBenefitsWithitemsList(newMember.getPhoneNumber(), "10500",Lists.newArrayList(item,item2,item3));

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-960", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }



    @Test
    public void testSmartClubDeal40PercentWithout3Buskets2disabledBestForBusiness14() throws Exception {

        smartClubDeal = createSmartGiftWithDiscountsWithOfferLimit3discountsTwoAreOff("40",IS_LIMIT_OFFER_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem("668773", "Prod1", "123", "Dep1", 4800, 4);
        Item item2 = createItem("821172", "Prod1", "123", "Dep1", 8500, 5);
        Item item3 = createItem("325242", "Prod1", "123", "Dep1", 3000, 4);

        String discountSum = getMemberBenefitsWithitemsList(newMember.getPhoneNumber(), "16300",Lists.newArrayList(item,item2,item3));

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-960", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartClubDeal40PercentWithout3Buskets2disabledBestForBusiness15() throws Exception {

        smartClubDeal = createSmartGiftWithDiscountsWithOfferLimit3discountsTwoAreOff("40",IS_LIMIT_OFFER_OFF,"");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem("668773", "Prod1", "123", "Dep1", 4800, 4);
        Item item2 = createItem("821172", "Prod1", "123", "Dep1", 8500, 5);
        Item item3 = createItem("325242", "Prod1", "123", "Dep1", 3000, 7);

        String discountSum = getMemberBenefitsWithitemsList(newMember.getPhoneNumber(), "16300",Lists.newArrayList(item,item2,item3));

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-1920", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartClubDeal25PercentWith2EnabledBusketsNoLimitBestForBusiness16() throws Exception {

        smartClubDeal = createSmartGiftWithDiscountsWithOfferLimit("25",IS_LIMIT_OFFER_CHECKED,"1");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 15000, 5);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 12000, 3);

        String discountSum = getMemberBenefitsWithitemsList(newMember.getPhoneNumber(), "27000",Lists.newArrayList(item,item2));

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-4500", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSmartClubDeal25PercentWith2EnabledBusketsNoLimitBestForBusiness17() throws Exception {

        smartClubDeal = createSmartGiftWithDiscountsWithOfferLimit("25",IS_LIMIT_OFFER_CHECKED,"1");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 6000, 2);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 32000, 8);

        String discountSum = getMemberBenefitsWithitemsList(newMember.getPhoneNumber(), "38000",Lists.newArrayList(item,item2));

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-4500", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartClubDeal25PercentWith2EnabledBusketsNoLimitBestForBusiness18() throws Exception {

        smartClubDeal = createSmartGiftWithDiscountsWithOfferLimit("25",IS_LIMIT_OFFER_CHECKED,"1");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 21000, 7);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 40000, 10);

        String discountSum = getMemberBenefitsWithitemsList(newMember.getPhoneNumber(), "61000",Lists.newArrayList(item,item2));

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-4500", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    public SmartClubDeal createSmartClubDealWithDiscount(String percentOffNumber, String isLimit, String limitNum) throws Exception {

        smartClubDeal = createSmartClubDeal(timestamp);
        Scenario scenario = createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());

        //percent off smartGiftAction
        PercentOff percentOff = createPercentOffDiscount(smartClubDeal, percentOffNumber, isLimit, limitNum, DISCOUNTS_CHECK_BOX_ON, IS_LIMIT_OFFER_OFF, "");

        SetDiscountOnItems d = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_ON, percentOff);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_OFF, percentOff);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        createDiscount(smartClubDeal, smartGiftAction, scenario, percentOff, discount);

        return smartClubDeal;

    }

    public SmartClubDeal createSmartGiftWithDiscountsWithOfferLimit(String discountNum, String isOfferLimit, String limitOfferNum) throws Exception {

        smartClubDeal = createSmartClubDeal(timestamp);
        Scenario scenario = createSmartGiftScenario(smartClubDeal);


        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());

        //percent off smartGiftAction
        PercentOff percentOff = createPercentOffDiscount(smartClubDeal, discountNum, IS_LIMIT_OFF, "", DISCOUNTS_CHECK_BOX_ON,isOfferLimit, limitOfferNum);

        SetDiscountOnItems d = createSetDiscountOnTimes(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "2", DISCOUNT_ON);
        percentOff.addSetDiscountOnItems(d, smartClubDeal);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnTimes(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "3",DISCOUNT_ON);
        percentOff.addSetDiscountOnItems(d2, smartClubDeal);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        createDiscount(smartClubDeal, smartGiftAction, scenario, percentOff, discount);

        return smartClubDeal;
    }

    public SmartClubDeal createSmartClubDealWithDiscountNoGroup(String percentOffNumber, String isLimit, String limitNum) throws Exception {

        smartClubDeal = createSmartClubDeal(timestamp);
        Scenario scenario = createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());

        //percent off smartGiftAction
        PercentOff percentOff = createPercentOffDiscount(smartClubDeal, percentOffNumber, isLimit, limitNum, DISCOUNTS_CHECK_BOX_ON, IS_LIMIT_OFFER_OFF, "");
        createDiscount(smartClubDeal, smartGiftAction, scenario, percentOff, discount);

        return smartClubDeal;

    }

    public SmartClubDeal createSmartGiftWithDiscountsWithOfferLimit3discountsTwoAreOff(String discountNum, String isOfferLimit, String limitOfferNum) throws Exception {

        smartClubDeal = createSmartClubDeal(timestamp);
        Scenario scenario = createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());
        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.PercentOff.toString());
        //percent off smartGiftAction
        PercentOff percentOff = createPercentOffDiscount(smartClubDeal, discountNum, IS_LIMIT_OFF, "", DISCOUNTS_CHECK_BOX_ON, isOfferLimit, limitOfferNum);

        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal, createDiscountWithGroup(percentOff, DiscountItems_ForEach.quantity.toString(), "2", DISCOUNT_ON));
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Isoneof.toString(), "668773,525561", group);

        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, createDiscountWithGroup(percentOff, DiscountItems_ForEach.quantity.toString(), "3", DISCOUNT_OFF));
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Isoneof.toString(), "325242,526251", group2);

        Thread.sleep(1000);
        //Create new group3
        Group group3 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, createDiscountWithGroup(percentOff, DiscountItems_ForEach.spend.toString(), "2", DISCOUNT_OFF));
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Isoneof.toString(), "322122,821172", group3);

        createDiscount(smartClubDeal, smartGiftAction, scenario, percentOff, discount);
        return smartClubDeal;
    }



}



