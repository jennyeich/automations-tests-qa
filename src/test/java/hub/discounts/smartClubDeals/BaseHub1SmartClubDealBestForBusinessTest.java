package hub.discounts.smartClubDeals;

import hub.common.objects.smarts.SmartClubDeal;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.PercentOff;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.SetDiscountOnItems;
import hub.discounts.BaseHub1Discount;
import hub.services.member.MembersService;
import org.junit.After;
import org.junit.BeforeClass;
import server.common.asset.actions.DiscountStrategy;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

/**
 * Created by Goni on 7/18/2017.
 */

public class BaseHub1SmartClubDealBestForBusinessTest extends BaseHub1Discount {


    public static SmartClubDeal smartClubDeal;



    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();

        //update the server with best for business
        serverHelper.saveApiClient(DEFAULT_DISCOUNT_STRATEGY, DiscountStrategy.BEST_FOR_BUISNESS.strategy());
        serverHelper.saveApiClient(DETAILED_REDEEM_RESPONSE, true);
    }

    @After
    public void cleanAssets() throws Exception {
        MembersService.deactivateSmartClubDeals();
    }

    public SetDiscountOnItems createDiscountWithGroup(PercentOff percentOff, String discountType, String discountNum, String discountOffOn) {

        SetDiscountOnItems d = createDiscountWithGroup(discountType, discountNum, discountOffOn);
        percentOff.addSetDiscountOnItems(d, smartClubDeal);

        return d;
    }

    public SetDiscountOnItems createDiscountWithGroup(String discountType, String discountNum, String discountOffOn) {

        SetDiscountOnItems d = BaseHub1Discount.createSetDiscountOnTimes(smartClubDeal, discountType, discountNum, discountOffOn);
        return d;
    }

}

