package hub.discounts.smartClubDeals;

import hub.common.objects.smarts.SmartClubDeal;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.IDiscountType;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.SetDiscountOnItems;
import hub.discounts.BaseHub1Discount;
import hub.services.member.MembersService;
import org.junit.After;
import org.junit.BeforeClass;
import server.common.asset.actions.DiscountStrategy;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

/**
 * Created by Goni on 7/18/2017.
 */

public class BaseHub1SmartClubDealBestForCustomerTest extends BaseHub1Discount {

    public static SmartClubDeal smartClubDeal;


    /////////////////////////////////////////////
    //////////////// I  N  I  T  ////////////////
    /////////////////////////////////////////////
    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();

        //update the server with best for customer
        serverHelper.saveApiClient(DEFAULT_DISCOUNT_STRATEGY, DiscountStrategy.BEST_FOR_CUSTOMER.strategy());
        serverHelper.saveApiClient(DETAILED_REDEEM_RESPONSE, true);

    }

    public SetDiscountOnItems createDiscountWithGroup (IDiscountType iDiscountType, String discountType, String discountNum, String discountOffOn ){

        SetDiscountOnItems d = createDiscountWithGroup(discountType, discountNum , discountOffOn);
        iDiscountType.addSetDiscountOnItems(d, smartClubDeal);

        return d;
    }

    public SetDiscountOnItems createDiscountWithGroup(String discountType, String discountNum, String discountOffOn) {

        SetDiscountOnItems d = BaseHub1Discount.createSetDiscountOnTimes(smartClubDeal, discountType, discountNum, discountOffOn);
        return d;
    }

    @After
    public void cleanAssets() throws Exception {
        MembersService.deactivateSmartClubDeals();
    }
}

