package hub.discounts.smartClubDeals;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartClubDeal;
import hub.common.objects.smarts.smarObjects.Scenario;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftActions;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.AddDiscountAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.DiscountTypeOperation;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.FreeItem;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.SetDiscountOnItems;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.DiscountItems_ForEach;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Group;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.GroupField;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.GroupOperators;
import hub.discounts.DiscountsConstants;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.v2_8.common.Item;

/**
 * Created by Jenny on 21/5/2017.
 */

public class SmartClubDealFreeItemDiscountsBestForCustomerTest extends BaseHub1SmartClubDealBestForCustomerTest {


    @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroupBestForCusotmer1() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscount(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 10000, 1);
        String discountSum = getMemberBenefitsWithitemsList(newMember.getPhoneNumber(), "10000",Lists.newArrayList(item));

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", String.valueOf(EXPECT_NO_DISCOUNT), discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroupBestForCustomer2() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscount(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 90000, 3);

        String discountSum = getMemberBenefitsWithitemsList(newMember.getPhoneNumber(), "100000",Lists.newArrayList(item,item2));

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-10000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }


    @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroupBestForCustomer3() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscount(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 20000, 2);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 60000, 2);

        String discountSum = getMemberBenefitsWithitemsList(newMember.getPhoneNumber(), "80000",Lists.newArrayList(item,item2));

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-20000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

      @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroupBestForCustomer4() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscount(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 50000, 5);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 90000, 3);

        String discountSum = getMemberBenefitsWithitemsList(newMember.getPhoneNumber(), "140000",Lists.newArrayList(item,item2));

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-30000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }



    @Test
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroupBestForCustomer5() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscountWith12and1items(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 12000, 12);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 90000, 3);

        String discountSum = getMemberBenefitsWithitemsList(newMember.getPhoneNumber(), "102000",Lists.newArrayList(item,item2));

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-30000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testSmartClubDealFreeItemBestForBusinessNoLimitsWithGroupBestForCustomer6() throws Exception {

        smartClubDeal = createSmartClubDealWithDiscountWith12and1items(IS_LIMIT_OFF,"",IS_LIMIT_OFFER_OFF, "");
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        Item item = createItem(ITEM_CODE_111, "Prod1", "123", "Dep1", 240000, 24);
        Item item2 = createItem(ITEM_CODE_222, "Prod1", "123", "Dep1", 9000, 3);

        String discountSum = getMemberBenefitsWithitemsList(newMember.getPhoneNumber(), "33000",Lists.newArrayList(item,item2));

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-6000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }



    public SmartClubDeal createSmartClubDealWithDiscount(String isLimit, String limitNum, String isLimitOffer, String limiitOfferNum) throws Exception {

        smartClubDeal = createSmartClubDeal(timestamp);
        Scenario scenario = createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.FreeItem.toString());

        //percent off smartGiftAction
        FreeItem freeItem = createFreeItemDiscount(smartClubDeal, isLimit, limitNum, DiscountsConstants.DISCOUNTS_CHECK_BOX_ON, isLimitOffer, limiitOfferNum);

        SetDiscountOnItems d = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_ON, freeItem);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_OFF, freeItem);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        createDiscount(smartClubDeal, smartGiftAction, scenario, freeItem, discount);

        return smartClubDeal;

    }

    public SmartClubDeal createSmartClubDealWithDiscountWith12and1items(String isLimit, String limitNum, String isLimitOffer, String limiitOfferNum) throws Exception {

        smartClubDeal = createSmartClubDeal(timestamp);
        Scenario scenario = createSmartGiftScenario(smartClubDeal);

        //Create smartGift action
        SmartGiftAction smartGiftAction = createSmartGiftAction(SmartGiftActions.AddDiscount.toString());

        //create discount smartGiftAction
        AddDiscountAction discount = createAddDiscountAction(smartClubDeal, "discount description " + timestamp, DiscountTypeOperation.FreeItem.toString());

        //percent off smartGiftAction
        FreeItem freeItem = createFreeItemDiscount(smartClubDeal, isLimit, limitNum, DiscountsConstants.DISCOUNTS_CHECK_BOX_ON, isLimitOffer, limiitOfferNum);

        SetDiscountOnItems d = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "12", DISCOUNT_OFF, freeItem);
        //Create new group
        Group group = createGroupWithConds(timestamp, smartClubDeal, d);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_111, group);

        SetDiscountOnItems d2 = createSetDiscountOnItem(smartClubDeal, DiscountItems_ForEach.quantity.toString(), "1", DISCOUNT_ON, freeItem);
        //Create new group
        Group group2 = createGroupWithConds(String.valueOf(System.currentTimeMillis()), smartClubDeal, d2);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(smartClubDeal, GroupField.ItemCode, GroupOperators.Equals.toString(), ITEM_CODE_222, group2);
        createDiscount(smartClubDeal, smartGiftAction, scenario, freeItem, discount);

        return smartClubDeal;

    }


}



