package hub.smartAutomations.actions;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.TextAreaParameters;
import hub.common.objects.common.TimingUnit;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.services.automations.SmartAutomationService;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.utils.ServerHelper;
import server.utils.UserActionsConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 6/20/2017.
 *
 * This class test using trigger 'Join the Club' to test all available uses of the 'push notification' action in the smart automation
 */
public class PushNotificationActionTest extends BaseSmartActionTest{

    private static String textMessage = "";


    @Override
    public String getAutomationName() {
        return "PushNotificationAction_" + timestamp;
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "sendPush_" +timestamp;
        }
    };


    @Test
    public void testActionSendPushNotificationDelay1SecNoTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList();
        String delay = "1";
        textMessage = "@" + TextAreaParameters.Membership_FirstName + " hello";
        buildScenarioWithoutOccurances(automation, SmartAutomationActions.SendAPushNotification,tags,delay, TimingUnit.SECONDS);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);
        createNewMember();

        checkAutomationRunAndSendPushNotification(tags);

        checkTimingDefinition(delay);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testActionSendPushNotificationDelay2SecTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList("tag1",timestamp.toString());
        String delay = "2";
        textMessage = "@" + TextAreaParameters.Membership_FirstName +" @" + TextAreaParameters.Membership_LastName + " hello";
        buildScenarioWithoutOccurances(automation, SmartAutomationActions.SendAPushNotification,tags,delay, TimingUnit.SECONDS);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);
        createNewMember();

        checkAutomationRunAndSendPushNotification(tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionSendPushNotificationNoDelayTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList("tag1",timestamp.toString());
        String delay = null;
        textMessage = "@" + TextAreaParameters.Membership_FirstName +" hello. You have @" + TextAreaParameters.Membership_Points + " points.";
        buildScenarioWithoutOccurances(automation, SmartAutomationActions.SendAPushNotification,tags,delay, TimingUnit.SECONDS);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);
        createNewMember();

        checkAutomationRunAndSendPushNotification(tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionPushMemberDelay1HourCheckNotPerform() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList("tag1",timestamp.toString());
        String delay = "1";
        textMessage = "@" + TextAreaParameters.Membership_FirstName +" hello.";
        buildScenarioWithoutOccurances(automation, SmartAutomationActions.SendAPushNotification,tags,delay,  TimingUnit.HOURS);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);
        createNewMember();


        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(PUSH_OPERATION);

        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.HOURS));
    }


    private void checkAutomationRunAndSendPushNotification(ArrayList<String> tags) throws Exception {

        log4j.info("check Data Store Updated with push action");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find UserAction with the automation: " + automationRuleId,
                buildQuery(UserActionsConstants.PUSH_FAILED,predicate) , 1, dataStore);
        Assert.assertEquals("Must find one UserAction with the automation: " + automationRuleId, 1, res.size());

        checkActionTags(tags, res);
    }

    @Override
    protected ISmartAction getConcreteAction(SmartAutomationAction action) {
        return action.getPushNotification();
    }


    @Override
    protected SmartAutomationAction getAction(String actionName, SmartAutomationAction smartAction, ISmartAutomationCounter automation) throws Exception{
        SmartAutomationAction action = hubAssetsCreator.getSendPushNotificationAction(smartAction, automation,textMessage,"Y");
        return action;
    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Finish test : " + PushNotificationActionTest.class.getName());
        try {
            checkSmartAutomationsAfterTestsFinished();
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
    }
}
