package hub.smartAutomations.actions;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.TextAreaParameters;
import hub.common.objects.common.TimingUnit;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.services.automations.SmartAutomationService;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.utils.ServerHelper;
import server.utils.UserActionsConstants;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 6/21/2017.
 *
 * This class test using trigger 'Join the Club' to test all available uses of the 'push notification' action in the smart automation
 */
public class SendSMSActionTest  extends BaseSmartActionTest{

    private static String textMessage = "";


    @Override
    public String getAutomationName() {
        return "SendSMSAction_" + timestamp;
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "sendSMS_" +timestamp;
        }
    };


    @Test
    @Category(Hub1Regression.class)
    public void testActionSendSMSDelay55SecNoTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList();
        String delay = "55";
        textMessage = "@" + TextAreaParameters.Membership_FirstName + " welcome";
        buildScenarioWithoutOccurances(automation, SmartAutomationActions.SendTextSMS,tags,delay, TimingUnit.SECONDS);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);
        createNewMember();

        //wait 1 min delay
        Thread.sleep(60000);
        checkAutomationRunAndSendMemberSMS(tags);
        checkTimingDefinition(delay);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testActionSendSMSDelay1MinTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList("tag1",timestamp.toString());
        String delay = "1";
        textMessage = "@" + TextAreaParameters.Membership_FirstName +" @" + TextAreaParameters.Membership_LastName + " hello";
        buildScenarioWithoutOccurances(automation, SmartAutomationActions.SendTextSMS,tags,delay, TimingUnit.MINUTES);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);
        createNewMember();

        //wait 1 min delay
        Thread.sleep(60000);
        checkAutomationRunAndSendMemberSMS(tags);

        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.MINUTES));
    }

    @Test
    @Category(Hub1Regression.class)
    public void testActionSendSMS2MinDelayTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList("tag1",timestamp.toString());
        String delay = "2";
        textMessage = "@" + TextAreaParameters.Membership_FirstName +" welcome. You have @" + TextAreaParameters.Membership_Points + " points.";
        buildScenarioWithoutOccurances(automation, SmartAutomationActions.SendTextSMS,tags,delay, TimingUnit.MINUTES);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);
        createNewMember();

        //wait 1 min delay
        Thread.sleep(60000);
        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(MEMBER_SMS_OPERATION);
        //wait 1 more min delay - check now it run
        Thread.sleep(60000);
        checkAutomationRunAndSendMemberSMS(tags);

        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.MINUTES));
    }

    @Test
    public void testActionPushMemberDelay1HourCheckNotPerform() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList("tag1",timestamp.toString());
        String delay = "1";
        textMessage = "@" + TextAreaParameters.Membership_FirstName +" hello.";
        buildScenarioWithoutOccurances(automation, SmartAutomationActions.SendTextSMS,tags,delay,  TimingUnit.HOURS);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);
        createNewMember();


        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(MEMBER_SMS_OPERATION);

        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.HOURS));
    }




    private void checkAutomationRunAndSendMemberSMS(ArrayList<String> tags) throws Exception {

        log4j.info("check Data Store Updated with send member sms action");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWaitUntilResultEqOrGreat("Must find at least one UserAction with the RuleID: " + automationRuleId,
                buildQuery(UserActionsConstants.MEMBER_SMS,predicate), 1, dataStore,200000);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info("ExportData contains the following data : " + t.getValue());
        System.out.println("Data: " + t.getValue());
        checkActionTags(tags, res);
    }


    @Override
    protected ISmartAction getConcreteAction(SmartAutomationAction action) {
        return action.getSendSMS();
    }


    @Override
    protected SmartAutomationAction getAction(String actionName, SmartAutomationAction smartAction, ISmartAutomationCounter automation) throws Exception{
        SmartAutomationAction action = hubAssetsCreator.getSendSMSAction(smartAction, automation,textMessage);
        return action;
    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Finish test : " + SendSMSActionTest.class.getName());
        try {
            checkSmartAutomationsAfterTestsFinished();
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
    }
}
