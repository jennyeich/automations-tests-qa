package hub.smartAutomations.actions;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.TimingUnit;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.services.member.MembersService;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.utils.ServerHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 5/22/2017.
 *
 * This class test using trigger 'Receives An Asset' to test all available uses of the 'Punch the punch card' action in the smart automation
 */
public class PunchThePunchCardActionTest extends BaseSmartActionTest{


    @Override
    public Triggers getTrigger() {
        return Triggers.ReceivesAnAsset;
    }

    @Override
    public String getAutomationName() {
        return "PunchAction_" + timestamp;
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "punch_" +timestamp;
            if(smartPunchCard == null) {
                //create assets
                try {
                    smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");

                } catch (Exception e) {
                    log4j.error("error while creating asset",e);
                }
            }
        }
    };


    @Test
    public void testActionPunchThePunchCardOccurences10Exceeded() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList("tag1",tag);
        String occurences = "10";
        String delay = null;
        createSmartAutomationScenario(automation, SmartAutomationActions.PunchThePunchCard,occurences,tags,delay);
        //Create member
        createNewMember();
        //send the punch card so it will activate the automation to punch it
        performAnActionSendPunchCard(smartPunchCard);

        int punchNumOfPunches = Integer.valueOf(smartPunchCard.getNumOfPunches()).intValue();
        int punchesToPunch = Integer.valueOf(occurences).intValue();
        if(punchNumOfPunches < punchesToPunch){
            checkAutomationRunAndPunchTheCard(smartPunchCard.getNumOfPunches(),tags);
            //check that a user action exceededPunch was creates as well
            checkDataStoreUpdatedWithExceededPunch( String.valueOf(punchesToPunch-punchNumOfPunches));
        }else {
            checkAutomationRunAndPunchTheCard(occurences, tags);
        }

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionPunchThePunchCardOccurences3Delay10secTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList("tag1",tag);
        String occurences = "3";
        String delay = "10";
        createSmartAutomationScenario(automation, SmartAutomationActions.PunchThePunchCard,occurences,tags,delay);
        //Create member
        createNewMember();
        //send the punch card so it will activate the automation to punch it
        performAnActionSendPunchCard(smartPunchCard);

        checkAutomationRunAndPunchTheCard(occurences,tags);

        checkTimingDefinition(delay);
    }


    @Test
    public void testActionPunchThePunchCardOccurences2Delay5SecNoTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList();
        String occurences = "2";
        String delay = "5";

        createSmartAutomationScenario(automation, SmartAutomationActions.PunchThePunchCard,occurences,tags,delay);

        //Create member
        createNewMember();
        //send the punch card so it will activate the automation to punch it
        performAnActionSendPunchCard(smartPunchCard);

        checkAutomationRunAndPunchTheCard(occurences,tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionPunchThePunchCardOccurences1NoDelayTag() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "1";
        String delay = null;//no delay

        createSmartAutomationScenario(automation, SmartAutomationActions.PunchThePunchCard,occurences,tags,delay);

        //Create member
        createNewMember();
        //send the punch card so it will activate the automation to punch it
        performAnActionSendPunchCard(smartPunchCard);

        checkAutomationRunAndPunchTheCard(occurences,tags);

        checkTimingDefinition(delay);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testActionPunchThePunchCardOccurences1Delay2WeeksCheckNotPerform() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "1";
        String delay = "2";//no delay

        createSmartAutomationScenario(automation, SmartAutomationActions.PunchThePunchCard,occurences,tags,delay,TimingUnit.WEEKS);

        //Create member
        createNewMember();
        //send the punch card so it will activate the automation to punch it
        performAnActionSendPunchCard(smartPunchCard);

        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(PUNCH);

        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.WEEKS));
    }

    @Override
    protected ISmartAction getConcreteAction(SmartAutomationAction action) {
        return action.getPunchAPunchCard();
    }


    protected void checkAutomationRunAndPunchTheCard(String occurancesTimes,ArrayList<String> tags) throws Exception {

        checkDataStoreUpdatedWithPunchByRuleID(occurancesTimes,tags);

        //Verify the automation run and the gift was sent
        Assert.assertTrue("The member did not get all punches in the HUB sent by the automation, but appeared in the data store", MembersService.isPunchCardPunched(smartPunchCard.getTitle(),occurancesTimes));
        log4j.info("The member got the punches sent by the automation");
    }

    public void checkDataStoreUpdatedWithPunchByRuleID(String occurancesTimes,ArrayList<String> tags) throws Exception {

        log4j.info("check Data Store Updated with punches");
        int occurences = Integer.valueOf(occurancesTimes).intValue();
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the automation: " + automationRuleId,
                buildQuery(PUNCH,predicate) , occurences, dataStore);
        Assert.assertEquals("Must find " + occurancesTimes + " UserAction with the automation: " + automationRuleId, occurences, res.size());
        Text t = ((Text)res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());
        Assert.assertTrue("Data must contain the assetType: ", t.getValue().contains("AssetType=\"punch_card\""));

        checkActionTags(tags, res);
    }



    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Finish test : " + PunchThePunchCardActionTest.class.getName());
        try {
            checkSmartAutomationsAfterTestsFinished();
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
    }
}
