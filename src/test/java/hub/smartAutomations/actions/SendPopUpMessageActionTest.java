package hub.smartAutomations.actions;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.repackaged.com.google.common.collect.Lists;
import com.mashape.unirest.http.HttpResponse;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.TextAreaParameters;
import hub.common.objects.common.TimingUnit;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SendPopUpMessage;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.member.MembersService;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.IServerResponse;
import server.utils.ServerHelper;
import server.utils.UserActionsConstants;
import server.v2_8.GetMemberMessages;
import server.v2_8.GetMemberMessagesResponse;
import server.v2_8.DataStoreKeyUtility;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Message;
import utils.StringUtils;

import java.util.List;

/**
 * Created by Goni on 6/22/2017.
 *
 * This class test using trigger 'Join the Club' to test all available uses of the 'send pop up message' action in the smart automation
 */
public class SendPopUpMessageActionTest extends BaseSmartActionTest {

    public static final String AFTER_PUNCH = "afterPunch";
    public static final String AFTER_REDEEM = "afterRedeem";
    public static final String INBOX = "inbox";
    public static final String OPENING_APP = "popup";
    public static final String AFTER_CODEBASE_ACTION = "afterCodeBasedAction";
    public static final String NAVIGATE_HOME_SCREEN = "openMainLocaitonTC";
    private final static String MESSAGE_PREFIX = " welcome.";
    private static String textMessage = "@" + TextAreaParameters.Membership_FirstName + " " + MESSAGE_PREFIX;
    private static String title = "Test message";
    private static String sender = "Automation";
    private static String membershipKey;


    @Override
    public String getAutomationName() {
        return "SendPopUpAction_" + timestamp;
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "sendPopUp_" + timestamp;
        }
    };


    //CNP-6179
    @Test
    @Category(Hub1Regression.class)
    public void testActionSendPopUpDelay2Sec() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);

        String delay = "2";

        buildScenarioWithoutOccurances(automation, SmartAutomationActions.SendPopUPMessage, Lists.newArrayList(), delay, TimingUnit.SECONDS);
        List<String> checkedTags = Lists.newArrayList(AFTER_REDEEM,NAVIGATE_HOME_SCREEN);
        updatePopupAction(automation,checkedTags);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

        joinClubAndUpdateMembershipKey();
        checkAutomationRunAndSendPopUp();
        checkMemberMessage(checkedTags,MESSAGE_PREFIX);
        checkTimingDefinition(delay);
    }

    @Test
    public void testActionSendPopUpNoDelay() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        String delay = null;

        buildScenarioWithoutOccurances(automation, SmartAutomationActions.SendPopUPMessage, Lists.newArrayList(), delay, TimingUnit.SECONDS);
        List<String> checkedTags = Lists.newArrayList(AFTER_PUNCH,AFTER_CODEBASE_ACTION,NAVIGATE_HOME_SCREEN);
        updatePopupAction(automation,checkedTags);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);
        joinClubAndUpdateMembershipKey();
        checkAutomationRunAndSendPopUp();
        checkMemberMessage(checkedTags,MESSAGE_PREFIX);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionSendPopUpNoDelayOnlyAfterPunchChecked() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        String delay = null;

        buildScenarioWithoutOccurances(automation, SmartAutomationActions.SendPopUPMessage, Lists.newArrayList(), delay, TimingUnit.SECONDS);
        List<String> checkedTags = Lists.newArrayList(AFTER_PUNCH);
        updatePopupAction(automation,checkedTags);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);
        joinClubAndUpdateMembershipKey();
        checkAutomationRunAndSendPopUp();
        checkMemberMessage(checkedTags,MESSAGE_PREFIX);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionSendPopUpNoDelayAllCheckBoxesChecked() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        String delay = null;

        buildScenarioWithoutOccurances(automation, SmartAutomationActions.SendPopUPMessage, Lists.newArrayList(), delay, TimingUnit.SECONDS);
        List<String> checkedTags = Lists.newArrayList(AFTER_PUNCH,AFTER_CODEBASE_ACTION,NAVIGATE_HOME_SCREEN,AFTER_REDEEM,OPENING_APP);
        updatePopupAction(automation,checkedTags);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

        joinClubAndUpdateMembershipKey();
        checkAutomationRunAndSendPopUp();
        checkMemberMessage(checkedTags,MESSAGE_PREFIX);

        checkTimingDefinition(delay);
    }

    //CNP-6179
    @Test
    public void testActionSendPopUpDelay1HourCheckNotPerform() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);

        String delay = "1";

        buildScenarioWithoutOccurances(automation, SmartAutomationActions.SendPopUPMessage, Lists.newArrayList(), delay, TimingUnit.HOURS);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);
        createNewMember();

        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(SEND_POPUP_OPERATION);

        checkTimingDefinition(getCalculatedDelay(delay, TimingUnit.HOURS));
    }

    private void joinClubAndUpdateMembershipKey() throws Exception {
        createNewMember();
        membershipKey = MembersService.getMembershipKey();
    }

    private void updatePopupAction(SmartAutomation automation,List<String> tagsChecked) {
        SendPopUpMessage popUpAction = automation.getScenarios().get(0).getSmartAutomationActions().get(0).getSendPopUpMessage();
        for (String tag: tagsChecked){
            if(INBOX.equals(tag)){
                popUpAction.setChkAInbox("Y");
            }else if(AFTER_PUNCH.equals(tag)){
                popUpAction.setChkAfterPunch("Y");
            }else if(AFTER_CODEBASE_ACTION.equals(tag)){
                popUpAction.setChkAfterCodeBaseAction("Y");
            }else if(AFTER_REDEEM.equals(tag)){
                popUpAction.setChkAfterRedeem("Y");
            }else if(NAVIGATE_HOME_SCREEN.equals(tag)){
                popUpAction.setChkNavigateHome("Y");
            }else if(OPENING_APP.equals(tag)){
                popUpAction.setChkOpenApp("Y");
            }
        }

    }

    private void checkMemberMessage(List<String> checkedTags,String content) {

        GetMemberMessages getMemberMessages = new GetMemberMessages(membershipKey);

        IServerResponse response = getMemberMessages.sendRequestByTokenAndLocation(serverToken,locationId, GetMemberMessagesResponse.class);


        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        // Cast to GetMemberMessagesResponse object to fetch all response data
        GetMemberMessagesResponse memberMessagesResponse = (GetMemberMessagesResponse) response;
        Assert.assertFalse(memberMessagesResponse.getMessages().isEmpty());
        Message message = memberMessagesResponse.getMessages().get(0);

        //check that all checked tags appear in the returned message
        Assert.assertTrue("Message tags should contain the tag: " + tag, message.getTags().containsAll(checkedTags));
        Assert.assertTrue("Message should contain the content: " + content, message.getContent().contains(content));
        Assert.assertEquals("Message should contain the title: " + content, message.getTitle(),title);
    }
    private void checkAutomationRunAndSendPopUp() throws Exception {

        log4j.info("check Data Store updated with send pop up message action");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find UserAction with the automation: " + automationRuleId,
                buildQuery(UserActionsConstants.SEND_POPUP_MSG, predicate), 1, dataStore);
        Assert.assertEquals("Must find one UserAction with the automation: " + automationRuleId, 1, res.size());
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info("Data contains the following Member Message key : " + t.getValue());
        String data = t.getValue().toString();
        String memberMessageKey = data.substring(data.indexOf("=\"")+2,data.lastIndexOf("\""));

        log4j.info("get message key : " + memberMessageKey);
        Key messageKeyDS = serverHelper.getDSKey(memberMessageKey);
        Query q1 = new Query("MemberMessage");
        q1.setFilter(new Query.FilterPredicate("__key__", Query.FilterOperator.EQUAL, messageKeyDS));
        res = ServerHelper.queryWithWait("Must find Message with the key: " + messageKeyDS,
                q1, 1, dataStore);

        String title1 = (String) res.get(0).getProperty("Title");
        Assert.assertEquals("Message should contain the title: " + title, title1,title);
    }

    @Override
    protected ISmartAction getConcreteAction(SmartAutomationAction action) {
        return action.getSendPopUpMessage();
    }


    @Override
    protected SmartAutomationAction getAction(String actionName, SmartAutomationAction smartAction, ISmartAutomationCounter automation) throws Exception {
        SmartAutomationAction action = hubAssetsCreator.getSendPopUpMessageAction(smartAction, automation, title, textMessage);
        return action;
    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

    @AfterClass
    public static void tearDown() {
        log4j.info("Finish test : " + SendPopUpMessageActionTest.class.getName());
        try {
            checkSmartAutomationsAfterTestsFinished();
        } catch (Exception e) {
            log4j.error(e.getMessage(), e);
        }
    }
}