package hub.smartAutomations.actions;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.TimingUnit;
import hub.common.objects.member.NewMember;
import hub.common.objects.member.performActionObjects.TagUntagOperation;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.common.objects.smarts.SmartAutomation;
import hub.services.member.MembersService;
import org.junit.experimental.categories.Category;
import server.utils.ServerHelper;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 5/23/2017.
 *
 * This class test using trigger 'Join The Club' to test all available uses of the 'Tag member' action in the smart automation and
 *  using trigger 'Update membership details' to test all available uses of the 'Untag member' action in the smart automation
 */
public class TagUntagActionTest extends BaseSmartActionTest{

    private static TagUntagOperation operation;

    @Override
    public Triggers getTrigger() {
        if(TagUntagOperation.Tag.equals(operation))
            return Triggers.JoinsTheClub;
        else
            return Triggers.UpdatesMembershipDetails;
    }

    @Override
    public String getAutomationName() {
        return "TagUntagAction_" + timestamp;
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_untag_" +timestamp;
        }
    };


    @Test
    @Category(Hub1Regression.class)
    public void testActionTagMemberDelay2secTags() throws Exception {

        operation = TagUntagOperation.Tag;

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag,"tag2");

        String delay = "2";
        createScenarioWithoutOccurences(automation, SmartAutomationActions.TagMember,tags,delay,TimingUnit.SECONDS);
        //Create member
        createNewMember();

        Thread.sleep(2000);//wait 2 sec till automation will run

        checkAutomationRunAndTagMember(tags);

        checkTimingDefinition(delay);
    }


    @Test
    public void testActionTagMemberDelay1SecNoTags() throws Exception {

        operation = TagUntagOperation.Tag;
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList();
        String delay = "1";

        createScenarioWithoutOccurences(automation, SmartAutomationActions.TagMember,tags,delay,TimingUnit.SECONDS);

        createNewMember();

        checkAutomationRunAndTagMember(tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionTagMemberNoDelayTag() throws Exception {

        operation = TagUntagOperation.Tag;
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String delay = null;//no delay

        createScenarioWithoutOccurences(automation, SmartAutomationActions.TagMember,tags,delay,null);

        createNewMember();

        checkAutomationRunAndTagMember(tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionTagMemberDelay1HourCheckNotPerform() throws Exception {

        operation = TagUntagOperation.Tag;
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String delay = "1";//no delay

        createScenarioWithoutOccurences(automation, SmartAutomationActions.TagMember,tags,delay, TimingUnit.HOURS);

        createNewMember();

        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.HOURS));
    }


    @Test
    public void testActionUnTagMemberDelay2secTags() throws Exception {

        operation = TagUntagOperation.Untag;
        //Create member and send the tag to him
        NewMember newMember = createNewMember();
        performAnActionTagMember(newMember,tag);

        //Create smart automation to untag member
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag,"tag2");

        String delay = "2";
        createScenarioWithoutOccurences(automation, SmartAutomationActions.TagMember,tags,delay,TimingUnit.SECONDS);

        performUpdateMemberDetails(newMember);

        checkAutomationRunAndTagMember(tags);

        checkTimingDefinition(delay);
    }


    @Test
    @Category(Hub1Regression.class)
    public void testActionUnTagMemberDelay1SecNoTags() throws Exception {

        operation = TagUntagOperation.Untag;
        //Create member and send the tag to him
        NewMember newMember = createNewMember();
        performAnActionTagMember(newMember,tag);

        //Create smart automation to untag member
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList();
        String delay = "1";

        createScenarioWithoutOccurences(automation, SmartAutomationActions.TagMember,tags,delay,TimingUnit.SECONDS);

        performUpdateMemberDetails(newMember);

        checkAutomationRunAndTagMember(tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionUnTagMemberNoDelayTag() throws Exception {

        operation = TagUntagOperation.Untag;
        //Create member and send the tag to him
        NewMember newMember = createNewMember();
        performAnActionTagMember(newMember,tag);

        //Create smart automation to untag member
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String delay = null;//no delay
        createScenarioWithoutOccurences(automation, SmartAutomationActions.TagMember,tags,delay,TimingUnit.SECONDS);

        performUpdateMemberDetails(newMember);

        checkAutomationRunAndTagMember(tags);
        checkTimingDefinition(delay);
    }

    @Test
    public void testActionUnTagMemberDelay1HourCheckNotPerform() throws Exception {

        operation = TagUntagOperation.Untag;
        //Create member and send the tag to him
        NewMember newMember = createNewMember();
        performAnActionTagMember(newMember,tag);

        //Create smart automation to untag member
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String delay = "1";//no delay

        createScenarioWithoutOccurences(automation, SmartAutomationActions.TagMember,tags,delay,TimingUnit.HOURS);

        performUpdateMemberDetails(newMember);

        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.HOURS));
    }

    protected void checkAutomationRunAndTagMember(ArrayList<String> tags) throws Exception {

        checkDataStoreUpdatedWithTagByRuleID(tags);

        //Verify the automation run and the gift was sent
        if(TagUntagOperation.Tag.equals(operation)) {
            Assert.assertTrue("The member did not get the tag in the HUB sent by the automation, but appeared in the data store", MembersService.isTagExists(tag));
            log4j.info("The member got the tag sent by the automation");
        }
    }

    public void checkDataStoreUpdatedWithTagByRuleID(ArrayList<String> tags) throws Exception {

        log4j.info("check Data Store Updated with tag/untag");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the automation: " + automationRuleId,
                buildQuery(TAG_OPERATION,predicate) , 1, dataStore);
        Assert.assertEquals("Must find one UserAction with the automation: " + automationRuleId, 1, res.size());
        Text t = ((Text)res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());

        Assert.assertTrue("Data must contain the tag: ", t.getValue().contains("Tag=\"" + tag + "\""));

        if(TagUntagOperation.Tag.equals(operation))
            Assert.assertTrue("Data must contain the operation: ", t.getValue().contains("Operation=\"" + TagUntagOperation.Tag.toString() + "\""));
        else
            Assert.assertTrue("Data must contain the operation: ", t.getValue().contains("Operation=\"" + TagUntagOperation.Untag.toString() + "\""));

        checkActionTags(tags, res);
    }



    @Override
    protected ISmartAction getConcreteAction(SmartAutomationAction action) {
        return action.getTagMember();
    }


    @Override
    protected SmartAutomationAction getAction(String actionName, SmartAutomationAction smartAction, ISmartAutomationCounter automation) throws Exception{
        SmartAutomationAction action = hubAssetsCreator.getTagMemberAction(smartAction, automation, tag,operation);
        return action;
    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Finish test : " + TagUntagActionTest.class.getName());
        try {
            checkSmartAutomationsAfterTestsFinished();
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
    }

}
