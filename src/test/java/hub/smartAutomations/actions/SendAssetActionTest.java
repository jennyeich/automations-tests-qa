package hub.smartAutomations.actions;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.TimingUnit;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Goni on 5/21/2017.
 *
 * This class test using trigger 'Join The Club' to test all available uses of the 'Send an asset' action in the smart automation
 */
public class SendAssetActionTest extends BaseSmartActionTest{


    @Override
    public String getAutomationName() {
        return "SendAssetAction_" + timestamp;
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "sendAsset_" +timestamp;
            if(smartGift == null) {
                //create assets
                try {
                    smartGift = hubAssetsCreator.createSmartGift(timestamp);

                } catch (Exception e) {
                    log4j.error("error while creating asset",e);
                }
            }
        }
    };

    @Test
    public void testActionSendAssetOccurences3Delay10secTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag,"tag2");
        String occurences = "3";
        String delay = "10";
        createSmartAutomationScenario(automation, SmartAutomationActions.SendAnAsset,occurences,tags,delay);
        //Create member
        createNewMember();

        checkAutomationRunAndSendAssetToMember(occurences,tags);

        checkTimingDefinition(delay);
    }


    @Test
    @Category(Hub1Regression.class)
    public void testActionSendAssetOccurences2Delay5SecNoTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList();
        String occurences = "2";
        String delay = "5";

        createSmartAutomationScenario(automation, SmartAutomationActions.SendAnAsset,occurences,tags,delay);

        createNewMember();

        checkAutomationRunAndSendAssetToMember(occurences,tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionSendAssetOccurences1NoDelayTag() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "1";
        String delay = null;//no delay

        createSmartAutomationScenario(automation, SmartAutomationActions.SendAnAsset,occurences,tags,delay);

        createNewMember();

        checkAutomationRunAndSendAssetToMember(occurences,tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionSendAssetOccurences1Delay1HourCheckNotPerform() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "1";
        String delay = "1";//no delay

        createSmartAutomationScenario(automation, SmartAutomationActions.SendAnAsset,occurences,tags,delay,TimingUnit.HOURS);

        createNewMember();

        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(RECEIVEDASSET);

        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.HOURS));
    }

    @Test
    public void  testActionSendAssetOccurences1Delay1Min() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "1";
        String delay = "1";//no delay

        createSmartAutomationScenario(automation, SmartAutomationActions.SendAnAsset,occurences,tags,delay,TimingUnit.MINUTES);

        createNewMember();
        //wait 1 min to allow automation to run
        TimeUnit.MINUTES.sleep(1);
        log4j.info("wait 1 min to allow automation to run");

        checkAutomationRunAndSendAssetToMember(occurences,tags);

        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.MINUTES));
    }

    @Override
    protected ISmartAction getConcreteAction(SmartAutomationAction action) {
        return action.getSendAsset();
    }




    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Finish test : " + SendAssetActionTest.class.getName());
        try {
            checkSmartAutomationsAfterTestsFinished();
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
    }

}
