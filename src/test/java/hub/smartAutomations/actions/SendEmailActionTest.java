package hub.smartAutomations.actions;

import com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.RegistrationField;
import hub.common.objects.common.YesNoListNumber;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.OperationService;
import hub.services.ContentService;
import hub.services.member.MembersLog;
import hub.services.member.MembersService;
import hub.utils.HubTemplateCreator;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.common.Item;
import utils.AutomationException;

import java.beans.IntrospectionException;
import java.util.*;

public class SendEmailActionTest extends BaseSmartActionTest {

    private static final String FIRST_NAME = "testFullFN";
    private static final String LAST_NAME = "testFullLN";
    private static final String EMAIL_ADDRESS = "automationcomo@gmail.com";
    private static final String HOME_BRANCH = "Rehovot";
    private static final String FAVORITE_BRANCH = "Tel-Aviv";
    private static final String GENERIC_STRING = "BlaBlaBla";
    private static final String ADDRESS_ZIP_CODE = "1111";
    private static final String EXPIRATION_DATE = "01012035";
    private static final String BIRTHDAY = "04071986";
    private static final String ANNIVERSARY = "03092014";
    private static final String BRANCH_ID = "1514";
    private static final String TOTAL_SUM = "78900";

    private static final String EXPIRATION_DATE_FROM_EMAIL = "01&#x2F;01&#x2F;2035";
    private static final String BIRTHDAY_FROM_EMAIL = "04&#x2F;07&#x2F;1986";
    private static final String ANNIVERSARY_FROM_EMAIL = "03&#x2F;09&#x2F;2014";
    private static final String TOTAL_SUM_FROM_EMAIL = "789.00";


    @Override
    protected ISmartAction getConcreteAction(SmartAutomationAction action) {
        return action.getSendEmail();
    }

    @Override
    public String getAutomationName() {
        return "SendEmailAction_" + timestamp;
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "sendEmail_" +timestamp;
            templateName = "template_" + timestamp;
        }
    };


    @Test
    public void testSendEmailSmartAction() throws Exception {
        HubTemplateCreator.createImageTemplate(templateName);
        ArrayList<String> tags = Lists.newArrayList(tag);
        SmartAutomation smartAutomation = hubAssetsCreator.createSmartAutomation(Triggers.UpdatesMembershipDetails, "SendEMail" + timestamp, settings);
        createSmartAutomationScenario(smartAutomation, SmartAutomationActions.SendEmail, "1",tags, null);
        NewMember memeber = createMember();
        Thread.sleep(20000);
        checkEmail(Collections.emptyList(), String.format(HubTemplateCreator.DEFAULT_MAIL_SUBJECT, memeber.getFirstName(), memeber.getLastName()));
    }


    @Test
    @Category(Hub1Regression.class)
    public void testSendEmailSmartActionWithDelay() throws Exception {
        HubTemplateCreator.createCodeTemplate(templateName);
        ArrayList<String> tags = Lists.newArrayList(tag);
        SmartAutomation smartAutomation = hubAssetsCreator.createSmartAutomation(Triggers.UpdatesMembershipDetails, "SendEMail" + timestamp, settings);
        createSmartAutomationScenario(smartAutomation, SmartAutomationActions.SendEmail, "1", tags, "10");
        NewMember memeber = createMember();
        Thread.sleep(30000);
        checkEmail(Arrays.asList("Click the gift to get your special surprise!"), String.format(HubTemplateCreator.DEFAULT_MAIL_SUBJECT, memeber.getFirstName(), memeber.getLastName()));
    }

    @Test
    public void testSendEmailDynamicAttributesSmartAction() throws Exception {
        OperationService.addRequiredFieldsToRegistrationForm(Arrays.asList(RegistrationField.zipCode, RegistrationField.CarNumber,
                RegistrationField.homeBranchID, RegistrationField.FavoriteBranchID), YesNoListNumber.NO.toString());

        String filePath = HubTemplateCreator.resourcesDir + "email_template_dynamicAttributes.html";
        HubTemplateCreator.createCodeTemplate(templateName, HubTemplateCreator.SUBJECT, filePath, true);
        ArrayList<String> tags = Lists.newArrayList(tag);
        SmartAutomation smartAutomation = hubAssetsCreator.createSmartAutomation(Triggers.SubmitAPurchase, "SendEMail" + timestamp, settings);
        createSmartAutomationScenario(smartAutomation, SmartAutomationActions.SendEmail, "1",tags, null);

        NewMember member = createFullMember();
        Item item = createItem(timestamp.toString(),ITEM_NAME_PREFIX + "33", DEPARTMENT_CODE, "Dep1", 78900);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchaseWithParams(Arrays.asList(member),TOTAL_SUM, null, transactionId, items, BRANCH_ID);
        Assert.assertTrue("Email sent log not found in member profile.", MembersService.isLogAppears(MembersLog.EMAIL_SENT_MESSAGE(templateName, "Automation")));
        Assert.assertTrue("Email reached inbox log not found in member profile.", MembersService.isLogAppears(MembersLog.EMAIL_REACHED_MESSAGE(templateName)));
        checkEmail(buildHtmlData(member), String.format(HubTemplateCreator.DEFAULT_MAIL_SUBJECT, member.getFirstName(), member.getLastName()));
    }


    private NewMember createMember() throws InterruptedException, AutomationException {
        NewMember member = hubMemberCreator.buildMember(timestamp, "testFN", "testLN");
        member.setEmail("automationcomo+" + timestamp + "@gmail.com");
        MembersService.createNewMember(member);
        return member;
    }

    private NewMember createFullMember() throws InterruptedException, AutomationException{
        NewMember member = hubMemberCreator.buildMember(timestamp, FIRST_NAME, LAST_NAME);
        member.setEmail("automationcomo+" + timestamp + "@gmail.com");
        member.setGenericString1(GENERIC_STRING);
        member.setMembershipExpiration(EXPIRATION_DATE);
        member.setAnniversary(ANNIVERSARY);
        member.setBirthday(BIRTHDAY);
        member.setHomeBranchId(HOME_BRANCH);
        member.setFavoriteBranchId(FAVORITE_BRANCH);
        member.setAddressZipCode(ADDRESS_ZIP_CODE);
        MembersService.createNewMember(member);
        return member;
    }

    private void checkEmail(List<String> content, String subject) throws Exception {
        boolean result = ContentService.checkMail(content, subject);
        if (!result) {
            Assert.fail("EMail didnt contain the expected subject and/or content.");
        }
    }

    private List buildHtmlData(NewMember member) {
        List<String> data = new ArrayList<>();
        data.add( String.format("Hi %s %s", member.getFirstName(), member.getLastName()));
        data.add( String.format("your birthday is %s", BIRTHDAY_FROM_EMAIL));
        data.add( String.format("your anniversary is: %s", ANNIVERSARY_FROM_EMAIL));
        data.add( String.format("Email: %s", "automationcomo+" + timestamp + "@gmail.com" ));
        data.add( String.format("FavoriteBranchID: %s", FAVORITE_BRANCH ));
        data.add( String.format("HomeBranchID:  %s", HOME_BRANCH ));
        data.add( String.format("GenericString1:  %s", GENERIC_STRING ));
        data.add( String.format("AddressZipCode :  %s", ADDRESS_ZIP_CODE ));
        data.add( String.format("ExpirationDate:  %s", EXPIRATION_DATE_FROM_EMAIL ));
        data.add( String.format("Purchase.TotalSum:  %s", TOTAL_SUM_FROM_EMAIL));
        data.add( String.format("Purchase.BranchID:  %s", BRANCH_ID ));
        return data;
    }

    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Finish test : " + SendAssetActionTest.class.getName());
        try {
            checkSmartAutomationsAfterTestsFinished();
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
    }


}
