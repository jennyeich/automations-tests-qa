package hub.smartAutomations.actions;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import hub.common.objects.common.TimingTypes;
import hub.common.objects.common.TimingUnit;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Occurrences;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.OccurrencesType;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Timing;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import server.utils.ServerHelper;
import server.utils.UserActionsConstants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Goni on 5/21/2017.
 */
public abstract class BaseSmartActionTest extends BaseAutomationTest {

    @Override
    public Triggers getTrigger() {
        return Triggers.JoinsTheClub;
    }

    protected abstract ISmartAction getConcreteAction(SmartAutomationAction action);

    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        init();
    }

    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }

    protected NewMember createNewMember() throws Exception {
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        return newMember;
    }

    protected void createSmartAutomationScenario(SmartAutomation automation, SmartAutomationActions actionName, String occurancesTimes,ArrayList<String> tags,String delay) throws Exception {

        createSmartAutomationScenario(automation,actionName,occurancesTimes,tags,delay,TimingUnit.SECONDS);

    }

    protected void createSmartAutomationScenario(SmartAutomation automation, SmartAutomationActions actionName, String occurancesTimes,ArrayList<String> tags,String delay,TimingUnit timingUnit) throws Exception {
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction action = getAction(actionName.toString(), smartAction, automation);

        setTags(automation, getConcreteAction(action),tags);
        setOccurrences(automation, occurancesTimes, getConcreteAction(action), OccurrencesType.fixed);
        if(delay != null)
            setTiming(automation, getConcreteAction(action), TimingTypes.DELAYED,delay,timingUnit);

        scenario.addAction(action,automation);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

    }


    protected void buildScenarioWithoutOccurances(SmartAutomation automation, SmartAutomationActions actionName, ArrayList<String> tags, String delay, TimingUnit timingUnit) throws Exception {
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction action = getAction(actionName.toString(), smartAction, automation);

        setTags(automation, getConcreteAction(action),tags);
        if(delay != null)
            setTiming(automation, getConcreteAction(action), TimingTypes.DELAYED,delay,timingUnit);

        scenario.addAction(action,automation);
    }

    protected void createScenarioWithoutOccurences(SmartAutomation automation, SmartAutomationActions actionName,ArrayList<String> tags,String delay,TimingUnit timingUnit) throws Exception {

        buildScenarioWithoutOccurances(automation, actionName,tags,delay,timingUnit);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

    }

    protected String getCalculatedDelay(String delay, TimingUnit timingUnit) {
        switch (timingUnit){
            case SECONDS:
                return delay;
            case MINUTES:
                return String.valueOf(Integer.valueOf(delay).intValue() *60);
            case HOURS:
                return String.valueOf(Integer.valueOf(delay).intValue() *3600);
            case DAYS:
                return String.valueOf(Integer.valueOf(delay).intValue() *86400);
            case WEEKS:
                return String.valueOf(Integer.valueOf(delay).intValue() *604800);
           default:
               return delay;
        }
    }

    protected void setTags(SmartAutomation automation, ISmartAction smartAction,ArrayList<String> tagsToWrite) {

        smartAction.setTags(tagsToWrite,automation);
    }

    protected void setTiming(SmartAutomation automation, ISmartAction smartAction,TimingTypes timingTypes,String input,TimingUnit timingUnit) {
        Timing timing = new Timing(automation);
        timing.setTimingTypes(timingTypes.toString());
        timing.setValueInput(input);
        timing.setTimingUnits(timingUnit);
        smartAction.setTiming(timing);
    }


    protected void setOccurrences(SmartAutomation automation, String occurancesTimes, ISmartAction smartAction,OccurrencesType occurrencesType) {
        Occurrences occurrences = new Occurrences(automation);
        occurrences.setOccurrences_type(occurrencesType.toString()); //add the fixed occurrances
        occurrences.setOccurrences_times(occurancesTimes);
        smartAction.setOccurrences(occurrences);
    }

    protected void checkTimingDefinition(String delay)
    {
        Query q = new Query("EventBasedActionRule");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("LocationID", Query.FilterOperator.EQUAL, locationId),
                new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId))));
        List<Entity> res = dataStore.findByQuery(q);

        Text t = (Text) res.get(0).getProperty("SmartAction");
        if(delay == null)
            Assert.assertFalse("smartAction must not contain the scheduling: ", t.getValue().contains("\"delay\":\"" + delay +"\""));
        else
            Assert.assertTrue("smartAction must contain the scheduling: ", t.getValue().contains("\"delay\":\"" + delay +"\""));
    }

    protected void checkAutomationRunAndSendAssetToMember(String occurancesTimes,ArrayList<String> tags) throws Exception {
        int occurences = Integer.valueOf(occurancesTimes).intValue();
        checkDataStoreUpdatedWithAssetByRuleID(occurences,tags);

        //Verify the automation run and the gift was sent
        Assert.assertTrue("The member did not get the gift in the HUB sent by the automation, but appeared in the data store", MembersService.isAllAssetsExist(smartGift.getTitle(),occurences));
        log4j.info("The member got the gift sent by the automation");
    }

    public void checkDataStoreUpdatedWithAssetByRuleID(int occurancesTimes,ArrayList<String> tags) throws Exception {

        log4j.info("check Data Store Updated with asset");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find UserAction with the automation: " + automationRuleId,
                buildQuery(UserActionsConstants.RECEIVEDASSET,predicate) , occurancesTimes, dataStore);
        Assert.assertEquals("Must find " + occurancesTimes + " UserAction with the automation: " + automationRuleId, occurancesTimes, res.size());
        Text t = ((Text)res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());
        Assert.assertTrue("Data must contain the assetType: ", t.getValue().contains("AssetType=\"gift\""));

        checkActionTags(tags, res);
    }

    public void checkDataStoreUpdatedWithAssetByTransactionId(String transactionId,int occurancesTimes,ArrayList<String> tags) throws Exception {

        log4j.info("check Data Store Updated with asset");
        Query.FilterPredicate predicate = new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, transactionId);
        List<Entity> res = ServerHelper.queryWithWait("Must find UserAction with the transactionId: " + transactionId,
                buildQuery(UserActionsConstants.RECEIVEDASSET,predicate) , occurancesTimes, dataStore);
        Assert.assertEquals("Must find " + occurancesTimes + " UserAction with the transactionId: " + transactionId, occurancesTimes, res.size());
        Text t = ((Text)res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());
        Assert.assertTrue("Data must contain the assetType: ", t.getValue().contains("AssetType=\"gift\""));

        checkActionTags(tags, res);
    }

}
