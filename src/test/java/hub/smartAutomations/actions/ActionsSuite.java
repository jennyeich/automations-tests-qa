package hub.smartAutomations.actions;

import hub.base.BaseHubTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.IOException;

/**
 * Created by Goni on 6/6/2017.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        RegisterActionTest.class,
        AddPointsCreditActionTest.class,
        PunchThePunchCardActionTest.class,
        SendAssetActionTest.class,
        TagUntagActionTest.class,
        SendLotteryRewardActionTest.class,
        SendEmailActionTest.class
})
public class ActionsSuite {

    public static final Logger log4j = Logger.getLogger(ActionsSuite.class);


    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseSmartActionTest.init();
    }

    @AfterClass
    public static void tearDown(){

        log4j.info("close browser after finish test");
        BaseSmartActionTest.cleanBase();
        BaseHubTest.cleanHub();
    }

}
