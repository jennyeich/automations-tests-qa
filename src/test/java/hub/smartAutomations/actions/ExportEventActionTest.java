package hub.smartAutomations.actions;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.TimingUnit;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.common.objects.smarts.SmartAutomation;
import org.jetbrains.annotations.NotNull;
import org.junit.experimental.categories.Category;
import server.utils.ServerHelper;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.repackaged.com.google.common.collect.Lists;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 5/25/2017.
 *
 *  This class test using trigger 'Join the Club' to test all available uses of the 'Export event' action in the smart automation
 */
public class ExportEventActionTest extends BaseSmartActionTest{


    private static final String DESTINATION_URL = "https://services-dot-comoqa.appspot.com/echo";

    private static String TRIGGER_JOIN = "joinedclub";


    @Override
    public String getAutomationName() {
        return "ExportAction_" + timestamp;
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "export_" +timestamp;
        }
    };

    @Test
    @Category(Hub1Regression.class)
    public void testActionExportEventDelay2secTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag,"tag2");

        String delay = "2";
        createScenarioWithoutOccurences(automation, SmartAutomationActions.ExportEvent,tags,delay, TimingUnit.SECONDS);

        createNewMember();

        checkAutomationRunAndEventExport(tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionExportEventDelay1SecNoTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList();

        String delay = "1";
        createScenarioWithoutOccurences(automation, SmartAutomationActions.ExportEvent,tags,delay, TimingUnit.SECONDS);

        createNewMember();

        checkAutomationRunAndEventExport(tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionExportEventNoDelayTag() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);

        String delay = null;
        createScenarioWithoutOccurences(automation, SmartAutomationActions.ExportEvent,tags,delay, TimingUnit.SECONDS);

        createNewMember();

        checkAutomationRunAndEventExport(tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionExportEventDelay1HourCheckNotPerform() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag,"tag2");

        String delay = "1";
        createScenarioWithoutOccurences(automation, SmartAutomationActions.ExportEvent,tags,delay, TimingUnit.HOURS);

        createNewMember();

        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(EVENT_EXPORTED_OPERATION);

        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.HOURS));
    }


    protected void checkAutomationRunAndEventExport(ArrayList<String> tags) throws Exception {

        checkDataStoreUpdatedWithEventExportByRuleID(tags);

    }

    public void checkDataStoreUpdatedWithEventExportByRuleID(ArrayList<String> tags) throws Exception {

        List<Entity> res = checkDataStoreWithEventExport(TRIGGER_JOIN,timestamp.toString(),DESTINATION_URL);
        checkActionTags(tags, res);
    }





    @Override
    protected ISmartAction getConcreteAction(SmartAutomationAction action) {
        return action.getExportEvent();
    }


    @Override
    protected SmartAutomationAction getAction(String actionName, SmartAutomationAction smartAction, ISmartAutomationCounter automation) throws Exception{
        SmartAutomationAction action = hubAssetsCreator.getExportEventAction(smartAction, automation,getAutomationName(),DESTINATION_URL);
        return action;
    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Finish test : " + ExportEventActionTest.class.getName());
        try {
            checkSmartAutomationsAfterTestsFinished();
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
    }
}
