package hub.smartAutomations.actions;

import hub.common.objects.common.TimingUnit;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.common.objects.smarts.SmartAutomation;
import hub.services.member.MembersService;
import ru.yandex.qatools.allure.annotations.Issue;
import server.utils.ServerHelper;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 5/24/2017.
 *
 * This class test using trigger 'Tag member' to test all available uses of the 'Unregister' action in the smart automation
 */
@Issue("CNP-8928")
@Deprecated
public class UnregisterActionTest extends BaseSmartActionTest{

    private static final String NOT_REGISTERED = "not_registered";

    @Override
    public Triggers getTrigger() {
        return Triggers.TagOrUntag;
    }

    @Override
    public String getAutomationName() {
        return "UnregisterAction_" + timestamp;
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "unregister_" +timestamp;
        }
    };

    @Test
    public void testActionUnregisterMemberDelay2secTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag,"tag2");

        String delay = "2";
        createScenarioWithoutOccurences(automation, SmartAutomationActions.UnregisterMembership,tags,delay, TimingUnit.SECONDS);

        //Create member and send the tag to him
        NewMember newMember = createNewMember();
        performAnActionTagMember(newMember,tag);

        checkAutomationRunAndUnregisterMember(tags);

        checkTimingDefinition(delay);
    }


    @Test
    public void testActionUnregisterMemberDelay1SecNoTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList();
        String delay = "1";

        createScenarioWithoutOccurences(automation, SmartAutomationActions.UnregisterMembership,tags,delay,TimingUnit.SECONDS);

        //Create member and send the tag to him
        NewMember newMember = createNewMember();
        performAnActionTagMember(newMember,tag);

        checkAutomationRunAndUnregisterMember(tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionUnregisterMemberNoDelayTag() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String delay = null;//no delay
        createScenarioWithoutOccurences(automation, SmartAutomationActions.UnregisterMembership,tags,delay,TimingUnit.SECONDS);

        //Create member and send the tag to him
        NewMember newMember = createNewMember();
        performAnActionTagMember(newMember,tag);

        checkAutomationRunAndUnregisterMember(tags);
        checkTimingDefinition(delay);
    }

    @Test
    public void testActionUnregisterTagMemberDelay1HourCheckNotPerform() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String delay = "1";//no delay

        createScenarioWithoutOccurences(automation, SmartAutomationActions.UnregisterMembership,tags,delay,TimingUnit.HOURS);

        //Create member and send the tag to him
        NewMember newMember = createNewMember();
        performAnActionTagMember(newMember,tag);


        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.HOURS));

        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(UNREGISTER);

    }

    protected void checkAutomationRunAndUnregisterMember(ArrayList<String> tags) throws Exception {

        checkDataStoreUpdatedWithUnregisterByRuleID(tags);
        Assert.assertTrue("The member still register, but appeared unregister in the data store", MembersService.checkMemberStatus(NOT_REGISTERED));
        log4j.info("The member became unregistered by the automation");
    }

    public void checkDataStoreUpdatedWithUnregisterByRuleID(ArrayList<String> tags) throws Exception {

        log4j.info("check Data Store Updated with unregister");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the automation: " + automationRuleId,
                buildQuery(UNREGISTER,predicate) , 1, dataStore);
        Assert.assertEquals("Must find one UserAction with the automation: " + automationRuleId, 1, res.size());

        checkActionTags(tags, res);
    }


    @Override
    protected ISmartAction getConcreteAction(SmartAutomationAction action) {
        return action.getUnregister();
    }


    @Override
    protected SmartAutomationAction getAction(String actionName, SmartAutomationAction smartAction, ISmartAutomationCounter automation) throws Exception{
        SmartAutomationAction action = hubAssetsCreator.getUnregisterAction(smartAction, automation);
        return action;
    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Finish test : " + UnregisterActionTest.class.getName());
        try {
            checkSmartAutomationsAfterTestsFinished();
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
    }
}
