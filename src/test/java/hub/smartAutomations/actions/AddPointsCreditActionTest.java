package hub.smartAutomations.actions;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.base.categories.serverCategories.ServerRegression;
import hub.common.objects.common.TimingUnit;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.common.objects.smarts.SmartAutomation;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.services.member.MembersService;
import org.junit.experimental.categories.Category;
import server.utils.ServerHelper;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 5/21/2017.
 *
 * This class test using trigger 'Join The Club' to test all available uses of the 'Add Points/Credit' action in the smart automation
 */
public class AddPointsCreditActionTest extends BaseSmartActionTest{


    private static AmountType amountType;
    private static String points = "10";

    @Override
    public String getAutomationName() {
        return "AddPointsCreditAction_" + timestamp;
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "addPoints_" +timestamp;
        }
    };

    @Test
    @Category(Hub1Regression.class)
    public void testActionAddPointsOccurences3Delay10secTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        amountType = AmountType.Points;
        points = "10";
        ArrayList<String> tags = Lists.newArrayList("tag1",tag);
        String occurences = "3";
        String delay = "10";
        createSmartAutomationScenario(automation, SmartAutomationActions.AddSetNumberofPoints,occurences,tags,delay);
        //Create member
        createNewMember();

        checkAutomationRunAndAddPointsToMember(occurences,tags);

        checkTimingDefinition(delay);
    }


    @Test
    public void testActionAddPointsOccurences2Delay5SecNoTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        amountType = AmountType.Points;
        points = "10";
        ArrayList<String> tags = Lists.newArrayList();
        String occurences = "2";
        String delay = "5";

        createSmartAutomationScenario(automation, SmartAutomationActions.AddSetNumberofPoints,occurences,tags,delay);

        createNewMember();

        checkAutomationRunAndAddPointsToMember(occurences,tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionAddPointsOccurences1NoDelayTag() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        amountType = AmountType.Points;
        points = "10";
        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "1";
        String delay = null;//no delay

        createSmartAutomationScenario(automation, SmartAutomationActions.AddSetNumberofPoints,occurences,tags,delay);

        createNewMember();

        checkAutomationRunAndAddPointsToMember(occurences,tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionAddPointsOccurences1Delay1HourCheckNotPerform() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        amountType = AmountType.Points;
        points = "10";
        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "1";
        String delay = "1";//no delay

        createSmartAutomationScenario(automation, SmartAutomationActions.AddSetNumberofPoints,occurences,tags,delay, TimingUnit.HOURS);

        createNewMember();

        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(POINTSTRASACTION);

        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.HOURS));
    }


    @Test
    @Category(Hub1Regression.class)
    public void testActionAddCreditOccurences3Delay10secTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        amountType = AmountType.Credit;
        points = "10";
        ArrayList<String> tags = Lists.newArrayList(tag,"tag2");
        String occurences = "3";
        String delay = "10";
        createSmartAutomationScenario(automation, SmartAutomationActions.AddSetNumberofPoints,occurences,tags,delay);
        //Create member
        createNewMember();

        checkAutomationRunAndAddPointsToMember(occurences,tags);

        checkTimingDefinition(delay);
    }


    @Test
    public void testActionAddCreditOccurences2Delay5SecNoTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        amountType = AmountType.Credit;
        points = "10";
        ArrayList<String> tags = Lists.newArrayList();
        String occurences = "2";
        String delay = "5";

        createSmartAutomationScenario(automation, SmartAutomationActions.AddSetNumberofPoints,occurences,tags,delay);

        createNewMember();

        checkAutomationRunAndAddPointsToMember(occurences,tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionAddCreditOccurences1NoDelayTag() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        amountType = AmountType.Credit;
        points = "10";
        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "1";
        String delay = null;//no delay

        createSmartAutomationScenario(automation, SmartAutomationActions.AddSetNumberofPoints,occurences,tags,delay);

        createNewMember();

        checkAutomationRunAndAddPointsToMember(occurences,tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionAddCreditOccurences1Delay1DayCheckNotPerform() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        amountType = AmountType.Credit;
        points = "10";
        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "1";
        String delay = "1";//no delay

        createSmartAutomationScenario(automation, SmartAutomationActions.AddSetNumberofPoints,occurences,tags,delay, TimingUnit.DAYS);

        createNewMember();

        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(POINTSTRASACTION);

        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.DAYS));
    }

    @Override
    protected SmartAutomationAction getAction(String actionName, SmartAutomationAction smartAction, ISmartAutomationCounter automation) throws Exception{
        SmartAutomationAction action = hubAssetsCreator.getAddPointsAction(smartAction, automation, amountType,points);
        return action;
    }

    @Override
    protected ISmartAction getConcreteAction(SmartAutomationAction action) {
        return action.getAddPoints();
    }


    protected void checkAutomationRunAndAddPointsToMember(String occurancesTimes,ArrayList<String> tags) throws Exception {
        int occurences = Integer.valueOf(occurancesTimes).intValue();
        int totalPoints = (Integer.valueOf(points).intValue() * occurences );
        checkDataStoreUpdatedWithPointsByRuleID(occurences,tags);

        //Verify the automation run and the points were added
        if(AmountType.Points.equals(amountType)) {
            Assert.assertTrue("The member did not get the points in the HUB sent by the automation, but appeared in the data store", MembersService.isPointsAdded("0", String.valueOf(totalPoints)));
            log4j.info("The member got the points sent by the automation");
        }else{
            Assert.assertTrue("The member did not get the points in the HUB sent by the automation, but appeared in the data store", MembersService.isPointsAddedToCredit(0,totalPoints));
            log4j.info("The member got the points sent by the automation");
        }
    }

    public void checkDataStoreUpdatedWithPointsByRuleID(int occurancesTimes,ArrayList<String> tags) throws Exception {

        log4j.info("check Data Store Updated with points");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the automation: " + automationRuleId,
                buildQuery(POINTSTRASACTION,predicate) , 1, dataStore);
        Assert.assertEquals("Must find one UserAction with the automation: " + automationRuleId, 1, res.size());
        Text t = ((Text)res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());
        int totalPoints = (Integer.valueOf(points).intValue() * occurancesTimes *100);
        if(AmountType.Points.equals(amountType)){
            Assert.assertTrue("Data must contain the Points : " + totalPoints, t.getValue().contains("Points=\"" + totalPoints + "\" "));
        }else{
            Assert.assertTrue("Data must contain the Points : " + totalPoints, t.getValue().contains("BudgetWeighted=\"" + totalPoints + "\" "));
        }

        checkActionTags(tags, res);
    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Finish test : " + AddPointsCreditActionTest.class.getName());
        try {
            checkSmartAutomationsAfterTestsFinished();
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
    }

}