package hub.smartAutomations.actions;

import com.google.appengine.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.TimingUnit;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.ArrayList;


/**
 * Created by Goni on 5/24/2017.
 *
 * * This class test using trigger 'Update membership details' to test all available uses of the 'Register' action in the smart automation
 */
public class RegisterActionTest extends BaseSmartActionTest{

    private static final String REGISTERED = "registered";
    private static final String JOINED_CLUB = "joinedclub";

    @Override
    public Triggers getTrigger() {
        return Triggers.UpdatesMembershipDetails;
    }

    @Override
    public String getAutomationName() {
        return "RegisterAction_" + timestamp;
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "register_" +timestamp;
        }
    };

    @Test
    @Category(Hub1Regression.class)
    public void testActionRegisterMemberDelay2secTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        automation.setNotRegisterChk("Yes");
        ArrayList<String> tags = Lists.newArrayList(tag,"tag2");

        String delay = "2";
        createScenarioWithoutOccurences(automation, SmartAutomationActions.RegisterMembership,tags,delay, TimingUnit.SECONDS);

        NewMember member = createNewMember();
        automationsHelper.unregisterMemberAndUpdate(member);


        checkAutomationRunAndRegisterMember(tags);

        checkTimingDefinition(delay);
    }



    @Test
    public void testActionRegisterMemberDelay1SecNoTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        automation.setNotRegisterChk("Yes");
        ArrayList<String> tags = Lists.newArrayList();
        String delay = "1";

        createScenarioWithoutOccurences(automation, SmartAutomationActions.UnregisterMembership,tags,delay,TimingUnit.SECONDS);

        NewMember member = createNewMember();
        automationsHelper.unregisterMemberAndUpdate(member);

        checkAutomationRunAndRegisterMember(tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionRegisterMemberNoDelayTag() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        automation.setNotRegisterChk("Yes");
        ArrayList<String> tags = Lists.newArrayList(tag);
        String delay = null;//no delay
        createScenarioWithoutOccurences(automation, SmartAutomationActions.UnregisterMembership,tags,delay,TimingUnit.SECONDS);

        NewMember member = createNewMember();
        automationsHelper.unregisterMemberAndUpdate(member);

        checkAutomationRunAndRegisterMember(tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionRegisterTagMemberDelay1HourCheckNotPerform() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        automation.setNotRegisterChk("Yes");
        ArrayList<String> tags = Lists.newArrayList(tag);
        String delay = "1";//no delay

        createScenarioWithoutOccurences(automation, SmartAutomationActions.UnregisterMembership,tags,delay,TimingUnit.HOURS);

        NewMember member = createNewMember();
        automationsHelper.unregisterMemberAndUpdate(member);

        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(JOINED_CLUB);

        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.HOURS));
    }


    @Override
    protected ISmartAction getConcreteAction(SmartAutomationAction action) {
        return action.getRegister();
    }


    @Override
    protected SmartAutomationAction getAction(String actionName, SmartAutomationAction smartAction, ISmartAutomationCounter automation) throws Exception{
        SmartAutomationAction action = hubAssetsCreator.getRegisterAction(smartAction, automation);
        return action;
    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Finish test : " + RegisterActionTest.class.getName());
        try {
            checkSmartAutomationsAfterTestsFinished();
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
    }
}
