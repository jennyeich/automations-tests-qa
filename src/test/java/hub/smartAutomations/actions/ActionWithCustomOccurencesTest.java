package hub.smartAutomations.actions;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.TimingTypes;
import hub.common.objects.common.TimingUnit;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.ICounter;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.member.MembersService;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.IServerResponse;
import server.v2_8.SubmitPurchase;
import server.v2_8.SubmitPurchaseResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Customer;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Goni on 5/28/2017.
 */
public class ActionWithCustomOccurencesTest extends BaseSmartActionTest{

    private static final Integer NO_LIMIT = null;


    @Override
    public Triggers getTrigger() {
        return Triggers.SubmitAPurchase;
    }

    @Override
    public String getAutomationName() {
        return "CustomOccurAction_" + timestamp;
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "customOccur_" +timestamp;
            if(smartGift == null) {
                //create assets
                try {
                    smartGift = hubAssetsCreator.createSmartGift(timestamp);

                } catch (Exception e) {
                    log4j.error("error while creating asset",e);
                }
            }
        }
    };

    @Test
    public void testActionSendAssetCustom3Foreach2InTotalQuantity10InGroupAllNoDelay() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "3";
        int forEach = 2;
        String quantity = "10";

        //create the send asset action with custom occurences 3 times with : foreach 2 item when total quantity =10 all purchase
        createSmartAutomationScenarioWithCustomActionNoDelayNoGroup(automation,occurences,tags,forEach,ForEachTotalType.TotalQuantity,NO_LIMIT);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),"2000",Integer.valueOf(10).intValue(),tags,transactionId);

        //we expect 15 assets
        checkAutomationRunAndSendAssetToMember(transactionId,getCalculatedOccurences(occurences,forEach,quantity,NO_LIMIT),tags);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testActionSendAssetCustom2Foreach2InTotalQuantity7InGroupAllLimit3() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "3";
        int forEach = 2;
        String quantity = "7";
        int limitTimes = 10;

        //create the send asset action with custom occurences 3 times with : foreach 2 item when total quantity =7 (not round result) all purchase
        createSmartAutomationScenarioWithCustomActionNoDelayNoGroup(automation,occurences,tags,forEach,ForEachTotalType.TotalQuantity,limitTimes);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),"1400",Integer.valueOf(7).intValue(),tags,transactionId);

        //we expect 9 assets
        checkAutomationRunAndSendAssetToMember(transactionId,getCalculatedOccurences(occurences,forEach,quantity,limitTimes),tags);
    }

    @Test
    public void testActionSendAssetCustom3Foreach2InTotalQuantityInGroupAllNoDelayLimit2() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "3";
        int forEach = 2;
        int limitTimes = 2;
        String quantity = "10";

        //create the send asset action with custom occurences 3 times with : foreach 2 item when total quantity =10 all purchase
        createSmartAutomationScenarioWithCustomActionNoDelayNoGroup(automation,occurences,tags,forEach,ForEachTotalType.TotalQuantity,limitTimes);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),"2000",Integer.valueOf(10).intValue(),tags,transactionId);

        //we expect 2 assets
        checkAutomationRunAndSendAssetToMember(transactionId,getCalculatedOccurences(occurences,forEach,quantity,limitTimes),tags);
    }


    @Test
    public void testActionSendAssetCustom3Foreach100InTotalSumInGroupAllNoDelay() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "3";
        int forEach = 100;
        String totalSum = "200";

        //create the send asset action with custom occurences 3 times with : foreach 100  when total sum = 200 all purchase
        createSmartAutomationScenarioWithCustomActionNoDelayNoGroup(automation,occurences,tags,forEach,ForEachTotalType.TotalSum,NO_LIMIT);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),totalSum,10,tags,transactionId);

        //we expect 6 assets
        checkAutomationRunAndSendAssetToMember(transactionId,getCalculatedOccurences(occurences,forEach,totalSum,NO_LIMIT),tags);
    }

    @Test
    public void testActionSendAssetCustom3Foreach50InTotalSumInGroupAllNoDelayLimit2() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "3";
        int forEach = 50;
        int limitTimes = 2;
        String totalSum = "300";

        //create the send asset action with custom occurences 3 times with : foreach 2 item when total quantity =10 all purchase
        createSmartAutomationScenarioWithCustomActionNoDelayNoGroup(automation,occurences,tags,forEach,ForEachTotalType.TotalSum,limitTimes);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),totalSum,5,tags,transactionId);

        //we expect 6 assets
        checkAutomationRunAndSendAssetToMember(transactionId,getCalculatedOccurences(occurences,forEach,totalSum,limitTimes),tags);
    }


    @Test
    public void testActionSendAssetCustom2Foreach3InTotalQuantityInGroupItemCodeEqlDelayNoLimit() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "2";
        int forEach = 3;
        String totalSumOfGroup = "6"; //3 items each quantity of 2 each
        String itemCode = "111";

        createScenarioWithCustomActionInGroup(automation,ForEachTotalType.TotalQuantity, tags, occurences, forEach, GroupField.ItemCode, GroupOperators.Equals, itemCode,NO_LIMIT);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createOneItem(itemCode, "Prod1", DEPARTMENT_CODE, "Dep1", 200, 2);
        Item item2 = createOneItem("222", "Prod1", DEPARTMENT_CODE, "Dep1", 100, 2);
        performAnActionSubmitPurchase(Lists.newArrayList(member),"900",Lists.newArrayList(item,item,item,item2,item2,item2),tags,transactionId);

        //we expect 4 assets
        checkAutomationRunAndSendAssetToMember(transactionId,getCalculatedOccurences(occurences,forEach,totalSumOfGroup,NO_LIMIT),tags);
    }

    @Test
    public void testActionSendAssetCustom2Foreach100InTotalSumInGroupItemCodeEqlDelayNoLimit() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "2";
        int forEach = 100;
        String totalSumOfGroup = "600";//3 items of 200 price
        String itemCode = "111";

        createScenarioWithCustomActionInGroup(automation,ForEachTotalType.TotalSum, tags, occurences, forEach,GroupField.ItemCode, GroupOperators.Equals, itemCode,NO_LIMIT);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createOneItem(itemCode, "Prod1", DEPARTMENT_CODE, "Dep1", 200, 2);
        Item item2 = createOneItem("222", "Prod1", DEPARTMENT_CODE, "Dep1", 100, 2);
        performAnActionSubmitPurchase(Lists.newArrayList(member),"900",Lists.newArrayList(item,item,item,item2,item2,item2),tags,transactionId);

        //we expect 12 assets
        checkAutomationRunAndSendAssetToMember(transactionId,getCalculatedOccurences(occurences,forEach,totalSumOfGroup,NO_LIMIT),tags);
    }

    @Test
    public void testActionSendAssetCustom2Foreach100InTotalSumInGroupItemCodeEqlDelayLimit2() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "2";
        int forEach = 100;
        String totalSumOfGroup = "600";//3 items of 200 price
        String itemCode = "111";
        int limitTimes = 2;

        createScenarioWithCustomActionInGroup(automation,ForEachTotalType.TotalSum, tags, occurences, forEach,GroupField.ItemCode, GroupOperators.Equals, itemCode,limitTimes);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createOneItem(itemCode, "Prod1", DEPARTMENT_CODE, "Dep1", 200, 2);
        Item item2 = createOneItem("222", "Prod1", DEPARTMENT_CODE, "Dep1", 100, 2);
        performAnActionSubmitPurchase(Lists.newArrayList(member),"900",Lists.newArrayList(item,item,item,item2,item2,item2),tags,transactionId);

        //we expect 2 assets
        checkAutomationRunAndSendAssetToMember(transactionId,getCalculatedOccurences(occurences,forEach,totalSumOfGroup,limitTimes),tags);
    }

    @Test
    public void testActionSendAssetCustom2Foreach3InTotalQuantityInGroupItemCodeEqlDelayLimit2() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "2";
        int forEach = 3;
        String totalSumOfGroup = "6"; //3 items each quantity of 2 each
        String itemCode = "111";
        int limitTimes = 2;

        createScenarioWithCustomActionInGroup(automation,ForEachTotalType.TotalQuantity, tags, occurences, forEach,GroupField.ItemCode, GroupOperators.Equals, itemCode,limitTimes);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createOneItem(itemCode, "Prod1", DEPARTMENT_CODE, "Dep1", 200, 2);
        Item item2 = createOneItem("222", "Prod1", DEPARTMENT_CODE, "Dep1", 100, 2);
        performAnActionSubmitPurchase(Lists.newArrayList(member),"900",Lists.newArrayList(item,item,item,item2,item2,item2),tags,transactionId);

        //we expect 2 assets
        checkAutomationRunAndSendAssetToMember(transactionId,getCalculatedOccurences(occurences,forEach,totalSumOfGroup,limitTimes),tags);
    }

    @Test
    public void testActionSendAssetCustom2Foreach6InTotalQuantityInGroupItemCodeIsOneOf() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "2";
        int forEach = 6;
        String totalSumOfGroup = "12"; //6 items each quantity of 2 each
        String itemCode = "111,222";

        createScenarioWithCustomActionInGroup(automation,ForEachTotalType.TotalQuantity, tags, occurences, forEach,GroupField.ItemCode, GroupOperators.Isoneof, itemCode,NO_LIMIT);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createOneItem("111", "Prod1", DEPARTMENT_CODE, "Dep1", 200, 2);
        Item item2 = createOneItem("222", "Prod1", DEPARTMENT_CODE, "Dep1", 100, 2);
        performAnActionSubmitPurchase(Lists.newArrayList(member),"900",Lists.newArrayList(item,item,item,item2,item2,item2),tags,transactionId);

        //we expect 4 assets
        checkAutomationRunAndSendAssetToMember(transactionId,getCalculatedOccurences(occurences,forEach,totalSumOfGroup,NO_LIMIT),tags);
    }

    @Test
    public void testActionSendAssetCustom2Foreach6InTotalQuantityInGroupItemCodeIsOneOfNegative() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "2";
        int forEach = 6;
        String totalSumOfGroup = "12"; //6 items each quantity of 2 each
        String itemCode = "111,222";

        createScenarioWithCustomActionInGroup(automation,ForEachTotalType.TotalQuantity, tags, occurences, forEach,GroupField.ItemCode, GroupOperators.Isoneof, itemCode,NO_LIMIT);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createOneItem("333", "Prod1", DEPARTMENT_CODE, "Dep1", 200, 2);
        Item item2 = createOneItem("444", "Prod1", DEPARTMENT_CODE, "Dep1", 100, 2);
        performAnActionSubmitPurchase(Lists.newArrayList(member),"900",Lists.newArrayList(item,item,item,item2,item2,item2),tags,transactionId);

        //we expect no asset - condition result is false
        checkDataStoreNegativeCheckByRuleID(RECEIVEDASSET);

    }

    @Test
    public void testActionSendAssetCustom2Foreach6InTotalQuantityInGroupItemNameNotEqOf() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "2";
        int forEach = 6;
        String totalSumOfGroup = "6"; //3 items each quantity of 2 with name "prod1"
        String itemName = "stam";

        createScenarioWithCustomActionInGroup(automation,ForEachTotalType.TotalQuantity, tags, occurences, forEach,GroupField.ItemName, GroupOperators.NotEquals, itemName,NO_LIMIT);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createOneItem("111", itemName, DEPARTMENT_CODE, "Dep1", 200, 2);
        Item item2 = createOneItem("222", "Prod1", DEPARTMENT_CODE, "Dep1", 100, 2);
        performAnActionSubmitPurchase(Lists.newArrayList(member),"900",Lists.newArrayList(item,item,item,item2,item2,item2),tags,transactionId);

        //we expect 2 assets
        checkAutomationRunAndSendAssetToMember(transactionId,getCalculatedOccurences(occurences,forEach,totalSumOfGroup,NO_LIMIT),tags);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testActionSendAssetCustom2Foreach6InTotalQuantityInGroupItemNameNotEqOfConditionShoppingCard() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.LessOrEqualsTo);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.Isoneof.toString(),"111",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "6",group);

        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        SmartAutomationAction action = new SmartAutomationAction();
        SmartAutomationAction smartAction = hubAssetsCreator.getSendAssetAction(action, automation, smartGift.getTitle());

        scenario.addCondition(cond1, automation);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "2";
        int forEach = 6;
        String totalSumOfGroup = "6"; //3 items each quantity of 2 with name "prod1"
        String itemName = "stam";


        setTags(automation, getConcreteAction(smartAction),tags);
        setCustomOccurences(automation, ForEachTotalType.TotalQuantity, occurences, forEach, GroupField.ItemName, GroupOperators.NotEquals, itemName,NO_LIMIT, smartAction);

        scenario.addAction(action,automation);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);


        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createOneItem("111", itemName, DEPARTMENT_CODE, "Dep1", 200, 2);
        Item item2 = createOneItem("222", "Prod1", DEPARTMENT_CODE, "Dep1", 100, 2);
        performAnActionSubmitPurchase(Lists.newArrayList(member),"900",Lists.newArrayList(item,item,item,item2,item2,item2),tags,transactionId);

        //we expect 2 assets
        checkAutomationRunAndSendAssetToMember(transactionId,getCalculatedOccurences(occurences,forEach,totalSumOfGroup,NO_LIMIT),tags);
    }

    @Test
    public void testActionSendAssetCustom2Foreach6InTotalQuantityInGroupItemNameNotEqOfDelay2Sec() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "2";
        int forEach = 6;
        String totalSumOfGroup = "6"; //3 items each quantity of 2 with name "prod1"
        String itemName = "stam";
        String delay = "2";

        createScenarioWithCustomActionInGroup(automation,ForEachTotalType.TotalQuantity, tags, occurences, forEach,GroupField.ItemName, GroupOperators.NotEquals, itemName,NO_LIMIT,true);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createOneItem("111", itemName, DEPARTMENT_CODE, "Dep1", 200, 2);
        Item item2 = createOneItem("222", "Prod1", DEPARTMENT_CODE, "Dep1", 100, 2);
        performAnActionSubmitPurchase(Lists.newArrayList(member),"900",Lists.newArrayList(item,item,item,item2,item2,item2),tags,transactionId);

        //we expect 2 assets
        checkAutomationRunAndSendAssetToMember(transactionId,getCalculatedOccurences(occurences,forEach,totalSumOfGroup,NO_LIMIT),tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionSendAssetCustom2Foreach6InTotalQuantityInGroupDepCodeEqOf() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "2";
        int forEach = 6;
        String totalSumOfGroup = "6"; //3 items each quantity of 2 with name "prod1"


        createScenarioWithCustomActionInGroup(automation,ForEachTotalType.TotalQuantity, tags, occurences, forEach,GroupField.DepartmentCode, GroupOperators.Equals, DEPARTMENT_CODE,NO_LIMIT);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createOneItem("111", "Prod1", DEPARTMENT_CODE, "Dep1", 200, 2);
        Item item2 = createOneItem("222", "Prod1", "333", "Dep1", 100, 2);
        performAnActionSubmitPurchase(Lists.newArrayList(member),"900",Lists.newArrayList(item,item,item,item2,item2,item2),tags,transactionId);

        //we expect 2 assets
        checkAutomationRunAndSendAssetToMember(transactionId,getCalculatedOccurences(occurences,forEach,totalSumOfGroup,NO_LIMIT),tags);
    }

    private void createScenarioWithCustomActionInGroup(SmartAutomation automation, ForEachTotalType totalType,ArrayList<String> tags, String occurences, int forEach,GroupField groupField, GroupOperators oper, String itemCode,Integer limitTimes) throws Exception {

        createScenarioWithCustomActionInGroup(automation,totalType,tags,occurences,forEach,groupField,oper, itemCode,limitTimes,false);
    }

    private void createScenarioWithCustomActionInGroup(SmartAutomation automation, ForEachTotalType totalType,ArrayList<String> tags, String occurences, int forEach,GroupField groupField, GroupOperators oper, String itemCode,Integer limitTimes,boolean delay) throws Exception {

        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction  action = hubAssetsCreator.getSendAssetAction(smartAction, automation, smartGift.getTitle());

        setTags(automation, getConcreteAction(action),tags);
        setCustomOccurences(automation, totalType, occurences, forEach, groupField, oper, itemCode, limitTimes, action);

        if(delay)
            setTiming(automation, getConcreteAction(action));
        scenario.addAction(action,automation);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);
    }

    private void setCustomOccurences(SmartAutomation automation, ForEachTotalType totalType, String occurences, int forEach, GroupField groupField, GroupOperators oper, String value, Integer limitTimes, SmartAutomationAction action) {

        Group group = createGroup(automation,groupField,oper,value);
        CustomOccurrences customOccurrences = automationsHelper.setCustomOccurences(automation, action.getSendAsset(),occurences,forEach, totalType,limitTimes);
        customOccurrences.setGroup(group);
    }

    protected void setTiming(SmartAutomation automation, ISmartAction smartAction) {
        Timing timing = new Timing(automation);
        timing.setTimingTypes( TimingTypes.DELAYED.toString());
        timing.setValueInput("2");
        timing.setTimingUnits(TimingUnit.SECONDS);
        smartAction.setTiming(timing);
    }


    private Item createOneItem(String itemCode, String itemName, String depCode, String depName, Integer itemPrice, Integer quantity) {

        Item item = new Item();
        item.setItemCode(itemCode);
        item.setQuantity(quantity);
        item.setAmount(1.2415);
        item.setItemName(itemName);
        item.setDepartmentCode(depCode);
        item.setDepartmentName(depName);
        item.setPrice(itemPrice);
        item.setTags(Lists.newArrayList("DEP1"));
        return item;
    }


    private void performAnActionSubmitPurchase(List<NewMember> members, String totalSum, ArrayList<Item> items,ArrayList<String> tags, String transactionId) throws Exception {


        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setPosId("1");
        submitPurchase.setBranchId(BRANCH_ID);
        submitPurchase.setTotalSum(totalSum);
        submitPurchase.setTransactionId(transactionId);
        submitPurchase.setChainId("chain123");
        submitPurchase.setCashier(EMPLOYEE + timestamp.toString());
        log4j.info("perform action submit purchase with TransactionID: " + transactionId);

        for (int i = 0; i < members.size(); i++) {
            Customer customer = new Customer();
            customer.setPhoneNumber(members.get(i).getPhoneNumber());
            submitPurchase.getCustomers().add(customer);
        }

        ArrayList<Item> itemsList = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            itemsList.add(items.get(i));
        }

        submitPurchase.setTags(tags);
        submitPurchase.setItems(itemsList);

        log4j.info("send request submit purchase");
        // Send the request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);
        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }
    }



    protected void createSmartAutomationScenarioWithCustomActionNoDelayNoGroup(SmartAutomation automation,String occurancesTimes,ArrayList<String> tags,int forEach,ForEachTotalType totalType,Integer limitTimes) throws Exception {
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction  action = hubAssetsCreator.getSendAssetAction(smartAction, automation, smartGift.getTitle());

        setTags(automation, getConcreteAction(action),tags);
        automationsHelper.setCustomOccurences(automation, action.getSendAsset(),occurancesTimes,forEach,totalType,limitTimes);
        scenario.addAction(action,automation);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

    }

    public Group createGroup (ICounter automation,GroupField groupField,GroupOperators oper,String value){

        String groupName = "group_" + timestamp;
        log4j.info("creating group: " + groupName);
        Group group = new Group(automation);
        group.setGroupName(groupName);
        //Add group conditions
        hubAssetsCreator.createGroupCondition(automation, groupField, oper.toString(), value, group);
        return group;
    }

    //calculates the number of actions that should be send to member accourding to the occurences,
    // num of items and total quantity or sum of the busket.
    //when we have limit and it smaller then the calculation result, it replace the result of (totalQuantitySum/forEachItem)*occurences
    private int getCalculatedOccurences(String occurancesTimes,int forEachItem,String total,Integer limitTimes) {
        int occurences = Integer.valueOf(occurancesTimes).intValue();
        int totalQuantitySum = Integer.valueOf(total).intValue();
        int numOfAssets = (totalQuantitySum/forEachItem)*occurences;
        if(limitTimes != NO_LIMIT && limitTimes.intValue() < numOfAssets)
            numOfAssets = limitTimes.intValue();

        return numOfAssets;
    }

    protected void checkAutomationRunAndSendAssetToMember(String transactionId,int numOfAssets,ArrayList<String> tags) throws Exception {

        checkDataStoreUpdatedWithAssetByTransactionId(transactionId,numOfAssets,tags);

        //Verify the automation run and the gift was sent
        Assert.assertTrue("The member did not get the gift in the HUB sent by the automation, but appeared in the data store", MembersService.isAllAssetsExist(smartGift.getTitle(),numOfAssets));
        log4j.info("The member got the gift sent by the automation");
    }




    @Override
    protected ISmartAction getConcreteAction(SmartAutomationAction action) {
        return action.getSendAsset();
    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Finish test : " + ActionWithCustomOccurencesTest.class.getName());
        try {
            checkSmartAutomationsAfterTestsFinished();
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
    }
}
