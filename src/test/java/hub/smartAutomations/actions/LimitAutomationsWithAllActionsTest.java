package hub.smartAutomations.actions;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.benefits.IAsset;
import hub.common.objects.benefits.LotteryReward;
import hub.common.objects.common.Automation_TimeUnits;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.ApplyLimitTimes;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.PerTimePeriod;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.smartAutomations.BaseAutomationTest;
import hub.utils.HubTemplateCreator;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import ru.yandex.qatools.allure.annotations.Issue;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.common.Response;
import server.internal.UpdateApiClient;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import static hub.services.NewApplicationService.getEnvProperties;


/**
 * Created by Jenny on 29/11/2017.
 *
 */
public class LimitAutomationsWithAllActionsTest extends BaseAutomationTest {

    private static IAsset activeAsset;
    private static LotteryReward lotteryReward;

    @Override
    public Triggers getTrigger() {
        return Triggers.SubmitAPurchase;
    }

    @Override
    public String getAutomationName() {
        return "Makes a purchase_" + timestamp;
    }
    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }


    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
            UpdateApiClient saveApiClientRequest = new UpdateApiClient(getEnvProperties().getServerToken());
            saveApiClientRequest.addProp(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(), true);

            IServerResponse saveApiClientResponse = saveApiClientRequest.sendRequestByLocation(locationId, Response.class);
            Assert.assertEquals("status code must be 200", saveApiClientResponse.getStatusCode(), (Integer) 200);

            if(smartGift == null) {
                try {
                    smartGift = hubAssetsCreator.createSmartGift(timestamp);
                    activeAsset = smartGift;

                } catch (Exception e) {
                    log4j.error(e);
                    Assert.fail("error while creating asset");
                }
            }
        }
    };



    @Test
    @Category(Hub1Regression.class)
    public void testMakesAPurchaseWithConditionShoppingCartTotalQuantityGroupCondWithItemCodeTagNotEqualsOfMemberWithLimitAutomation1() throws Exception {

        //Create smart automation
        createLimitTimesPerMember("1");

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.NotEqualto);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation, GroupField.ItemCode, GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "5",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreEventBasedAction(automationRuleId,1);
        //Trigger the automation again and make sure the automation is not running
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreEventBasedAction(automationRuleId,1);


    }


    //Test trigger make purchase with condition  Condition: Purchase/BranchID equals to 1 AND Punches/TotalSum greater than 200
    // then punch a punch card with automation limit
    @Test
    public void testPunchAPunchCardActionLimitAutomation2() throws Exception {

        smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "6");
        activeAsset = smartPunchCard;

        //Create smart automation
        createLimitTimesPerMember("1");

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.NotEqualto);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "5",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.PunchThePunchCard));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreEventBasedAction(automationRuleId,1);
        //Trigger the automation again and make sure the automation is not running
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        Thread.sleep(6000);
        checkDataStoreEventBasedAction(automationRuleId,1);

    }


    @Test
    public void testMakesAPurchaseWithConditionShoppingCartTotalQuantityGroupCondWithItemCodeTagNotEqualsOfMemberWithLimitAutomation3() throws Exception {

        //Create smart automation
        createLimitTimesPerMember("1");

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.NotEqualto);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.Equals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "5",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreDoesntContainsTheTag(0,transactionId);

        item = createItem("10000","itemA","depA", "depAA", 10000);
        item2 = createItem("3000","itemA","depA", "depAA", 10000);

        //Trigger the automation again and make sure the automation is running
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreEventBasedAction(automationRuleId,1);


        //Trigger the automation again and make sure the automation is not running
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreEventBasedAction(automationRuleId,1);


    }

    //Test automation limit with export event action
    @Test
    public void testExportEventActionLimitAutomation4() throws Exception {

        //Create smart automation
        createLimitTimesPerMember("1");

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.NotEqualto);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "5",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.ExportEvent));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreEventBasedAction(automationRuleId,1);
        //Trigger the automation again and make sure the automation is not running
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        Thread.sleep(6000);
        checkDataStoreEventBasedAction(automationRuleId,1);

    }

    //Test automation limit with push action
    @Test
    public void testPushActionLimitAutomation5() throws Exception {

        //Create smart automation
        createLimitTimesPerMember("2");

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "100",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.SendAPushNotification));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");

        Item item3 = createItem("1000","itemA","depA", "depAA", 1000);
        Item item4 = createItem("3000","itemA","depA", "depAA", 1000);
        //Trigger the automation again and make sure the automation is not running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item3,item4),1,"2000");

        //Verify the automation is running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2, "20000");

        //Verify the automation is not running since the limit was reached
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2, "20000");
    }

    //Test automation limit with pop up action
    @Test
    public void testPopUpActionLimitAutomation6() throws Exception {

        //Create smart automation
        createLimitTimesPerMember("2");

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "100",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.SendPopUPMessage));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");

        Item item3 = createItem("1000","itemA","depA", "depAA", 1000);
        Item item4 = createItem("3000","itemA","depA", "depAA", 1000);
        //Trigger the automation again and make sure the automation is not running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item3,item4),1,"2000");

        //Verify the automation is running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");

        //Verify the automation is not running since the limit was reached
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");
    }

    //Test automation limit with send asset action
    @Test
    public void testSendAssetActionLimitAutomation7() throws Exception {

        activeAsset = smartGift;

        //Create smart automation
        createLimitTimesPerMember("2");

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "100",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");

        Item item3 = createItem("1000","itemA","depA", "depAA", 1000);
        Item item4 = createItem("3000","itemA","depA", "depAA", 1000);
        //Trigger the automation again and make sure the automation is not running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item3,item4),1,"2000");

        //Verify the automation is running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");

        //Verify the automation is not running since the limit was reached
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");
    }

    //Test automation limit with lottery reward action
    @Test
    public void testSendLotteryRewardLimitAutomation8() throws Exception {
        lotteryReward = hubAssetsCreator.createLotteryReward(timestamp,smartGift.getTitle(),"5",null);
        activeAsset = lotteryReward;
        //Create smart automation
        createLimitTimesPerMember("2");

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "100",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.SendALotteryReward));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");

        Item item3 = createItem("1000","itemA","depA", "depAA", 1000);
        Item item4 = createItem("3000","itemA","depA", "depAA", 1000);
        //Trigger the automation again and make sure the automation is not running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item3,item4),1,"2000");

        //Verify the automation is running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2, "20000");

        //Verify the automation is not running since the limit was reached
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2, "20000");
    }



    //Test automation limit with send SMS action
    @Test
    public void testSendSMSActionLimitAutomation9() throws Exception {

        //Create smart automation
        createLimitTimesPerMember("2");

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "100",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.SendTextSMS));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");

        Item item3 = createItem("1000","itemA","depA", "depAA", 1000);
        Item item4 = createItem("3000","itemA","depA", "depAA", 1000);
        //Trigger the automation again and make sure the automation is not running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item3,item4),1,"2000");

        //Verify the automation is running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");

        //Verify the automation is not running since the limit was reached
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");
    }



    //Test automation limit with add points action
    @Test
    public void testAddPointsLimitAutomation10() throws Exception {

        createLimitTimesPerMember("2");

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "100",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.AddSetNumberofPoints));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");

        Item item3 = createItem("1000","itemA","depA", "depAA", 1000);
        Item item4 = createItem("3000","itemA","depA", "depAA", 1000);
        //Trigger the automation again and make sure the automation is not running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item3,item4),1,"2000");

        //Verify the automation is running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");

        //Verify the automation is not running since the limit was reached
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");
    }

    @Test
    @Category(Hub1Regression.class)
    public void testPerTimePeriodHours11 () throws Exception{

        createMaxOccurrencesPerTimePeriod("3","1","1",Automation_TimeUnits.HOURS);
        prepareAutomation();

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

        //Make sure the automation is not running since it is limites to 1 per hour
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");

        //The automation should not run since only 15 minutes passed
        updateDateEventBaseAction(automationRuleId,1, dateMinutesBack(15));
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");


        //Verify the automation is running since
        updateDateEventBaseAction(automationRuleId,1, dateMinutesBack(61));
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");

        //Verify the automation is not running since the limit was reached, this hour
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");

    }


    @Test
    public void testPerTimePeriodHours12 () throws Exception{

        createMaxOccurrencesPerTimePeriod("5","3","2",Automation_TimeUnits.HOURS);

        prepareAutomation();

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

        //Make sure the automation is running 3 times per 2 hours as defined
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),3,"20000");


        //The automation should not run since only 15 minutes passed
        updateDateEventBaseAction(automationRuleId,3, dateMinutesBack(75));
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),3,"20000");


        //Verify the automation is running since
        updateDateEventBaseAction(automationRuleId,3, dateMinutesBack(120));
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),4,"20000");

        //Verify the automation is not running since the limit was reached, this hour
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),4,"20000");


    }


    @Test
    public void testPerTimePeriodDays13 () throws Exception{

        createMaxOccurrencesPerTimePeriod("3","1","0",Automation_TimeUnits.DAYS);
        prepareAutomation();

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

        //Make sure the automation is not running since it is limits to 1 per day
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");

        //The automation should not run since only 15 minutes passed
        updateDateEventBaseAction(automationRuleId,1, hubAssetsCreator.dateHoursBack(2));
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");


        //Verify the automation is running since
        updateDateEventBaseAction(automationRuleId,1, hubAssetsCreator.dateHoursBack(23));
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");

        //Verify the automation is not running since the limit was reached, this hour
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");

    }


    @Test
    public void testPerTimePeriodDays14 () throws Exception{

        createMaxOccurrencesPerTimePeriod("5","3","0",Automation_TimeUnits.DAYS);
        prepareAutomation();

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

        //Make sure the automation is not running since it is limits to 1 per day
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),3,"20000");

        //The automation should not run since only 15 minutes passed
        updateDateEventBaseAction(automationRuleId,3, hubAssetsCreator.dateHoursBack(2));
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),3,"20000");


        //Verify the automation is running since
        updateDateEventBaseAction(automationRuleId,3, hubAssetsCreator.dateHoursBack(23));
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),4,"20000");

        //Verify the automation is not running since the limit was reached, this hour
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),4,"20000");

    }

    @Test
    @ru.yandex.qatools.allure.annotations.Description("Smart automation with limit time period week")
    public void testPerTimePeriodWeeks15 () throws Exception{

        createMaxOccurrencesPerTimePeriod("5","3","0",Automation_TimeUnits.WEEKS);
        prepareAutomation();

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

        //Make sure the automation is not running since it is limits to 5  every 3 weeks
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),3,"20000");

        //The automation should not run since this is in the same day/week
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),3,"20000");


        //Verify the automation is running since
        updateDateEventBaseAction(automationRuleId,3, hubAssetsCreator.date1WeekBack());
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),4,"20000");

        //Verify the automation is not running since the limit was reached, this hour
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),4,"20000");

    }

    @Test
    public void testPerTimePeriodMonths16 () throws Exception{

        createMaxOccurrencesPerTimePeriod("7","5","0",Automation_TimeUnits.MONTHS);
        prepareAutomation();

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

        //Make sure the automation is not running since it is limits to 1 per day
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),3,"20000");
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),4,"20000");
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),5,"20000");

        //The automation should not run since only 15 minutes passed
        updateDateEventBaseAction(automationRuleId,5, hubAssetsCreator.dateDaysBack(31));
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),6,"20000");


        //Verify the automation is not running since the limit was reached, this hour
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),6,"20000");

    }

    @Test
    public void testPerTimePeriodYears17 () throws Exception{

        createMaxOccurrencesPerTimePeriod("8","5","0",Automation_TimeUnits.YEARS);
        prepareAutomation();

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

        //Make sure the automation is not running since it is limits to 1 per day
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),3,"20000");
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),4,"20000");
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),5,"20000");

        //The automation should not run
        updateDateEventBaseAction(automationRuleId,5,hubAssetsCreator.dateYearBack());
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),6,"20000");

        //Verify the automation is not running since the limit was reached
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),6,"20000");

    }

    //Test automation limit with send sms action
    @Test
    public void testSendSMSActionLimitAutomation18() throws Exception {

        //Create smart automation
        createLimitTimesPerMember("3");

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "100",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.SendTextSMS));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");

        Item item3 = createItem("1000","itemA","depA", "depAA", 1000);
        Item item4 = createItem("3000","itemA","depA", "depAA", 1000);
        //Trigger the automation again and make sure the automation is not running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item3,item4),1,"2000");

        //Verify the automation is running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");

        //Verify the automation is running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),3,"20000");

        //Verify the automation is not running since the limit was reached
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),3,"20000");
    }

    //Test automation limit with register action
    @Test
    public void testRegisterLimitAutomation19() throws Exception {

        //Create smart automation
        createLimitTimesPerMember("3");

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "100",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.RegisterMembership));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");

        Item item3 = createItem("1000","itemA","depA", "depAA", 1000);
        Item item4 = createItem("3000","itemA","depA", "depAA", 1000);
        //Trigger the automation again and make sure the automation is not running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item3,item4),1,"2000");

        //Verify the automation is running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");

        //Verify the automation is running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),3,"20000");

        //Verify the automation is not running since the limit was reached
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),3,"20000");
    }

    //Test automation limit with mail action
    @Test
    public void testSendMailActionLimitAutomation20() throws Exception {

        templateName = "template_" + timestamp;
        template = HubTemplateCreator.createImageTemplate(templateName);
        activeAsset = template;

        //Create smart automation
        createLimitTimesPerMember("2");

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "100",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.SendEmail));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),1,"20000");

        Item item3 = createItem("1000","itemA","depA", "depAA", 1000);
        Item item4 = createItem("3000","itemA","depA", "depAA", 1000);
        //Trigger the automation again and make sure the automation is not running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item3,item4),1,"2000");

        //Verify the automation is running
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");

        //Verify the automation is not running since the limit was reached
        submitPurchaseAndVerifyResult(member,transactionId,Lists.newArrayList(item,item2),2,"20000");
    }

    @Override
    protected SmartAutomationAction getAction(String actionName, SmartAutomationAction smartAction, ISmartAutomationCounter automation) throws Exception{
        SmartAutomationAction action = automationsHelper.getAction(actionName,smartAction,automation,activeAsset.getTitle(),"");
        return action;
    }

    private void prepareAutomation () throws Exception{

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "100",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.AddSetNumberofPoints));

    }

    private Date dateMinutesBack (int minutes){

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, - minutes);
        Date date = cal.getTime();
        return date;

    }


    private void  submitPurchaseAndVerifyResult (NewMember member, String transactionId,ArrayList<Item> items, int expectedResults, String totalSum) throws Exception {

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),totalSum, Lists.newArrayList("DEP-1"),transactionId,items);
        Thread.sleep(10000);
        checkDataStoreEventBasedAction(automationRuleId,expectedResults);

    }


    private void createLimitTimesPerMember(String limit){

        //Create smart automation
        ApplyLimitTimes applyLimitTimes = new ApplyLimitTimes();
        applyLimitTimes.setApplyLimitTimes("Y");
        applyLimitTimes.setPerMemberCheckBox("Y");
        applyLimitTimes.setPerMemberTimes(limit);
        settings.setApplyLimitTimes(applyLimitTimes);
    }


    private void createMaxOccurrencesPerTimePeriod (String limit, String maxOccurrences,String timePeriod,Automation_TimeUnits units) {

        //Create smart automation
        ApplyLimitTimes applyLimitTimes = new ApplyLimitTimes();
        applyLimitTimes.setApplyLimitTimes("Y");
        applyLimitTimes.setPerMemberCheckBox("Y");
        applyLimitTimes.setPerMemberTimes(limit);

        PerTimePeriod perTimePeriod = new PerTimePeriod();
        perTimePeriod.setMaxOccurrencesPerTimePeriod(maxOccurrences);
        if (units.equals(Automation_TimeUnits.HOURS))
            perTimePeriod.setTimePeriodNumber(timePeriod);
        perTimePeriod.setTimePeriodUnits(units);
        applyLimitTimes.setPerTimePeriod(perTimePeriod);

        settings.setApplyLimitTimes(applyLimitTimes);

    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

}
