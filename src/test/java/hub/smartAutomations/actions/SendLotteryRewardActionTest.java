package hub.smartAutomations.actions;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.benefits.LotteryReward;
import hub.common.objects.common.TimingUnit;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.common.objects.smarts.SmartAutomation;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.ArrayList;

/**
 * Created by Goni on 5/22/2017.
 *
 * This class test using trigger 'Join The Club' to test all available uses of the 'Send Lottery Reward' action in the smart automation
 */
public class SendLotteryRewardActionTest extends BaseSmartActionTest{

    private static LotteryReward lotteryReward;

    @Override
    public String getAutomationName() {
        return "SendLotteryRewardAction_" + timestamp;
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "lottery_" +timestamp;
            try {
               createSmartGift();

                lotteryReward = hubAssetsCreator.createLotteryReward(lotteryReward,smartGift.getTitle(),timestamp);

            } catch (Exception e) {
                log4j.error("error while creating asset",e);
            }
        }
    };

    //in this test we create lottery reward that is limited to 1 use.
    //When creating automation with 3 occurences we expect that the member will only get one asset since it is limited.
    @Test
    public void testActionSendLotteryRewardOccurences3LotteryLimitedTo1() throws Exception {

        //create lottery with max use 1 for the current test
        lotteryReward = hubAssetsCreator.createLotteryReward("temp" + timestamp,smartGift.getTitle(),"1","1");

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "3";
        String delay = null;
        createSmartAutomationScenario(automation, SmartAutomationActions.SendALotteryReward,occurences,tags,delay);
        //Create member
        createNewMember();

        //check that the user get only 1 asset
        checkAutomationRunAndSendAssetToMember("1",tags);

        checkTimingDefinition(delay);
    }


    @Test
    @Category(Hub1Regression.class)
    public void testActionSendLotteryRewardOccurences3Delay5SecTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList("tag1",tag);
        String occurences = "3";
        String delay = "5";
        createSmartAutomationScenario(automation, SmartAutomationActions.SendALotteryReward,occurences,tags,delay);
        //Create member
        createNewMember();

        checkAutomationRunAndSendAssetToMember(occurences,tags);

        checkTimingDefinition(delay);
    }


    @Test
    public void testActionSendLotteryRewardOccurences2Delay5SecNoTags() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList();
        String occurences = "2";
        String delay = "5";

        createSmartAutomationScenario(automation, SmartAutomationActions.SendALotteryReward,occurences,tags,delay);

        createNewMember();

        checkAutomationRunAndSendAssetToMember(occurences,tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionSendLotteryRewardOccurences1NoDelayTag() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "1";
        String delay = null;//no delay

        createSmartAutomationScenario(automation, SmartAutomationActions.SendALotteryReward,occurences,tags,delay);

        createNewMember();

        checkAutomationRunAndSendAssetToMember(occurences,tags);

        checkTimingDefinition(delay);
    }

    @Test
    public void testActionSendLotteryRewardOccurences1Delay1HourCheckNotPerform() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList(tag);
        String occurences = "1";
        String delay = "1";//no delay

        createSmartAutomationScenario(automation, SmartAutomationActions.SendALotteryReward,occurences,tags,delay, TimingUnit.HOURS);

        createNewMember();

        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(RECEIVEDASSET);

        checkTimingDefinition(getCalculatedDelay(delay,TimingUnit.HOURS));
    }


    @Test
    public void testActionSendLotteryRewardOccurences1TagsWithSpaces() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        ArrayList<String> tags = Lists.newArrayList("tag-with-space",tag);
        String occurences = "1";
        String delay = null;//no delay

        //we send tag with spaces and we want to check that it is written with '-' instead
        createSmartAutomationScenario(automation, SmartAutomationActions.SendALotteryReward,occurences,Lists.newArrayList("tag with space",tag),delay);

        createNewMember();


        checkAutomationRunAndSendAssetToMember(occurences,tags);

        checkTimingDefinition(delay);
    }


    @Override
    protected ISmartAction getConcreteAction(SmartAutomationAction action) {
        return action.getLotteryReward();
    }


    @Override
    protected SmartAutomationAction getAction(String actionName, SmartAutomationAction smartAction, ISmartAutomationCounter automation) throws Exception{
        SmartAutomationAction action = hubAssetsCreator.getSendLotteryRewardAction(smartAction, automation,lotteryReward.getTitle());
        return action;
    }

    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

    @AfterClass
    public static void tearDown(){
        log4j.info("Finish test : " + SendLotteryRewardActionTest.class.getName());
        try {
            checkSmartAutomationsAfterTestsFinished();
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
    }


}
