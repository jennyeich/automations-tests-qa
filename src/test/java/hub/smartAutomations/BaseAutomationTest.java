package hub.smartAutomations;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.common.collect.Lists;
import groovy.util.logging.Log4j;
import hub.base.BaseHubTest;
import hub.base.ISmartAutomationTest;
import hub.common.objects.common.TextAreaParameters;
import hub.common.objects.content.Template;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.common.objects.member.PerformSmartActionOnMember;
import hub.common.objects.member.PerformSmartActionOnResults;
import hub.common.objects.member.performActionObjects.TagUntagOperation;
import hub.common.objects.member.performSmartActionObjects.SendAnAssetAction;
import hub.common.objects.member.performSmartActionObjects.TagOrUnTagAction;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.SmartPunchCard;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AmountType;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.common.objects.smarts.smarObjects.settings.ISettings;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Condition;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.AcpService;
import hub.services.member.MembersService;
import hub.utils.HubAssetsCreator;
import hub.utils.SmartAutomationsHelper;
import integration.base.BaseIntegrationTest;
import org.apache.commons.collections.map.HashedMap;
import org.jetbrains.annotations.NotNull;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import server.common.IServerResponse;
import server.common.InvokeEventBasedActionRule;
import server.common.InvokeEventBasedActionRuleBuilder;
import server.common.Response;
import server.utils.ServerHelper;
import server.utils.UserActionsConstants;
import server.utils.V4Builder;
import server.v2_8.*;
import server.v2_8.base.IServerErrorResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Customer;
import server.v2_8.common.Item;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by Goni on 4/26/2017.
 */
public abstract class BaseAutomationTest extends BaseIntegrationTest implements ISmartAutomationTest{

    public static final String INACTIVE = "inactive";

    public static final String MOCK_EMAIL = "mockemail@coco.com";
    public static final String MAIL_SUFFIX = "@como.com";
    public static final String MEMBER_TAG_VIP= "VIP";
    public static final String MEMBER_TAG_ANYUSER= "ANY_USER";
    public static final boolean AND = false;
    public static final boolean OR = true;
    public static final String EMPLOYEE = "Employee";
    public static final String BRANCH_ID = "1";
    public static final String DEPARTMENT_CODE ="123";
    public static final String ITEM_NAME_PREFIX = "item_";

    //UserActions action name
    public static final String TAG_OPERATION = UserActionsConstants.TAG_OPERATION;
    public static final String RECEIVEDASSET = UserActionsConstants.RECEIVEDASSET;
    public static final String PUNCH = UserActionsConstants.PUNCH;
    public static final String POINTSTRASACTION = UserActionsConstants.POINTSTRASACTION;
    public static final String EXCEEDED_PUNCH = UserActionsConstants.EXCEEDED_PUNCH;
    public static final String UNREGISTER =UserActionsConstants.UNREGISTER;
    public static final String JOINED_CLUB =UserActionsConstants.JOINED_CLUB;
    public static final String PUSH_OPERATION = UserActionsConstants.PUSH_FAILED;
    public static final String PUSH_SENT_OPERATION = UserActionsConstants.PUSH_SENT;
    public static final String MEMBER_SMS_OPERATION = UserActionsConstants.MEMBER_SMS;
    public static final String SEND_POPUP_OPERATION = UserActionsConstants.SEND_POPUP_MSG;
    public static final String EVENT_EXPORTED_OPERATION = UserActionsConstants.EVENT_EXPORTED;
    public static final String FORM_FILLED_OPERATION = UserActionsConstants.FORM_FILLED;
    public static final String REGISTERED = "registered";


    public static final String AUTOMATION_RULE_PREFIX = "automation_";
    public static final String DEFAULT_SCENARIO_NAME = "NEW SCENARIO";

    public ISettings settings;
    public CharSequence timestamp;
    public static SmartGift smartGift;
    public static SmartPunchCard smartPunchCard;
    public static String templateName;
    public static Key key = null;
    public String tag = "tag_";
    public static Template template;

    public String automationRuleId;
    public SmartAutomationsHelper automationsHelper = new SmartAutomationsHelper(locationId,apiKey,serverToken);
    public V4Builder v4Builder = new V4Builder(apiKey);

    public HubAssetsCreator hubAssetsCreator = new HubAssetsCreator();

    @BeforeClass
    public static void setup() throws Exception {
        init();
    }

    @AfterClass
    public static void tearDown() {
        if (suiteRunning) {
            log4j.info("Finish Suite tests");
        } else {
            log4j.info("Finish test : " + BaseAutomationTest.class.getName());
            try {
                checkSmartAutomationsAfterTestsFinished();
                //AcpService.uncheckApplication(locationId);
            } catch (Exception e) {
                log4j.error(e.getMessage(), e);
            }
            log4j.info("close browser after finish test");
            cleanBase();
            BaseHubTest.cleanHub();
        }
    }
    public void createSmartGift() {
        timestamp = String.valueOf(System.currentTimeMillis());
        if(smartGift == null) {
            //create assets
            try {
                smartGift = hubAssetsCreator.createSmartGift(timestamp);

            } catch (Exception e) {
                log4j.error(e);
                Assert.fail("error while creating asset");
            }
        }
    }

    public void createSmartPunchCard(String numOfPunches) {

        try {
            smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, numOfPunches);

        } catch (Exception e) {
            log4j.error(e);
            Assert.fail("error while creating asset");
        }

    }

    //Check that no user action was created for this action
    public void checkDataStoreNegativeCheckByRuleID(String actionName) throws Exception {
        //give the data store time to update
        Thread.sleep(5000);
        log4j.info("check DataStore not updated with :" + actionName);
        List<Entity> res = runQuery(actionName, new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId));
        Assert.assertEquals("The automation: " + automationRuleId + " is not supposed to run:", 0, res.size());

    }


    protected void checkAutomationRunAndTagMember() throws Exception {

        checkDataStoreUpdatedWithTagByRuleID();

        //Verify the automation run and the tag exists
        Assert.assertTrue("The member did not get the tag in the HUB sent by the automation, but appeared in the data store", MembersService.isTagExists(tag));
        log4j.info( "The member got the tag -> the automation run");
    }
    protected void checkAutomationRunAndTagMemberByTransaction(String transactionId) throws Exception {

        checkDataStoreUpdatedWithTag(transactionId);

        //Verify the automation run and the tag exists
        Assert.assertTrue("The member did not get the tag in the HUB sent by the automation, but appeared in the data store", MembersService.isTagExists(tag));
        log4j.info( "The member got the tag -> the automation run");
    }

    protected void checkDataStoreUpdatedWithTag(String transactionId) throws Exception {
        checkDataStoreUpdatedWithTag(1, transactionId);
    }

    protected void checkDataStoreUpdatedWithTag(int numOfRaws, String transactionId) throws Exception {
        log4j.info("check Data Store Updated with Tag");
        List<Entity> res = ServerHelper.queryWithWait("Must find " + numOfRaws + " UserAction with the TransactionID: " + transactionId.toString(),
                buildQuery(TAG_OPERATION, transactionId), numOfRaws, dataStore);

        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info("ExportData contains the following data : " + t.getValue());
        Assert.assertTrue("Data must contain the tag: " + tag, t.getValue().contains(tag));
    }

    public void checkAutomationRunAndSendPushNotification() throws Exception {

        checkAutomationRunAndSendPushNotification(1);

    }

    public void checkAutomationRunAndSendPushNotification(int expectedResults) throws Exception {

        log4j.info("check Data Store Updated with push action");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find UserAction with the automation: " + automationRuleId,
                buildQuery(UserActionsConstants.PUSH_FAILED,predicate) , expectedResults, dataStore);
        Assert.assertEquals("Must find " + expectedResults + " UserAction with the automation: " + automationRuleId, expectedResults, res.size());

    }

    public void checkAutomationRunAndSendMemberSMS() throws Exception {

        log4j.info("check Data Store Updated with send member sms action");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWaitUntilResultEqOrGreat("Must find at least one UserAction with the RuleID: " + automationRuleId,
                buildQuery(UserActionsConstants.MEMBER_SMS,predicate), 1, dataStore,200000);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info("ExportData contains the following data : " + t.getValue());
        System.out.println("Data: " + t.getValue());
        Assert.assertTrue("Must find at least one UserAction with the automation: " + automationRuleId, res.size()>0);
    }

    public void checkDataStoreUpdatedWithSendAction(String transactionId, String operation) throws InterruptedException {
        log4j.info("check Data Store Updated with action");
        List<Entity> res = ServerHelper.queryWithWaitUntilResultEqOrGreat("Must find at least one UserAction with the TransactionID: " + transactionId,
                buildQuery(operation, transactionId), 1, dataStore,200000);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info("ExportData contains the following data : " + t.getValue());
        System.out.println("Data: " + t.getValue());
    }


    protected void  checkDataStoreEventBasedAction (String automationRuleId, int expectedNumOfResults)throws Exception{

        log4j.info("Verify the number of run automations is as expected: " + expectedNumOfResults + "automationRuleID: "  + automationRuleId);
        //Verify with DataStore
        Query q = new Query("EventBasedAction");
        q.setFilter(new Query.FilterPredicate("RuleID",Query.FilterOperator.EQUAL, automationRuleId));
        List<Entity>result = serverHelper.queryWithWait("Must find automation runs : " + expectedNumOfResults, q, expectedNumOfResults, dataStore);
        int sizeResults = result.size();
        log4j.info("Number of automations which runned: " + sizeResults);
        Assert.assertEquals(expectedNumOfResults,sizeResults);

    }

    protected void updateDateEventBaseAction (String automationRuleId, int expectedNumOfResults,Date date) throws Exception{

        log4j.info("Verify the number of run automations is as expected: " + expectedNumOfResults);
        //Verify with DataStore
        Query q = new Query("EventBasedAction");
        q.setFilter(new Query.FilterPredicate("RuleID",Query.FilterOperator.EQUAL, automationRuleId));
        List<Entity>result = serverHelper.queryWithWait("Must find automation runs : " + expectedNumOfResults, q, expectedNumOfResults, dataStore);
        Entity e = result.get(0);

        log4j.info("changing the <CreatedOn> value for ruleID:" + automationRuleId  + " to:" +  date.toString());
        serverHelper.updateEntity(dataStore,e,"CreatedOn", date);
        Thread.sleep(10000);
    }

    protected void updateDateForMember (String phoneNumber,Date date) throws Exception{

        //Verify with DataStore
        Query q = new Query("UserAtLocation");
        q.setFilter(new Query.FilterPredicate("PhoneNumber",Query.FilterOperator.EQUAL, phoneNumber));
        List<Entity>result = serverHelper.queryWithWait("Must find one user", q, 1, dataStore);
        Entity e = result.get(0);

        log4j.info("changing the <CreatedOn> value for member with phone:" + phoneNumber  + " to:" +  date.toString());
        serverHelper.updateEntity(dataStore,e,"CreatedOn", date);
        Thread.sleep(10000);


    }
    protected void checkDataStoreDoesntContainsTheTag(int numOfRaws, String transactionId) throws Exception {
        log4j.info("check Data Store Updated with Tag");
        List<Entity> res = ServerHelper.queryWithWait("Must find " + numOfRaws + " UserAction with the TransactionID: " + transactionId.toString(),
                buildQuery(TAG_OPERATION, transactionId), numOfRaws, dataStore);
        Assert.assertTrue("Data doesn't contain any rows", res.isEmpty());
    }

    protected void checkDataStoreUpdatedWithBudget(String transactionId, AmountType amountType ,String amount) throws Exception {
        log4j.info("check Data Store Updated with budget");
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the TransactionID: " + transactionId,
                buildQuery(POINTSTRASACTION, transactionId), 1, dataStore);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info( "ExportData contains the following data : " + t.getValue());
        Assert.assertTrue("Data must contain"  + amountType.toString() + "=" +  amount, t.getValue().contains(amountType.toString() +"=\"" + amount +"\""));
    }

    protected void checkDataStoreUpdatedWithBudgetWeight(String transactionId ,String BudgetWeighted) throws Exception {
        log4j.info("check Data Store Updated with budget");
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the TransactionID: " + transactionId,
                buildQuery(POINTSTRASACTION, transactionId), 1, dataStore);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info( "ExportData contains the following data : " + t.getValue());
        Assert.assertTrue("Data must contain the BudgetWeighted:" + BudgetWeighted , t.getValue().contains("BudgetWeighted=\"" + BudgetWeighted + "\""));
    }

    protected Query buildQuery(String actionName, String transactionId){
        return buildQuery(actionName,new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, transactionId));
    }

    public void checkDataStoreUpdatedWithPointsByRuleID(String points,AmountType amountType) throws Exception {
        List<Entity> res = checkDataStoreUpdatedWithPointsByRuleID(1);
        Text t = ((Text)res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());

        if(AmountType.Points.equals(amountType)){
            Assert.assertTrue("Data must contain the Points : " + points, t.getValue().contains("Points=\"" + points + "\" "));
        }else{
            Assert.assertTrue("Data must contain the BudgetWeighted : " + points, t.getValue().contains("BudgetWeighted=\"" + points + "\" "));
        }

    }

    public List<Entity> checkDataStoreUpdatedWithPointsByRuleID(int numOfExpectedResults) throws Exception {
        log4j.info("check Data Store Updated with points");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find:" + numOfExpectedResults + " UserAction with the automation: " + automationRuleId,
                buildQuery(POINTSTRASACTION,predicate) , numOfExpectedResults, dataStore);
        Assert.assertEquals("Must find:" + numOfExpectedResults + " UserAction with the automation: " + automationRuleId, numOfExpectedResults, res.size());
        return res;
    }

    protected void checkDataStoreUpdatedWithSendPushNotification(int numOfActions) throws Exception {

        log4j.info("check Data Store Updated with push");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find " + numOfActions + " UserAction with the automation ID: " + automationRuleId,
                buildQuery(PUSH_OPERATION, predicate), numOfActions, dataStore);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info("ExportData contains the following data : " + t.getValue());
    }

    protected void checkAutomationRunAndSendAssetToMember() throws Exception {
       checkAutomationRunAndSendAssetToMember(smartGift.getTitle());
    }

    protected void checkAutomationRunAndSendAssetToMember(String assetName) throws Exception {
        checkDataStoreUpdatedWithAssetByRuleID();
        TimeUnit.SECONDS.sleep(10);
        //Verify the automation run and the gift was sent
        Assert.assertTrue("The member did not get the gift in the HUB sent by the automation, but appeared in the data store", MembersService.isGiftExists(assetName));
        log4j.info("The member got the gift sent by the automation");
    }

    protected void checkAutomationRunAndSendAssetToMember(String assetName, int numOfActionRun) throws Exception {
        checkDataStoreUpdatedWithAsset(numOfActionRun);

        //Verify the automation run and the gift was sent
        Assert.assertTrue("The member did not get the gift in the HUB sent by the automation, but appeared in the data store", MembersService.isGiftExists(assetName));
        log4j.info("The member got the gift sent by the automation");
    }

    protected void checkAutomationRunAndPunchedThePunchCard(String assetName, int numOfActionRun) throws Exception {
        checkDataStoreUpdatedWithPunchByRuleID(numOfActionRun);

        //Verify the automation run and punch the punch card
        Assert.assertTrue("Punch action didn't occure", MembersService.isGiftExists(assetName));
        log4j.info("The member got the gift sent by the automation");

    }

    public void checkDataStoreUpdatedWithAsset(int numOfActionRun) throws Exception {

        log4j.info("check Data Store Updated with asset");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find " + numOfActionRun + " UserAction with the automation: " + automationRuleId,
                buildQuery(RECEIVEDASSET, predicate),numOfActionRun, dataStore);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " + t.getValue());
        Assert.assertTrue("Data must contain the assetType: ", t.getValue().contains("AssetType=\"gift\""));
        Assert.assertTrue("Data must contain the asset source: ", t.getValue().contains("Source=\"Automation\""));
        Assert.assertEquals("Source is automation", "Automation", res.get(0).getProperty("Source"));
    }


    public void checkDataStoreUpdatedWithExceededPunch(String exceededPunches) throws InterruptedException {
        log4j.info("check Data Store Updated with EXCEEDED_PUNCH");
        Query.FilterPredicate predicate = new Query.FilterPredicate("LocationID", Query.FilterOperator.EQUAL, locationId);
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the action: " + EXCEEDED_PUNCH,
                buildQuery(EXCEEDED_PUNCH,predicate) , 1, dataStore);
        Assert.assertEquals("Must find 1 UserAction with the automation: " + automationRuleId, 1, res.size());
        Text t = ((Text)res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());
        Assert.assertTrue("Data must contain the NumOfPunches: ",t.getValue().contains("NumOfPunches=\"" +exceededPunches + "\""));
        Assert.assertTrue("Data must contain the AssetTemplateName: ", t.getValue().contains("AssetTemplateName=\"" +smartPunchCard.getTitle() + "\""));

    }

    public void checkDataStoreNotUpdatedWithExceededPunch() throws Exception{

        //give the data store time to update as if automation was running - delay of 20 seconds
        Thread.sleep(20000);
        log4j.info("check DataStore not updated with :" + EXCEEDED_PUNCH);
        Query.FilterPredicate predicate = new Query.FilterPredicate("LocationID", Query.FilterOperator.EQUAL, locationId);
        List<Entity> res = runQuery(EXCEEDED_PUNCH,predicate);
        Assert.assertEquals("The automation is not supposed to run:", 0, res.size());
    }

    public void checkDataStoreUpdatedWithAssetByRuleID() throws Exception {

        log4j.info("check Data Store Updated with asset");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the automation: " + automationRuleId,
                buildQuery(RECEIVEDASSET,predicate) , 1, dataStore);
        Assert.assertEquals("Must find one UserAction with the automation: " + automationRuleId, 1, res.size());
        Text t = ((Text)res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());
        Assert.assertTrue("Data must contain the assetType: ", t.getValue().contains("AssetType=\"gift\""));
        Assert.assertTrue("Data must contain the asset source: ", t.getValue().contains("Source=\"Automation\""));
        Assert.assertEquals("Source is automation","Automation",res.get(0).getProperty("Source"));
    }


    public  void checkDataStoreUpdatedWithTagByRuleID() throws Exception {
        log4j.info("check Data Store Updated with Tag");
        checkDataStoreUpdatedWithTagByRuleID(tag);
    }

    public void checkDataStoreUpdatedWithTagByRuleID(String tagName) throws Exception {

        log4j.info("check Data Store Updated with Tag");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the automation: " + automationRuleId,
                buildQuery(TAG_OPERATION,predicate) , 1, dataStore);
        Assert.assertEquals("Must find one UserAction with the automation: " + automationRuleId, 1, res.size());
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info( "ExportData contains the following data : " + t.getValue());
        Assert.assertTrue("Data must contain the tag: " + tagName, t.getValue().contains(tagName));
    }


    public void checkDataStoreUpdatedWithPunchByRuleID(int numOfActionRun) throws Exception {
        log4j.info("check Data Store Updated with Punch");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the automation: " + automationRuleId,
                buildQuery(PUNCH,predicate) , numOfActionRun, dataStore);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " + t.getValue());
        Assert.assertTrue("Data must contain the assetType: ", t.getValue().contains("AssetType=\"punch_card\""));
    }

    public void checkDataStoreUpdatedWithPunchByRuleID() throws Exception {
        log4j.info("check Data Store Updated with Punch");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the automation: " + automationRuleId,
                buildQuery(PUNCH,predicate) , 1, dataStore);
        Assert.assertEquals("Must find one UserAction with the automation: " + automationRuleId, 1, res.size());

    }


    public void checkDataStoreUpdatedWithTagByTransaction(String transactionId ,String tagName) throws Exception {

        log4j.info("check Data Store Updated with Tag");
        Query.FilterPredicate predicate = new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, transactionId);
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the transactionId: " + transactionId,
                buildQuery(TAG_OPERATION,predicate) , 1, dataStore);
        Assert.assertEquals("Must find one UserAction with the transaction: " + transactionId, 1, res.size());
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info( "ExportData contains the following data : " + t.getValue());
        Assert.assertTrue("Data must contain the tag: " + tagName, t.getValue().contains(tagName));
    }

    public  void checkDataStoreUpdatedWithTag(String tagName,int numOfActionRun) throws Exception {

        log4j.info("check Data Store Updated with Tag");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find " + numOfActionRun + " UserAction with the automation: " + automationRuleId,
                buildQuery(TAG_OPERATION,predicate) , numOfActionRun, dataStore);
        checkDSResult(tagName, numOfActionRun, res);
    }

    public  void checkDataStoreUpdatedWithTagNoUserKey(String tagName,int numOfActionRun) throws Exception {

        log4j.info("check Data Store Updated with Tag");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find " + numOfActionRun + " UserAction with the automation: " + automationRuleId,
                buildQueryNoUserKey(TAG_OPERATION,predicate) , numOfActionRun, dataStore);
        checkDSResult(tagName, numOfActionRun, res);
    }

    public  void checkDataStoreUpdatedWithAssetByRuleIDNoUserKey(int numOfActionRun) throws Exception {

        log4j.info("check Data Store Updated with Receive Asset");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find " + numOfActionRun + " UserAction with the automation: " + automationRuleId,
                buildQueryNoUserKey(RECEIVEDASSET,predicate) , numOfActionRun, dataStore);
        checkDSResultWithoutDatainfo( numOfActionRun, res);
    }

    private void checkDSResult(String name, int numOfActionRun, List<Entity> res) {
        Assert.assertEquals("Must find " + numOfActionRun + " UserAction with the automation: " + automationRuleId, numOfActionRun, res.size());
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info( "ExportData contains the following data : " + t.getValue());
        Assert.assertTrue("Data must contain the tag: " + name, t.getValue().contains(name));
    }

    private void checkDSResultWithoutDatainfo( int numOfActionRun, List<Entity> res) {
        Assert.assertEquals("Must find " + numOfActionRun + " UserAction with the automation: " + automationRuleId, numOfActionRun, res.size());
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info( "ExportData contains the following data : " + t.getValue());

    }

    public List<Entity> runQuery(String actionName,Query.FilterPredicate predicate) {
        return dataStore.findByQuery(buildQuery(actionName, predicate));
    }

    public Query buildQuery (String actionName) {
        Query q = new Query("UserAction");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, actionName),
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, key))));
        return q;
    }

    public Query buildQuery (String actionName,Query.FilterPredicate predicate) {
        Query q = new Query("UserAction");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, actionName),
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, key),
                predicate)));
        return q;
    }

    public Query buildQueryWithSort(String actionName) {
        Query q = new Query("UserAction");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, actionName),
                new Query.FilterPredicate("LocationID", Query.FilterOperator.EQUAL, locationId))));
        q.addSort("CreatedOn", Query.SortDirection.DESCENDING);
        return q;
    }

    public Query buildQueryNoUserKey(String actionName,Query.FilterPredicate predicate) {
        Query q = new Query("UserAction");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, actionName),
                predicate)));
        return q;
    }

    //check that there are no active automations(except: registationPurchaseAnalysisInqa ,purchaseAnalysisInqa ) before running new test
    public  void checkSmartAutomation() {
        List<Entity> res = getAllActiveAutomations();
        if(res.size()> 2 ) {
            log4j.info("automations are active: \n");
            for (Entity entity:res)  {
                log4j.info(String.valueOf(entity.getProperty("RuleID")) + "\n");
            }
            Assert.fail("Some automations (except: registationPurchaseAnalysisInqa ,purchaseAnalysisInqa ) remaine active but should be inactive - please check data store");
            return;
        }
        for (int i = 0; i < res.size(); i++) {
            Entity entity = res.get(i);
            String ruleId = String.valueOf(entity.getProperty("RuleID"));
            if(!(ruleId.contains("Analysis"))){
                log4j.info("automation : " + ruleId + " should not be active");
                Assert.fail("Some automations remaine active but should be inactive: " + ruleId + " - please check data store");
                return;
            }
        }

    }

    public static List<Entity> getAllActiveAutomations() {
        log4j.info("check all automations (except 'registationPurchaseAnalysis' ,'purchaseAnalysis') are inactive.");
        Query q = new Query("EventBasedActionRule");
        q.setFilter( new Query.FilterPredicate("LocationID", Query.FilterOperator.EQUAL, locationId));
        List<Entity> results = dataStore.findByQuery(q);
        List<Entity> listActiveAutomations = new ArrayList<>();
        for (Entity row: results){
            if(row.getProperty("Status").equals("active"))
                listActiveAutomations.add(row);
        }
        return listActiveAutomations;
    }

    public static void deactivateAutomation() {
        List<Entity> res = getAllActiveAutomations();

        for (int i = 0; i < res.size(); i++) {
            Entity entity = res.get(i);
            String ruleId = String.valueOf(entity.getProperty("RuleID"));
            if(!(ruleId.contains("Analysis"))){
                log4j.info("Inactivate automation: " + ruleId);
                sendRequestSetAutomationStatus(ruleId,INACTIVE);
            }
        }
    }

    public static void checkSmartAutomationsAfterTestsFinished(){
        List<Entity> res = getAllActiveAutomations();
        if(res.size() > 2){
            log4j.info("******************************");
            log4j.info("*******    Test is finished !!!! Some automations remained active   *******");
            for(Entity entity :res) {
                String ruleId = String.valueOf(entity.getProperty("RuleID"));
                if (!(ruleId.contains("Analysis"))) {
                    log4j.info("Automation: " + ruleId);
                    sendRequestSetAutomationStatus(ruleId, INACTIVE);
                }
            }
            log4j.info("******************************");
        }
    }

    public static void sendRequestSetAutomationStatus(String ruleId,String status) {
        try {
            SaveEventBasedActionRule eventBasedActionRule = new SaveEventBasedActionRule();
            eventBasedActionRule.setLocationId(locationId);
            eventBasedActionRule.setRuleId(ruleId);
            eventBasedActionRule.setStatus(status);

            log4j.info("send request for automation rule: set status to inactivate");
            // Send the request
            IServerResponse response = eventBasedActionRule.SendRequestByToken(serverToken, SaveEventBasedActionRuleResponse.class);

            // make sure the response is not Error response
            if (response instanceof ZappServerErrorResponse) {
                Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
            }

            Thread.sleep(7000);
            // Cast to SubmitPurchaseResponse object to fetch all response data
            SaveEventBasedActionRuleResponse eventBasedActionRuleResponse = (SaveEventBasedActionRuleResponse) response;
            log4j.info("Status of automation  " + ruleId + " :" + eventBasedActionRuleResponse.getStatus());
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);

        }
    }


    public String getAutomationRuleID(ISettings settings) throws Exception {

        String id = SmartAutomationService.getAutomationURL(settings);
        String ruleId = id;

        if(settings instanceof Settings) {
            ruleId = AUTOMATION_RULE_PREFIX + ruleId;
        }else{
            ruleId = "userFieldsAutomations_" + ruleId;
        }
        log4j.info("RuleID : "+ ruleId);
        return ruleId;
    }

    public String getAssetKey(String memberPhone)  throws InterruptedException{
        return serverHelper.getAssetKey(memberPhone);
    }


    /**
     *methos creates smart automation scenario with names for scenario ,actions and conditions
     * @param scenarionName
     * @param automation
     * @param condList
     * @param isOR
     * @param actionMap
     * @throws Exception
     */
    protected void createSmartAutomationScenario(String scenarionName,ISmartAutomationCounter automation, List<Condition> condList, boolean isOR, Map<SmartAutomationActions,String> actionMap) throws Exception {
        log4j.info("creating scenario");
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        scenario.setScenarioName(scenarionName);
        automation.addScenario(scenario);

        for (Condition cond : condList) {
            scenario.addCondition(cond, automation);
        }
        if (isOR)
            scenario.setOrOperator();

        for (SmartAutomationActions action : actionMap.keySet()) {

            SmartAutomationAction smartAction = new SmartAutomationAction();
            SmartAutomationAction updateSmartAction = getAction(action.toString(), smartAction, automation);
            String actionName = actionMap.get(action);
            updateSmartAction.setActionName(actionName);
            scenario.addAction(updateSmartAction, automation);
        }

        SmartAutomationService.createSmartAutomation(automation);
        if(automation.getSaveAsPreset() == null)
            automationRuleId = getAutomationRuleID(settings);
    }


    protected void createSmartAutomationScenario(ISmartAutomationCounter automation, java.util.List<Condition> condList, boolean isOR, java.util.List<SmartAutomationActions> actionList) throws Exception {

        Map<SmartAutomationActions,String> actionMap = new HashedMap();
        for (SmartAutomationActions actionName : actionList) {

            actionMap.put(actionName, null);
        }

        createSmartAutomationScenario(DEFAULT_SCENARIO_NAME, automation,condList,isOR, actionMap);

    }



    protected SmartAutomationScenario createScenarioWithTagAction(SmartAutomation automation) throws Exception {

        return automationsHelper.createScenarioWithTagAction(automation,tag);
    }

    protected SmartAutomationAction getAction(String actionName, SmartAutomationAction smartAction, ISmartAutomationCounter automation) throws Exception {

        SmartAutomationAction action = new SmartAutomationAction();

        if (SmartAutomationActions.SendAnAsset.toString().equals(actionName)) {
            action = hubAssetsCreator.getSendAssetAction(smartAction, automation, smartGift.getTitle());

        } else if (SmartAutomationActions.TagMember.toString().equals(actionName)) {
            action = hubAssetsCreator.getTagMemberAction(smartAction, automation, tag,TagUntagOperation.Tag);

        } else if (SmartAutomationActions.AddSetNumberofPoints.toString().equals(actionName)) {
            action = hubAssetsCreator.getAddPointsAction(smartAction, automation, AmountType.Credit);

        } else if (SmartAutomationActions.PunchThePunchCard.toString().equals(actionName)) {
            action = hubAssetsCreator.getPunchThePunchCardAction(smartAction, automation, smartPunchCard.getTitle(), "1");

        }else if (SmartAutomationActions.SendAPushNotification.toString().equals(actionName)) {
            String txtMessage =  "@" + TextAreaParameters.Membership_FirstName + " hello. You got push message.";
            action = hubAssetsCreator.getSendPushNotificationAction(smartAction, automation,txtMessage, "Y");

        }else if (SmartAutomationActions.SendTextSMS.toString().equals(actionName)) {
            String txtMessage =  "@" + TextAreaParameters.Membership_FirstName + " hello. You got sms.";
            action = hubAssetsCreator.getSendSMSAction(smartAction, automation,txtMessage);

        }else if (SmartAutomationActions.SendPopUPMessage.toString().equals(actionName)) {
            String txtMessage =  "@" + TextAreaParameters.Membership_FirstName + " hello. You got popup message.";
            action = hubAssetsCreator.getSendPopUpMessageAction(smartAction, automation,"test message",txtMessage);

        }else if (SmartAutomationActions.ExportEvent.toString().equals(actionName)) {
            String DESTINATION_URL = "https://services-dot-comoqa.appspot.com/echo";
            action = hubAssetsCreator.getExportEventAction(smartAction, automation,getAutomationName(),DESTINATION_URL);

        }else if(SmartAutomationActions.SendEmail.toString().equals(actionName)) {
            action = hubAssetsCreator.getSendEmailAction(smartAction, automation, templateName);
        }else if(SmartAutomationActions.RegisterMembership.toString().equals(actionName)) {
            action = hubAssetsCreator.getRegisterAction(smartAction, automation);
        }
        action.setActionName(actionName);
        return action;

    }

    protected void performUpdateMemberDetails(NewMember member) throws Exception {
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setPhoneNumber(member.getPhoneNumber());
        memberDetails.setFirstName(member.getFirstName() + "_Updated");
        MembersService.updateMemberDetails(memberDetails);
    }

    protected void performAnActionSendGift(SmartGift smartGift) throws Exception {
        MembersService.performSmartActionSendAnAsset(smartGift,timestamp);
    }

    protected void performAnActionSendGift(SmartGift smartGift,String sendPush) throws Exception {

        log4j.info("perform smart action send asset");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.SendAnAsset.toString());
        SendAnAssetAction sendAnAssetAction = new SendAnAssetAction();
        sendAnAssetAction.setAssetsListDropdown(smartGift.getTitle());
        sendAnAssetAction.setSendPushNotificationCheckbox(sendPush);
        sendAnAssetAction.setPerformActionButton("Y");
        performSmartAction.setSendAnAssetAction(sendAnAssetAction);
        MembersService.performSmartActionOnMember(performSmartAction);
        log4j.info("The gift was send to the member");
        Assert.assertTrue("The member did not get the gift: " + smartGift.getTitle(),MembersService.isGiftExists(smartGift.getTitle()));

    }


    protected void performAnActionSendPunchCard(SmartPunchCard punchCard1) throws Exception {

        MembersService.performSmartActionSendAPunchCard(punchCard1.getTitle(),timestamp);
        Assert.assertTrue("The member did not get the gift: " + punchCard1.getTitle(),MembersService.isGiftExists(punchCard1.getTitle()));
    }

    protected void performAnActionTagMember(NewMember member, String tag) throws Exception {
        log4j.info("perform action tag member");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.TagUntag.toString());
        TagOrUnTagAction tagOrUnTagAction = new TagOrUnTagAction();
        tagOrUnTagAction.setTagUntagText(tag);
        tagOrUnTagAction.setActionTag("Y");
        tagOrUnTagAction.setPerformActionButton("Y");
        String actionTag = "TagAction_" + tag;
        ArrayList<String> tags = Lists.newArrayList(actionTag);
        tagOrUnTagAction.setAddTags(tags);
        log4j.info("Action tag: " +actionTag);
        performSmartAction.setTagOrUnTagAction(tagOrUnTagAction);
        MembersService.performSmartActionOnMember(performSmartAction);
        Assert.assertTrue("The tag was not found for member " + member.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info(tag + " was added to the member ");

    }

    protected void performAnActionTagMember(String tag) throws Exception {
        log4j.info("perform action tag member");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.TagUntag.toString());
        TagOrUnTagAction tagOrUnTagAction = new TagOrUnTagAction();
        tagOrUnTagAction.setTagUntagText(tag);
        tagOrUnTagAction.setActionTag("Y");
        tagOrUnTagAction.setPerformActionButton("Y");
        String actionTag = "TagAction_" + tag;
        ArrayList<String> tags = Lists.newArrayList(actionTag);
        tagOrUnTagAction.setAddTags(tags);
        log4j.info("Action tag: " +actionTag);
        performSmartAction.setTagOrUnTagAction(tagOrUnTagAction);
        MembersService.performSmartActionOnMember(performSmartAction);
        log4j.info(tag + " was added to the member ");

    }


    protected void performAnActionPunchAPunchCard(SmartPunchCard punchCard) throws  Exception {

        performAnActionPunchAPunchCard(punchCard,1);

    }


    protected void performAnActionPunchAPunchCard(SmartPunchCard punchCard,int numOfPunches) throws Exception {

        MembersService.performSmartActionPunchAPunchCard(punchCard.getTitle(),numOfPunches);
        Assert.assertTrue("The punchcard was not punched as expected" ,MembersService.isPunchCardPunched(punchCard.getTitle(),String.valueOf(numOfPunches)));

    }

    protected void performPunchAPunchCardNoValidation(SmartPunchCard punchCard, int numOfPunches) throws Exception {
        Thread.sleep(5000); //Wait before th punch card will be send
        MembersService.performSmartActionPunchAPunchCard(punchCard.getTitle(), numOfPunches);
    }

    protected void performAnActionSubmitPurchase(List<NewMember> members, String totalSum, int numOfItems,ArrayList<String> tags, String transactionId) throws Exception {

        ArrayList<Item> items = new ArrayList<>();
        int itemPrice = Integer.valueOf(totalSum) / numOfItems;
        for (int i = 0; i < numOfItems; i++) {
            Item item = createItem(timestamp.toString(),ITEM_NAME_PREFIX + (i+1),DEPARTMENT_CODE,"Dep1",itemPrice);
            items.add(item);
        }

        performAnActionSubmitPurchaseWithParams(members,totalSum,tags,transactionId,items, BRANCH_ID);
    }

    protected void performAnActionSubmitPurchase(String status, List<NewMember> members, String totalSum, int numOfItems,ArrayList<String> tags, String transactionId) throws Exception {

        ArrayList<Item> items = new ArrayList<>();
        int itemPrice = Integer.valueOf(totalSum) / numOfItems;
        for (int i = 0; i < numOfItems; i++) {
            Item item = createItem(timestamp.toString(),ITEM_NAME_PREFIX + (i+1),DEPARTMENT_CODE,"Dep1",itemPrice);
            items.add(item);
        }

        performAnActionSubmitPurchaseWithParams(status,members,totalSum,tags,transactionId,items, BRANCH_ID);
    }

    protected SubmitPurchaseResponse performAnActionSubmitPurchaseWithParams(String status,List<NewMember> members, String totalSum, ArrayList<String> tags, String transactionId, ArrayList<Item> items, String branchId) throws Exception {
        SubmitPurchase submitPurchase = buildSubmitPurchaseRequest(members, totalSum, tags, transactionId, items, branchId);
        submitPurchase.setStatus(status);
        log4j.info("send request submit purchase");
        return getSubmitPurchaseResponse(submitPurchase);

    }

    @NotNull
    protected SubmitPurchaseResponse getSubmitPurchaseResponse(SubmitPurchase submitPurchase) {
        // Send the request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);


        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        // Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        Assert.assertEquals("status code must be 200", submitPurchaseResponse.getStatusCode(), httpStatusCode200);
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());
        log4j.info("status code: " + submitPurchaseResponse.getStatusCode());
        return submitPurchaseResponse;
    }

    protected SubmitPurchaseResponse performAnActionSubmitPurchaseWithParams(List<NewMember> members, String totalSum, List<String> tags, String transactionId, List<Item> items, String branchId) throws Exception {
        SubmitPurchase submitPurchase = buildSubmitPurchaseRequest(members, totalSum, tags, transactionId, items, branchId);

        log4j.info("send request submit purchase");
        // Send the request
        return getSubmitPurchaseResponse(submitPurchase);
    }

    protected SubmitPurchaseResponse performAnActionSubmitPurchaseWithParams(List<NewMember> members, String totalSum, List<String> tags, String transactionId, List<Item> items, String branchId,String status) throws Exception {
        SubmitPurchase submitPurchase = buildSubmitPurchaseRequest(members, totalSum, tags, transactionId, items, branchId);
        submitPurchase.setStatus(status);
        log4j.info("send request submit purchase");
        // Send the request
        return getSubmitPurchaseResponse(submitPurchase);
    }


    protected SubmitPurchase buildSubmitPurchaseRequest(List<NewMember> members, String totalSum, List<String> tags, String transactionId, List<Item> items, String branchId) {
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setPosId("1");
        submitPurchase.setBranchId(branchId);
        submitPurchase.setTotalSum(totalSum);
        submitPurchase.setTransactionId(transactionId);
        submitPurchase.setChainId("chain123");
        submitPurchase.setCashier(EMPLOYEE + timestamp.toString());
        log4j.info("perform action submit purchase with TransactionID: " + transactionId);

        for (int i = 0; i < members.size(); i++) {
            Customer customer = new Customer();
            customer.setPhoneNumber(members.get(i).getPhoneNumber());
            submitPurchase.getCustomers().add(customer);
        }


        submitPurchase.setTags(tags);
        submitPurchase.setItems(items);
        return submitPurchase;
    }


    protected SubmitPurchaseResponse performAnActionSubmitPurchaseWithParams(List<NewMember> members, String totalSum,List<String> tags, String transactionId,  List<Item> items) throws Exception {
        return performAnActionSubmitPurchaseWithParams(members,totalSum,tags,transactionId,items,BRANCH_ID);

    }

    protected SubmitPurchaseResponse performAnActionSubmitPurchaseWithParams(String status, List<NewMember> members, String totalSum,List<String> tags, String transactionId,  List<Item> items) throws Exception {
        return performAnActionSubmitPurchaseWithParams(members,totalSum,tags,transactionId,items,BRANCH_ID,status);

    }

    protected Item createItem (String itemCode, String itemName, String depCode, String depName, Integer itemPrice,List<String> itemTags){

        Item item = buildDefaultItem(itemCode, itemName, depCode, depName, itemPrice);
        item.setTags((ArrayList<String>) itemTags);
        return item;
    }

    @NotNull
    private Item buildDefaultItem(String itemCode, String itemName, String depCode, String depName, Integer itemPrice) {
        Item item = new Item();

        item.setItemCode(itemCode);
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName(itemName);
        item.setDepartmentCode(depCode);
        item.setDepartmentName(depName);
        item.setPrice(itemPrice);
        return item;
    }

    protected Item createItem (String itemCode, String itemName, String depCode, String depName, Integer itemPrice){

        Item item = buildDefaultItem(itemCode, itemName, depCode, depName, itemPrice);
        item.setTags(Lists.newArrayList("DEP1"));
        return item;
    }

    protected void checkDataStoreUpdatedWithTagForOneOfMembers(String transactionId,Key key1) throws Exception {
        log4j.info("check Data Store Updated with Tag for one of the members");
        Query q = new Query("UserAction");
        Query.CompositeFilter filter = new Query.CompositeFilter(Query.CompositeFilterOperator.OR, Arrays.asList( new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, key), new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, key1)));
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, TAG_OPERATION),
                filter,
                new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, transactionId))));
        List<Entity> res = ServerHelper.queryWithWait("Must find " + 1 + " UserAction with the TransactionID: " + transactionId.toString(),
                q, 1, dataStore);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info("ExportData contains the following data : " + t.getValue());
        Assert.assertTrue("Data must contain the tag: " + tag, t.getValue().contains(tag));
    }


    //perform submit with  one member ,one item by default
    protected void performAnActionSubmitPurchase(NewMember member, String totalSum, String transactionId) throws Exception {
        performAnActionSubmitPurchase(Lists.newArrayList(member), totalSum, 1, Lists.newArrayList("DEP-1"), transactionId);
    }

    //perform submit with  one member ,one item by default
    protected void performAnActionSubmitPurchase(String status, NewMember member, String totalSum, String transactionId) throws Exception {
        performAnActionSubmitPurchase(status,Lists.newArrayList(member), totalSum, 1, Lists.newArrayList("DEP-1"), transactionId);
    }

    //perform submit with one member ,one item by default and list of tags
    protected void performAnActionSubmitPurchase(NewMember member, String totalSum, ArrayList<String> tags, String transactionId) throws Exception {
        performAnActionSubmitPurchase(Lists.newArrayList(member), totalSum, 1,tags, transactionId);
    }

    //perform submit with list of members ,one item by default and default tag
    protected void performAnActionSubmitPurchase(List<NewMember> members, String totalSum, String transactionId) throws Exception {

        performAnActionSubmitPurchase(members, totalSum, 1, Lists.newArrayList("DEP-1"), transactionId);
    }

    //perform submit with list of members ,total sum,numOfItems given  and default tag
    protected void performAnActionSubmitPurchase(List<NewMember> members, String totalSum, int numOfItems, String transactionId) throws Exception {
        performAnActionSubmitPurchase(members, totalSum, numOfItems, Lists.newArrayList("DEP-1"), transactionId);
    }



    protected void checkDataStoreUpdatedWithPunch(String transactionId) throws InterruptedException {
        log4j.info("check Data Store Updated with Punch");
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the TransactionID: " + transactionId,
                buildQuery(PUNCH, transactionId), 1, dataStore);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info("ExportData contains the following data : " + t.getValue());
        System.out.println("Data: " + t.getValue());
    }



    protected void checkDataStoreUpdatedWithAsset(String transactionId) throws Exception {

        log4j.info("check Data Store Updated with asset");
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the TransactionID: " + transactionId,
                buildQuery(RECEIVEDASSET, transactionId), 1, dataStore);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info( "Data contains the following data : " + t.getValue());
        Assert.assertTrue("Data must contain the assetType: ", t.getValue().contains("AssetType=\"gift\""));
        Assert.assertTrue("Data must contain the asset source: ", t.getValue().contains("Source=\"Automation\""));
        Assert.assertEquals("Source is automation", "Automation", res.get(0).getProperty("Source"));
    }




    protected void checkDataStoreNegative(String actionName, String transactionId) throws Exception {
        //give the data store time to update
        Thread.sleep(3000);
        log4j.info("check DataStore not updated with :" + actionName + "for transaction ID:" + transactionId);
        List<Entity> res = runQuery(actionName, transactionId);
        Assert.assertEquals("The automation is not supposed to run:", 0, res.size());

    }

    protected List<Entity> runQuery(String actionName, String transactionId) {
        return runQuery(actionName,new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, transactionId));
    }

    protected void performAnActionRedeemAsset(String membershipKey,String userKey, String assetKey) {

        InvokeEventBasedActionRuleBuilder invokeEventBasedActionRuleBuilder = new InvokeEventBasedActionRuleBuilder();
        InvokeEventBasedActionRule invokeEventBasedActionRule = invokeEventBasedActionRuleBuilder.buildRedeemAssetRequest(membershipKey, userKey, locationId,assetKey);
        IServerResponse response = invokeEventBasedActionRule.SendRequestByToken(serverToken, Response.class);

        if (response instanceof IServerErrorResponse) {
            Assert.fail(((IServerErrorResponse) response).getErrorMessage());
        }
    }

    protected String performPayWithBudget(String phoneNumber,String totalSum,String transactionId) {

        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        //Set pay with budget
        PayWithBudget payWithBudget = new PayWithBudget();
        payWithBudget.setAllowPartialPayment(true);
        payWithBudget.getCustomers().add(customer);
        payWithBudget.setTotalSum(totalSum);
        payWithBudget.setTimeStamp(timestamp.toString());
        payWithBudget.setTransactionId(transactionId);
        payWithBudget.setBranchId("1");

        // Send the request
        log4j.info("perform pay with budget (como wallet)");
        IServerResponse response = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        // Cast to PayWithBudgetResponse object to fetch all response data
        PayWithBudgetResponse payWithBudgetResponse = (PayWithBudgetResponse) response;
        String actualCharge = payWithBudgetResponse.getActualCharge();
        log4j.info("actual charge: " + actualCharge);
        return actualCharge;
    }

    protected List<Entity>  checkDataStoreUpdatedWithRegisterByRuleID() throws Exception {
        log4j.info("check Data Store Updated with register");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the automation: " + automationRuleId,
                buildQuery(JOINED_CLUB,predicate) , 1, dataStore);
        Assert.assertEquals("Must find one UserAction with the automation: " + automationRuleId, 1, res.size());
        return res;
    }


    protected void checkActionTags(List<String> tags, List<Entity> res) {
        if(tags.size() > 0) {
            ArrayList<String> actionTags = ((ArrayList<String>) res.get(0).getProperty("ActionTags"));

            Assert.assertTrue("actionTags must contain all tags", actionTags.containsAll(tags));
        }
    }


    protected void checkAutomationRunAndRegisterMember(List<String> tags) throws Exception {
        List<Entity> res = checkDataStoreUpdatedWithRegisterByRuleID();
        checkActionTags(tags, res);
        Assert.assertTrue("The member still not register, but appeared register in the data store", MembersService.checkMemberStatus(REGISTERED));
        log4j.info("The member became registered by the automation");
    }

    protected void checkAutomationRunAndRegisterMember() throws Exception {
        checkDataStoreUpdatedWithRegisterByRuleID();
        Assert.assertTrue("The member still not register, but appeared register in the data store", MembersService.checkMemberStatus(REGISTERED));
        log4j.info("The member became registered by the automation");
    }

    protected void checkUserAction(String action,String actionTag) throws Exception {

        Query q = new Query("UserAction");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, action),
                new Query.FilterPredicate("ActionTags", Query.FilterOperator.EQUAL,actionTag))));
        log4j.info("Check member get the point transaction in datastore: ActionTags =" +actionTag );
        serverHelper.queryWithWait("Member did not get the points ",q,1,dataStore);
    }

    protected List<Entity> checkDataStoreWithEventExport(String triggerName,String phone,String url) throws InterruptedException {

        log4j.info("check Data Store Updated with event_export action");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the automation: " + automationRuleId,
                buildQuery(EVENT_EXPORTED_OPERATION,predicate) , 1, dataStore);
        Assert.assertEquals("Must find one UserAction with the automation: " + automationRuleId, 1, res.size());

        Text t = ((Text)res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());

        Assert.assertTrue("Data must contain the data: ", t.getValue().contains("DestinationURL=\"" + url + "\""));
        Assert.assertTrue("Data must contain the data: ", t.getValue().contains("PhoneNumber=\"" + phone + "\""));
        Assert.assertTrue("Data must contain the data: ", t.getValue().contains("Trigger=\"" + triggerName + "\""));
        return res;
    }
}
