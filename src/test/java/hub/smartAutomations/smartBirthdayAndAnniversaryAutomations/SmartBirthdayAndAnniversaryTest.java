package hub.smartAutomations.smartBirthdayAndAnniversaryAutomations;


import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import common.BaseTest;
import hub.base.BaseHubTest;
import hub.base.BaseService;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.base.categories.hub1Categories.hub1Sanity;
import hub.base.categories.hub1Categories.hub1SmartActions;
import hub.common.objects.smarts.SmartBirthdayAndAnniversaryAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.common.objects.smarts.smarObjects.triggers.BirthdayAnniversaryAutomationTriggers;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.ApplyIf;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Condition;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Membership;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.TagOperator;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.BirthdayAnniversarySettings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.Navigator;
import hub.services.AcpService;
import hub.smartAutomations.BaseAutomationTest;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.PropsReader;
import utils.StringUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


public class SmartBirthdayAndAnniversaryTest extends BaseAutomationTest {

    private static final String IMPORT_FOLDER = "import";
    private static final String BIRTHDAY_FOLDER = "birthday";
    private static final String IMPORT_BIRTHDAY_ANNIVERSARY = "birthdayAnniversaryImport.csv";
    private static final String IMPORT_550_FILENAME = "550Import.csv";
    private static final String TAG_OF_MEMBER = "b_2709";

    public static final Logger log4j = Logger.getLogger(SmartBirthdayAndAnniversaryTest.class);

    @BeforeClass
    public static void setup() throws Exception {

        init();

        //create updated import file
        log4j.info("start creating updated csv file");
        writeToCSV();

        //import 550 members
        log4j.info("start import");
        AcpService.importMembers(IMPORT_FOLDER + File.separator + IMPORT_BIRTHDAY_ANNIVERSARY);
        TimeUnit.SECONDS.sleep(30);

        BaseService.waitForLoad(By.xpath("//*[@class=\"block usersimport ng-scope\"]/div[2]//span[contains(text(), 'Success')]"));
        log4j.info("finish import");

        log4j.info("Wait 25 minutes for BQ to update with the new members");
        //Wait 25 minutes for BQ to update with the new members
        TimeUnit.MINUTES.sleep(25);
    }


    @AfterClass
    public static void tearDown(){

        log4j.info("Finish test : " + SmartBirthdayAndAnniversaryTest.class.getName());
        log4j.info("deactivate smart automation from HUB");
        try {
            Navigator.operationsTabSmartBirthdayAutoamtion();
        } catch (Exception e) {
            log4j.error("Error while trying to navigate to Smart Birthday/Anniversary Automations");
        }
        deactivateBDayAutomations();
        log4j.info("close browser after finish test");
        cleanBase();
        BaseHubTest.cleanHub();
    }

    @Override
    public Triggers getTrigger() {
        return null;
    }

    @Override
    public String getAutomationName() {
        return "";
    }


    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {


        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new BirthdayAnniversarySettings();
            tag = "tag_" + timestamp;
            if (smartGift == null) {
                //create assets
                try {
                    smartGift = hubAssetsCreator.createSmartGift(timestamp);
                } catch (Exception e) {
                    log4j.error(e);
                    Assert.fail("error while creating asset");
                }
            }
        }
    };




    @Test
    public void testMatchingBirthdayMonthOnlyWithTagMemberAction() throws Exception {

        //Create smart automation
        SmartBirthdayAndAnniversaryAutomation automation = hubAssetsCreator.createSmartBirthdayAnniversaryAutomation(BirthdayAnniversaryAutomationTriggers.MatchingBirthdayMonthOnly, "BirthdayMonthOnly" + timestamp, ((BirthdayAnniversarySettings) settings));
        //Set when the automation will run
        String[] timeArr = getTimePlusXMinutes();
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtHours(timeArr[0]);
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtMinutes(timeArr[1]);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(TAG_OF_MEMBER);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Wait 7 minutes for birthday/anniversary automation to run
        TimeUnit.MINUTES.sleep(7);

        checkDataStoreUpdatedWithTagNoUserKey(tag, 550);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testMatchingBirthdayMonthAndDayWithTagMemberAction() throws Exception {

        //Create smart automation
        SmartBirthdayAndAnniversaryAutomation automation = hubAssetsCreator.createSmartBirthdayAnniversaryAutomation(BirthdayAnniversaryAutomationTriggers.MatchingBirthdayMonthAndDate, "BirthdayMonth&Day" + timestamp, ((BirthdayAnniversarySettings) settings));
        //Set when the automation will run
        String[] timeArr = getTimePlusXMinutes();
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtHours(timeArr[0]);
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtMinutes(timeArr[1]);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(TAG_OF_MEMBER);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Wait 7 minutes for birthday/anniversary automation to run
        TimeUnit.MINUTES.sleep(7);

        checkDataStoreUpdatedWithTagNoUserKey(tag, 550);
    }


    @Test
    public void testMatchingBirthdayMonthOnlyWithSendAssetAction() throws Exception {

        //Create smart automation
        SmartBirthdayAndAnniversaryAutomation automation = hubAssetsCreator.createSmartBirthdayAnniversaryAutomation(BirthdayAnniversaryAutomationTriggers.MatchingBirthdayMonthOnly, "BirthdayMonthOnly" + timestamp, ((BirthdayAnniversarySettings) settings));
        //Set when the automation will run
        String[] timeArr = getTimePlusXMinutes();
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtHours(timeArr[0]);
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtMinutes(timeArr[1]);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(TAG_OF_MEMBER);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Wait 7 minutes for birthday/anniversary automation to run
        TimeUnit.MINUTES.sleep(7);

        checkDataStoreUpdatedWithAssetByRuleIDNoUserKey(550);

    }

    @Test
    @Category( {hub1Sanity.class, hub1SmartActions.class} )
    public void testMatchingBirthdayMonthAndDayWithSendAssetAction() throws Exception {

        //Create smart automation
        SmartBirthdayAndAnniversaryAutomation automation = hubAssetsCreator.createSmartBirthdayAnniversaryAutomation(BirthdayAnniversaryAutomationTriggers.MatchingBirthdayMonthAndDate, "BirthdayMonth&Day" + timestamp, ((BirthdayAnniversarySettings) settings));
        //Set when the automation will run
        String[] timeArr = getTimePlusXMinutes();
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtHours(timeArr[0]);
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtMinutes(timeArr[1]);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(TAG_OF_MEMBER);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Wait 7 minutes for birthday/anniversary automation to run
        TimeUnit.MINUTES.sleep(7);

        checkDataStoreUpdatedWithAssetByRuleIDNoUserKey(550);

    }

    @Test
    public void testMatchingAnniversaryMonthOnlyWithTagMemberAction() throws Exception {

        //Create smart automation
        SmartBirthdayAndAnniversaryAutomation automation = hubAssetsCreator.createSmartBirthdayAnniversaryAutomation(BirthdayAnniversaryAutomationTriggers.MatchingAnniversaryMonthOnly, "AnniversaryMonthOnly" + timestamp, ((BirthdayAnniversarySettings) settings));
        //Set when the automation will run
        String[] timeArr = getTimePlusXMinutes();
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtHours(timeArr[0]);
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtMinutes(timeArr[1]);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(TAG_OF_MEMBER);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Wait 7 minutes for birthday/anniversary automation to run
        TimeUnit.MINUTES.sleep(7);

        checkDataStoreUpdatedWithTagNoUserKey(tag, 550);

    }

    @Test
    public void testMatchingAnniversaryMonthOnlyWithSendAssetAction() throws Exception {

        //Create smart automation
        SmartBirthdayAndAnniversaryAutomation automation = hubAssetsCreator.createSmartBirthdayAnniversaryAutomation(BirthdayAnniversaryAutomationTriggers.MatchingAnniversaryMonthOnly, "AnniversaryMonthOnly" + timestamp, ((BirthdayAnniversarySettings) settings));
        //Set when the automation will run
        String[] timeArr = getTimePlusXMinutes();
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtHours(timeArr[0]);
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtMinutes(timeArr[1]);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(TAG_OF_MEMBER);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Wait 7 minutes for birthday/anniversary automation to run
        TimeUnit.MINUTES.sleep(7);

        checkDataStoreUpdatedWithAssetByRuleIDNoUserKey(550);

    }

    @Test
    @Category(Hub1Regression.class)
    public void testMatchingAnniversaryMonthAndDayWithTagMemberAction() throws Exception {

        //Create smart automation
        SmartBirthdayAndAnniversaryAutomation automation = hubAssetsCreator.createSmartBirthdayAnniversaryAutomation(BirthdayAnniversaryAutomationTriggers.MatchingAnniversaryMonthAndDate, "AnniversaryMonth&Day" + timestamp, ((BirthdayAnniversarySettings) settings));
        //Set when the automation will run
        String[] timeArr = getTimePlusXMinutes();
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtHours(timeArr[0]);
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtMinutes(timeArr[1]);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(TAG_OF_MEMBER);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Wait 7 minutes for birthday/anniversary automation to run
        TimeUnit.MINUTES.sleep(7);

        checkDataStoreUpdatedWithTagNoUserKey(tag, 550);

    }

    @Test
    public void testMatchingAnniversaryMonthAndDayWithSendAssetAction() throws Exception {

        //Create smart automation
        SmartBirthdayAndAnniversaryAutomation automation = hubAssetsCreator.createSmartBirthdayAnniversaryAutomation(BirthdayAnniversaryAutomationTriggers.MatchingAnniversaryMonthAndDate, "AnniversaryMonth&Day" + timestamp, ((BirthdayAnniversarySettings) settings));
        //Set when the automation will run
        String[] timeArr = getTimePlusXMinutes();
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtHours(timeArr[0]);
        ((BirthdayAnniversarySettings)settings).setRunEveryDayAtMinutes(timeArr[1]);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(TAG_OF_MEMBER);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Wait 7 minutes for birthday/anniversary automation to run
        TimeUnit.MINUTES.sleep(7);

        checkDataStoreUpdatedWithAssetByRuleIDNoUserKey(550);

    }

    public String[] getTimePlusXMinutes(){

        TimeZone timeZone = TimeZone.getTimeZone(System.getProperty("user.timezone"));

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("HH:mm");
        dateFormat1.setTimeZone(timeZone);
        Calendar cal = Calendar.getInstance();
        String time = dateFormat1.format(cal.getTime());

        String[] arr = new String[2];

        String hh = time.split(":")[0];
        String mm = time.split(":")[1];
        arr[0] = hh;
        arr[1] = mm;

        if (Integer.parseInt(mm)>=00 && Integer.parseInt(mm)<50)
        {
            int m1 = Integer.parseInt(mm) + 6;
            if(m1 > 0 && m1 < 10)
                arr[1] = "0" + String.valueOf(m1);
            else
                arr[1] = String.valueOf(m1);

        }
        else if (Integer.parseInt(mm)>49 && Integer.parseInt(mm)<60)
        {
            int h1 = Integer.parseInt(hh) + 1;
            if(h1 >= 0 && h1 < 10)
                arr[0] = "0" + String.valueOf(h1);
            else
                arr[0] = String.valueOf(h1);

            mm = "03";
            arr[1] = mm;
        }
        return arr;

    }


    public static void writeToCSV()throws Exception {


        String pathToFile = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "import" + File.separator + IMPORT_550_FILENAME;


        Reader reader = Files.newBufferedReader(Paths.get(pathToFile));
        CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT.withFirstRecordAsHeader());

        String csv = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator + "import" + File.separator;


        BufferedWriter writer = Files.newBufferedWriter(Paths.get(csv + IMPORT_BIRTHDAY_ANNIVERSARY));
        CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader("First Name", "Phone Number", "Tag", "Points", "Credit", "ExtraIdentifiers", "ExtClubMemberID", "MemberID", "Birthday", "Anniversary","Email Address"));

        List<CSVRecord> csvRecords = csvParser.getRecords();


        int birthdayColIndex = 8;
        int anniversaryColIndex = 9;
        String todayDate = getToday();


        for (int i = 0; i < csvRecords.size(); i++) {
            CSVRecord csvRecord = csvRecords.get(i);
            String[] values = new String[csvRecord.size()];
            for (int j = 0; j < csvRecord.size(); j++) {

                if (birthdayColIndex == j || anniversaryColIndex == j) {
                    String previous = csvRecord.get(j);
                    String year = previous.substring(previous.lastIndexOf("/"));
                    values[j] = todayDate + year;
                }else
                    values[j] = csvRecord.get(j);

            }
            csvPrinter.printRecord(values);
        }

        csvPrinter.flush();


    }

    private static String getToday() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM");
        Date date = new Date();
        String today = dateFormat.format(date);

        return today;
    }

    private static void deactivateBDayAutomations() {
        try {

            List<WebElement> elemList = Navigator.getDriver().findElements(By.xpath("//input[contains(@class,\"ng-not-empty\") and starts-with(@id,\"active_automation_\")]"));
            for (int j = 0; j <elemList.size() ; j++) {
                WebElement element = elemList.get(j);
                BaseService.executeJavascript("arguments[0].click();", element);
                Navigator.waitForLoad();
            }
        } catch (Exception e) {
            log4j.error(e);
        }
    }

    //@Test
    public void deactivateSmartBirthdayAutomations() throws Exception{

        String fileLocations = StringUtils.readFile(BaseService.resourcesDir + BIRTHDAY_FOLDER +  File.separator +"BirthdayLocationsActive.csv");
        String[] locations = fileLocations.split(",");
        for (int i = 0; i < locations.length; i++) {
            String location = locations[i];
            String url = MessageFormat.format("https://qa.como.com/dashboard/#/operations/users-fields-automations?location_id={0}",location);
            Navigator.getDriver().get(url);
            Navigator.getDriver().navigate().to(url);
            Navigator.waitForLoad();
            deactivateBDayAutomations();
        }
    }
}

