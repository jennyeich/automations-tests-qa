package hub.smartAutomations.triggers;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.SmartPunchCard;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.Navigator;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.utils.ServerHelper;

import java.util.List;
import java.util.UUID;

/**
 * Created by Goni on 5/14/2017.
 * Test Rail: C5386
 */
public class OverflowsPunchcardTriggerTest extends BaseAutomationTest {

    private final static String NUM_OF_PUNCHES_EXPR = "@NumOfPunches";

    @Override
    public Triggers getTrigger() {
        return Triggers.OverflowsPunchCard;
    }

    @Override
    public String getAutomationName() {
        return "PunchOverflow_" + timestamp;
    }


    @Before
    public void checkActiveAutomations() {
        checkSmartAutomation();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {


        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
        }
    };


    @Test
    public void testPunchLastPunchYES_OverflowConditionAssetEquals() throws Exception {

        smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");

        //Create smart automation for punch/LastPunch=YES
        createPunchAutomationLastPunchYesNo("Yes");

        //Create smart automation for overflowPunch
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator oper1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.PUNCHCARD_OVERFLOW, PunchcardOverflow.Asset, oper1, smartPunchCard.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.PunchThePunchCard));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        //punch 5 in the first punch and then 2 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,7);

        checkDataStoreUpdatedWithExceededPunch("2");

    }

    @Test
    public void testPunchLastPunchYES_OverflowConditionAssetEquals_Overflow2cards() throws Exception {

        smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");

        //Create smart automation for punch/LastPunch=YES
        createPunchAutomationLastPunchYesNo("Yes");

        //Create smart automation for overflowPunch
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator oper1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.PUNCHCARD_OVERFLOW, PunchcardOverflow.Asset, oper1, smartPunchCard.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.PunchThePunchCard));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        //punch 5 in the first punch and then 5 in the new punch card that will be send from automation
        //and then 2 in the new punch card that will be send from automation - total 7 overflow
        performAnActionPunchAPunchCard(smartPunchCard,12);

        checkDataStoreUpdatedWithExceededPunch("7");

    }

    @Test
    @Category(Hub1Regression.class)
    public void testPunchLastPunchYES_OverflowConditionAssetEquals_OverflowContinues() throws Exception {

        smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");

        //Create smart automation for punch/LastPunch=YES
        createPunchAutomationLastPunchYesNo("Yes");

        //Create smart automation for overflowPunch
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator oper1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.PUNCHCARD_OVERFLOW, PunchcardOverflow.Asset, oper1, smartPunchCard.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.PunchThePunchCard));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        //punch 5 in the first punch and then 3 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,8);

        checkDataStoreUpdatedWithExceededPunch("3");//exceeded = 3

        performAnActionPunchAPunchCard(smartPunchCard,8);//exceeded = 6

        Query.FilterPredicate predicate = new Query.FilterPredicate("LocationID", Query.FilterOperator.EQUAL, locationId);
        List<Entity> res = ServerHelper.queryWithWait("Must find 2 UserAction with the action: " + EXCEEDED_PUNCH,
                buildQuery(EXCEEDED_PUNCH,predicate) , 2, dataStore);
        for (int i = 0; i < res.size(); i++) {
            Text t = ((Text)res.get(i).getProperty("Data"));
            Assert.assertTrue("Data must contain the NumOfPunches: 3 or 6 accourdinally",(t.getValue().contains("NumOfPunches=\"3\"") || t.getValue().contains("NumOfPunches=\"6\"") ));
        }

    }

    @Test
    public void testPunchLastPunchYES_OverflowConditionAssetNotEquals() throws Exception {

        smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");
        String tempPunchId = UUID.randomUUID().toString();
        SmartPunchCard punchCard1 = hubAssetsCreator.createSmartPunchCard(tempPunchId, "5");

        //Create smart automation for punch/LastPunch=Yes
        createPunchAutomationLastPunchYesNo("Yes");

        //Create smart automation for overflowPunch
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator oper1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond1 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.PUNCHCARD_OVERFLOW, PunchcardOverflow.Asset, oper1, punchCard1.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.PunchThePunchCard));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        //punch 5 in the first punch and then 2 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,7);

        checkDataStoreUpdatedWithExceededPunch("2");

    }



    //Test the case that user has 2 same punch cards with 5 punches and we autopunch from the hub 7 punches
    //so the overflow is done from the hub directly to the second punchcard.
    // We should check that the automation of overflow will not run
    @Test
    public void testPunchLastYES_OverflowConditionAssetEqual_Punch2AssetsFromHUB_Negative() throws Exception {

        //create punchcard
        smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");

        //Create smart automation for overflowPunch
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.PUNCHCARD_OVERFLOW, PunchcardOverflow.Asset, oper,smartPunchCard.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.PunchThePunchCard));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);
        Thread.sleep(2000);
        //Send another punch to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        //punch 5 in the first punch and then 2 in the new punch card
        performAnActionPunchAPunchCard(smartPunchCard,7);

        checkDataStoreNotUpdatedWithExceededPunch();
    }


    @Test
    public void testPunchTotalPunchesEqualsToMaxPunches_OverflowConditionAssetEquals() throws Exception {

        smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");

        //Create smart automation for punch/Total punches after operation = 5 - then send another punch card
        createPunchAutomationTotalPunches(IntOperator.Int_Equals_enum.EqualsTo,"5");

        //Create smart automation for overflowPunch
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.PUNCHCARD_OVERFLOW, PunchcardOverflow.Asset, oper,smartPunchCard.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.PunchThePunchCard));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        //punch 5 in the first punch and then 3 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,7);

        checkDataStoreUpdatedWithExceededPunch("2");
    }



    @Test
    @Category(Hub1Regression.class)
    public void testPunchTotalPunchesEqualsBeforeLastPunch_OverflowConditionAssetEquals() throws Exception {

        smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");

        //Create smart automation for punch/Total punches after operation = 5 - then send another punch card
        createPunchAutomationTotalPunches(IntOperator.Int_Equals_enum.EqualsTo,"4");

        //Create smart automation for overflowPunch
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.PUNCHCARD_OVERFLOW, PunchcardOverflow.Asset, oper,smartPunchCard.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.PunchThePunchCard));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        //punch 5 in the first punch and then 3 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,6);

        checkDataStoreUpdatedWithExceededPunch("1");
    }

    @Test
    public void testPunchTotalPunchesGreaterThan5_OverflowConditionAssetEquals() throws Exception {

        smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");

        //Create smart automation for punch/Total punches after operation > 4 - then send another punch card
        createPunchAutomationTotalPunches(IntOperator.Int_Equals_enum.Greaterthan,"5");

        //Create smart automation for overflowPunch
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.PUNCHCARD_OVERFLOW, PunchcardOverflow.Asset, oper,smartPunchCard.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.PunchThePunchCard));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        //punch 5 in the first punch and then 3 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,6);

        checkDataStoreUpdatedWithExceededPunch("1");
    }

    @Override
    protected void performAnActionPunchAPunchCard(SmartPunchCard punchCard,int numOfPunches) throws Exception {

        MembersService.performSmartActionPunchAPunchCard(punchCard.getTitle(),numOfPunches);
        Thread.sleep(5000);//Wait after the punch card sent so the data store will be updated with exceededPunch action

    }


    private void createPunchAutomationLastPunchYesNo(String YesNo) throws Exception {
        String automationName = "PunchForTriggerOverflow_" + timestamp;
        log4j.info("automationName: " + automationName);
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(Triggers.PunchesAPunchCard, automationName, settings);
        //create condition
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.PUNCH, Punch.LastPunch, oper,YesNo);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
    }

    private void createPunchAutomationTotalPunches(IntOperator.Int_Equals_enum operator,String numOfPunches) throws Exception {
        String automationName = "PunchForTriggerOverflow_" + timestamp;
        log4j.info("automationName: " + automationName);
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(Triggers.PunchesAPunchCard, automationName, settings);
        //create condition
        IntOperator oper = hubAssetsCreator.getIntOperator(automation, operator);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PUNCH, Punch.TotalPunchesAfterOperation, oper,numOfPunches);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
    }


    @Override
    protected SmartAutomationAction getAction(String actionName, SmartAutomationAction smartAction, ISmartAutomationCounter automation) throws Exception {

        SmartAutomationAction action = new SmartAutomationAction();

        if (SmartAutomationActions.SendAnAsset.toString().equals(actionName)) {
            action = hubAssetsCreator.getSendAssetAction(smartAction, automation, smartPunchCard.getTitle());

        } else if (SmartAutomationActions.PunchThePunchCard.toString().equals(actionName)) {
            action = hubAssetsCreator.getPunchThePunchCardActionWithTiming(smartAction, automation, smartPunchCard.getTitle(), NUM_OF_PUNCHES_EXPR);

        }
        return action;

    }


      @After
      public void cleanData() throws Exception {
          if (smartPunchCard != null)
              MembersService.deactivateSmartPunchCard(smartPunchCard.getTitle());
          Navigator.refresh();
          log4j.info("deactivate smart automation from APIs");
          deactivateAutomation();
      }

}
