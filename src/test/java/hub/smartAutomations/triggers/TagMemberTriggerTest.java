package hub.smartAutomations.triggers;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.TagMember;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by goni on 2/28/2017.
 * Test Rail : C4553
 */
public class TagMemberTriggerTest extends BaseAutomationTest {

    @Override
    public Triggers getTrigger() {
        return Triggers.TagOrUntag;
    }

    @Override
    public String getAutomationName() {
        return "TagMember_" + timestamp;
    }

    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }


    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
            createSmartGift();
        }
    };

    //Test trigger tag member with condition  Date and time are from now till a year later : action -send asset
    @Test
    public void testConditionDateAndTimeActionSendAnAsset() throws Exception {


        //step 1
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextYear);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation(member);

    }


    //Test trigger tag member with condition Expiration Date is After today : action -send asset
    @Test
    public void testConditionExpirationDateIsAfterActionSendAnAsset() throws Exception {


        //step 2
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));

        //Create member
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calNextYear);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation(member);

    }

    //Test trigger tag member with condition Email not Equal And FirstName Equal  : action -send asset
    @Test
    public void testConditionFNameAndEmailEqualActionSendAnAsset() throws Exception {


        //step 3
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal1, MOCK_EMAIL);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal2, AUTO_USER_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation(member);

    }

    //    Negative: action not performed
    //    Condition : Expiration Date is After today
    //    Add Action: Send an Asset
    //    send tag to the member with expiration date as a year before today
    @Test
    public void testConditionExpirationDateIsAfterTodayNegative() throws Exception {


        //step 4
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        Calendar calPrevYear = Calendar.getInstance();
        calPrevYear.add(Calendar.YEAR, -1);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));

        //Create member
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calPrevYear);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionTagMember(member, tag);

        //check that the automation did not run
        checkDataStoreNegativeCheckByRuleID(RECEIVEDASSET);

    }


    //Test trigger tag member with condition DaysTime Today am pm : action -send asset
    @Test
    public void testConditionDaysTimeActionSendAnAsset() throws Exception {


        //step 5
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar timeFrom = Calendar.getInstance();

        Calendar timeUntil = Calendar.getInstance();
        timeUntil.add(Calendar.HOUR, 1);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, ApplyIf.DAYSTIME, Calendar.getInstance(), timeFrom, timeUntil);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation(member);

    }

    //with condition DaysTime 2 days (today & tomorrow): action -Send An Asset
    @Test
    public void testConditionDaysTime2DaysSendAnAsset() throws Exception {


        //step 6
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DAY_OF_WEEK, 1);

        //create today
        DaysTime day1 = hubAssetsCreator.generateSingleDay(Calendar.getInstance());
        //create next day
        DaysTime day2 = hubAssetsCreator.generateSingleDay(nextDay);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, Lists.newArrayList(day1, day2));
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));


        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation(member);

    }


    //Test trigger tag member with condition  Date and time are from now till day later And email contains string '@como.com': action -send asset
    @Test
    public void testConditionDateAndTimeDayLaterAndEmailContainsActionSendAnAsset() throws Exception {


        //step 7
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextDay = Calendar.getInstance();
        calNextDay.add(Calendar.DAY_OF_WEEK, +1);
        Condition cond1 = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextDay);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond2 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation(member);

    }



    //Test trigger tag member with condition Birthday is Before or equal 'Today'
    // action -send asset
    @Test
    public void testConditionBirthdayIsBeboreOrEqualActionSendAnAsset() throws Exception {


        //step 8
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        DateOperator equal = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsBeforeOrEqualsTo);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, equal, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));

        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        //Create member
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, date);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation(member);
    }

    //Test trigger tag member with condition LastName is one of , action send asset
    @Test
    public void testConditionLastNameIsOneOfActionSendAnAsset() throws Exception {


        //step 9
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(AUTO_LAST_PREFIX + timestamp).append(',').append("XXXXX");
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.IsOneOf);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation(member);

    }

    //Test trigger tag member with condition Gender is not one of action send asset
    @Test
    public void testConditionGenderIsNotOneOfActionSendAnAsset() throws Exception {

        //step 10
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Male").append(',').append("Other");
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Isnotoneof);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Gender, equal, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));

        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation(member);

    }

    // Negative test
    //condition: TagOperation/Operation equals 'Untag' - condition result is false ,action not performed
    //Action: send asset
    @Test
    public void testWithConditionTagOperationOperationEqUntagActionSendAssetNegative() throws Exception {

        //step 11
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.TAG_OPERATION, TagOperation.Operation, equal, TagOperationValue.Untag.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionTagMember(member, tag);

        //check that the automation did not run
        checkDataStoreNegativeCheckByRuleID(RECEIVEDASSET);

    }

    // Negative test
    //condition: TagOperation/Operation not equals 'Tag' - condition result is false ,action not performed
    //Action: send asset
    @Test
    public void testWithConditionTagOperationOperationNotEqTagActionSendAssetNegative() throws Exception {

        //step 12
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.TAG_OPERATION, TagOperation.Operation, equal, TagOperationValue.Tag.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionTagMember(member, tag);

        checkDataStoreNegativeCheckByRuleID(RECEIVEDASSET);

    }

    //condition: TagOperation/Operation not equals 'Untag'
    //Action: send asset
    @Test
    public void testWithConditionTagOperationOperationNotEqUntagActionSendAsset() throws Exception {

        //step 13
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.TAG_OPERATION, TagOperation.Operation, equal, TagOperationValue.Untag.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation(member);

    }


    //condition: TagOperation/Operation equals 'Tag'
    //Action: send asset
    @Test
    @Category(Hub1Regression.class)
    public void testWithConditionTagOperationOperationEqTagActionSendAsset() throws Exception {

        //step 14
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.TAG_OPERATION, TagOperation.Operation, equal, TagOperationValue.Tag.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation(member);
    }

    //condition: TagOperation/Tag equals 'tagName'
    //Action: send asset
    @Test
    public void testWithConditionTagOperationTagEqualsActionSendAsset() throws Exception {

        //step 15
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.TAG_OPERATION, TagOperation.Tag, equal, tag);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation(member);
    }

    //condition: TagOperation/Tag String contaims suffix (timestamp)
    //Action: send asset
    @Test
    public void testWithConditionTagOperationTagStringContainsActionSendAsset() throws Exception {

        //step 16
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.TAG_OPERATION, TagOperation.Tag, equal, timestamp.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation(member);
    }

    //condition: TagOperation/Tag not equal "xxxx"
    //Action: send asset
    @Test
    public void testWithConditionTagOperationTagNotEqualActionSendAsset() throws Exception {

        //step 17
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.TAG_OPERATION, TagOperation.Tag, equal, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation(member);
    }

    //condition: ActionTags contains All
    //Action: send asset
    @Test
    public void testWithConditionActionTagsContainsAllActionSendAsset() throws Exception {
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //step 18
        //create smart automation of Update member details that will run action TagMember with tags action "tag1,tag2,tag3"
        createSmartAutomationUpdateMemberWithActionTagMember(Lists.newArrayList("tag1","tag2","tag3"));

        //create smart automation for tagMemberTrigger that will check condition Tag actions contains all "tag1,tag2,tag3"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsAll);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag1,tag2,tag3");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));

        performUpdateMemberDetails(member);

        Assert.assertTrue("The member did not get the tag sent, so the trigger did not started", MembersService.isTagExists(tag));
        log4j.info("The member got the gift sent by the automation");

        checkAutomationRunAndSendAssetToMember();
    }

    //Negative test
    // condition: ActionTags contains All
    //Action: send asset
    @Test
    public void testWithConditionActionTagsContainsAllNegative() throws Exception {

        //step 19
        //create smart automation of Update member details that will run action TagMember with tags action "tag1,tag2"
        createSmartAutomationUpdateMemberWithActionTagMember(Lists.newArrayList("tag1","tag2"));

        //create smart automation for tagMemberTrigger that will check condition Tag actions contains all "tag1,tag2,tag3"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsAll);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag1,tag2,tag3");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performUpdateMemberDetails(member);

        Assert.assertTrue("The member did not get the tag sent, so the trigger did not started", MembersService.isTagExists(tag));

        checkDataStoreNegativeCheckByRuleID(RECEIVEDASSET);

    }

    //condition: ActionTags contains One of
    //Action: send asset
    @Test
    public void testWithConditionActionTagsContainsOneOfActionSendAsset() throws Exception {
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //step 20
        //create smart automation of Update member details that will run action TagMember with tags action "tag1,tag2"
        createSmartAutomationUpdateMemberWithActionTagMember(Lists.newArrayList("tag1","tag2"));

        //create smart automation for tagMemberTrigger that will check condition Tag actions contains one of "tag4,tag2,tag3"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag4,tag2,tag3");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));

        performUpdateMemberDetails(member);

        Assert.assertTrue("The member did not get the tag sent, so the trigger did not started", MembersService.isTagExists(tag));
        log4j.info("The member got the gift sent by the automation");

        checkAutomationRunAndSendAssetToMember();

    }

    //Negative test
    // condition: ActionTags contains One of
    //Action: send asset
    @Test
    public void testWithConditionActionTagsContainsOneOfNegative() throws Exception {

        //step 21
        //create smart automation of Update member details that will run action TagMember with tags action "tag2,tag5"
        createSmartAutomationUpdateMemberWithActionTagMember(Lists.newArrayList("tag5","tag2"));

        //create smart automation for tagMemberTrigger that will check condition Tag actions contains one of"tag1,tag3,tag4"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag1,tag3,tag4");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performUpdateMemberDetails(member);

        Assert.assertTrue("The member did not get the tag sent, so the trigger did not started", MembersService.isTagExists(tag));

        checkDataStoreNegativeCheckByRuleID(RECEIVEDASSET);

    }


    //condition: UnTagOperation/Operation equals 'Untag'
    //Action: send asset
    @Test
    public void testWithConditionUnTagOperationOperationEqualsTagActionSendAsset() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.TAG_OPERATION, TagOperation.Operation, equal, TagOperationValue.Untag.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendUnTagAndCheckAutomation();

    }


    private void performTagAndUntag() throws Exception {

        //should activate the tag trigger
        MembersService.performTagMemberSmartAction(tag, "TagMember");
        Assert.assertTrue("The member did not get the tag sent", MembersService.isTagExists(tag));
        MembersService.performUntagMemberSmartAction(tag, "UnTagMember");
    }


    private void sendUnTagAndCheckAutomation() throws Exception {
        //should activate the Untag trigger
        performTagAndUntag();

        checkAutomationRunAndSendAssetToMember();

    }


    private void sendTagAndCheckAutomation(NewMember member) throws Exception {
        //should activate the tag trigger
        performAnActionTagMember(member, tag);

        checkAutomationRunAndSendAssetToMember();

    }


    private String createSmartAutomationUpdateMemberWithActionTagMember(ArrayList<String> actionTags) throws Exception {
        //Create smart automation with action tag member
        String automationName = "UpdateForTagTrigger_" + timestamp;
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(Triggers.UpdatesMembershipDetails, automationName, settings);
        createSmartAutomationScenarioForTagAction(automation, actionTags);
        return automationName;
    }



    private void createSmartAutomationScenarioForTagAction(SmartAutomation automation, ArrayList<String> tags) throws Exception {
        log4j.info("creating scenario");
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal, UpdateMemberOperationValue.UpdateMembership.toString());
        scenario.addCondition(cond, automation);
        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getTagAction(smartAction, automation, tags);
        scenario.addAction(updateSmartAction, automation);
        SmartAutomationService.createSmartAutomation(automation);

    }

    private SmartAutomationAction getTagAction(SmartAutomationAction smartAction, SmartAutomation automation, ArrayList<String> actionTags) throws Exception {

        smartAction.setPerformTheAction(SmartAutomationActions.TagMember.toString());
        TagMember tagMember = new TagMember(automation);
        tagMember.setTagText(tag);
        tagMember.setTag_member("Y");
        tagMember.setTags(actionTags, automation);
        smartAction.setTagMember(tagMember);
        return smartAction;

    }

    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

}
