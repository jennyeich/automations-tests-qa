package hub.smartAutomations.triggers;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.Gender;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.ApplyLimitTimes;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.JoinClubResponse;
import server.v2_8.UpdatedFields;
import utils.StringUtils;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

/**
 * Created by goni on 2/13/2017.
 * Test Rail: C4469
 */
public class UpdateMembershipDetailsTriggerTest extends BaseAutomationTest {

    @Override
    public Triggers getTrigger() {
        return Triggers.UpdatesMembershipDetails;
    }

    @Override
    public String getAutomationName() {
        return "Update_Details_" + timestamp;
    }

    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
            createSmartGift();
        }
    };


    //Test trigger update member details no condition action - tag member
    @Test
    public void testNoConditionActionTagMember() throws Exception {

        //step 1
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        createSmartAutomationScenario(automation, Lists.newArrayList(), AND, Lists.newArrayList(SmartAutomationActions.TagMember));//no condition


        checkDataStoreTagNotExist();

        performUpdateMemberDetails(member);

        checkAutomationRunAndTagMember();
    }

    //Test trigger update member details with condition: Membership/FirstName contains string "_Update"
    // action - tag member
    @Test
    public void testWithConditionFNameContainsNewNameActionTagMember() throws Exception {


        //step 2
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal1, "_Update");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        checkDataStoreTagNotExist();

        performUpdateMemberDetails(member);

        checkAutomationRunAndTagMember();

    }

    // Add New automation with:
    //condition: UpdateMemberOperation/Operation equals 'update member details'
    //Action: tag member
    @Test
    public void testWithConditionUpdateMemberOperationEqUpdateActionTagMember() throws Exception {

        //step 3
        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal1, UpdateMemberOperationValue.UpdateMembership.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member - see that because of the condition - the tag is not created when joining the club
        JoinClubResponse joinClubResponse = createMemberByJoinClub();
        key = StringUtils.StringToKey(joinClubResponse.getUserKey());
        checkDataStoreTagNotExist();

        updateMemeberDetails(joinClubResponse);

        checkAutomationRunAndTagMember();
    }




    // Add New automation with:
    //condition: UpdateMemberOperation/Operation equals 'update member details' AND ChangedFields/FirstName equal userName
    //Action: tag member
    @Test
    public void testWith2ConditionUpdateMemberOperationEqUpdateANDChangedFieldsFNameEqActionTagMember() throws Exception {


        //step 4
        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.CHANGED_FIELDS, ChangedFields.FirstName, equal1, AUTO_USER_PREFIX + timestamp);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal2, UpdateMemberOperationValue.UpdateMembership.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member - see that because of the condition - the tag is not created when joining the club
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkDataStoreTagNotExist();

        performUpdateMemberDetails(member);

        checkAutomationRunAndTagMember();
    }


    //Triggered when member: Update membership details
    //Scenarios 1-> Condition: Changed field/email equal "mockemail" (false condition)
    //                  Add Action: tag member scenario1
    // Scenarios 2-> No Condition
    //    Add Action: tag member scenarion2
    //
    //update member when user has the mail "username@como.com"
    //expect the tag 'scenario2'
    @Test
    public void testWith2ScenariosChangedFieldNotTrueSecondPassedActionTagMember() throws Exception {


        //Create member - see that because of the condition - the tag is not created when joining the club
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //step 5
        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.CHANGED_FIELDS, ChangedFields.Email, equal1, MOCK_EMAIL);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal2, UpdateMemberOperationValue.UpdateMembership.toString());
        //create first scenario
        tag = "scenario1";
        SmartAutomationScenario scenario1 = createScenarioWithTagAction(automation);
        scenario1.addCondition(cond1,automation);
        scenario1.addCondition(cond2,automation);

        //create scenario 2
        StringOperator equal3 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond3 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal3, UpdateMemberOperationValue.UpdateMembership.toString());

        tag = "scenario2";
        SmartAutomationScenario scenario2 = createScenarioWithTagAction(automation);
        scenario2.addCondition(cond3, automation);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

        performUpdateMemberDetails(member);


        checkDataStoreUpdatedWithTagByRuleID(tag);

        //Verify the automation run and the tag exists
        Assert.assertTrue("The member did not get the tag sent by the automation", MembersService.isTagExists(tag));
       log4j.info("The member got the tag -> the automation run");

    }

    // Add New automation with:
    //condition: UpdateMemberOperation/Operation not equals 'Join the club'
    //Action: tag member
    @Test
    public void testWithConditionUpdateMemberOperationNotEqJoinClubActionTagMember() throws Exception {

        //step 6
        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal1, UpdateMemberOperationValue.JoinTheClub.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        JoinClubResponse joinClubResponse = createMemberByJoinClub();
        key = StringUtils.StringToKey(joinClubResponse.getUserKey());

        checkDataStoreTagNotExist();

        updateMemeberDetails(joinClubResponse);

        checkAutomationRunAndTagMember();
    }


    // Add New automation with:
    //condition: Gender is one of "male,female" AND UpdateMemberOperation/Operation not equals 'Join the club'
    //Action: tag member
    @Test
    public void testWithConditionGenderIsOneofActionTagMember() throws Exception {

        //Create member - see that because of the condition - the tag is not created when joining the club
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //step 7
        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.IsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(Gender.MALE.name().toLowerCase()).append(',').append(Gender.FEMALE.name().toLowerCase());
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Gender, equal1, stringBuffer.toString());
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond2 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal2, UpdateMemberOperationValue.JoinTheClub.toString());

        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        checkDataStoreTagNotExist();

        performUpdateMemberDetails(member);

        checkAutomationRunAndTagMember();
    }

    //Test trigger update member details with limitation for member 1 time, Action: send asset
    @Test
    public void testScenarioLimit1TimeActionSendAsset() throws Exception {


        //step 8
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Create smart automation
        ApplyLimitTimes applyLimitTimes = new ApplyLimitTimes();
        applyLimitTimes.setApplyLimitTimes("Y");
        applyLimitTimes.setPerMemberCheckBox("Y");
        applyLimitTimes.setPerMemberTimes("1");
        settings.setApplyLimitTimes(applyLimitTimes);

        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        createSmartAutomationScenario(automation, Lists.newArrayList(), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));//no condition

        performUpdateMemberDetails(member); //first time - should get the asset
        //Verify the automation run and the gift was sent
        Assert.assertTrue("The member did not get the gift sent by the automation", MembersService.isGiftExists(smartGift.getTitle()));
       log4j.info("The member got the gift sent by the automation");

        //Get member userKey and turn into User_KEY
        checkDataStoreUpdatedWithAsset(1);

        performUpdateMemberDetails(member); //second time - should not get the asset
        //check that data store still has one userAction for this trigger
        checkDataStoreUpdatedWithAsset(1);

    }

    //Test trigger update member details with no limitation for member, Action: send asset
    //update member details twice - get asset twice
    @Test
    public void testScenarioNoTimesLimitActionSendAsset() throws Exception {


        //step 9
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        createSmartAutomationScenario(automation, Lists.newArrayList(), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));//no condition

        performUpdateMemberDetails(member); //first time - should get the asset
        //Verify the automation run and the gift was sent
        Assert.assertTrue("The member did not get the gift sent by the automation", MembersService.isGiftExists(smartGift.getTitle()));
       log4j.info("The member got the gift sent by the automation");

        //Get member userKey and turn into User_KEY
        checkDataStoreUpdatedWithAsset(1);

        performUpdateMemberDetails(member);
        //check that data store still has one userAction for this trigger
        checkDataStoreUpdatedWithAsset(2);

    }


    //Test trigger update details with condition DaysTime Today am pm AND Expiration date after today: action -tag member
    @Test
    public void testConditionDaysTimeAndExpirationDateEqualActionTagMember() throws Exception {


        //step 10
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);
        //Create member
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calNextYear);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar timeFrom = Calendar.getInstance();

        Calendar timeUntil = Calendar.getInstance();
        timeUntil.add(Calendar.HOUR, 5);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, ApplyIf.DAYSTIME, Calendar.getInstance(), timeFrom, timeUntil);

        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        checkDataStoreTagNotExist();

        performUpdateMemberDetails(member);

        checkAutomationRunAndTagMember();

    }

    //Test trigger update details with condition expiration date next year: action -tag member
    @Test
    public void testConditionExpirationDateActionTagMember() throws Exception {


        //step 11
        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, +1);
        //Create member with the expiration date next year
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, nextYear);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, equal1, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        checkDataStoreTagNotExist();

        performUpdateMemberDetails(member);

        checkAutomationRunAndTagMember();
    }

    //Test trigger update details with condition DaysTime 2 days (today & tomorrow): action -tag member
    @Test
    public void testConditionDaysTime2DaysTagMember() throws Exception {


        //step 12
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DAY_OF_WEEK, 1);

        //create today
        DaysTime day1 = hubAssetsCreator.generateSingleDay(Calendar.getInstance());
        //create next day
        DaysTime day2 = hubAssetsCreator.generateSingleDay(nextDay);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, Lists.newArrayList(day1, day2));
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        checkDataStoreTagNotExist();

        performUpdateMemberDetails(member);

        checkAutomationRunAndTagMember();

    }

    //Test trigger update details with Condition: Membership\Birthday is before 'Today'
    // then tag member
    @Test
    public void testWithConditionBirthdayIsBeforeDateActionTagMember() throws Exception {


        //step 13
        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        //Create member
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, date);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsBefore);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, equal1, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        checkDataStoreTagNotExist();

        performUpdateMemberDetails(member);

        checkAutomationRunAndTagMember();

    }

    //Test trigger update details with condition Date and time are from now till a year later : action -tag member
    @Test
    public void testConditionTimeAndDateActionTagMember() throws Exception {


        //step 14
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextYear);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        checkDataStoreTagNotExist();

        performUpdateMemberDetails(member);

        checkAutomationRunAndTagMember();
    }

    //Test trigger update details with condition Date and time are from now till a year later And email contains string '@como.com': action -tag member
    @Test
    public void testConditionTimeAndDateAndEmailContainsActionTagMember() throws Exception {


        //step 15
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextYear);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal2, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        checkDataStoreTagNotExist();

        performUpdateMemberDetails(member);

        checkAutomationRunAndTagMember();
    }




    // Negative test
    //condition: UpdateMemberOperation/Operation equals 'join the club' - condition result is false ,action not performed
    //Action: tag member
    @Test
    @Category(Hub1Regression.class)
    public void testWithConditionUpdateMemberOperationEqJoinClubActionTagMemberNegative() throws Exception {


        //step 16
        //Create member - see that because of the condition - the tag is not created when joining the club
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal1, UpdateMemberOperationValue.JoinTheClub.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        performUpdateMemberDetails(member);

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    // Negative test
    //condition: UpdateMemberOperation/Operation not equals 'update member details' - condition result is false ,action not performed
    //Action: tag member
    @Test
    public void testWithConditionUpdateMemberOperationNotEqUpdateActionTagMemberNegative() throws Exception {


        //step 17
        //Create member - see that because of the condition - the tag is not created when joining the club
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal1, UpdateMemberOperationValue.UpdateMembership.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        performUpdateMemberDetails(member);

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }

    // Add New automation with:
    //condition: UpdateMemberOperation/Operation equals 'update member details' AND ChangedFields/Gender is one of "male,female"
    //Action: tag member
    @Test
    public void testWith2ConditionUpdateMemberOperationEqUpdateANDChangedFieldsGenderIsOneOfActionTagMember() throws Exception {


        //step 18
        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.IsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(Gender.MALE.name().toLowerCase()).append(',').append(Gender.FEMALE.name().toLowerCase());
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Gender, equal1, stringBuffer.toString());
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal2, UpdateMemberOperationValue.UpdateMembership.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member - see that because of the condition - the tag is not created when joining the club
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performUpdateMemberGender(member);

        checkAutomationRunAndTagMember();
    }

    // Add New automation with:
    //condition: UpdateMemberOperation/Operation equals 'update member details' AND ChangedFields/ExpirationDate is after 2 years"
    //Action: tag member
    @Test
    public void testWith2ConditionUpdateMemberOperationEqUpdateANDChangedFieldsExpirationDateIsAfterActionTagMember() throws Exception {


        //step 19
        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);
        //Create member - see that because of the condition - the tag is not created when joining the club
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calNextYear);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal2, UpdateMemberOperationValue.UpdateMembership.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        performUpdateMemberExpirationDate(member);

        checkAutomationRunAndTagMember();
    }

    private void updateMemeberDetails(JoinClubResponse joinClubResponse) throws Exception {
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setPhoneNumber(joinClubResponse.getPhoneNumber());
        memberDetails.setFirstName(joinClubResponse.getFirstName() + "_Updated");
        MembersService.updateMemberDetails(memberDetails);
    }

    private JoinClubResponse createMemberByJoinClub() throws InterruptedException {
        String customerPhone = timestamp.toString();
        UpdatedFields updatedFields = new UpdatedFields();
        updatedFields.setPhoneNumber(customerPhone);
        updatedFields.setFirstName(AUTO_USER_PREFIX + customerPhone);
        updatedFields.setLastName( AUTO_LAST_PREFIX + customerPhone);
        return serverHelper.createMember(updatedFields);
    }

    private void performUpdateMemberGender(NewMember member) throws Exception {
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setPhoneNumber(member.getPhoneNumber());
        memberDetails.setGender(Gender.MALE.toString());
        MembersService.updateMemberDetails(memberDetails);
    }

    private void performUpdateMemberExpirationDate(NewMember member) throws Exception {
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setPhoneNumber(member.getPhoneNumber());
        Calendar calNext2Year = Calendar.getInstance();
        calNext2Year.add(Calendar.YEAR, 2);
        memberDetails.setMembershipExpiration(hubAssetsCreator.getDate(calNext2Year));
        MembersService.updateMemberDetails(memberDetails);
    }


    private void checkDataStoreTagNotExist() throws Exception {
        //check that the tag does not exist yet
        Query q = new Query("UserAction");
        String autoURL = SmartAutomationService.getAutomationURL(settings);
        String autoName = "automation_" + autoURL;
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, TAG_OPERATION), new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, key), new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, autoName))));
        List<Entity> res = dataStore.findByQuery(q);
        Assert.assertTrue("Should not have UserAction with the automation: " + autoName, res.isEmpty());
    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

}
