package hub.smartAutomations.triggers;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.TextAreaParameters;
import hub.common.objects.common.YesNoList;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AmountType;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SendAsset;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Jenny on 2/27/2017.
 *  TestRail : C4459
 */
public class ReceivesAnAssetTriggerTest extends BaseAutomationTest {

    @Override
    public Triggers getTrigger() {
        return Triggers.ReceivesAnAsset;
    }

    @Override
    public String getAutomationName() {
        return "Receives an asset_" + timestamp;
    }

    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
            createSmartGift();
        }
    };


    //Tests the send asset with notification message, with codition -first name equals ,action send push notification
    //@Test
    public void testSendAssetWithNotificationConditionFirstNameEqualsSendPushNotification() throws Exception {

        smartGift = hubAssetsCreator.createSmartGiftWithPushMessage(timestamp,"@" + TextAreaParameters.Membership_FirstName + " , you got new gift" );
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal1, "auto_user_" + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAPushNotification));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift with push notification to member - activate the trigger
        performAnActionSendGift(smartGift, YesNoList.YES.toString());

        //check that we have 2 push message (from asset and from automation
        checkDataStoreUpdatedWithSendPushNotification(2);
    }

    //Tests the send asset with notification message, with codition -first name equals ,action send push notification
   // @Test
    public void testSendAssetWithNotificationConditionFirstNameEqualsTagMember() throws Exception {

        smartGift = hubAssetsCreator.createSmartGiftWithPushMessage(timestamp,"@" + TextAreaParameters.Membership_FirstName + " , you got new gift" );
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal1, "auto_user_" + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift with push notification to member - activate the trigger
        performAnActionSendGift(smartGift, YesNoList.YES.toString());

        //check that we have 1 push message (from asset )
        checkDataStoreUpdatedWithSendPushNotification(1);
        checkAutomationRunAndTagMember();
    }

    //Tests the send asset with  codition - email not equals mock email ,action  tag member
    @Test
    public void testConditionEmailNotEqActionTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal1, MOCK_EMAIL);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));//no condition
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();

    }


    //Tests the send asset with  codition - first name equals ,action  tag member
    @Test
    public void testConditionFirstNameEqualsTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal1, "auto_user_" + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();
    }


    //Tests the send asset with  codition - last name equals ,action  tag member
    @Test
    public void testConditionMembershipLastNameEqualsConditionTagAction() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, AUTO_LAST_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();
    }


    //asset condition
    @Test
    public void testConditionAssetEqualsConditionTagAction() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartGift.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();
    }

    //asset condition
    @Test
    public void testConditionAssetEqualsConditionAddPoints() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartGift.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.AddSetNumberofPoints));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member - activate the trigger
        performAnActionSendGift(smartGift);

        checkDataStoreUpdatedWithPointsByRuleID("1000" , AmountType.Credit);
    }

    //Last name conds
    @Test
    public void testConditionLastEqualsORNotEqualsTagAction() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, AUTO_LAST_PREFIX + timestamp);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal2, AUTO_LAST_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), OR, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();
    }

    //CNP-4933
    @Test
    public void testConditionFieldNotExistORFieldExistTagAction() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, AUTO_LAST_PREFIX + timestamp);
        DateOperator equal2 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.NotEqualto);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Anniversary, equal2, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), OR, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();
    }

    //CNP-4933
    @Test
    public void testConditionFieldExistORFieldNotExistTagAction() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.NotEqualto);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Anniversary, equal1, Calendar.getInstance());
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal2, AUTO_LAST_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2), OR, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();
    }


    //Last name conds - not equals OR contains
    @Test
    public void testConditionLastContainsORNotEqualsTagAction() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, AUTO_LAST_PREFIX + timestamp);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal2, AUTO_LAST_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), OR, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();
    }

    //Last name conds - not equals OR contains
    @Test
    @Category(Hub1Regression.class)
    public void testMembershipPhoneNumberNotEqualORLastNameIsNOTOneOFTagActionNegativeTest() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Isnotoneof);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, AUTO_LAST_PREFIX + timestamp);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.PhoneNumber, equal2, timestamp.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), OR, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get member userKey and turn into User_KEY
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @Test
    public void testConditionLastNameEqualsTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, AUTO_LAST_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();
    }

    @Test
    public void testConditionLastNameIsNontOneOFTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Isnotoneof);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, "testLast");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();


    }

    //asset condition NOT equals  - negative test
    @Test
    public void testConditionAssetNOTEqualsConditionTagActioNegativeTest() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartGift.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendGift(smartGift);

        //Get member userKey and turn into User_KEY
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);


    }

    @Test
    public void testConditionBirthdayIsBeforeTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsBefore);
        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, equal1, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, date);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();


    }

    @Test
    public void testConditionExpirationDateAfterTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calNextYear);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();

    }


    //Test with condition DaysTime 2 days (today & tomorrow): action -tag member
    @Test
    public void testConditionDaysTime2DaysTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        Calendar nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DAY_OF_WEEK, 1);

        //create today
        DaysTime day1 = hubAssetsCreator.generateSingleDay(Calendar.getInstance());
        //create next day
        DaysTime day2 = hubAssetsCreator.generateSingleDay(nextDay);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, Lists.newArrayList(day1, day2));
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();

    }


    //Test condition DaysTime Today am pm : action -tag member
    @Test
    public void testConditionDaysTimeActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar timeFrom = Calendar.getInstance();

        Calendar timeUntil = Calendar.getInstance();
        timeUntil.add(Calendar.HOUR, 5);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, ApplyIf.DAYSTIME, Calendar.getInstance(), timeFrom, timeUntil);

        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();

    }

    //Test  condition Date and time are from now till a year later : action -tag member
    @Test
    public void testConditionTimeAndDateActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextYear);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();

    }

    //Test condition Date and time are from now till 2 days later And email contains string '@como.com': action -tag member
    @Test
    public void testConditionTimeAndDateAndEmailContainsActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextDay = Calendar.getInstance();
        calNextDay.add(Calendar.DAY_OF_WEEK, +2);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextDay);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal2, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendGiftAndCheckAutomation();

    }

    //condition: ActionTags contains One of
    //Action: tag member
    @Test
    public void testConditionActionTagsContainsOneOfActionSendAsset() throws Exception {
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action send asset with tags action "tag1,tag2"
        createSmartAutomationUpdateMemberWithActionReceiveAsset(Lists.newArrayList("tag1","tag2"));

        //create smart automation for receiveAssetTrigger that will check condition Tag actions contains one of "tag4,tag2,tag3"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag4,tag2,tag3");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        performUpdateMemberDetails(member);

        //Verify the automation run and the gift was sent
        Assert.assertTrue("The member did not get the gift sent by the automation,so the trigger did not started", MembersService.isGiftExists(smartGift.getTitle()));
        log4j.info("The member got the gift sent by the automation");

        checkAutomationRunAndTagMember();

    }

    //Negative test
    // condition: ActionTags contains One of
    //Action: tag member
    @Test
    public void testConditionActionTagsContainsOneOfNegative() throws Exception {


        //create smart automation of Update member details that will run action send asset with tags action "tag2,tag5"
        String updateAutomation = createSmartAutomationUpdateMemberWithActionReceiveAsset(Lists.newArrayList("tag5","tag2"));

        //create smart automation for receiveAssetTrigger that will check condition Tag actions contains one of"tag1,tag3,tag4"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag1,tag3,tag4");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performUpdateMemberDetails(member);

        //Verify the automation run and the gift was sent
        Assert.assertTrue("The member did not get the gift sent by the automation,so the trigger did not started", MembersService.isGiftExists(smartGift.getTitle()));
        log4j.info("The member got the gift sent by the automation");

        Thread.sleep(2000);//wait for data store to be updated after automation performed
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }


    private String createSmartAutomationUpdateMemberWithActionReceiveAsset(ArrayList<String> actionTags) throws Exception {
        //Create smart automation with action tag member
        String automationName = "UpdateForTrigger_" + timestamp;
        log4j.info("automationName: " + automationName);
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(Triggers.UpdatesMembershipDetails, automationName, settings);
        createSmartAutomationScenarioForReceiveAction(automation, actionTags);
        return automationName;
    }

    private void createSmartAutomationScenarioForReceiveAction(SmartAutomation automation, ArrayList<String> tags) throws Exception {
        log4j.info("creating scenario");
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal, UpdateMemberOperationValue.UpdateMembership.toString());
        scenario.addCondition(cond, automation);
        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getSendAssetAction(smartAction, automation, tags);
        scenario.addAction(updateSmartAction, automation);
        SmartAutomationService.createSmartAutomation(automation);

    }

    private void sendGiftAndCheckAutomation() throws Exception {
        //Send gift to member - activate the trigger
        performAnActionSendGift(smartGift);

        checkAutomationRunAndTagMember();
    }


    private SmartAutomationAction getSendAssetAction(SmartAutomationAction smartAction, SmartAutomation automation, ArrayList<String> actionTags) throws Exception {

        smartAction.setPerformTheAction(SmartAutomationActions.SendAnAsset.toString());
        SendAsset action = new SendAsset(automation);
        action.setAssetName(smartGift.getTitle());
        smartAction.setSendAsset(action);
        action.setTags(actionTags, automation);
        return smartAction;

    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }


}
