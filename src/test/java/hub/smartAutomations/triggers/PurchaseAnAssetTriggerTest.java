package hub.smartAutomations.triggers;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.benefits.PointShopPage;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.PerformActionOnResults;
import hub.common.objects.member.PerformAnActionOnMember;
import hub.common.objects.member.performActionObjects.AddPointsAction;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.services.NewApplicationService;
import hub.services.assets.AssetService;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.IServerResponse;
import server.v2_8.GetResource;
import server.v2_8.GetResourceResponse;
import server.v2_8.PurchaseAsset;
import server.v2_8.PurchaseAssetResponse;
import server.v2_8.base.ServicesErrorResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.GiftShopItem;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Goni on 5/16/2017.
 * Test Rail: C5387
 */
public class PurchaseAnAssetTriggerTest extends BaseAutomationTest {

    private static String userToken;//the user token that we receive after sign on
    private static String assetTemplateKey; //the asset template key as returns from GetResource request
    private static String itemID; //the itemId as returns from GetResource request

    private static String userKey; //the userKey that returns from JoinClub request
    private static String randomId;
    private static String phoneNumber; //the phone number that we use to find the user for all tests

    @Override
    public Triggers getTrigger() {
        return Triggers.PurchasesAnAsset;
    }

    @Override
    public String getAutomationName() {
        return "PurchaseAsset_" + randomId;
    }

    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            randomId = getRandomId();
            settings = new Settings();
            tag = "tag_" + randomId;
            if(smartGift == null) {
                //create assets
                try {
                    phoneNumber = String.valueOf(System.currentTimeMillis());
                    smartGift = hubAssetsCreator.createSmartGift(phoneNumber);
                } catch (Exception e) {
                    log4j.error("error while creating asset",e);
                    Assert.fail(e.getMessage());
                }

                try {
                    String version = hubAssetsCreator.addItemToPointShopAndGetVersionAfterPublish(smartGift.getTitle(),locationId);
                    getDataForPurchaseAssetRequest(version);

                } catch (Exception e) {
                    log4j.error("error while adding item to gift shop",e);
                    Assert.fail(e.getMessage());
                }
                try {
                    createUserAndUpdateForTests();

                } catch (Exception e) {
                    log4j.error("error while creating user and update details for test",e);
                    Assert.fail(e.getMessage());
                }
            }
            //each test should initialize the user key for the data store
            key = automationsHelper.getKey(userKey);
        }
    };

    private void getDataForPurchaseAssetRequest(String version) {

        GiftShopItem item = serverHelper.getGiftShopItem(version);
        assetTemplateKey = item.getAssetKey();
        itemID = item.getItemID();
    }


    //Tests the send asset with  codition - first name equals ,action  tag member
    @Test
    public void testConditionFirstNameEqualsTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, oper, AUTO_USER_PREFIX + phoneNumber);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        checkAutomationRunAndTagMember();

    }

    //Test trigger tag member with condition Gender is not one of action tag member
    @Test
    public void testConditionGenderIsNotOneOfActionTagMember() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Male").append(',').append("Other");
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Isnotoneof);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Gender, equal, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        checkAutomationRunAndTagMember();

    }

    // action : tag  member with one condition :email string not contain "@como.com" suffix
    //check that no user action was created in data store
    @Test
    public void testWithConditionEmailStringNotContainsTagMemberNegative() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.DoesntContain);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.Email,equal1, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @Test
    public void testConditionLastNameIsOneOfActionTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(AUTO_LAST_PREFIX + phoneNumber).append(',').append("XXXXX");
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.IsOneOf);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testWithConditionEqualPhoneTagMember() throws Exception {

        //Create smart automatio
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.PhoneNumber,equal1,phoneNumber);
        createSmartAutomationScenario( automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        checkAutomationRunAndTagMember();

    }


    @Test
    public void testConditionBirthdayIsBeforeTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsBefore);
        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, equal1, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionExpirationDateAfterTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        checkAutomationRunAndTagMember();

    }


    //Test with condition DaysTime 2 days (today & tomorrow): action -tag member
    @Test
    public void testConditionDaysTime2DaysTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        Calendar nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DAY_OF_WEEK, 1);

        //create today
        DaysTime day1 = hubAssetsCreator.generateSingleDay(Calendar.getInstance());
        //create next day
        DaysTime day2 = hubAssetsCreator.generateSingleDay(nextDay);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, Lists.newArrayList(day1, day2));
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        checkAutomationRunAndTagMember();

    }


    //Test condition DaysTime Today am pm : action -tag member
    @Test
    public void testConditionDaysTimeActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar timeFrom = Calendar.getInstance();

        Calendar timeUntil = Calendar.getInstance();
        timeUntil.add(Calendar.HOUR, 5);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, ApplyIf.DAYSTIME, Calendar.getInstance(), timeFrom, timeUntil);

        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        checkAutomationRunAndTagMember();


    }

    //Test  condition Date and time are from now till a year later : action -tag member
    @Test
    public void testConditionTimeAndDateActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextYear);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        checkAutomationRunAndTagMember();


    }

    //    Negative: action not performed
    //    Condition : Expiration Date is After today
    //    Add Action: tag member
    //    send gift to the member with expiration date as a year before today and redeem
    @Test
    public void testConditionExpirationDateIsAfterTodayNegative() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 2);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr,cal);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    //Test condition Date and time are from now till 2 days later And email contains string '@como.com': action -tag member
    @Test
    public void testConditionTimeAndDateAndEmailContainsActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextDay = Calendar.getInstance();
        calNextDay.add(Calendar.DAY_OF_WEEK, +2);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextDay);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal2, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        checkAutomationRunAndTagMember();


    }

    @Test
    @Category(Hub1Regression.class)
    public void testConditionEmailDoesntContainsAndEmailContainsActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.DoesntContain);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal1,MOCK_EMAIL);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal2, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        checkAutomationRunAndTagMember();


    }

    //************************ asset conditions *******************************************
    //@Test
    //BUG: CNP-6044
    public void testConditionAssetEqualsConditionTagAction() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartGift.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        checkAutomationRunAndTagMember();


    }

    //asset condition NOT equals  - negative test
   // @Test
    //BUG: CNP-6044
    public void testConditionAssetNOTEqualsConditionTagActioNegative() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartGift.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //purchase asset from gift shop
        performPurchaseAsset();

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);


    }

    //Create the user as a mobile client to simulate the purchase an assset trigger from point shop
    private void createUserAndUpdateForTests() throws Exception {
        //signOn and get the user token
        userToken = serverHelper.signOn();
        log4j.info("userToken: " + userToken);
        //join the club with the user token
        userKey = automationsHelper.joinClubWithUserToken(phoneNumber, userToken);

        log4j.info("userKey: " + userKey);
        FindMember findMember = new FindMember();
        findMember.setUserKey(userKey);
        MembersService.findAndGetMember(findMember);
        performAnActionAddPoints("300000");
        log4j.info("Find member according to user key and give user 300000 points for using in all tests");
        automationsHelper.performUpdateMemberDetails(phoneNumber);
        log4j.info("Update user with relevant details(phone,name,birthday)");
        key = automationsHelper.getKey(userKey);
    }


    //Add the user points to use in all tests when purchasing the asset
    private void performAnActionAddPoints(String numOfPoints) throws Exception {

        log4j.info("perform action add points");
        MembersService.performSmartActionAddPoints(numOfPoints, String.valueOf(System.currentTimeMillis()));

    }


    private void performPurchaseAsset() {

        PurchaseAsset purchaseAsset = new PurchaseAsset();
        GiftShopItem giftShopItem = new GiftShopItem();
        giftShopItem.setAssetKey(assetTemplateKey);
        giftShopItem.setPrice("2");
        giftShopItem.setItemID(itemID);
        purchaseAsset.setItem(giftShopItem);

        IServerResponse response = purchaseAsset.sendRequestByTokenAndLocation(userToken,locationId, PurchaseAssetResponse.class);
        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }
    }




    private String getRandomId() {
        return String.valueOf(System.currentTimeMillis());
    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();

    }

    @AfterClass
    public static void removeItemsFromPointShop() throws Exception{
        AssetService.removeItemsFromPointShop();
    }
}
