package hub.smartAutomations.triggers;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.TextAreaParameters;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.SmartPunchCard;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.PunchAPunchCard;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SendAsset;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.utils.ServerHelper;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Jenny on 3/26/2017.
 * Test Rail: C4457
 */

public class PunchApunchCardTriggerTest extends BaseAutomationTest {

    @Override
    public Triggers getTrigger() {
        return Triggers.PunchesAPunchCard;
    }

    @Override
    public String getAutomationName() {
        return "Punch a punch card_" + timestamp;
    }


    @Before
    public void checkActiveAutomations() {
        checkSmartAutomation();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {


        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
            if (smartPunchCard == null) {
                //create assets
                try {
                    smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");
                } catch (Exception e) {
                    log4j.error(e);
                    Assert.fail("error while creating asset");
                }
            }
        }
    };


    //1 - Tests the send asset with  codition - email not equals mock email ,action  tag member
    @Test
    public void testConditionEmailNotEqActionTagMember1() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal1, MOCK_EMAIL);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();


    }


    //2 - Tests the send asset with  codition - first name equals ,action  tag member
    @Test
    public void testConditionFirstNameEqualsTagMember2() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal1, AUTO_USER_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();


    }


    //3 - Tests the punch a punch card trigger with  codition - last name equals ,action  tag member
    @Test
    @Category(Hub1Regression.class)
    public void testConditionMembershipLastNameEqualsConditionTagAction3() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, AUTO_LAST_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();

    }


    //4 - asset condition
    @Test
    public void testConditionAssetEqualsConditionTagAction4() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartPunchCard.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionAssetEqualsAndConditionLastPunchYesActionSendAssetAndPushNotification() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator oper1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.PUNCH, Punch.LastPunch, oper1, "Yes");

        StringOperator oper2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, oper2, smartPunchCard.getTitle());

        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);
        scenario.addCondition(cond1, automation);
        scenario.addCondition(cond2, automation);

        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = hubAssetsCreator.getSendAssetAction(smartAction, automation, smartPunchCard.getTitle());
        scenario.addAction(updateSmartAction, automation);
        smartAction = new SmartAutomationAction();
        String txtMessage =  "@" + TextAreaParameters.Membership_FirstName + " hello. You got push message.";
        updateSmartAction = hubAssetsCreator.getSendPushNotificationAction(smartAction, automation,txtMessage, "Y");
        scenario.addAction(updateSmartAction, automation);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performPunchAPunchCardNoValidation(smartPunchCard,5);

        //verify asset was sent to member and also push action performed
        checkDataStoreWithOperation(RECEIVEDASSET);

        checkDataStoreUpdatedWithSendPushNotification(1);

    }

    private void checkDataStoreWithOperation(String operation) throws InterruptedException {
        log4j.info("check Data Store Updated with asset");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        java.util.List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the automation: " + automationRuleId,
                buildQuery(operation,predicate) , 1, dataStore);
        Assert.assertEquals("Must find one UserAction with the automation: " + automationRuleId, 1, res.size());
    }

    //5 - Last name conds
    @Test
    public void testConditionLastEqualsORNotEqualsTagAction5() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, AUTO_LAST_PREFIX + timestamp);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal2, AUTO_LAST_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), OR, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();
    }


    //6 - Last name conds - not equals OR contains
    @Test
    public void testConditionLastContainsORNotEqualsTagAction6() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, AUTO_LAST_PREFIX + timestamp);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal2, AUTO_LAST_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), OR, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);


        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();
    }

    //7 - Last name conds - not equals OR contains
    @Test
    public void testMembershipPhoneNumberNotEqualORLastNameIsNOTOneOFTagActionNegativeTest7() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Isnotoneof);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, AUTO_LAST_PREFIX + timestamp);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.PhoneNumber, equal2, timestamp.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), OR, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Get member userKey and turn into User_KEY
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    //8 last name is
    @Test
    public void testConditionLastNameEqualsTagMember8() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, AUTO_LAST_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();
    }

    //9 test
    @Test
    public void testConditionLastNameIsNontOneOFTagMember9() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Isnotoneof);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, "testLast");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();

    }

    //Bug:
    //10 asset condition NOT equals  - negative test
    @Test
    public void testConditionAssetNOTEqualsConditionTagActioNegativeTest10() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartPunchCard.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Get member userKey and turn into User_KEY
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);


    }

    //11
    @Test
    public void testConditionBirthdayIsBeforeTagMember11() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsBefore);
        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, equal1, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, date);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();


    }

    //12
    @Test
    public void testConditionExpirationDateAfterTagMember12() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calNextYear);

        //Get user's key
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();

    }

    // 13 - Test with condition DaysTime 2 days (today & tomorrow): action -tag member
    @Test
    public void testConditionDaysTime2DaysTagMember13() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);

        Calendar nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DAY_OF_WEEK, 1);

        //create today
        DaysTime day1 = hubAssetsCreator.generateSingleDay(Calendar.getInstance());
        //create next day
        DaysTime day2 = hubAssetsCreator.generateSingleDay(nextDay);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, Lists.newArrayList(day1, day2));
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();

    }


    // 14 - Test condition DaysTime Today am pm : action -tag member
    @Test
    public void testConditionDaysTimeActionTagMember14() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        Calendar timeFrom = Calendar.getInstance();

        Calendar timeUntil = Calendar.getInstance();
        timeUntil.add(Calendar.HOUR, 5);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, ApplyIf.DAYSTIME, Calendar.getInstance(), timeFrom, timeUntil);

        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();


    }

    // 15 - Test  condition Date and time are from now till a year later : action -tag member
    @Test
    public void testConditionTimeAndDateActionTagMember15() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextYear);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();


    }

    // 16 - Test condition Date and time are from now till 2 days later And email contains string '@como.com': action -tag member
    @Test
    public void testConditionTimeAndDateAndEmailContainsActionTagMember16() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextDay = Calendar.getInstance();
        calNextDay.add(Calendar.DAY_OF_WEEK, +2);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextDay);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal2, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);

        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();


    }


    // 17 - condition: ActionTags contains One of
    //Action: tag member
    @Test
    public void testConditionActionTagsContainsOneOfActionSendAsset17() throws Exception {


        //create smart automation of Update member details that will run action send asset with tags action "tag1,tag2"
        createSmartAutomationUpdateMemberWithActionPunchAPunchCard(Lists.newArrayList("tag1","tag2"));

        //create smart automation for punch a punch card Trigger that will check condition Tag actions contains one of "tag4,tag2,tag3"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag1,tag2,tag3");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);

        performUpdateMemberDetails(member);

        //Verify the automation run and the gift was sent
        Assert.assertTrue("The member did not get the punch with tags sent by the automation", MembersService.isGiftExists(smartPunchCard.getTitle()));
      log4j.info("The member got the punch with tags sent by the automation");

        //check the DB that tag exists
        checkDataStoreUpdatedWithTagByRuleID();


    }

    //18 - Negative test
    // condition: ActionTags contains One of
    @Test
    public void testConditionActionTagsContainsOneOfNegative18() throws Exception {


        //create smart automation of Update member details that will run action send asset with tags action
        createSmartAutomationUpdateMemberWithActionPunchAPunchCard(Lists.newArrayList("tag5","tag2"));

        //create smart automation for receiveAssetTrigger that will check condition Tag actions contains one of"tag1,tag3,tag4"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag1,tag3,tag4");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);


        performUpdateMemberDetails(member);

        //make sure puchcard trigger invoked
        MembersService.isPunchCardPunched(smartPunchCard.getTitle());

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }

    //25 - Tests last punch condition
    @Test
    public void testConditionLastPunchActionTagMember25() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.PUNCH, Punch.LastPunch, equal1, "Yes");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchLastPunchCard(smartPunchCard);

        //Get member userKey and turn into User_KEY
        checkDataStoreUpdatedWithTagByRuleID();


    }


    //26 - Tests total number of punches
    @Test
    public void testConditionTotalNumOfPunchesActionTagMember26() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PUNCH, Punch.TotalPunchesAfterOperation, equal1, "5");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchLastPunchCard(smartPunchCard);

        //Get member userKey and turn into User_KEY
        checkDataStoreUpdatedWithTagByRuleID();


    }


    private String createSmartAutomationUpdateMemberWithActionReceiveAsset(String actionTags) throws Exception {
        //Create smart automation with action tag member
        String automationName = "UpdateForTrigger_" + timestamp;
        log4j.info("automationName: " + automationName);
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(Triggers.UpdatesMembershipDetails, automationName, settings);
        createSmartAutomationScenarioForReceiveAction(automation, Lists.newArrayList(actionTags));
        return automationName;
    }




    private String createSmartAutomationUpdateMemberWithActionPunchAPunchCard(ArrayList<String> actionTags) throws Exception {
        //Create smart automation with action tag member
        String automationName = "UpdateForTrigger_" + timestamp;
        log4j.info("automationName: " + automationName);
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(Triggers.UpdatesMembershipDetails, automationName, settings);
        createSmartAutomationScenarioForPunchAPunchCard(automation, actionTags);
        return automationName;
    }


    private void createSmartAutomationScenarioForReceiveAction(SmartAutomation automation, ArrayList<String> tags) throws Exception {
        log4j.info("creating scenario");
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal, UpdateMemberOperationValue.UpdateMembership.toString());
        scenario.addCondition(cond, automation);
        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getSendAssetAction(smartAction, automation, tags);
        scenario.addAction(updateSmartAction, automation);
        SmartAutomationService.createSmartAutomation(automation);

    }

    private void createSmartAutomationScenarioForPunchAPunchCard(SmartAutomation automation, ArrayList<String> tags) throws Exception {
        log4j.info("creating scenario");
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal, UpdateMemberOperationValue.UpdateMembership.toString());
        scenario.addCondition(cond, automation);
        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getPunchAPunchCardAction(smartAction, automation, tags);
        scenario.addAction(updateSmartAction, automation);
        SmartAutomationService.createSmartAutomation(automation);

    }

    private SmartAutomationAction getSendAssetAction(SmartAutomationAction smartAction, SmartAutomation automation, ArrayList<String> actionTags) throws Exception {

        smartAction.setPerformTheAction(SmartAutomationActions.SendAnAsset.toString());
        SendAsset action = new SendAsset(automation);
        action.setAssetName(smartPunchCard.getTitle());
        smartAction.setSendAsset(action);
        action.setTags(actionTags, automation);
        return smartAction;

    }


    private SmartAutomationAction getPunchAPunchCardAction(SmartAutomationAction smartAction, SmartAutomation automation, ArrayList<String> actionTags) throws Exception {

        smartAction.setPerformTheAction(SmartAutomationActions.PunchThePunchCard.toString());
        PunchAPunchCard punchAPunchCard = new PunchAPunchCard(automation);
        punchAPunchCard.setNumOfPunches("1");
        punchAPunchCard.setAssetName(smartPunchCard.getTitle());
        punchAPunchCard.setTags(actionTags,automation);
        smartAction.setPunchAPunchCard(punchAPunchCard);
        return smartAction;
    }



    private void performAnActionPunchLastPunchCard(SmartPunchCard punchCard1) throws Exception {

        Thread.sleep(5000); //Wait before th punch card will be send
        MembersService.performSmartActionPunchAPunchCard(punchCard1.getTitle(),5);

    }

    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

}

