package hub.smartAutomations.triggers;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.common.YesNoList;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.smarObjects.Scenario;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.ApplyLimitTimes;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.assets.AssetService;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.utils.ServerHelper;

import java.util.List;

/**
 * Created by Goni on 6/4/2017.
 */
public class SmartExtrasTest extends BaseAutomationTest {


    @Override
    public Triggers getTrigger() {
        return Triggers.UpdatesMembershipDetails;
    }

    @Override
    public String getAutomationName() {
        return "UpdateMember_" + timestamp;
    }
    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }


    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;

        }
    };


    //Test trigger update member details with limitation for member 1 time, Action: tag member
    @Test
    public void testScenarioLimit1TimeActionTagMember() throws Exception {

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Create smart automation
        ApplyLimitTimes applyLimitTimes = new ApplyLimitTimes();
        applyLimitTimes.setApplyLimitTimes("Y");
        applyLimitTimes.setPerMemberCheckBox("Y");
        applyLimitTimes.setPerMemberTimes("1");
        settings.setApplyLimitTimes(applyLimitTimes);

        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        createSmartAutomationScenario(automation, Lists.newArrayList(), AND, Lists.newArrayList(SmartAutomationActions.TagMember));//no condition

        performUpdateMemberDetails(member); //first time - should get the tag

        //Verify the automation run and the tag exists
        Assert.assertTrue("The member did not get the tag sent by the automation", MembersService.isTagExists(tag));
        log4j.info("The member got the tag -> the automation run");

        checkDataStoreUpdatedWithTag(tag,1);

        performUpdateMemberDetails(member); //second time - should not get the tag
        //check that data store still has one userAction for this trigger
        checkDataStoreUpdatedWithTag(tag,1);

    }

    //Test trigger update member details with limitation for member 2 time and condition in scenario, Action: tag member
    //run the trigger 3 times when only 2 times the condition is true.See that the limitation counter is counting it.
    @Test
    public void testScenarioLimit2With3RunsWhen1NotTrueTagAction1() throws Exception {

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Create smart automation
        setLimitTimes("2");

        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal1, "_Update");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));//no condition

        performUpdateMemberDetails(member,AUTO_USER_PREFIX + "_Updated"); //first time - should get the tag

        //Verify the automation run and the tag exists
        Assert.assertTrue("The member did not get the tag sent by the automation", MembersService.isTagExists(tag));
        log4j.info("The member got the tag -> the automation run");

        checkDataStoreUpdatedWithTag(tag,1);

        performUpdateMemberDetails(member,AUTO_USER_PREFIX + "xxx"); //second time - should not get the tag
        //check that data store still has one userAction for this trigger - the condition result here is false
        checkDataStoreUpdatedWithTag(tag,1);

        performUpdateMemberDetails(member,AUTO_USER_PREFIX + "_Updated"); //third time - should get the tag again
        //check that data store still has one userAction for this trigger since the limit of triggerring the automation was set to 2
        checkDataStoreUpdatedWithTag(tag,2);

    }

    //Test trigger update member details with limitation for member 2 time condition on trigger, Action: tag member
    //run the trigger 2 times .See that the limitation counter is counting it.
    @Test
    public void testConditionTriggerLimit2With2RunsWhen1NotTrueTagAction1() throws Exception {

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Create smart automation
        setLimitTimes("2");

        SmartAutomation automation = new SmartAutomation();
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal1, "Update");
        settings.setAutomationName(getAutomationName());
        settings.setTrigger(getTrigger());
        settings.addCondition(cond,automation);
        automation.setSettings(settings);
        createScenarioWithTagAction(automation);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

        performUpdateMemberDetails(member,AUTO_USER_PREFIX + "Updated"); //first time - should get the tag

        //Verify the automation run and the tag exists
        Assert.assertTrue("The member did not get the tag sent by the automation", MembersService.isTagExists(tag));
        log4j.info("The member got the tag -> the automation run");

        checkDataStoreUpdatedWithTag(tag,1);

        performUpdateMemberDetails(member,AUTO_USER_PREFIX + "xxx"); //second time - should not get the tag
        //check that data store still has one userAction for this trigger - the condition result here is false
        checkDataStoreUpdatedWithTag(tag,1);

    }

    //Test trigger update member details with limitation for member 1 time, Action: tag member
    //create 2 automations that will run,both have limit tag but the limitation is 1 so only one user action will be created
    @Test
    public void testScenarioLimit1WithLimitationTagActionTagMember() throws Exception {

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Create smart automation 1
        setLimitTimes("1");

        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        String automationRuleId1 = createSmartAutomation(automation, Lists.newArrayList(), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create smart automation 2
        settings.setAutomationName("automation2_" + timestamp);
        automation =hubAssetsCreator.createSmartAutomation(this,settings);
        String automationRuleId2 = createSmartAutomation(automation, Lists.newArrayList(), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //automations should run only once - should get the one tag
        performUpdateMemberDetails(member);

        checkDataStoreUpdatedWithTag(automationRuleId1,automationRuleId2);
    }

    //Test trigger update member details with no limitation for member , Action: tag member
    //create 2 automations that will run,both have limit tag but no limitation  - both automations should run
    @Test
    @Category(Hub1Regression.class)
    public void testScenariotWithLimit5WithLimitationTagActionTagMember() throws Exception {

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Create smart automation 1
        setLimitTimes("5");

        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        String automationRuleId1 = createSmartAutomation(automation, Lists.newArrayList(), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create smart automation 2
        settings.setAutomationName("automation2_" + timestamp);
        automation =hubAssetsCreator.createSmartAutomation(this,settings);
        String tag2 = "tag2";
        String automationRuleId2 = createSmartAutomation(automation, Lists.newArrayList(), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //automations should run only once - should get the one tag
        performUpdateMemberDetails(member);

        checkDataStoreUpdatedWithTagByRuleId(automationRuleId1);
        checkDataStoreUpdatedWithTagByRuleId(automationRuleId2);
    }

    private void setLimitTimes(String perMemberTimes) {
        ApplyLimitTimes applyLimitTimes = new ApplyLimitTimes();
        applyLimitTimes.setApplyLimitTimes("Y");
        applyLimitTimes.setPerMemberCheckBox("Y");
        applyLimitTimes.setLimitationTag("limitTag");
        applyLimitTimes.setPerMemberTimes(perMemberTimes);
        settings.setApplyLimitTimes(applyLimitTimes);
    }

    //Test trigger update member details with no limitation for member, Action: tag member
    //update member details twice - get tag twice
    @Test
    public void testScenarioNoTimesLimitActionTagMember() throws Exception {

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        createSmartAutomationScenario(automation, Lists.newArrayList(), AND, Lists.newArrayList(SmartAutomationActions.TagMember));//no condition

        performUpdateMemberDetails(member); //first time - should get the tag

        //Verify the automation run and the tag exists
        Assert.assertTrue("The member did not get the tag sent by the automation", MembersService.isTagExists(tag));
        log4j.info("The member got the tag -> the automation run");


        checkDataStoreUpdatedWithTag(tag,1);

        performUpdateMemberDetails(member);
        //check that data store has 2 userAction for this trigger
        checkDataStoreUpdatedWithTag(tag,2);

    }



    //Triggered when member: Update membership details
    //Scenarios 1-> Condition: Membership/email equal "mockemail" (false condition)
    //                  Add Action: tag member scenario1
    // Scenarios 2-> No Condition
    //    Add Action: tag member scenarion2
    //
    //update member when user has the mail "username@como.com"
    //expect the tag 'scenario2'
    @Test
    public void testTriggerWith2Scenarios() throws Exception {


        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();


        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal1, MOCK_EMAIL);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal2, UpdateMemberOperationValue.UpdateMembership.toString());
        //create first scenario
        tag = "scenario1";
        SmartAutomationScenario scenario1 = createScenarioWithTagAction(automation);
        scenario1.addCondition(cond1,automation);
        scenario1.addCondition(cond2,automation);

        tag = "scenario2";
        createScenarioWithTagAction(automation);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

        performUpdateMemberDetails(member);

        checkDataStoreUpdatedWithTagByRuleID(tag);

        //Verify the automation run and the tag exists
        Assert.assertTrue("The member did not get the tag sent by the automation", MembersService.isTagExists(tag));
       log4j.info( "The member got the tag -> the automation run");


    }

    //Test trigger update member details with no limitation for member, Action: tag member
    //update member details twice - get tag
    @Test
    public void testConditionInSettingsActionTagMember() throws Exception {

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Create smart automation
        SmartAutomation automation = new SmartAutomation();
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal1, "_Update");
        settings.setAutomationName(getAutomationName());
        settings.setTrigger(getTrigger());
        settings.addCondition(cond,automation);
        automation.setSettings(settings);

        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getAction(SmartAutomationActions.TagMember.toString(), smartAction, automation);
        scenario.addAction(updateSmartAction, automation);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);


       performUpdateMemberDetails(member); //first time - should get the tag

        //Verify the automation run and the tag exists
        Assert.assertTrue("The member did not get the tag sent by the automation", MembersService.isTagExists(tag));
       log4j.info( "The member got the tag -> the automation run");

        checkDataStoreUpdatedWithTag(tag,1);

    }
    //Test trigger update member details with no limitation for member and condition in setting returns false, Action: tag member
    //update member details twice - dont get tag
    @Test
    public void testConditionInSettingsActionTagMemberNegative() throws Exception {

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Create smart automation
        SmartAutomation automation = new SmartAutomation();
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal1, "Update");
        settings.setAutomationName(getAutomationName());
        settings.setTrigger(getTrigger());
        settings.addCondition(cond,automation);
        automation.setSettings(settings);

        createScenarioWithTagAction(automation);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

        performUpdateMemberDetails(member);

       checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }


    //Test trigger update member details with no limitation for member, Action: tag member
    //update member details twice - get tag
    @Test
    public void testConditionInSettingsAndInScenarioActionTagMember() throws Exception {

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Create smart automation
        SmartAutomation automation = new SmartAutomation();
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal1, "_Update");
        settings.setAutomationName(getAutomationName());
        settings.setTrigger(getTrigger());
        settings.addCondition(cond,automation);
        automation.setSettings(settings);

        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, oper, "_Update");
        scenario.addCondition(cond1,automation);
        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getAction(SmartAutomationActions.TagMember.toString(), smartAction, automation);
        scenario.addAction(updateSmartAction, automation);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);


        performUpdateMemberDetails(member); //should get the tag

        //Verify the automation run and the tag exists
        Assert.assertTrue("The member did not get the tag sent by the automation", MembersService.isTagExists(tag));
       log4j.info( "The member got the tag -> the automation run");

        checkDataStoreUpdatedWithTag(tag,1);

    }

    @Test
    public void testTriggerWith2ScenariosAndConditionInSettings() throws Exception {

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Create smart automation
        SmartAutomation automation = new SmartAutomation();
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal1, "_Update");
        settings.setAutomationName(getAutomationName());
        settings.setTrigger(getTrigger());
        settings.addCondition(cond,automation);
        automation.setSettings(settings);

        //create first scenario
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal2, MOCK_EMAIL);
        //create first scenario
        tag = "scenario1";
        SmartAutomationScenario scenario = createScenarioWithTagAction(automation);
        scenario.addCondition(cond1, automation);

        //create second scenario
        tag = "scenario2";
        createScenarioWithTagAction(automation);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);


        performUpdateMemberDetails(member); //should get the second tag (of scenario2)

        //Verify the automation run and the tag exists
        Assert.assertTrue("The member did not get the tag sent by the automation", MembersService.isTagExists(tag));
       log4j.info( "The member got the tag -> the automation run");

        checkDataStoreUpdatedWithTag(tag,1);

    }

    //Test trigger update member details with no limitation for member, Action: tag member
    //update member details twice - get tag twice
    @Test
    public void testUpdateMemberWithActionSendAssetWithPushMessage() throws Exception {

        smartGift = hubAssetsCreator.buildSmartGift(timestamp);

        StringOperator oper = hubAssetsCreator.getStringAssetOperator(smartGift, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(smartGift,ApplyIf.MEMBERSHIP,Membership.FirstNAme,oper,AUTO_USER_PREFIX + timestamp.toString());
        Scenario scenario = createSmartGiftScenario(smartGift);
        scenario.addCondition(cond,smartGift);
        AssetService.createSmartGift(smartGift);
        //Create smart automation
        SmartAutomation automation =hubAssetsCreator.createSmartAutomation(this,settings);
        createSmartAutomationScenario(automation, Lists.newArrayList(), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSendGift(smartGift, YesNoList.YES.toString());

        //check that we have 1 push message (from asset )
        //checkDataStoreUpdatedWithSendPushNotification(1);
    }

    public static Scenario createSmartGiftScenario(SmartGift smartGift) {

        Scenario scenario = new Scenario();
        smartGift.addScenario(scenario);
        return scenario;

    }

    private void performUpdateMemberDetails(NewMember member,String name) throws Exception {
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setPhoneNumber(member.getPhoneNumber());
        memberDetails.setFirstName(name);
        MembersService.updateMemberDetails(memberDetails);
    }

    //we dont use here the query wait because we need to check 2 rule ids
    private void checkDataStoreUpdatedWithTag(String ruleId1,String ruleId2) throws Exception {

        log4j.info("check Data Store Updated with Tag");
        Thread.sleep(10000);
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, ruleId1);
        List<Entity> res = dataStore.findByQuery( buildQuery(TAG_OPERATION,predicate));

        boolean found = (res.size() ==1);//found 1 user action 'tag_operation

        predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, ruleId2);
        res = dataStore.findByQuery( buildQuery(TAG_OPERATION,predicate));
        found = (found && (res.size()==0)) || (!found && (res.size()==1));

        Assert.assertTrue("only one user action 'tag_operation' should be created",found);
    }

    private void checkDataStoreUpdatedWithTagByRuleId(String ruleId) throws Exception {

        log4j.info("check Data Store Updated with Tag");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, ruleId);
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the automation: " + ruleId,
                buildQuery(TAG_OPERATION,predicate) , 1, dataStore);
        Assert.assertEquals("Only one user action 'tag_operation' should be created",res.size() , 1);
    }

    private String createSmartAutomation(SmartAutomation automation, java.util.List<Condition> condList, boolean isOR, java.util.List<SmartAutomationActions> actionList) throws Exception {
        log4j.info("creating scenario");
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);


        for (SmartAutomationActions actionName : actionList) {

            SmartAutomationAction smartAction = new SmartAutomationAction();
            SmartAutomationAction updateSmartAction = getAction(actionName.toString(), smartAction, automation);
            scenario.addAction(updateSmartAction, automation);
        }


        for (Condition cond : condList) {
            scenario.addCondition(cond, automation);
        }
        if (isOR)
            scenario.setOrOperator();

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);
        return automationRuleId;
    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

}
