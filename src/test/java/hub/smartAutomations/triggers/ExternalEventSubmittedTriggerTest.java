package hub.smartAutomations.triggers;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.common.objects.smarts.SmartAutomation;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.experimental.categories.Category;
import server.common.IServerResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Customer;
import server.v2_8.SubmitEvent;
import server.v2_8.SubmitEventResponse;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by Goni on 5/15/2017.
 * Test Rail: C4472
 */
public class ExternalEventSubmittedTriggerTest extends BaseAutomationTest {

    @Override
    public Triggers getTrigger() {
        return Triggers.ExternalEventSubmitted;
    }

    @Override
    public String getAutomationName() {
        return "ExternalEvent_" + timestamp;
    }

    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
        }
    };




    //Test trigger external event submitted with condition email not equal mock mail, then tag member
    @Test
    public void testWithConditionEmailNotEqTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal1, MOCK_EMAIL);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon" , "Enter",new ArrayList<>());

        checkAutomationRunAndTagMember();


    }

    @Test
    public void testConditionLastNameEqualsTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, oper, AUTO_LAST_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon" , "Enter",new ArrayList<>());

        checkAutomationRunAndTagMember();
    }

    @Test
    public void testConditionFirstNameIsOneOfActionTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(AUTO_USER_PREFIX + timestamp).append(',').append(AUTO_USER_PREFIX + timestamp + "_Updated");
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.IsOneOf);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon" , "Enter",new ArrayList<>());

        checkAutomationRunAndTagMember();

    }

    //Test trigger tag member with condition Gender is not one of action tag member
    @Test
    public void testConditionGenderIsNotOneOfActionTagMember() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Male").append(',').append("Other");
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Isnotoneof);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Gender, equal, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon" , "Enter",new ArrayList<>());

        checkAutomationRunAndTagMember();

    }


    @Test
    public void testConditionBirthdayIsBeforeTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsBefore);
        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, equal1, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, date);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon" , "Enter",new ArrayList<>());

        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionExpirationDateAfterTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calNextYear);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon" , "Enter",new ArrayList<>());

        checkAutomationRunAndTagMember();

    }

    //Test condition DaysTime Today am pm : action -tag member
    @Test
    public void testConditionDaysTimeActionTagMember() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar timeFrom = Calendar.getInstance();

        Calendar timeUntil = Calendar.getInstance();
        timeUntil.add(Calendar.HOUR, 5);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, ApplyIf.DAYSTIME, Calendar.getInstance(), timeFrom, timeUntil);

        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon" , "Enter",new ArrayList<>());

        checkAutomationRunAndTagMember();
    }

    //Test  condition Date and time are from now till a year later : action -tag member
    @Test
    public void testConditionTimeAndDateActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextYear);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon" , "Enter",new ArrayList<>());

        checkAutomationRunAndTagMember();

    }

    //Test condition ExternalEvent/Type equals "Beacon" : action -tag member
    @Test
    @Category(Hub1Regression.class)
    public void testConditionExternalEventTypeEqualsActionTagMember() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.EXTERNAL_EVENT, ExternalEventField.Type, oper, "Beacon");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Enter",new ArrayList<>());

        checkAutomationRunAndTagMember();
    }

    //Test condition ExternalEvent/SubType equals "Enter" : action -tag member
    @Test
    public void testConditionExternalEventSubTypeEqualsActionTagMember() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.EXTERNAL_EVENT, ExternalEventField.Subtype, oper, "Enter");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Enter",new ArrayList<>());

        checkAutomationRunAndTagMember();
    }

    //Test condition ExternalEvent/SubType not equals "Enter" : action -tag member
    @Test
    public void testConditionExternalEventSubTypeNotEqualsActionTagMember() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.EXTERNAL_EVENT, ExternalEventField.Subtype, oper, "Enter");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Exit",new ArrayList<>());

        checkAutomationRunAndTagMember();
    }

    //Test condition ExternalEvent/SubType contains "Enter" : action -tag member - not running
    @Test
    public void testConditionExternalEventSubTypeContainsActionTagMemberNegative() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.EXTERNAL_EVENT, ExternalEventField.Subtype, oper, "Enter");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Abort",new ArrayList<>());

        //check that the automation did not run
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }

    @Test
    public void  testWithConditionExternalEventTypeEqualsANDSubTypeEqualsTagMember() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);

        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.EXTERNAL_EVENT, ExternalEventField.Type, oper, "Beacon");
        StringOperator oper1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.EXTERNAL_EVENT, ExternalEventField.Subtype, oper1, "Enter");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond,cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Enter",new ArrayList<>());

        checkAutomationRunAndTagMember();
    }

    @Test
    public void  testMembershipPhonerEqAndExternalEventTypeEqTagAction() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);

        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.EXTERNAL_EVENT, ExternalEventField.Type, oper, "Beacon");
        StringOperator oper1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.PhoneNumber, oper1, timestamp.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond,cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Enter",new ArrayList<>());

        checkAutomationRunAndTagMember();
    }

    //Test condition ExternalEvent/Type is one of "Enter","Exit" : action -tag member not performed
    @Test
    public void testConditionExternalEventTypeIsOneOfActionTagMemberNegative() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.IsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Enter").append(',').append("Exit");
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.EXTERNAL_EVENT, ExternalEventField.Subtype, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Aborted",new ArrayList<>());

        //check that the automation did not run
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }

    //Test condition ActionTags ContainsOneOf "Tag1","Tag2" : action -tag member
    @Test
    public void testConditionActionTagsContainsOneOfActionTagMember() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Tag1").append(',').append("Tag2");
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Enter",Lists.newArrayList("Tag1"));

        checkAutomationRunAndTagMember();
    }

    //Test condition ActionTags ContainsOneOf "Tag1","Tag2" : action -tag member - not running
    @Test
    public void testConditionActionTagsContainsOneOfActionTagMemberNegative() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Tag1").append(',').append("Tag2");
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, oper,stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Enter",Lists.newArrayList("Tag3","Tag4"));

        //check that the automation did not run
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }

    //Test condition ActionTags ContainsOneOf "Tag1","Tag2" : action -tag member - not running
    @Test
    public void testConditionActionTagsContainsAllActionTagMember() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsAll);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Tag1").append(',').append("Tag2");
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, oper,stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Enter",Lists.newArrayList("Tag1","Tag2"));

        checkAutomationRunAndTagMember();
    }


    private void performAnActionSubmitEvent(List<NewMember> members, String type, String subType, ArrayList<String> tags) {

        SubmitEvent submitEvent = new SubmitEvent();
        submitEvent.setEventTime(timestamp.toString());
        submitEvent.setType(type);
        submitEvent.setSubType(subType);
        submitEvent.setTags(tags);

        for (int i = 0; i < members.size(); i++) {
            Customer customer = new Customer();
            customer.setPhoneNumber(members.get(i).getPhoneNumber());
            submitEvent.getCustomers().add(customer);
            log4j.info("send request submit event on member: " + customer.getPhoneNumber());
        }

        log4j.info("send request submit event with type: " + type + ", subType: " + subType);
        // Send the request
        IServerResponse response = submitEvent.SendRequestByApiKey(apiKey, SubmitEventResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

    }

    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

}
