package hub.smartAutomations.triggers;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.Unregister;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Goni on 5/10/2017.
 * Test Rail: C4474
 */
@Issue("CNP-8928")
@Deprecated
public class UnregistersTriggerTest extends BaseAutomationTest {

    @Override
    public Triggers getTrigger() {
        return Triggers.Unregisters;
    }

    @Override
    public String getAutomationName() {
        return "Unregisters_" + timestamp;
    }

    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
        }
    };


    @Test
    public void testConditionLastNameEqualsTagMember() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, oper, AUTO_LAST_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);
        checkAutomationRunAndTagMember();
    }

    @Test
    public void testConditionFirstNameIsOneOfActionTagMember() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(AUTO_USER_PREFIX + timestamp).append(',').append(AUTO_USER_PREFIX + timestamp + "_Updated");
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.IsOneOf);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionBirthdayIsBeforeTagMember() throws Exception {
        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, date);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsBefore);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, equal1, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionBirthdayEqualsTagMember() throws Exception {
        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, date);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        DateOperator oper = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.EqualsTo);

        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, oper, date);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);
        Thread.sleep(2000);

        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionExpirationDateAfterTagMember() throws Exception {
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calNextYear);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);
        Thread.sleep(2000);

        checkAutomationRunAndTagMember();

    }

    //Negative Test with condition DaysTime tomorrow: action -tag member not performed
    @Test
    public void testConditionDaysTimeTomorrowNegative() throws Exception {

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        Calendar nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DAY_OF_WEEK, 1);

        DaysTime day2 = hubAssetsCreator.generateSingleDay(nextDay);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, Lists.newArrayList(day2));
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);
        Thread.sleep(2000);

        //check that the automation did not run
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }


    //Test condition DaysTime Today am pm : action -tag member
    @Test
    public void testConditionDaysTimeActionTagMember() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar timeFrom = Calendar.getInstance();

        Calendar timeUntil = Calendar.getInstance();
        timeUntil.add(Calendar.HOUR, 5);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, ApplyIf.DAYSTIME, Calendar.getInstance(), timeFrom, timeUntil);

        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);

        checkAutomationRunAndTagMember();


    }

    //Test  condition Date and time are from now till a year later : action -tag member
    @Test
    public void testConditionTimeAndDateActionTagMember() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextYear);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);

        checkAutomationRunAndTagMember();


    }

    //Test condition Date and time are from now till 2 days later And email contains string '@como.com': action -tag member
    @Test
    public void testConditionTimeAndDateAndPhoneEqualsActionTagMember() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextDay = Calendar.getInstance();
        calNextDay.add(Calendar.DAY_OF_WEEK, +2);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextDay);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.PhoneNumber, equal2, timestamp.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);

        checkAutomationRunAndTagMember();

    }

    //    Negative: action not performed
    //    Condition : Expiration Date is After today
    //    Add Action: tag member
    //    send gift to the member with expiration date as a year before today and redeem
    @Test
    public void testConditionExpirationDateIsAfterTodayNegative() throws Exception {
        Calendar calPrevYear = Calendar.getInstance();
        calPrevYear.add(Calendar.YEAR, -1);

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calPrevYear);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);

        //check that the automation did not run
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }


    //Test trigger tag member with condition Gender is not one of action tag member
    @Test
    public void testConditionGenderIsNotOneOfActionTagMember() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Male").append(',').append("Other");
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Isnotoneof);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Gender, equal, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testWithConditionPhoneEqualsTagMember() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        //Create smart automatio
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.PhoneNumber,equal1,timestamp.toString());
        createSmartAutomationScenario( automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);
        checkAutomationRunAndTagMember();

    }


    @Test
    public void testWithConditionEmailStringContainsTagMember() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.Email,equal1, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);
        checkAutomationRunAndTagMember();

    }


    //condition: ActionTags contains All
    //Action: tag member
    @Test
    public void testConditionActionTagsContainsAllActionTagMember() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister with tags action "tag1,tag2,tag3"
        String updateAutomation = createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1","tag2","tag3"));

        //create smart automation that will check condition Tag actions contains all "tag1,tag2,tag3"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsAll);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag1,tag2,tag3");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);
        checkAutomationRunAndTagMember();
    }

    //Negative test
    // condition: ActionTags contains All
    //Action: tag member
    @Test
    public void testConditionActionTagsContainsAllNegative() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister with tags action "tag1,tag2"
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1","tag2"));

        //create smart automation  that will check condition Tag actions contains all "tag1,tag2,tag3"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsAll);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag1,tag2,tag3");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);
        Thread.sleep(2000);

        //check that the automation did not run
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    //condition: ActionTags contains One of
    //Action: tag member
    @Test
    public void testConditionActionTagsContainsOneOfActionTagMember() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister with tags action "tag1,tag2"
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1","tag2"));

        //create smart automation that will check condition Tag actions contains one of "tag4,tag2,tag3"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag4,tag2,tag3");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);
        checkAutomationRunAndTagMember();

    }

    //condition: ActionTags does not contains One of
    //Action: tag member
    @Test
    public void testConditionActionTagsDoesntContainsOneOfActionTagMember﻿() throws Exception{
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        //create smart automation that will check condition Tag actions contains one of "tag4,tag5,tag3"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.DoesntContainAny);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag4,tag5,tag3");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        // perform update to activate unregister action
        performUpdateMemberDetails(member);
        checkAutomationRunAndTagMember();
    }

    //Negative test
    //The member has the tag VIP - when trigger unregister is activate he will not get a tag(condition result false)
    @Test
    public void testConditionMembershipTagDoesntContainOneOfActionTagMemberNegative() throws Exception {
        //Create member with expiration date
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //create smart automation of Update member details that will run action unregister
        createSmartAutomationUpdateMemberWithActionUnrigester(Lists.newArrayList("tag1"));

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.DoesntContainAny);
        Condition cond = hubAssetsCreator.createConditionTextArea(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, MEMBER_TAG_VIP);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));


        MembersService.findAndGetMember(member.getPhoneNumber());

        //tag member with VIP
        performAnActionTagMember(member,MEMBER_TAG_VIP);

        Thread.sleep(2000);

        //check that the automation did not run
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }


    private String createSmartAutomationUpdateMemberWithActionUnrigester(ArrayList<String> actionTags) throws Exception {
        //Create smart automation with action tag member
        String automationName = "UpdateForUnregistersTrigger_" + timestamp;
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(Triggers.UpdatesMembershipDetails, automationName, settings);
        createSmartAutomationScenarioForUnregisterAction(automation, actionTags);
        return automationName;
    }

    private void createSmartAutomationScenarioForUnregisterAction(SmartAutomation automation, ArrayList<String> tags) throws Exception {
        log4j.info("creating scenario");
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal, UpdateMemberOperationValue.UpdateMembership.toString());
        scenario.addCondition(cond, automation);
        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getUnregisterAction(smartAction, automation, tags);
        scenario.addAction(updateSmartAction, automation);
        SmartAutomationService.createSmartAutomation(automation);

    }

    private SmartAutomationAction getUnregisterAction(SmartAutomationAction smartAction, SmartAutomation automation, ArrayList<String> actionTags) throws Exception {
        smartAction.setPerformTheAction(SmartAutomationActions.UnregisterMembership.toString());
        Unregister action = new Unregister(automation);
        action.setTags(actionTags, automation);
        smartAction.setUnregister(action);
        return smartAction;

    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

}
