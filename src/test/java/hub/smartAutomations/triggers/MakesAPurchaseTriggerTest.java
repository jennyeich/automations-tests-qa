package hub.smartAutomations.triggers;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Calendar;
import java.util.UUID;


/**
 * Created by goni on 2/7/2017.
 * Test Rail: C4461
 */
public class MakesAPurchaseTriggerTest extends BaseAutomationTest {


    @Override
    public Triggers getTrigger() {
        return Triggers.SubmitAPurchase;
    }

    @Override
    public String getAutomationName() {
        return "Makes a purchase_" + timestamp;
    }
    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }


    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
            createSmartGift();
        }
    };

    //Test trigger make purchase with condition email not equal mock mail, then tag member
    @Test
    public void testMakesAPurchaseWithConditionEmailNotEqTagMember() throws Exception {


        //step2
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal1, MOCK_EMAIL);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);


    }


    //Test trigger make purchase with condition email not equal mock mail AND firstName equal user name, then send asset
    @Test
    public void testMakesAPurchaseWithConditionEmailNotEqAndFNameEqUserActionSendAsset() throws Exception {

        //step3
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal1, MOCK_EMAIL);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal2, AUTO_USER_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        checkDataStoreUpdatedWithAsset(transactionId);
        //Verify the automation run and the gift was sent
        Assert.assertTrue("The member did not get the gift sent by the automation in the hub,but it appears in the datastore.", MembersService.isGiftExists(smartGift.getTitle()));
        log4j.info("The member got the gift sent by the automation");
    }


    //Test trigger make purchase with condition email not equal mock mail AND firstName equal user name, then send asset
    @Test
    public void testMakesAPurchaseWithConditionEmailNotEqActionAddPoints() throws Exception {


        //step1
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal1, MOCK_EMAIL);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.AddSetNumberofPoints));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        checkDataStoreUpdatedWithBudgetWeight(transactionId, "1000");
        //Verify that member used 200 points and was given +10 again
        Assert.assertTrue("The member did not get the 10 points to budget sent by the automation in the HUB,but appears in datastore.", MembersService.isPointsAddedToCredit(0));
        log4j.info("The member got the 10 points to budget -> the automation run");
    }


    //Test trigger make purchase with condition email not equal mock mail AND purchase total sum greater than 200, then add point to budger
    @Test
    public void testMakesAPurchaseWithConditionEmailNotEqANDPurchaseTotalGreater200ActionAddPoints() throws Exception {

        //step4
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal1 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.TotalSum, equal1, "200");
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond2 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal2, MOCK_EMAIL);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2), AND, Lists.newArrayList(SmartAutomationActions.AddSetNumberofPoints));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "30000", transactionId);

        checkDataStoreUpdatedWithBudgetWeight(transactionId, "1000");
        //Verify that member used 200 points and was given +10 again
        Assert.assertTrue("The member did not get the 10 points to budget sent by the automation in the HUB,but appears in datastore.", MembersService.isPointsAddedToCredit(0));
        log4j.info("The member got the 10 points to budget -> the automation run");

    }

    //Test trigger make purchase with condition Purchase/BranchID is one of 1,2,3,4 OR Purchase/BranchID equals to 1 then add point to budger& tag member
    @Test
    public void testMakesAPurchaseWithConditionBranchIdEqOROneofAddPointsTagMember() throws Exception {

        //step5
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.BranchID, equal1, BRANCH_ID);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.IsOneOf);
        Condition cond2 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.BranchID, equal2, "2,3,4");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2), OR, Lists.newArrayList(SmartAutomationActions.AddSetNumberofPoints, SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        checkDataStoreUpdatedWithBudgetWeight(transactionId, "1000");
        //Verify that member used 200 points and was given +10 again
        Assert.assertTrue("The member did not get the 10 points to budget sent by the automation in the HUB,but appears in datastore.", MembersService.isPointsAddedToCredit(0));
        log4j.info("The member got the 10 points to budget -> the automation run");

        checkAutomationRunAndTagMemberByTransaction(transactionId);


    }


    //Test trigger make purchase with condition:
    // Purchase/BranchID equals to 1 AND
    // PurchaseBusket/total price>200 AND
    //PurchaseBusket/quanity>1 AND
    //Membership/Email not equals 'mockemail@coco.com'
    //Action: tag member

    @Test
    public void testMakesAPurchaseWith4ConditionANDActionTagMember() throws Exception {


        //step6
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal1 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal1, "1");
        IntOperator equal2 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        Condition cond2 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal2, "200");
        StringOperator equal3 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond3 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.BranchID, equal3, "1");
        StringOperator equal4 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond4 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal4, MOCK_EMAIL);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2, cond3, cond4), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member), "300000", 2, transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Negative test : trigger make purchase with condition:
    // Purchase/BranchID equals to 1 AND
    // PurchaseBusket/total price<200 AND
    //PurchaseBusket/quanity>1 AND
    //Membership/Email not equals 'mockemail@coco.com'
    //Action: tag member - NOT PERFORMED (quantity = 1)

    @Test
    public void testMakesAPurchaseWith4ConditionANDActionTagMemberNegative() throws Exception {


        //step7
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal1 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal1, "1");
        IntOperator equal2 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        Condition cond2 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal2, "200");
        StringOperator equal3 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond3 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.BranchID, equal3, "1");
        StringOperator equal4 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond4 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal4, MOCK_EMAIL);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2, cond3, cond4), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member), "300000", 1, transactionId);

        checkDataStoreNegative(TAG_OPERATION, transactionId);
    }


    // Add New automation with 2 scenarios:
    // 1. Automation Name: Make a purchase
    //2. Triggered when member: Makes a purchase
    //3. Scenarios 1-> Condition: Membership/email equal "mockemail" (false condition)
    //                  Add Action: tag member scenario1
    // Scenarios 2-> No Condition
    //    Add Action: tag member scenarion2
    //
    //send "SubmitPurchase" request when user has the mail "username@como.com"
    @Test
    public void testMakesAPurchaseWith2Scenarios() throws Exception {


        //step10
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal2, MOCK_EMAIL);
        //create first scenario
        tag = "scenario1";
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //create second scenario
        tag = "scenario2";
        createSmartAutomationScenario(automation, Lists.newArrayList(), AND, Lists.newArrayList(SmartAutomationActions.TagMember));


        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "30000", transactionId);

        //Verify the automation run and the tag exists
        Assert.assertTrue("The member did not get the tag sent by the automation", MembersService.isTagExists("scenario2"));
        log4j.info("The member got the tag -> the automation run");

        checkDataStoreUpdatedWithTag(transactionId);

    }


    //Test trigger make purchase with Condition: Membership\Birthday is before 'Today'
    // then tag member
    @Test
    public void testMakesAPurchaseWithConditionDateActionTagMember() throws Exception {


        //step11
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsBefore);
        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, equal1, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, date);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();


        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "30000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);


    }

    //Test trigger make purchase with Condition: Membership\Birthday equals user birthday date
    // then tag member
    @Test
    public void testMakesAPurchaseWithConditionDateEqualsActionTagMember() throws Exception {


        //step12
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.EqualsTo);
        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, equal1, date);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, date);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();


        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "30000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);
    }


    //Test trigger make purchase with condition Date and time are from now till a year later, then tag member
    @Test
    public void testWithConditionDateAndTimeTagMember() throws Exception {


        //step 13
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextYear);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }


    //Test trigger make purchase with condition Date and time are from now till day later And email contains string '@como.com',Action:tag member
    @Test
    public void testWithConditionDateAndTimeDayLaterAndEmailContainsActionTagMember() throws Exception {


        //step 14
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextDay = Calendar.getInstance();
        calNextDay.add(Calendar.DAY_OF_WEEK, +1);
        Condition cond1 = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextDay);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond2 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }


    //Test trigger tag member with condition DaysTime Today am pm : action tag member
    @Test
    public void testConditionDaysTimeActionTagMember() throws Exception {

        //step 15
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar timeFrom = Calendar.getInstance();

        Calendar timeUntil = Calendar.getInstance();
        timeUntil.add(Calendar.HOUR, 5);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, ApplyIf.DAYSTIME, Calendar.getInstance(), timeFrom, timeUntil);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);


    }

    //with condition DaysTime 2 days (today & tomorrow): action tag member
    @Test
    public void testConditionDaysTime2DaysTagMember() throws Exception {
        //step 16
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DAY_OF_WEEK, 1);

        //create today
        DaysTime day1 = hubAssetsCreator.generateSingleDay(Calendar.getInstance());
        //create next day
        DaysTime day2 = hubAssetsCreator.generateSingleDay(nextDay);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, Lists.newArrayList(day1, day2));
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Test trigger make purchase with condition Purchase/BranchID is not one of 2,3,4 (by default branchId =1) tag member
    @Test
    public void testMakesAPurchaseWithConditionBranchIdNotOneofTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Isnotoneof);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.BranchID, oper, "2,3,4");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.AddSetNumberofPoints, SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Test trigger make purchase with condition Purchase/BranchID not equal to 2(by default branchId =1) tag member
    @Test
    public void testMakesAPurchaseWithConditionBranchIdNotEqualsTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.BranchID, oper, "2");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.AddSetNumberofPoints, SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }


    //Test trigger make purchase with condition Purchase/BranchID contains 1(by default branchId =1) tag member
    @Test
    public void testMakesAPurchaseWithConditionBranchIDStringContainsTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.BranchID, oper, BRANCH_ID);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Negative test
    // Test trigger make purchase with condition Purchase/BranchID contains 2(by default branchId =1) tag member
    @Test
    public void testMakesAPurchaseWithConditionBranchIDStringContainsNegative() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.BranchID, oper, "2");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        //check that automation did not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

    }

    //Test trigger make purchase with condition Purchase/NumberOfMembers equals to 2, tag member
    @Test
    public void testMakesAPurchaseWithConditionPurchaseNumOfMembersEqualsTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        IntOperator oper = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.NumOfMembers, oper, "2");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member with temp timestamp
        CharSequence timestampTemp = String.valueOf(System.currentTimeMillis());
        NewMember member1 = hubMemberCreator.createMember(timestampTemp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        key = hubMemberCreator.getMemberKeyToUserKey();

        NewMember member2 = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        Key key1 = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member1,member2), "1000", transactionId);

        checkDataStoreUpdatedWithTagForOneOfMembers(transactionId,key1);

    }


    //Negative test - submit purchase for only one member
    // Test trigger make purchase with condition Purchase/NumberOfMembers equals to 2, tag member
    @Test
    public void testMakesAPurchaseWithConditionPurchaseNumOfMembersEqualsNegative() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        IntOperator oper = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.NumOfMembers, oper, "2");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        //check that automation did not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

    }

    //Test trigger make purchase with condition Purchase/NumberOfMembers GreaterOrEqualsTo to 1, tag member
    @Test
    public void testMakesAPurchaseWithConditionPurchaseNumOfMembersGreaterOrEqualsTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        IntOperator oper = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.NumOfMembers, oper, "1");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member1 = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member1), "1000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Negative test - submit purchase for only one member
    // Test trigger make purchase with condition Purchase/NumberOfMembers GreaterOrEqualsTo to 2, tag member
    @Test
    public void testMakesAPurchaseWithConditionPurchaseNumOfMembersGreaterOrEqualsNegative() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        IntOperator oper = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.NumOfMembers, oper, "2");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        //check that automation did not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

    }

    //Test trigger make purchase with condition  purchase total sum greater than 200, then tag member
    @Test
    @Category(Hub1Regression.class)
    public void testWithConditionPurchaseTotalSumGreater200TagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal1 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.TotalSum, equal1, "200");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "30000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Negative test - purchase less than 200
    // Test trigger make purchase with condition  purchase total sum greater than 200, then tag member
    @Test
    public void testWithConditionPurchaseTotalSumGreater200Negative() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal1 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.TotalSum, equal1, "200");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        //check that automation did not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

    }

    //Test trigger make purchase with condition  purchase total sum less or equal 300, then tag member
    @Test
    public void testWithConditionPurchaseTotalSumLessOrEqual50TagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal1 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.LessOrEqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.TotalSum, equal1, "300");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);;

    }

    //Test trigger make purchase with condition  purchase total sum greater than 50, then tag member
    //(**add specific here for failed string compare bug 50 -> 100 is bigger -)
    @Test
    public void testWithConditionPurchaseTotalSumGreater50SpecialCheckTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal1 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.TotalSum, equal1, "50");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        //the automation should run here
        performAnActionSubmitPurchase(member, "10000", transactionId);
        checkDataStoreUpdatedWithTag(1, transactionId);

        transactionId = UUID.randomUUID().toString();
        //the automation should run here
        performAnActionSubmitPurchase(member, "11000", transactionId);
        checkDataStoreUpdatedWithTag(1, transactionId);

        transactionId = UUID.randomUUID().toString();
        //the automation should run here
        performAnActionSubmitPurchase(member, "20000", transactionId);
        checkDataStoreUpdatedWithTag(1, transactionId);

        transactionId = UUID.randomUUID().toString();
        //the automation should not run here - stay the same number for user action "Tag_operation"
        performAnActionSubmitPurchase(member, "600", transactionId);
        checkDataStoreNegative(TAG_OPERATION, transactionId);
    }

    //Test trigger make purchase with condition  purchase POS ID equal 1, then tag member
    @Test
    public void testWithConditionPurchasePOSIdEqualTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.POSID, equal, "1");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Test trigger make purchase with condition  purchase POS ID doesn't contain 2, then tag member
    @Test
    public void testWithConditionPurchasePOSIdDoesntContainTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.DoesntContain);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.POSID, equal, "2");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Test trigger make purchase with condition  purchase POS ID contains 1, then tag member
    @Test
    public void testWithConditionPurchasePOSIdContainsTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.POSID, equal, "1");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Negative test - POS ID = 1 -> condition result is false
    // Test trigger make purchase with condition  purchase POS ID contains 1, then tag member
    @Test
    public void testWithConditionPurchasePOSIdContainsNegative() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.POSID, equal, "2");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", transactionId);

        //automation should not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

    }

    //Test trigger make purchase with condition  purchase tags containsAll tag1,tag2 , then tag member
    @Test
    public void testWithConditionPurchaseTagsContainsAllTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsAll);
        String tagForItem = "tag1,tag2";
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.PURCHASE, Purchase.Tags, oper, tagForItem);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000",Lists.newArrayList("tag1","tag2"),transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Negative test - condition result false - automation should not run
    // Test trigger make purchase with condition  purchase tags containsAll tag1,tag2,tag3
    @Test
    public void testWithConditionPurchaseTagsContainsAllNegative() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsAll);
        String tagForItem = "tag1,tag2,tag3";
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.PURCHASE, Purchase.Tags, oper, tagForItem);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000",Lists.newArrayList("tag1","tag2"), transactionId);

        //automation should not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

    }

    //Test trigger make purchase with condition  purchase tags ContainsOneOf tag1,tag2 , then tag member
    @Test
    public void testWithConditionPurchaseTagsContainsOneOfTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        String tagForItem = "tag1,tag2";
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.PURCHASE, Purchase.Tags, oper, tagForItem);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000",Lists.newArrayList("tag3","tag2","tag4"), transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

}
