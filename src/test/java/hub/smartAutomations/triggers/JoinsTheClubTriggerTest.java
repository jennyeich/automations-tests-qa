package hub.smartAutomations.triggers;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.common.objects.smarts.SmartAutomation;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.smartAutomations.BaseAutomationTest;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Calendar;


/**
 * Created by Goni on 1/23/2017.
 * TestRail : C3931
 */
public class JoinsTheClubTriggerTest extends BaseAutomationTest {

    @Override
    public Triggers getTrigger() {
        return Triggers.JoinsTheClub;
    }

    @Override
    public String getAutomationName() {
        return "Joins the club_" + timestamp;
    }

    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
            createSmartGift();
        }
    };
    /**
     * C3741 - Tests the Joins the Club trigger and send asset action
     **/
    @Test
    public void testNoConditionSendAsset() throws Exception {

        //step 1
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        createSmartAutomationScenario(automation, Lists.newArrayList(),AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));//no condition
        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndSendAssetToMember();
    }

    @Test
    public void testNoConditionSendAssetAndSendPushNotification() throws Exception {

        //step 1
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        createSmartAutomationScenario(automation, Lists.newArrayList(),AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset,SmartAutomationActions.SendAPushNotification));//no condition
        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndSendAssetToMember();
        checkDataStoreUpdatedWithSendPushNotification(1);
    }


    //Joins the club with condition phone number equal user phone
    @Test
    public void testWithConditionEqualPhoneSendAsset() throws Exception {

       //step 4
        //Create smart automatio
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.PhoneNumber,equal1,timestamp.toString());
        createSmartAutomationScenario( automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndSendAssetToMember();

    }


    //Joins the club with condition first name equals user first name
    @Test
    public void testWithConditionEqualFNameSendAsset() throws Exception {

        //Create smart automation
        //step 17
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.FirstNAme,equal1, AUTO_USER_PREFIX +timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond),AND,Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndSendAssetToMember();

    }

    //Joins the club with condition first name not equals 'xxx' name and 2 actions:send an asset and tag member
    @Test
    public void testWithConditionNotEqualFNameSendAssetTagMember() throws Exception {


        //step9
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.FirstNAme,equal1, timestamp.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset,SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndSendAssetToMember();
        checkAutomationRunAndTagMember();

    }


    //Joins the club with 2 actions : send asset & add tag to member with one condition :email string not contain "@como.com" suffix
    //check that no user action was created in data store
    @Test
    public void testWithConditionEmailStringNotContainsSendAssetTagMemberNegative() throws Exception {

        //step10
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.DoesntContain);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.Email,equal1, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset,SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //check when action is received asset
       checkDataStoreNegativeCheckByRuleID(RECEIVEDASSET);

        //check when action is tag member
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    //Joins the club with condition last name equals user last name
    @Test
    public void testWithConditionEqualLNameSendAsset() throws Exception {

        //step7
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.LastName,equal1, AUTO_LAST_PREFIX +timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond),AND,Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndSendAssetToMember();

    }

    //Joins the club with condition first name is one of list of users
    @Test
    public void testWithConditionFNameOneOfSendAsset() throws Exception {

        //step5
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(AUTO_LAST_PREFIX +timestamp).append(',')
                .append(AUTO_LAST_PREFIX+ 1).append(',').append(AUTO_LAST_PREFIX+ 2)
                .append(',').append(AUTO_LAST_PREFIX+ 3);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.IsOneOf);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.LastName,equal1, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond),AND,Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndSendAssetToMember();

    }

    //Joins the club with condition email not equals mock email
    @Test
    public void testWithConditionEmailNotEqSendAsset() throws Exception {

        //step6

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.Email,equal1, MOCK_EMAIL);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond),false, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndSendAssetToMember();
    }

    //Joins the club with condition first name equals user first name OR email not equal to mock mail
    @Test
    @Category(Hub1Regression.class)
    public void testWithConditionEmailNotEqORFNameEqSendAsset() throws Exception {
        //step 3
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond1 = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.Email,equal1, MOCK_EMAIL);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.FirstNAme,equal2, AUTO_USER_PREFIX + timestamp);
        createSmartAutomationScenario( automation, Lists.newArrayList(cond1,cond2),OR, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndSendAssetToMember();

    }

    //Joins the club with condition first name equals user name - negative test - give wrong user name
    //check that no user action was created in data store
    @Test
    public void testWithConditionFNameEqSendAssetNegative() throws Exception {

        //step 8
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.FirstNAme,equal1, "stam");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //check that datastore  has not been updated
        checkDataStoreNegativeCheckByRuleID(RECEIVEDASSET);

    }

    //Joins the club with condition first name equals user first name AND email equal to user mail
    //BUG: CNP-5002
    @Test
    public void testWithConditionEmailEqAndFNameEqSendAsset() throws Exception {

        //step 2
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.Email,equal1, AUTO_USER_PREFIX + timestamp + timestamp +MAIL_SUFFIX);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.FirstNAme,equal2, AUTO_USER_PREFIX+ timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1,cond2), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndSendAssetToMember();

    }

    //Test trigger join the club with condition ExpirationDate is After: action -tag member
    @Test
    public void testConditionExpirationDateIsAfterActionTagMember() throws Exception {

        //step 11
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calNextYear);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndTagMember();
    }

    //Test trigger join the club with condition DaysTime Today am pm & First name equal user name : action -tag member
    @Test
    public void testConditionDaysTimeAndFNameEqualActionTagMember() throws Exception{

        //step 12
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        DaysTime day1 = hubAssetsCreator.generateSingleDay(Calendar.getInstance());

        Condition cond = hubAssetsCreator.createConditionDaysTime(automation,Lists.newArrayList(day1));
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.FirstNAme,equal2, AUTO_USER_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond,cond1),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndTagMember();

    }

    //Test trigger join the club with condition DaysTime 2 days (today & tomorrow): action -tag member
    @Test
    public void testConditionDaysTime2DaysTagMember() throws Exception{

        //step 13
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        Calendar nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DAY_OF_WEEK,1);

        //create today
        DaysTime day1 = hubAssetsCreator.generateSingleDay(Calendar.getInstance());
        //create next day
        DaysTime day2 = hubAssetsCreator.generateSingleDay(nextDay);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation,Lists.newArrayList(day1,day2));
        createSmartAutomationScenario(automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndTagMember();
    }

    //Test trigger join club with Condition: Membership\Birthday is before 'Today'
    // then tag member
    @Test
    public void testWithConditionBirthdayIsBeforeDateActionTagMember() throws Exception{

        //step 14
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsBefore);
        Calendar date  = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation,ApplyIf.MEMBERSHIP,Membership.Birthday,equal1,Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1),AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX,  date);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndTagMember();
    }

    //Test trigger join club with condition Date and time are from now till a year later : action -tag member
    @Test
    public void testConditionTimeAndDateActionTagMember() throws Exception{

        //step 15
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear  = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow,calNextYear);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
         hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndTagMember();

    }

    //Test trigger join club with condition Date and time are from now till a year later And email contains string '@como.com': action -tag member
    @Test
    public void testConditionTimeAndDateAndEmailContainsActionTagMember() throws Exception{

        //step 16
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear  = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow,calNextYear);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond1 = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.Email,equal2, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond,cond1),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndTagMember();

    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

}
