package hub.smartAutomations.triggers;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Calendar;

/**
 * Created by Goni on 5/4/2017.
 * TestRail : C4459
 */
public class RedeemsAnAssetTriggerTest extends BaseAutomationTest {

    @Override
    public Triggers getTrigger() {
        return Triggers.RedeemAnAsset;
    }

    @Override
    public String getAutomationName() {
        return "Redeems an asset_" + timestamp;
    }
    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
            if(smartGift == null) {
                //create assets
                try {
                    smartGift = hubAssetsCreator.createSmartGiftWithRedeemCode(timestamp);
                    smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");

                } catch (Exception e) {
                    log4j.error(e);
                    Assert.fail("error while creating asset");
                }
            }
        }
    };


    //Tests the send asset with  codition - first name equals ,action  tag member
    @Test
    public void testConditionFirstNameEqualsTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, oper, AUTO_USER_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndRedeem();

        checkAutomationRunAndTagMember();


    }

    //Test trigger tag member with condition Gender is not one of action tag member
    @Test
    public void testConditionGenderIsNotOneOfActionTagMember() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Male").append(',').append("Other");
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Isnotoneof);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Gender, equal, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndRedeem();

        checkAutomationRunAndTagMember();

    }

    // action : tag  member with one condition :email string not contain "@como.com" suffix
    //check that no user action was created in data store
    @Test
    public void testWithConditionEmailStringNotContainsTagMemberNegative() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.DoesntContain);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.Email,equal1, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndRedeem();

        //check when action is tag member
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @Test
    public void testConditionLastNameIsOneOfActionTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(AUTO_LAST_PREFIX + timestamp).append(',').append("XXXXX");
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.IsOneOf);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndRedeem();

        checkAutomationRunAndTagMember();

    }

    @Test
    public void testWithConditionEqualPhoneTagMember() throws Exception {

        //Create smart automatio
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.PhoneNumber,equal1,timestamp.toString());
        createSmartAutomationScenario( automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndRedeem();

        checkAutomationRunAndTagMember();

    }


    @Test
    @Category({Hub1Regression.class})
    public void testRedeemPunchCardConditionAssetEqualsTagAction() throws Exception {

        //Create smart automatio
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartPunchCard.getTitle());
        createSmartAutomationScenario( automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform smartPunchCard
        performAnActionPunchAPunchCard(smartPunchCard,5);

        String assetKey = getAssetKey(member.getPhoneNumber());

        //redeem asset
        performAnActionRedeemAsset(membershipKey,userKey,assetKey);

        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionBirthdayIsBeforeTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsBefore);
        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, equal1, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, date);
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();

        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendGift(smartGift);
        Assert.assertTrue("Member did not get the gift",MembersService.isGiftExists(smartGift.getTitle()));

        String assetKey = getAssetKey(member.getPhoneNumber());

        //redeem asset
        performAnActionRedeemAsset(membershipKey,userKey,assetKey);

        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionExpirationDateAfterTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calNextYear);
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendGift(smartGift);
        Assert.assertTrue("Member did not get the gift",MembersService.isGiftExists(smartGift.getTitle()));

        String assetKey = getAssetKey(member.getPhoneNumber());

        //redeem asset
        performAnActionRedeemAsset(membershipKey,userKey,assetKey);

        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionBirthdayIsBeforeAndMembershipTagVIPActionTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsBefore);
        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, equal1, Calendar.getInstance());
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        Condition cond2 = hubAssetsCreator.createConditionTextArea(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, MEMBER_TAG_VIP);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1,cond2), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, date);

        //tag member with VIP
        performAnActionTagMember(member,MEMBER_TAG_VIP);

        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();

        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();


        //Send gift to member
        performAnActionSendGift(smartGift);
        Assert.assertTrue("Member did not get the gift",MembersService.isGiftExists(smartGift.getTitle()));

        String assetKey = getAssetKey(member.getPhoneNumber());

        //redeem asset
        performAnActionRedeemAsset(membershipKey,userKey,assetKey);

        checkAutomationRunAndTagMember();

    }

    //Test with condition DaysTime 2 days (today & tomorrow): action -tag member
    @Test
    public void testConditionDaysTime2DaysTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        Calendar nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DAY_OF_WEEK, 1);

        //create today
        DaysTime day1 = hubAssetsCreator.generateSingleDay(Calendar.getInstance());
        //create next day
        DaysTime day2 = hubAssetsCreator.generateSingleDay(nextDay);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, Lists.newArrayList(day1, day2));
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndRedeem();

        checkAutomationRunAndTagMember();

    }


    //Test condition DaysTime Today am pm : action -tag member
    @Test
    public void testConditionDaysTimeActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar timeFrom = Calendar.getInstance();

        Calendar timeUntil = Calendar.getInstance();
        timeUntil.add(Calendar.HOUR, 5);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, ApplyIf.DAYSTIME, Calendar.getInstance(), timeFrom, timeUntil);

        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndRedeem();

        checkAutomationRunAndTagMember();


    }

    //Test  condition Date and time are from now till a year later : action -tag member
    @Test
    public void testConditionTimeAndDateActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextYear);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndRedeem();

        checkAutomationRunAndTagMember();


    }

    //    Negative: action not performed
    //    Condition : Expiration Date is After today
    //    Add Action: tag member
    //    send gift to the member with expiration date as a year before today and redeem
    @Test
    public void testConditionExpirationDateIsAfterTodayNegative() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        Calendar calPrevYear = Calendar.getInstance();
        calPrevYear.add(Calendar.YEAR, -1);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calPrevYear);
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendGift(smartGift);
        Assert.assertTrue("Member did not get the gift",MembersService.isGiftExists(smartGift.getTitle()));

        String assetKey = getAssetKey(member.getPhoneNumber());

        //redeem asset
        performAnActionRedeemAsset(membershipKey,userKey,assetKey);


        //check that the automation did not run
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    //Test condition Date and time are from now till 2 days later And email contains string '@como.com': action -tag member
    @Test
    public void testConditionTimeAndDateAndEmailContainsActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextDay = Calendar.getInstance();
        calNextDay.add(Calendar.DAY_OF_WEEK, +2);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextDay);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal2, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndRedeem();

        checkAutomationRunAndTagMember();


    }

    //************************ asset conditions *******************************************
    @Test
    public void testConditionAssetEqualsConditionTagAction() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartGift.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndRedeem();

        checkAutomationRunAndTagMember();


    }

    //asset condition NOT equals  - negative test
    @Test
    public void testConditionAssetNOTEqualsConditionTagActioNegative() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartGift.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndRedeem();

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);


    }

    @Test
    public void testConditionAssetEqualsConditionSendAssetAction() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartGift.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));

        createMemberAssignAssetAndRedeem();

        checkAutomationRunAndSendAssetToMember();
    }


    private void createMemberAssignAssetAndRedeem() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        MembersService.performSmartActionSendAnAsset(smartGift,timestamp);
        Assert.assertTrue("Member did not get the gift",MembersService.isGiftExists(smartGift.getTitle()));

        String assetKey = getAssetKey(member.getPhoneNumber());

        //redeem asset
        performAnActionRedeemAsset(membershipKey,userKey,assetKey);

    }






    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

}
