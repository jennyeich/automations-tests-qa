package hub.smartAutomations.triggers;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SendPopUpMessage;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SendPushNotification;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by Goni on 12/19/2017.
 */

public class MakesAPurchaseTriggerTest2 extends BaseAutomationTest {


    @Override
    public Triggers getTrigger() {
        return Triggers.SubmitAPurchase;
    }

    @Override
    public String getAutomationName() {
        return "Makes a purchase_" + timestamp;
    }
    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }


    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
            if(smartPunchCard == null) {
                //create assets
                try {
                    smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");

                } catch (Exception e) {
                    log4j.error(e);
                    Assert.fail("error while creating asset");
                }
            }
        }
    };
    //Negative test - condition result false - automation should not run
    // Test trigger make purchase with condition  purchase tags ContainsOneOf tag1,tag2,tag3
    @Test
    public void testWithConditionPurchaseTagsContainsOneOfNegative() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        String tagForItem = "tag1,tag2,tag3";
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.PURCHASE, Purchase.Tags, oper, tagForItem);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000",Lists.newArrayList("tag4","tag5"), transactionId);

        //automation should not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

    }

    //Test trigger make purchase with condition  purchase tags DoesntContainOneOf , then tag member
    @Test
    @Category(Hub1Regression.class)
    public void testWithConditionPurchaseTagsDoesntContainOneOfTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.DoesntContainAny);
        String tagForItem = "tag1,tag2";
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.PURCHASE, Purchase.Tags, oper, tagForItem);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000",Lists.newArrayList("tag3","tag4","tag5"), transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Negative test - condition result false - automation should not run
    // Test trigger make purchase with condition  purchase tags DoesntContainOneOf tag1,tag2,
    @Test
    public void testWithConditionPurchaseTagsDoesntContainOneOfNegative() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.DoesntContainAny);
        String tagForItem = "tag1,tag2";
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.PURCHASE, Purchase.Tags, oper, tagForItem);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000",Lists.newArrayList("tag1","tag5"), transactionId);

        //automation should not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

    }

    //Test trigger make purchase with condition  purchase Employee equals , then tag member
    //@Test
    @Deprecated
    public void testWithConditionPurchaseEmployeeEqualsTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);

        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.Employee, oper, EMPLOYEE+timestamp.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Test trigger make purchase with condition  purchase Employee Contains , then tag member
    //@Test
    @Deprecated
    public void testWithConditionPurchaseEmployeeContainsTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);

        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.Employee, oper, EMPLOYEE);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Negative test - condition result is false- automation should not run
    // Test trigger make purchase with condition  purchase Employee Contains , then tag member
    //@Test
    @Deprecated
    public void testWithConditionPurchaseEmployeeContainsNegative() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);

        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.Employee, oper, "ddd");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", transactionId);

        //automation should not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

    }

    //Test trigger make purchase with condition  purchase Employee IsOneOf , then tag member
    //@Test
    @Deprecated
    public void testWithConditionPurchaseEmployeeIsOneOfTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.IsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(EMPLOYEE).append(',').append(EMPLOYEE + timestamp.toString());
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.Employee, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Test trigger make purchase with condition shopping cart- eq=1
    @Test
    public void testMakesAPurchaseWithConditionShoppingCartTotalQuantityTagMember1() throws Exception {


        //step1
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "1");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "1000", transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Test trigger make purchase with condition shopping cart- eq=12
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityTagMember2() throws Exception {


        //step1
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "12");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),"1200",12,transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Test trigger make purchase with condition shopping cart- not eq=12
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityTagMember3() throws Exception {


        //step1
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.NotEqualto);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "10");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),"1200",12,transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Test trigger make purchase with condition shopping cart- > 6
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityTagMember4() throws Exception {


        //step1
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "6");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),"1200",10,transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Test trigger make purchase with condition shopping cart- >= 6 Negative test
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityNegativeTagMember5() throws Exception {


        //step1
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "10");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),"1200",6,transactionId);

        //automation should not run
        checkDataStoreNegative(TAG_OPERATION,transactionId);

    }
    //Test trigger make purchase with condition shopping cart- <= 6 Negative test
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityNegativeTagMember6() throws Exception {


        //step1
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.LessOrEqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "5");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),"1200",10,transactionId);

        //automation should not run
        checkDataStoreNegative(TAG_OPERATION,transactionId);

    }

    //Test trigger make purchase with condition shopping cart- < 6 Negative test
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityNegativeTagMember7() throws Exception {


        //step1
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.LessThan);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "5");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),"1200",5,transactionId);

        //automation should not run
        checkDataStoreNegative(TAG_OPERATION,transactionId);

    }


    //Test trigger make purchase with condition shopping cart- = 10 total price
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalPriceTagMember8() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "10");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),"1000",5,transactionId);

        checkDataStoreUpdatedWithTag(transactionId);


    }

    //Test trigger make purchase with condition shopping cart- >= total price 10
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalPriceTagMember9() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "10");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),"1000",5,transactionId);

        checkDataStoreUpdatedWithTag(transactionId);


    }

    //Test trigger make purchase with condition shopping cart- >= total price 10
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalPriceTagMember10() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "50");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),"10000",5,transactionId);

        checkDataStoreUpdatedWithTag(transactionId);


    }


    //Test trigger make purchase with condition shopping cart- != total price 50
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalPriceTagMember11() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.NotEqualto);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "50");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member),"10000",5,transactionId);

        checkDataStoreUpdatedWithTag(transactionId);


    }

    //Test trigger make purchase with condition shopping cart [total sum < 5] Negative test
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityNegativeTagMember12() throws Exception {


        //step1
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.LessThan);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "5");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member), "1200", 5,transactionId);

        //automation should not run
        checkDataStoreNegative(TAG_OPERATION,transactionId);


    }

    //Test trigger make purchase with condition shopping cart [total sum < 5] Negative test
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityNegativeTagMember13() throws Exception {


        //step1
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.LessThan);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "5");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member), "500", 5,transactionId);

        //automation should not run
        checkDataStoreNegative(TAG_OPERATION,transactionId);


    }

    //Test trigger make purchase with condition shopping cart [total sum > 5] Negative test
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityNegativeTagMember14() throws Exception {


        //step1
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "5");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member), "400", 5, transactionId);

        //automation should not run
        checkDataStoreNegative(TAG_OPERATION,transactionId);


    }

    //Test trigger make purchase with condition shopping cart [total sum > 5] Negative test
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityNegativeTagMember15() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this, settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalPrice, equal, "50");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(member), "900", 5,transactionId);

        //automation should not run
        checkDataStoreNegative(TAG_OPERATION,transactionId);

    }


    //Test trigger make purchase with condition shopping cart- with group with item code = 100
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityGroupCondWithItemCodeTagMember16() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.LessOrEqualsTo);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.Isoneof.toString(),"100",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "1",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("100","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"10000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item));
        checkDataStoreUpdatedWithTag(transactionId);

    }


    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityGroupCondWithItemCodeTagIsOneOfMember17() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.EqualsTo);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.Isoneof.toString(),"100,200,303",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "1",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("100","itemA","depA", "depAA", 500);
        ArrayList<Item> items= new ArrayList<>();
        items.add(item);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"500", Lists.newArrayList("DEP-1"),transactionId,items);
        checkDataStoreUpdatedWithTag(transactionId);

    }

    //repeated number and spaces in the condition

    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityGroupCondWithItemCodeTagIsOneOfMember18() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.Isoneof.toString(),"200,303, 200, 100, 200",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "1",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("100","itemA","depA", "depAA", 10000);
        Item item2 = createItem("300","itemA","depA", "depAA", 10000);
        ArrayList<Item> items= new ArrayList<>();
        items.add(item);
        items.add(item2);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,items);
        checkDataStoreUpdatedWithTag(transactionId);

    }



    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityGroupCondWithItemCodeTagIsOneOfRepeatSpacesMember19() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.Isoneof.toString(),"200,303, 200, 100, 200",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "1",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("200","itemA","depA", "depAA", 10000);
        Item item2 = createItem("300","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreUpdatedWithTag(transactionId);

    }

    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityGroupCondWithItemCodeTagIsNotOneOfMember20() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.IsNotOneOf.toString(),"200,303, 200, 100, 200",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "1",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreUpdatedWithTag(transactionId);

    }

    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityGroupCondWithItemCodeTagNotEqualsOfMember21() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "1",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreUpdatedWithTag(transactionId);

    }

    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityGroupCondWithItemCodeTagNotEqualsNegativeMember22() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "1",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("10000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("10000","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreNegative(TAG_OPERATION, transactionId);

    }

    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityGroupCondWithItemCodeTagNotEqualsOfMember23() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.NotEqualto);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"10000",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "5",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreUpdatedWithTag(transactionId);

    }

    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityGroupCondWithItemCodeTagNotEqualsOfMember24() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.NotEqualto);

        Group group = hubAssetsCreator.createGroup(automation,"new group"+ timestamp);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.NotEquals.toString(),"01005656500676700",group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "5",group);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("0100565650067670011","itemA","depA", "depAA", 10000);
        Item item2 = createItem("101005656500676700","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreUpdatedWithTag(transactionId);

    }



    /*****************************************/
    //CNP-6329
    /*****************************************/
    @Test
    public void test2ScenariosWithConditionShopingCartTotalQuantityGroupCondWithItemCodePunchApunchPushNotification6343() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        //create group
        Group group = hubAssetsCreator.createGroup(automation,"ramatGan");
        String itemCodesRamatGan = "10000,10001,10002,10003,10004,10005,10006,10025,10007,10008,10009,10010,10011,10012,10013,10014,10019,10020,10021,10024,10103,10104,12401,114";
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.Isoneof.toString(),itemCodesRamatGan,group);

        //create condition1
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        ConditionFieldOprValue cond1 = hubAssetsCreator.createConditionNoGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "1");
        cond1.setGroup(group);
        //condition2
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.BranchID, equal1, "ramatGan");

        //create first scenario
        SmartAutomationScenario scenario1 = new SmartAutomationScenario();
        automation.addScenario(scenario1);
        scenario1.addCondition(cond1,automation);
        scenario1.addCondition(cond2,automation);
        createScenarioWithPunchAction(automation,scenario1,"ramatGan",itemCodesRamatGan);
        addPushNotificationAction(automation,scenario1);


        //create second scenario
        Group group2 = hubAssetsCreator.createGroup(automation,"telAviv");
        String itemCodesTA ="10000,10001,10002,10003,10004,10005,10006,10007,10008,10009,10010,10011,10012,10013,10014,10019,10020,10021,10024,10103,10104,10108,114";
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.Isoneof.toString(),itemCodesTA,group2);

        IntOperator equal2 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        ConditionFieldOprValue cond21 = hubAssetsCreator.createConditionNoGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal2, "1");
        cond21.setGroup(group2);

        StringOperator equal21 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond22 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.BranchID, equal21, "telAviv");
        SmartAutomationScenario scenario2 = new SmartAutomationScenario();
        automation.addScenario(scenario2);
        scenario2.addCondition(cond21,automation);
        scenario2.addCondition(cond22,automation);
        createScenarioWithPunchAction(automation,scenario2,"telAviv",itemCodesTA);
        addPushNotificationAction(automation,scenario2);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSendPunchCard(smartPunchCard);


        //make the trigger
        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("10009","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"10000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item),"telAviv");
        checkDataStoreUpdatedWithPunch(transactionId);
        //Verify the automation run and the gift was sent
        Assert.assertTrue("The member did not get all punches in the HUB sent by the automation, but appeared in the data store", MembersService.isPunchCardPunched(smartPunchCard.getTitle(),"1"));
        log4j.info("The member got the punches sent by the automation");

        checkDataStoreUpdatedWithSendAction(transactionId,PUSH_OPERATION);

    }

    //CNP-6329
    @Test
    public void testScenariosWithConditionShopingCartTotalQuantityGroupCondWithItemCodeSendSMS() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        String itemCodesRamatGan = "10000,10001,10002,10003,10004,10005,10006,10025,10007,10008,10009,10010,10011,10012,10013,10014,10019,10020,10021,10024,10103,10104,12401,114";
        SmartAutomationScenario scenario1 = buildSpecialScenario(automation, itemCodesRamatGan);
        createScenarioWithPunchAction(automation,scenario1,"ramatGan",itemCodesRamatGan);
        addSendSMSAction(automation,scenario1);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSendPunchCard(smartPunchCard);


        //make the trigger
        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("10009","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"10000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item));
        checkDataStoreUpdatedWithPunch(transactionId);

        checkDataStoreUpdatedWithSendAction(transactionId,MEMBER_SMS_OPERATION);
    }

    //CNP-6329
    @Test
    public void testScenariosWithConditionShopingCartTotalQuantityGroupCondWithItemCodeSendPopupMessage() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        String itemCodesRamatGan = "10000,10001,10002,10003,10004,10005,10006,10025,10007,10008,10009,10010,10011,10012,10013,10014,10019,10020,10021,10024,10103,10104,12401,114";
        SmartAutomationScenario scenario1 = buildSpecialScenario(automation, itemCodesRamatGan);
        createScenarioWithPunchAction(automation,scenario1,"ramatGan",itemCodesRamatGan);
        addSendPopUpMessageAction(automation,scenario1);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSendPunchCard(smartPunchCard);


        //make the trigger
        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("10009","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"10000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item));
        checkDataStoreUpdatedWithPunch(transactionId);

        checkDataStoreUpdatedWithSendAction(transactionId,SEND_POPUP_OPERATION);

    }

    //Test trigger make purchase with condition  Condition: Purchase/BranchID equals to 1 AND Punches/TotalSum greater than 200
    // then punch a punch card
    @Test
    public void testMakesAPurchaseWith2ConditionsActionPunchACard() throws Exception {


        //step8
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal1 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.TotalSum, equal1, "20");
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.BranchID, equal2, "1");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2), AND, Lists.newArrayList(SmartAutomationActions.PunchThePunchCard));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSendPunchCard(smartPunchCard);

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "30000", transactionId);

        checkDataStoreUpdatedWithPunch(transactionId);

        //Verify that the punchcard was punched
        Assert.assertTrue("punch card should have punched 1 time in HUB ,but didn't.In data store it appears.", MembersService.isPunchCardPunched(smartPunchCard.getTitle()));
        log4j.info("punch card was punched 1 time -> the automation run");
    }

    //Test trigger make purchase with condition  Condition: Purchase/BranchID equals to 1 OR Punches/TotalSum less than 200(fail condition)
    // then punch a punch card
    @Test
    public void testMakesAPurchaseWith2ORConditionsActionPunchACard() throws Exception {


        //step9
        smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal1 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.LessOrEqualsTo);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.TotalSum, equal1, "20");
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.BranchID, equal2, "1");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1, cond2), OR, Lists.newArrayList(SmartAutomationActions.PunchThePunchCard));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSendPunchCard(smartPunchCard);

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "30000",transactionId);

        checkDataStoreUpdatedWithPunch(transactionId);

        //Verify that the punchcard was punched
        Assert.assertTrue("punch card should have punched 1 time in HUB ,but didn't.In data store it appears.", MembersService.isPunchCardPunched(smartPunchCard.getTitle()));
        log4j.info("punch card was punched 1 time -> the automation run");

    }

    //CNP-6179
    @Test
    public void testScenariosWithConditionShopingCartTotalQuantityGroupCondWithItemCodeExportEvent() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        String itemCodesRamatGan = "10000,10001,10002,10003,10004,10005,10006,10025,10007,10008,10009,10010,10011,10012,10013,10014,10019,10020,10021,10024,10103,10104,12401,114";
        SmartAutomationScenario scenario1 = buildSpecialScenario(automation, itemCodesRamatGan);
        createScenarioWithPunchAction(automation,scenario1,"ramatGan",itemCodesRamatGan);
        addExportEventAction(automation,scenario1);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSendPunchCard(smartPunchCard);


        //make the trigger
        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("10009","itemA","depA", "depAA", 10000);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"10000", Lists.newArrayList("DEP-1"),transactionId,Lists.newArrayList(item));
        checkDataStoreUpdatedWithPunch(transactionId);

        checkDataStoreUpdatedWithSendAction(transactionId,EVENT_EXPORTED_OPERATION);

    }

    //C7748 : Condition based on Item Tag on Smart Automation
    @Test
     public void testMakesAPurchaseWithConditionShopingCartTotalQuantityGroupCondWithItemTagContainsAll25() throws Exception {

            //Create smart automation
            SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
            IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
            tag = "PampersBrand";
            Group group = hubAssetsCreator.createGroup(automation,"PampersBrand"+ timestamp);

            hubAssetsCreator.createGroupConditionItemTag(automation,GroupField.ItemTag,GroupOperators.ContainsAll.toString(),tag,group);

            Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "1",group);

            //create first scenario
            SmartAutomationScenario scenario1 = createScenarioWithTagAction(automation);
            scenario1.addCondition(cond1,automation);

            IntOperator equal2 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
            tag = "HuggiesBrand";
            Group group2 = hubAssetsCreator.createGroup(automation,"HuggiesBrand"+ timestamp);
            hubAssetsCreator.createGroupConditionItemTag(automation,GroupField.ItemTag,GroupOperators.ContainsAll.toString(),"HuggiesBrand",group2);
            Condition cond2 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal2, "1",group2);

            //create second scenario
            SmartAutomationScenario scenario2 = createScenarioWithTagAction(automation);
            scenario2.addCondition(cond2,automation);
            SmartAutomationService.createSmartAutomation(automation);
            automationRuleId = getAutomationRuleID(settings);

            tag = "PampersBrand";
            //Create member
            NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
            //Get member userKey and turn into User_KEY
            key = hubMemberCreator.getMemberKeyToUserKey();

            //Go to postman and send Submit Purchase request with item in the Shopping Cart which tagged as "PampersBrand"
            String transactionId = UUID.randomUUID().toString();
            Item item = createItem("1000","itemA","depA", "depAA", 10000,Lists.newArrayList(tag));

            Item item2 = createItem("3000","itemA","depA", "depAA", 10000);

            performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList(),transactionId,Lists.newArrayList(item,item2));
            checkDataStoreUpdatedWithTagByTransaction(transactionId,tag);
            Assert.assertTrue(MembersService.isTagExists(tag));

    }

    //C7748 : Condition based on Item Tag on Smart Automation
    //Go to postman and send Submit Purchase request with item in the Shopping Cart which tagged as "HuggiesBrand"
    //only second tag is given
    @Test
    @Category(Hub1Regression.class)
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityGroupCondWithItemTagContainsAll26() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        tag = "PampersBrand";
        Group group = hubAssetsCreator.createGroup(automation,"PampersBrand"+ timestamp);

        hubAssetsCreator.createGroupConditionItemTag(automation,GroupField.ItemTag,GroupOperators.ContainsAll.toString(),tag,group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "1",group);

        //create first scenario
        SmartAutomationScenario scenario1 = createScenarioWithTagAction(automation);
        scenario1.addCondition(cond1,automation);

        IntOperator equal2 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        tag = "HuggiesBrand";
        Group group2 = hubAssetsCreator.createGroup(automation,"HuggiesBrand"+ timestamp);
        hubAssetsCreator.createGroupConditionItemTag(automation,GroupField.ItemTag,GroupOperators.ContainsAll.toString(),"HuggiesBrand",group2);
        Condition cond2 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal2, "1",group2);

        //create second scenario
        SmartAutomationScenario scenario2 = createScenarioWithTagAction(automation);
        scenario2.addCondition(cond2,automation);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        tag = "HuggiesBrand";
        //Go to postman and send Submit Purchase request with item in the Shopping Cart which tagged as "HuggiesBrand"
        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000);

        Item item2 = createItem("3000","itemA","depA", "depAA", 10000,Lists.newArrayList(tag));

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList(),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreUpdatedWithTagByTransaction(transactionId,tag);
        Assert.assertTrue(MembersService.isTagExists(tag));


    }

    //C7748 : Condition based on Item Tag on Smart Automation
    //Go to postman and send Submit Purchase request with one item in the Shopping Cart which tagged as "PampersBrand" and additional item which tagged as "HuggiesBrand"
    //only first tag is given
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityGroupCondWithItemTagContainsAll27() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        tag = "PampersBrand";
        Group group = hubAssetsCreator.createGroup(automation,"PampersBrand"+ timestamp);

        hubAssetsCreator.createGroupConditionItemTag(automation,GroupField.ItemTag,GroupOperators.ContainsAll.toString(),tag,group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "1",group);

        //create first scenario
        SmartAutomationScenario scenario1 = createScenarioWithTagAction(automation);
        scenario1.addCondition(cond1,automation);

        IntOperator equal2 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        tag = "HuggiesBrand";
        Group group2 = hubAssetsCreator.createGroup(automation,"HuggiesBrand"+ timestamp);
        hubAssetsCreator.createGroupConditionItemTag(automation,GroupField.ItemTag,GroupOperators.ContainsAll.toString(),"HuggiesBrand",group2);
        Condition cond2 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal2, "1",group2);

        //create second scenario
        SmartAutomationScenario scenario2 = createScenarioWithTagAction(automation);
        scenario2.addCondition(cond2,automation);
        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        tag = "PampersBrand";
        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000,Lists.newArrayList(tag));

        Item item2 = createItem("3000","itemA","depA", "depAA", 10000,Lists.newArrayList("HuggiesBrand"));

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList(),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreUpdatedWithTagByTransaction(transactionId,tag);
        Assert.assertTrue(MembersService.isTagExists(tag));

    }

    //C8680 : Condition based on Item Tag on Smart Automation
    //Go to postman and send Submit Purchase request with one item in the Shopping Cart which tagged as "PampersBrand"
    @Test
    public void testMakesAPurchaseWithConditionShopingCartTotalQuantityGroupCondWithItemTagContainsOneOf28() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        tag = "PampersBrand";
        String tagForItem = tag + ",tag1,tag2,tag3";
        Group group = hubAssetsCreator.createGroup(automation,"PampersBrand"+ timestamp);

        hubAssetsCreator.createGroupConditionItemTag(automation,GroupField.ItemTag,GroupOperators.ContainsOneOf.toString(),tagForItem,group);

        Condition cond1 = hubAssetsCreator.createConditionWithGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "1",group);

        //create first scenario
        SmartAutomationScenario scenario1 = createScenarioWithTagAction(automation);
        scenario1.addCondition(cond1,automation);

        SmartAutomationService.createSmartAutomation(automation);
        automationRuleId = getAutomationRuleID(settings);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("1000","itemA","depA", "depAA", 10000,Lists.newArrayList(tag));

        Item item2 = createItem("3000","itemA","depA", "depAA", 10000,Lists.newArrayList("HuggiesBrand"));

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member),"20000", Lists.newArrayList(),transactionId,Lists.newArrayList(item,item2));
        checkDataStoreUpdatedWithTagByTransaction(transactionId,tag);
        Assert.assertTrue(MembersService.isTagExists(tag));

    }

    //special scenario according to production bug CNP-6329
    private SmartAutomationScenario buildSpecialScenario(SmartAutomation automation, String itemCodesRamatGan) throws Exception {
        Group group = hubAssetsCreator.createGroup(automation,"ramatGan");
        hubAssetsCreator.createGroupCondition(automation, GroupField.ItemCode, GroupOperators.Isoneof.toString(),itemCodesRamatGan,group);

        //create condition1
        IntOperator equal = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        ConditionFieldOprValue cond1 = hubAssetsCreator.createConditionNoGroup(automation, ApplyIf.SHOPPING_CART, ShoppingCart.TotalQuantity, equal, "1");
        cond1.setGroup(group);
        //condition2
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond2 = hubAssetsCreator.createCondition(automation, ApplyIf.PURCHASE, Purchase.BranchID, equal1,BRANCH_ID);

        //create scenario
        SmartAutomationScenario scenario1 = new SmartAutomationScenario();
        automation.addScenario(scenario1);
        scenario1.addCondition(cond1,automation);
        scenario1.addCondition(cond2,automation);
        return scenario1;
    }

    private void addExportEventAction(SmartAutomation automation,SmartAutomationScenario scenario) throws Exception{

        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getAction(SmartAutomationActions.ExportEvent.toString(), smartAction, automation);
        ExportEvent exportEvent = updateSmartAction.getExportEvent();
        updateSmartAction.setExportEvent(exportEvent);
        scenario.addAction(updateSmartAction, automation);
    }


    private void addSendSMSAction(SmartAutomation automation,SmartAutomationScenario scenario) throws Exception{

        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getAction(SmartAutomationActions.SendTextSMS.toString(), smartAction, automation);
        SendSMS sendSMS = updateSmartAction.getSendSMS();
        updateSmartAction.setSendSMS(sendSMS);
        scenario.addAction(updateSmartAction, automation);
    }

    private void addSendPopUpMessageAction(SmartAutomation automation,SmartAutomationScenario scenario) throws Exception{

        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getAction(SmartAutomationActions.SendPopUPMessage.toString(), smartAction, automation);
        SendPopUpMessage sendPopUpMessage = updateSmartAction.getSendPopUpMessage();
        updateSmartAction.setSendPopUpMessage(sendPopUpMessage);
        scenario.addAction(updateSmartAction, automation);
    }

    private void addPushNotificationAction(SmartAutomation automation,SmartAutomationScenario scenario) throws Exception{

        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getAction(SmartAutomationActions.SendAPushNotification.toString(), smartAction, automation);
        SendPushNotification pushNotification = updateSmartAction.getPushNotification();
        updateSmartAction.setPushNotification(pushNotification);
        scenario.addAction(updateSmartAction, automation);
    }

    private SmartAutomationScenario createScenarioWithPunchAction(SmartAutomation automation,SmartAutomationScenario scenario,String groupName,String itemCode) throws Exception {

        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getAction(SmartAutomationActions.PunchThePunchCard.toString(), smartAction, automation);
        PunchAPunchCard punchAPunchCard = updateSmartAction.getPunchAPunchCard();

        Group group = new Group(automation);
        group.setGroupName(groupName);
        hubAssetsCreator.createGroupCondition(automation,GroupField.ItemCode,GroupOperators.Isoneof.toString(),itemCode,group);

        Occurrences occurrences = new Occurrences(automation);
        occurrences.setOccurrences_type(OccurrencesType.custom.toString());
        occurrences.setOccurrences_times("1");
        CustomOccurrences customOccurrences = new CustomOccurrences(automation);
        customOccurrences.setCustomOccurrencesForEach("1");
        customOccurrences.setCustomOccurrencesForEachTotal(ForEachTotalType.TotalQuantity.toString());

        occurrences.setCustomOccurrences(customOccurrences);
        punchAPunchCard.setOccurrences(occurrences);

        customOccurrences.setGroup(group);
        punchAPunchCard.setOccurrences(occurrences);
        updateSmartAction.setPunchAPunchCard(punchAPunchCard);
        scenario.addAction(updateSmartAction, automation);
        return scenario;
    }



    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }
}

