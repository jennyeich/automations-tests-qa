package hub.smartAutomations.triggers;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import ru.yandex.qatools.allure.annotations.Issue;
import server.common.IServerResponse;
import server.v2_8.RedeemAsset;
import server.v2_8.RedeemAssetResponse;
import server.v2_8.base.IServerErrorResponse;

import java.util.Calendar;

/**
 * Created by Goni on 5/7/2017.
 * Test Rail: C4460
 */
@Issue("CNP-8928")
@Deprecated
public class AttemptToRedeemAssetTriggerTest extends BaseAutomationTest {

    @Override
    public Triggers getTrigger() {
        return Triggers.AttemptRedeemAnAsset;
    }

    @Override
    public String getAutomationName() {
        return "Attempt to Redeem_" + timestamp;
    }

    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
            if(smartGift == null) {
                //create assets
                try {
                    smartGift = hubAssetsCreator.createSmartGift(timestamp);
                    smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");
                } catch (Exception e) {
                    log4j.error(e);
                    Assert.fail("error while creating asset");
                }
            }
        }
    };

    //Tests the send asset with  codition - first name equals ,action  tag member
    @Test
    public void testConditionFirstNameEqualsTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, oper, AUTO_USER_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndAttempToRedeem();

        checkAutomationRunAndTagMember();

    }

    //Test trigger tag member with condition Gender is not one of action tag member
    @Test
    public void testConditionGenderIsNotOneOfActionTagMember() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Male").append(',').append("Other");
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Isnotoneof);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Gender, equal, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndAttempToRedeem();

        checkAutomationRunAndTagMember();

    }

    // action : tag  member with one condition :email string not contain "@como.com" suffix
    //check that no user action was created in data store
    @Test
    public void testWithConditionEmailStringNotContainsTagMemberNegative() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.DoesntContain);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.Email,equal1, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndAttempToRedeem();

        //check when action is tag member
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @Test
    public void testConditionLastNameIsOneOfActionTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(AUTO_LAST_PREFIX + timestamp).append(',').append("XXXXX");
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.IsOneOf);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndAttempToRedeem();

        checkAutomationRunAndTagMember();

    }

    @Test
    public void testWithConditionEqualPhoneTagMember() throws Exception {

        //Create smart automatio
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.PhoneNumber,equal1,timestamp.toString());
        createSmartAutomationScenario( automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndAttempToRedeem();

        checkAutomationRunAndTagMember();

    }

    @Test
    public void testAttemptToRedeemPunchCardConditionPhoneNotEqualsTagAction() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.PhoneNumber,equal1,"454545458");
        createSmartAutomationScenario( automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String membershipKey = MembersService.getMembershipKey();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard,5);

        String assetKey = getAssetKey(member.getPhoneNumber());
        log4j.info("assetKey : " + assetKey);
        //redeem asset
        performAnActionAttemptRedeemAsset(membershipKey,assetKey);

        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionBirthdayIsBeforeTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsBefore);
        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, equal1, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, date);
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();

        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendGift(smartGift);
        Assert.assertTrue("Member did not get the gift",MembersService.isGiftExists(smartGift.getTitle()));

        String assetKey = getAssetKey(member.getPhoneNumber());

        //attempt to redeem asset
        performAnActionAttemptRedeemAsset(membershipKey,assetKey);

        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionExpirationDateAfterTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calNextYear);
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendGift(smartGift);
        Assert.assertTrue("Member did not get the gift",MembersService.isGiftExists(smartGift.getTitle()));

        String assetKey = getAssetKey(member.getPhoneNumber());

        //attempt to redeem asset
        performAnActionAttemptRedeemAsset(membershipKey,assetKey);

        checkAutomationRunAndTagMember();

    }

    //Test with condition DaysTime 2 days (today & tomorrow): action -tag member
    @Test
    public void testConditionDaysTime2DaysTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        Calendar nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DAY_OF_WEEK, 1);

        //create today
        DaysTime day1 = hubAssetsCreator.generateSingleDay(Calendar.getInstance());
        //create next day
        DaysTime day2 = hubAssetsCreator.generateSingleDay(nextDay);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, Lists.newArrayList(day1, day2));
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndAttempToRedeem();

        checkAutomationRunAndTagMember();;

    }


    //Test condition DaysTime Today am pm : action -tag member
    @Test
    public void testConditionDaysTimeActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar timeFrom = Calendar.getInstance();

        Calendar timeUntil = Calendar.getInstance();
        timeUntil.add(Calendar.HOUR, 5);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, ApplyIf.DAYSTIME, Calendar.getInstance(), timeFrom, timeUntil);

        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndAttempToRedeem();

        checkAutomationRunAndTagMember();


    }

    //Test  condition Date and time are from now till a year later : action -tag member
    @Test
    public void testConditionTimeAndDateActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextYear);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndAttempToRedeem();

        checkAutomationRunAndTagMember();

    }

    //    Negative: action not performed
    //    Condition : Expiration Date is After today
    //    Add Action: tag member
    //    send gift to the member with expiration date as a year before today and redeem
    @Test
    public void testConditionExpirationDateIsAfterTodayNegative() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        Calendar calPrevYear = Calendar.getInstance();
        calPrevYear.add(Calendar.YEAR, -1);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calPrevYear);
        String membershipKey = MembersService.getMembershipKey();

        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendGift(smartGift);
        Assert.assertTrue("Member did not get the gift",MembersService.isGiftExists(smartGift.getTitle()));

        String assetKey = getAssetKey(member.getPhoneNumber());

        //attempt to redeem asset
        performAnActionAttemptRedeemAsset(membershipKey,assetKey);


        //check that the automation did not run
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    //Test condition Date and time are from now till 2 days later And email contains string '@como.com': action -tag member
    @Test
    public void testConditionTimeAndDateAndEmailContainsActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextDay = Calendar.getInstance();
        calNextDay.add(Calendar.DAY_OF_WEEK, +2);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextDay);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal2, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndAttempToRedeem();

        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionAssetEqualsAndMemberTagDoesntContainOneOfActionTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartGift.getTitle());

        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.DoesntContainAny);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("STAM").append(',').append(MEMBER_TAG_ANYUSER);
        Condition cond2 = hubAssetsCreator.createConditionTextArea(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, stringBuffer.toString());

        createSmartAutomationScenario(automation, Lists.newArrayList(cond1,cond2), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //tag member with VIP
        performAnActionTagMember(member,MEMBER_TAG_VIP);

        String membershipKey = MembersService.getMembershipKey();

        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();


        //Send gift to member
        performAnActionSendGift(smartGift);
        Assert.assertTrue("Member did not get the gift",MembersService.isGiftExists(smartGift.getTitle()));

        String assetKey = getAssetKey(member.getPhoneNumber());

        //redeem asset
        performAnActionAttemptRedeemAsset(membershipKey,assetKey);

        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionAssetEqualsAndMemberTagContainsOneOfActionTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond1 = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartGift.getTitle());

        TagOperator oper = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(MEMBER_TAG_VIP).append(',').append(MEMBER_TAG_ANYUSER).append(',').append("GOLD") ;
        Condition cond2 = hubAssetsCreator.createConditionTextArea(automation, ApplyIf.MEMBERSHIP, Membership.Tags, oper, stringBuffer.toString());

        createSmartAutomationScenario(automation, Lists.newArrayList(cond1,cond2), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //tag member with ANYUSER
        performAnActionTagMember(member,MEMBER_TAG_ANYUSER);

        String membershipKey = MembersService.getMembershipKey();

        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();


        //Send gift to member
        performAnActionSendGift(smartGift);
        Assert.assertTrue("Member did not get the gift",MembersService.isGiftExists(smartGift.getTitle()));

        String assetKey = getAssetKey(member.getPhoneNumber());

        //redeem asset
        performAnActionAttemptRedeemAsset(membershipKey,assetKey);

        checkAutomationRunAndTagMember();

    }

    //************************ asset conditions *******************************************
    @Test
    public void testConditionAssetEqualsConditionTagAction() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartGift.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndAttempToRedeem();

        checkAutomationRunAndTagMember();


    }

    //asset condition NOT equals  - negative test
    @Test
    public void testConditionAssetNOTEqualsConditionTagActionNegative() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.ASSET, Asset.Asset, equal1, smartGift.getTitle());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        createMemberAssignAssetAndAttempToRedeem();

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);


    }


    private void createMemberAssignAssetAndAttempToRedeem() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String membershipKey = MembersService.getMembershipKey();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        performAnActionSendGift(smartGift);
        Assert.assertTrue("Member did not get the gift",MembersService.isGiftExists(smartGift.getTitle()));


        String assetKey = getAssetKey(member.getPhoneNumber());
        log4j.info("assetKey : " + assetKey);
        //redeem asset
        performAnActionAttemptRedeemAsset(membershipKey,assetKey);
    }


    private void performAnActionAttemptRedeemAsset(String membershipKey,String assetKey) {
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(assetKey);
        redeemAsset.setMembershipKey(membershipKey);
        redeemAsset.setToken(serverToken);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);


        //Make sure the response is not Error response
        if (response instanceof IServerErrorResponse) {
            Assert.fail(((IServerErrorResponse) response).getErrorMessage());
        }
    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }
}
