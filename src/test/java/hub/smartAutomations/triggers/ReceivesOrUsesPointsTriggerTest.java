package hub.smartAutomations.triggers;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AddPoints;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AmountType;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.utils.ServerHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

/**
 * Created by goni on 2/5/2017.
 * TestRail : C3941
 */
public class ReceivesOrUsesPointsTriggerTest extends BaseAutomationTest {


    private static boolean firstTest = true;

    @Override
    public Triggers getTrigger() {
        return Triggers.ReceivesOrUsesPoints;
    }

    @Override
    public String getAutomationName() {
        return "ReceivesUses Points_" + timestamp;
    }

    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }


    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
            if(firstTest ) {
                serverHelper.configureBackendForPointsPayment();
                firstTest = false;
            }
            createSmartGift();
        }
    };



    //Tests the add points or uses trigger with no codition - add 200 points to budget then tag member
    @Test
    public void testReceivePointsNoConditionReceivePointsActionTagMember() throws Exception {


        //step1
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        createSmartAutomationScenario(automation, Lists.newArrayList(), AND, Lists.newArrayList(SmartAutomationActions.TagMember));//no condition
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "200";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);

        checkAutomationRunAndTagMember();
    }

    //Tests the add points or uses trigger with  codition - email not equals mock email ,action  tag member when user gets 200 points
    @Test
    public void testReceivePointsWithConditionEmailNotEqActionTagMember() throws Exception {


        //step2
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal1, MOCK_EMAIL);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenPoints = "200";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);

        checkAutomationRunAndTagMember();


    }

    //Tests the use points trigger use 100 points(using server API) with condition (points quantity = -20)for stopping the loop
    // and action  tag member - should not receive
    @Test
    public void testUsesPointsWithConditionQuantityEqActionNegative() throws Exception {

        //step3
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //add points so user can use them
        MembersService.performSmartActionAddPoints("1000",timestamp);

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal1 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.POINTS, Points.Quantity, equal1, "-20");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));


        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "1000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);

        //wait for data store to update
        TimeUnit.SECONDS.sleep(5);
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    //Tests the use points trigger use 200 points(using server API) with condition (points quantity = -20) and action  send asset to member
    @Test
    public void testUsesPointsWithConditionQuantityEqActionSendAsset() throws Exception {


        //step4
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //add points so user can use them
        MembersService.performSmartActionAddPoints("1000",timestamp);

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal1 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.POINTS, Points.Quantity, equal1, "-20");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));


        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "2000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);

        checkAutomationRunAndSendAssetToMember();

    }

    //Tests the use points trigger use 200 points(using server API)
    // with condition (email not equal to mockemail@coco.com) and action  tag member
    @Test
    public void testUsesPointsWithConditionEmailNotEqTagMember() throws Exception {


        //step5
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //add points so user can use them
        MembersService.performSmartActionAddPoints("100",timestamp);

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal1, MOCK_EMAIL);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));


        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "200";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);

        checkAutomationRunAndTagMember();

    }

    //Negative Test:
    // tests the use points trigger use 200 points(using server API)
    // with condition (email not equal to mockemail@coco.com) and action  send asset to member - automation not running
    @Test
    public void testUsesPointsWithConditionFNameNotEqSendAssetNegative() throws Exception {


        //step6
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //add points so user can use them
        MembersService.performSmartActionAddPoints("100",timestamp);

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal1, AUTO_USER_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));


        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "200";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);

        //wait for data store to update
        checkDataStoreNegativeCheckByRuleID(RECEIVEDASSET);

    }

    //Tests the add points or uses trigger with  codition - birthday equals user birthday ,action  tag member when user gets 200 points
    @Test
    public void testReceivePointsWithConditionBirthdayEqActionTagMember() throws Exception {


        //step7
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.EqualsTo);
        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, equal1, date);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member with the exact birthday date
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, date);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenPoints = "200";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);

        checkAutomationRunAndTagMember();
    }

    //Recieve points test:
    // codition - if expiration date is After Today ,action  tag member when user gets 200 points
    @Test
    public void testReceivePointsWithConditionExpirationDateIsAfter() throws Exception {


        //step8
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, equal1, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member with the expiration date next year
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, nextYear);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenPoints = "200";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);

        checkAutomationRunAndTagMember();

    }

    //Test Receive Points with condition Date and time are from now till 5 days later : action -tag member
    @Test
    public void testReceivePointsConditionDateAndTimeActionTagMember() throws Exception {


        //step 9
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.DAY_OF_WEEK, 5);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextYear);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "200";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);

        checkAutomationRunAndTagMember();
    }

    //Test Receive Points with condition Date and time are from now till a year later And email contains string '@como.com': action -tag member
    @Test
    public void testReceivePointsConditionDateAndTimeAndEmailContainsActionTagMember() throws Exception {


        //step 10
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextYear);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal2, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "200";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);

        checkAutomationRunAndTagMember();

    }

    //Test Receive Points with condition DaysTime 2 days (today & 2 days after): action -tag member
    @Test
    public void testReceivePointsConditionDaysTime2DaysTagMember() throws Exception {


        //step 11
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DAY_OF_WEEK, 2);

        //create today
        DaysTime day1 = hubAssetsCreator.generateSingleDay(Calendar.getInstance());
        //create next day
        DaysTime day2 = hubAssetsCreator.generateSingleDay(nextDay);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, Lists.newArrayList(day1, day2));
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "200";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);

        checkAutomationRunAndTagMember();

    }

    //Test Receive Points with condition DaysTime Today am pm & expiration date equals next year: action -tag member
    @Test
    public void testReceivePointsConditionDaysTimeAndExpirationDateEqualActionTagMember() throws Exception {


        //step 12
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar timeFrom = Calendar.getInstance();

        Calendar timeUntil = Calendar.getInstance();
        timeUntil.add(Calendar.HOUR, 5);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, ApplyIf.DAYSTIME, Calendar.getInstance(), timeFrom, timeUntil);

        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);
        //Create member
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calNextYear);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "200";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);

        checkAutomationRunAndTagMember();
    }


    //Negative test:  use points trigger use 30 points(using server API)
    // with condition (points quantity great than -20 (since it is negative we give opposite operator) and action tag member
    // -> condition result is false - no action performed
    @Test
    public void testUses30PointsConditionQuantityUsedLessThan20Negative() throws Exception {


        //step 14
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //add points so user can use them
        MembersService.performSmartActionAddPoints("1000",timestamp);

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal1 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.Greaterthan);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.POINTS, Points.Quantity, equal1, "-20");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));


        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "3000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);
        //wait for data store to update
        TimeUnit.SECONDS.sleep(5);
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }


    //condition: ActionTags contains All
    //Action: tag member
    @Test
    public void testReceivePointsWithConditionActionTagsContainsAllActionTagMember() throws Exception {


        //step 15
        //create smart automation of Update member details that will run action add points with tags action "tag1,tag2,tag3"
        createSmartAutomationUpdateMemberWithActionAddPoints(Lists.newArrayList("tag1","tag2","tag3"));

        //create smart automation f that will check condition Tag actions contains all "tag1,tag2,tag3"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsAll);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag1,tag2,tag3");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();


        // perform pay with budget to use members points
        performUpdateMemberDetails(member);
        checkAutomationRunAndTagMember();
    }

    //Negative test
    // condition: ActionTags contains All
    //Action: tag member
    @Test
    public void testReceivePointsWithConditionActionTagsContainsAllNegative() throws Exception {


        //step 16
        //create smart automation of Update member details that will run action add points with tags action "tag1,tag2"
        createSmartAutomationUpdateMemberWithActionAddPoints(Lists.newArrayList("tag1","tag2"));

        //create smart automation for tagMemberTrigger that will check condition Tag actions contains all "tag1,tag2,tag3"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsAll);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag1,tag2,tag3");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performUpdateMemberDetails(member);

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    //condition: ActionTags contains one of
    //Action: tag member
    @Test
    @Category(Hub1Regression.class)
    public void testReceivePointsWithConditionActionTagsContainsOneOfActionTagMember() throws Exception {


        //step 17
        //create smart automation of Update member details that will run action add points with tags action "tag1,tag2"
        createSmartAutomationUpdateMemberWithActionAddPoints(Lists.newArrayList("tag1","tag2"));

        //create smart automation f that will check condition Tag actions contains one of "tag4,tag2,tag3"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.ContainsOneOf);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag4,tag2,tag3");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        // perform pay with budget to use members points
        performUpdateMemberDetails(member);
        checkAutomationRunAndTagMember();
    }

    //Negative test
    // condition: ActionTags doesn't contain one of
    //Action: tag member
    @Test
    public void testReceivePointsWithConditionActionTagsDoesntContainOfTheFollowingNegative() throws Exception {


        //step 18
        //create smart automation of Update member details that will run action TagMember with tags action "tag5,tag2"
        createSmartAutomationUpdateMemberWithActionAddPoints(Lists.newArrayList("tag5","tag2"));

        //create smart automation for tagMemberTrigger that will check condition Tag actions doesn't contain one of "tag2,tag4"
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        TagOperator equal = hubAssetsCreator.getTagOperator(automation, TagOperator.Tags_Equals_enum.DoesntContainAny);
        Condition cond = hubAssetsCreator.createConditionTagValue(automation, ApplyIf.ACTION_TAGS, ActionTags.ActionTags, equal, "tag2,tag4");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performUpdateMemberDetails(member);

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }




    //Tests the use points trigger use 200 points(using server API)
    // with condition (points TotalSum not equal 300) and action tag member
    @Test
    @Category(Hub1Regression.class)
    public void testUsesPointsWithConditionTotalSumNotEqualActionTagMember() throws Exception {


        //step 20
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //add points so user can use them
        MembersService.performSmartActionAddPoints("100",timestamp);

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal1 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.POINTS, Points.TotalSumValue, equal1, "30");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));


        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "2000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);

        checkAutomationRunAndTagMember();

    }

    //Negative test:  use points trigger use 1 points(using server API)
    // with condition (points total sum not equal  100) and action tag member
    // -> condition result is false - no action performed
    // BUG : CNP-5605
    @Test
    public void testUsesPointsWithConditionTotalSumNotEqualActionTagMemberNegative() throws Exception {

        //step 21
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //add points so user can use them
        MembersService.performSmartActionAddPoints("100",timestamp);

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        IntOperator equal1 = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.POINTS, Points.TotalSumValue, equal1, "99");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));


        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "100";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);
        //wait for data store to update
        TimeUnit.SECONDS.sleep(5);
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    //Recieve points test:
    // codition - if Last Name Contains timestamp ,action  tag member when user gets 200 points
    @Test
    public void testReceivePointsWithConditionLastNameStringContainsActionTagMember() throws Exception {


        //step 22
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, timestamp.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member with the expiration date next year
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenPoints = "200";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);

        checkAutomationRunAndTagMember();
    }


    private String createSmartAutomationUpdateMemberWithActionAddPoints(ArrayList<String> actionTags) throws Exception {
        //Create smart automation with action tag member
        String automationName = "UpdateForReceivePointsTrigger_" + timestamp;
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(Triggers.UpdatesMembershipDetails, automationName, settings);
        createSmartAutomationScenarioForAddPointsAction(automation, actionTags);
        return automationName;
    }

    private void createSmartAutomationScenarioForAddPointsAction(SmartAutomation automation, ArrayList<String> tags) throws Exception {
        log4j.info("creating scenario");
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal, UpdateMemberOperationValue.UpdateMembership.toString());
        scenario.addCondition(cond, automation);
        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getAddPointsAction(smartAction, automation, tags);
        scenario.addAction(updateSmartAction, automation);
        SmartAutomationService.createSmartAutomation(automation);

    }

    private SmartAutomationAction getAddPointsAction(SmartAutomationAction smartAction, SmartAutomation automation, ArrayList<String> actionTags) throws Exception {
        smartAction.setPerformTheAction(SmartAutomationActions.AddSetNumberofPoints.toString());
        AddPoints action = new AddPoints(automation);
        action.setNumOfPoints("10");
        action.setAmountType(AmountType.Points.toString());
        action.setTags(actionTags, automation);
        smartAction.setAddPoints(action);
        return smartAction;

    }


    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }

}