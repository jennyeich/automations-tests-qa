package hub.smartAutomations.triggers;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.utils.UserActionsConstants;

import java.util.Calendar;
import java.util.UUID;

/**
 * Created by Goni on 6/25/2017.
 */
public class PayWithComoWalletTest extends BaseAutomationTest {

    private static boolean firstTest = true;

    @Override
    public Triggers getTrigger() {
        return Triggers.PaysWithComoWallet;
    }

    @Override
    public String getAutomationName() {
        return "PayWithWallet_" + timestamp;
    }

    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }


    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
            if(firstTest ) {
                serverHelper.configureDefaultBackendForPayWithBudget();
                firstTest = false;
            }
        }
    };

    //Test trigger with condition Gender is not one of action tag member
    @Test
    public void testConditionGenderIsNotOneOfActionTagMember() throws Exception {

        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Male").append(',').append("Other");
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Isnotoneof);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Gender, equal, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }


    @Test
    public void testConditionBirthdayIsBeforeTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        DateOperator equal1 = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsBefore);
        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        Condition cond1 = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.Birthday, equal1, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMemberWithBirthday(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, date);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);


    }

    @Test
    public void testConditionExpirationDateAfterTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, 1);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calNextYear);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }
    //    Negative: action not performed
    //    Condition : Expiration Date is After today
    //    Add Action: tag member
    //    send gift to the member with expiration date as a year before today and redeem
    @Test
    @Category(Hub1Regression.class)
    public void testConditionExpirationDateIsAfterTodayNegative() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        Calendar calPrevYear = Calendar.getInstance();
        calPrevYear.add(Calendar.YEAR, -1);
        DateOperator opr = hubAssetsCreator.getDateOperator(automation, DateOperator.Date_Equals_enum.IsAfter);
        Condition cond = hubAssetsCreator.createConditionDateValue(automation, ApplyIf.MEMBERSHIP, Membership.ExpirationDate, opr, Calendar.getInstance());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member with expiration date
        NewMember member = hubMemberCreator.createMemberWithExpirationDate(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX, calPrevYear);
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        //check that the automation did not run
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }


    //Test with condition DaysTime 2 days (today & tomorrow): action -tag member
    @Test
    public void testConditionDaysTime2DaysTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);

        Calendar nextDay = Calendar.getInstance();
        nextDay.add(Calendar.DAY_OF_WEEK, 1);

        //create today
        DaysTime day1 = hubAssetsCreator.generateSingleDay(Calendar.getInstance());
        //create next day
        DaysTime day2 = hubAssetsCreator.generateSingleDay(nextDay);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, Lists.newArrayList(day1, day2));
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }


    //Test condition DaysTime Today am pm : action -tag member
    @Test
    public void testConditionDaysTimeActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar timeFrom = Calendar.getInstance();

        Calendar timeUntil = Calendar.getInstance();
        timeUntil.add(Calendar.HOUR, 5);
        Condition cond = hubAssetsCreator.createConditionDaysTime(automation, ApplyIf.DAYSTIME, Calendar.getInstance(), timeFrom, timeUntil);

        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Test  condition Date and time are from now till a year later : action -tag member
    @Test
    public void testConditionTimeAndDateActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextYear = Calendar.getInstance();
        calNextYear.add(Calendar.YEAR, +1);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextYear);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Test condition Date and time are from now till 2 days later And email contains string '@como.com': action -tag member
    @Test
    public void testConditionTimeAndDateAndEmailContainsActionTagMember() throws Exception {


        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        Calendar calNow = Calendar.getInstance();
        Calendar calNextDay = Calendar.getInstance();
        calNextDay.add(Calendar.DAY_OF_WEEK, +2);
        Condition cond = hubAssetsCreator.createConditionDateAndTime(automation, ApplyIf.DATETIME, calNow, calNextDay);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal2, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }

    //Last name conds
    @Test
    public void testConditionLastNameIsOneOfActionTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(AUTO_LAST_PREFIX + timestamp).append(',').append("XXXXX");
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.IsOneOf);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, oper, stringBuffer.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);
    }


    //Last name conds - not equals OR contains
    @Test
    public void testConditionLastContainsORNotEqualsTagAction() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Contains);
        StringOperator equal2 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, AUTO_LAST_PREFIX + timestamp);
        Condition cond1 = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal2, AUTO_LAST_PREFIX + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond, cond1), OR, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);
    }



    @Test
    public void testConditionLastNameIsNontOneOFTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.Isnotoneof);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.LastName, equal1, "testLast");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);


    }

    // action : tag  member with one condition :email string not contain "@como.com" suffix
    //check that no user action was created in data store
    @Test
    public void testWithConditionEmailStringNotContainsTagMemberNegative() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.DoesntContain);
        Condition cond = hubAssetsCreator.createCondition(automation,ApplyIf.MEMBERSHIP,Membership.Email,equal1, MAIL_SUFFIX);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond),AND, Lists.newArrayList(SmartAutomationActions.TagMember));

        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        //Get member userKey and turn into User_KEY
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    //Tests the send asset with  codition - email not equals mock email ,action  tag member
    @Test
    public void testConditionEmailNotEqActionTagMember() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.Email, equal1, MOCK_EMAIL);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);

    }


    //Tests the send asset with  codition - first name equals ,action  tag member
    @Test
    public void testConditionFirstNameEqualsTagMember() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator equal1 = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal1, "auto_user_" + timestamp);
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);
    }


      //Tests the send asset with  condition - wallet payment->payment type == credit
    @Test
    public void testConditionWalletPaymentCreditTagAction() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.WALLET_PAYMENT, WalletPayment.PaymentType, oper, PaymentTypeValue.Credit.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);
    }

    //Tests the send asset with  condition - wallet payment->payment type != points
    @Test
    public void testConditionWalletPaymentNotEqualsPointsTagAction() throws Exception {


        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        StringOperator oper = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.NotEqualto);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.WALLET_PAYMENT, WalletPayment.PaymentType, oper, PaymentTypeValue.Points.toString());
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "3000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"222",transactionId);

        checkAutomationRunAndTagMemberByTransaction(transactionId);
    }



    //Tests the send asset with  condition - wallet payment->RequestedSum smaller then 50
    //in order to acheive this we need to create condition that say that the requested sum > -50
    //(because we pay it is negative number and we compare negative we need to use opposite operator) (-33333 < -50) --> 33333> 50 false
    @Test
    public void testWithWalletPaymentRequestedSumLessThanNegativeTest() throws Exception {

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        IntOperator oper = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.GreaterOrEqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.WALLET_PAYMENT, WalletPayment.RequestedSum, oper, "-50");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "50000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"33333",transactionId);

        //check that the automation did not run
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }

    //Send members a special gift when they use more than a certain number of points.
    //in order to acheive this we need to create condition that say that the requested sum < -100
    //(because we pay it is negative number and we compare negative we need to use opposite operator)
    @Test
    public void testWithWalletPaymentRequestedSumGreaterThan100PointsActionSendAsset() throws Exception {

        smartGift = hubAssetsCreator.createSmartGift(timestamp);

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        //create condition
        IntOperator oper = hubAssetsCreator.getIntOperator(automation, IntOperator.Int_Equals_enum.LessOrEqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.WALLET_PAYMENT, WalletPayment.RequestedSum, oper, "-100");
        createSmartAutomationScenario(automation, Lists.newArrayList(cond), AND, Lists.newArrayList(SmartAutomationActions.SendAnAsset));
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints = "50000";
        //add points to member credit
        String actionTag = MembersService.performSmartActionAddCredit(givenPoints,timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(member.getPhoneNumber(),"30000",transactionId);

        //check that the automation did not run
        checkAutomationRunAndSendAssetToMember();
    }



    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }
}
