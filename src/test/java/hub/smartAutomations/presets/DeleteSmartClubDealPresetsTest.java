package hub.smartAutomations.presets;


import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.smarts.SmartClubDeal;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Preset;
import hub.hub1_0.services.Navigator;
import hub.services.NewApplicationService;
import hub.services.assets.AssetService;
import hub.utils.HubAssetsCreator;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.io.IOException;
import java.lang.invoke.MethodHandles;


/**
 * Created by Jenny on 3/08/2017.
 *
 */
public class DeleteSmartClubDealPresetsTest extends BaseIntegrationTest {


    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }
    HubAssetsCreator hubAssetsCreator = new HubAssetsCreator();

    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     **/
    @Test
    @Category(Hub1Regression.class)
    public void testDeletePresetFromNewSmartClubDeal() throws Exception {

        //create preset
        String presetName = hubAssetsCreator.createSmartClubDealPreset("preset_" + String.valueOf(System.currentTimeMillis()));
        Assert.assertTrue(AssetService.checkIfPresetExistsClubDeal(presetName));
        deletePresetFromNewSmartClubDeal(presetName);
        Assert.assertFalse(AssetService.checkIfPresetExistsClubDeal(presetName));

    }

    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     **/
    @Test
    public void testDeletePresetFromExistingSmartClubDeal() throws Exception {

        //create preset
        String presetName = hubAssetsCreator.createSmartClubDealPreset("preset_" + String.valueOf(System.currentTimeMillis()));
        //Verify the preset exists
        Assert.assertTrue(AssetService.checkIfPresetExistsClubDeal(presetName));
        //Create smart gift
        SmartClubDeal smartGift = hubAssetsCreator.createClubDeal(String.valueOf(System.currentTimeMillis()));
        deletePresetFromExistingClubDeal(smartGift,presetName);
        //Delete the preset from existing smart gift
        Assert.assertFalse(AssetService.checkIfPresetExistsClubDeal(presetName));
    }

    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     **/
    @Test
    public void testDeletePresetSpecialCharsFromNewClubDeal() throws Exception {

        //create preset
        String presetName = hubAssetsCreator.createSmartClubDealPreset("preset!@#$%^&*()<>,.';:{}[]+-" + String.valueOf(System.currentTimeMillis()));
        Assert.assertTrue(AssetService.checkIfPresetExistsClubDeal(presetName));
        deletePresetFromNewSmartClubDeal(presetName);
        Assert.assertFalse(AssetService.checkIfPresetExistsClubDeal(presetName));
    }

    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     * 	delete preset from another app
     **/
    @Test
    public void testDeletePresetFromAnotherAppFromNewSmartClubDealNegative() throws Exception {

        //Create new app
        NewApplicationService.createNewAppFromUrl();

        //create preset
        String presetName = hubAssetsCreator.createSmartClubDealPreset("presetAnotherApp_" + String.valueOf(System.currentTimeMillis()));
        Assert.assertTrue(AssetService.checkIfPresetExistsClubDeal(presetName));

        //switch to testing application
        Navigator.selectApplication();
        deletePresetFromNewSmartClubDeal(presetName);
        Assert.assertTrue("Preset was deleted when should not be ! ",AssetService.checkIfPresetExistsClubDeal(presetName));

    }

    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     * 	delete preset from another app - existing club deal
     **/
    @Test
    public void testDeletePresetFromAnotherAppFromExistingSmartClubDealNegative() throws Exception {

        //Create new app
        NewApplicationService.createNewAppFromUrl();
        //create preset
        String presetNameAnotherApp = hubAssetsCreator.createSmartClubDealPreset("presetAnotherApp_" + String.valueOf(System.currentTimeMillis()));
        Assert.assertTrue(AssetService.checkIfPresetExistsClubDeal(presetNameAnotherApp));

        //switch to testing application
        Navigator.selectApplication();
        //Create smart gift
        SmartClubDeal smartGift = hubAssetsCreator.createClubDeal(String.valueOf(System.currentTimeMillis()));
        deletePresetFromExistingClubDeal(smartGift,presetNameAnotherApp);
        Assert.assertTrue("Preset was deleted from another application when should not be !", AssetService.checkIfPresetExistsClubDeal(presetNameAnotherApp));

    }


    private void deletePresetFromNewSmartClubDeal(String presetName) throws Exception {

        Preset preset = new Preset();
        preset.setPreset(presetName);
        preset.setDelete("Y");
        AssetService.deletePresetFromNewClubDeal(preset);
        log4j.info("Deleted smart club deal preset : " + presetName);

    }

    private void deletePresetFromExistingClubDeal(SmartClubDeal smartClubDeal, String presetName) throws Exception {

        Preset preset = new Preset();
        preset.setPreset(presetName);
        preset.setDelete("Y");
        AssetService.deletePresetFromExistingClubDeal(smartClubDeal,preset);
        log4j.info("Deleted smart gift preset : " + presetName);

    }
}
