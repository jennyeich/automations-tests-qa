package hub.smartAutomations.presets;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Preset;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.Navigator;
import hub.services.NewApplicationService;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.smartAutomations.BaseAutomationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;


/**
 * Created by Jenny on 3/08/2017.
 *
 */
public class DeleteSmartAutomationPresetsTest extends BaseAutomationTest {

    @Override
    public Triggers getTrigger() {
        return Triggers.JoinsTheClub;
    }

    @Override
    public String getAutomationName() {
        return "Joins the club_" + timestamp;
    }

    @Before
    public void checkActiveAutomations(){
        checkSmartAutomation();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            settings = new Settings();
            tag = "tag_" + timestamp;
        }
    };

    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     **/
    @Test
    public void testDeletePresetFromNewAutomation() throws Exception {

        //create preset
        String presetName = createPreset("preset_"+ String.valueOf(System.currentTimeMillis()));
        Assert.assertTrue(SmartAutomationService.checkIfPresetExists(presetName));
        deletePresetFromNewAutomation(presetName);
        Assert.assertFalse(SmartAutomationService.checkIfPresetExists(presetName));

    }

    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     **/
    @Test
    public void testDeletePresetFromExistingAutomation() throws Exception {

        //create preset
        String presetName = createPreset("preset_"+ String.valueOf(System.currentTimeMillis()));
        //Verify the preset exists
        Assert.assertTrue(SmartAutomationService.checkIfPresetExists(presetName));
        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        createSmartAutomationScenario(automation, Lists.newArrayList(),AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        log4j.info("Created automation: " + automation.getSettings().getAutomationName());
        //Delete the preset from existing automation -find and open automation
        deletePresetFromExistingAutomation(automation, presetName);
        Assert.assertFalse(SmartAutomationService.checkIfPresetExists(presetName));
    }

    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     **/
    @Test
    @Category(Hub1Regression.class)
    public void testDeletePresetSpecialCharsFromNewAutomation() throws Exception {

        //create preset
        String presetName = createPreset("preset!@#$%^&*()_+:<>;'"+ String.valueOf(System.currentTimeMillis()));
        Assert.assertTrue(SmartAutomationService.checkIfPresetExists(presetName));
        deletePresetFromNewAutomation(presetName);
        Assert.assertFalse(SmartAutomationService.checkIfPresetExists(presetName));
    }


    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     * 	Creates new application and create a preset and try to delete from another app
     **/
    @Test
    public void testDeletePresetFromAnotherAppNegativeTest() throws Exception {

        //Create new app
        NewApplicationService.createNewAppFromUrl();
        //create preset
        String presetNameAnotherApp = createPreset("anotherAppPreset"+ String.valueOf(System.currentTimeMillis()));
        Assert.assertTrue(SmartAutomationService.checkIfPresetExists(presetNameAnotherApp));
        //switch to testing application
        Navigator.selectApplicationFromEnv();
        deletePresetFromNewAutomation(presetNameAnotherApp);
        Assert.assertTrue(SmartAutomationService.checkIfPresetExists(presetNameAnotherApp));

    }

    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     * 	Creates new application and create a preset and try to delete from another app
     **/
    @Test
    public void testDeletePresetFromAnotherAppFromExistingAutomationNegativeTest() throws Exception {

        //Create new app
        NewApplicationService.createNewAppFromUrl();
        //create preset
        String presetNameAnotherApp = createPreset("anotherAppPreset"+ String.valueOf(System.currentTimeMillis()));
        Assert.assertTrue(SmartAutomationService.checkIfPresetExists(presetNameAnotherApp));
        //switch to testing application
        Navigator.selectApplicationFromEnv();

        //Create smart automation
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(this,settings);
        createSmartAutomationScenario(automation, Lists.newArrayList(),AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        log4j.info("Created automation: " + automation.getSettings().getAutomationName());

        //Delete the preset from existing automation -find and open automation
        deletePresetFromExistingAutomation(automation, presetNameAnotherApp);
        Assert.assertTrue(SmartAutomationService.checkIfPresetExists(presetNameAnotherApp));
    }


    private void deletePresetFromNewAutomation (String presetName) throws Exception {

        Preset preset = new Preset();
        preset.setPreset(presetName);
        preset.setDelete("Y");
        SmartAutomationService.deletePresetFromNewAutomation(preset);

    }

    private void deletePresetFromExistingAutomation (SmartAutomation automation, String presetName) throws Exception {

        Preset preset = new Preset();
        preset.setPreset(presetName);
        preset.setDelete("Y");
        SmartAutomationService.deletePresetFromExistingAutomation(automation,preset);

    }

    private String createPreset (String name) throws Exception {

        //Create smart automation preset
        SmartAutomation automationPreset = hubAssetsCreator.createSmartAutomation(Triggers.UpdatesMembershipDetails, name, settings);
        automationPreset.setSaveAsPreset("Yes");
        createSmartAutomationScenario(automationPreset, Lists.newArrayList(),AND, Lists.newArrayList(SmartAutomationActions.TagMember));
        String presetName = automationPreset.getSettings().getAutomationName();
        Thread.sleep(5000);
        return presetName;
    }




    @After
    public void cleanData() {
        log4j.info("deactivate smart automation from APIs");
        deactivateAutomation();
    }


}
