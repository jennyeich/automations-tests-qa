package hub.smartAutomations.presets;


import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.smarts.SmartGift;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Preset;
import hub.hub1_0.services.Navigator;
import hub.services.NewApplicationService;
import hub.services.assets.AssetService;
import hub.utils.HubAssetsCreator;
import integration.base.BaseIntegrationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;

import java.io.IOException;
import java.lang.invoke.MethodHandles;


/**
 * Created by Jenny on 3/08/2017.
 *
 */
public class DeleteSmartGiftPresetsTest extends BaseIntegrationTest {


    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }
    HubAssetsCreator hubAssetsCreator = new HubAssetsCreator();

    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     **/
    @Test
    public void testDeletePresetFromNewSmartGift() throws Exception {

        //create preset
        String presetName = hubAssetsCreator.createSmartGiftPreset("preset_" + String.valueOf(System.currentTimeMillis()));
        Assert.assertTrue(AssetService.checkIfPresetExistsSmartGift(presetName));
        deletePresetFromNewSmartGift(presetName);
        Assert.assertFalse(AssetService.checkIfPresetExistsSmartGift(presetName));

    }

    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     **/
    @Test
    @Category(Hub1Regression.class)
    public void testDeletePresetFromExistingSmartGift() throws Exception {

        //create preset
        String presetName = hubAssetsCreator.createSmartGiftPreset("preset_" + String.valueOf(System.currentTimeMillis()));
        //Verify the preset exists
        Assert.assertTrue(AssetService.checkIfPresetExistsSmartGift(presetName));
        //Create smart gift
        SmartGift smartGift = hubAssetsCreator.createSmartGift(String.valueOf(System.currentTimeMillis()));
        deletePresetFromExistingSmartGift(smartGift,presetName);
        //Delete the preset from existing smart gift
        Assert.assertFalse(AssetService.checkIfPresetExistsSmartGift(presetName));
    }

    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     **/
    @Test
    public void testDeletePresetSpecialCharsFromNewSmartGift() throws Exception {

        //create preset
        String presetName = hubAssetsCreator.createSmartGiftPreset("preset!@#$%^&*()_+:<>;'"+ String.valueOf(System.currentTimeMillis()));
        Assert.assertTrue(AssetService.checkIfPresetExistsSmartGift(presetName));
        deletePresetFromNewSmartGift(presetName);
        Assert.assertFalse(AssetService.checkIfPresetExistsSmartGift(presetName));
    }

    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     * 	Delete preset from another application
     **/
    @Test
    public void testDeletePresetFromNewSmartGiftAnotherApplication() throws Exception {

        //Create new app
        NewApplicationService.createNewAppFromUrl();

        //create preset
        String presetName = hubAssetsCreator.createSmartGiftPreset("preset_" + String.valueOf(System.currentTimeMillis()));
        Assert.assertTrue(AssetService.checkIfPresetExistsSmartGift(presetName));

        //switch to testing application
        Navigator.selectApplicationFromEnv();
        deletePresetFromNewSmartGift(presetName);
        Assert.assertTrue("Preset from another application was deleted while should not be !",AssetService.checkIfPresetExistsSmartGift(presetName));

    }

    /**
     * 	test: C6235 CNP - 6528 Delete Presets
     * 	Delete preset from another application existing gift
     **/
    @Test
    public void testDeletePresetFromExistingSmartGiftAnotherApplication() throws Exception {

        //Create new app
        NewApplicationService.createNewAppFromUrl();

        //create preset
        String presetName = hubAssetsCreator.createSmartGiftPreset("preset_" + String.valueOf(System.currentTimeMillis()));
        Assert.assertTrue(AssetService.checkIfPresetExistsSmartGift(presetName));

        //switch to testing application
        Navigator.selectApplicationFromEnv();
        SmartGift smartGift = hubAssetsCreator.createSmartGift(String.valueOf(System.currentTimeMillis()));
        deletePresetFromExistingSmartGift(smartGift,presetName);
        Assert.assertTrue("Preset from another application was deleted while should not be !",AssetService.checkIfPresetExistsSmartGift(presetName));

    }



    private void deletePresetFromNewSmartGift (String presetName) throws Exception {

        Preset preset = new Preset();
        preset.setPreset(presetName);
        preset.setDelete("Y");
        AssetService.deletePresetFromNewSmartGift(preset);
        log4j.info("Deleted smart gift preset : " + presetName);

    }

    private void deletePresetFromExistingSmartGift(SmartGift smartGift, String presetName) throws Exception {

        Preset preset = new Preset();
        preset.setPreset(presetName);
        preset.setDelete("Y");
        AssetService.deletePresetFromExistingGift(smartGift,preset);
        log4j.info("Deleted smart gift preset : " + presetName);

    }
}
