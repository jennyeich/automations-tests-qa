package hub.suites;

import hub.base.BaseHubTest;
import hub.base.categories.hub1Categories.Hub1Regression;
import integration.base.BaseIntegrationTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.IOException;

@RunWith(Categories.class)

@Categories.IncludeCategory( {Hub1Regression.class} )
/*
this class run suites which contains web tests
 */
@Suite.SuiteClasses({


})
public class RegressionSuiteHub1 {

    public static final Logger log4j = Logger.getLogger(RegressionSuiteHub1.class);

    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseIntegrationTest.initSuite(true);
    }


    @AfterClass
    public static void tearDown(){
        log4j.info("close browser after finish all Suite tests");
        BaseIntegrationTest.cleanBase();
        BaseHubTest.cleanHub();
    }
}
