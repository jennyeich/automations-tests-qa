package hub.base;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.NewMember;
import hub.common.objects.member.performActionObjects.TagUntagOperation;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.ApplyIf;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Condition;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Membership;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.StringOperator;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import hub.hub1_0.services.Navigator;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.member.MembersService;
import hub.smartAutomations.BaseAutomationTest;
import hub.utils.HubAssetsCreator;
import integration.base.BaseIntegrationTest;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.runners.MethodSorters;
import ru.yandex.qatools.allure.annotations.Issue;
import server.utils.ServerHelper;
import utils.EnvProperties;
import utils.PropsReader;

import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Goni on 3/27/2017.
 * Sanity tests for basic actions on the HUB.
 * This should be invoked after every git push.
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SanityTest extends BaseIntegrationTest{

    public static final String AUTO_USER_PREFIX = "auto_user_";
    public static final String AUTO_LAST_PREFIX = "auto_last_";
    public static Settings settings;
    public static CharSequence timestamp;

    public static Key key = null;

    HubAssetsCreator hubAssetsCreator = new HubAssetsCreator();

    @BeforeClass
    public static void setup() throws IOException {
        if(PropsReader.getPropValuesForEnv("location_prefix").contains("qa_") ||
                PropsReader.getPropValuesForEnv("location_prefix").contains("hotfix_")||
                PropsReader.getPropValuesForEnv("location_prefix").contains("")) {
            isNewApplication = false;
        }
        init();
        log4j.info("HUB login succeeded...");
    }

    @Test
    public void testSelectAppAndCreateNewMember() throws Exception {

        String appName = EnvProperties.getInstance().getApplicationName();
        if(appName == null)
            appName = PropsReader.getPropValuesForEnv("appName");
        Navigator.selectApplication(appName);
        log4j.info("Select application " + appName + " succeeded...");

        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        Thread.sleep(5000);
        checkDataStoreUserAtLocation(member.getPhoneNumber());
    }

    @Test
    @Category(Hub1Regression.class)
    public void testCreateSmartGift() throws Exception {

        timestamp = String.valueOf(System.currentTimeMillis());
        SmartGift smartGift = hubAssetsCreator.createSmartGift(timestamp);

        log4j.info("check Data Store Updated with Tag");
        Query q = ServerHelper.buildQuery("Asset", Arrays.asList(
                new Query.FilterPredicate("Type", Query.FilterOperator.EQUAL, "gift"),
                new Query.FilterPredicate("Name", Query.FilterOperator.EQUAL, smartGift.getTitle())));

        java.util.List<Entity> res = dataStore.findByQuery(q);
        Assert.assertEquals("Must find one asset with the name: " + smartGift.getTitle(), 1, res.size());

        cleanAsset(smartGift);
    }

    @Test
    @Category(Hub1Regression.class)
    public void testFindMember() throws Exception {

        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        Thread.sleep(2000);
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findMembersByFirstName(findMember.getFirstName());

        Thread.sleep(5000);
        checkDataStoreUserAtLocation(member.getPhoneNumber());

    }

    @Test
    public void testCreateSmartAutomationWithConditionAndActionTagMember() throws Exception {

        settings = new Settings();
        timestamp = String.valueOf(System.currentTimeMillis());
        SmartAutomation automation = hubAssetsCreator.createSmartAutomation(Triggers.JoinsTheClub,"JoinsTheClub_" + timestamp.toString(),settings);
        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createCondition(automation, ApplyIf.MEMBERSHIP, Membership.FirstNAme, equal, AUTO_USER_PREFIX + timestamp);
        createSmartAutomationScenario(automation, cond);

        Thread.sleep(5000);
        String autoURL = SmartAutomationService.getAutomationURL(settings);
        String autoName = "automation_" + autoURL;

        log4j.info("check Data Store Updated with Tag");
        Query q = new Query("EventBasedActionRule");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Trigger", Query.FilterOperator.EQUAL, "joinedclub"),
                new Query.FilterPredicate("Status", Query.FilterOperator.EQUAL, "active"),
                new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, autoName))));
        java.util.List<Entity> res = dataStore.findByQuery(q);
        Assert.assertEquals("Must find one action with the RuleID: " + autoName, 1, res.size());
        Text t = ((Text) res.get(0).getProperty("SmartAction"));
        log4j.info("SmartAction contains the following data : " + t.getValue());

        BaseAutomationTest.deactivateAutomation();
    }


    private void checkDataStoreUserAtLocation(String phone) throws Exception {
        log4j.info("check Data Store ");
        Query q = new Query("UserAtLocation");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, key),
                new Query.FilterPredicate("PhoneNumber", Query.FilterOperator.EQUAL, phone))));
        java.util.List<Entity> res = dataStore.findByQuery(q);
        Assert.assertEquals("Must find one User with the userKey: " + key, 1, res.size());
        String phoneNumber = (String) res.get(0).getProperty("PhoneNumber");
        log4j.info( "member phone number : " + phoneNumber);
        Assert.assertEquals(phoneNumber,phone);
    }

    private void createSmartAutomationScenario(SmartAutomation automation, Condition cond) throws Exception {

        log4j.info("creating scenario");
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        SmartAutomationAction smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = hubAssetsCreator.getTagMemberAction(smartAction, automation, timestamp.toString(), TagUntagOperation.Tag);
        scenario.addAction(updateSmartAction, automation);

        scenario.addCondition(cond, automation);

        SmartAutomationService.createSmartAutomation(automation);
        Object auto = SmartAutomationService.checkIfAutomationCreated(settings.getAutomationName());
        Assert.assertNotNull("Automation :" + settings.getAutomationName() + " was not created.", auto);
    }



    public static void cleanAsset(SmartGift smartGift) throws Exception {
        if(smartGift != null)
            MembersService.deactivateGift(smartGift.getTitle());
        Navigator.refresh();
    }

    @AfterClass
    public static void closeBrowser(){
        cleanBase();
        BaseHubTest.cleanHub();
    }

}
