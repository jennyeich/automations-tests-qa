package hub.base;

import common.BaseTest;
import hub.services.LoginLogoutService;
import hub.hub1_0.services.Navigator;
import hub.services.NewApplicationService;
import hub.services.assets.AssetService;
import hub.utils.ScreenShotTaker;
import org.junit.AfterClass;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Created by Jenny on 11/2/2016.
 */
public class BaseHubTest extends BaseTest{

    public static WebDriver driver;

    public static void  initHub(boolean isPermissionTag){
        try {
            LoginLogoutService l= new LoginLogoutService();
            driver =  l.performLogin(isPermissionTag);
            AssetService.setDriver(driver);
            Navigator.setDriver(driver);
            Navigator.selectApplicationFromEnv();
        } catch (Exception e) {
            takeScreenShotAfterFailLogin(e);
        }
    }

    private static void takeScreenShotAfterFailLogin(Exception e) {
        DateFormat df = new SimpleDateFormat("ddMMyyyyHHmmss");
        String data = df.format(new Date());
        ScreenShotTaker.takeScreenShot("initFlow_" + data, log4j);
        log4j.error(e.getMessage(),e);
        e.printStackTrace();
        Assert.fail(e.getMessage());
    }

    @AfterClass
    public static void closeBrowser(){
        cleanBase();
        cleanHub();
    }

    public static void cleanHub(){
        if(driver !=null) {
            driver.close();
            driver.quit();
        }
    }


    public static void initHubForNewApp(boolean isPermissionTagTest ){

        try {
            LoginLogoutService l= new LoginLogoutService();
            driver =  l.performLogin(isPermissionTagTest);
            ScreenShotTaker.takeScreenShot("afterLogin",log4j);
            AssetService.setDriver(driver);
            Navigator.setDriver(driver);
            NewApplicationService.setDriver(driver);
            NewApplicationService.createNewApp();
            ScreenShotTaker.takeScreenShot("after_createNewApp", log4j);
        } catch (Exception e) {
            takeScreenShotAfterFailLogin(e);
        }
    }

}