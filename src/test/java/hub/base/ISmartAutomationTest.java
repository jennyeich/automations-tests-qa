package hub.base;


import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;


/**
 * Created by Goni on 3/15/2017.
 *
 */
public interface ISmartAutomationTest {

    /**
     * Returns the Trigger that is tested in the the test class
     * @return Trigger name (e.g: triggers.SubmitAPurchase )
     */

    ITrigger getTrigger();


    /**
     * Returns the automation name we build according to the test class
     * @return
     */
    String getAutomationName();

}
