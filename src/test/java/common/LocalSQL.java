package common;

import org.apache.commons.dbcp.BasicDataSource;
import utils.PropsReader;

import java.sql.*;

/**
 * Created by lior on 6/4/17.
 */
public class LocalSQL {

    private Statement stmt = null;
    private Connection conn = null;
    private BasicDataSource ds;

    public void setUp() throws Exception {
        try {
            ds = new BasicDataSource();
            ds.setDriverClassName("com.mysql.cj.jdbc.Driver");
            ds.setUsername(PropsReader.getPropValuesForEnv("SQLUser"));
            ds.setPassword(PropsReader.getPropValuesForEnv("SQLPassword"));
            ds.setUrl(PropsReader.getPropValuesForEnv("ComoSQLUrl"));

            // the settings below are optional -- dbcp can work with defaults
            ds.setMinIdle(5);
            ds.setMaxIdle(30);
            ds.setMaxOpenPreparedStatements(180);

        }catch (Exception e){
            throw new Exception("Failed to init sql db", e);
        }
    }


    public Connection getConn() throws SQLException{
        if (conn == null || conn.isClosed())
            conn = ds.getConnection();
        return conn;
    }


    public static String removeTillWord(String input, String word) {
        return input.substring(input.indexOf(word));

    }


    public String getLastBigQueryForBusiness(String locationID) throws ClassNotFoundException, SQLException {

        String location = "'%" + locationID + "%'";

        String query = "SELECT * FROM keeprz.YiiLog_WebWorker WHERE category = 'application.BigQueryController' AND message LIKE '%handleQueryRequest query ready:%' AND message LIKE" + location + " ORDER BY id DESC LIMIT 1";
        ResultSet rs = this.query(query);

        while (rs.next()) {
                String message = rs.getString("message");
                String selectQuery = removeTillWord(message, "SELECT");
                return selectQuery;
        }
        return "";
    }


    public void setHub2Wallet(String locationID, String wallet) throws SQLException{
        String query =   "UPDATE applications_alpha.purchase_weights SET businessWallet = '" + wallet + "' WHERE location_id = '" + locationID + "'";
        //set the new val
        PreparedStatement preparedStmt = conn.prepareStatement(query);
        preparedStmt.executeUpdate();

    }

    /**
     * # 0 Entertainment
     # 1 Retail Foreign Exchange
     # 2 Services
     # 3 Food & Drink
     # 4 General
     # 5 Sports
     # 6 Retail & Fashion
     # 7 Health & Beauty
     # 8 Other
     # 9 More
     */
    public void setHub2BusinessVertical(String locationID,int newVertical) throws SQLException{
        String query =   "UPDATE applications_alpha.locations_data SET sf_token=" + newVertical + " WHERE location_id = '" + locationID + "'";
        //set the new val
        PreparedStatement preparedStmt = conn.prepareStatement(query);
        preparedStmt.executeUpdate();
    }

    public void tearDown(){
        try {
            if(!stmt.isClosed())
                stmt.close();
            conn.close();

        }catch (SQLException e){
            System.out.println(e);
        }

    }

    public ResultSet query(String query) throws SQLException {

        createStatment();
        return stmt.executeQuery(query);
    }

    public void createStatment() throws SQLException{
        if(stmt == null || stmt.isClosed())
            stmt = getConn().createStatement();
    }

    public void openConnection() throws SQLException{
        if (conn == null || conn.isClosed()){
            conn = ds.getConnection();
            stmt = conn.createStatement();
        }


    }

    public void closeStatment() throws SQLException{
        stmt.close();
    }
}

