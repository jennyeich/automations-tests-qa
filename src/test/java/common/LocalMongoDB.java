package common;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.result.DeleteResult;
import org.apache.log4j.Logger;
import org.bson.Document;
import org.junit.Assert;
import utils.PropsReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class LocalMongoDB {

    public static final String GOALS = "goals";

    private MongoClient mongoClient;
    private MongoDatabase database;
    private MongoCollection<Document> collection;

    private static final Logger log4j = Logger.getLogger(LocalMongoDB.class);

    public void setUp() throws Exception {
        try {
            mongoClient = new MongoClient(PropsReader.getPropValuesForEnv("mongoConnection"), Integer.valueOf(PropsReader.getPropValuesForEnv("mongoPort")).intValue());

            database = mongoClient.getDatabase(PropsReader.getPropValuesForEnv("mongoDatabase"));

        } catch (Exception e) {
            throw new Exception("Failed to init mongo db", e);
        }

    }

    public List<String> queryCollectionByProperty(String collectionName, String goalName, String propertyName) {

        List<String> goals = new ArrayList<>();
        try {

            collection = database.getCollection(collectionName);
            Document query = new Document();
            query.append("name", goalName);

            Block<Document> processBlock = new Block<Document>() {
                @Override
                public void apply(final Document document) {
                    goals.add((String) document.get(propertyName));
                }
            };

            collection.find(query).forEach(processBlock);

        } catch (MongoClientException e) {
            log4j.error(e.getMessage(), e);
            Assert.fail("Failed to retrieve data from collection: " + collectionName);
        }

        return goals;
    }

    public List<String> getProductTemplatesByCollection(String collectionName, String filter, Set<String> productTemplates) {
        List<String> templatesIds = new ArrayList<>();
        collection = database.getCollection(collectionName);
        Document query = new Document();
        query.append("_t", filter);
        query.append("archived", false);
        query.append("templateName", new Document()
                .append("$in", Lists.newArrayList(productTemplates))
        );
        Document projection = new Document();
        projection.append("_id", "$_id");

        Block<Document> processBlock = new Block<Document>() {
            @Override
            public void apply(final Document document) {
                templatesIds.add((String) document.get("_id"));
            }
        };

        collection.find(query).projection(projection).forEach(processBlock);
        return templatesIds;
    }


    public List<String> queryProductTemplatesOfGoal(String collectionName, String goalName) {

        List<String> templatesNames = new ArrayList<>();
        List<String> templatesIds = new ArrayList<>();
        try {
            collection = database.getCollection(collectionName);
            Document query = new Document();
            query.append("archived", true);

            Document projection = new Document();
            projection.append("templatesRules", "$templatesRules");


            Block<Document> processBlock = new Block<Document>() {
                @Override
                public void apply(final Document document) {
                    templatesIds.addAll(((ArrayList<String>) document.get("templatesRules")));
                }
            };
            collection.find(query).projection(projection).forEach(processBlock);

            templatesNames.addAll(queryProductTemplateNamesOfGoal("ruleTemplates", templatesIds));

        } catch (MongoClientException e) {
            log4j.error(e.getMessage(), e);
            Assert.fail("Failed to retrieve data from collection: " + collectionName);
        }

        return templatesNames;

    }

    public void queryArchiveRuleProductTemplates() {
        queryArchiveProductTemplates("ruleTemplates", "RuleProductTemplate");
    }

    public void queryArchiveDealProductTemplates() {
        queryArchiveProductTemplates("dealTemplates", "DealProductTemplate");
    }


    private void queryArchiveProductTemplates(String collectionName, String templateType) {
        try {
            collection = database.getCollection(collectionName);
            Document searchQuery = new Document();
            searchQuery.append("_t", templateType);
            searchQuery.append("templateName", Pattern.compile("^PT..*$", Pattern.CASE_INSENSITIVE));
            searchQuery.append("archived", false);

            Document updateQuery = new Document();
            updateQuery.append("$set",
                    new BasicDBObject().append("archived", true));

            collection.updateMany(searchQuery, updateQuery);

        } catch (MongoClientException e) {
            log4j.error(e.getMessage(), e);
            Assert.fail("Failed to archive templates from collection: " + collectionName);
        }

    }

    private List<String> queryProductTemplateNamesOfGoal(String collectionName, List<String> templateIds) {

        List<String> templatesNames = new ArrayList<>();
        try {
            collection = database.getCollection(collectionName);

            for (String templateId : templateIds) {
                Document query = new Document();
                query.append("_id", templateId);
                query.append("archived", false);

                Document projection = new Document();
                projection.append("_id", "$_id");
                projection.append("templateName", "$templateName");

                Block<Document> processBlock = new Block<Document>() {
                    @Override
                    public void apply(final Document document) {
                        templatesNames.add((String) document.get("templateName"));
                    }
                };

                collection.find(query).projection(projection).forEach(processBlock);
            }

        } catch (MongoException e) {
            log4j.error(e.getMessage(), e);
            Assert.fail("Failed to retrieve data from collection: " + collectionName);
        }


        return templatesNames;

    }

    public long removeGoal(String goalName) {

        long deleteCount = 0;
        try {

            Document query = new Document();
            query.append("name", goalName);
            DeleteResult deleteResult = database.getCollection("goals").deleteOne(query);
            deleteCount = deleteResult.getDeletedCount();
        } catch (MongoException e) {
            log4j.error(e.getMessage(), e);
            Assert.fail("Failed to delete data from collection goals ");
        }


        return deleteCount;

    }

    public void tearDown() {
        try {
            mongoClient.close();

        } catch (Exception e) {
            log4j.error("Failed to close mongoDB!!");
        }

    }
}
