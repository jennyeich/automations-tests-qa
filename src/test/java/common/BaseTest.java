package common;

import hub.utils.HubAssetsCreator;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import server.utils.ServerHelper;
import server.v2_8.Consent;
import server.v2_8.base.ICleanable;
import utils.EnvProperties;
import utils.PropsReader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by gili on 12/7/16.
 */
public class BaseTest {

    public static final String ENV_PREFIX = PropsReader.getPropValuesForEnv("location_prefix");
    public static String apiKey;
    public static String locationId;
    public static String serverToken;
    public static String applicationName;

    public static final Integer httpStatusCode200 = 200;
    public static final Integer httpStatusCode201 = 201;
    public static final Integer httpStatusCode400 = 400;
    public static final Integer httpStatusCode404 = 404;
    public static final Integer httpStatusCode500 = 500;

    public final static String STATUS_OK = "ok";
    public final static String ERRORS = "errors";
    public final static String MESSAGE = "message";
    public final static String CODE = "code";

    public static HubAssetsCreator hubAssetsCreator = new HubAssetsCreator();
    private static Random random = new Random();


    /*flag that indicates if run the tests with new application or with exist one*/
    public static boolean isNewApplication = true;

    /*flag is needed when using suite and we don't want to init application for every class in the suite,only one time*/
    public static boolean firstTime = true;
    public static boolean suiteRunning = false;
    public static final Logger log4j = Logger.getLogger(BaseTest.class);

    public static String logPrefix = "Log";

    public static LocalDatastore dataStore = new LocalDatastore();
    public static LocalSQL sqlDB = new LocalSQL();
    public static LocalMongoDB mongoDB = new LocalMongoDB();

    public List<ICleanable> objectsToClean;
    public static ServerHelper serverHelper;



    @Rule
    public TestWatcher testWatcher = new TestWatcher(){
        @Override
        protected void failed(Throwable e, Description description){
            log4j.info(description.getDisplayName() + " FAILED");
            log4j.error(e);
           e.printStackTrace();
        }

        @Override
        protected void succeeded(Description description){
            log4j.info(description.getDisplayName() + " PASSED");
        }

        @Override
        protected void starting(Description description){
            log4j.info("Starting test :" + description.getDisplayName());
        }

        @Override
        protected void finished(Description description){
            log4j.info("Finished test: " + description.getDisplayName());

        }

    };

    public class Retry implements TestRule {
        private int retryCount;

        public Retry(int retryCount) {
            this.retryCount = retryCount;
        }

        public Statement apply(Statement base, Description description) {
            return statement(base, description);
        }

        private Statement statement(final Statement base, final Description description) {
            return new Statement() {
                @Override
                public void evaluate() throws Throwable {
                    Throwable caughtThrowable = null;

                    // implement retry logic here
                    for (int i = 0; i < retryCount; i++) {
                        try {
                            base.evaluate();
                            return;
                        } catch (Throwable t) {
                            caughtThrowable = t;
                            System.err.println(description.getDisplayName() + ": run " + (i+1) + " failed");
                        }
                    }
                    System.err.println(description.getDisplayName() + ": giving up after " + retryCount + " failures");
                    throw caughtThrowable;
                }
            };
        }
    }

    @Rule
    public Retry retry = new Retry(2);


    public static void initBase() throws Exception {

        if(!PropsReader.getPropValuesForEnv("location_prefix").contains("qa_") &&
                !PropsReader.getPropValuesForEnv("location_prefix").contains("hotfix_")&&
                !PropsReader.getPropValuesForEnv("location_prefix").contains("")) {

            EnvProperties.getInstance().setIsNewApplication(Boolean.valueOf(PropsReader.getPropValuesForEnv("is.new.application")).booleanValue());
            isNewApplication = EnvProperties.getInstance().isNewApplication();
        }
        initDB();

    }

    /**
     * Init for Sanity test of Hub2 or any test taht need to set isNewApplication = false
     * @param isNew flag to set to isNewApplication field
     */
    public static void initBase(boolean isNew) throws Exception {

        EnvProperties.getInstance().setIsNewApplication(Boolean.valueOf(isNew));
        isNewApplication = EnvProperties.getInstance().isNewApplication();

        initDB();
    }

    private static void initDB() throws Exception {
        if (isNewApplication) { // need to wait only if we run with new App
            //sleep for a random timeout to make sure the test will run not at the same time
            int millsec = getRandomInt();
            log4j.info("Going to sleep for " + millsec);
            Thread.sleep(millsec);
         }
        dataStore.setUp();
        sqlDB.setUp();
        sqlDB.openConnection();
        mongoDB.setUp();
    }

    public static int getRandomInt() {


        return random.nextInt((300000 - 60000) + 10000) + 10000;
    }

    public static void initSuite(boolean isSuiteRunning) {
        suiteRunning = isSuiteRunning;
    }


    protected static void setServerFlags()  throws IOException{
        serverHelper.updateBusinessBackend("GiveConsentOnImport",PropsReader.getPropValuesForEnv("GiveConsentOnImport"));
        serverHelper.updateConsentFlag(Boolean.valueOf(PropsReader.getPropValuesForEnv(Consent.REGISTER_MEMBERS_BY_CONSENT)).booleanValue());
    }

    public static void cleanBase() {
        dataStore.tearDown();
        sqlDB.tearDown();
        mongoDB.tearDown();
    }



    @Before
    public void initTest(){
        this.objectsToClean = new ArrayList();
    }

    @After
    public void clean(){
        this.objectsToClean.forEach(cleanable -> {
            try {
                cleanable.clean(apiKey);
            } catch (Exception e) {
                Assert.fail("failed to clean " + cleanable);
            }
        });
    }


}
