package common;

import com.como.rc.ReadOnlyDatastoreService;
import com.google.appengine.api.NamespaceManager;
import com.google.appengine.api.datastore.*;
import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.appengine.tools.remoteapi.RemoteApiException;
import com.google.appengine.tools.remoteapi.RemoteApiInstaller;
import com.google.appengine.tools.remoteapi.RemoteApiOptions;
import org.apache.log4j.Logger;
import org.junit.Assert;
import utils.PropsReader;

import java.io.IOException;
import java.util.List;

/**
 * Created by gili on 12/7/16.
 */
public class LocalDatastore {
    private final LocalServiceTestHelper helper =
            new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
    RemoteApiInstaller remoteApiInstaller = new RemoteApiInstaller();
    DatastoreService dataStoreService;
    private DatastoreService dataStoreServiceForUpdate;
    private static boolean remoteApiIsInstalled = false;

    private static final Logger log4j = Logger.getLogger(LocalDatastore.class);

    private final int DB_ACCESS_RETRY_COUNT = 10;

    public void setUp() throws IOException {

        if(!remoteApiIsInstalled) {
            helper.setUp();
            RemoteApiOptions remoteApiOptions = new RemoteApiOptions();
            remoteApiOptions.server(
                    PropsReader.getPropValuesForEnv("server.host"),
                    Integer.valueOf(PropsReader.getPropValuesForEnv("server.port")));
            remoteApiOptions.useServiceAccountCredential(
                    PropsReader.getPropValuesForEnv("auth.service_account_id"),
                    PropsReader.getPropValuesForEnv("auth.certificate"));

            log4j.info("server.host: " + PropsReader.getPropValuesForEnv("server.host"));
            log4j.info("auth.service_account_id: " + PropsReader.getPropValuesForEnv("auth.service_account_id"));
            log4j.info("Namespace: " + PropsReader.getPropValuesForEnv("namespace"));
            remoteApiInstaller.install(remoteApiOptions);
            NamespaceManager.set(PropsReader.getPropValuesForEnv("namespace"));
            dataStoreService = new ReadOnlyDatastoreService(DatastoreServiceFactory.getDatastoreService());// Instantiates a client
            dataStoreServiceForUpdate = DatastoreServiceFactory.getDatastoreService();
            remoteApiIsInstalled = true;
        }
    }

    public void tearDown() {

        remoteApiInstaller.uninstall();
        helper.tearDown();
        remoteApiIsInstalled = false;
    }


    public void update (Entity e, String property, Object newValue) {

        e.setProperty(property,newValue);
        dataStoreServiceForUpdate.put(e);

    }


    public Entity findOneByKey(Key key){
        int retryCount = DB_ACCESS_RETRY_COUNT;
        log4j.info("find key: " + key.getId());
        RemoteApiException exception = null;
        while (retryCount > 0) {
            try {
                Thread.sleep(500);
                return dataStoreService.get(key);
            }catch (EntityNotFoundException e){
                return null;
            }
            catch (RemoteApiException e){
                retryCount--;
                exception = e;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        exception.printStackTrace();
        Assert.fail("Failed to find One By Key after retry " + retryCount + " times");
        return null;
    }

    public List<Entity> findByQuery(Query query){
        int retryCount = DB_ACCESS_RETRY_COUNT;
        Exception exception = null;
        while (retryCount > 0) {
            try {
                Thread.sleep(500);
                List<Entity> res = dataStoreService.prepare(query).asList(FetchOptions.Builder.withDefaults());
                return res;
            }catch (Exception e){
                retryCount--;
                exception = e;
            }
        }
        exception.printStackTrace();
        Assert.fail("Failed to find By Query after retry " + retryCount + " times");
        return null;
    }
}
