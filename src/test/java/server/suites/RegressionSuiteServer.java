package server.suites;

import hub.base.BaseHubTest;
import hub.base.categories.serverCategories.ServerRegression;
import hub2_0.suites.RegressionSuiteHub2;
import integration.base.BaseIntegrationTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import server.base.BaseServerTest;
import server.v2_8.*;
import server.v4.CancelPaymentTest;
import server.v4.PaymentTest;
import server.v4.ServerSanityTest;

import java.io.IOException;

@RunWith(Categories.class)
@Categories.IncludeCategory( {ServerRegression.class} )
/*
this class run suites which contains server tests API of v2.8 and v4.0
 */
@Suite.SuiteClasses({
        /*****v2.8******/
        RedeemResponseDetailedCodesTest.class, //This test MUST run first in the suite - DO NOT!!! change the order
        CancelBudgetTest.class,
        CancelPurchaseTest.class,
        GenerateTemporaryTokenTest.class,
        GetMemberBenefitsTest.class,
        GetMemberDetailsTest.class,
        PayWithBudgetTest.class,
        RedeemTest.class,
        SubmitPurchaseTest.class,
        MultipleApiKeysTest.class,

        /*****v4.0******/
        PaymentTest.class,
        CancelPaymentTest.class,
        server.v4.CancelPurchaseTest.class,
        ServerSanityTest.class,
        server.v4.SubmitPurchaseTest.class,
        server.v4.MultipleApiKeysTest.class
})


public class RegressionSuiteServer {
    public static final Logger log4j = Logger.getLogger(RegressionSuiteHub2.class);

    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseServerTest.initSuite(true);
    }


    @AfterClass
    public static void tearDown(){
        log4j.info("close browser after finish all Suite tests");
        BaseServerTest.cleanBase();
    }
}
