package server.suites;

import hub.base.BaseHubTest;
import hub.base.categories.serverCategories.serverV2_8;
import hub.base.categories.serverCategories.serverV4_0;
import integration.base.BaseIntegrationTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import server.base.BaseServerTest;
import server.v4.GetBenefitsTest;

import java.io.IOException;


@RunWith(Categories.class)

@Categories.IncludeCategory( {serverV2_8.class, serverV4_0.class} )
/*
this class run suites which contains server tests API of v2.8 and v4.0
 */
@Suite.SuiteClasses({
          server.v2_8.CancelBudgetTest.class,
          server.v2_8.CancelPurchaseTest.class,
          server.v2_8.GenerateTemporaryTokenTest.class,
          server.v2_8.MultipleApiKeysTest.class,
          server.v2_8.GetMemberDetailsTest.class,
          server.v2_8.PayWithBudgetTest.class,
          server.v2_8.RedeemTest.class,

          server.v4.SubmitPurchaseTest.class,
          server.v4.CancelPurchaseTest.class,
          server.v4.CancelPaymentTest.class,
          server.v4.PaymentTest.class
        })


public class SanitySuiteServer {

    public static final Logger log4j = Logger.getLogger(SanitySuiteServer.class);

    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseServerTest.initSuite(true);
    }


    @AfterClass
    public static void tearDown(){
        log4j.info("close browser after finish all Suite tests");
        BaseServerTest.cleanBase();
    }


}




