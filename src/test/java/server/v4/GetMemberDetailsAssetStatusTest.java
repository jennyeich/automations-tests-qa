package server.v4;

import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.SmartPunchCard;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import server.common.IServerResponse;
import server.utils.V4Builder;
import server.v2_8.JoinClubResponse;
import server.v4.base.ZappServerErrorResponse;
import server.v4.common.enums.V4ConstantsLabels;
import server.v4.common.models.Asset;
import server.v4.common.models.Customer;
import server.v4.common.models.Membership;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;

import static java.lang.System.currentTimeMillis;

/**
 * Created by Jenny on 8/8/2017.
 */

public class GetMemberDetailsAssetStatusTest extends BaseIntegrationTest {

    private V4Builder v4Builder = new V4Builder(apiKey);

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }


    /***
     * Test C6081 include assets for a member without assets (should return an empty asset list) - automated
     ***/
    @Test
    public void testGMDWithMemberNoAssets ()throws Exception{

        //Create new customer
        JoinClubResponse newMember = createMember();

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        //Verify the status is Inactive
        ArrayList<Asset> assets = getMemberDetailsReturnAssets(customer, V4ConstantsLabels.ALL, "");
        Assert.assertEquals("Verify no assets are returned: " ,0, assets.size());
    }

    /***
     * Test C6008 Smart asset: Asset status - Deactivated
     ***/
    @Test
    public void testGMDWithMemberReturnDeactivated ()throws Exception{

        NewMember member = createGiftsAndSendToMember();

        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        //Verify the status is Inactive
        ArrayList<Asset> assets = getMemberDetailsReturnAssets(customer, V4ConstantsLabels.ALL,"");
        Assert.assertEquals("Verify 2 assets are returned: " ,2, checkAssets(assets,"Deactivated"));
    }

    /***
     * Test C6195	Old Asset status - Deactivated - automated
     ***/
    //@Test
    @Deprecated
    public void testGMDWithMemberReturndeactivatedStatusOldAssets ()throws Exception{

//        NewMember member = createOldGiftsAndSendToMember();
//
//        Customer customer = new Customer();
//        customer.setPhoneNumber(member.getPhoneNumber());
//        //Verify the status is Inactive
//        ArrayList<Asset> assets = getMemberDetailsReturnAssets(customer, V4ConstantsLabels.ALL,"");
//        Assert.assertEquals("Verify 1 assets are deactivated: " ,1 , checkAssets(assets,"Deactivated"));
    }

    /***
     * Test ALL
     ***/
    @Test
    public void testGMDWithMemberReturnAllAssetsSmart ()throws Exception{

        NewMember member = createGiftsAndSendToMember();

        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        //Verify the status is Inactive
        ArrayList<Asset> assets = getMemberDetailsReturnAssets(customer, V4ConstantsLabels.ALL,"");
        Assert.assertEquals("Verify 4 assets are returned: " ,4, assets.size());
    }

    /***
     * Test C6195	Old Asset status - Deactivated - automated
     ***/
    //@Test
    @Deprecated
    public void testGMDWithMemberReturnAllAssetsOld ()throws Exception{

//        NewMember member = createOldGiftsAndSendToMember();
//
//        Customer customer = new Customer();
//        customer.setPhoneNumber(member.getPhoneNumber());
//        //Verify the status is Inactive
//        ArrayList<Asset> assets = getMemberDetailsReturnAssets(customer, V4ConstantsLabels.ALL,"");
//        Assert.assertEquals("Verify 2 assets are returned: " ,2 , assets.size());
    }

    /***
     * Test C6008 Smart asset: Asset status - inactive
     ***/
    @Test
    public void testGMDWithMemberReturnInactive ()throws Exception{

        NewMember member = createGiftsAndSendToMember ();
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        //Verify the status is Inactive
        ArrayList<Asset> assets = getMemberDetailsReturnAssets(customer, V4ConstantsLabels.ASSET_STATUS_INACTIVE,"");
        Assert.assertEquals("Verify 2 inactive assets are returned: " ,2, checkNonActiveAssets(assets));
    }

    /***
     * Test 6195 Old Asset status - Deactivated - automated
     ***/
    //@Test
    @Deprecated
    public void testGMDWithMemberReturnInactiveOldGifts ()throws Exception{

//        NewMember member = createOldGiftsAndSendToMember ();
//        Customer customer = new Customer();
//        customer.setPhoneNumber(member.getPhoneNumber());
//        //Verify the status is Inactive
//        ArrayList<Asset> assets = getMemberDetailsReturnAssets(customer, V4ConstantsLabels.ASSET_STATUS_INACTIVE,"");
//        Assert.assertEquals("Verify 1 inactive assets are returned: " ,1, checkNonActiveAssets(assets));
    }


    /***
     * Test G.M.D 4.0 - Smart assets - 'Active' - There is a bug !!!
     ***/
    @Test
    public void testVerifyRedeemField ()throws Exception{

        NewMember member = createGiftsAndSendToMember ();
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        //Verify the status is Inactive
        ArrayList<Asset> assets = getMemberDetailsReturnAssets(customer, V4ConstantsLabels.ALL, V4ConstantsLabels.ASSETS_REDEEMABLE.toString());
        Assert.assertEquals("Verify 4 assets are returned: " ,4, assets.size());
        Assert.assertTrue("check the redeem field is returned: ", checkRedeemableField(assets));
    }


    /***
     * Test G.M.D 4.0 - Smart assets - 'Active' - There is a bug !!!
     ***/
    //@Test
    @Deprecated
    public void testVerifyRedeemFieldOldGifts ()throws Exception{

//        NewMember member = createOldGiftsAndSendToMember ();
//        Customer customer = new Customer();
//        customer.setPhoneNumber(member.getPhoneNumber());
//        //Verify the status is Inactive
//        ArrayList<Asset> assets = getMemberDetailsReturnAssets(customer, V4ConstantsLabels.ALL,V4ConstantsLabels.ASSETS_REDEEMABLE.toString());
//        Assert.assertEquals("Verify 2 assets are returned: " ,2, assets.size());
//        Assert.assertTrue("Check the redeem field is returned: ", checkRedeemableField(assets));

    }


    /***
     * Test C6078 -  G.M.D 4.0 - all attributes return - automated
     ***/
    @Test
    public void testVerifyAllAttributesAreReturned ()throws Exception{

        NewMember member = createGiftsAndSendToMember ();
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        //Verify the status is Inactive
        ArrayList<Asset> assets = getMemberDetailsReturnAssets(customer, V4ConstantsLabels.ALL,V4ConstantsLabels.ASSETS_REDEEMABLE.toString());
        Assert.assertTrue("check the redeem field is returned: ", checkAllFields(assets));

    }

    /***
     * Test C6211 G.M.D 4.0 - all attributes return
     ***/
    //@Test
    @Deprecated
    public void testVerifyAllAttributesAreReturnedOldGifts ()throws Exception{

//        NewMember member = createOldGiftsAndSendToMember ();
//        Customer customer = new Customer();
//        customer.setPhoneNumber(member.getPhoneNumber());
//        //Verify the status is Inactive
//        ArrayList<Asset> assets = getMemberDetailsReturnAssets(customer, V4ConstantsLabels.ALL,V4ConstantsLabels.ASSETS_REDEEMABLE.toString());
//        Assert.assertTrue("check the redeem field is returned: ", checkAllFields(assets));

    }

    protected JoinClubResponse createMember() throws Exception{

        JoinClubResponse newMember = serverHelper.createMember();

        return newMember;
    }


    private NewMember createGiftsAndSendToMember () throws Exception{

        //create gifts and punch cards
        SmartGift gift1 = hubAssetsCreator.createSmartGiftWithAutoGenerateCodes(String.valueOf("Smart_" + currentTimeMillis()));
        SmartGift gift2 = hubAssetsCreator.createSmartGift(String.valueOf("Smart_" + currentTimeMillis()));
        SmartPunchCard punchCard = hubAssetsCreator.createSmartPunchCard(String.valueOf(currentTimeMillis()),"10");
        SmartPunchCard punchCard1 = hubAssetsCreator.createSmartPunchCard(String.valueOf(currentTimeMillis()),"10");

        NewMember member = hubMemberCreator.createMember(AUTO_LAST_PREFIX,AUTO_LAST_PREFIX);

        hubAssetsCreator.performAnActionSendGift(gift1);
        hubAssetsCreator.performAnActionSendGift(gift2);
        hubAssetsCreator.performAnActionSendSmartPunch(punchCard);
        hubAssetsCreator.performAnActionSendSmartPunch(punchCard1);

        hubAssetsCreator.deactivateSmartgift(gift1.getTitle());
        hubAssetsCreator.deactivateSmartPunchCard(punchCard1.getTitle());

        return member;
    }





    public boolean checkRedeemableField(ArrayList<Asset> assets){

        boolean isRedeemable = false;
        for (int i =0 ; i< assets.size(); i++) {
            if (assets.get(i).getRedeemable() == null) {
                isRedeemable = false;
                break;
            }
            else {
                isRedeemable = true;
            }
        }
         return isRedeemable;
    }


  public boolean checkAllFields(ArrayList<Asset> assets)   {

      boolean isRedeemable = false;
      for (int i =0 ; i< assets.size(); i++) {
          isRedeemable = isAllFieldsExist(assets.get(i));
          //if isRedeemable == false - quit and return false
          if (!isRedeemable)
              return isRedeemable;
      }

      return isRedeemable;
  }


  public boolean isAllFieldsExist (Asset asset) {

      boolean isExists = false;
      if (asset.getRedeemable() != null) {
          if (asset.getNonRedeemableClause()!= null)
              isExists = true;
      }
      if (asset.getName() != null)
          isExists = true;
      if (asset.getStatus() != null)
          isExists = true;
      if (asset.getDescription() != null)
          isExists = true;
      if (asset.getKey() != null)
          isExists = true;
      if (asset.getValidUntil() != null)
          isExists = true;
      if (asset.getValidFrom() != null)
          isExists = true;
      if (asset.getImage() != null)
          isExists = true;
      return isExists;
  }



    public int checkAssets(ArrayList<Asset> assets, String status){

            int count =0;
            for (int i =0 ; i< assets.size(); i++)
                if (assets.get(i).getStatus().equals(status))
                    count++;

            return count;

        }


    public int checkNonActiveAssets(ArrayList<Asset> assets){

        int count =0;
        for (int i =0 ; i< assets.size(); i++)
            if (assets.get(i).getStatus().equals("Deactivated"))
                count++;

        return count;

    }


    private ArrayList<Asset> getMemberDetailsReturnAssets (Customer customer, String returnAssets, String expand)throws Exception{

        GetMemberDetails getMemberDetails = new GetMemberDetails();

        if (!returnAssets.equals(""))
            getMemberDetails.addQueryParameter("returnAssets",returnAssets);
        if (!expand.equals(""))
            getMemberDetails.addQueryParameter("expand",expand);

        getMemberDetails.setApiKey(apiKey);
        getMemberDetails.setDefauldHeaderParams();
        getMemberDetails.setCustomer(customer);
        Thread.sleep(10000);
        IServerResponse response = getMemberDetails.SendRequestByApiKey(apiKey,GetMemberDetailsResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) response;
        Membership membership = getMemberDetailsResponse.getMembership();
        return membership.getAssets();
    }


}

