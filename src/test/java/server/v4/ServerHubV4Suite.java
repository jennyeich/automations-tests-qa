package server.v4;

import hub.base.BaseHubTest;
import hub.services.AcpService;
import hub.services.NewApplicationService;
import integration.base.BaseIntegrationTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import server.base.BaseServerTest;

import java.io.IOException;

/**
 * Created by Goni on 10/18/2017.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        VoidPurchaseTest.class,
        GetBenefitsTest.class,
        GetMemberDetailsTest.class,
        GetMemberDetailsAssetStatusTest.class,
        CustomFieldIdentificationTest.class,
        MemberNotesTest.class

})
public class ServerHubV4Suite {
    public static final Logger log4j = Logger.getLogger(ServerHubV4Suite.class);


    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseIntegrationTest.init();
    }

    @AfterClass
    public static void tearDown(){

        log4j.info("Remove application from user");
        try {
            AcpService.uncheckApplication(NewApplicationService.getEnvProperties().getLocationId());
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
        BaseServerTest.cleanBase();
        log4j.info("close browser after finish test");
        BaseHubTest.cleanHub();
    }

}

