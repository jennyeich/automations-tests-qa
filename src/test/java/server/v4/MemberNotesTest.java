package server.v4;


import hub.base.categories.hub1Categories.Hub1Regression;
import hub.base.categories.serverCategories.ServerRegression;
import hub.common.objects.member.NewMember;
import hub.common.objects.operations.GeneralPOSSettingsPage;
import hub.common.objects.operations.MemberNotes;
import hub.common.objects.operations.MemberNotesTargetFields;
import hub.common.objects.operations.MemberNotesTypeFields;
import hub.hub1_0.services.OperationService;
import integration.base.BaseIntegrationTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.common.IServerResponse;
import server.utils.V4Builder;
import server.v2_8.base.ZappServerErrorResponse;
import server.v4.common.models.Customer;
import server.v4.common.models.MemberNote;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;


/**
 * Created by Jenny on 7/12/2017.
 */
public class MemberNotesTest extends BaseIntegrationTest {

    V4Builder v4Builder = new V4Builder(apiKey);
    public static final Integer httpStatusCode200 = 200;
    public static final String AUTO_USER_PREFIX = "auto_user_";
    public static final String AUTO_LAST_PREFIX = "auto_last_";
    public static CharSequence timestamp;
    public static final String TYPE_TEXT = "text";
    public static final String TYPE_URL = "url";


    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }

    @Test
    @Category({Hub1Regression.class,ServerRegression.class})
    public void testPOSandTextMemberNotes1() throws Exception{

        OperationService.cleanMemberNotes();
        timestamp = String.valueOf(System.currentTimeMillis());

        String noteText = timestamp +" note text 123";
        String noteTarget = MemberNotesTargetFields.POS.toString();
        String noteType =MemberNotesTypeFields.TEXT.toString();

        createMemberNotes(noteText, noteTarget, noteType);
        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        GetMemberDetailsResponse getMemberDetailsResponse = getMemberDetails(createCustomer(member.getPhoneNumber()));
        checkMemberNote(getMemberDetailsResponse.getMemberNotes(), noteText, noteTarget, TYPE_TEXT);

        }

    @Test
    public void testCreateFewMemberNoteMessages () throws Exception {

        OperationService.cleanMemberNotes();
        timestamp = String.valueOf(System.currentTimeMillis());
        String noteText = timestamp +" note text 123";
        String noteTarget = MemberNotesTargetFields.POS.toString();
        String noteType =MemberNotesTypeFields.TEXT.toString();

        ArrayList <MemberNotes> memberNotes = createMemberNotesFewMemberNotes(noteText, noteTarget, noteType, 3);
        timestamp = String.valueOf(System.currentTimeMillis());

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        GetMemberDetailsResponse getMemberDetailsResponse = getMemberDetails(createCustomer(member.getPhoneNumber()));
        checkMemberNoteArray(getMemberDetailsResponse.getMemberNotes(), memberNotes);
    }


    @Test
    public void testReceiptAndTextMemberNotesNegative2() throws Exception{

        OperationService.cleanMemberNotes();
        timestamp = String.valueOf(System.currentTimeMillis());
        String noteText = timestamp +" note text 123";
        String noteTarget = MemberNotesTargetFields.RECEIPT.toString();
        String noteType =MemberNotesTypeFields.TEXT.toString();

        createMemberNotes(noteText, noteTarget, noteType);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        GetMemberDetailsResponse getMemberDetailsResponse = getMemberDetails(createCustomer(member.getPhoneNumber()));

        checkMemberNoteNegative(getMemberDetailsResponse.getMemberNotes(), noteText, noteTarget, TYPE_TEXT);

    }


    @Test
    public void testPOSandTextMemberNotesSpecialCharacters3() throws Exception{

        OperationService.cleanMemberNotes();
        timestamp = String.valueOf(System.currentTimeMillis());
        String noteText = timestamp +" !@#$%^&*()_+<>/[]'|<>Q/#@$%^&*()(*^&Y%^$%#$$%^&*FDSADflkgjdflmv,cx.mvfrjdgtretjo";
        String noteTarget = MemberNotesTargetFields.POS.toString();
        String noteType =MemberNotesTypeFields.TEXT.toString();

        createMemberNotes(noteText, noteTarget, noteType);
        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        GetMemberDetailsResponse getMemberDetailsResponse = getMemberDetails(createCustomer(member.getPhoneNumber()));

        checkMemberNote(getMemberDetailsResponse.getMemberNotes(), noteText, noteTarget, TYPE_TEXT);

    }

    @Test
    public void testReceiptAndTextMemberNotes4() throws Exception{

        OperationService.cleanMemberNotes();
        timestamp = String.valueOf(System.currentTimeMillis());
        String noteText = timestamp +" note text 123";
        String noteTarget = MemberNotesTargetFields.RECEIPT.toString();
        String noteType =MemberNotesTypeFields.TEXT.toString();

        createMemberNotes(noteText, noteTarget, noteType);
        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchase(createCustomer(member.getPhoneNumber()));

        checkMemberNote(submitPurchaseResponse.getMemberNotes(), noteText, noteTarget, TYPE_TEXT);

    }


    @Test
    public void testPOSAndTextMemberNotesNegative5() throws Exception{
        OperationService.cleanMemberNotes();
        timestamp = String.valueOf(System.currentTimeMillis());
        String noteText = timestamp +" note text 123";
        String noteTarget = MemberNotesTargetFields.POS.toString();
        String noteType =MemberNotesTypeFields.TEXT.toString();

        createMemberNotes(noteText, noteTarget, noteType);
        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchase(createCustomer(member.getPhoneNumber()));

        checkMemberNoteNegative(submitPurchaseResponse.getMemberNotes(), noteText, noteTarget, TYPE_TEXT);

    }
    @Test
    public void testReceiptAndTextMemberNotesSpecialCharacters6() throws Exception{

        OperationService.cleanMemberNotes();
        timestamp = String.valueOf(System.currentTimeMillis());
        String noteText = timestamp +" !@#$%^&*()_+<>/[]'|<>Q/#@$%^&*()(*^&Y%^$%#$$%^&*FDSADflkgjdflmv,cx.mvfrjdgtretjo";
        String noteTarget = MemberNotesTargetFields.RECEIPT.toString();
        String noteType =MemberNotesTypeFields.TEXT.toString();

        createMemberNotes(noteText, noteTarget, noteType);
        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchase(createCustomer(member.getPhoneNumber()));

        checkMemberNote(submitPurchaseResponse.getMemberNotes(), noteText, noteTarget, TYPE_TEXT);

    }

    @Test
    public void testPOSandUrlMemberNotes7() throws Exception{

        OperationService.cleanMemberNotes();

        timestamp = String.valueOf(System.currentTimeMillis());
        String noteText = timestamp +"http://google.com";
        String noteTarget = MemberNotesTargetFields.POS.toString();
        String noteType =MemberNotesTypeFields.URL.toString();

        createMemberNotes(noteText, noteTarget, noteType);
        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        GetMemberDetailsResponse getMemberDetailsResponse = getMemberDetails(createCustomer(member.getPhoneNumber()));

        checkMemberNote(getMemberDetailsResponse.getMemberNotes(), noteText, noteTarget, TYPE_URL);

    }

    @Test
    public void testReceiptAndURLMemberNotes8() throws Exception{

        OperationService.cleanMemberNotes();
        timestamp = String.valueOf(System.currentTimeMillis());
        String noteText = timestamp +" note text 123";
        String noteTarget = MemberNotesTargetFields.RECEIPT.toString();
        String noteType =MemberNotesTypeFields.URL.toString();

        createMemberNotes(noteText, noteTarget, noteType);
        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchase(createCustomer(member.getPhoneNumber()));

        checkMemberNote(submitPurchaseResponse.getMemberNotes(), noteText, noteTarget, TYPE_URL);

    }
    @Test
    public void testReceiptAndURLMemberNotesTestMembershipAttributes9() throws Exception{

        OperationService.cleanMemberNotes();
        timestamp = String.valueOf(System.currentTimeMillis());
        String noteText = " %$firstName% thank for coffee and cake";
        String noteTarget = MemberNotesTargetFields.RECEIPT.toString();
        String noteType =MemberNotesTypeFields.URL.toString();

        createMemberNotes(noteText, noteTarget, noteType);
        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchase(createCustomer(member.getPhoneNumber()));

        checkMemberNote(submitPurchaseResponse.getMemberNotes(),member.getFirstName() + " thank for coffee and cake" , noteTarget, TYPE_URL);

    }

    @Test
    public void testReceiptAndURLMemberNotesTestMembershipAttributes10() throws Exception{

        OperationService.cleanMemberNotes();
        timestamp = String.valueOf(System.currentTimeMillis());
        String noteText = " %$phoneNumber% thank for coffee and cake";
        String noteTarget = MemberNotesTargetFields.RECEIPT.toString();
        String noteType =MemberNotesTypeFields.URL.toString();

        createMemberNotes(noteText, noteTarget, noteType);
        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchase(createCustomer(member.getPhoneNumber()));

        checkMemberNote(submitPurchaseResponse.getMemberNotes(),member.getPhoneNumber() + " thank for coffee and cake" , noteTarget, TYPE_URL);

    }


    @Test
    @Category({Hub1Regression.class,ServerRegression.class})
    public void testReceiptAndURLMemberNotesTestMembershipAttributes11() throws Exception{

        OperationService.cleanMemberNotes();
        timestamp = String.valueOf(System.currentTimeMillis());
        String noteText = " %$lastName% thank for coffee and cake";
        String noteTarget = MemberNotesTargetFields.RECEIPT.toString();
        String noteType =MemberNotesTypeFields.URL.toString();

        createMemberNotes(noteText, noteTarget, noteType);
        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchase(createCustomer(member.getPhoneNumber()));

        checkMemberNote(submitPurchaseResponse.getMemberNotes(),member.getLastName() + " thank for coffee and cake" , noteTarget, TYPE_URL);

    }

    @Test
    @Category({Hub1Regression.class,ServerRegression.class})
    public void testCreateFewMemberNoteMessagesSubmitPurchase () throws Exception {

        OperationService.cleanMemberNotes();
        timestamp = String.valueOf(System.currentTimeMillis());
        String noteText = timestamp +" note text 123";
        String noteTarget = MemberNotesTargetFields.RECEIPT.toString();
        String noteType =MemberNotesTypeFields.TEXT.toString();

        ArrayList <MemberNotes> memberNotes = createMemberNotesFewMemberNotes(noteText, noteTarget, noteType, 4);
        timestamp = String.valueOf(System.currentTimeMillis());

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchase(createCustomer(member.getPhoneNumber()));
        checkMemberNoteArray(submitPurchaseResponse.getMemberNotes(), memberNotes);
    }


    private void checkMemberNote (ArrayList<MemberNote> memberNotes, String noteText, String noteTarget, String noteType){

        int size = memberNotes.size();
        for (int i =0; i< size; i++) {

            MemberNote memberNote = memberNotes.get(i);
            Assert.assertEquals(noteText,memberNote.getContent());
            Assert.assertEquals(noteType,memberNote.getNoteType());
           // Assert.assertEquals(noteTarget,memberNote.getNoteTargetType());
        }
    }

    private void checkMemberNoteArray (ArrayList<MemberNote> memberNotes, ArrayList<MemberNotes> expectedNotes){

        String expectedType;
        int size = memberNotes.size();
        for (int i =0; i< size; i++) {

            MemberNote memberNote = memberNotes.get(i);
            MemberNotes expectedMemberNotes = expectedNotes.get(i);

            Assert.assertEquals(expectedMemberNotes.getMemberNotes(),memberNote.getContent());
            if (expectedMemberNotes.getType().equals("Text"))
                expectedType = "text";
            else
                expectedType = "url";

            Assert.assertEquals(expectedType,memberNote.getNoteType());
        }
    }

    private void checkMemberNoteNegative (ArrayList<MemberNote> memberNotes, String noteText, String noteTarget, String noteType){

        int size = memberNotes.size();
        for (int i =0; i< size; i++) {

            MemberNote memberNote = memberNotes.get(i);

            Assert.assertNotEquals(memberNote.getContent(), noteText);
            Assert.assertNotEquals(memberNote.getNoteType(), noteTarget);
           // Assert.assertNotEquals(memberNote.getNoteTargetType(), noteType);

        }
    }
    private Customer createCustomer (String phoneNumber){

        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        return customer;
    }


    public void createMemberNotes (String noteText,String target, String type) throws Exception {

        GeneralPOSSettingsPage generalPOSSettingsPage = new GeneralPOSSettingsPage();
        MemberNotes memberNotes = new MemberNotes();
        memberNotes.setMemberNotes(noteText);
        memberNotes.setTarget(target);
        memberNotes.setType(type);
        generalPOSSettingsPage.addMemberNotes(memberNotes,generalPOSSettingsPage);
        OperationService.updatePOSSettings(generalPOSSettingsPage);
}



    public ArrayList<MemberNotes> createMemberNotesFewMemberNotes (String noteText,String target, String type, int counter) throws Exception {

        GeneralPOSSettingsPage generalPOSSettingsPage = new GeneralPOSSettingsPage();
        MemberNotes memberNotes;
        ArrayList<MemberNotes> memberNotes1 = new ArrayList<>(10);


        for (int i=0; i< counter; i++) {

            memberNotes = new MemberNotes();
            memberNotes.setMemberNotes(noteText +  " " + i);
            memberNotes.setTarget(target);
            memberNotes.setType(type);
            generalPOSSettingsPage.addMemberNotes(memberNotes, generalPOSSettingsPage);
            memberNotes1.add(memberNotes);
        }
        OperationService.updatePOSSettings(generalPOSSettingsPage);
        return  memberNotes1;
    }



    private GetMemberDetailsResponse getMemberDetails (Customer customer){

        GetMemberDetails memberDetails = v4Builder.buildMemberDetailsRequest(customer);
        IServerResponse response = v4Builder.getGetMemberDetailsResponse(memberDetails);
        if(response instanceof ZappServerErrorResponse)
            Assert.fail("Expected an error!");

       //Verify the returned customer is as expected
        Assert.assertEquals("",customer.getPhoneNumber(),((GetMemberDetailsResponse)response).getMembership().getPhoneNumber());
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) response;

        return getMemberDetailsResponse;
    }

    private SubmitPurchaseResponse submitPurchase (Customer customer){

        IServerResponse response = v4Builder.submitPurchaseResponse(customer);
        if(response instanceof ZappServerErrorResponse)
            Assert.fail("Expected an error!");

        //Verify the returned customer is as expected
        Assert.assertNotNull(((SubmitPurchaseResponse)response).getConfirmation());
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;

        return submitPurchaseResponse;
    }

    @After
    public void clearMemberNotes () throws Exception{
        OperationService.cleanMemberNotes();
    }

}
