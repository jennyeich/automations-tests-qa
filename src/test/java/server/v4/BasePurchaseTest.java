package server.v4;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import server.common.IServerResponse;
import server.utils.PosErrorsEnum;
import server.v4.common.models.Customer;
import server.v4.common.models.Deal;
import server.v4.common.models.Purchase;
import server.v4.common.models.RedeemAsset;

import java.util.ArrayList;
import java.util.List;


public class BasePurchaseTest extends server.base.BasePurchaseTest {


    protected SubmitPurchase submitPurchaseRequest (ArrayList<Customer> customers, ArrayList<RedeemAsset> redeemAssets, ArrayList<Deal> deals, Purchase purchase) throws Exception{

        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setDefauldHeaderParams();
        submitPurchase.setCustomers(customers);


        if (redeemAssets.size() > 0)
            submitPurchase.setRedeemAssets(redeemAssets);
        if (deals.size() > 0)
            submitPurchase.setDeals(deals);

        submitPurchase.setPurchase(purchase);

        //save the transaction id for tests
        transactionId = submitPurchase.getPurchase().getTransactionId();

        return submitPurchase;

    }

    protected IServerResponse submitPurchaseResponse (SubmitPurchase submitPurchase){

        IServerResponse response =  (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
        if(!(((SubmitPurchaseResponse) response).getStatus().equals("ok")))
            Assert.fail(getError(response).get(MESSAGE).toString());
        //Verify the returned customer is as expected
        Assert.assertNotNull("Verify the confirmation is not null",((SubmitPurchaseResponse)response).getConfirmation());

        return response;
    }

    protected IServerResponse submitPurchaseResponseOpenStatus (SubmitPurchase submitPurchase){

        IServerResponse response =  (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
        if(!(((SubmitPurchaseResponse) response).getStatus().equals("ok")))
            Assert.fail(getError(response).get(MESSAGE).toString());
        //Verify the returned customer is as expected
        Assert.assertNull("Verify the confirmation is null",((SubmitPurchaseResponse)response).getConfirmation());

        return response;
    }

    protected String submitPurchaseResponseError (SubmitPurchase submitPurchase){

        IServerResponse response =  (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
        if(((SubmitPurchaseResponse) response).getStatus().equals("ok"))
            Assert.fail("Expected an error!");
        //Verify the returned customer is as expected
        Assert.assertNull("Verify the confirmation is null",((SubmitPurchaseResponse)response).getConfirmation());
        Assert.assertTrue("Response must be an error", ((SubmitPurchaseResponse) response).getStatus().equals("error"));

        return getError(response).get(MESSAGE).toString();
    }

    protected List<Entity> verifyPurchaseWithDataStore(SubmitPurchase submitPurchase, int numberOfPurchases) throws InterruptedException {
        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, transactionId));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + transactionId, q, numberOfPurchases, dataStore);

        for (int i=0; i<numberOfPurchases; i++) {
            serverHelper.validatePurchaseResult(submitPurchase, result.get(i));
        }

        return result;
    }

    protected void validateErrorMessage(PosErrorsEnum errorCode, IServerResponse response) {
        log4j.info("Validate error code: " + errorCode.getCode());
        String errorMessage = getError(response).get(MESSAGE).toString();
        Assert.assertTrue(errorCode.isValidMessageForCode(errorMessage));
    }

    protected JSONObject getError(IServerResponse response){
        return ((JSONObject)((JSONArray)(response.getRawObject().get(ERRORS))).get(0));
    }



    protected Query getQueryForPurchase() {
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, transactionId));
        return q;
    }

}
