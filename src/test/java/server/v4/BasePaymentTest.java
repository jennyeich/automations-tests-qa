package server.v4;

import com.google.appengine.api.datastore.Query;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.base.BaseServerTest;
import server.common.IServerResponse;
import server.common.LocationBackendFields;
import server.common.PaymentType;
import server.utils.PosErrorsEnum;
import server.utils.V4Builder;
import server.v2_8.RegisterResponse;
import server.v4.base.ZappServerErrorResponse;
import server.v4.common.enums.Mode;
import server.v4.common.enums.PaymentMethod;
import server.v4.common.models.Customer;
import server.v4.common.models.PaymentProviderConfigurations;
import server.v4.common.models.PaymentQueryParams;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;

import static java.lang.System.currentTimeMillis;

/**
 * Created by Goni on 7/24/2017.
 */

public class BasePaymentTest extends BaseServerTest {

    public final static String STATUS_OK = "ok";
    public final String ERRORS = "errors";
    public final String CODE = "code";
    public final String MESSAGE = "message";

    protected V4Builder v4Builder = new V4Builder(apiKey);

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
        serverHelper.updateBusinessBackend("UniqueFields", Lists.newArrayList("GovID","PhoneNumber","MemberID").toArray());
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
           setPaymentConfigurationDefaults();
        }

    };

    private void setPaymentConfigurationDefaults() {
        setDefaultConfigurationForTest(PaymentType.POINTS.type());
        serverHelper.updateBusinessBackend(LocationBackendFields.DECIMAL_MODE.key(),true);
    }

    public Query buildQuery (String queryName, List<Query.Filter> predicateList) {
        Query q = new Query(queryName);
        if(predicateList.size()>1){
            q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, predicateList));
        }else{
            q.setFilter(predicateList.get(0));
        }
        return q;
    }

    protected void validateErrorMessage(PosErrorsEnum errorCode,IServerResponse response) {
        log4j.info("Validate error code: " + errorCode.getCode());
        String errorMessage = getError(response).get(MESSAGE).toString();
        Assert.assertTrue("Error is not as expected: expect: " + errorCode.getMessage() + ", actual: " + errorMessage,errorCode.isValidMessageForCode(errorMessage));
    }

    protected JSONObject getError(IServerResponse response){
        return ((JSONObject)((JSONArray)(response.getRawObject().get(ERRORS))).get(0));
    }

    public server.v4.common.models.Customer createMemberWithAllIdentifiersWithPoints(String amountOfPoints, String govId, String memberId) throws Exception {
        long currentTimeMillis = currentTimeMillis();
        String customerPhone = String.valueOf(currentTimeMillis);
        RegisterResponse newMember = serverHelper.createMemberWithAllIdentifiers(customerPhone,govId,memberId,updateConfiguration);

        server.v4.common.models.Customer customer = new server.v4.common.models.Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        serverHelper.addPointsToMember(amountOfPoints, newMember.getMembershipKey(),newMember.getUserKey());
        serverHelper.checkPointsTransaction(newMember.getUserKey(),dataStore);
        return customer;
    }

    protected Customer createMemberWithPoints() throws Exception {
        RegisterResponse newMember = serverHelper.createMemberWithPoints("10000");
        server.v4.common.models.Customer customer = new server.v4.common.models.Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        serverHelper.checkPointsTransaction(newMember.getUserKey(),dataStore);
        return customer;
    }

    protected Customer createMemberWithPoints(String amountOfPoints) throws Exception {
        RegisterResponse newMember = serverHelper.createMemberWithPoints(amountOfPoints);
        server.v4.common.models.Customer customer = new server.v4.common.models.Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        if(!amountOfPoints.equals("0"))
            serverHelper.checkPointsTransaction(newMember.getUserKey(),dataStore);
        return customer;
    }

    protected Customer createMemberWithBudget() throws Exception {
        RegisterResponse newMember = serverHelper.createMemberWithBudget("10000");
        server.v4.common.models.Customer customer = new server.v4.common.models.Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        serverHelper.checkPointsTransaction(newMember.getUserKey(),dataStore);
        return customer;
    }

    protected Customer createMemberWithBudget(String amountOfPoints) throws Exception {
        RegisterResponse newMember = serverHelper.createMemberWithBudget(amountOfPoints);
        server.v4.common.models.Customer customer = new server.v4.common.models.Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        if(!amountOfPoints.equals("0"))
            serverHelper.checkPointsTransaction(newMember.getUserKey(),dataStore);
        return customer;
    }



    protected void setPaymentProviderConfiguration(String paymentMethod,boolean allowPartialPayment) {

        PaymentProviderConfigurations providerConfigurations = v4Builder.buildPaymentProviderConfigurations(paymentMethod,allowPartialPayment);
        serverHelper.saveApiClient("PaymentProviderConfigurations" ,providerConfigurations);
    }

    protected void setDefaultConfigurationForTest(String PayWithBudgetType) {
        setConfigurationForTest("false",PayWithBudgetType,"1","false");
    }

    protected void setConfigurationForTest(String PaymentRequiresVerificationCode,String PayWithBudgetType,String PayWithBudgetRatio,String allowNegativePointBalance) {

        serverHelper.setConfigurationForPayment(PaymentRequiresVerificationCode,PayWithBudgetType,PayWithBudgetRatio,allowNegativePointBalance);

    }

    protected PaymentAPIResponse getPaymentAPIResponse(PaymentAPI payment) {
        // Send the request
        log4j.info("Send payment request");
        IServerResponse response = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }
        if(payment.getRequestParams().containsValue(Mode.PAY)) {
            Assert.assertFalse("Payment UID must not be empty!", StringUtils.isEmpty(((PaymentAPIResponse) response).getConfirmation()));
        }
        Assert.assertEquals("status code must be 200", (Integer) 200, response.getStatusCode());
        Assert.assertEquals("Status must be ok", STATUS_OK, ((PaymentAPIResponse) response).getStatus());
        return (PaymentAPIResponse) response;
    }

    protected PaymentQueryParams getPaymentQueryParams(PaymentMethod paymentMethod, Mode mode) {
        PaymentQueryParams queryParams = new PaymentQueryParams();
        queryParams.setAllowedPaymentMethod(paymentMethod);
        queryParams.setMode(mode);
        return queryParams;
    }

    protected PaymentQueryParams getDefaultPaymentQueryParams(PaymentMethod paymentMethod ) {
        PaymentQueryParams queryParams = new PaymentQueryParams();
        queryParams.setAllowedPaymentMethod(paymentMethod);
        queryParams.setMode(Mode.PAY);
        return queryParams;
    }

}

