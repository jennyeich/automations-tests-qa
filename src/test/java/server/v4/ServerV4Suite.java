package server.v4;

import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import server.base.BaseServerTest;

import java.io.IOException;
/**
 * Created by Goni on 7/7/2017.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        RedeemResponseDetailedCodesTest.class, //This test MUST run first in the suite - DO NOT!!! change the order
        PaymentTest.class,
        CancelPaymentTest.class,
        CancelPurchaseTest.class,
        ServerSanityTest.class,
        SubmitPurchaseTest.class,
        MultipleApiKeysTest.class

})
public class ServerV4Suite {
    public static final Logger log4j = Logger.getLogger(ServerV4Suite.class);


    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseServerTest.init();
    }

    @AfterClass
    public static void tearDown(){

        BaseServerTest.cleanBase();
    }

}
