package server.v4;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.serverCategories.ServerRegression;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.base.BaseServerTest;
import server.common.IServerResponse;
import server.v2_8.RegisterResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v4.common.models.Customer;
import server.v4.common.models.Purchase;
import server.v4.common.models.PurchaseItem;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.UUID;

import static java.lang.System.currentTimeMillis;


/**
 * Created by Jenny on 7/12/2017.
 */
public class ServerSanityTest extends BaseServerTest{

    public static CharSequence timestamp;

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }



    @Test
    @Category({ServerRegression.class})
    public void testItemCodeIsRequired() throws Exception{

        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        Customer customer = createNewMember();
        PurchaseItem purchaseItem1 = buildPurchaseItemNoCode(500,1);
        Integer submitPurchaseResponse = submitPurchase(customer,purchaseItem1);
        Assert.assertEquals("Verify the code is 422", Integer.valueOf(422) , submitPurchaseResponse);
    }


    @Test
    public void testItemNameIsRequired() throws Exception{

        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        Customer customer = createNewMember();
        PurchaseItem purchaseItem1 = buildPurchaseItemNoName(500,1);
        Integer submitPurchaseResponse = submitPurchase(customer,purchaseItem1);
        Assert.assertEquals("Verify the code is 422", Integer.valueOf(422) , submitPurchaseResponse);
    }

    @Test
    public void testDepCodeIsRequired() throws Exception{

        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        Customer customer = createNewMember();
        PurchaseItem purchaseItem1 = buildPurchaseItemNoDepCode(500,1);
        Integer submitPurchaseResponse = submitPurchase(customer,purchaseItem1);
        Assert.assertEquals("Verify the code is 422", Integer.valueOf(422) , submitPurchaseResponse);
    }

    @Test
    public void testDepNameIsRequired() throws Exception{

        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        Customer customer = createNewMember();
        PurchaseItem purchaseItem1 = buildPurchaseItemNoDepName(500,1);
        Integer submitPurchaseResponse = submitPurchase(customer,purchaseItem1);
        Assert.assertEquals("Verify the code is 422", Integer.valueOf(422) , submitPurchaseResponse);
    }

    @Test
    public void testQuantityIsRequired() throws Exception{

        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        Customer customer = createNewMember();
        PurchaseItem purchaseItem1 = buildPurchaseItemNoQuantity(500);
        Integer submitPurchaseResponse = submitPurchase(customer,purchaseItem1);
        Assert.assertEquals("Verify the code is 422", Integer.valueOf(422) , submitPurchaseResponse);
    }

    @Test
    public void testGrossAmountIsRequired() throws Exception{

        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        Customer customer = createNewMember();
        PurchaseItem purchaseItem1 = buildPurchaseItemNoGrossAmount(500,1);
        Integer submitPurchaseResponse = submitPurchase(customer,purchaseItem1);
        Assert.assertEquals("Verify the code is 422", Integer.valueOf(422) , submitPurchaseResponse);
    }

    @Test
    @Category({ServerRegression.class})
    public void testNetAmountIsRequired() throws Exception{

        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        Customer customer = createNewMember();
        PurchaseItem purchaseItem1 = buildPurchaseItemNoNetAmount(1);
        Integer submitPurchaseResponse = submitPurchase(customer,purchaseItem1);
        Assert.assertEquals("Verify the code is 422", Integer.valueOf(422) , submitPurchaseResponse);
    }

    @Test
    public void testPOSIDIsRequired() throws Exception{

        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        Customer customer = createNewMember();
        PurchaseItem purchaseItem1 = buildPurchaseItem(500,1);
        Integer submitPurchaseResponse = submitPurchaseNOPOS(customer,purchaseItem1);
        Assert.assertEquals("Verify the code is 422", Integer.valueOf(422) , submitPurchaseResponse);
    }

    @Test
    public void testBranchIDIsRequired() throws Exception{

        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        Customer customer = createNewMember();
        PurchaseItem purchaseItem1 = buildPurchaseItem(500,1);
        Integer submitPurchaseResponse = submitPurchaseNOBranchID(customer,purchaseItem1);
        Assert.assertEquals("Verify the code is 422", Integer.valueOf(422) , submitPurchaseResponse);
    }

    @Test
    @Category({ServerRegression.class})
    public void testTransactionIDIsRequired() throws Exception{

        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        Customer customer = createNewMember();
        PurchaseItem purchaseItem1 = buildPurchaseItem(500,1);
        Integer submitPurchaseResponse = submitPurchaseNotransactionID(customer,purchaseItem1);
        Assert.assertEquals("Verify the code is 422", Integer.valueOf(422) , submitPurchaseResponse);
    }

    @Test
    @Category({ServerRegression.class})
    public void testTotalAmountIsRequired() throws Exception{

        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        Customer customer = createNewMember();
        PurchaseItem purchaseItem1 = buildPurchaseItem(500,1);
        Integer submitPurchaseResponse = submitPurchaseNoTotalAmount(customer,purchaseItem1);
        Assert.assertEquals("Verify the code is 422", Integer.valueOf(422) , submitPurchaseResponse);
    }

    @Test
    public void testOpenTimeIsRequired() throws Exception{

        timestamp = String.valueOf(System.currentTimeMillis());
        //Create member
        Customer customer = createNewMember();
        PurchaseItem purchaseItem1 = buildPurchaseItem(500,1);
        Integer submitPurchaseResponse = submitPurchaseNoOpenTime(customer,purchaseItem1);
        Assert.assertEquals("Verify the code is 422", Integer.valueOf(422) , submitPurchaseResponse);
    }

    public Customer createNewMember() throws Exception {

        long currentTimeMillis = currentTimeMillis();
        String customerPhone = String.valueOf(currentTimeMillis);
        RegisterResponse newMember = serverHelper.createMemberWithAllIdentifiers(customerPhone,null,null,updateConfiguration);
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        return customer;
    }


    private Integer submitPurchase (Customer customer, PurchaseItem purchaseItem1){

        IServerResponse response = submitPurchaseResponse(customer,purchaseItem1);
        if(response instanceof ZappServerErrorResponse)
            return 0;

        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        return submitPurchaseResponse.getStatusCode();
    }

    private Integer submitPurchaseNOPOS (Customer customer, PurchaseItem purchaseItem1){

        IServerResponse response = submitPurchaseResponseNOPOS(customer,purchaseItem1);
        if(response instanceof ZappServerErrorResponse)
            return 0;

        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        return submitPurchaseResponse.getStatusCode();
    }


    private Integer submitPurchaseNOBranchID (Customer customer, PurchaseItem purchaseItem1){

        IServerResponse response = submitPurchaseResponseNOBranchID(customer,purchaseItem1);
        if(response instanceof ZappServerErrorResponse)
            return 0;

        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        return submitPurchaseResponse.getStatusCode();
    }

    private Integer submitPurchaseNotransactionID (Customer customer, PurchaseItem purchaseItem1){

        IServerResponse response = submitPurchaseResponseNoTransactionID(customer,purchaseItem1);
        if(response instanceof ZappServerErrorResponse)
            return 0;

        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        return submitPurchaseResponse.getStatusCode();
    }
    private Integer submitPurchaseNoTotalAmount (Customer customer, PurchaseItem purchaseItem1){

        IServerResponse response = submitPurchaseResponseNoTotalAmount(customer,purchaseItem1);
        if(response instanceof ZappServerErrorResponse)
            return 0;

        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        return submitPurchaseResponse.getStatusCode();
    }

    private Integer submitPurchaseNoOpenTime (Customer customer, PurchaseItem purchaseItem1){

        IServerResponse response = submitPurchaseResponseNoOpenTime(customer,purchaseItem1);
        if(response instanceof ZappServerErrorResponse)
            return 0;

        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        return submitPurchaseResponse.getStatusCode();
    }







    public SubmitPurchaseResponse submitPurchaseResponse(Customer customer, PurchaseItem purchaseItem1) {

        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setBranchId("123");
        submitPurchase.setPosId("1324");
        submitPurchase.setxSourceName("123424");
        submitPurchase.setxSourceVersion("2332");
        submitPurchase.setxSourceType("234346");
        submitPurchase.setCustomers(Lists.newArrayList(customer));

        //build purchase
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = buildPurchase(uuid,500, Lists.newArrayList(purchaseItem1)) ;
        submitPurchase.setPurchase(purchase);
        return (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
    }


    public SubmitPurchaseResponse submitPurchaseResponseNoTransactionID(Customer customer, PurchaseItem purchaseItem1) {

        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setBranchId("123");
        submitPurchase.setPosId("1324");
        submitPurchase.setxSourceName("123424");
        submitPurchase.setxSourceVersion("2332");
        submitPurchase.setxSourceType("234346");
        submitPurchase.setCustomers(Lists.newArrayList(customer));

        //build purchase
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = buildPurchaseNoTransactionID(500, Lists.newArrayList(purchaseItem1)) ;
        submitPurchase.setPurchase(purchase);
        return (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
    }

    public SubmitPurchaseResponse submitPurchaseResponseNoTotalAmount(Customer customer, PurchaseItem purchaseItem1) {

        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setBranchId("123");
        submitPurchase.setPosId("1324");
        submitPurchase.setxSourceName("123424");
        submitPurchase.setxSourceVersion("2332");
        submitPurchase.setxSourceType("234346");
        submitPurchase.setCustomers(Lists.newArrayList(customer));

        //build purchase
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = buildPurchaseNoTotalAmount(uuid, Lists.newArrayList(purchaseItem1)) ;
        submitPurchase.setPurchase(purchase);
        return (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
    }


    public SubmitPurchaseResponse submitPurchaseResponseNoOpenTime(Customer customer, PurchaseItem purchaseItem1) {

        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setBranchId("123");
        submitPurchase.setPosId("1324");
        submitPurchase.setxSourceName("123424");
        submitPurchase.setxSourceVersion("2332");
        submitPurchase.setxSourceType("234346");
        submitPurchase.setCustomers(Lists.newArrayList(customer));

        //build purchase
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = buildPurchaseNoOpenTime(uuid,500, Lists.newArrayList(purchaseItem1)) ;
        submitPurchase.setPurchase(purchase);
        return (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
    }




    public SubmitPurchaseResponse submitPurchaseResponseNOPOS(Customer customer, PurchaseItem purchaseItem1) {

        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setBranchId("123");
        //submitPurchase.setPosId("1324");
        submitPurchase.setxSourceName("123424");
        submitPurchase.setxSourceVersion("2332");
        submitPurchase.setxSourceType("234346");
        submitPurchase.setCustomers(Lists.newArrayList(customer));

        //build purchase
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = buildPurchase(uuid,500, Lists.newArrayList(purchaseItem1)) ;
        submitPurchase.setPurchase(purchase);
        return (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
    }


    public SubmitPurchaseResponse submitPurchaseResponseNOBranchID(Customer customer, PurchaseItem purchaseItem1) {

        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setPosId("1324");
        submitPurchase.setxSourceName("123424");
        submitPurchase.setxSourceVersion("2332");
        submitPurchase.setxSourceType("234346");
        submitPurchase.setCustomers(Lists.newArrayList(customer));

        //build purchase
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = buildPurchase(uuid,500, Lists.newArrayList(purchaseItem1)) ;
        submitPurchase.setPurchase(purchase);
        return (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
    }


    public static  Purchase buildPurchase(String uuid, int totalAmount, List<PurchaseItem> items){
        Purchase purchase = new Purchase();
        purchase.setOpenTime(System.currentTimeMillis());
        purchase.setTransactionId(uuid);
        purchase.setTotalAmount(totalAmount);
        purchase.setItems(items);
        return purchase;
    }

    public static  Purchase buildPurchaseNoTransactionID(int totalAmount, List<PurchaseItem> items){
        Purchase purchase = new Purchase();
        purchase.setOpenTime(System.currentTimeMillis());
        purchase.setTotalAmount(totalAmount);
        purchase.setItems(items);
        return purchase;
    }

    public static  Purchase buildPurchaseNoTotalAmount(String uuid, List<PurchaseItem> items){
        Purchase purchase = new Purchase();
        purchase.setOpenTime(System.currentTimeMillis());
        purchase.setTransactionId(uuid);
        purchase.setItems(items);
        return purchase;
    }

    public static  Purchase buildPurchaseNoOpenTime(String uuid, int totalAmount, List<PurchaseItem> items){
        Purchase purchase = new Purchase();
        purchase.setTransactionId(uuid);
        purchase.setTotalAmount(totalAmount);
        purchase.setItems(items);
        return purchase;
    }




    public PurchaseItem buildPurchaseItemNoCode(int price, int quantity){
        PurchaseItem item = new PurchaseItem();
        item.setName("item1");
        item.setDepartmentCode("111");
        item.setDepartmentName("Dep1");
        item.setQuantity(quantity);
        item.setNetAmount(price);
        item.setGrossAmount(500);
        item.setTags(Lists.newArrayList("tag1","tag2"));
        return item;
    }

    public PurchaseItem buildPurchaseItemNoName(int price, int quantity){
        PurchaseItem item = new PurchaseItem();
        item.setCode("item1");
        item.setDepartmentCode("111");
        item.setDepartmentName("Dep1");
        item.setQuantity(quantity);
        item.setNetAmount(price);
        item.setGrossAmount(500);
        item.setTags(Lists.newArrayList("tag1","tag2"));
        return item;
    }

    public PurchaseItem buildPurchaseItemNoDepCode(int price, int quantity){
        PurchaseItem item = new PurchaseItem();
        item.setCode("item1");
        item.setName("item1");
        item.setDepartmentName("Dep1");
        item.setQuantity(quantity);
        item.setNetAmount(price);
        item.setGrossAmount(500);
        item.setTags(Lists.newArrayList("tag1","tag2"));
        return item;
    }

    public PurchaseItem buildPurchaseItemNoDepName(int price, int quantity){
        PurchaseItem item = new PurchaseItem();
        item.setCode("item1");
        item.setName("item1");
        item.setDepartmentCode("111");
        item.setQuantity(quantity);
        item.setNetAmount(price);
        item.setGrossAmount(500);
        item.setTags(Lists.newArrayList("tag1","tag2"));
        return item;
    }

    public PurchaseItem buildPurchaseItemNoQuantity(int price){
        PurchaseItem item = new PurchaseItem();
        item.setCode("item1");
        item.setName("item1");
        item.setDepartmentCode("111");
        item.setDepartmentName("Dep1");
        item.setNetAmount(price);
        item.setGrossAmount(500);
        item.setTags(Lists.newArrayList("tag1","tag2"));
        return item;
    }

    public PurchaseItem buildPurchaseItemNoGrossAmount(int price, int quantity){
        PurchaseItem item = new PurchaseItem();
        item.setCode("item1");
        item.setName("item1");
        item.setDepartmentCode("111");
        item.setDepartmentName("Dep1");
        item.setQuantity(quantity);
        item.setNetAmount(price);
        item.setTags(Lists.newArrayList("tag1","tag2"));
        return item;
    }


    public PurchaseItem buildPurchaseItemNoNetAmount( int quantity){
        PurchaseItem item = new PurchaseItem();
        item.setCode("item1");
        item.setName("item1");
        item.setDepartmentCode("111");
        item.setDepartmentName("Dep1");
        item.setQuantity(quantity);
        item.setGrossAmount(500);
        item.setTags(Lists.newArrayList("tag1","tag2"));
        return item;
    }


    public PurchaseItem buildPurchaseItem(int price, int quantity){
        PurchaseItem item = new PurchaseItem();
        item.setCode("item1");
        item.setName("item1");
        item.setDepartmentCode("111");
        item.setDepartmentName("Dep1");
        item.setQuantity(quantity);
        item.setNetAmount(price);
        item.setGrossAmount(500);
        item.setTags(Lists.newArrayList("tag1","tag2"));
        return item;
    }







}
