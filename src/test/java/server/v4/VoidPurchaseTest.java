package server.v4;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.serverCategories.ServerRegression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartGift;
import hub.services.member.MembersService;
import integration.base.BaseIntegrationTest;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.utils.V4Builder;
import server.v2_8.RedeemAssetResponse;
import server.v4.base.ZappServerErrorResponse;
import server.v4.common.enums.Status;
import server.v4.common.models.Customer;
import server.v4.common.models.Purchase;
import server.v4.common.models.RedeemAsset;
import server.v4.common.models.RedeemAssetOfSubmitPurchase;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.*;

import static utils.StringUtils.StringToKey;

/**
 * Created by Jenny on 7/8/2017.
 */

public class VoidPurchaseTest extends BaseIntegrationTest {

    private V4Builder v4Builder = new V4Builder(apiKey);
    private static SmartGift smartGift;

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
        try {
            smartGift = hubAssetsCreator.createSmartGiftWithAutoGenerateCodes(UUID.randomUUID().toString());
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
            Assert.fail("Error while creating smart gift - cannot continue with tests..");
        }
    }

    /***
     * Test CNP-6577 Cancel Purchase based on transaction identifiers for unlock asset
     ***/

    @Test
    @Category(ServerRegression.class)
    public void testVoidCancelAssetKey() throws Exception{

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);
        NewMember member =createNewMemberAndSendGift();
        //Get asset's ket
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        String assetKey = getMemberDetails(customer);

        //Verify that the asset is not locked in the DataStore
        Key dataStoreAssetKey = StringToKey(assetKey);
        verifyAssetIsUnlocked(dataStoreAssetKey);

        Purchase purchase = v4Builder.buildDefaultPurchase(UUID.randomUUID().toString());
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        //GetBenefits request
        getBenefits(redeemAsset,customer, purchase);

        //Verify that the asset is locked
        verifyAssetIsLocked(dataStoreAssetKey,purchase.getTransactionId());

        //Send voidPurchase request
        voidPurchase(purchase);

        //verify asset is unlocked
        verifyAssetIsUnlocked(dataStoreAssetKey);

    }

    /***
     * Test CNP-6577 Cancel Purchase based on transaction identifiers for unlock asset
     ***/
    @Test
    @Category(ServerRegression.class)
    public void testVoidPurchaseAssetCode() throws Exception{

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);
        NewMember member =createNewMemberAndSendGift();

        //Get asset's ket
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        String assetKey = getMemberDetails(customer);

        //Verify that the asset is not locked in the DataStore
        Key dataStoreAssetKey = StringToKey(assetKey);
        verifyAssetIsUnlocked(dataStoreAssetKey);

        Purchase purchase = v4Builder.buildDefaultPurchase(UUID.randomUUID().toString());

        //Perform redeem asset (client)
        server.v2_8.RedeemAsset redeemAsset = new server.v2_8.RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(assetKey);
        String memKey = getMembershipKey(member.getPhoneNumber());
        redeemAsset.setMembershipKey(memKey);
        redeemAsset.setToken(serverToken);
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse)redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);
        String code = redeemAssetResponse.getCode();

        RedeemAsset redeemAsset1 = new RedeemAsset();
        redeemAsset1.setCode(code);

        //GetBenefits request
        getBenefits(redeemAsset1,customer, purchase);

        //Verify that the asset is locked
        verifyAssetIsLocked(dataStoreAssetKey,purchase.getTransactionId());

        //Send voidPurchase request
        voidPurchase(purchase);

        //verify asset is unlocked
        verifyAssetIsUnlocked(dataStoreAssetKey);
    }

    /***
     * Test CNP-6577 Cancel Purchase based on transaction identifiers for unlock asset
     ***/
    @Test
    public void testVoidCancelAssetKeyNegative() throws Exception{

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);
        NewMember member =createNewMemberAndSendGift();
        //Get asset's ket
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        String assetKey = getMemberDetails(customer);

        //Verify that the asset is not locked in the DataStore
        Key dataStoreAssetKey = StringToKey(assetKey);
        verifyAssetIsUnlocked(dataStoreAssetKey);

        Purchase purchase = v4Builder.buildDefaultPurchase(UUID.randomUUID().toString());
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        //GetBenefits request
        getBenefits(redeemAsset,customer, purchase);
        String transactionID = purchase.getTransactionId();
        //Verify that the asset is locked
        verifyAssetIsLocked(dataStoreAssetKey,transactionID);

        //Send voidPurchase request
        purchase.setTransactionId("12323223");
        voidPurchase(purchase);

        //Verify that the asset is locked
        verifyAssetIsLocked(dataStoreAssetKey,transactionID);
    }

    /***
     * Test CNP-6577 Cancel Purchase based on transaction identifiers for unlock asset
     ***/
    @Test
    public void testVoidCancelAssetKeyNegative2() throws Exception{

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);
        NewMember member =createNewMemberAndSendGift();
        //Get asset's ket
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        String assetKey = getMemberDetails(customer);

        //Verify that the asset is not locked in the DataStore
        Key dataStoreAssetKey = StringToKey(assetKey);
        verifyAssetIsUnlocked(dataStoreAssetKey);

        Purchase purchase = v4Builder.buildDefaultPurchase(UUID.randomUUID().toString());
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        //GetBenefits request
        getBenefits(redeemAsset,customer, purchase);
        String transactionID = purchase.getTransactionId();
        //Verify that the asset is locked
        verifyAssetIsLocked(dataStoreAssetKey,transactionID);

        //Send voidPurchase request
        purchase.setOpenTime(System.currentTimeMillis());
        voidPurchase(purchase);

        //Verify that the asset is locked
        verifyAssetIsLocked(dataStoreAssetKey,transactionID);
    }

    /***
     * Test CNP-6577 Cancel Purchase based on transaction identifiers for unlock asset
     ***/
    @Test
    public void testVoidPurchasePositive3() throws Exception{

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);
        NewMember member =createNewMemberAndSendGift();
        //Get asset's ket
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        String assetKey = getMemberDetails(customer);

        //Verify that the asset is not locked in the DataStore
        Key dataStoreAssetKey = StringToKey(assetKey);
        verifyAssetIsUnlocked(dataStoreAssetKey);

        Purchase purchase = v4Builder.buildDefaultPurchase(UUID.randomUUID().toString());
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        //GetBenefits request
        getBenefits(redeemAsset,customer, purchase);
        String transactionID = purchase.getTransactionId();
        //Verify that the asset is locked
        verifyAssetIsLocked(dataStoreAssetKey,transactionID);

        //Send voidPurchase request
        purchase.setTotalAmount(0);
        voidPurchase(purchase);

        //Verify that the asset is locked
        verifyAssetIsUnlocked(dataStoreAssetKey);
    }

    /***
     * Test CNP-6577 Cancel Purchase based on transaction identifiers for unlock asset
     ***/

    @Test
    @Category(ServerRegression.class)
    public void testVoidPurchaseAfterSubmitPurchase() throws Exception{

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);
        NewMember member =createNewMemberAndSendGift();
        //Get asset's ket
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        String assetKey = getMemberDetails(customer);

        //Verify that the asset is not locked in the DataStore
        Key dataStoreAssetKey = StringToKey(assetKey);
        verifyAssetIsUnlocked(dataStoreAssetKey);

        Purchase purchase = v4Builder.buildDefaultPurchase(UUID.randomUUID().toString());
        RedeemAssetOfSubmitPurchase redeemAsset = new RedeemAssetOfSubmitPurchase();
        redeemAsset.setKey(assetKey);
        redeemAsset.setAppliedAmount("2");

        //Submit Purchase request
        purchase.setStatus(Status.OPEN);
        SubmitPurchase submitPurchase = buildSubmitPurchaseRequest(customer,Lists.newArrayList(redeemAsset),purchase);
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("status" , Status.OPEN.toString());
        submitPurchase.setQueryParameters(queryParams);

        IServerResponse response = submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
        if(response instanceof ZappServerErrorResponse)
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());

        String transactionID = purchase.getTransactionId();
        //Verify that the purchase status is open
        checkResults(transactionID,"Open");

        //Send voidPurchase request
        voidPurchase(purchase);

        //verify asset is unlocked
        checkResults(transactionID, "Deleted");

    }

    /***
     * Test CNP-6577 Cancel Purchase based on transaction identifiers for unlock asset
     ***/
    @Test
    @Category(ServerRegression.class)
    public void testVoidPurchaseAfterSubmitPurchaseNegative() throws Exception{

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);
        NewMember member = createNewMemberAndSendGift();
        //Get asset's ket
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        String assetKey = getMemberDetails(customer);

        //Verify that the asset is not locked in the DataStore
        Key dataStoreAssetKey = StringToKey(assetKey);
        verifyAssetIsUnlocked(dataStoreAssetKey);

        Purchase purchase = v4Builder.buildDefaultPurchase(UUID.randomUUID().toString());
        RedeemAssetOfSubmitPurchase redeemAsset = new RedeemAssetOfSubmitPurchase();
        redeemAsset.setKey(assetKey);
        redeemAsset.setAppliedAmount("2");
        //Submit Purchase request
        purchase.setStatus(Status.FINAL);
        submitPurchase(customer,Lists.newArrayList(redeemAsset),purchase);

        String transactionID = purchase.getTransactionId();

        //Verify that the asset is locked
        checkResults(transactionID,"Submitted");

        //Send voidPurchase request
        voidPurchase(purchase);

        //verify asset is unlocked
        checkResults(transactionID,"Submitted");

    }

    /***
     * Test CNP-6577 Cancel Purchase based on transaction identifiers for unlock asset
     ***/
    @Test
    public void testVoidPurchaseAfterSubmitPurchaseNegative2() throws Exception {

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(), false);
        NewMember member = createNewMemberAndSendGift();
        //Get asset's ket
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        String assetKey = getMemberDetails(customer);

        //Verify that the asset is not locked in the DataStore
        Key dataStoreAssetKey = StringToKey(assetKey);
        verifyAssetIsUnlocked(dataStoreAssetKey);

        Purchase purchase = v4Builder.buildDefaultPurchase(UUID.randomUUID().toString());
        RedeemAssetOfSubmitPurchase redeemAsset = new RedeemAssetOfSubmitPurchase();
        redeemAsset.setKey(assetKey);
        redeemAsset.setAppliedAmount("2");

        //Submit Purchase request
        purchase.setStatus(Status.OPEN);
        SubmitPurchase submitPurchase = buildSubmitPurchaseRequest(customer, Lists.newArrayList(), purchase);
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("status" , Status.OPEN.toString());
        submitPurchase.setQueryParameters(queryParams);

        IServerResponse response = submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
        if(response instanceof ZappServerErrorResponse)
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());

        String transactionID = purchase.getTransactionId();
        //Verify that the asset is locked
        checkResults(transactionID, "Open");

        //Send voidPurchase request
        purchase.setTransactionId("345435345");
        voidPurchase(purchase);

        //verify asset is unlocked
        checkResults(transactionID, "Open");
    }

    public SubmitPurchase submitPurchase (Customer customer, ArrayList<RedeemAsset> redeemAssets, Purchase purchase) throws Exception{

        SubmitPurchase submitPurchase = buildSubmitPurchaseRequest(customer, redeemAssets, purchase);
        IServerResponse response = submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);

        if(response instanceof ZappServerErrorResponse)
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        //Verify the returned customer is as expected
        Assert.assertEquals("Verify the status code","ok",((SubmitPurchaseResponse)response).getStatus());
        return submitPurchase;

    }

    @NotNull
    private SubmitPurchase buildSubmitPurchaseRequest(Customer customer, ArrayList<RedeemAsset> redeemAssets, Purchase purchase) {
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setDefauldHeaderParams();
        submitPurchase.setCustomers(Lists.newArrayList(customer));

        if (redeemAssets.size() > 0)
            submitPurchase.setRedeemAssets(redeemAssets);

        submitPurchase.setPurchase(purchase);
        return submitPurchase;
    }

    private void checkResults (String transactionID, String expectedStatus) throws Exception{

        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, transactionID));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: "+ transactionID, q, 1, dataStore);
        String status = (String) result.get(0).getProperty("Status");
        Assert.assertEquals("Verify the status is:" + expectedStatus, expectedStatus, status);

    }



    private void voidPurchase (Purchase purchase) throws Exception{

      //Send voidPurchase request
      VoidPurchase voidPurchase = new VoidPurchase(apiKey);
      voidPurchase.setPurchase(purchase);
      VoidPurchaseResponse voidPurchaseResponse =  (VoidPurchaseResponse)voidPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,VoidPurchaseResponse.class);
      Thread.sleep(20000);

  }



   private String getMembershipKey (String phone) {

       server.v2_8.common.Customer customer1 = new server.v2_8.common.Customer();
       customer1.setPhoneNumber(phone);
       server.v2_8.GetMemberDetails getMemberDetails = new server.v2_8.GetMemberDetails();
       getMemberDetails.getCustomers().add(customer1);
       getMemberDetails.setExpandAssets(true);
       getMemberDetails.setIncludeArchivedAssets(false);

       //Send the request
       server.v2_8.GetMemberDetailsResponse getMemberDetailsResponse = (server.v2_8.GetMemberDetailsResponse)getMemberDetails.SendRequestByApiKeyAndValidateResponse(apiKey, server.v2_8.GetMemberDetailsResponse.class);
       Assert.assertEquals("Result must be Y", "Y", getMemberDetailsResponse.getResult());


      return getMemberDetailsResponse.getMemberships().get(0).getKey();

   }

    private NewMember createNewMemberAndSendGift ()throws Exception{
        NewMember member = hubMemberCreator.createMember(AUTO_LAST_PREFIX,AUTO_LAST_PREFIX);
        performAnActionSendGift(smartGift);
        return member;

    }

    private void performAnActionSendGift(SmartGift smartGift) throws Exception {

        Thread.sleep(4000);
        boolean isGiftExist = MembersService.performSmartActionSendAAsset(smartGift.getTitle());
        log4j.info("The gift was send to the member");
        Assert.assertTrue(isGiftExist);

    }


    private void getBenefits (RedeemAsset redeemAsset, Customer customer, Purchase purchase) {

        //GetBenefits request

        GetBenefits getBenefits = v4Builder.buildGetBenefitsWithAssetKey(customer,Lists.newArrayList(redeemAsset),purchase);
        GetBenefitsResponse getBenefitsResponse =  (GetBenefitsResponse)getBenefits.SendRequestByApiKeyAndValidateResponse(apiKey,GetBenefitsResponse.class);

    }

    private void verifyAssetIsLocked (Key datastoreKey, String transactionID) throws Exception{
        Thread.sleep(2000);
        Entity result2 = dataStore.findOneByKey(datastoreKey);
        Assert.assertNotNull("Status in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result2.getProperty("Status"));
      //  Assert.assertEquals("Asset RedeemState must be Unused","Unused",result2.getProperty("RedeemState"));
        Assert.assertNotNull("Asset RedeemLock property should not be null " ,result2.getProperty("RedeemLock"));
        String redeemLock = (String) result2.getProperty("RedeemLock");
        Assert.assertTrue("Asset RedeemLock status is locked" ,redeemLock.contains(locationId));
        Assert.assertTrue("Asset RedeemLock status is locked" ,redeemLock.contains(transactionID));
    }

    private void verifyAssetIsUnlocked (Key dataStoreAssetKey) {
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result.getProperty("Status"));
      //  Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));
        Assert.assertEquals("Asset RedeemLock status is not locked",null,result.getProperty("RedeemLock"));
    }

    private String getMemberDetails (Customer customer){

        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.addQueryParameter("returnAssets","active");
        getMemberDetails.setApiKey(apiKey);
        getMemberDetails.setDefauldHeaderParams();
        getMemberDetails.setCustomer(customer);
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse)getMemberDetails.SendRequestByApiKey(apiKey,GetMemberDetailsResponse.class);

        return getMemberDetailsResponse.getMembership().getAssets().get(0).getKey();
    }


}

