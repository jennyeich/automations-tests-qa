package server.v4;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.serverCategories.ServerRegression;
import hub.base.categories.serverCategories.serverV4_0;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.common.AnonymousPurchaseTypes;
import server.common.ApiClientBuilder;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.common.asset.AssignAsset;
import server.utils.PosErrorsEnum;
import server.utils.V4Builder;
import server.v2_8.GetPurchase;
import server.v2_8.GetPurchaseResponse;
import server.v2_8.JoinClubResponse;
import server.v2_8.PurchaseResponse;
import server.v2_8.common.Asset;
import server.v4.base.ZappServerErrorResponse;
import server.v4.common.enums.Status;
import server.v4.common.enums.Type;
import server.v4.common.models.*;

import java.util.*;

/**
 * Created by Goni on 7/3/2017.
 */

public class SubmitPurchaseTest extends BasePurchaseTest {

    private static final String ASSET_KEY = "AssetKey";
    private static final String ASSET = "Asset";
    private static final String REDEEM_CODE = "RedeemCode";
    private static final String DEALS = "Deals";
    private V4Builder v4Builder = new V4Builder(apiKey);

    private String confirmation;


   /**
    * CNP - 6252 Add to 'Submit purchase' deals reporting - automated
    **/

    @Test
    public void testSubmitPurchaseWithoutAppliedAmount() throws Exception{

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        RedeemAssetOfSubmitPurchase redeemAsset = new RedeemAssetOfSubmitPurchase();
        redeemAsset.setCode("123");
        submitPurchaseNegative(customer,PosErrorsEnum.REDEEM_ASSET_APPLIED_AMOUNT_MUST_CONTAIN_KEY_OR_CODE, Lists.newArrayList(redeemAsset));
    }

    /**
     * CNP - 6252 Add to 'Submit purchase' deals reporting - automated
     **/
    @Test
    @Category({ServerRegression.class})
    public void testSubmitPurchaseWithoutCode() throws Exception{


        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        RedeemAssetOfSubmitPurchase redeemAsset = new RedeemAssetOfSubmitPurchase();
        redeemAsset.setAppliedAmount("2");
        submitPurchaseNegative(customer,PosErrorsEnum.REDEEM_ASSET_MUST_CONTAIN_KEY_OR_CODE, Lists.newArrayList(redeemAsset));
    }

    /**
     * CNP - 6252 Add to 'Submit purchase' deals reporting - automated
     **/
    @Test
    @Category({ServerRegression.class})
    public void testSubmitPurchaseWithAssetKeyAndAppliedAmountOneRedeemAsset() throws Exception{

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "None", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();


        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        RedeemAssetOfSubmitPurchase redeemAsset = new RedeemAssetOfSubmitPurchase();
        redeemAsset.setAppliedAmount("2");
        redeemAsset.setKey(assetKey);
        submitPurchase(customer, Lists.newArrayList(redeemAsset), Lists.newArrayList(),ASSET_KEY,assetKey,redeemAsset.getAppliedAmount(), ASSET);
    }

    /**
     * CNP - 6252 Add to 'Submit purchase' deals reporting - automated
     **/
    @Test
    public void testSubmitPurchaseWithCodeAndAppliedAmountOneRedeemAsset() throws Exception{

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "None", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        RedeemAssetOfSubmitPurchase redeemAsset = new RedeemAssetOfSubmitPurchase();
        redeemAsset.setAppliedAmount("3");
        redeemAsset.setCode("2345");
        submitPurchase(customer, Lists.newArrayList(redeemAsset),Lists.newArrayList(), REDEEM_CODE,"2345",redeemAsset.getAppliedAmount(),ASSET);
    }


    /**
     * CNP - 6252 Add to 'Submit purchase' deals reporting - automated
     **/
    @Test
    public void testSubmitPurchaseWithCodeAndAppliedAmountDeals() throws Exception{

        JoinClubResponse newMember = createMember ();
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        Deal deal = new Deal();
        deal.setAppliedAmount("3");
        deal.setCode("2345");
        submitPurchase(customer,Lists.newArrayList(), Lists.newArrayList(deal),REDEEM_CODE,"2345",deal.getAppliedAmount(),DEALS);

    }


    /**
     * CNP - 6252 Add to 'Submit purchase' deals reporting - automated
     **/
    @Test
    public void testSubmitPurchaseWithCodeAndAppliedAmountDealsNegativeAmount() throws Exception{

        JoinClubResponse newMember = createMember ();
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        Deal deal = new Deal();
        deal.setAppliedAmount("-1000");
        deal.setCode("2345");
        submitPurchase(customer,Lists.newArrayList(), Lists.newArrayList(deal),REDEEM_CODE,"2345",deal.getAppliedAmount(), DEALS);

    }

    /**
     * CNP - 6252 Add to 'Submit purchase' deals reporting - automated
     **/
    @Test
    public void testSubmitPurchaseWithAssetKeyAndAppliedAmountOneRedeemAssetLargeAmount() throws Exception{

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "None", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        RedeemAssetOfSubmitPurchase redeemAsset = new RedeemAssetOfSubmitPurchase();
        redeemAsset.setAppliedAmount("259685698");
        redeemAsset.setKey(assetKey);
        submitPurchase(customer, Lists.newArrayList(redeemAsset), Lists.newArrayList(), ASSET_KEY,assetKey,redeemAsset.getAppliedAmount(), ASSET);
    }

    /**
     * CNP - 6252 Add to 'Submit purchase' deals reporting - automated
     **/
    @Test
    public void testSubmitPurchaseWithoutAppliedAmountDealsNegative() throws Exception{

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        Deal deal = new Deal();
        deal.setCode("2345");

        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setCode("123");
        submitPurchaseNegativeDeals(customer,PosErrorsEnum.DEALS_APPLIED_AMOUNT_MUST_CONTAIN_KEY_OR_CODE, Lists.newArrayList(deal));
    }

    /**
     * CNP - 6252 Add to 'Submit purchase' deals reporting - automated
     **/
    @Test
    public void testSubmitPurchaseWithoutDealCodeDealsNegative() throws Exception{

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        Deal deal = new Deal();
        deal.setAppliedAmount("100");

        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setCode("123");
        submitPurchaseNegativeDeals(customer,PosErrorsEnum.DEALS_MUST_CONTAIN_KEY_OR_CODE, Lists.newArrayList(deal));
    }

    /**
     * CNP - 6252 Add to 'Submit purchase' deals reporting - automated
     **/
    @Test
    public void testSubmitPurchaseWithAssetKeyAndAppliedAmountNegativeOneRedeemAsset() throws Exception{

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "None", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        RedeemAssetOfSubmitPurchase redeemAsset = new RedeemAssetOfSubmitPurchase();
        redeemAsset.setAppliedAmount("-2323432");
        redeemAsset.setKey(assetKey);
        submitPurchase(customer, Lists.newArrayList(redeemAsset), Lists.newArrayList(),ASSET_KEY,assetKey,redeemAsset.getAppliedAmount(), ASSET);
    }


    @Test
    @Category(serverV4_0.class)
    public void submitPurchaseWith10Items() throws Exception {
        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, v4Builder.buildSubmitPurchaseItemsList()) ;
        SubmitPurchase submitPurchase = submitPurchaseGeneral(Lists.newArrayList(customer),Lists.newArrayList() ,Lists.newArrayList(), purchase);

        Query q = getOueryForUserAction(submitPurchase.getPurchase().getTransactionId());
        List<Entity> result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getPurchase().getTransactionId(), q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, confirmation));
    }

    @Test
    public void testSubmitPurchaseMoreThan1Customer() throws Exception {

        submitPurchaseWithTotalSum(5000);
    }


    /**
     *   C8358 remove validate items validation in POS 2.8 and 4.0 - Automated
     *   Items totla sum is not validated
     */

    @Test
    public void testSubmitPurchaseWhenTheTotalSumIsNotAsTotalItems() throws Exception {

        submitPurchaseWithTotalSum(21212);
    }


    @Test
    public void testSubmitPurchaseAndAllowMultiplePurchasesForPOSIdentifiersNo() throws Exception {
        //AllowMultiplePurchasesForPOSIdentifiers = No
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),false));

        //Assert that AllowMultiplePurchasesForPOSIdentifiers = no in the dataStore
        List<Entity> result = serverHelper.getBusinessFromApiClient(dataStore);
        Assert.assertEquals("AllowMultiplePurchasesForPOSIdentifiers is False", false, result.get(0).getProperty(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key()));

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,0, Lists.newArrayList()) ;
        SubmitPurchase submitPurchase = submitPurchaseGeneral(Lists.newArrayList(customer),Lists.newArrayList() ,Lists.newArrayList(), purchase);

        Query qAction = getOueryForUserAction(submitPurchase.getPurchase().getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: "+ submitPurchase.getPurchase().getTransactionId(), qAction,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, confirmation));

    }

    @Test
    @Category({ServerRegression.class})
    public void testAnonymousPurchaseEqualsToNeverMultiplePurchasesNo() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),false),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()));

        String uuid = UUID.randomUUID().toString();
        log4j.info("transactionId: " + uuid);
        Purchase purchase = v4Builder.buildPurchase(uuid,0, Lists.newArrayList()) ;

        SubmitPurchase submitPurchase = submitPurchaseRequest(Lists.newArrayList(),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        String errorMessage = submitPurchaseResponseError(submitPurchase);
        Assert.assertTrue(PosErrorsEnum.PROPRIETARY_PURCHASE_NO_CLUB_MEMBERS_FOUND.isValidMessageForCode(errorMessage));


        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NO_IDENTIFIER.type()));


        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        submitPurchase = submitPurchaseRequest(Lists.newArrayList(customer),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        submitPurchaseResponse(submitPurchase);

        //Verify Purchase with DataStore
        verifyPurchaseWithDataStore(submitPurchase, 1);
    }


    @Test
    public void testAnonymousPurchaseEqualsToAlwaysMultiplePurchasesYes() throws Exception {

        //SaveAnonymousPurchaseOn
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()));


        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,0, Lists.newArrayList()) ;

        //Run first submitPurchase and validate response
        SubmitPurchase submitPurchase = submitPurchaseRequest(Lists.newArrayList(),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        IServerResponse response = submitPurchaseResponse(submitPurchase);

        confirmation = ((SubmitPurchaseResponse)response).getConfirmation();

        Query q = getOueryForUserAction(submitPurchase.getPurchase().getTransactionId());
        List <Entity> result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getPurchase().getTransactionId(),q, 1, dataStore);
        Assert.assertTrue("UID must be inside", ((Text)result.get(0).getProperty("Data")).getValue().contains(confirmation));

        //Verify Purchase with DataStore
        verifyPurchaseWithDataStore(submitPurchase, 1);

        //Run second submitPurchase and validate response
        submitPurchaseResponse(submitPurchase);

        //Verify Purchase with DataStore ---- second submit purchase -----
        q = new Query("Purchase");
        ArrayList<Query.Filter> filters = new ArrayList<>();
        filters.add(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, transactionId));
        filters.add(new Query.FilterPredicate("BusinessID",Query.FilterOperator.EQUAL, locationId));
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, filters));
        List<Entity> result2 = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + transactionId,q,2,dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result2.get(0));
        serverHelper.validatePurchaseResult(submitPurchase, result2.get(1));

        q = getOueryForUserAction(transactionId);
        List<Entity> resultUserAction = serverHelper.queryWithWait("Must find two UserAction with the transaction ID: " + transactionId, q,2, dataStore);

    }

    @Test
    public void testAnonymousPurchaseEqualsToAlwaysMultiplePurchasesNo() throws Exception {

        //SaveAnonymousPurchaseOn
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),false),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()));

        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,0, Lists.newArrayList()) ;

        //Run first submitPurchase and validate response
        SubmitPurchase submitPurchase = submitPurchaseRequest(Lists.newArrayList(),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        IServerResponse response= submitPurchaseResponse(submitPurchase);

        confirmation = ((SubmitPurchaseResponse)response).getConfirmation();

        //Verify Purchase with DataStore
        verifyPurchaseWithDataStore(submitPurchase, 1);

        Query q = getOueryForUserAction(submitPurchase.getPurchase().getTransactionId());
        List<Entity> result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getPurchase().getTransactionId(), q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, confirmation));

        //Run second submitPurchase and validate response is an error
        String errorMessage = submitPurchaseResponseError(submitPurchase);
        Assert.assertTrue(PosErrorsEnum.PROPRIETARY_PURCHASE_DUPLICATE_NOT_ALLOWED.isValidMessageForCode(errorMessage));

    }

    @Test
    public void testAnonymousPurchaseEqualsToNoIdentifierMultiplePurchasesYes() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NO_IDENTIFIER.type()));

        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,0, Lists.newArrayList()) ;

        SubmitPurchase submitPurchase = submitPurchaseRequest(Lists.newArrayList(),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        IServerResponse response = submitPurchaseResponse(submitPurchase);

        confirmation = ((SubmitPurchaseResponse)response).getConfirmation();

        //Verify Purchase with DataStore
        verifyPurchaseWithDataStore(submitPurchase, 1);

        Query q = getOueryForUserAction(submitPurchase.getPurchase().getTransactionId());
        List<Entity> result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getPurchase().getTransactionId(),q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, confirmation));

        //Run second submitPurchase and validate response
        submitPurchaseResponse(submitPurchase);

        //Verify Purchase with DataStore ---- second submit purchase -----
        q = new Query("Purchase");
        ArrayList<Query.Filter> filters = new ArrayList<>();
        filters.add(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, transactionId));
        filters.add(new Query.FilterPredicate("BusinessID",Query.FilterOperator.EQUAL, locationId));
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, filters));
        List<Entity> result2 = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + transactionId,q,2,dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result2.get(0));
        serverHelper.validatePurchaseResult(submitPurchase, result2.get(1));

        q = getOueryForUserAction(transactionId);
        List<Entity> resultUserAction = serverHelper.queryWithWait("Must find two UserAction with the transaction ID: " + transactionId, q,2, dataStore);

    }

    @Test
    public void testAnonymousPurchaseEqualsToNoIdentifierMultiplePurchasesNo() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),false),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NO_IDENTIFIER.type()));

        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,0, Lists.newArrayList()) ;

        SubmitPurchase submitPurchase = submitPurchaseRequest(Lists.newArrayList(),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        IServerResponse response = submitPurchaseResponse(submitPurchase);

        confirmation = ((SubmitPurchaseResponse)response).getConfirmation();

        //Verify Purchase with DataStore
        verifyPurchaseWithDataStore(submitPurchase, 1);

        Query q = getOueryForUserAction(submitPurchase.getPurchase().getTransactionId());
        List<Entity> result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getPurchase().getTransactionId(),q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, confirmation));

        //run second submit purchase request and validate error message
        submitPurchase = submitPurchaseRequest(Lists.newArrayList(),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        String errorMessage = submitPurchaseResponseError(submitPurchase);
        Assert.assertTrue(PosErrorsEnum.PROPRIETARY_PURCHASE_DUPLICATE_NOT_ALLOWED.isValidMessageForCode(errorMessage));
    }

    @Test
    public void testSubmitPurchaseWithAllPOSIntegrationField() throws Exception {

        String uuid = UUID.randomUUID().toString();

        //payments
        Payment payment =  new Payment();
        payment.setType(Type.CREDIT_CARD);
        payment.setSum(5000);
        ArrayList<Payment> payments = new ArrayList<>();
        payments.add(payment);

        //customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        String memberKey = newMember.getMembershipKey();

        //redeem assets
        RedeemAssetOfSubmitPurchase redeemAsset = new RedeemAssetOfSubmitPurchase();
        redeemAsset.setCode("123");
        redeemAsset.setAppliedAmount("10");
        Asset asset = AssignAsset.assignAssetToMember("gift", "None", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();
        redeemAsset.setKey(assetKey);

        //Deals
        Deal deal = new Deal();
        deal.setAppliedAmount("3");
        deal.setCode("2345");

        Purchase purchase = v4Builder.buildPurchase (uuid, 5000, v4Builder.buildSubmitPurchaseItemsList(),payments, 0 );

        //run submit purchase and validate the response
        SubmitPurchase submitPurchase = submitPurchaseRequest(Lists.newArrayList(customer),Lists.newArrayList(redeemAsset) ,Lists.newArrayList(deal), purchase);
        IServerResponse response = submitPurchaseResponse(submitPurchase);

        confirmation = ((SubmitPurchaseResponse)response).getConfirmation();

        //Verify with DataStore
        Entity result = verifyPurchaseWithDataStore(submitPurchase, 1).get(0);

        Query q = getOueryForUserAction(submitPurchase.getPurchase().getTransactionId());
        List<Entity> queryResult = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getPurchase().getTransactionId(), q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(queryResult, confirmation));

        Assert.assertEquals("TotalGeneralDiscount is equal", submitPurchase.getPurchase().getTotalGeneralDiscount().longValue(), result.getProperty("TotalGeneralDiscount"));
        Assert.assertEquals("OrderType is equal", submitPurchase.getPurchase().getOrderType().toString(),result.getProperty("OrderType"));

    }

    @Test
    public void testAnonymousPurchaseEqualsToNever() throws Exception {
        //SaveAnonymousPurchaseOn
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()));

        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,0, Lists.newArrayList()) ;

        SubmitPurchase submitPurchase = submitPurchaseRequest(Lists.newArrayList(),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        submitPurchaseResponseError(submitPurchase);

        submitPurchase = submitPurchaseRequest(Lists.newArrayList(),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        String errorMessage = submitPurchaseResponseError(submitPurchase);
        Assert.assertTrue(PosErrorsEnum.PROPRIETARY_PURCHASE_NO_CLUB_MEMBERS_FOUND.isValidMessageForCode(errorMessage)) ;

    }

    @Test
    @Category({ServerRegression.class})
    public void testSubmitPurchaseAndAllowMultiplePurchasesForPOSIdentifiersIsYes() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true),
                                    ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()));

        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,0, Lists.newArrayList()) ;

        //send the first request
        SubmitPurchase submitPurchase = submitPurchaseRequest(Lists.newArrayList(customer),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        IServerResponse response = submitPurchaseResponse(submitPurchase);

        //Verify with DataStore
        verifyPurchaseWithDataStore (submitPurchase, 1);

        Query q = getOueryForUserAction(submitPurchase.getPurchase().getTransactionId());
        List<Entity> result2 = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: "+ submitPurchase.getPurchase().getTransactionId(),q,1,dataStore);
        Assert.assertTrue("UID must be inside", ((Text)result2.get(0).getProperty("Data")).getValue().contains(((SubmitPurchaseResponse)response).getConfirmation()));

        //send second request and validate the response
        submitPurchase = submitPurchaseRequest(Lists.newArrayList(customer),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        IServerResponse secondResponse = submitPurchaseResponse(submitPurchase);

        //Verify with DataStore
        verifyPurchaseWithDataStore (submitPurchase, 2);


        q = getOueryForUserAction(submitPurchase.getPurchase().getTransactionId());
        List<Entity> result3 = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: "+ submitPurchase.getPurchase().getTransactionId(),q, 2, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result3, ((SubmitPurchaseResponse)response).getConfirmation()));
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result3, ((SubmitPurchaseResponse)secondResponse).getConfirmation()));
    }



    @Test
    @Category(serverV4_0.class)
    public void testGPurchasesWithStatusOpen() throws Exception {

        String uuid = UUID.randomUUID().toString();

        //customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());


        Purchase purchase = v4Builder.buildPurchase(uuid,0, Lists.newArrayList()) ;
        purchase.setStatus(Status.OPEN);
        //run submit purchase and validate the response
        SubmitPurchase submitPurchase = submitPurchaseRequest(Lists.newArrayList(customer),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("status" , Status.OPEN.toString());
        submitPurchase.setQueryParameters(queryParams);
        IServerResponse response =  submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
        if(!(((SubmitPurchaseResponse) response).getStatus().equals("ok")))
            Assert.fail(getError(response).get(MESSAGE).toString());

        //Verify with DataStore
        verifyPurchaseWithDataStore (submitPurchase, 1);

        GetPurchase getPurchase = new GetPurchase(locationId, "Open");
        GetPurchaseResponse getPurchaseResponse = (GetPurchaseResponse) getPurchase.sendRequestByTokenAndValidateResponse(serverToken, GetPurchaseResponse.class);
        Assert.assertNotNull("GetPurchase should not return empty response", getPurchaseResponse);
        List<PurchaseResponse> data = getPurchaseResponse.getData();
        Assert.assertNotNull("GetPurchase should not return empty data", data);
        Assert.assertTrue("GetPurchase should return at least one purchase", data.size() > 0);
        final boolean[] purchaseFromThisTestFound = {false};
        data.forEach( purchaseStatusOpen -> {
            Assert.assertEquals("Status for each purchase should be 'open'", "Open", purchaseStatusOpen.getStatus());
            Assert.assertNotNull("TransactionId should not be null", purchaseStatusOpen.getTransactionId());
            if(purchaseStatusOpen.getTransactionId().equals(transactionId)){
                purchaseFromThisTestFound[0] = true;
            }

        });

    }


    //C8354 - Report Tax in Submit Purchase Call - CNP-9147 -Automated
    @Test
    @Category({ServerRegression.class})
    public void testReportTaxInSubmitPurchase () throws Exception {

        submitPurchaseWithTax(100);
        submitPurchaseWithTax(100000);
        submitPurchaseWithTax(999999999);

    }
    //C8354 - Report Tax in Submit Purchase Call - CNP-9147 -Automated
    @Test
    public void testReportTaxInSubmitPurchaseZeroTaxAmount () throws Exception {

        submitPurchaseWithTax(0);

    }

    //C8354 - Report Tax in Submit Purchase Call - CNP-9147 -Automated
    @Test
    public void testReportTaxInSubmitPurchaseNegativeTaxAmount () throws Exception {

        submitPurchaseWithTax(-10000000);
        submitPurchaseWithTax(-999999999);
    }


    /**
     *   CNP-9735 line id is not saved on purchase
     *   Added Bug verification + more validations on item properties
     *   Also validates CNP-10056
     */
    @Test
    public void testSubmitPurchaseWithItemsLineID () throws Exception {

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        PurchaseItem item = v4Builder.buildPurchaseItem("code1", 100, 1, "name123");
        String uuid = String.valueOf(System.currentTimeMillis());
        Purchase purchase = v4Builder.buildPurchase(uuid, 40, Lists.newArrayList(item));
        SubmitPurchase submitPurchase = submitPurchaseGeneral(Lists.newArrayList(customer), Lists.newArrayList(), Lists.newArrayList(), purchase);

        //Verify with DataStore
        Query q = new Query("PurchaseItem");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getPurchase().getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one PurchaseItem with the transaction ID: " + submitPurchase.getPurchase().getTransactionId(), q, 1, dataStore);
        serverHelper.validatePurchaseItemResultv4(submitPurchase, item, result.get(0));

    }


    private void submitPurchaseWithTax (int taxAmout) throws Exception {


        String uuid = UUID.randomUUID().toString();

        //customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());


        Purchase purchase = v4Builder.buildPurchase(uuid,0, Lists.newArrayList()) ;
        purchase.setTotalTaxAmount(taxAmout);
        SubmitPurchase submitPurchase = submitPurchaseRequest(Lists.newArrayList(customer),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        IServerResponse response = submitPurchaseResponse(submitPurchase);



        //Verify Purchase with DataStore ---- first submit purchase -----
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getPurchase().getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getPurchase().getTransactionId(),q, 1, dataStore);

        serverHelper.validatePurchaseWithTaxAmount(submitPurchase,result.get(0));


    }

    public JoinClubResponse createMember () throws InterruptedException{

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        return newMember;

    }

    private void checkResults (SubmitPurchase submitPurchase, String type, String typeValue, String appliedAmount, String AssetType) throws Exception{

        Text t;
        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, submitPurchase.getPurchase().getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: "+ submitPurchase.getPurchase().getTransactionId(), q, 1, dataStore);
        if (AssetType.equals(DEALS))
            t = ((Text) result.get(0).getProperty(DEALS));
        else
            t = ((Text) result.get(0).getProperty("RedeemItems"));
        log4j.info("RedeemItems contains the following data : " +   t.getValue());
        log4j.info("type:" + type);
        log4j.info("typeValue:" +typeValue);
        Assert.assertTrue("RedeemItems must contain the type: " + type,t.getValue().contains("\"" +type +"\":\"" + typeValue + "\""));
        Assert.assertTrue("Data must contain the asset source: ",t.getValue().contains("\"AppliedAmount\":" + appliedAmount));

    }



    private void submitPurchase (Customer customer, ArrayList<RedeemAsset> redeemAssets,ArrayList<Deal> deals,  String type, String typeValue, String appliedAmount, String assetType) throws Exception{

        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setDefauldHeaderParams();
        submitPurchase.setCustomers(Lists.newArrayList(customer));

        if (redeemAssets.size() > 0)
            submitPurchase.setRedeemAssets(redeemAssets);
        if (deals.size() > 0)
            submitPurchase.setDeals(deals);

        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(500,1);
        Purchase purchase = v4Builder.buildPurchase(uuid,500, Lists.newArrayList(purchaseItem1)) ;

        submitPurchase.setPurchase(purchase);
        IServerResponse response =  submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);

        if(response instanceof ZappServerErrorResponse)
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        //Verify the returned customer is as expected
        Assert.assertNotNull("Confirmation should not be null",((SubmitPurchaseResponse)response).getConfirmation());
        checkResults(submitPurchase, type, typeValue, appliedAmount, assetType);

    }

    private SubmitPurchase submitPurchaseGeneral (ArrayList<Customer> customers, ArrayList<RedeemAsset> redeemAssets,ArrayList<Deal> deals, Purchase purchase) throws Exception{

        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setDefauldHeaderParams();
        submitPurchase.setCustomers(customers);

        if (redeemAssets.size() > 0)
            submitPurchase.setRedeemAssets(redeemAssets);
        if (deals.size() > 0)
            submitPurchase.setDeals(deals);

        String uuid = UUID.randomUUID().toString();

        submitPurchase.setPurchase(purchase);
        IServerResponse response =  (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);

        if(!(((SubmitPurchaseResponse) response).getStatus().equals("ok")))
            Assert.fail(getError(response).get(MESSAGE).toString());
        //Verify the returned customer is as expected
        Assert.assertNotNull("Verify the confirmation is not null",((SubmitPurchaseResponse)response).getConfirmation());
        //save for confirmation for user action validation
        confirmation = ((SubmitPurchaseResponse)response).getConfirmation();
        return submitPurchase;
    }


    private void submitPurchaseNegative (Customer customer, PosErrorsEnum errorCode, ArrayList<RedeemAsset> redeemAssets){
        IServerResponse response = v4Builder.submitPurchaseResponse(customer, redeemAssets);
        log4j.info("Submit Purchase request");
        validateErrorMessage(errorCode, getError(response).get(MESSAGE).toString());
    }

    private void submitPurchaseNegativeDeals (Customer customer, PosErrorsEnum errorCode, ArrayList<Deal> deals){
        log4j.info("Submit Purchase request");
        IServerResponse response = v4Builder.submitPurchaseResponseDeals(customer, deals);
        String errorMessage = getError(response).get(MESSAGE).toString();
        validateErrorMessage(errorCode,errorMessage);
    }




    protected void validateErrorMessage(PosErrorsEnum errorCode, String errorMessage) {
        log4j.info("Validate error code: " + errorCode + " , error message: "+ errorMessage);
        Assert.assertTrue(errorCode.isValidMessageForCode(errorMessage));
    }

    protected JSONObject getError(IServerResponse response){
        return ((JSONObject)((JSONArray)(response.getRawObject().get(ERRORS))).get(0));
    }

    private void submitPurchaseWithTotalSum (int totalSum) throws Exception{

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        //Create another customer
        JoinClubResponse newMember2 = serverHelper.createMember();
        Assert.assertNotNull(newMember2);
        Assert.assertNotNull(newMember2.getMembershipKey());
        Customer customer2 = new Customer();
        customer2.setPhoneNumber(newMember2.getPhoneNumber());

        ArrayList<Customer> customers = Lists.newArrayList();
        customers.add(customer);
        customers.add(customer2);

        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,totalSum, v4Builder.buildSubmitPurchaseItemsList()) ;
        SubmitPurchase submitPurchase = submitPurchaseGeneral(customers,Lists.newArrayList() ,Lists.newArrayList(), purchase);

        Query q = getOueryForUserAction(submitPurchase.getPurchase().getTransactionId());
        List<Entity> result = serverHelper.queryWithWait("Must find two UserAction with the transaction ID: " + submitPurchase.getPurchase().getTransactionId(),q, 2, dataStore);
        Assert.assertTrue("UID must be in the first user action ", ((Text)result.get(0).getProperty("Data")).getValue().contains(confirmation));
        Assert.assertTrue("UID must be in the second user action ", ((Text)result.get(1).getProperty("Data")).getValue().contains(confirmation));
    }





}
