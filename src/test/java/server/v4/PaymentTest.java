package server.v4;


import com.google.common.collect.Lists;
import hub.base.categories.serverCategories.ServerRegression;
import hub.base.categories.serverCategories.serverV4_0;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.common.IServerResponse;
import server.common.LocationBackendFields;
import server.common.PaymentType;
import server.internal.GetShortUserID;
import server.internal.GetShortUserIDResponse;
import server.utils.PosErrorsEnum;
import server.utils.ServerHelper;
import server.v2_8.JoinClubResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v4.common.enums.Mode;
import server.v4.common.enums.PaymentMethod;
import server.v4.common.enums.Type;
import server.v4.common.models.*;

import java.text.MessageFormat;
import java.util.UUID;

/**
 * Created by Goni on 22/06/2017.
 */
public class PaymentTest extends BasePaymentTest {

    /**
     * C5220
     * */
    @Test
    @Category({ServerRegression.class})
    public void testPayTypePointstNoVarificationCodeWithPaymentMethodMeanOfPaymentBodyContainsDiscount() throws Exception {

        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.DISCOUNT;
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(paymentMethod);

        //build purchase
        int sumToPay = 5000;
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(500,2);
        Payment paymentObj1 = v4Builder.buildPayment(Type.CASH,0);
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, Lists.newArrayList(purchaseItem1),Lists.newArrayList(paymentObj1),100) ;
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        assertResponse(payment, paymentResponse,PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString());

        Assert.assertTrue("Type should be 'memberPoints'" ,paymentResponse.getType().contains(PaymentType.POINTS.type()));
        //if ratio=1 we expect same amount in both nonMonetary and monetary
        Assert.assertEquals("Balance should be same in nonMonetary and monetary: " + paymentResponse.getUpdatedBalance().getMonetary() ,paymentResponse.getUpdatedBalance().getMonetary(),paymentResponse.getUpdatedBalance().getNonMonetary());

        //Verify with DataStore
        serverHelper.checkResultsForPay(payment, paymentResponse,dataStore);

        // verify the customer spent 50 points
        int pointsAfterPurchase = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to spend 50 points",(sumToPay * -1), (pointsAfterPurchase-pointsAfter));

    }

    /**
     * C5242
     * */
    @Test
    public void testPayTypePointstNoVarificationCodeWithPaymentMethodMeanOfPaymentBodyContainsMEAN_OF_PAYMENT() throws Exception {

        //Verify if the parameter in the Business Backend :"Payment Requires Verification Code" = No
        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT.toString();
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT.toString(),true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT;
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(paymentMethod);

        //build purchase
        int sumToPay = 5000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        assertResponse(payment, paymentResponse,paymentConfig);

        Assert.assertTrue("Type should be 'memberPoints'" ,paymentResponse.getType().contains(PaymentType.POINTS.type()));
        //if ratio=1 we expect same amount in both nonMonetary and monetary
        Assert.assertEquals("Balance should be same in nonMonetary and monetary: " + paymentResponse.getUpdatedBalance().getMonetary() ,paymentResponse.getUpdatedBalance().getMonetary(),paymentResponse.getUpdatedBalance().getNonMonetary());

        //Verify with DataStore
        serverHelper.checkResultsForPay(payment, paymentResponse,dataStore);

        // verify the customer spent 50 points
        int pointsAfterPurchase = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to spend 50 points",(sumToPay * -1), (pointsAfterPurchase-pointsAfter));

    }

    /**
     * C5242
     * */
    @Test
    public void testPayTypePointsNoVarificationCodeWithPaymentMethodDISCOUNTBodyContainsMEAN_OF_PAYMENT() throws Exception {

        String paymentConfig = PaymentMethod.DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT;
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(paymentMethod);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);


        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        Assert.assertEquals("Error code must be " + PosErrorsEnum.PROPRIETARY_PAYMENT_METHOD_IS_NOT_ALLOWED.getCode(),PosErrorsEnum.PROPRIETARY_PAYMENT_METHOD_IS_NOT_ALLOWED.getCode(), getError(paymentResponse).get(CODE));
        Assert.assertNotNull(MessageFormat.format(getError(paymentResponse).get(MESSAGE).toString(),((PaymentAPIResponse)paymentResponse).getType(),paymentConfig), ((PaymentAPIResponse)paymentResponse).getPayments());

    }


    /**
     * C5287
     * */
    @Test
    public void testPaymetWithAllPurchaseAttributes() throws Exception {

        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        //set payment options
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT);

        //build purchase
        int sumToPay = 5000;
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(400,1);
        PurchaseItem purchaseItem2 = v4Builder.buildPurchaseItem(500,1);
        Payment paymentObj1 = v4Builder.buildPayment(Type.CASH,0);
        Payment paymentObj2 = v4Builder.buildPayment(Type.CLUB_BUDGET,0);
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, Lists.newArrayList(purchaseItem1,purchaseItem2),Lists.newArrayList(paymentObj1,paymentObj2),100) ;
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);

        assertResponse(payment, paymentResponse,paymentConfig);
        Assert.assertTrue("Type should be 'memberPoints'" ,paymentResponse.getType().contains(PaymentType.POINTS.type()));

        //Verify with DataStore
        serverHelper.checkResultsForPay(payment, paymentResponse,dataStore);

    }

    /**
     * C5287
     * */
    @Test
    public void testPaymetMemberIdentifierWrongPhoneNumberNegativeTest() throws Exception {

        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        //set payment options
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT);

        //build purchase
        int sumToPay = 5000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid) ;

        customer.setPhoneNumber("363634634634");
        log4j.info("Give customer unknown phone number: " + customer.getPhoneNumber() );
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);



        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.SINGLE_CUSTOMER_NOT_FOUND, paymentResponse);

    }

    /**
     * C5705
     * */
    @Test
    public void testPaymetMemberIdentifierAppClientID() throws Exception {

        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,true);

        //set payment options
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT);

        //build purchase
        int sumToPay = 500;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid) ;
        String phoneNumber = String.valueOf(System.currentTimeMillis());
        GetShortUserIDResponse shortUserIDResponse = getShortUserIDResponse(phoneNumber);
        Customer customer = new Customer();

        serverHelper.addPointsToMember("10000", shortUserIDResponse.getMembershipKey(),shortUserIDResponse.getKey());
        Thread.sleep(10000);
        customer.setAppClientId(shortUserIDResponse.getId());
        log4j.info("Give customer app client ID : " + customer.getAppClientId() );
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);
        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);

        assertResponse(payment, paymentResponse,paymentConfig);
        Assert.assertTrue("Type should be 'memberPoints'" ,paymentResponse.getType().contains(PaymentType.POINTS.type()));

        //Verify with DataStore
        serverHelper.checkResultsForPay(payment, paymentResponse,dataStore);

    }


     /**
     * C5703
     * */
    @Test
    @Category({ServerRegression.class})
    public void testPaymetMemberIdentifierMemberID() throws Exception {

        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,true);

        String memberId = UUID.randomUUID().toString();
        createMemberWithAllIdentifiersWithPoints("10000",null, memberId);
        int pointsAfter = 10000;//v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        //set payment options
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT);

        //build purchase
        int sumToPay = 5000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid) ;

        Customer customer = new Customer();
        customer.setMemberId(memberId);

        log4j.info("set customer member id: " + customer.getMemberId() );
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);
        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);

        assertResponse(payment, paymentResponse,paymentConfig);
        Assert.assertTrue("Type should be 'memberPoints'" ,paymentResponse.getType().contains(PaymentType.POINTS.type()));

        //Verify with DataStore
        serverHelper.checkResultsForPay(payment, paymentResponse,dataStore);

    }


    /**
     * C5704
     * */
    @Test
    @Category( serverV4_0.class )
    public void testPaymetMemberIdentifierGovID() throws Exception {

        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,true);

        String govId = UUID.randomUUID().toString();
        createMemberWithAllIdentifiersWithPoints("10000",govId, null);
        int pointsAfter = 10000;//v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        //set payment options
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT);

        //build purchase
        int sumToPay = 5000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid) ;

        Customer customer = new Customer();
        customer.setGovId(govId);

        log4j.info("set customer gov id: " + customer.getGovId() );
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);
        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);

        assertResponse(payment, paymentResponse,paymentConfig);
        Assert.assertTrue("Type should be 'memberPoints'" ,paymentResponse.getType().contains(PaymentType.POINTS.type()));

        //Verify with DataStore
        serverHelper.checkResultsForPay(payment, paymentResponse,dataStore);

    }

    /**
     * C5196
     * total sum > Member's amount of balance
     * */
    @Test
    public void testPayTypePointsNoVarificationCodeWithAllowPartialPaymentFalseMemberInsufficienBalance1() throws Exception {

        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(), false);

        Customer customer = createMemberWithPoints("1000");
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 10 points", 1000, pointsAfter);


        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod, Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams, customer, sumToPay, purchase);


        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if (paymentResponse instanceof server.v4.base.ZappServerErrorResponse) {
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS, paymentResponse);

    }
    /**
     * C5196
     * member have a zero points balance.
     * */
    @Test
    public void testPayTypePointsNoVarificationCodeWithAllowPartialPaymentFalseMemberZeroBalance2() throws Exception {

        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),false);

        Customer customer = createMemberWithPoints("0");
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 0 points", 0,pointsAfter);

        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);


        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS, paymentResponse);
    }

    /**
     * C5196
     * new member have an empty balance.
     * */
    @Test
    public void testPayTypePointsNoVarificationCodeWithAllowPartialPaymentFalseMemberEmptyBalance3() throws Exception {

        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),false);

        JoinClubResponse member = serverHelper.createMember();
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());


        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);


        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS, paymentResponse);

    }


    /**
     * C5196
     * member's balance to be negative
     * */
    @Test
    public void testPayTypePointsNoVarificationCodeWithAllowPartialPaymentFalseMemberNegativenBalance4() throws Exception {

        setConfigurationForTest("false",PaymentType.POINTS.type(),"1","true");

        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),false);

        Customer customer = createMemberWithPoints("-1000");
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to have -10 points", -1000,pointsAfter);

        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS, paymentResponse);
    }

    /**
     * C5190
     * sum > Member's amount of balance
     * */
    @Test
    public void testPayTypePointsNoVarificationCodeWithAllowPartialPaymentTrueMemberInsufficienBalance1() throws Exception {

        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to have 100 points", 10000,pointsAfter);

        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 40000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);

        Assert.assertEquals("status code must be 200",httpStatusCode200, paymentResponse.getStatusCode());
        Assert.assertEquals("status must be ok", STATUS_OK, paymentResponse.getStatus());
        Assert.assertNotNull("Payments list should not be null", paymentResponse.getPayments());
        PaymentReturned paymentReturned = paymentResponse.getPayments().get(0);
        //Assert.assertEquals("Actual Charge not equals",(pointsAfter * -1), paymentReturned.getAmount());
        Assert.assertEquals("paymentMethod should be the same", paymentMethod.toString(), paymentReturned.getPaymentMethod().toString());


        Assert.assertTrue("Type should be 'memberPoints'" ,paymentResponse.getType().contains(PaymentType.POINTS.type()));
        //if ratio=1 we expect same amount in both nonMonetary and monetary
        Assert.assertEquals("Balance should be same in nonMonetary and monetary: " + paymentResponse.getUpdatedBalance().getMonetary() ,paymentResponse.getUpdatedBalance().getMonetary(),paymentResponse.getUpdatedBalance().getNonMonetary());

        //Verify with DataStore
        serverHelper.checkResultsForPay(payment, paymentResponse,dataStore);
    }

    /**
     * C5190
     * member have a zero points balance.
     * */
    @Test
    public void testPayTypePointsNoVarificationCodeWithAllowPartialPaymentTrueMemberZeroBalance2() throws Exception {

        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);

        Customer customer = createMemberWithPoints("0");
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 0 points", 0,pointsAfter);


        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS, paymentResponse);
    }

    /**
     * C5190
     * new member have an empty balance.
     * */
    @Test
    public void testPayTypePointsNoVarificationCodeWithAllowPartialPaymentTrueMemberEmptyBalance3() throws Exception {

        setConfigurationForTest("false",PaymentType.POINTS.type(),"1","true");

        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);


        JoinClubResponse member = serverHelper.createMember();
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());


        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS, paymentResponse);

    }
    /**
     * C5190
     * member's balance to be negative
     * */
    @Test
    public void testPayTypePointsNoVarificationCodeWithAllowPartialPaymentTrueMemberNegativeBalance4() throws Exception {

        setConfigurationForTest("false",PaymentType.POINTS.type(),"1","true");

        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);

        Customer customer = createMemberWithPoints("-1000");
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to have -1000 points", -1000,pointsAfter);

        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);


        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS, paymentResponse);
    }

  /****************************  Budget ******************************************/


    /**
     * C5247
     * */
    @Test
    public void testPayTypeBudgetNoVarificationCodeWithPaymentMethodDISCOUNTBodyContainsMEAN_OF_PAYMENT() throws Exception {

        setDefaultConfigurationForTest(PaymentType.BUDGET.type());

        String paymentConfig = PaymentMethod.DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,true);

        Customer customer = createMemberWithBudget();

        int pointsAfter = v4Builder.getMembersCredit(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);


        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(PaymentMethod.MEAN_OF_PAYMENT);

        //build purchase
        int sumToPay = 5000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        Assert.assertEquals("Error code must be " + PosErrorsEnum.PROPRIETARY_PAYMENT_METHOD_IS_NOT_ALLOWED.getCode(),PosErrorsEnum.PROPRIETARY_PAYMENT_METHOD_IS_NOT_ALLOWED.getCode(), getError(paymentResponse).get(CODE));
        Assert.assertNotNull(MessageFormat.format(getError(paymentResponse).get(MESSAGE).toString(),((PaymentAPIResponse)paymentResponse).getType(),paymentConfig), ((PaymentAPIResponse)paymentResponse).getPayments());

    }



    /**
     * C5247
     * */
    @Test
    public void testPayTypeBudgetNoVarificationCodeWithPaymentMethodMEAN_OF_PAYMENTBodyContainsMEAN_OF_PAYMENT() throws Exception {

        setDefaultConfigurationForTest(PaymentType.BUDGET.type());
        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT.toString();
        setPaymentProviderConfiguration(paymentConfig,true);

        Customer customer = createMemberWithBudget();

        int pointsAfter = v4Builder.getMembersCredit(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT;
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(paymentMethod);

        //build purchase
        int sumToPay = 5000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);

        assertResponse(payment, paymentResponse,paymentConfig);
        Assert.assertTrue("Type should be 'memberCredit'" ,paymentResponse.getType().contains("Credit"));

        //Verify with DataStore
        serverHelper.checkResultsForPay(payment, paymentResponse,dataStore);
    }

    /**
     * C5196
     * total sum > Member's amount of balance
     * */
    @Test
    public void testPayTypeBudgetNoVarificationCodeWithAllowPartialPaymentFalseMemberInsufficienBalance1() throws Exception {

        setConfigurationForTest("false",PaymentType.BUDGET.type(),"1","false");

        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,false);

        Customer customer = createMemberWithBudget("1000");
        int pointsAfter = v4Builder.getMembersCredit(customer);
        Assert.assertEquals("Customer needs to receive 10 points", 1000,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS, paymentResponse);
    }

    /**
     * C5196
     * member have a zero points balance.
     * */
    @Test
    public void testPayTypeBudgetNoVarificationCodeWithAllowPartialPaymentFalseMemberZeroBalance2() throws Exception {

        setConfigurationForTest("false",PaymentType.BUDGET.type(),"1","false");

        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,false);

        Customer customer = createMemberWithBudget("0");
        int pointsAfter = v4Builder.getMembersCredit(customer);
        Assert.assertEquals("Customer needs to receive 0 points", 0,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);


        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS, paymentResponse);
    }

    /**
     * C5196
     * new member have an empty balance.
     * */
    @Test
    public void testPayTypeBudgetNoVarificationCodeWithAllowPartialPaymentFalseMemberEmptyBalance3() throws Exception {

        setConfigurationForTest("false",PaymentType.BUDGET.type(),"1","false");

        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),false);


        JoinClubResponse member = serverHelper.createMember();
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS, paymentResponse);
    }


    /**
     * C5196
     * member's balance to be negative
     * */
    @Test
    public void testPayTypeBudgetNoVarificationCodeWithAllowPartialPaymentFalseMemberNegativenBalance4() throws Exception {

        setConfigurationForTest("false",PaymentType.BUDGET.type(),"1","true");

        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),false);

        Customer customer = createMemberWithBudget("-1000");
        int pointsAfter = v4Builder.getMembersCredit(customer);
        Assert.assertEquals("Customer needs to have -100 points", -1000,pointsAfter);

        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS, paymentResponse);
    }

    /**
     * C5190
     * sum > Member's amount of balance
     * */
    @Test
    public void testPayTypeBudgetNoVarificationCodeWithAllowPartialPaymentTrueMemberInsufficienBalance1() throws Exception {

        setConfigurationForTest("false",PaymentType.BUDGET.type(),"1","true");

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;

        setPaymentProviderConfiguration(paymentMethod.toString(),true);

        Customer customer = createMemberWithBudget();
        int pointsAfter = v4Builder.getMembersCredit(customer);
        Assert.assertEquals("Customer needs to have 100 points", 10000,pointsAfter);


        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 40000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);

        Assert.assertEquals("status code must be 200",httpStatusCode200, paymentResponse.getStatusCode());
        Assert.assertEquals("status must be ok", STATUS_OK, paymentResponse.getStatus());
        Assert.assertNotNull("Payments list should not be null", paymentResponse.getPayments());
        PaymentReturned paymentReturned = paymentResponse.getPayments().get(0);
        Assert.assertEquals("Actual Charge not equals",(pointsAfter * -1), paymentReturned.getAmount());
        Assert.assertEquals("paymentMethod should be the same", paymentMethod.toString(), paymentReturned.getPaymentMethod().toString());


        Assert.assertTrue("Type should be 'memberCredit'" ,paymentResponse.getType().contains("Credit"));
        //if ratio=1 we expect same amount in both nonMonetary and monetary
        Assert.assertEquals("Balance should be same in nonMonetary and monetary: " + paymentResponse.getUpdatedBalance().getMonetary() ,paymentResponse.getUpdatedBalance().getMonetary(),paymentResponse.getUpdatedBalance().getNonMonetary());

        //Verify with DataStore
        serverHelper.checkResultsForPay(payment, paymentResponse,dataStore);
    }

    /**
     * C5190
     * member have a zero points balance.
     * */
    @Test
    public void testPayTypeBudgetNoVarificationCodeWithAllowPartialPaymentTrueMemberZeroBalance2() throws Exception {

        setConfigurationForTest("false",PaymentType.BUDGET.type(),"1","false");

        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        setPaymentProviderConfiguration(paymentMethod.toString(),true);

        Customer customer = createMemberWithBudget("0");
        int pointsAfter = v4Builder.getMembersCredit(customer);
        Assert.assertEquals("Customer needs to receive 0 points", 0,pointsAfter);


        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS, paymentResponse);
    }


    /**
     * C5190
     * new member have an empty balance.
     * */
    @Test
    @Category({ServerRegression.class})
    public void testPayTypeBudgetNoVarificationCodeWithAllowPartialPaymentTrueMemberEmptyBalance3() throws Exception {

        setConfigurationForTest("false",PaymentType.BUDGET.type(),"1","true");

        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        setPaymentProviderConfiguration(paymentMethod.toString(),true);

        JoinClubResponse member = serverHelper.createMember();
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());

        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS, paymentResponse);
    }
    /**
     * C5190
     * member's balance to be negative
     * */
    @Test
    public void testPayTypeBudgetNoVarificationCodeWithAllowPartialPaymentTrueMemberNegativeBalance4() throws Exception {

        setConfigurationForTest("false",PaymentType.BUDGET.type(),"1","true");

        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        setPaymentProviderConfiguration(paymentMethod.toString(),true);

        Customer customer = createMemberWithBudget("-1000");
        int pointsAfter = v4Builder.getMembersCredit(customer);
        Assert.assertEquals("Customer needs to have -100 points", -1000,pointsAfter);

        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS, paymentResponse);
    }


    /****************************  Decimal point in Payment ******************************************/


    /**
     *
     Decimal point in Query mode (Ratio 1)
     * */
    @Test
    public void testDecimalPointInQueryMode () throws Exception {

        setDefaultConfigurationForTest(PaymentType.POINTS.type());
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.DISCOUNT;
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(paymentMethod);
        queryParams.setMode(Mode.QUERY);

       //build purchase
        int sumToPay = -5000;
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(500,2);
        Payment paymentObj1 = v4Builder.buildPayment(Type.CASH,0);
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, Lists.newArrayList(purchaseItem1),Lists.newArrayList(paymentObj1),100) ;
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        assertQueryModeResponse(payment, paymentResponse,PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString());

        Assert.assertTrue("Type should be 'memberPoints'" ,paymentResponse.getType().contains(PaymentType.POINTS.type()));
        //if ratio=1 we expect same amount in both nonMonetary and monetary
        Assert.assertEquals("Balance should be same in nonMonetary and monetary: " + paymentResponse.getUpdatedBalance().getMonetary() ,paymentResponse.getUpdatedBalance().getMonetary(),paymentResponse.getUpdatedBalance().getNonMonetary());

        //Verify with DataStore
        serverHelper.updateBusinessBackend(LocationBackendFields.DECIMAL_MODE.key(),true);
        PaymentAPIResponse paymentResponse2 = getPaymentAPIResponse(payment);
        int memberDecimal = paymentResponse.getUpdatedBalance().getNonMonetary();

        Assert.assertEquals("Member points return with decimal point",memberDecimal,paymentResponse2.getUpdatedBalance().getNonMonetary());

    }

    /**
     *
     Decimal point in Query mode (Ratio 0.2))
     * */

    @Test
    @Category({ServerRegression.class})
    public void testDecimalPointInQueryModeWithRatio () throws Exception {

        setConfigurationForTest("false",PaymentType.POINTS.type(),"0.2","true");
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 50000,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.DISCOUNT;
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(paymentMethod);
        queryParams.setMode(Mode.QUERY);

        //build purchase
        int sumToPay = -5000;
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(500,2);
        Payment paymentObj1 = v4Builder.buildPayment(Type.CASH,0);
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, Lists.newArrayList(purchaseItem1),Lists.newArrayList(paymentObj1),100) ;
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        assertQueryModeResponse(payment, paymentResponse,PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString());

        Assert.assertTrue("Type should be 'memberPoints'" ,paymentResponse.getType().contains(PaymentType.POINTS.type()));

        //Verify with DataStore
        serverHelper.updateBusinessBackend(LocationBackendFields.DECIMAL_MODE.key(),true);
        PaymentAPIResponse paymentResponse2 = getPaymentAPIResponse(payment);
        int memberDecimal = paymentResponse.getUpdatedBalance().getNonMonetary();

        Assert.assertEquals("Member points return with decimal point",memberDecimal,paymentResponse2.getUpdatedBalance().getNonMonetary());

    }

    /**
     *
     Decimal point in Pay mode (Ratio 1)
     * */
    @Test
    public void testDecimalPointInPayMode () throws Exception {

        setDefaultConfigurationForTest(PaymentType.POINTS.type());
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.DISCOUNT;
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(paymentMethod);

        //build purchase
        int sumToPay = -5000;
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(500,2);
        Payment paymentObj1 = v4Builder.buildPayment(Type.CASH,0);
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, Lists.newArrayList(purchaseItem1),Lists.newArrayList(paymentObj1),100) ;
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        assertResponse(payment, paymentResponse,PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString());

        Assert.assertTrue("Type should be 'memberPoints'" ,paymentResponse.getType().contains(PaymentType.POINTS.type()));
        //if ratio=1 we expect same amount in both nonMonetary and monetary
        Assert.assertEquals("Balance should be same in nonMonetary and monetary: " + paymentResponse.getUpdatedBalance().getMonetary() ,paymentResponse.getUpdatedBalance().getMonetary(),paymentResponse.getUpdatedBalance().getNonMonetary());

        //Verify with DataStore
        serverHelper.checkResultsForPay(payment, paymentResponse,dataStore);
        serverHelper.updateBusinessBackend(LocationBackendFields.DECIMAL_MODE.key(),true);
        PaymentAPIResponse paymentResponse2 = getPaymentAPIResponse(payment);
        int sumPositive = Math.abs(sumToPay);
        int memberDecimal = paymentResponse.getUpdatedBalance().getNonMonetary()+sumPositive;

        Assert.assertEquals("Member points return with decimal point",memberDecimal,paymentResponse2.getUpdatedBalance().getNonMonetary());

    }

    /**
     *
     Decimal point in Pay mode (Ratio 0.2))
     * */

    @Test
    public void testDecimalPointInPayModeWithRatio () throws Exception {

        double ratio = 0.2;
        setConfigurationForTest("false",PaymentType.POINTS.type(),String.valueOf(ratio),"true");
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 50000,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.DISCOUNT;
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(paymentMethod);

        //build purchase
        int sumToPay = -5000;
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(500,2);
        Payment paymentObj1 = v4Builder.buildPayment(Type.CASH,0);
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, Lists.newArrayList(purchaseItem1),Lists.newArrayList(paymentObj1),100) ;
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        assertResponse(payment, paymentResponse,PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString());

        Assert.assertTrue("Type should be 'memberPoints'" ,paymentResponse.getType().contains(PaymentType.POINTS.type()));

        //Verify with DataStore
        serverHelper.checkResultsForPay(payment, paymentResponse,dataStore);
        serverHelper.updateBusinessBackend(LocationBackendFields.DECIMAL_MODE.key(),true);
        PaymentAPIResponse paymentResponse2 = getPaymentAPIResponse(payment);
        double sumPositive = Math.abs(sumToPay)*ratio;
        int memberDecimal = paymentResponse.getUpdatedBalance().getNonMonetary()+(int) sumPositive;

        Assert.assertEquals("Member points return with decimal point",memberDecimal,paymentResponse2.getUpdatedBalance().getNonMonetary());

    }

    /**
     *
     Decimal budget in Pay mode (Ratio 1)
     * */

    @Test
    @Category({ServerRegression.class})
    public void testDecimalBudgetInPayMode () throws Exception {

        setDefaultConfigurationForTest(PaymentType.BUDGET.type());
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.DISCOUNT;
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(paymentMethod);

        //build purchase
        int sumToPay = -5000;
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(500,2);
        Payment paymentObj1 = v4Builder.buildPayment(Type.CASH,0);
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, Lists.newArrayList(purchaseItem1),Lists.newArrayList(paymentObj1),100) ;
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        assertResponse(payment, paymentResponse,PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString());

        Assert.assertTrue("Type should be 'memberCredit'" ,paymentResponse.getType().contains("Credit"));
        //if ratio=1 we expect same amount in both nonMonetary and monetary
        Assert.assertEquals("Balance should be same in nonMonetary and monetary: " + paymentResponse.getUpdatedBalance().getMonetary() ,paymentResponse.getUpdatedBalance().getMonetary(),paymentResponse.getUpdatedBalance().getNonMonetary());

        //Verify with DataStore
        serverHelper.checkResultsForPay(payment, paymentResponse,dataStore);
        serverHelper.updateBusinessBackend(LocationBackendFields.DECIMAL_MODE.key(),true);
        PaymentAPIResponse paymentResponse2 = getPaymentAPIResponse(payment);
        int sumPositive = Math.abs(sumToPay);
        int memberDecimal = paymentResponse.getUpdatedBalance().getNonMonetary()+sumPositive;

        Assert.assertEquals("Member points return with decimal point",memberDecimal,paymentResponse2.getUpdatedBalance().getNonMonetary());

    }

    /**
     *
     Decimal budget in Pay mode (Ratio 0.2))
     * */

    @Test
    public void testDecimalBudgetInPayModeWithRatio () throws Exception {

        double ratio = 0.2;
        setConfigurationForTest("false",PaymentType.BUDGET.type(),String.valueOf(ratio),"true");
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);

        Customer customer = createMemberWithBudget();
        Thread.sleep(10000);
        int pointsAfter = v4Builder.getMembersCredit(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 50000,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.DISCOUNT;
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(paymentMethod);

        //build purchase
        int sumToPay = -5000;
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(500,2);
        Payment paymentObj1 = v4Builder.buildPayment(Type.CASH,0);
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, Lists.newArrayList(purchaseItem1),Lists.newArrayList(paymentObj1),100) ;
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        assertResponse(payment, paymentResponse,PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString());

        Assert.assertTrue("Type should be 'memberCredit'" ,paymentResponse.getType().contains("Credit"));

        //Verify with DataStore
        serverHelper.checkResultsForPay(payment, paymentResponse,dataStore);
        serverHelper.updateBusinessBackend(LocationBackendFields.DECIMAL_MODE.key(),true);
        PaymentAPIResponse paymentResponse2 = getPaymentAPIResponse(payment);
        double sumPositive = Math.abs(sumToPay)*ratio;
        int memberDecimal = paymentResponse.getUpdatedBalance().getNonMonetary()+(int) sumPositive;

        Assert.assertEquals("Member points return with decimal point",memberDecimal,paymentResponse2.getUpdatedBalance().getNonMonetary());

    }

    /**
     *
     Decimal budget in Query mode (Ratio 0.2))
     * */

    @Test
    @Category({ServerRegression.class})
    public void testDecimalBudgetInQueryModeWithRatio () throws Exception {

        setConfigurationForTest("false",PaymentType.BUDGET.type(),"0.2","true");
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);

        Customer customer = createMemberWithBudget();
        int pointsAfter = v4Builder.getMembersCredit(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 50000,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.DISCOUNT;
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(paymentMethod);
        queryParams.setMode(Mode.QUERY);

        //build purchase
        int sumToPay = -5000;
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(500,2);
        Payment paymentObj1 = v4Builder.buildPayment(Type.CASH,0);
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, Lists.newArrayList(purchaseItem1),Lists.newArrayList(paymentObj1),100) ;
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        assertQueryModeResponse(payment, paymentResponse,PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString());

        Assert.assertTrue("Type should be 'memberCredit'" ,paymentResponse.getType().contains("Credit"));

        //Verify with DataStore
        serverHelper.updateBusinessBackend(LocationBackendFields.DECIMAL_MODE.key(),true);
        PaymentAPIResponse paymentResponse2 = getPaymentAPIResponse(payment);
        int memberDecimal = paymentResponse.getUpdatedBalance().getNonMonetary();

        Assert.assertEquals("Member points return with decimal point",memberDecimal,paymentResponse2.getUpdatedBalance().getNonMonetary());

    }

    /**
     *
     Decimal budget in Query mode (Ratio 0.2) - DecimalMode = false)
     * */

    @Test
    public void testDecimalBudgetInQueryModeWithRatioDecimalModeFalse () throws Exception {

        setConfigurationForTest("false",PaymentType.BUDGET.type(),"0.2","true");
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);
        serverHelper.updateBusinessBackend(LocationBackendFields.DECIMAL_MODE.key(),false);

        Customer customer = createMemberWithBudget();
        int pointsAfter = v4Builder.getMembersCredit(customer);
        Thread.sleep(10000);
        Assert.assertEquals("Customer needs to receive 100 points", 5000000,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.DISCOUNT;
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(paymentMethod);
        queryParams.setMode(Mode.QUERY);

        //build purchase
        int sumToPay = -5000;
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(500,2);
        Payment paymentObj1 = v4Builder.buildPayment(Type.CASH,0);
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, Lists.newArrayList(purchaseItem1),Lists.newArrayList(paymentObj1),100) ;
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        assertQueryModeResponse(payment, paymentResponse,PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString());

        Assert.assertTrue("Type should be 'memberCredit'" ,paymentResponse.getType().contains("Credit"));


        Assert.assertEquals("Member points return with decimal point",5000000,paymentResponse.getUpdatedBalance().getMonetary());
        serverHelper.updateBusinessBackend(LocationBackendFields.DECIMAL_MODE.key(),true);

    }

    /**
     *
     Decimal budget in Pay mode (Ratio 0.2) - DecimalMode = false)
     * */

    @Test
    public void testDecimalBudgetInPayModeWithRatioDecimalModeFalse () throws Exception {

        setConfigurationForTest("false",PaymentType.BUDGET.type(),"0.2","true");
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);
        serverHelper.updateBusinessBackend(LocationBackendFields.DECIMAL_MODE.key(),false);

        Customer customer = createMemberWithBudget();
        int pointsAfter = v4Builder.getMembersCredit(customer);
        Thread.sleep(10000);
        Assert.assertEquals("Customer needs to receive 100 points", 5000000,pointsAfter);

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.DISCOUNT;
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(paymentMethod);

        //build purchase
        int sumToPay = -5000;
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(500,2);
        Payment paymentObj1 = v4Builder.buildPayment(Type.CASH,0);
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, Lists.newArrayList(purchaseItem1),Lists.newArrayList(paymentObj1),100) ;
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        assertResponse(payment, paymentResponse,PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString());

        Assert.assertTrue("Type should be 'memberCredit'" ,paymentResponse.getType().contains("Credit"));

        //Verify with DataStore
        serverHelper.checkResultsForPay(payment, paymentResponse,dataStore);

        Assert.assertEquals("Member points return with decimal point",5500000,paymentResponse.getUpdatedBalance().getMonetary());
        serverHelper.updateBusinessBackend(LocationBackendFields.DECIMAL_MODE.key(),true);

    }

    /**
     * Test Gift card with no verification code
     * */
    @Test
    public void testPayTypeBudgetNoVerificationCode() throws Exception {

        setConfigurationForTest("true",PaymentType.BUDGET.type(),"1","false");

        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,false);

        Customer customer = createMemberWithBudget("0");

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);
        payment.setVerificationCode("");

        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_CODE_REQUIRED, paymentResponse);
    }

    /**
     * Test Gift card with invalid verification code
     * */
    @Test
    @Category({ServerRegression.class})
    public void testPayTypeBudgetInvalidVerificationCode() throws Exception {

        setConfigurationForTest("true",PaymentType.BUDGET.type(),"1","false");

        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,false);

        Customer customer = createMemberWithBudget("0");

        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;
        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod,Mode.PAY);

        //build purchase
        int sumToPay = 4000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);
        payment.setVerificationCode("21321321");

        log4j.info("Send payment request");
        IServerResponse paymentResponse = payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        if(paymentResponse instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse) paymentResponse).getErrorMessage());
        }

        validateErrorMessage(PosErrorsEnum.PROPRIETARY_PAYMENT_VERIFICATION_INVALID, paymentResponse);
    }



    private GetShortUserIDResponse getShortUserIDResponse(String phoneNumber) {
        String userToken = serverHelper.createNewMemberGetUserToken(phoneNumber, ServerHelper.APP);
        GetShortUserID getShortUserID = new GetShortUserID(userToken,locationId);
        IServerResponse response = getShortUserID.sendRequest(GetShortUserIDResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response).getErrorMessage(), false);
        }
        return ((GetShortUserIDResponse)response);
    }

    private void assertResponse(PaymentAPI payment, PaymentAPIResponse response,String paymentConfig) {
        log4j.info("Check Payment response");
        Assert.assertEquals("status code must be 200",httpStatusCode200, response.getStatusCode());
        Assert.assertEquals("status must be ok", STATUS_OK, response.getStatus());
        Assert.assertNotNull("Payments list should not be null", response.getPayments());
        Assert.assertEquals("Payments list should not be empty",1, response.getPayments().size());
        Assert.assertNotNull("Payments should not be null", response.getPayments().get(0));
        PaymentReturned paymentReturned = response.getPayments().get(0);
        Assert.assertEquals("Actual Charge not equals", (payment.getAmount() * -1), paymentReturned.getAmount());
        Assert.assertEquals("paymentMethod should be the same", paymentConfig, paymentReturned.getPaymentMethod().toString());

    }

    private void assertQueryModeResponse(PaymentAPI payment, PaymentAPIResponse response,String paymentConfig) {
        log4j.info("Check Payment query mode response");
        Assert.assertEquals("status code must be 200",httpStatusCode200, response.getStatusCode());
        Assert.assertEquals("status must be ok", STATUS_OK, response.getStatus());

    }

}
