package server.v4;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.serverCategories.ServerRegression;
import hub.base.categories.serverCategories.serverV4_0;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.common.AnonymousPurchaseTypes;
import server.common.ApiClientBuilder;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.utils.PosErrorsEnum;
import server.utils.V4Builder;
import server.v2_8.JoinClubResponse;
import server.v4.common.enums.Status;
import server.v4.common.models.Customer;
import server.v4.common.models.Purchase;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class CancelPurchaseTest extends BasePurchaseTest {

    private V4Builder v4Builder = new V4Builder(apiKey);


    @Test
    @Category({ServerRegression.class})
    public void testCancelPurchaseAndAllowAnalyzedPurchaseCancellationIsYes() throws Exception {

        //Preconditions - Save anonymous purchase = Always and AllowAnalyzedPurchaseCancellation = Yes
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(),AnonymousPurchaseTypes.ALWAYS.type()),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key(), true));


        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, v4Builder.buildSubmitPurchaseItemsList()) ;
        SubmitPurchase submitPurchase = submitPurchaseRequest(Lists.newArrayList(),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        SubmitPurchaseResponse response = (SubmitPurchaseResponse) submitPurchaseResponse(submitPurchase);

        //Verify with DataStore
        verifyPurchaseWithDataStore (submitPurchase, 1);

        Query q = getOueryForUserAction(submitPurchase.getPurchase().getTransactionId());
        List<Entity> result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: "+ submitPurchase.getPurchase().getTransactionId(),
                q, 1,dataStore);
        Assert.assertTrue("UID must be inside", serverHelper.dataContainsConfirmation(result, response.getConfirmation()));

        cancelPurchaseAndValidateResponse(submitPurchase, response.getConfirmation());
    }

    @Test
    @Category({ServerRegression.class})
    public void testCancelPurchaseAndAllowAnalyzedPurchaseCancellationIsNo() throws Exception {

        //Preconditions - Save anonymous purchase = Always and AllowAnalyzedPurchaseCancellation = Yes
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key(), false));


        //Assert that SaveAnonymousPurchaseOn = 'Always' in the dataStore and AllowAnalyzedPurchaseCancellation = 'No'
        List<Entity> result = serverHelper.getBusinessFromApiClient(dataStore);
        Assert.assertEquals("AllowAnalyzedPurchaseCancellation is set to false", false, result.get(0).getProperty(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key()));

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, v4Builder.buildSubmitPurchaseItemsList()) ;
        SubmitPurchase submitPurchase = submitPurchaseRequest(Lists.newArrayList(customer),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        SubmitPurchaseResponse response = (SubmitPurchaseResponse) submitPurchaseResponse(submitPurchase);

        //Verify Purchase with DataStore
        Query q = getQueryForPurchase();
        result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + transactionId, q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        Thread.sleep(10000);

        q = getOueryForUserAction(submitPurchase.getPurchase().getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: "+ submitPurchase.getPurchase().getTransactionId(),q, 1, dataStore);
        Assert.assertTrue("UID must be inside", serverHelper.dataContainsConfirmation(result, response.getConfirmation()));

        String errorMessage = PosErrorsEnum.PROPRIETARY_CANCEL_PURCHASE_NOT_CANCELABLE.getMessage();
        Assert.assertEquals(errorMessage,cancelPurchaseAndValidateResponseError(submitPurchase, response.getConfirmation()));

    }

    @Test
    @Category( serverV4_0.class )
    public void testCancelPurchaseWithOpenPurchaseStatus() throws Exception {

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, v4Builder.buildSubmitPurchaseItemsList()) ;
        // Initialize a new Purchase with status open
        purchase.setStatus(Status.OPEN);
        SubmitPurchase submitPurchase = submitPurchaseRequest(Lists.newArrayList(customer),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("status","open");
        submitPurchase.setQueryParameters(queryParams);
        SubmitPurchaseResponse response = (SubmitPurchaseResponse) submitPurchaseResponseOpenStatus(submitPurchase);

        //Verify Purchase with DataStore
        Query q = getQueryForPurchase();
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + transactionId, q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        String errorMessage = PosErrorsEnum.PROPRIETARY_CANCEL_CONFIRMATION_EMPTY.getMessage();
        Assert.assertEquals(errorMessage,cancelPurchaseAndValidateResponseError(submitPurchase, response.getConfirmation()));
    }

    private void cancelPurchaseAndValidateResponse (SubmitPurchase submitPurchase, String confirmation) throws InterruptedException {

        CancelPurchase cancelPurchase = new CancelPurchase();
        cancelPurchase.setBranchId(submitPurchase.getBranchId());
        cancelPurchase.setPosId(submitPurchase.getPosId());
        cancelPurchase.setxSourceName("name");
        cancelPurchase.setxSourceType("type");
        cancelPurchase.setxSourceVersion("version");
        cancelPurchase.setConfirmation(confirmation);
        IServerResponse response = cancelPurchase.SendRequestByApiKey(apiKey, CancelPaymentResponse.class);

        Assert.assertEquals("Status must be ok", "ok", ((CancelPaymentResponse) response).getStatus());

        //Verify if the transaction status is Cancelled = true
        Query q = getQueryForPurchase();
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: "+ transactionId,
                q, 1, dataStore);
        Assert.assertEquals("Cancelled status must be true", true, result.get(0).getProperty("Cancelled"));

    }



    private String cancelPurchaseAndValidateResponseError (SubmitPurchase submitPurchase, String confirmation) throws InterruptedException {

        Thread.sleep(5000);
        CancelPurchase cancelPurchase = new CancelPurchase();
        cancelPurchase.setBranchId(submitPurchase.getBranchId());
        cancelPurchase.setPosId(submitPurchase.getPosId());
        cancelPurchase.setxSourceName("name");
        cancelPurchase.setxSourceType("type");
        cancelPurchase.setxSourceVersion("version");
        cancelPurchase.setConfirmation(confirmation);
        IServerResponse response = cancelPurchase.SendRequestByApiKey(apiKey, CancelPaymentResponse.class);

        Assert.assertEquals("Status must be error", "error", ((CancelPaymentResponse) response).getStatus());

        //Verify if the transaction status is Cancelled = false
        Query q = getQueryForPurchase();
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: "+ transactionId,
                q, 1, dataStore);
        Assert.assertEquals("Cancelled status must be false", false, result.get(0).getProperty("Cancelled"));
        return getError(response).get(MESSAGE).toString();
    }


}
