package server.v4;

import hub.base.categories.hub1Categories.Hub1Regression;
import hub.base.categories.serverCategories.ServerRegression;
import hub.common.objects.common.YesNoList;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartAutomation;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationActions;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.Unregister;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.*;
import hub.hub1_0.services.automations.SmartAutomationService;
import hub.services.member.MembersService;
import integration.base.BaseIntegrationTest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.common.IServerResponse;
import server.common.InvokeEventBasedActionRuleBuilder;
import server.common.PaymentType;
import server.common.Response;
import server.common.asset.AssignAsset;
import server.utils.Initializer;
import server.utils.PosErrorsEnum;
import server.utils.V4Builder;
import server.v2_8.JoinClubResponse;
import server.v2_8.RegisterResponse;
import server.v4.base.ZappServerErrorResponse;
import server.v4.common.enums.PaymentMethod;
import server.v4.common.enums.ReturnAssetsStatus;
import server.v4.common.models.Asset;
import server.v4.common.models.Customer;
import server.v4.common.models.MemberDetailsQueryParams;
import server.v4.common.models.PaymentProviderConfigurations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.invoke.MethodHandles;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static java.lang.System.currentTimeMillis;

/**
 * Created by Goni on 7/10/2017.
 */

public class GetMemberDetailsTest extends BaseIntegrationTest {

    private V4Builder v4Builder = new V4Builder(apiKey);
    public final String ERRORS = "errors";
    public final String CODE = "code";
    public final String MESSAGE = "message";
    public static CharSequence timestamp;
    private final String USER_AGENT = "Mozilla/5.0";

    public static boolean updateConfiguration = true;

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
        serverHelper.setRegistrationConfigurationV2();
    }

    /*****************
     *  test CNP-6219	Image url - include the asset image
     */

    @Test
    public void testCreateNewGiftAndCheckImageIsrReturned() throws Exception{

        timestamp = String.valueOf(System.currentTimeMillis());
        SmartGift smartGift = hubAssetsCreator.createSmartGift(timestamp);
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send asset
        hubAssetsCreator.performAnActionSendGift(smartGift);
        GetMemberDetailsResponse getMemberDetailsResponse = getMemberDetails(createCustomer(member.getPhoneNumber()));
        checkImages(getMemberDetailsResponse.getMembership().getAssets());
    }


    @Test
    public void testCreateNewGiftAndCheckImageIsrReturnedFewGifts() throws Exception{

        timestamp = String.valueOf(System.currentTimeMillis());
        SmartGift smartGift = hubAssetsCreator.createSmartGift(timestamp);
        SmartGift smartGift1 = hubAssetsCreator.createSmartGift(timestamp);
        SmartGift smartGift2 = hubAssetsCreator.createSmartGift(timestamp);
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Send asset
        hubAssetsCreator.performAnActionSendGift(smartGift);
        hubAssetsCreator.performAnActionSendGift(smartGift1);
        hubAssetsCreator.performAnActionSendGift(smartGift2);
        GetMemberDetailsResponse getMemberDetailsResponse = getMemberDetails(createCustomer(member.getPhoneNumber()));
        checkImages(getMemberDetailsResponse.getMembership().getAssets());

    }


    /***
     * Test C5982 - membership with deleted status
     ***/

    @Test
    public void testGetMemberDetailsWithDeletedStatus() throws Exception{

        //Create member
        RegisterResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String userKey = newMember.getUserKey();
        String membershipKey = newMember.getMembershipKey();
        performDeleteMember(userKey,membershipKey);

        //wait 10 sec for update that member was deleted
        Thread.sleep(10000);
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        getMemberDetailsNegative(customer);

    }



    /***
     * C5983 - member without expiration date
     ***/

    @Test
    public void testGetMemberDetailsWithMemberWithoutExpirationDate() throws Exception {

        //Create member
        RegisterResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        getMemberDetailsNoExpiration(customer, newMember.getPhoneNumber());

    }

    /***
     * C5984 member with expiration date = today
     ***/
    @Test
    public void testGetMemberDetailsWithExpirationToday() throws Exception {

        Calendar calNextYear = Calendar.getInstance();
        CharSequence timestamp = String.valueOf(System.currentTimeMillis());

        //Create member
        Map<String,Object> fieldsMap = new HashMap<>();
        fieldsMap.put(Initializer.PHONE_NUMBER,timestamp.toString());
        fieldsMap.put(Initializer.FIRST_NAME,AUTO_USER_PREFIX + timestamp.toString());
        fieldsMap.put(Initializer.LAST_NAME,AUTO_LAST_PREFIX + timestamp.toString());
        fieldsMap.put(Initializer.EXPIRATION_DATE,getDate(calNextYear));
        RegisterResponse newMember = createMember(fieldsMap,updateConfiguration);
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        getMemberDetails(customer, newMember.getPhoneNumber(), "Active");
    }

    public String getDate(Calendar cal) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        String day = dateFormat.format(cal.getTime());
        return day;
    }


    /***
     * C5985 member with expiration date < today
     ***/
    @Test
    public void testGetMemberDetailsWithExpirationBeforeToday() throws Exception {

        Calendar calPrevYear = Calendar.getInstance();
        calPrevYear.add(Calendar.YEAR, -1);
        CharSequence timestamp = String.valueOf(System.currentTimeMillis());

        //Create member
        Map<String,Object> fieldsMap = new HashMap<>();
        fieldsMap.put(Initializer.PHONE_NUMBER,timestamp.toString());
        fieldsMap.put(Initializer.FIRST_NAME,AUTO_USER_PREFIX + timestamp.toString());
        fieldsMap.put(Initializer.LAST_NAME,AUTO_LAST_PREFIX + timestamp.toString());
        fieldsMap.put(Initializer.EXPIRATION_DATE,getDate(calPrevYear));
        RegisterResponse newMember = createMember(fieldsMap,updateConfiguration);
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        getMemberDetails(customer, newMember.getPhoneNumber(), "Expired");
    }

    /***
     * C5985 member with expiration date > today
     ***/
    @Test
    @Category(ServerRegression.class)
    public void testGetMemberDetailsWithExpirationAfterToday() throws Exception {

        Calendar nextYear = Calendar.getInstance();
        nextYear.add(Calendar.YEAR, +1);
        CharSequence timestamp = String.valueOf(System.currentTimeMillis());

        //Create member
        Map<String,Object> fieldsMap = new HashMap<>();
        fieldsMap.put(Initializer.PHONE_NUMBER,timestamp.toString());
        fieldsMap.put(Initializer.FIRST_NAME,AUTO_USER_PREFIX + timestamp.toString());
        fieldsMap.put(Initializer.LAST_NAME,AUTO_LAST_PREFIX + timestamp.toString());
        fieldsMap.put(Initializer.EXPIRATION_DATE,getDate(nextYear));
        RegisterResponse newMember = createMember(fieldsMap,updateConfiguration);
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        getMemberDetails(customer, newMember.getPhoneNumber(), "Active");
    }

    /***
     * Test C6113 - customer not found
     ***/

    @Test
    @Category(ServerRegression.class)
    public void testGetMemberDetailsWithNotExistingCustomer() throws Exception {

        Customer customer = new Customer();
        customer.setPhoneNumber("1245454875458774");
        getMemberDetailsNegative(customer);
    }

    /***
     * Test C5987 Membership status is not_registered
     ***/
    @Test
    public void testConditionBirthdayIsBeforeTagMember() throws Exception {

        Calendar date = Calendar.getInstance();
        date.add(Calendar.YEAR, -20);
        CharSequence timestamp = String.valueOf(System.currentTimeMillis());

        //Create member
        Map<String,Object> fieldsMap = new HashMap<>();
        fieldsMap.put(Initializer.PHONE_NUMBER,timestamp.toString());
        fieldsMap.put(Initializer.FIRST_NAME,AUTO_USER_PREFIX + timestamp.toString());
        fieldsMap.put(Initializer.LAST_NAME,AUTO_LAST_PREFIX + timestamp.toString());
        fieldsMap.put(Initializer.BIRTHDAY,getDate(date));
        RegisterResponse newMember = createMember(fieldsMap,updateConfiguration);
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        // perform update to activate unregister action
        String userKey = newMember.getUserKey();
        String membershipKey = newMember.getMembershipKey();
        performUnregisterMember(userKey,membershipKey);

        //wait 10 sec for update that member was unregistered
        Thread.sleep(10000);

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        //Verify the status is Inactive
        getMemberDetails(customer, newMember.getPhoneNumber(), "Inactive");

    }

    /***
     * Test C6242 Asset data exist in GMD response
     ***/
    @Test
    public void testAssetDataExistInGetMemberDetailsResponse() throws Exception {

        //Create new member
        long currentTimeMillis = currentTimeMillis();
        String customerPhone = String.valueOf(currentTimeMillis);
        JoinClubResponse newMember = serverHelper.createMember(customerPhone);
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        String memberKey = newMember.getMembershipKey();
        //Assign asset to member
        server.v2_8.common.Asset newAsset = AssignAsset.assignAssetToMember("gift", "Auto", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetName = newAsset.getName();
        //set returnAssets=active options
        MemberDetailsQueryParams queryParams = new MemberDetailsQueryParams();
        queryParams.setReturnAssets(ReturnAssetsStatus.ASSET_STATUS_ACTIVE.toString());
        //get customer phone number
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        Thread.sleep(1000);
        //Send GetMemberDetails request
        GetMemberDetails getMemberDetails = v4Builder.buildMemberDetailsRequest(customer);
        getMemberDetails.setQueryParameters(queryParams.getParams());
        IServerResponse response = getMemberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);
        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response).getErrorMessage(), false);
        }
        // Cast to getMemberDetailsResponse object to fetch all response data
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) response;
        server.v4.common.models.Asset assignedAsset = getMemberDetailsResponse.getMembership().findAssetByName(assetName);
        Assert.assertNotNull("Asset shouldn't be null" , assignedAsset);

    }



    protected RegisterResponse createMember(Map<String,Object> fieldsMap,boolean updateConfiguration) throws InterruptedException {
        Thread.sleep(10000);
        RegisterResponse registerResponse = serverHelper.createNewMember(fieldsMap, updateConfiguration);
        Assert.assertNotNull(registerResponse);
        Assert.assertNotNull(registerResponse.getMembershipKey());
        Assert.assertNotNull(registerResponse.getMembershipStatus());
        Assert.assertNotNull(registerResponse.getUserKey());
        log4j.info("Creating member: " + registerResponse.getPhoneNumber());
        return registerResponse;
    }

    protected RegisterResponse createMember() throws InterruptedException {
        Thread.sleep(10000);
        long currentTimeMillis = currentTimeMillis();
        String customerPhone = String.valueOf(currentTimeMillis);
        log4j.info("Creating member with phone number: " + customerPhone);
        Map<String,Object> fieldsMap = new HashMap<>();
        fieldsMap.put(Initializer.PHONE_NUMBER,customerPhone);
        RegisterResponse registerResponse = serverHelper.createNewMember(fieldsMap, updateConfiguration);
        Assert.assertNotNull(registerResponse);
        Assert.assertNotNull(registerResponse.getMembershipKey());
        Assert.assertNotNull(registerResponse.getMembershipStatus());
        Assert.assertNotNull(registerResponse.getUserKey());
        updateConfiguration = false;
        return registerResponse;
    }
    @Test
    public void testGetMemberDetailsWith0balance() throws Exception{

        //Create member
        RegisterResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());


        Thread.sleep(10000);
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        getMemberDetails0Balance(customer);

    }

    /***
     * Test  - membership with Points balance
     ***/

    @Test
    @Category(ServerRegression.class)
    public void testGetMemberDetailsWithBalance() throws Exception{

        serverHelper.setConfigurationForPayment("false",PaymentType.POINTS.type(),"1","true");

        String paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        PaymentProviderConfigurations providerConfigurations = v4Builder.buildPaymentProviderConfigurations(paymentMethod,true);
        serverHelper.saveApiClient("PaymentProviderConfigurations" ,providerConfigurations);

        String amountOfPoints = "1000";

        //Create new customer with points
        Customer customer = serverHelper.createMemberWithPointsOrBudget(amountOfPoints, PaymentType.POINTS.type(),dataStore);
        getMemberDetailsWithPointsBalance(customer,amountOfPoints);

    }

    /***
     * Test  - membership with Credit balance and ratio of 0.2
     ***/

    @Test
    public void testGetMemberDetailsWithCreditBalanceAndRatio() throws Exception{

        serverHelper.setConfigurationForPayment("false",PaymentType.BUDGET.type(),"0.2","true");

        String paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        PaymentProviderConfigurations providerConfigurations = v4Builder.buildPaymentProviderConfigurations(paymentMethod,true);
        serverHelper.saveApiClient("PaymentProviderConfigurations" ,providerConfigurations);

        String amountOfCredit = "1000";
        String amountAfterRatioCalc = "5000";

        //Create new customer with points
        Customer customer = serverHelper.createMemberWithPointsOrBudget(amountOfCredit,PaymentType.BUDGET.type(),dataStore);
        getMemberDetailsWithCreditBalance(customer,amountAfterRatioCalc);

    }

    /***
     * Test - membership with Credit balance
     ***/

    @Test
    public void testGetMemberDetailsWithCreditBalance() throws Exception{

        serverHelper.setConfigurationForPayment("false",PaymentType.BUDGET.type(),"1","true");

        String paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        PaymentProviderConfigurations providerConfigurations = v4Builder.buildPaymentProviderConfigurations(paymentMethod,true);
        serverHelper.saveApiClient("PaymentProviderConfigurations" ,providerConfigurations);

        String amountOfCredit = "1000";

        //Create new customer with points
        Customer customer = serverHelper.createMemberWithPointsOrBudget(amountOfCredit,PaymentType.BUDGET.type(),dataStore);
        getMemberDetailsWithCreditBalance(customer,amountOfCredit);

    }

    /***
     * Test  - membership with Points balance and ratio of 0.2
     ***/

    @Test
    public void testGetMemberDetailsWithPointsBalanceAndRatio() throws Exception{

        serverHelper.setConfigurationForPayment("false",PaymentType.POINTS.type(),"0.2","true");

        String paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        PaymentProviderConfigurations providerConfigurations = v4Builder.buildPaymentProviderConfigurations(paymentMethod,true);
        serverHelper.saveApiClient("PaymentProviderConfigurations" ,providerConfigurations);

        String amountOfCredit = "1000";
        String amountAfterRatioCalc = "5000";

        //Create new customer with points
        Customer customer = serverHelper.createMemberWithPointsOrBudget(amountOfCredit,PaymentType.POINTS.type(),dataStore);
        getMemberDetailsWithPointsBalance(customer,amountAfterRatioCalc);

    }

    /***
     * Test  - membership with negative Credit balance and ratio of 0.2
     ***/

    @Test
    public void testGetMemberDetailsWithNegativeCreditBalanceAndRatio() throws Exception{

        serverHelper.setConfigurationForPayment("false",PaymentType.BUDGET.type(),"0.2","true");

        String paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        PaymentProviderConfigurations providerConfigurations = v4Builder.buildPaymentProviderConfigurations(paymentMethod,true);
        serverHelper.saveApiClient("PaymentProviderConfigurations" ,providerConfigurations);

        String amountOfCredit = "-1000";
        String amountAfterRatioCalc = "-5000";

        //Create new customer with points
        Customer customer = serverHelper.createMemberWithPointsOrBudget(amountOfCredit,PaymentType.BUDGET.type(),dataStore);
        getMemberDetailsWithCreditBalance(customer,amountAfterRatioCalc);

    }

    /***
     * Test  - membership with defualt wallet
     ***/

    @Test
    @Category(ServerRegression.class)
    public void testGetMemberDetailsWithDefaultWallet() throws Exception{

        serverHelper.updateBusinessBackend("AllowNegativePointBalance","true");

        String amountOfCredit = "-1000";
        String amountAfterRatioCalc = "-1000";

        //Create new customer with points
        Customer customer = serverHelper.createMemberWithPointsOrBudget(amountOfCredit,PaymentType.POINTS.type(),dataStore);
        getMemberDetailsWithPointsBalance(customer,amountAfterRatioCalc);

    }

    private void getMemberDetailsNoExpiration(Customer customer, String phoneNumber) {

        IServerResponse response = v4Builder.getGetMemberDetailsNotValidateResponse(customer);
        if (response instanceof ZappServerErrorResponse)
            Assert.fail("Expected an error!");
        //Verify the returned customer is as expected
        Assert.assertEquals("Verify member", phoneNumber, ((GetMemberDetailsResponse) response).getMembership().getPhoneNumber());
        Assert.assertEquals("Verify expiration is not returned", null, ((GetMemberDetailsResponse) response).getMembership().getExpirationDate());
        Assert.assertEquals("Verify the status", "Active", ((GetMemberDetailsResponse) response).getMembership().getStatus());
    }

    private void getMemberDetails(Customer customer, String phoneNumber, String status) {

        IServerResponse response = v4Builder.getGetMemberDetailsNotValidateResponse(customer);
        if (response instanceof ZappServerErrorResponse)
            Assert.fail("Expected an error!");
        //Verify the returned customer is as expected
        Assert.assertEquals("Verify member", phoneNumber, ((GetMemberDetailsResponse) response).getMembership().getPhoneNumber());
        Assert.assertEquals("Verify the status", status, ((GetMemberDetailsResponse) response).getMembership().getStatus());
    }


    private void getMemberDetailsNegative(Customer customer) {

        IServerResponse response = v4Builder.getGetMemberDetailsNotValidateResponse(customer);
        log4j.info("GetMember details request");
        validateErrorMessage(PosErrorsEnum.SINGLE_CUSTOMER_NOT_FOUND,response);

    }
    private void getMemberDetails0Balance (Customer customer) throws InterruptedException {
        Thread.sleep(5000);
        IServerResponse response = v4Builder.getGetMemberDetailsNotValidateResponse(customer);
        if(response instanceof ZappServerErrorResponse)
            Assert.fail("Expected an error!");
        //Verify the returned customer is as expected
        Assert.assertEquals("Verify member",0,((GetMemberDetailsResponse)response).getMembership().getPointsBalance().getBalance().getMonetary());

    }


    protected void validateErrorMessage(PosErrorsEnum errorCode,IServerResponse response) {
        log4j.info("Validate error code: " + errorCode.getCode());
        String errorMessage = getError(response).get(MESSAGE).toString();
        Assert.assertTrue(errorCode.isValidMessageForCode(errorMessage));
    }

    protected JSONObject getError(IServerResponse response) {
        return ((JSONObject) ((JSONArray) (response.getRawObject().get(ERRORS))).get(0));
    }


    protected void performUpdateMemberDetails(NewMember member) throws Exception {
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setPhoneNumber(member.getPhoneNumber());
        memberDetails.setFirstName(member.getFirstName() + "_Updated");
        MembersService.updateMemberDetails(memberDetails);
        Thread.sleep(5000);
    }

    private void createSmartAutomationScenarioForUnregisterAction(SmartAutomation automation, ArrayList<String> tags) throws Exception {
        log4j.info("creating scenario");
        SmartAutomationScenario scenario = new SmartAutomationScenario();
        automation.addScenario(scenario);

        StringOperator equal = hubAssetsCreator.getStringOperator(automation, StringOperator.String_Equals_enum.EqualsTo);
        Condition cond = hubAssetsCreator.createConditionSelectValue(automation, ApplyIf.UPDATE_MEMBERSHIP_OPERATION, UpdateMembershipOperation.Operation, equal, UpdateMemberOperationValue.UpdateMembership.toString());
        scenario.addCondition(cond, automation);
        SmartAutomationAction  smartAction = new SmartAutomationAction();
        SmartAutomationAction updateSmartAction = getUnregisterAction(smartAction, automation, tags);
        scenario.addAction(updateSmartAction, automation);
        SmartAutomationService.createSmartAutomation(automation);

    }

    private SmartAutomationAction getUnregisterAction(SmartAutomationAction smartAction, SmartAutomation automation, ArrayList<String> actionTags) throws Exception {
        smartAction.setPerformTheAction(SmartAutomationActions.UnregisterMembership.toString());
        Unregister action = new Unregister(automation);
        action.setTags(actionTags, automation);
        smartAction.setUnregister(action);
        return smartAction;

    }

    private void getMemberDetailsWithPointsBalance (Customer customer, String requestedPoints) throws InterruptedException {
        Thread.sleep(5000);
        int response = v4Builder.getMembersPoints(customer);
        String response1 = String.valueOf(response);
        //Verify the returned customer is as expected
        Assert.assertEquals("Verify if the customer points are equals",requestedPoints,response1);
    }

    private void getMemberDetailsWithCreditBalance (Customer customer, String requestedPoints) throws InterruptedException {
        Thread.sleep(5000);
        int response = v4Builder.getMembersCredit(customer);
        String response1 = String.valueOf(response);
        //Verify the returned customer is as expected
        Assert.assertEquals("Verify if the customer credits are equals",requestedPoints,response1);
    }

    private void performDeleteMember(String userKey,String membershipKey) {
        try {
            InvokeEventBasedActionRuleBuilder invokeEventBasedActionRuleBuilder = new InvokeEventBasedActionRuleBuilder();
            server.common.InvokeEventBasedActionRule invokeEventBasedActionRule = invokeEventBasedActionRuleBuilder.buildDeleteRequest(membershipKey, userKey, locationId);
            IServerResponse response = invokeEventBasedActionRule.SendRequestByToken(serverToken, Response.class);

            // make sure the response is not Error response
            if (response instanceof ZappServerErrorResponse) {
                Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
            }
            log4j.info("User with userKey: " + userKey + " was deleted.");
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);

        }
    }

    private void performUnregisterMember(String userKey,String membershipKey) {
        try {
            InvokeEventBasedActionRuleBuilder invokeEventBasedActionRuleBuilder = new InvokeEventBasedActionRuleBuilder();
            server.common.InvokeEventBasedActionRule invokeEventBasedActionRule = invokeEventBasedActionRuleBuilder.buildUnregisterRequest(membershipKey, userKey, locationId);
            IServerResponse response = invokeEventBasedActionRule.SendRequestByToken(serverToken, Response.class);

            // make sure the response is not Error response
            if (response instanceof ZappServerErrorResponse) {
                Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
            }
            log4j.info("User with userKey: " + userKey + " was unregister.");
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);

        }

    }
    private void checkImages (ArrayList<Asset> assets) throws Exception{

        String imageURL;
        int size = assets.size();
        for (int i=0; i<size; i++) {
            imageURL = assets.get(i).getImage();
            sendGet(imageURL);
        }
    }

    // HTTP GET request
    private void sendGet(String url) throws Exception {

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        // optional default is GET
        con.setRequestMethod("GET");
        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);
        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();
        Assert.assertEquals("Status should be 200", 200,responseCode);
    }

    private Customer createCustomer (String phoneNumber){
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        return customer;
    }
    private GetMemberDetailsResponse getMemberDetails (Customer customer)throws Exception{

        Thread.sleep(5000);
        MemberDetailsQueryParams queryParams =  new MemberDetailsQueryParams();
        IServerResponse response = v4Builder.getMemberDetailsWithQueryParamsResponse(customer,queryParams);
        if(response instanceof server.v2_8.base.ZappServerErrorResponse)
            Assert.fail("Expected an error!");
        //Verify the returned customer is as expected
        Assert.assertEquals("",customer.getPhoneNumber(),((GetMemberDetailsResponse)response).getMembership().getPhoneNumber());
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) response;

        return getMemberDetailsResponse;

    }


}

