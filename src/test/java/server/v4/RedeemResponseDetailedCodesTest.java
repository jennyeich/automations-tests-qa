package server.v4;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import server.base.BaseServerTest;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.common.asset.AssignAsset;
import server.common.asset.NewAssetResponse;
import server.common.asset.conditions.AssetDateCondition;
import server.utils.RedeemErrorCodesMap;
import server.utils.V4Builder;
import server.v2_8.*;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Asset;
import server.v2_8.common.RedeemItem;
import server.v4.common.enums.ExpandTypes;
import server.v4.common.enums.ReturnedBenefits;
import server.v4.common.models.GetBenefitsQueryParams;
import server.v4.common.models.Purchase;
import server.v4.common.models.ReturnedRedeemAsset;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static utils.StringUtils.StringToKey;

/**
 * Created by Tatiana on 08/15/2017.
 */

public class RedeemResponseDetailedCodesTest extends BaseServerTest {

    public final String STATUS = "Status";
    public final String REDEEM_STATE = "RedeemState";
    private V4Builder v4Builder = new V4Builder(apiKey);

    private static String assetNotRedeemableAtThisLocation;

    @BeforeClass
    public static void _init() throws Exception {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        updateConfiguration = true;
        /**
         * We create a different location here to create assigned asset on other location to use in 2 tests.
         * After creating the asset, we set the firstTime variable to true and start the test again with new location.
         * We must varify that this test will run first in the suite.
         * */
        assetNotRedeemableAtThisLocation = getAssetFromDifferentLocation();
        firstTime = true;
        init();
        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),true);
        serverHelper.updateBusinessBackend("UniqueFields", Lists.newArrayList("GovID","PhoneNumber","MemberID").toArray());

    }


    // Check error code for second redeem
    @Test
    public void verifyRedeemCode5510ParalelRedeemLock() throws Exception {

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        JoinClubResponse newMember = createMember();

        Asset asset = AssignAsset.assignAssetToMember("gift", "None",newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();

        Key dataStoreAssetKey = StringToKey(assetKey);
        checkAssetStateInDB(dataStoreAssetKey);

        // Redeem first time and verify the error response for the second redeem request
        // create RedeemAsset fot getBenefits request
        server.v4.common.models.RedeemAsset redeemAsset = new server.v4.common.models.RedeemAsset();
        redeemAsset.setKey(assetKey);

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), ReturnedBenefits.REDEEM_ASSETS);

        //Get the redeemAsset response (first redeem)
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Status code must be ok","ok", getBenefitsResponse.getStatus());

        Purchase purchase = getBenefits.getPurchase();
        purchase.setTransactionId(UUID.randomUUID().toString());
        getBenefits.setPurchase(purchase);

        //second redeem
        GetBenefitsResponse getBenefitsResponse2 = getGetBenefitsResponse(getBenefits);

        ReturnedRedeemAsset returnedRedeemAsset = getBenefitsResponse2.getRedeemAssets().get(0);

        checkResponse(returnedRedeemAsset,RedeemErrorCodesMap.ASSET_IS_LOCKED_FOR_REDEMPTION);

        //make sure the property is set as true for rest of the tests
        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),true);



    }

    // Check error code for not redeemable asset for this location
    @Test
    public void verifyRedeemCode5509AssetNotRedeemableAtThisLocation() throws IOException, InterruptedException {

        // create RedeemAsset fot getBenefits request
        server.v4.common.models.RedeemAsset redeemAsset = new server.v4.common.models.RedeemAsset();
        //Asset key for existing asset at other location
        redeemAsset.setKey(assetNotRedeemableAtThisLocation);

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), ReturnedBenefits.REDEEM_ASSETS);

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        ReturnedRedeemAsset returnedRedeemAsset = getBenefitsResponse.getRedeemAssets().get(0);

        checkResponse(returnedRedeemAsset,RedeemErrorCodesMap.ASEET_NOT_REDEEMABLE_AT_LOCATION);
    }



    // Check error code for deactivated asset redeem
    @Test
    public void verifyRedeemCode5511AssetDeactivate() throws Exception {

        JoinClubResponse newMember = createMember();

        //create and deactivate asset
        Asset asset = AssignAsset.assignAssetToMember("gift", "None",newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();
        serverHelper.deactivateAsset(assetKey);

        //Verify with DataStore
        Key dataStoreAssetKey = StringToKey(assetKey);
        checkAssetStateInDB(dataStoreAssetKey);

        // create RedeemAsset fot getBenefits request
        server.v4.common.models.RedeemAsset redeemAsset = new server.v4.common.models.RedeemAsset();
        redeemAsset.setKey(assetKey);

        //Redeem deactivated asset
        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), ReturnedBenefits.REDEEM_ASSETS);

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        ReturnedRedeemAsset returnedRedeemAsset = getBenefitsResponse.getRedeemAssets().get(0);

        checkResponse(returnedRedeemAsset,RedeemErrorCodesMap.DEACTIVATE_ASSET);

    }

    // Check error code for redeem of redeemed asset
    @Test
    public void verifyRedeemCode5501AssetAlreadyRedeemed() throws Exception {

        //Redeem the asset
        String assetKey = redeemFromPOS();
        Thread.sleep(10000);
        //Redeem first time and verify the error response for the second redeem request
        // create RedeemAsset fot getBenefits request
        server.v4.common.models.RedeemAsset redeemAsset = new server.v4.common.models.RedeemAsset();
        redeemAsset.setKey(assetKey);

        //Redeem deactivated asset
        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), ReturnedBenefits.REDEEM_ASSETS);

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        ReturnedRedeemAsset returnedRedeemAsset = getBenefitsResponse.getRedeemAssets().get(0);

        checkResponse(returnedRedeemAsset,RedeemErrorCodesMap.ASSET_ALREADY_REDEEMED);

    }

    // Check error code for asset with wrong date
    @Test
    public void verifyRedeemCode5523DueDate() throws Exception {

        //Assign asset to member
        JoinClubResponse newMember = createMember();

        AssetDateCondition invalidDatesForAsset = serverHelper.getInvalidDatesForAsset();
        Asset asset = AssignAsset.assignAssetToMember("gift", "Bulk",newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber(), invalidDatesForAsset);
        String assetKey = asset.getKey();

        //Verify with DataStore
        Key dataStoreAssetKey = StringToKey(assetKey);
        checkAssetStateInDB(dataStoreAssetKey);

        //Redeem first time and verify the error response for the second redeem request
        // create RedeemAsset fot getBenefits request
        server.v4.common.models.RedeemAsset redeemAsset = new server.v4.common.models.RedeemAsset();
        redeemAsset.setKey(assetKey);

        //Redeem deactivated asset
        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), ReturnedBenefits.REDEEM_ASSETS);

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        ReturnedRedeemAsset returnedRedeemAsset = getBenefitsResponse.getRedeemAssets().get(0);

        checkResponse(returnedRedeemAsset,RedeemErrorCodesMap.VIOLATION_OF_ASSET_NO_BENEFITS);
    }

    // Check error code for redeem of not assigned asset
    //CNP-7185
    @Test
    public void verifyRedeemCode5525AssetNotFound() throws Exception {

        NewAssetResponse asset = serverHelper.buildAsset("gift","Bulk",1,locationId,serverToken,null);
        String assetKey = asset.getKey();

        // create RedeemAsset fot getBenefits request
        server.v4.common.models.RedeemAsset redeemAsset = new server.v4.common.models.RedeemAsset();
        redeemAsset.setKey(assetKey);

        //Redeem deactivated asset
        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), ReturnedBenefits.REDEEM_ASSETS);

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        ReturnedRedeemAsset returnedRedeemAsset = getBenefitsResponse.getRedeemAssets().get(0);

        checkResponse(returnedRedeemAsset, RedeemErrorCodesMap.ASSET_NOT_FOUND);
    }


    public String redeemFromPOS() throws Exception {

        //Assign asset to member
        JoinClubResponse newMember = serverHelper.createMember();

        //serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(), true);
        Asset asset = AssignAsset.assignAssetToMember("gift", "Auto", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();
        Key dataStoreAssetKey = StringToKey(assetKey);

        //Redeem asset
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(assetKey);
        redeemAsset.setMembershipKey(newMember.getMembershipKey());
        redeemAsset.setToken(serverToken);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) response;
        Assert.assertFalse("Response code must not be empty!", StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y", "Y", redeemAssetResponse.getResult());

        //Send redeem request in order to check redeem status
        RedeemItem redeemItem = new RedeemItem();
        String code = redeemAssetResponse.getCode().toString();
        redeemItem.setRedeemCode(code);

        Redeem redeem = new Redeem();
        redeem.setReturnExtendedItems(true);
        redeem.setMarkAsUsed(true);
        redeem.getRedeemItems().add(redeemItem);

        //Send the redeem request
        IServerResponse response2 = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (response2 instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response2).getErrorMessage());
        }

        Thread.sleep(10000);
        //Verify with DataStore
        Entity result3 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null", result3);
        Assert.assertEquals("Asset Status must be Redeemed", "Redeemed", result3.getProperty(STATUS));
        Assert.assertEquals("Asset RedeemState must be Burned", "Burned", result3.getProperty("RedeemState"));

        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action",Query.FilterOperator.EQUAL, "redeem"),
                new Query.FilterPredicate("AssetKey",Query.FilterOperator.EQUAL, dataStoreAssetKey))));

        List<Entity> resultUserAction = dataStore.findByQuery(qUserAction);
        Assert.assertEquals("Asset Source must be POS", "POS", resultUserAction.get(0).getProperty("Source"));

        return assetKey;
    }

    private void checkResponse(ReturnedRedeemAsset returnedRedeemAsset,String errorCode) {
        Assert.assertEquals("Verify the result is as expected: false",false, returnedRedeemAsset.isRedeemable());
        Assert.assertEquals("Verify error code is as expected: ", errorCode, returnedRedeemAsset.getNonRedeemableCause().getCode());
        Assert.assertEquals("Verify error message is as expected: ",RedeemErrorCodesMap.getErrorMessage(errorCode), returnedRedeemAsset.getNonRedeemableCause().getMessage());
    }

    private GetBenefitsResponse getGetBenefitsResponse(GetBenefits getBenefits) {
       return v4Builder.getMemberBenefitsResponse(getBenefits);
    }

    private GetBenefits createGetBenefitsRequestForRedeemAsset(ArrayList<server.v4.common.models.RedeemAsset> redeemAssets, ReturnedBenefits returnedBenefits) {
        GetBenefits getBenefits = new GetBenefits(apiKey);
        getBenefits.setRedeemAssets(redeemAssets);

        //add query parameters
        setQueryParams(Lists.newArrayList(ExpandTypes.DISCOUNT_BY_DISCOUNT.toString()),false, returnedBenefits.toString(),getBenefits);
        Purchase purchase = v4Builder.buildDefaultPurchase(UUID.randomUUID().toString());
        getBenefits.setPurchase(purchase);
        return getBenefits;
    }

    private void setQueryParams(List<String> expand, boolean markAssetsAsUsed, String returnBenefits, GetBenefits getBenefits) {
        GetBenefitsQueryParams queryParams =  new GetBenefitsQueryParams();
        queryParams.setExpand(Lists.newArrayList(expand));
        queryParams.setMarkAssetsAsUsed(markAssetsAsUsed);
        queryParams.setReturnBenefits(returnBenefits);
        getBenefits.setQueryParameters(queryParams.getParams());
    }

    private void checkAssetStateInDB (Key dataStoreAssetKey){

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned", "Assigned", result.getProperty(STATUS));
        Assert.assertEquals("Asset RedeemState must be Unused", "Unused", result.getProperty(REDEEM_STATE));
    }

}





