package server.v4;


import hub.common.objects.common.RegistrationField;
import hub.common.objects.member.NewMember;
import hub.common.objects.operations.*;
import hub.hub1_0.services.OperationService;
import hub.services.member.MembersService;
import integration.base.BaseIntegrationTest;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import server.common.IServerResponse;
import server.utils.PosErrorsEnum;
import server.utils.V4Builder;
import server.v2_8.RegisterResponse;
import server.v4.base.ZappServerErrorResponse;
import server.v4.common.enums.Mode;
import server.v4.common.enums.PaymentMethod;
import server.v4.common.models.Customer;
import server.v4.common.models.PaymentQueryParams;
import server.v4.common.models.Purchase;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.UUID;

import static java.lang.System.currentTimeMillis;

/**
 * Created by Jenny on 7/12/2017.
 */
public class CustomFieldIdentificationTest extends BaseIntegrationTest {

    private V4Builder v4Builder = new V4Builder(apiKey);

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }

    @Test
    public void testPhoneNumberIdentificationGetMemberDetails1() throws Exception{

        String customerPhone = String.valueOf(currentTimeMillis());
        RegisterResponse registerResponseV2 = updateFieldsAndCreateMembers(RegistrationField.PhoneNumber.toString(),customerPhone);

        Customer customer = new Customer();
        customer.setCustomIdentifier(registerResponseV2.getPhoneNumber());
        checkGetMemberDetails(customer, customerPhone);

    }

    @Test
    public void testPhoneNumberIdentificationSubmitPurchase2() throws Exception{

        //update purchase with customer is required
        updateSavePurchaseOptions();
        String customerPhone = String.valueOf(currentTimeMillis());
        RegisterResponse registerResponseV2 = updateFieldsAndCreateMembers(RegistrationField.PhoneNumber.toString(),customerPhone);
        Customer customer = new Customer();
        customer.setCustomIdentifier(registerResponseV2.getPhoneNumber());
        submitPurchase(customer);

    }

    @Test
    public void testPhoneNumberIdentificationSubmitEvent3() throws Exception{

        String customerPhone = String.valueOf(currentTimeMillis());
        RegisterResponse registerResponseV2 = updateFieldsAndCreateMembers(RegistrationField.PhoneNumber.toString(),customerPhone);

        Customer customer = new Customer();
        customer.setCustomIdentifier(registerResponseV2.getPhoneNumber());
        submitEvent(customer);

    }


    @Test
    public void testCarNumberIdentificationGetMemberDetails4() throws Exception{

        updateIdentificationFields (RegistrationField.CarNumber.toString());
        String customerPhone = String.valueOf(currentTimeMillis());
        String carNumber = String.valueOf(currentTimeMillis() + 1);

        //Update the authentication fields
        createMember(customerPhone, carNumber);

        Customer customer = new Customer();
        customer.setCustomIdentifier(carNumber);
        checkGetMemberDetails(customer, customerPhone);

    }

    @Test
    public void testCarNumberIdentificationSubmitEvent5() throws Exception{

        updateIdentificationFields (RegistrationField.CarNumber.toString());
        String customerPhone = String.valueOf(currentTimeMillis());
        String carNumber = String.valueOf(currentTimeMillis());

        //Update the authentication fields
        createMember(customerPhone, carNumber);

        Customer customer = new Customer();
        customer.setCustomIdentifier(carNumber);
        submitEvent(customer);

    }

    @Test
    public void testCarNumberIdentificationSubmitPurchase6() throws Exception{

        //Update the authentication fields
        updateIdentificationFields (RegistrationField.CarNumber.toString());
        //update purchase with customer is required
        updateSavePurchaseOptions();

        String customerPhone = String.valueOf(currentTimeMillis());
        String carNumber = String.valueOf(currentTimeMillis());

        //Update the authentication fields
        createMember(customerPhone, carNumber);
        Customer customer = new Customer();
        customer.setCustomIdentifier(carNumber);
        submitPurchase(customer);


    }

    @Test
    public void testFirstNameIdentificationGetMemberDetails7() throws Exception{

        updateIdentificationFields (RegistrationField.FirstNAme.toString());
        String customerPhone = String.valueOf(currentTimeMillis());
        String carNumber = String.valueOf(currentTimeMillis());
        //Update the authentication fields
        NewMember member = createMember(customerPhone, carNumber);

        Customer customer = new Customer();
        customer.setCustomIdentifier(member.getFirstName());
        checkGetMemberDetails(customer, member.getPhoneNumber());

    }


    @Test
    public void testFirstNameIdentificationSubmitPurchase8() throws Exception{

        //Update the authentication fields
        updateIdentificationFields (RegistrationField.FirstNAme.toString());
        //update purchase with customer is required
        updateSavePurchaseOptions();
        String carNumber = String.valueOf(currentTimeMillis());
        String customerPhone = String.valueOf(currentTimeMillis());


        //Update the authentication fields
        NewMember member = createMember(customerPhone, carNumber);
        Customer customer = new Customer();
        customer.setCustomIdentifier(member.getFirstName());
        submitPurchase(customer);


    }

    @Test
    public void testFirstNameIdentificationSubmitEvent9() throws Exception{

        updateIdentificationFields (RegistrationField.FirstNAme.toString());
        String customerPhone = String.valueOf(currentTimeMillis());
        String carNumber = String.valueOf(currentTimeMillis());
        //Update the authentication fields
        NewMember member = createMember(customerPhone, carNumber);

        Customer customer = new Customer();
        customer.setCustomIdentifier(member.getFirstName());
        submitEvent(customer);

    }


    @Test
    public void testFirstNameIdentificationGetMemberDetailsNegativeTest10() throws Exception{

        updateIdentificationFields (RegistrationField.FirstNAme.toString());
        String customerPhone = String.valueOf(currentTimeMillis());
        String carNumber = String.valueOf(currentTimeMillis());
        //Update the authentication fields
        createMember(customerPhone, carNumber);
        Customer customer = new Customer();
        customer.setCustomIdentifier(carNumber);
        getMemberDetailsNegative(customer);
    }

    @Test
    public void testFirstNameIdentificationSubmitEventNegative11() throws Exception{

        updateIdentificationFields (RegistrationField.FirstNAme.toString());
        String customerPhone = String.valueOf(currentTimeMillis());
        String carNumber = String.valueOf(currentTimeMillis());
        //Update the authentication fields
        createMember(customerPhone, carNumber);
        Customer customer = new Customer();
        customer.setCustomIdentifier(carNumber);
        getSubmitEventNegative(customer);

    }

    @Test
    public void testFirstNameIdentificationSubmitPurchaseNegativeTest12() throws Exception{

        updateIdentificationFields (RegistrationField.FirstNAme.toString());
        String customerPhone = String.valueOf(currentTimeMillis());
        String carNumber = String.valueOf(currentTimeMillis());
        //Update the authentication fields
        createMember(customerPhone, carNumber);
        Customer customer = new Customer();
        customer.setCustomIdentifier(carNumber);
        submitPurchaseNegative(customer);
    }

    @Test
    public void testCarNumberIdentificationUpdateMember14() throws Exception{

        updateIdentificationFields (RegistrationField.CarNumber.toString());
        String customerPhone = String.valueOf(currentTimeMillis());
        String carNumber = String.valueOf(currentTimeMillis());
        //Update the authentication fields
        NewMember member = createMember(customerPhone, carNumber);
        Customer customer = new Customer();
        customer.setCustomIdentifier(carNumber);

        updateMember(customer);

    }

    @Test
    public void testPhoneNumberIdentificationUpdateMember15() throws Exception{

        String customerPhone = String.valueOf(currentTimeMillis());
        RegisterResponse registerResponseV2 = updateFieldsAndCreateMembers(RegistrationField.PhoneNumber.toString(),customerPhone);

        Customer customer = new Customer();
        customer.setCustomIdentifier(registerResponseV2.getPhoneNumber());
        updateMember(customer);

    }


    @Test
    public void testPhoneNumberIdentificationPayment16() throws Exception{

        String customerPhone = String.valueOf(currentTimeMillis());
        RegisterResponse registerResponseV2 = updateFieldsAndCreateMembers(RegistrationField.PhoneNumber.toString(),customerPhone);
        Customer customer = new Customer();
        customer.setCustomIdentifier(registerResponseV2.getPhoneNumber());

        //test payment response
        payment(customer);

    }


    @Test
    public void testFirstNameIdentificationPayment17() throws Exception{

        updateIdentificationFields (RegistrationField.FirstNAme.toString());
        String customerPhone = String.valueOf(currentTimeMillis());
        String carNumber = String.valueOf(currentTimeMillis());
        //Update the authentication fields
        NewMember member = createMember(customerPhone, carNumber);

        Customer customer = new Customer();
        customer.setCustomIdentifier(member.getFirstName());

        //test payment response
        payment(customer);

    }

    @Test
    public void testCarNumberIdentificationPayment18() throws Exception{

        updateIdentificationFields (RegistrationField.CarNumber.toString());
        String customerPhone = String.valueOf(currentTimeMillis());
        String carNumber = String.valueOf(currentTimeMillis());
        //Update the authentication fields
        NewMember member = createMember(customerPhone, carNumber);
        Customer customer = new Customer();
        customer.setCustomIdentifier(carNumber);

        //test payment response
        payment(customer);

    }

    @Test
    public void testPhoneNumberIdentificationGetMemberBenefits19() throws Exception{

        String customerPhone = String.valueOf(currentTimeMillis());
        RegisterResponse registerResponseV2 = updateFieldsAndCreateMembers(RegistrationField.PhoneNumber.toString(),customerPhone);

        Customer customer = new Customer();
        customer.setCustomIdentifier(registerResponseV2.getPhoneNumber());
        checkGetMemberBenefits(customer);

    }


    private NewMember createMember (String customerPhone,String carNumber)throws Exception {

        NewMember member = hubMemberCreator.buildMember(customerPhone, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        member.setGenericString1(carNumber);
        MembersService.createNewMember(member);
        log4j.info("New member with car number was created :  "+member.getGenericString1());
        return member;

    }



    private void updateIdentificationFields (String field) throws Exception {

        AppSettingsPage appSettingsPage = new AppSettingsPage();
        MemberIdentificationAndRegistration memberIdentificationAndRegistration = new MemberIdentificationAndRegistration();
        memberIdentificationAndRegistration.setMembershipFields(field);
        memberIdentificationAndRegistration.setMoveFromMemberToUnique("yes");
        memberIdentificationAndRegistration.setUniqueFields(field);
        memberIdentificationAndRegistration.setMoveFromUniqueToPOSFields("yes");
        memberIdentificationAndRegistration.setCustomFieldForPOSIdentification(field);
        appSettingsPage.setMemberIdentificationAndRegistration(memberIdentificationAndRegistration);
        OperationService.updateAppSettings(appSettingsPage);


    }

    private void payment (Customer customer)throws Exception {

        //Verify if the parameter in the Business Backend :"Payment Requires Verification Code" = No
        setDefaultConfigurationForTest("Points");

        //set payment options
        PaymentQueryParams queryParams = new PaymentQueryParams();
        queryParams.setAllowedPaymentMethod(PaymentMethod.MEAN_OF_PAYMENT);
        queryParams.setMode(Mode.PAY);

        //build purchase
        int sumToPay = -5000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        PaymentAPIResponse paymentResponse = (PaymentAPIResponse) payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        Assert.assertEquals("", STATUS_OK,paymentResponse.getStatus());

    }

    private void checkGetMemberDetails(Customer customer, String phoneNumber){

        GetMemberDetails memberDetails = v4Builder.buildMemberDetailsRequest(customer);
        IServerResponse response = v4Builder.getGetMemberDetailsResponse(memberDetails);
        if(response instanceof ZappServerErrorResponse)
            Assert.fail("server error: " + ((ZappServerErrorResponse) response).getErrorMessage());

        if(!(((GetMemberDetailsResponse) response).getStatus().equals(STATUS_OK)))
            Assert.fail(getError(response).get(MESSAGE).toString());

       //Verify the returned customer is as expected
        Assert.assertEquals("Phone number should be equal",phoneNumber,((GetMemberDetailsResponse)response).getMembership().getPhoneNumber());
    }

    private void checkGetMemberBenefits (Customer customer){

        GetBenefits getBenefits = v4Builder.buildGetMemberBenefitsRequest(customer);
        IServerResponse response = v4Builder.getMemberBenefitsResponse(getBenefits);
        if(response instanceof ZappServerErrorResponse)
            Assert.fail("server error: " + ((ZappServerErrorResponse) response).getErrorMessage());

        if(!(((GetBenefitsResponse) response).getStatus().equals(STATUS_OK)))
            Assert.fail(getError(response).get(MESSAGE).toString());

        //Verify the returned customer is as expected
        Assert.assertEquals("", STATUS_OK,((GetBenefitsResponse)response).getStatus());
    }



    private void updateMember (Customer customer){

        IServerResponse response = v4Builder.updateMemberResponse(customer);
        if(response instanceof ZappServerErrorResponse)
            Assert.fail("server error: " + ((ZappServerErrorResponse) response).getErrorMessage());
        if(!(((UpdateMemberResponse) response).getStatus().equals(STATUS_OK)))
            Assert.fail(getError(response).get(MESSAGE).toString());

        //Verify the returned customer is as expected
        Assert.assertEquals("", STATUS_OK,((UpdateMemberResponse)response).getStatus());
    }


    private void submitEvent (Customer customer){

        IServerResponse response = v4Builder.submitEvent(customer);
        if(response instanceof ZappServerErrorResponse)
            Assert.fail("server error: " + ((ZappServerErrorResponse) response).getErrorMessage());
        if(!(((SubmitEventResponse) response).getStatus().equals(STATUS_OK)))
            Assert.fail(getError(response).get(MESSAGE).toString());
        //Verify the returned customer is as expected
        Assert.assertEquals("",STATUS_OK,((SubmitEventResponse)response).getStatus());
    }

    private void submitPurchase (Customer customer){

        IServerResponse response = v4Builder.submitPurchaseResponse(customer);
        if(response instanceof ZappServerErrorResponse)
            Assert.fail("Expected an error!");
        if(!(((SubmitPurchaseResponse) response).getStatus().equals(STATUS_OK)))
            Assert.fail(getError(response).get(MESSAGE).toString());
        //Verify the returned customer is as expected
        Assert.assertNotNull("Verify the confirmation is not null",((SubmitPurchaseResponse)response).getConfirmation());
    }

    private void getMemberDetailsNegative (Customer customer){
        IServerResponse response = v4Builder.getGetMemberDetailsNotValidateResponse(customer);
        log4j.info("GetMember details request");
        validateErrorMessage(PosErrorsEnum.SINGLE_CUSTOMER_NOT_FOUND, response);

    }

    private void getSubmitEventNegative (Customer customer){

        IServerResponse response = v4Builder.submitEvent(customer);
        log4j.info("Get Submit Event request");
        validateErrorMessage(PosErrorsEnum.CUSTOMER_S_NOT_FOUND, response);

    }

    private void submitPurchaseNegative (Customer customer){

        IServerResponse response = v4Builder.submitPurchaseResponse(customer);
        log4j.info("Submit Purchase request");
        validateErrorMessage(PosErrorsEnum.PROPRIETARY_CUSTOMER_NOT_FOUND, response);
    }

    protected void validateErrorMessage(PosErrorsEnum errorCode, IServerResponse response) {
        log4j.info("Validate error code: " + errorCode.getCode());
        String errorMessage = getError(response).get(MESSAGE).toString();
        Assert.assertTrue(errorCode.isValidMessageForCode(errorMessage));
    }

    protected JSONObject getError(IServerResponse response){
        return ((JSONObject)((JSONArray)(response.getRawObject().get(ERRORS))).get(0));
    }


    private void setConfigurationForTest(String PaymentRequiresVerificationCode,String PayWithBudgetType,String PayWithBudgetRatio,String allowNegativePointBalance) {

        serverHelper.setConfigurationForPayment(PaymentRequiresVerificationCode,PayWithBudgetType,PayWithBudgetRatio,allowNegativePointBalance);

    }

    private void setDefaultConfigurationForTest(String PayWithBudgetType) {
        setConfigurationForTest("false",PayWithBudgetType,"1","false");
    }

    private void updateSavePurchaseOptions () throws Exception {

        GeneralPOSSettingsPage generalPOSSettingsPage = new GeneralPOSSettingsPage();
        Purchases purchases = new Purchases();
        purchases.setSavePurchasesOptions(SavePurchaseOptions.PURCHASE_ALL_MEMBERS.toString());
        generalPOSSettingsPage.setPurchases(purchases);
        OperationService.updatePOSSettings(generalPOSSettingsPage);
    }


    private RegisterResponse updateFieldsAndCreateMembers (String field , String phoneNumber) throws Exception {

        //Update the authentication fields
        updateIdentificationFields (field);
        long currentTimeMillis = currentTimeMillis();
        String memberId = String.valueOf(currentTimeMillis);
        String govId = String.valueOf(currentTimeMillis);
        RegisterResponse registerResponseV2 = serverHelper.getRegisterResponse(phoneNumber,memberId,govId, apiKey);

        return  registerResponseV2;
    }


}
