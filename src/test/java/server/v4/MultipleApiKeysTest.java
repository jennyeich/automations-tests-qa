package server.v4;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.serverCategories.ServerRegression;
import hub.base.categories.serverCategories.serverV4_0;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.base.BaseServerTest;
import server.common.*;
import server.common.asset.AssetBuilder;
import server.common.asset.NewAsset;
import server.common.asset.RedeemAction;
import server.common.asset.Scenario;
import server.internal.ApiClientResponse;
import server.internal.ApiKey;
import server.internal.UpdateApiClient;
import server.utils.Initializer;
import server.utils.ServerHelper;
import server.utils.V4Builder;
import server.v2_8.JoinClubResponse;
import server.v2_8.base.ServicesErrorResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v4.common.enums.ExpandTypes;
import server.v4.common.enums.Mode;
import server.v4.common.enums.PaymentMethod;
import server.v4.common.enums.ReturnedBenefits;
import server.v4.common.models.*;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static java.lang.System.currentTimeMillis;

/**
 * Created by Goni on 10/24/2017.
 */

public class MultipleApiKeysTest extends BaseServerTest {


    private static final boolean SHOULD_PASS = true;
    public static final String FORBIDDEN = "Forbidden";
    public static final String INACTIVE = "inactive";
    public static final String ACTIVE = "active";

    private V4Builder v4Builder = new V4Builder(apiKey);
    private AssetBuilder assetBuilder=new AssetBuilder();

    private static String inactiveApiKey;
    private static String activeApiKey;

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
        try {
            inactiveApiKey = ServerHelper.addApiKeyAndValidate("inActiveKey", INACTIVE,dataStore);
            activeApiKey = ServerHelper.addApiKeyAndValidate("activeKey", ACTIVE,dataStore);
        } catch (Exception e) {
            log4j.info("Error while trying to create apiKey - " + e.getMessage());
        }
    }

    @Test
    public void testAddApiKey() throws Exception{

        String newApiKey = ServerHelper.addApiKeyAndValidate("notDefault", ACTIVE,dataStore);

        submitPurchaseWithGivenApiKey(newApiKey);
    }

    @Test
    @Category({ServerRegression.class})
    public void testPayment() throws Exception{

        serverHelper.setConfigurationForPayment("false", PaymentType.POINTS.type(),"1","true");
        log4j.info("Testing /v4/payment");
        PaymentAPI payment1 = buildPayment(inactiveApiKey);
        IServerResponse response =  payment1.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        validateResponse(response,!SHOULD_PASS);
        payment1 = buildPayment(activeApiKey);
        response =  payment1.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        validateResponse(response,SHOULD_PASS);
    }

    @Test
    public void testCancelPayment() throws Exception{

        log4j.info("Testing /v4/cancelPayment");
        PaymentAPI payment2 = buildPayment(activeApiKey);
        IServerResponse response = cancelPayment(inactiveApiKey,payment2);
        validateResponse(response,!SHOULD_PASS);
        response = cancelPayment(activeApiKey,payment2);
        validateResponse(response,SHOULD_PASS);
    }
    @Test
    public void testVoidPurchase() throws Exception{

        log4j.info("Testing /v4/voidPurchase");
        IServerResponse response = voidPurchase(inactiveApiKey);
        validateResponse(response,!SHOULD_PASS);
        response = voidPurchase(activeApiKey);
        validateResponse(response,SHOULD_PASS);
    }

    @Test
    public void testSubmitPurchase() throws Exception{

        log4j.info("Testing /v4/submitPurchase");
        IServerResponse response = submitPurchase(inactiveApiKey);
        validateResponse(response,!SHOULD_PASS);
        response = submitPurchase(activeApiKey);
        validateResponse(response,SHOULD_PASS);
    }

    @Test
    public void testCancelPurchase() throws Exception{

        log4j.info("Testing /v4/cancelPurchase");
        SubmitPurchase submitPurchase = buildSubmitPurchase(activeApiKey,v4Builder.buildDefaultPurchase(UUID.randomUUID().toString()));
        IServerResponse response = submitPurchase.sendRequestAndValidateResponse(SubmitPurchaseResponse.class);
        String confirmation = ((SubmitPurchaseResponse)response).getConfirmation();
        response = cancelPurchase(inactiveApiKey,submitPurchase,confirmation);
        validateResponse(response,!SHOULD_PASS);
        response = cancelPurchase(activeApiKey,submitPurchase,confirmation);
        validateResponse(response,SHOULD_PASS);
    }

    @Test
    @Category(serverV4_0.class)
    public void testGetMemberDetails() throws Exception{

        log4j.info("Testing /v4/getMemberDetails");
        IServerResponse response = getMemberDetails(inactiveApiKey);
        validateResponse(response,!SHOULD_PASS);
        response = getMemberDetails(activeApiKey);
        validateResponse(response,SHOULD_PASS);
    }

    @Test
    @Category({ServerRegression.class})
    public void testGetBenefits() throws Exception{

        log4j.info("Testing /v4/getBenefits");
        IServerResponse response = getBenefits(inactiveApiKey);
        validateResponse(response,!SHOULD_PASS);
        response = getBenefits(activeApiKey);
        validateResponse(response,SHOULD_PASS);
    }

    @Test
    public void testSubmitEvent() throws Exception{

        log4j.info("Testing /v4/advanced/submitEvent");
        IServerResponse response = submitEvent(inactiveApiKey);
        validateResponse(response, !SHOULD_PASS);
        response = submitEvent(activeApiKey);
        validateResponse(response, SHOULD_PASS);

    }

    @Test
    @Ignore("Deprected API")
    public void testRegisterMemberV2() throws Exception{

//        log4j.info("Testing api/v2/memberships/register");
//        IServerResponse response = registerMemberV2(inactiveApiKey);
//        validateResponse(response, !SHOULD_PASS);
//        response = registerMemberV2(activeApiKey);
//        validateResponse(response, SHOULD_PASS);

    }


    @Test
    //api/v4/advanced/updateMember
    public void testUpdateMember() throws Exception{

        String phoneNumber = String.valueOf(currentTimeMillis());
        IServerResponse response = registerMemberV4(activeApiKey,phoneNumber);

        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);

        log4j.info("Testing api/v4/advanced/updateMember");
        response = updateMember(inactiveApiKey, customer);
        validateResponse(response, !SHOULD_PASS);
        response = updateMember(activeApiKey, customer);
        validateResponse(response, SHOULD_PASS);

    }



    @Test
    public void testAdd2ApiKey() throws Exception{

        String newApiKey1 = ServerHelper.getnerateNewApiKey();
        String newApiKey2 = ServerHelper.getnerateNewApiKey();
        UpdateApiClient saveApiClientRequest = new UpdateApiClient(serverToken);
        ApiKey apiKeyObj1 = new ApiKey("notDefault1", ACTIVE,newApiKey1);
        ApiKey apiKeyObj2 = new ApiKey("notDefault2", INACTIVE,newApiKey2);
        saveApiClientRequest.addProp(ApiClientFields.API_KEYS.key(), Lists.newArrayList(apiKeyObj1,apiKeyObj2));
        IServerResponse updateResponse = saveApiClientRequest.sendRequestByLocation(locationId, ApiClientResponse.class);
        if(updateResponse instanceof ZappServerErrorResponse)
            Assert.fail("Add api key failed: " + ((ZappServerErrorResponse) updateResponse).getErrorMessage());

        ServerHelper.checkApiKeyInDatastore(((ApiClientResponse)updateResponse).getApiKeys(),Lists.newArrayList(newApiKey1,newApiKey2),dataStore);
    }

    @Test
    public void testAdd2SameApiKeyNotDuplicate() throws Exception{

        String newApiKey = ServerHelper.getnerateNewApiKey();
        UpdateApiClient saveApiClientRequest = new UpdateApiClient(serverToken);
        ApiKey apiKeyObj1 = new ApiKey("notDefault1", ACTIVE,newApiKey);
        ApiKey apiKeyObj2 = new ApiKey("notDefault2", INACTIVE,newApiKey);
        saveApiClientRequest.addProp(ApiClientFields.API_KEYS.key(), Lists.newArrayList(apiKeyObj1));
        saveApiClientRequest.sendRequestByLocation(locationId, ApiClientResponse.class);

        saveApiClientRequest = new UpdateApiClient(serverToken);
        saveApiClientRequest.addProp(ApiClientFields.API_KEYS.key(), Lists.newArrayList(apiKeyObj2));
        IServerResponse updateResponse = saveApiClientRequest.sendRequestByLocation(locationId, ApiClientResponse.class);
        if(updateResponse instanceof ZappServerErrorResponse)
            Assert.fail("Add api key failed: " + ((ZappServerErrorResponse) updateResponse).getErrorMessage());

        ServerHelper.checkApiKeyInDatastore(((ApiClientResponse)updateResponse).getApiKeys(),Lists.newArrayList(newApiKey),dataStore);

    }


    private IServerResponse registerMemberV4(String apiKeyToTest,String phoneNumber) throws Exception{
        if(updateConfiguration){
            serverHelper.setRegistrationConfiguration();
        }

        RegisterMemberV4 registerMember = Initializer.initRegisterMemberV4(apiKeyToTest,phoneNumber);
        return registerMember.SendRequestByApiKey(apiKeyToTest, RegisterMemberV4Response.class);

    }

    private IServerResponse updateMember(String apiKeyToTest, Customer customer) {
        UpdateMember updateMember = new UpdateMember(apiKeyToTest);
        updateMember.setCustomer(customer);
        RegistrationData registrationData = new RegistrationData();
        Map<String,Object> fieldMap = new HashMap<>();
        fieldMap.put(Initializer.EMAIL,"email@como.com");
        registrationData.setFieldNameMap(fieldMap);
        updateMember.setRegistrationData(registrationData);
        return updateMember.SendRequestByApiKeyAndValidateResponse(apiKeyToTest,UpdateMemberResponse.class);
    }

    private IServerResponse submitEvent(String apiKeyToTest) throws InterruptedException {
        JoinClubResponse member = serverHelper.createMember();
        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        SubmitEvent submitEvent = new SubmitEvent(apiKeyToTest);
        Event event = v4Builder.buildEvent();
        submitEvent.setEvent(event);
        submitEvent.setCustomers(Lists.newArrayList(customer));
        return submitEvent.SendRequestByApiKey(apiKeyToTest,SubmitEventResponse.class);
    }


    private IServerResponse getBenefits(String apiKeyToTest) throws Exception{
        NewAsset newAsset = assetBuilder.buildDefaultClubDeals(locationId);
        String code = UUID.randomUUID().toString();
        RedeemAction redeemAction = new RedeemAction();
        Scenario scenario = redeemAction.addScenario();
        scenario.addItemCodeAction().addItemCodes(code);
        newAsset.setRedeemAction(redeemAction);

        String dealCode = UUID.randomUUID().toString();
        scenario.addDealCodeAction().addDealCode(dealCode);
        newAsset.setRedeemAction(redeemAction);

        IServerResponse assetResponse = newAsset.SendRequestByToken(serverToken, Response.class);
        Assert.assertEquals("New asset has been sent and return with status code 200",httpStatusCode200,assetResponse.getStatusCode());

        //due to cashing machanism in server - we need to wait 1 min after asset creation
        TimeUnit.MINUTES.sleep(1);
        JoinClubResponse member = serverHelper.createMember();
        GetBenefits getBenefits = new GetBenefits(apiKey);

        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        getBenefits.getCustomers().add(customer);


        //add query parameters
        GetBenefitsQueryParams queryParams =  new GetBenefitsQueryParams();
        queryParams.setExpand(Lists.newArrayList(ExpandTypes.DISCOUNT_BY_DISCOUNT.toString()));
        queryParams.setMarkAssetsAsUsed(false);
        queryParams.setReturnBenefits(ReturnedBenefits.ALL.toString());
        getBenefits.setQueryParameters(queryParams.getParams());
        //add default purchase
        Purchase purchase = v4Builder.buildDefaultPurchase(UUID.randomUUID().toString());
        getBenefits.setPurchase(purchase);
        getBenefits.setApiKey(apiKeyToTest);
        return getBenefits.sendRequestAndValidateResponse(GetBenefitsResponse.class);

    }

    private IServerResponse getMemberDetails(String apiKeyToTest) throws Exception{

        serverHelper.setConfigurationForPayment("false",PaymentType.BUDGET.type(),"1","true");

        String paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        PaymentProviderConfigurations providerConfigurations = v4Builder.buildPaymentProviderConfigurations(paymentMethod,true);
        serverHelper.saveApiClient("PaymentProviderConfigurations" ,providerConfigurations);

        //Create new customer with points
        Customer customer = serverHelper.createMemberWithPointsOrBudget("1000",PaymentType.BUDGET.type(),dataStore);
        GetMemberDetails memberDetails = v4Builder.buildMemberDetailsRequest(customer);
        memberDetails.setApiKey(apiKeyToTest);
        return memberDetails.sendRequestAndValidateResponse(GetMemberDetailsResponse.class);
    }

    private IServerResponse cancelPurchase(String apiKeyToTest, SubmitPurchase submitPurchase,String confirmation) {
        CancelPurchase cancelPurchase = new CancelPurchase();
        cancelPurchase.setApiKey(apiKeyToTest);
        cancelPurchase.setBranchId(submitPurchase.getBranchId());
        cancelPurchase.setPosId(submitPurchase.getPosId());
        cancelPurchase.setxSourceName("name");
        cancelPurchase.setxSourceType("type");
        cancelPurchase.setxSourceVersion("version");
        cancelPurchase.setConfirmation(confirmation);
        return cancelPurchase.sendRequestAndValidateResponse(CancelPaymentResponse.class);
    }

    private IServerResponse submitPurchase(String apiKeyToTest) {
        String uuid = UUID.randomUUID().toString();
        SubmitPurchase submitPurchase = buildSubmitPurchase(apiKeyToTest,v4Builder.buildDefaultPurchase(uuid));
        return submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKeyToTest,SubmitPurchaseResponse.class);

    }

    private SubmitPurchase buildSubmitPurchase(String apiKeyToTest,Purchase purchase) {
        
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKeyToTest);
        submitPurchase.setDefauldHeaderParams();

        submitPurchase.setPurchase(purchase);

        //save the transaction id for tests
        submitPurchase.getPurchase().getTransactionId();
        return submitPurchase;
    }


    private IServerResponse cancelPayment(String apiKeyToTest,PaymentAPI payment) throws Exception{

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false));

        serverHelper.setConfigurationForPayment("false",PaymentType.POINTS.type(),"1","true");
        IServerResponse paymentResponse =  payment.sendRequestAndValidateResponse(PaymentAPIResponse.class);
        String confirmation = ((PaymentAPIResponse)paymentResponse).getConfirmation();
        if(confirmation == null)
            confirmation = UUID.randomUUID().toString();
        CancelPayment cancelPayment = v4Builder.buildCancelPaymentRequest(confirmation,payment.getPurchase());
        cancelPayment.setApiKey(apiKeyToTest);
        log4j.info("Confirmation for cancel payment: " + confirmation);
        return cancelPayment.SendRequestByApiKey(apiKeyToTest, CancelPaymentResponse.class);
    }


    private PaymentAPI buildPayment(String apiKeyToTest) throws Exception{
        serverHelper.setConfigurationForPayment("false",PaymentType.POINTS.type(),"1","true");

        JoinClubResponse newMember = createMember();
        serverHelper.addPointsToMember("10000", newMember.getMembershipKey(),newMember.getUserKey());
        Thread.sleep(10000);
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        PaymentQueryParams queryParams = new PaymentQueryParams();
        queryParams.setAllowedPaymentMethod(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT);
        queryParams.setMode(Mode.PAY);

        //build purchase
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,3,purchase);
        payment.setApiKey(apiKeyToTest);

        return payment;
    }

    private IServerResponse voidPurchase(String apiKeyToTest) {
        Purchase purchase = v4Builder.buildDefaultPurchase(UUID.randomUUID().toString());
        VoidPurchase voidPurchase = new VoidPurchase(apiKeyToTest);
        voidPurchase.setPurchase(purchase);
        return voidPurchase.SendRequestByApiKey(apiKeyToTest,VoidPurchaseResponse.class);
    }

    private void validateResponse(IServerResponse response,boolean shouldPass) {
        if(shouldPass){
            log4j.info("response: " + response.getRawObject());
            Assert.assertTrue("response should be 200/201 but is: "+ response.getStatusCode(),((response.getStatusCode().intValue() == httpStatusCode200.intValue()) ||
                    (response.getStatusCode()==httpStatusCode201.intValue())));
        }else {
            if(response instanceof ServicesErrorResponse){
                ServicesErrorResponse errorResponse =(ServicesErrorResponse)response;
                Assert.assertEquals(errorResponse.getErrorType().toUpperCase(), FORBIDDEN.toUpperCase());
                Assert.assertEquals(errorResponse.getStatusCode(), Integer.valueOf(403));
            }else {
                JSONObject error = serverHelper.getError(response);
                Assert.assertEquals(error.get(MESSAGE).toString(), FORBIDDEN);
                Assert.assertEquals(error.get(CODE).toString(), "4030000");
            }
        }
    }



    private void submitPurchaseWithGivenApiKey(String apiKeyToTest) throws Exception {
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),false),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()));

        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,0, Lists.newArrayList()) ;

        //Run first submitPurchase and validate response
        SubmitPurchase submitPurchase = serverHelper.buildSubmitPurchaseRequest(Lists.newArrayList(),Lists.newArrayList() ,Lists.newArrayList(), purchase);
        submitPurchase.setApiKey(apiKeyToTest);
        IServerResponse response= serverHelper.submitPurchaseResponse(submitPurchase,apiKeyToTest);

        String confirmation = ((SubmitPurchaseResponse)response).getConfirmation();
        Assert.assertNotNull("Confirmation must not be null" ,confirmation);
    }

}

