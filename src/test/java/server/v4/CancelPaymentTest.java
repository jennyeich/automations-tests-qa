package server.v4;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.serverCategories.ServerRegression;
import hub.base.categories.serverCategories.serverV4_0;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.common.ApiClientBuilder;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.common.PaymentType;
import server.utils.PosErrorsEnum;
import server.v4.base.ZappServerErrorResponse;
import server.v4.common.enums.Mode;
import server.v4.common.enums.PaymentMethod;
import server.v4.common.enums.V4ConstantsLabels;
import server.v4.common.models.Customer;
import server.v4.common.models.PaymentQueryParams;
import server.v4.common.models.Purchase;

import java.util.List;
import java.util.UUID;

/**
 * Created by Goni on 7/10/2017.
 */
public class CancelPaymentTest extends BasePaymentTest {


    /**
     * C2939 - Cancel Budget Payment with confirmation
     **/
    @Test
    @Category({ServerRegression.class})
    public void testCancelPointsPaymentWithConfirmation() throws Exception {
        /*
        steps:
        1. verify if the flag AllowCancelMemberBudgetPayment = true
        2. send pay with budget , verify if the response 200 ok
        3. cancel the budget by confirmation id
        4. verify if the  data in BudgetTransaction table correct
         */
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),true),
        ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false));

        setConfigurationForTest("false",PaymentType.POINTS.type(),"1","false");

        Customer customer = createMemberWithPoints();
//        int pointsAfter = v4Builder.getMembersPoints(customer);
//        Assert.assertEquals("Customer needs to have 100 points", 10000,pointsAfter);

        //set payment options
        PaymentAPI payment = createPaymentRequest(customer);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);

        String confirmation = paymentResponse.getConfirmation();
        CancelPayment cancelPayment = v4Builder.buildCancelPaymentRequest(confirmation,payment.getPurchase());
        log4j.info("Confirmation for cancel payment: " + confirmation);
        IServerResponse responseCancel = cancelPayment.SendRequestByApiKey(apiKey, CancelPaymentResponse.class);

        // make sure the response is not Error response
        if (responseCancel instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) responseCancel).getErrorMessage());
        }

        checkCancelRespose((CancelPaymentResponse) responseCancel,V4ConstantsLabels.MEMBER_POINTS);

        Query.Filter filter = new Query.FilterPredicate("UID", Query.FilterOperator.EQUAL, confirmation);
        verifyDataStoreUpdatedByFilter(Lists.newArrayList(filter));


    }


    /**
     * C2939 - Cancel Budget Payment with confirmation
     **/
    @Test
    @Category({ServerRegression.class})
    public void testCancelCreditPaymentWithConfirmation() throws Exception {
        /*
        steps:
        1. verify if the flag AllowCancelMemberBudgetPayment = true
        2. send pay with budget , verify if the response 200 ok
        3. cancel the budget by confirmation id
        4. verify if the  data in BudgetTransaction table correct
         */
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),true),
        ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false));


        setConfigurationForTest("false",PaymentType.BUDGET.type(),"1","false");

        Customer customer = createMemberWithBudget();
//        int pointsAfter = v4Builder.getMembersBudget(customer);
//        Assert.assertEquals("Customer needs to have 100 points", 10000,pointsAfter);

        PaymentAPI payment = createPaymentRequest(customer);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);

        String confirmation = paymentResponse.getConfirmation();
        CancelPayment cancelPayment = v4Builder.buildCancelPaymentRequest(confirmation,payment.getPurchase());
        log4j.info("Confirmation for cancel payment: " + confirmation);
        IServerResponse responseCancel = cancelPayment.SendRequestByApiKey(apiKey, CancelPaymentResponse.class);

        // make sure the response is not Error response
        if (responseCancel instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) responseCancel).getErrorMessage());
        }

        checkCancelRespose((CancelPaymentResponse) responseCancel,V4ConstantsLabels.MEMBER_CREDIT);
        Query.Filter filter = new Query.FilterPredicate("UID", Query.FilterOperator.EQUAL, confirmation);
        verifyDataStoreUpdatedByFilter(Lists.newArrayList(filter));


    }

    /**
     * C3002 - Cancel Budget with TransactionID + BranchID
     **/
    @Test
    public void testCancelPointsWithTransactionIDBranchID() throws Exception {
        /*
        steps:
        1. verify if the flag AllowCancelMemberBudgetPayment = true && AllowCancelBudgetPaymentByPosIdentifiers = true
        2. send pay with budget , verify if the response 200 ok
        3. cancel the payment by confirmation TransactionID + BranchID
        4. verify if the  data in BudgetTransaction table correct
         */

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),true),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_BUDGET_PAYMENT_BY_POS_IDENTIFIERS.key(),true),
        ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false));

        setConfigurationForTest("false",PaymentType.POINTS.type(),"1","false");

        Customer customer = createMemberWithPoints();
//        int pointsAfter = v4Builder.getMembersPoints(customer);
//        Assert.assertEquals("Customer needs to have 100 points", 10000,pointsAfter);

        PaymentAPI payment = createPaymentRequest(customer);
        String BRANCH_ID = "2";
        payment.setBranchId(BRANCH_ID);


        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        Assert.assertEquals("status code must be 200", (Integer) 200,paymentResponse.getStatusCode());
        Assert.assertEquals("Status must be ok", STATUS_OK,paymentResponse.getStatus());
        Assert.assertFalse("Payment UID must not be empty!", StringUtils.isEmpty(paymentResponse.getConfirmation()));

        String confirmation = paymentResponse.getConfirmation();
        CancelPayment cancelPayment = v4Builder.buildCancelPaymentRequest(confirmation,payment.getPurchase());
        log4j.info("TransactionId for cancel payment: " + payment.getPurchase().getTransactionId());
        IServerResponse responseCancel = cancelPayment.SendRequestByApiKey(apiKey, CancelPaymentResponse.class);

        // make sure the response is not Error response
        if (responseCancel instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) responseCancel).getErrorMessage());
        }

        checkCancelRespose((CancelPaymentResponse) responseCancel,V4ConstantsLabels.MEMBER_POINTS);
        //Verify with DataStore
        Query.Filter filter = new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, payment.getPurchase().getTransactionId());
        Query.Filter filter1 = new Query.FilterPredicate("BranchID", Query.FilterOperator.EQUAL, BRANCH_ID);
        Query.Filter filter2 = new Query.FilterPredicate("UID", Query.FilterOperator.EQUAL, confirmation);
        verifyDataStoreUpdatedByFilter(Lists.newArrayList(filter,filter1,filter2));
    }

    /**
     * C3002 - Cancel Budget with TransactionID + BranchID
     **/
    @Test
    public void testCancelCreditWithTransactionIDBranchID() throws Exception {
        /*
        steps:
        1. verify if the flag AllowCancelMemberBudgetPayment = true && AllowCancelBudgetPaymentByPosIdentifiers = true
        2. send pay with budget , verify if the response 200 ok
        3. cancel the payment by confirmation TransactionID + BranchID
        4. verify if the  data in BudgetTransaction table correct
         */
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),true),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_BUDGET_PAYMENT_BY_POS_IDENTIFIERS.key(),true),
        ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false));

        setConfigurationForTest("false", PaymentType.BUDGET.type(),"1","false");

        Customer customer = createMemberWithBudget();
//        int pointsAfter = v4Builder.getMembersPoints(customer);
//        Assert.assertEquals("Customer needs to have 100 points", 10000,pointsAfter);

        PaymentAPI payment = createPaymentRequest(customer);
        String BRANCH_ID = "2";
        payment.setBranchId(BRANCH_ID);


        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);


        String confirmation = paymentResponse.getConfirmation();
        CancelPayment cancelPayment = v4Builder.buildCancelPaymentRequest(confirmation,payment.getPurchase());
        log4j.info("TransactionId for cancel payment: " + payment.getPurchase().getTransactionId());
        Thread.sleep(2000);//wait 2 sec before cancellation
        IServerResponse responseCancel = cancelPayment.SendRequestByApiKey(apiKey, CancelPaymentResponse.class);

        // make sure the response is not Error response
        if (responseCancel instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) responseCancel).getErrorMessage());
        }

        checkCancelRespose((CancelPaymentResponse) responseCancel,V4ConstantsLabels.MEMBER_CREDIT);
        //Verify with DataStore
        Query.Filter filter = new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, payment.getPurchase().getTransactionId());
        Query.Filter filter1 = new Query.FilterPredicate("BranchID", Query.FilterOperator.EQUAL, BRANCH_ID);
        Query.Filter filter2 = new Query.FilterPredicate("UID", Query.FilterOperator.EQUAL, confirmation);
        verifyDataStoreUpdatedByFilter(Lists.newArrayList(filter,filter1,filter2));
    }


    /**
     * C2803 - Cancel Budget by PosID, Branch ID, TransactionID
     */
    @Test
    @Category(serverV4_0.class )
    public  void testCancelPointsByPosIDBranchIDTransactionID() throws Exception{
        /*
        steps:
        1. verify if the flag AllowCancelMemberBudgetPayment = true && AllowCancelBudgetPaymentByPosIdentifiers = true
        2. send pay with budget , verify if the response 200 ok
        3. cancel the budget by PosID + TransactionID + BranchID
        4. verify if the  data in BudgetTransaction table correct
         */
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),true),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_BUDGET_PAYMENT_BY_POS_IDENTIFIERS.key(),true),
        ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false));

        setConfigurationForTest("false",PaymentType.POINTS.type(),"1","false");

        Customer customer = createMemberWithPoints();
//        int pointsAfter = v4Builder.getMembersPoints(customer);
//        Assert.assertEquals("Customer needs to have 100 points", 10000,pointsAfter);

        PaymentAPI payment = createPaymentRequest(customer);
        String BRANCH_ID = "2";
        String POS_ID = "1";
        payment.setBranchId(BRANCH_ID);
        payment.setPosId(POS_ID);


        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        Assert.assertEquals("status code must be 200", (Integer) 200,paymentResponse.getStatusCode());
        Assert.assertEquals("Status must be ok", STATUS_OK,paymentResponse.getStatus());
        Assert.assertFalse("Payment UID must not be empty!", StringUtils.isEmpty(paymentResponse.getConfirmation()));

        String confirmation = paymentResponse.getConfirmation();
        CancelPayment cancelPayment = v4Builder.buildCancelPaymentRequest(confirmation,payment.getPurchase());
        log4j.info("TransactionId for cancel payment: " + payment.getPurchase().getTransactionId());
        IServerResponse responseCancel = cancelPayment.SendRequestByApiKey(apiKey, CancelPaymentResponse.class);

        // make sure the response is not Error response
        if (responseCancel instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) responseCancel).getErrorMessage());
        }

        checkCancelRespose((CancelPaymentResponse) responseCancel,V4ConstantsLabels.MEMBER_POINTS);
        //Verify with DataStore
        Query.Filter filter1 = new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, payment.getPurchase().getTransactionId());
        Query.Filter filter2 = new Query.FilterPredicate("BranchID", Query.FilterOperator.EQUAL, BRANCH_ID);
        Query.Filter filter3 = new Query.FilterPredicate("PosID", Query.FilterOperator.EQUAL, POS_ID);
        Query.Filter filter4 = new Query.FilterPredicate("UID", Query.FilterOperator.EQUAL, confirmation);
        verifyDataStoreUpdatedByFilter(Lists.newArrayList(filter1,filter2,filter3,filter4));

    }

    /**
     * C2803 - Cancel Budget by PosID, Branch ID, TransactionID
     */
    @Test
    public  void testCancelCreditByPosIDBranchIDTransactionID() throws Exception{
        /*
        steps:
        1. verify if the flag AllowCancelMemberBudgetPayment = true && AllowCancelBudgetPaymentByPosIdentifiers = true
        2. send pay with budget , verify if the response 200 ok
        3. cancel the budget by PosID + TransactionID + BranchID
        4. verify if the  data in BudgetTransaction table correct
         */

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),true),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_BUDGET_PAYMENT_BY_POS_IDENTIFIERS.key(),true),
        ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false));

        setConfigurationForTest("false",PaymentType.BUDGET.type(),"1","false");

        Customer customer = createMemberWithBudget();
//        int pointsAfter = v4Builder.getMembersPoints(customer);
//        Assert.assertEquals("Customer needs to have 100 points", 10000,pointsAfter);

        PaymentAPI payment = createPaymentRequest(customer);
        String BRANCH_ID = "2";
        String POS_ID = "1";
        payment.setBranchId(BRANCH_ID);
        payment.setPosId(POS_ID);


        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        Assert.assertEquals("status code must be 200", (Integer) 200,paymentResponse.getStatusCode());
        Assert.assertEquals("Status must be ok", STATUS_OK,paymentResponse.getStatus());
        Assert.assertFalse("Payment UID must not be empty!", StringUtils.isEmpty(paymentResponse.getConfirmation()));

        String confirmation = paymentResponse.getConfirmation();
        CancelPayment cancelPayment = v4Builder.buildCancelPaymentRequest(confirmation,payment.getPurchase());
        log4j.info("TransactionId for cancel payment: " + payment.getPurchase().getTransactionId());
        IServerResponse responseCancel = cancelPayment.SendRequestByApiKey(apiKey, CancelPaymentResponse.class);

        // make sure the response is not Error response
        if (responseCancel instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) responseCancel).getErrorMessage());
        }

        checkCancelRespose((CancelPaymentResponse) responseCancel,V4ConstantsLabels.MEMBER_CREDIT);
        //Verify with DataStore
        Query.Filter filter1 = new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, payment.getPurchase().getTransactionId());
        Query.Filter filter2 = new Query.FilterPredicate("BranchID", Query.FilterOperator.EQUAL, BRANCH_ID);
        Query.Filter filter3 = new Query.FilterPredicate("PosID", Query.FilterOperator.EQUAL, POS_ID);
        Query.Filter filter4 = new Query.FilterPredicate("UID", Query.FilterOperator.EQUAL, confirmation);
        verifyDataStoreUpdatedByFilter(Lists.newArrayList(filter1,filter2,filter3,filter4));

    }


    /**
     * C2804 - Cancel Budget by fake confirmation - cancel failed
     */
    @Test
    @Category({ServerRegression.class})
    public  void testCancelPointsByPosIDTransactionIDFakeConfirmation() throws Exception{

        /*
        steps:
        1. verify if the flag AllowCancelMemberBudgetPayment = true && AllowCancelBudgetPaymentByPosIdentifiers = true
        2. send pay with budget , verify if the response 200 ok
        3. cancel the budget by fake confirmation
        4. verify if we get an error message   "ErrorMessage": "Payment transaction not found"
        5. verify if the status in the data store = approved
         */

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_BUDGET_PAYMENT_BY_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false));


        setConfigurationForTest("false",PaymentType.POINTS.type(),"1","false");

        Customer customer = createMemberWithPoints();
//        int pointsAfter = v4Builder.getMembersPoints(customer);
//        Assert.assertEquals("Customer needs to have 100 points", 10000,pointsAfter);

        PaymentAPI payment = createPaymentRequest(customer);
        String POS_ID = "1";
        payment.setPosId(POS_ID);


        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);
        Assert.assertEquals("status code must be 200", (Integer) 200,paymentResponse.getStatusCode());
        Assert.assertEquals("Status must be ok", STATUS_OK,paymentResponse.getStatus());
        Assert.assertFalse("Payment UID must not be empty!", StringUtils.isEmpty(paymentResponse.getConfirmation()));

        CancelPayment cancelPayment = v4Builder.buildCancelPaymentRequest(UUID.randomUUID().toString(),payment.getPurchase());

        log4j.info("TransactionId for cancel payment: " + payment.getPurchase().getTransactionId());
        IServerResponse responseCancel = cancelPayment.SendRequestByApiKey(apiKey, CancelPaymentResponse.class);

        // make sure the response is not Error response
        if (responseCancel instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) responseCancel).getErrorMessage());
        }

        //Make sure the response is Error response
        validateErrorMessage(PosErrorsEnum.PROPRIETARY_CANCEL_PAYMENT_NOT_FOUND, responseCancel);

        //Verify with DataStore
        Query.Filter filter1 = new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, payment.getPurchase().getTransactionId());
        Query.Filter filter2 = new Query.FilterPredicate("PosID", Query.FilterOperator.EQUAL, POS_ID);
        Query q = buildQuery("BudgetTransaction",Lists.newArrayList(filter1,filter2));
        List<Entity> result = serverHelper.queryWithWait("Must find one Budget Transaction",
                q, 1, dataStore);
        Assert.assertTrue("Cancelled status must be true", result.get(0).getProperty("Status").toString().equalsIgnoreCase("approved"));

    }


    private void checkCancelRespose(CancelPaymentResponse responseCancel,String budgetType) {
        Assert.assertEquals("status must be ok",STATUS_OK,responseCancel.getStatus());
        Assert.assertEquals("type should be " + budgetType, responseCancel.getType(),budgetType);
        //if ratio=1 we expect same amount in both nonMonetary and monetary
        Assert.assertEquals("Balance should be same in nonMonetary and monetary: " + responseCancel.getUpdatedBalance().getMonetary() , responseCancel.getUpdatedBalance().getMonetary(), responseCancel.getUpdatedBalance().getNonMonetary());
    }

    private void verifyDataStoreUpdatedByFilter(List<Query.Filter> filterList) throws Exception {
        //Verify with DataStore
        Query q = buildQuery("BudgetTransaction",filterList);
        List<Entity> result = serverHelper.queryWithWait("Must find one Budget Transaction",
                q, 1, dataStore);
        Assert.assertTrue("Cancelled status must be true", result.get(0).getProperty("Status").toString().equalsIgnoreCase("canceled"));
    }

    private PaymentAPI createPaymentRequest(Customer customer) {

        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,true);
        //set payment options
        PaymentMethod paymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;

        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod, Mode.PAY);

        //build purchase
        int sumToPay = 40000;
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid);
        purchase.setTotalAmount(100);
        return v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);
    }



}

