package server.v4.sla;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.common.objects.operations.MemberNotesTargetFields;
import hub.common.objects.operations.MemberNotesTypeFields;
import hub.hub1_0.services.OperationService;
import hub.utils.HubMemberNotesCreator;
import integration.base.BaseIntegrationTest;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import server.common.AnonymousPurchaseTypes;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.common.asset.AssignAsset;
import server.utils.V4Builder;
import server.v2_8.CancelPurchase;
import server.v2_8.CancelPurchaseResponse;
import server.v2_8.JoinClubResponse;
import server.v4.SubmitPurchase;
import server.v4.SubmitPurchaseResponse;
import server.v4.base.ZappServerErrorResponse;
import server.v4.common.models.*;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static java.lang.System.currentTimeMillis;

/**
 * Created by Ariel on 8/9/2017.
 * Tests SLA for submitPurchase & cancelPurchase
 */
public class SlaSubmitPurchaseTest extends BaseIntegrationTest {

    private V4Builder v4Builder = new V4Builder(apiKey);

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }

   /**
    * SLA - Submit Purchase
    **/

    @Test
    public void testSlaSubmitPurchase() throws Exception{

        submitPurchase(false, 1000);
    }

    /**
     * SLA - Submit Purchase with Member Notes
     **/

    @Test
    public void testSlaSubmitPurchaseWithMemberNotes() throws Exception{

        submitPurchase(true, 2000);
    }

    /**
     * SLA - Cancel Purchase
     **/

    @Test
    public void testSlaCancelPurchase() throws Exception {
        Customer customer = createCustomer();
        String redeemCode = "1234";
        String assetType = "Asset";

        RedeemAssetOfSubmitPurchase redeemAsset = new RedeemAssetOfSubmitPurchase();
        redeemAsset.setAppliedAmount("3");
        redeemAsset.setCode(redeemCode);

        IServerResponse response = submitPurchase(customer, Lists.newArrayList(redeemAsset), Lists.newArrayList());

        //Response confirmation
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;

        // Initialize a new CancelPurchase
        CancelPurchase cancelPurchase = new CancelPurchase();
        cancelPurchase.setConfirmation(submitPurchaseResponse.getConfirmation());
        Assert.assertFalse("Confirmation must not be empty!", StringUtils.isEmpty(cancelPurchase.getConfirmation()));

        //Cancel the Purchase via Postman
        response = cancelPurchase.SendRequestByApiKey(apiKey, CancelPurchaseResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        Integer timeoutInterval = 2000;
        CancelPurchaseResponse cancelPurchaseResponse = (CancelPurchaseResponse) response;
        Assert.assertEquals("status code must be 200", (Integer) 200, cancelPurchaseResponse.getStatusCode());
        Assert.assertEquals("Result must by Y", "Y", cancelPurchaseResponse.getResult());
        log4j.info("Response time: " + cancelPurchaseResponse.getTime());

        Assert.assertTrue("Response time must be under " + timeoutInterval + " milliseconds , time was: " + cancelPurchaseResponse.getTime(), cancelPurchaseResponse.getTime() <= timeoutInterval);


    }

    @Test
    public void testSlaCancelPurchaseAndAllowAnalyzedPurchaseCancellationIsYes() throws Exception {

        //Preconditions - Save anonymous purchase = Always and AllowAnalyzedPurchaseCancellation = Yes
        Map<String, Object> props = new HashMap<>();
        props.put(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type());
        props.put(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key(), true);
        serverHelper.saveApiClient(props);

        //Create new customer
        String customerPhone = String.valueOf(currentTimeMillis());
        JoinClubResponse newMember = serverHelper.createMember(customerPhone);
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(customerPhone);

        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, v4Builder.buildSubmitPurchaseItemsList()) ;
        IServerResponse response = submitPurchase(customer, Lists.newArrayList(), Lists.newArrayList());

        //Response confirmation
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        CancelPurchase cancelPurchase = new CancelPurchase();
        cancelPurchase.setConfirmation(submitPurchaseResponse.getConfirmation());
        Assert.assertFalse("Confirmation must not be empty!", StringUtils.isEmpty(cancelPurchase.getConfirmation()));

        //Cancel the Purchase via Postman
        response = cancelPurchase.SendRequestByApiKey(apiKey, CancelPurchaseResponse.class);
        Integer timeoutInterval = 2000;
        CancelPurchaseResponse cancelPurchaseResponse = (CancelPurchaseResponse) response;
        Assert.assertEquals("status code must be 200", (Integer) 200, cancelPurchaseResponse.getStatusCode());
        Assert.assertEquals("Result must by Y", "Y", cancelPurchaseResponse.getResult());
        log4j.info("Response time: " + cancelPurchaseResponse.getTime());

        Assert.assertTrue("Response time must be under " + timeoutInterval + " milliseconds , time was: " + cancelPurchaseResponse.getTime(), cancelPurchaseResponse.getTime() <= timeoutInterval);

    }

    private void submitPurchase(boolean includeMemberNotes, Integer timeoutInterval) throws Exception {
        if (includeMemberNotes == true)
        {
            OperationService.cleanMemberNotes();
            String noteText = "%$firstName%, thank you for visiting :)";
            String noteTarget = MemberNotesTargetFields.POS.toString();
            String noteType = MemberNotesTypeFields.TEXT.toString();

            HubMemberNotesCreator.getInstance().createMutipleMemberNotes(noteText, noteTarget, noteType, 5);
        }

        Customer customer = createCustomer();
        String redeemCode = "1234";
        String assetType = "Asset";

        RedeemAssetOfSubmitPurchase redeemAsset = new RedeemAssetOfSubmitPurchase();
        redeemAsset.setAppliedAmount("3");
        redeemAsset.setCode(redeemCode);

        IServerResponse timedResponse = submitPurchase(customer, Lists.newArrayList(redeemAsset), Lists.newArrayList());

        // Response time must be under the specified timeoutInterval
        Assert.assertTrue("Response time must be under " + timeoutInterval + " milliseconds , time was: " + timedResponse.getTime(), timedResponse.getTime() <= timeoutInterval );
    }

    protected JoinClubResponse createMember(String customerPhone) throws Exception{
        return serverHelper.createMember(customerPhone);
    }

    private IServerResponse submitPurchase (Customer customer, ArrayList<RedeemAsset> redeemAssets,ArrayList<Deal> deals) throws Exception{
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setDefauldHeaderParams();
        submitPurchase.setCustomers(Lists.newArrayList(customer));

        if (redeemAssets.size() > 0)
            submitPurchase.setRedeemAssets(redeemAssets);
        if (deals.size() > 0)
            submitPurchase.setDeals(deals);

        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(500,1);
        Purchase purchase = v4Builder.buildPurchase(uuid,500, Lists.newArrayList(purchaseItem1)) ;
        submitPurchase.setPurchase(purchase);

        IServerResponse timedResponse = submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
        SubmitPurchaseResponse response = (SubmitPurchaseResponse)timedResponse;
        response.getConfirmation();
        if(timedResponse instanceof ZappServerErrorResponse)
            Assert.fail("Expected an error!");

        return timedResponse;
    }

    private Customer createCustomer() throws Exception {
        //Create new customer
        String customerPhone = String.valueOf(currentTimeMillis());
        JoinClubResponse member = createMember(customerPhone);
        Assert.assertNotNull(member);
        Assert.assertNotNull(member.getMembershipKey());
        String memberKey = member.getMembershipKey();

        AssignAsset.assignAssetToMember("gift", "None", memberKey, apiKey, locationId, serverToken, customerPhone);

        Customer customer = new Customer();
        customer.setPhoneNumber(customerPhone);
        return customer;
    }
}
