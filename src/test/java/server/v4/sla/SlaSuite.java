package server.v4.sla;

import hub.base.BaseHubTest;
import integration.base.BaseIntegrationTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import server.base.BaseServerTest;
import server.v4.*;

import java.io.IOException;

/**
 * Created by Goni on 11/23/2017.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        SlaGetMemberDetailsTest.class,
        SlaPaymentTest.class,
        SlaSubmitPurchaseTest.class

})
public class SlaSuite {
    public static final Logger log4j = Logger.getLogger(SlaSuite.class);


    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseIntegrationTest.init();
    }

    @AfterClass
    public static void tearDown(){

        BaseServerTest.cleanBase();
        log4j.info("close browser after finish test");
        BaseHubTest.cleanHub();
    }

}

