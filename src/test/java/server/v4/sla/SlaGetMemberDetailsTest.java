package server.v4.sla;

import com.google.common.collect.Lists;
import hub.common.objects.operations.MemberNotesTargetFields;
import hub.common.objects.operations.MemberNotesTypeFields;
import hub.hub1_0.services.OperationService;
import hub.utils.HubMemberNotesCreator;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import server.common.IServerResponse;
import server.common.asset.AssignAsset;
import server.utils.V4Builder;
import server.v2_8.JoinClubResponse;
import server.v2_8.RegisterResponse;
import server.v4.GetMemberDetails;
import server.v4.GetMemberDetailsResponse;
import server.v4.base.ZappServerErrorResponse;
import server.v4.common.enums.V4ConstantsLabels;
import server.v4.common.models.Customer;
import server.v4.common.models.MemberDetailsQueryParams;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

import static java.lang.System.currentTimeMillis;


/**
 * Created by Ariel on 8/6/2017.
 */
public class SlaGetMemberDetailsTest extends BaseIntegrationTest {

    private V4Builder v4Builder = new V4Builder(apiKey);

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }

    /***
     * GetMemberDetails with Expand
     ***/

    @Test
    public void testSlaGetMemberDetailsWithExpand() throws Exception
    {
        MemberDetailsQueryParams queryParams =  new MemberDetailsQueryParams();
        queryParams.setExpand(Lists.newArrayList(V4ConstantsLabels.ASSETS_REDEEMABLE.toString()));
        getMemberDetailsWithAssetsAndQueryParams(queryParams, 10, 2000);
    }

    /***
     * GetMemberDetails with Redeem Conditions
     ***/

    @Test
    public void testSlaGetMemberDetailsWithTestRedeemConditions() throws Exception
    {
        MemberDetailsQueryParams queryParams =  new MemberDetailsQueryParams();
        queryParams.setReturnAssets(V4ConstantsLabels.ASSET_STATUS_ALL.toString());
        getMemberDetailsWithAssetsAndQueryParams(queryParams, 10, 2000);
    }

    /***
     * GetMemberDetails with Member Notes
     ***/

    @Test
    public void testSlaGetMemberDetailsWithMemberNotes() throws Exception
    {
        OperationService.cleanMemberNotes();
        String noteText = "%$firstName%, thank you for visiting :)";
        String noteTarget = MemberNotesTargetFields.POS.toString();
        String noteType = MemberNotesTypeFields.TEXT.toString();

        HubMemberNotesCreator.getInstance().createMutipleMemberNotes(noteText, noteTarget, noteType, 5);

        //Create member and Customer
        String customerPhone = String.valueOf(currentTimeMillis());
        JoinClubResponse member = serverHelper.createMember(customerPhone);
        Customer customer = new Customer();
        customer.setPhoneNumber(customerPhone);

        GetMemberDetails memberDetails = v4Builder.buildMemberDetailsRequest(customer);
        IServerResponse timedResponse = v4Builder.getGetMemberDetailsResponse(memberDetails);

        // make sure the response is not Error response
        if (timedResponse instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) timedResponse).getErrorMessage(), false);
        }

        Integer timeoutInterval = 1000;
        // Response time must be under the specified timeoutInterval
        Assert.assertTrue("Response time must be under " + timeoutInterval + " milliseconds, time was: " + timedResponse.getTime(), timedResponse.getTime() <= timeoutInterval );
    }

    private void getMemberDetailsWithAssetsAndQueryParams (MemberDetailsQueryParams queryParams, Integer assetCount, Integer timeoutInterval) throws Exception {
        //Create new customer
        String customerPhone = String.valueOf(currentTimeMillis());
        JoinClubResponse member = serverHelper.createMember(customerPhone);
        Assert.assertNotNull(member);
        Assert.assertNotNull(member.getMembershipKey());

        String memberKey = member.getMembershipKey();
        String phoneNumber =customerPhone;

        //Build GetMemberDetails request in order to get UserKey
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        GetMemberDetails getMemberDetails = v4Builder.buildMemberDetailsRequest(customer);

        //Send GetMemberDetails request
        IServerResponse response = getMemberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response).getErrorMessage(), false);
        }

        for (int i = 0 ; i < assetCount ; i++)
        {
            AssignAsset.assignAssetToMember("gift", "Auto", memberKey, apiKey, locationId, serverToken,customerPhone);
        }

        IServerResponse timedResponse = v4Builder.getMemberDetailsWithQueryParamsResponse(customer, queryParams);

        // make sure the response is not Error response
        if (timedResponse instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) timedResponse).getErrorMessage(), false);
        }

        // Response time must be under the specified timeoutInterval
        Assert.assertTrue("Response time must be under " + timeoutInterval + " milliseconds , time was: " + timedResponse.getTime(), timedResponse.getTime() <= timeoutInterval );
    }
}
