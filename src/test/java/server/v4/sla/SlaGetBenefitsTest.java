package server.v4.sla;

import com.google.common.collect.Lists;
import hub.services.AcpService;
import org.apache.commons.collections.map.HashedMap;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import server.common.*;
import server.common.asset.NewAsset;
import server.common.asset.NewAssetResponse;
import server.internal.QueryUsers;
import server.utils.ServerHelper;
import server.v2_8.GetMemberDetails;
import server.v2_8.GetMemberDetailsResponse;
import server.v2_8.JoinClubResponse;
import server.v2_8.RedeemAssetResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Asset;
import server.v4.*;
import server.v4.common.enums.ExpandTypes;
import server.v4.common.enums.ReturnedBenefits;
import server.v4.common.models.*;

import java.io.*;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Goni on 11/20/2017.
 *
 * Tests getBenefits : Run without HUB
 *      Multi items/benefits
 *      Non-Multi items/benefits
 */
public class SlaGetBenefitsTest extends GetBenefitsTest {

    private static final int timeoutInterval = 2000;
    private static long totalTime = 0;

    private static final String resourcesDir = "src" + File.separator + "main"+ File.separator +"resources"+ File.separator + "slaBenefits"+ File.separator;
    public static final String IMPORT_FOLDER = "import";
    public static final String IMPORT_2K_FILENAME = "2004KImport.csv";

    private static List<String> assetsKeys = new ArrayList<>();
    private static List<Deal> deals = new ArrayList<>();
    private static List<String> assetsTemplates = new ArrayList<>();
    private static List<String> assetsTemplatesKeys = new ArrayList<>();
    private static List<Member> members = new ArrayList();
    private static Map<Member,List<Asset>> memberAssetsMap = new HashedMap();

    private class Member{

        public String key;
        public String phone;
        public Member(String key,String phone){
            this.key = key;
            this.phone = phone;
        }
    }


    @After
    public void deactivateAssets(){
         for (String key: assetsKeys ) {
             serverHelper.deactivateAsset(key);
         }
    }

    @Test
    public void testSlaGetBenefitsMultiItems5TimesOn10Member() throws Exception {
        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);
        totalTime = 0;
        resetDataForTest();
        initAssetsForMultiBenefits();

        for (int i = 0; i < 10; i++) {
            createMemberAndAssignAssets();
        }

        TimeUnit.MINUTES.sleep(3);//wait for cache to be updated with assigned assets

        for (Member member:members) {
            List<Asset> memberAssets = getMemberAssets(member);
            memberAssetsMap.put(member,memberAssets);
        }

        for (Member member:members) {
            System.out.println("Member: " + member.phone);
            ArrayList<RedeemAsset> redeemAssets = createRedeemAssets(member.key,memberAssetsMap.get(member));
            int randomNum = randomInt(1,5);
            Purchase purchase = buildPurchase(20,String.valueOf(randomNum));
            for (int i = 0; i < 5; i++) {
                GetBenefits getBenefits = createGetBenefitsRequest(Lists.newArrayList(member),Lists.newArrayList(redeemAssets), ReturnedBenefits.ALL,purchase);
                GetBenefitsResponse response = getGetBenefitsResponse(getBenefits);
                System.out.println("GetBenefits one request time: " + response.getTime());
                totalTime = totalTime + response.getTime();
            }
        }


        log4j.info("*****Response total time: " + totalTime + " *******************");
        long avarage = totalTime/(3*members.size());
        log4j.info("*****Response avarage time: " + avarage + " *******************");
        Assert.assertTrue("Response time must be under " + timeoutInterval + " milliseconds , time was: " + avarage,avarage <= timeoutInterval );

    }

    //CNP-6518
    @Test
    public void testSlaGetBenefitsMultiItems() throws Exception {

        totalTime = 0;
        resetDataForTest();
        initAssetsForMultiBenefits();

        for (int i = 0; i < 20; i++) {
            createMemberAndAssignAssets();
        }

        TimeUnit.MINUTES.sleep(3);//wait for cache to be updated with assigned assets

        for (Member member:members) {
            List<Asset> memberAssets = getMemberAssets(member);
            memberAssetsMap.put(member,memberAssets);
        }


        checkGetBenefitsResponse(20);
    }

    @Test
    public void testSlaGetBenefitsMultiItemsParallel() throws Exception {

        totalTime = 0;
        resetDataForTest();
        initAssetsForMultiBenefits();

        for (int i = 0; i < 100; i++) {
            createMemberAndAssignAssets();
        }

        TimeUnit.MINUTES.sleep(3);//wait for cache to be updated with assigned assets

        for (Member member:members) {
            List<Asset> memberAssets = getMemberAssets(member);
            memberAssetsMap.put(member,memberAssets);
        }


        checkGetBenefitsResponseParallel(20);
    }

    //CNP-6518
    @Test
    public void testSlaGetBenefitsNonMultiItems() throws Exception {

        totalTime = 0;
        resetDataForTest();

        initAssetsForNonMultiBenefits();

        for (int i = 0; i < 10; i++) {
            createMemberAndAssignAssets();
        }

        TimeUnit.MINUTES.sleep(3);//wait for cache to be updated with assigned assets
        for (Member member : members) {
            List<Asset> memberAssets = getMemberAssets(member);
            memberAssetsMap.put(member, memberAssets);
        }

        checkGetBenefitsResponse(10);

    }

    @Test
    public void testSlaGetBenefitsNonMultiItemsParallel() throws Exception {

        totalTime = 0;
        resetDataForTest();

        initAssetsForNonMultiBenefits();

        for (int i = 0; i < 20; i++) {
            createMemberAndAssignAssets();
        }

        TimeUnit.MINUTES.sleep(3);//wait for cache to be updated with assigned assets
        for (Member member : members) {
            List<Asset> memberAssets = getMemberAssets(member);
            memberAssetsMap.put(member, memberAssets);
        }

        checkGetBenefitsResponseParallel(10);

    }

    @Test
    public void testSlaGetBenefits30ClubDealsNonMultiItemsParallel1000Members() throws Exception {

        totalTime = 0;
        resetDataForTest();

        //create smart Club Deals with discounts(30)
        for (int i = 0; i < 10 ; i++) {
            getNewAssetResponse(resourcesDir + "clubDeal-DiscountAmountOff.json",5);
            getNewAssetResponse(resourcesDir + "clubDeal-DiscountfreeItem.json",5);
            getNewAssetResponse(resourcesDir + "clubDeal-DiscountPercentOff.json",5);
        }

        import2000Members();

        checkGetBenefitsResponseParallel(30);

    }

    @Test
    public void testSlaSubmitPurchasesNonMultiItems10Members() throws Exception {

        totalTime = 0;
        resetDataForTest();

        initAssetsForNonMultiBenefits();

        for (int i = 0; i < 10; i++) {
            createMemberAndAssignAssets();
        }

        TimeUnit.MINUTES.sleep(3);//wait for cache to be updated with assigned assets
        for (Member member : members) {
            List<Asset> memberAssets = getMemberAssets(member);
            memberAssetsMap.put(member, memberAssets);
        }

        checkSubmitPurchaseResponse(10);

    }

    @Test
    public void testSlaSubmitPurchasesMultiItems20Members() throws Exception {

        totalTime = 0;
        resetDataForTest();

        initAssetsForMultiBenefits();

        for (int i = 0; i < 20; i++) {
            createMemberAndAssignAssets();
        }

        TimeUnit.MINUTES.sleep(3);//wait for cache to be updated with assigned assets
        for (Member member : members) {
            List<Asset> memberAssets = getMemberAssets(member);
            memberAssetsMap.put(member, memberAssets);
        }

        checkSubmitPurchaseResponse(20);

    }

    private void import2000Members(){
        try {
            AcpService.importMembers(IMPORT_FOLDER + File.separator + IMPORT_2K_FILENAME);
            TimeUnit.MINUTES.sleep(2);
            QueryUsers queryUsers = new QueryUsers(serverToken,locationId);
            Object response = queryUsers.sendRequestByClass(Object.class);

            if(response instanceof ZappServerErrorResponse){
                Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
            }
            List<Object> membershipArr = (List<Object>)response;
            if(membershipArr.isEmpty()) {
                log4j.info("No members found.... check if import succeeded ");
                return;
            }
            int size = membershipArr.size();

            for(int i=0; i < size ;i++) {
                String key = (String)((LinkedHashMap) membershipArr.get(i)).get("Key");
                String phone = (String)((LinkedHashMap) membershipArr.get(i)).get("PhoneNumber");
                members.add(new Member(key,phone));
            }

        } catch (Exception e) {
            log4j.error("Import failed");
            Assert.fail(e.getMessage());
        }
    }

    private void checkSubmitPurchaseResponse(int numOfItemsPerPurchaseTag) throws Exception{
        int randomNum = randomInt(1,5);
        Purchase purchase = buildPurchase(numOfItemsPerPurchaseTag,String.valueOf(randomNum));
        List<SubmitPurchase> requests = new ArrayList<>();

        for (Member member:members) {
            System.out.println("Member: " + member.phone);
            Customer customer = new Customer();
            customer.setPhoneNumber(member.phone);

            ArrayList<RedeemAssetOfSubmitPurchase> redeemAssets = createRedeemAssetsOfSubmitPurchase(member.key,memberAssetsMap.get(member));
            SubmitPurchase submitPurchase = submitPurchase(customer,Lists.newArrayList(redeemAssets), Lists.newArrayList(deals),purchase);
            requests.add(submitPurchase);
        }
        //runParallel(executor, requests);
        for ( SubmitPurchase submitPurchase:requests) {
            SubmitPurchaseResponse response =  (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
            System.out.println("SubmitPurchase one request time: " + response.getTime());
            totalTime = totalTime + response.getTime();
        }


        log4j.info("*****Response total time: " + totalTime + " *******************");
        log4j.info("*****Num of requests : " + requests.size() + " *******************");
        long avarage = totalTime/requests.size();
        log4j.info("*****Response avarage time: " + avarage + " *******************");
        Assert.assertTrue("Response time must be under " + timeoutInterval + " milliseconds , time was: " + avarage,avarage <= timeoutInterval );
    }

    private SubmitPurchase submitPurchase (Customer customer, List<RedeemAsset> redeemAssets,List<Deal> deals,Purchase purchase) throws Exception{
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setDefauldHeaderParams();
        submitPurchase.setCustomers(com.google.appengine.labs.repackaged.com.google.common.collect.Lists.newArrayList(customer));

        if (redeemAssets.size() > 0)
            submitPurchase.setRedeemAssets(redeemAssets);
        if (deals.size() > 0)
            submitPurchase.setDeals(deals);

        submitPurchase.setPurchase(purchase);

        return submitPurchase;
    }

    private void resetDataForTest() {
        assetsKeys = new ArrayList<>();
        assetsTemplates = new ArrayList<>();
        members = new ArrayList<>();
        memberAssetsMap = new HashedMap();
    }

    private void checkGetBenefitsResponseParallel(int numOfItemsPerPurchaseTag) {

        ExecutorService executor = Executors.newFixedThreadPool(numOfItemsPerPurchaseTag);

        List<GetBenefits> requests = new ArrayList<>();


        for (Member member:members) {
            ArrayList<RedeemAsset> redeemAssets = new ArrayList<>();
            System.out.println("Member: " + member.phone);
            int randomNum = randomInt(1,5);
            if(!memberAssetsMap.isEmpty())
                redeemAssets = createRedeemAssets(member.key,memberAssetsMap.get(member));
            GetBenefits getBenefits = createGetBenefitsRequest(Lists.newArrayList(member),Lists.newArrayList(redeemAssets), ReturnedBenefits.ALL,buildPurchase(numOfItemsPerPurchaseTag,String.valueOf(randomNum)));
            requests.add(getBenefits);
        }

        totalTime = System.currentTimeMillis();
        runParallel(executor, requests);
        totalTime = System.currentTimeMillis() - totalTime;
        log4j.info("*****Response total time: " + totalTime + " *******************");
        log4j.info("*****Num of requests : " + requests.size() + " *******************");
        long avarage = totalTime/requests.size();
        log4j.info("*****Response avarage time: " + avarage + " *******************");
        Assert.assertTrue("Response time must be under " + timeoutInterval + " milliseconds , time was: " + avarage,avarage <= timeoutInterval );
    }

    private void checkGetBenefitsResponse(int numOfItemsPerPurchaseTag) {

        for (Member member:members) {
                System.out.println("Member: " + member.phone);
                int randomNum = randomInt(1,5);
                ArrayList<RedeemAsset> redeemAssets = createRedeemAssets(member.key,memberAssetsMap.get(member));
                GetBenefits getBenefits = createGetBenefitsRequest(Lists.newArrayList(member),Lists.newArrayList(redeemAssets), ReturnedBenefits.ALL,buildPurchase(numOfItemsPerPurchaseTag,String.valueOf(randomNum)));
                GetBenefitsResponse response = getGetBenefitsResponse(getBenefits);
                System.out.println("GetBenefits one request time: " + response.getTime());
                totalTime = totalTime + response.getTime();
        }

        log4j.info("*****Response total time: " + totalTime + " *******************");
        log4j.info("*****Num of requests : " + members.size() + " *******************");
        long avarage = totalTime/members.size();
        log4j.info("*****Response avarage time: " + avarage + " *******************");
        Assert.assertTrue("Response time must be under " + timeoutInterval + " milliseconds , time was: " + avarage,avarage <= timeoutInterval );
    }

    private void runParallel(ExecutorService executor, List<GetBenefits> requests) {
        for ( GetBenefits getBenefits:requests) {
            Runnable worker = new MyRunnable(getBenefits);
            executor.execute(worker);
        }

        executor.shutdown();
        // Wait until all threads are finish
        while (!executor.isTerminated()) {

        }
    }


    /**
     * Returns a psuedo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     *
     * @param min Minimim value
     * @param max Maximim value.  Must be greater than min.
     * @return Integer between min and max, inclusive.
     * @see java.util.Random#nextInt(int)
     */
    private static int randomInt(int min, int max) {

        // Usually this can be a field rather than a method variable
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    /**
     * Register member and create assets and assigned to member
     */
    private void createMemberAndAssignAssets() throws Exception {
        //register member

        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();
        String userKey = newMember.getUserKey();

        for (String template : assetsTemplates) {
            assignAssetToMember(memberKey,userKey,template);
        }
       // members.add(new Member(newMember.getPhoneNumber(),newMember.getMembershipKey()));
        members.add(new Member(newMember.getMembershipKey(),newMember.getPhoneNumber()));
    }




    private void initAssetsForNonMultiBenefits() throws Exception {

        //create smart Club Deals with discounts(30)
        for (int i = 0; i < 10 ; i++) {
            getNewAssetResponse(resourcesDir + "clubDeal-DiscountAmountOff.json",5);
            getNewAssetResponse(resourcesDir + "clubDeal-DiscountfreeItem.json",5);
            getNewAssetResponse(resourcesDir + "clubDeal-DiscountPercentOff.json",5);
        }


        createAsset(resourcesDir + "smartGiftItemCode.json",-1);
        createAsset(resourcesDir + "smartGiftDealCode.json",-1);

        //2 assets with 3rd party code
        createAsset(resourcesDir + "smartGiftDiscountAmountOff3rdPartyCode.json",5);
        createAsset(resourcesDir + "smartGiftDiscountAmountOff3rdPartyCode.json",5);

        //3 non-3rd party codes (8)
        createAsset(resourcesDir + "smartGiftDiscountFreeItemAutogenerateCode.json",5);
        createAsset(resourcesDir + "smartGiftDiscountPercentOffBulkCode.json",5);
        createAsset(resourcesDir + "smartGiftDiscountPercentOffBulkCode.json",5);
        createAsset(resourcesDir + "smartGiftDiscountFreeItemAutogenerateCode.json",5);
        createAsset(resourcesDir + "smartGiftDiscountPercentOffBulkCode.json",5);
        createAsset(resourcesDir + "smartGiftDiscountPercentOffBulkCode.json",5);
        createAsset(resourcesDir + "smartGiftDiscountFreeItemAutogenerateCode.json",5);
        createAsset(resourcesDir + "smartGiftDiscountPercentOffBulkCode.json",5);

        for (int i = 0; i < assetsTemplatesKeys.size(); i++) {
            serverHelper.add3rdPartyRedeemCodeBulkFromApi(assetsTemplatesKeys.get(i));
        }
    }

    private void initAssetsForMultiBenefits() throws Exception {

        //create smart Club Deals with discounts(5)
        getNewAssetResponse(resourcesDir + "clubDeal-DiscountAmountOff.json",20);
        getNewAssetResponse(resourcesDir + "clubDeal-DiscountAmountOff.json",20);

        getNewAssetResponse(resourcesDir + "clubDeal-DiscountfreeItem.json",20);
        getNewAssetResponse(resourcesDir + "clubDeal-DiscountfreeItem.json",20);

        getNewAssetResponse(resourcesDir + "clubDeal-DiscountPercentOff.json",20);

        //create smart Club Deals without discounts((25)
        for (int i = 0; i < 10; i++) {
            getNewAssetResponse(resourcesDir + "clubDeal-DealCode.json",-1);
        }
        for (int i = 0; i < 10; i++) {
            getNewAssetResponse(resourcesDir + "clubDeal-ItemCode.json",-1);
        }

        for (int i = 0; i < 5 ; i++) {
            createAsset(resourcesDir + "smartGiftItemCode.json",-1);
        }
        for (int i = 0; i < 5 ; i++) {
            createAsset(resourcesDir + "smartGiftDealCode.json",-1);
        }

        //2 assets with 3rd party code
        createAsset(resourcesDir + "smartGiftDiscountAmountOff3rdPartyCode.json",20);
        createAsset(resourcesDir + "smartGiftDiscountAmountOff3rdPartyCode.json",20);

        //3 non-3rd party codes
        createAsset(resourcesDir + "smartGiftDiscountFreeItemAutogenerateCode.json",20);
        createAsset(resourcesDir + "smartGiftDiscountPercentOffBulkCode.json",20);
        createAsset(resourcesDir + "smartGiftDiscountPercentOffBulkCode.json",20);

        for (int i = 0; i < assetsTemplatesKeys.size(); i++) {
            serverHelper.add3rdPartyRedeemCodeBulkFromApi(assetsTemplatesKeys.get(i));
        }


    }


    private List<Asset> getMemberAssets(Member newMember) {
        server.v2_8.common.Customer customer = new server.v2_8.common.Customer();
        customer.setPhoneNumber(newMember.phone);
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.getCustomers().add(customer);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(false);

        //Send GetMemberDetails request
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) getMemberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);

        return getMemberDetailsResponse.getMemberships().get(0).getAssets();
    }


    private ArrayList<RedeemAssetOfSubmitPurchase> createRedeemAssetsOfSubmitPurchase(String memberKey, List<Asset> assets) {
        ArrayList<RedeemAssetOfSubmitPurchase> redeemAssets = new ArrayList<>();
        //create RedeemAsset for getBenefits request
        for (int i = 0; i < assets.size(); i++) {
            Asset asset = assets.get(i);
            String assetKey = asset.getKey();
            RedeemAssetOfSubmitPurchase redeemAsset = new RedeemAssetOfSubmitPurchase();
            redeemAsset.setAppliedAmount("0");
            String code = getRedeemCode(assetKey, memberKey);
            if(code == null ) {
                redeemAsset.setKey(assetKey);
                redeemAssets.add(redeemAsset);
            }else {
                if(!code.equals("Asset Already Redeemed")) {
                    redeemAsset.setCode(code);
                    redeemAssets.add(redeemAsset);
                }
            }


        }
        return redeemAssets;
    }


    private ArrayList<RedeemAsset> createRedeemAssets(String memberKey, List<Asset> assets) {
        ArrayList<RedeemAsset> redeemAssets = new ArrayList<>();
        //create RedeemAsset for getBenefits request
        for (int i = 0; i < assets.size(); i++) {
            Asset asset = assets.get(i);
            String assetKey = asset.getKey();
            RedeemAsset redeemAsset = new RedeemAsset();
            String code = getRedeemCode(assetKey, memberKey);
            if(code == null ) {
                redeemAsset.setKey(assetKey);
                redeemAssets.add(redeemAsset);
            }else {
                if(!code.equals("Asset Already Redeemed")) {
                    redeemAsset.setCode(code);
                    redeemAssets.add(redeemAsset);
                }
            }


        }
        return redeemAssets;
    }

    private void createAsset(String filePath,int quantityForBusket) throws IOException {

        NewAssetResponse newAssetResponse = getNewAssetResponse(filePath,quantityForBusket);
        String template = newAssetResponse.getTemplate();
        assetsTemplates.add(template);
        assetsTemplatesKeys.add(newAssetResponse.getKey());
    }

    @Override
    protected String getRedeemCode(String assetKey, String memKey){

        //in order to get redeem code need to create v2_8 redeem asset
        //the code returns with the response
        server.v2_8.RedeemAsset redeemAsset = new server.v2_8.RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(assetKey);
        redeemAsset.setMembershipKey(memKey);
        redeemAsset.setToken(serverToken);
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);
        if (response instanceof server.v2_8.base.ZappServerErrorResponse)
            return "Asset Already Redeemed";

        return ((RedeemAssetResponse)response).getCode();
    }

    private NewAssetResponse getNewAssetResponse(String filePath,Integer quantityForBusket) throws IOException {
        String url = "%s%s?InputType=Json&MimeType=application/json&Token=%s";
        String templateName = "smartAsset_" + ServerHelper.getShortUUID();
        String assetName = ServerHelper.getShortUUID();
        String body = readFileAsString(filePath);
        String formattedBody = "";
        if(quantityForBusket == -1)
            formattedBody = String.format(body,serverToken,locationId,templateName,assetName);
        else
            formattedBody = String.format(body,serverToken,quantityForBusket,quantityForBusket,quantityForBusket,quantityForBusket,quantityForBusket,locationId,templateName,assetName);

        NewAsset newAsset = new NewAsset();
        NewAssetResponse newAssetResponse = (NewAssetResponse) newAsset.sendRequestWithGivenBody(url,serverToken,formattedBody, NewAssetResponse.class);
        assetsKeys.add(newAssetResponse.getKey());
        if(newAssetResponse.getRawObject().get("Type").equals("deal")) {
            Deal deal = new Deal();
            deal.setKey(newAssetResponse.getKey());
            deal.setAppliedAmount("0");
            deals.add(deal);
        }
        return newAssetResponse;
    }

    private String readFileAsString(String filePath) throws IOException {
        InputStream is = new FileInputStream(filePath);
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));
        String line = buf.readLine();
        StringBuilder sb = new StringBuilder();
        while(line != null){
            sb.append(line).append("\n");
            line = buf.readLine();
        }
        return sb.toString();
    }

    private void assignAssetToMember(String membershipKey,String userKey,String template){
        InvokeEventBasedActionRuleBuilder assignRequest = new InvokeEventBasedActionRuleBuilder();
        InvokeEventBasedActionRule request = assignRequest.buildAssignAssetRequest(membershipKey, userKey, template, locationId);

        //Send assign asset via InvokeEventBasedActionRule
        IServerResponse basedActionResponse = request.SendRequestByToken(serverToken, Response.class);
        if(basedActionResponse instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse) basedActionResponse).getErrorMessage());
        }

    }


    private Purchase buildPurchase(int numOfItemsPerPurchaseTag , String PurchaseTag){
        List<PurchaseItem> items = new ArrayList<>();

        final String DEPARTMENT_CODE_1 = "1";
        final String DEPARTMENT_CODE_2 = "2";
        final String DEPARTMENT_CODE_3 = "3";
        final String DEPARTMENT_CODE_4 = "4";
        final String DEPARTMENT_CODE_5 = "5";
        for (int i = 0; i <numOfItemsPerPurchaseTag; i++) {
            items.add(buildPurchaseItem("1234",DEPARTMENT_CODE_1,1, 1000,"1"));
            items.add(buildPurchaseItem("2345",DEPARTMENT_CODE_2,1, 1000, "2"));
            items.add(buildPurchaseItem("3456",DEPARTMENT_CODE_3,1, 1000, "3"));
            items.add(buildPurchaseItem("4567",DEPARTMENT_CODE_4,1, 1000, "4"));
            items.add(buildPurchaseItem("5678",DEPARTMENT_CODE_5,1, 1000, "5"));
        }

        //Create purchase
        Purchase purchase = v4Builder.buildPurchase(UUID.randomUUID().toString(),(numOfItemsPerPurchaseTag*5000),items);
        purchase.setTags(Lists.newArrayList(PurchaseTag));
        return purchase;
    }

    private  PurchaseItem buildPurchaseItem(String itemCode,String depCode,int quantity,int price, String PurchaseTag){
        PurchaseItem purchaseItem = v4Builder.buildPurchaseItem(itemCode,quantity, price, depCode, depCode);
        purchaseItem.setTags(Lists.newArrayList(PurchaseTag));
        return purchaseItem;
    }

    private GetBenefits createGetBenefitsRequest(List<Member> members,ArrayList<RedeemAsset> assets, ReturnedBenefits returnedBenefits,Purchase purchase) {
        GetBenefits getBenefits = new GetBenefits(apiKey);

        for (Member member:members) {
            Customer customer = new Customer();
            customer.setPhoneNumber(member.phone);
            getBenefits.getCustomers().add(customer);
        }

        getBenefits.setRedeemAssets(assets);


        //add query parameters
        setQueryParams(Lists.newArrayList(ExpandTypes.DISCOUNT_BY_DISCOUNT.toString()),false, returnedBenefits.toString(),getBenefits);
        //add default purchase
        getBenefits.setPurchase(purchase);
        return getBenefits;
    }

    private class MyRunnable implements Runnable {

        private GetBenefits getBenefits;
        GetBenefitsResponse response;

        MyRunnable(GetBenefits getBenefits){
            this.getBenefits = getBenefits;
        }

        @Override
        public void run() {

                System.out.println("Thread: " + Thread.currentThread().getName());
                response = getGetBenefitsResponse(getBenefits);
        }
    }
}

