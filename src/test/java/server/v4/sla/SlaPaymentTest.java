package server.v4.sla;

import com.google.appengine.api.datastore.Query;
import com.google.common.collect.Lists;
import integration.base.BaseIntegrationTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import server.common.*;
import server.utils.V4Builder;
import server.v4.*;
import server.v4.base.ZappServerErrorResponse;
import server.v4.common.enums.Mode;
import server.v4.common.enums.PaymentMethod;
import server.v4.common.enums.Type;
import server.v4.common.enums.V4ConstantsLabels;
import server.v4.common.models.*;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.UUID;

/**
 * Created by Goni on 11/20/2017.
 *
 * Tests SLA for payment & CancelPayment (Query /PAY  modes)
 */
public class SlaPaymentTest extends PaymentTest {

    private V4Builder v4Builder = new V4Builder(apiKey);
    private static Integer timeoutInterval = 2000;

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }

    @Test
    public void testSlaPaymetWithAllPurchaseAttributesPAYMode() throws Exception {

        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        //set payment options
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT);

        //build purchase
        int sumToPay = 50;
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(4,1);
        PurchaseItem purchaseItem2 = v4Builder.buildPurchaseItem(5,1);
        Payment paymentObj1 = v4Builder.buildPayment(Type.CASH,0);
        Payment paymentObj2 = v4Builder.buildPayment(Type.CLUB_BUDGET,0);
        Purchase purchase = v4Builder.buildPurchase(uuid,50, Lists.newArrayList(purchaseItem1,purchaseItem2),Lists.newArrayList(paymentObj1,paymentObj2),100) ;
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);

        // Send the request
        checkResponseTime(payment,10);
    }

    @Test
    public void testSlaPaymetWithAllPurchaseAttributesQUERYMode() throws Exception {

        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        //set payment options
        PaymentQueryParams queryParams = getDefaultPaymentQueryParams(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT);
        queryParams.setMode(Mode.QUERY);

        //build purchase
        String uuid = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildDefaultPurchase(uuid) ;
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,5000,purchase);

        // Send the request
        checkResponseTime(payment,10);
    }

    @Test
    public void testSlaPayTypePointstNoVarificationCodeWithPaymentMethodMeanOfPaymentBodyContainsDiscount() throws Exception {

        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        //build purchase

        PaymentAPI payment = createPaymentRequest(customer,PaymentMethod.DISCOUNT);

        checkResponseTime(payment,10);
    }


    @Test
    public void testSlaDecimalPointInPayMode () throws Exception {

        setDefaultConfigurationForTest(PaymentType.POINTS.type());
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);

        Customer customer = createMemberWithPoints();
        int pointsAfter = v4Builder.getMembersPoints(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 10000,pointsAfter);

        PaymentAPI payment = createPaymentRequest(customer,PaymentMethod.DISCOUNT);

        // Send the request
        checkResponseTime(payment,10);
    }

    @Test
    public void testSlaDecimalBudgetInPayModeWithRatio () throws Exception {

        double ratio = 0.2;
        setConfigurationForTest("false",PaymentType.BUDGET.type(),String.valueOf(ratio),"true");
        setPaymentProviderConfiguration(PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString(),true);

        Customer customer = createMemberWithBudget();
        Thread.sleep(10000);
        int pointsAfter = v4Builder.getMembersCredit(customer);
        Assert.assertEquals("Customer needs to receive 100 points", 50000,pointsAfter);

        PaymentAPI payment = createPaymentRequest(customer,PaymentMethod.DISCOUNT);

        // Send the request
        checkResponseTime(payment,10);
    }

    @Test
    public void testSlaCancelCreditPaymentWithConfirmation() throws Exception {
        /*
        steps:
        1. verify if the flag AllowCancelMemberBudgetPayment = true
        2. send pay with budget , verify if the response 200 ok
        3. cancel the budget by confirmation id
        4. verify if the  data in BudgetTransaction table correct
         */
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false));


        setConfigurationForTest("false",PaymentType.BUDGET.type(),"1","false");

        Customer customer = createMemberWithBudget();

        PaymentAPI payment = createPaymentRequest(customer,PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT);

        // Send the request
        PaymentAPIResponse paymentResponse = getPaymentAPIResponse(payment);

        String confirmation = paymentResponse.getConfirmation();
        CancelPayment cancelPayment = v4Builder.buildCancelPaymentRequest(confirmation,payment.getPurchase());
        log4j.info("Confirmation for cancel payment: " + confirmation);

        IServerResponse responseCancel = cancelPayment.SendRequestByApiKey(apiKey, CancelPaymentResponse.class);
        if (responseCancel instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) responseCancel).getErrorMessage());
        }
        Assert.assertEquals("status code must be 200", (Integer) 200, responseCancel.getStatusCode());
        Assert.assertEquals("Status must be ok", STATUS_OK, ((CancelPaymentResponse)responseCancel).getStatus());
        log4j.info("Response time: " + responseCancel.getTime());

        Assert.assertTrue("Response time must be under " + timeoutInterval + " milliseconds , time was: " + responseCancel.getTime(),responseCancel.getTime() <= timeoutInterval );

    }

    private PaymentAPI createPaymentRequest(Customer customer,PaymentMethod paymentMethod) {

        String paymentConfig = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString();
        setPaymentProviderConfiguration(paymentConfig,true);

        //set payment options
        PaymentQueryParams queryParams = getPaymentQueryParams(paymentMethod, Mode.PAY);

        int sumToPay = 50;
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = v4Builder.buildPurchaseItem(500,2);
        Payment paymentObj1 = v4Builder.buildPayment(Type.CASH,0);
        Purchase purchase = v4Builder.buildPurchase(uuid,5000, Lists.newArrayList(purchaseItem1),Lists.newArrayList(paymentObj1),100) ;
        PaymentAPI payment = v4Builder.buildPaymentAPI(queryParams,customer,sumToPay,purchase);
        return payment;
    }

    private void checkResponseTime(PaymentAPI payment,int numOfRequests) {

        int totalTime = 0;
        for (int i = 0; i < numOfRequests ; i++) {
            IServerResponse paymentResponse = getPaymentAPIResponse(payment);
            log4j.info("Response time: " + paymentResponse.getTime());
            totalTime = totalTime + paymentResponse.getTime();
        }
        int avarage = totalTime/numOfRequests;
        log4j.info("Response avarage time: " + avarage);
        Assert.assertTrue("Response time must be under " + timeoutInterval + " milliseconds , time was: " + avarage,avarage <= timeoutInterval );

    }
}

