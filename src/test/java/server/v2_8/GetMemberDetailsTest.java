package server.v2_8;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import hub.base.categories.serverCategories.ServerRegression;
import hub.base.categories.serverCategories.serverV2_8;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.base.BaseServerTest;
import server.common.ApiClientFields;
import server.common.asset.AssignAsset;
import server.utils.Initializer;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Asset;
import server.v2_8.common.Customer;
import server.v2_8.common.Membership;
import server.v2_8.common.RedeemItem;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static utils.StringUtils.StringToKey;


/**
 * Created by Lior on 11/30/2016.
 */
public class GetMemberDetailsTest extends BaseServerTest {


    @Ignore
    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }

    /**
     * C4734 - Get Member Details Sanity Test
     */
    @Test
    @Category(serverV2_8.class)
    public void testGetMemberDetailsSanityTest() throws IOException, InterruptedException {

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.getCustomers().add(customer);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(false);

        //Send the request
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse)getMemberDetails.SendRequestByApiKeyAndValidateResponse(apiKey, GetMemberDetailsResponse.class);

        Assert.assertEquals("Result must be Y", "Y", getMemberDetailsResponse.getResult());

        Assert.assertEquals("Membership must be one", 1,getMemberDetailsResponse.getMemberships().size());

    }

    /**
     * C4260 - Get Member Details with Purchase context
     * Check that response returns correctly even if purchase context is included in request.
     * (When purchase is added to request no actual purchase is made. This is a place-holder for future API's).
     * Purchase/userActions logs are not written
     */
    @Test
    @Category (serverV2_8.class)
    public void testGetMemberDetailsWithPurchaseContext1() throws Exception {

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        // Build membership details
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        ArrayList customers = new ArrayList(Arrays.asList(customer));
        GetMemberDetails getMemberDetails = Initializer.initialGetMemberDetailsRequest(customers); //attach purchase details
        getMemberDetails.setExpandAssets(true);

        // Assign Asset To Member
        Asset newAsset = AssignAsset.assignAssetToMember("gift", "Auto", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = newAsset.getKey();
        ArrayList<RedeemItem> redeemItems = new ArrayList<>();
        RedeemItem redeemItem = new RedeemItem();
        redeemItem.setAssetKey(assetKey);
        redeemItems.add(redeemItem);

        getMemberDetails.setRedeemItems(redeemItems);

        //Send the request
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse)getMemberDetails.SendRequestByApiKeyAndValidateResponse(apiKey, GetMemberDetailsResponse.class);

        Assert.assertEquals("Result must be Y", "Y", getMemberDetailsResponse.getResult());
        ArrayList<Membership> memberships = getMemberDetailsResponse.getMemberships();
        Assert.assertEquals("Membership must be one", 1, memberships.size());
        Membership membership = memberships.get(0);
        validateMembership(membership);
        Asset asset = membership.findAssetByKey(assetKey);
        Assert.assertNotNull("Asset must exist", asset);

        ArrayList<Asset> assets = membership.getAssets();
        Assert.assertNotNull("Member should have at least one asset", assets);
        for(Asset assetFromResponse : assets){
            Assert.assertNull("Redeemable flag should not appear", assetFromResponse.getRedeemable());
            Assert.assertFalse("Archived flag should be false for " + assetFromResponse.getName(), assetFromResponse.getArchived());
        }

        //Add to Cleanup
        objectsToClean.add(newAsset);

    }

    /**
     * C4261 - Get Member Details without Purchase context ("ExpandAssets":true, "TestRedeemConditions":false;"IncludeArchivedAssets":false)
     * Check that response returns correctly when purchase context is not included in request.
     * No actual logs are written except appEngine
     */
    @Test
    @Category({ServerRegression.class})
    public void testGetMemberDetailsWithoutPurchaseContext1() throws Exception {

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        // Build membership details
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        ArrayList customers = new ArrayList(Arrays.asList(customer));
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.setCustomers(customers);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(false);
        getMemberDetails.setTestRedeemConditions(false);

        Asset newAsset = AssignAsset.assignAssetToMember("gift", "Auto", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());

        //Send the request
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) getMemberDetails.SendRequestByApiKeyAndValidateResponse(apiKey, GetMemberDetailsResponse.class);

        Assert.assertEquals("Result must be Y", "Y", getMemberDetailsResponse.getResult());
        ArrayList<Membership> memberships = getMemberDetailsResponse.getMemberships();
        Assert.assertEquals("Membership must be one", 1, memberships.size());
        Membership membership = memberships.get(0);
        validateMembership(membership);
        ArrayList<Asset> assets = membership.getAssets();
        Assert.assertNotNull("Member should have at least one asset", assets);

        for(Asset assetFromResponse : assets){
            Assert.assertNull("Redeemable flag should not appear", assetFromResponse.getRedeemable());
            Assert.assertFalse("Archived flag should be false for " + assetFromResponse.getName(), assetFromResponse.getArchived());
        }

        objectsToClean.add(newAsset);
    }

    /**
     * C4262 - Get Member Details without Purchase context ("ExpandAssets":true, "TestRedeemConditions":true;"IncludeArchivedAssets":true)
     * Adding asset and burn it in order to check archived is returned in response
     */
    @Test
    public void testGetMemberDetailsWithoutPurchaseContext2() throws Exception {

        JoinClubResponse newMember = createMember();

        // Build membership details
        GetMemberDetails getMemberDetails = buildCustomer(newMember.getPhoneNumber());

        // Create asset and burn it (In order to have archived asset and later on check getMemberDetails response)
        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);
        Asset newAsset = AssignAsset.assignAssetToMember("gift", "None", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = newAsset.getKey();

        Key dataStoreAssetKey = StringToKey(assetKey);
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);

        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(assetKey);
        redeemAsset.setMembershipKey(newMember.getMembershipKey());
        redeemAsset.setToken(serverToken);
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);
        Assert.assertEquals("Result must be Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore the asset is burned
        result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Could not find asset with key: " + dataStoreAssetKey, result);
        Assert.assertEquals("Asset RedeemState must be Burned ","Burned", result.getProperty("RedeemState"));

        //Send the getMemberDetails request
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) getMemberDetails.SendRequestByApiKeyAndValidateResponse(apiKey, GetMemberDetailsResponse.class);

        Assert.assertEquals("Result must be Y", "Y", getMemberDetailsResponse.getResult());

        ArrayList<Membership> memberships = getMemberDetailsResponse.getMemberships();
        Assert.assertEquals("Membership must be one", 1, memberships.size());
        Membership membership = memberships.get(0);
        validateMembership(membership);
        ArrayList<Asset> assets = membership.getAssets();
        Assert.assertNotNull("Member should have at least one asset", assets);

        Asset asset = membership.findAssetByKey(assetKey);
        Assert.assertNotNull("Asset must exist", asset);
        Assert.assertEquals("Redeemable flag must be N", "N" ,asset.getRedeemable());
        Assert.assertTrue("Archived flag must be true",asset.getArchived());

        for(Asset assetFromResponse : assets){
            Assert.assertNotNull("Redeemable flag appear", assetFromResponse.getRedeemable());
            Assert.assertNotNull("Archived flag must appear",asset.getArchived());

        }

        //Add to Cleanup
        objectsToClean.add(newAsset);

    }


    private GetMemberDetails buildCustomer(String customerPhone) {
        Customer customer = new Customer();
        customer.setPhoneNumber(customerPhone);
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.getCustomers().add(customer);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(true);
        getMemberDetails.setTestRedeemConditions(true);
        return getMemberDetails;
    }

    /**
     * C4263 - Get Member Details with Purchase context ("ExpandAssets":true, "TestRedeemConditions":false, "IncludeArchivedAssets":false)
     */
    @Test
    @Category({ServerRegression.class})
    public void testGetMemberDetailsWithPurchaseContext2() throws Exception {

        JoinClubResponse newMember = createMember();

        // Build membership details
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        ArrayList customers = new ArrayList(Arrays.asList(customer));
        GetMemberDetails getMemberDetails = Initializer.initialGetMemberDetailsRequest(customers);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(false);
        getMemberDetails.setTestRedeemConditions(false);

        // Create asset and burn it
        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);
        Asset newAsset = AssignAsset.assignAssetToMember("gift", "None", newMember.getMembershipKey(), apiKey, locationId, serverToken,newMember.getPhoneNumber());
        String assetKey = newAsset.getKey();
        Key dataStoreAssetKey = StringToKey(assetKey);
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(assetKey);
        redeemAsset.setMembershipKey(newMember.getMembershipKey());
        redeemAsset.setToken(serverToken);
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);
        Assert.assertEquals("Result must be Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore the asset is burned
        result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Could not find asset with key: " + dataStoreAssetKey, result);
        Assert.assertEquals("Asset RedeemState must be Burned ","Burned", result.getProperty("RedeemState"));

        //Create another asset to make sure that asset that are not burned appear
        Asset newAsset2 = AssignAsset.assignAssetToMember("gift", "None", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey2 = newAsset2.getKey();

        //attach 2 redeems to getMemberDetails
        ArrayList<RedeemItem> redeemItems = new ArrayList<>();
        RedeemItem redeemItem = new RedeemItem();
        redeemItem.setAssetKey(assetKey);
        redeemItems.add(redeemItem);
        RedeemItem redeemItem2 = new RedeemItem();
        redeemItem.setAssetKey(assetKey2);
        redeemItems.add(redeemItem2);
        getMemberDetails.setRedeemItems(redeemItems);

        //Send the getMemberDetails request
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) getMemberDetails.SendRequestByApiKeyAndValidateResponse(apiKey, GetMemberDetailsResponse.class);

        Assert.assertEquals("Result must be Y", "Y", getMemberDetailsResponse.getResult());
        ArrayList<Membership> memberships = getMemberDetailsResponse.getMemberships();
        Assert.assertEquals("Membership must be one", 1, memberships.size());
        ArrayList<Asset> assets = memberships.get(0).getAssets();
        Assert.assertNotNull("Member should have at least one asset", assets);

        //this asset is burned - therefor should not appear
        Asset asset = memberships.get(0).findAssetByKey(assetKey);
        Assert.assertNull("Asset should not appear", asset);

        Asset asset2 = memberships.get(0).findAssetByKey(assetKey2);
        Assert.assertNotNull("Asset should appear", asset2);

        for(Asset assetFromResponse : assets){
            Assert.assertNull("Redeemable flag should not appear", assetFromResponse.getRedeemable());
            Assert.assertNotNull("Archived flag should appear", assetFromResponse.getArchived());
        }

        //Add to Cleanup
        objectsToClean.add(newAsset);
        objectsToClean.add(newAsset2);

    }

    /**
     * C4264 - Get Member Details with Purchase context ("ExpandAssets":true, "TestRedeemConditions":true;"IncludeArchivedAssets":true)
     */
    @Test
    public void testGetMemberDetailsWithPurchaseContext3() throws Exception {

        JoinClubResponse newMember = createMember();

        // Build membership details
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        ArrayList customers = new ArrayList(Arrays.asList(customer));
        GetMemberDetails getMemberDetails = Initializer.initialGetMemberDetailsRequest(customers);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(true);
        getMemberDetails.setTestRedeemConditions(true);

        // Create asset and burn it
        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);
        Asset newAsset = AssignAsset.assignAssetToMember("gift", "None",newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = newAsset.getKey();
        Key dataStoreAssetKey = StringToKey(assetKey);
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(assetKey);
        redeemAsset.setMembershipKey(newMember.getMembershipKey());
        redeemAsset.setToken(serverToken);
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);
        Assert.assertEquals("Result must be Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore the asset is burned
        result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Could not find asset with key: " + dataStoreAssetKey, result);
        Assert.assertEquals("Asset RedeemState must be Burned ","Burned", result.getProperty("RedeemState"));

        //attach redeem to getMemberDetails
        ArrayList<RedeemItem> redeemItems = new ArrayList<>();
        RedeemItem redeemItem = new RedeemItem();
        redeemItem.setAssetKey(assetKey);
        redeemItems.add(redeemItem);
        getMemberDetails.setRedeemItems(redeemItems);

        //Send the getMemberDetails request
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) getMemberDetails.SendRequestByApiKeyAndValidateResponse(apiKey, GetMemberDetailsResponse.class);

        Assert.assertEquals("Result must be Y", "Y", getMemberDetailsResponse.getResult());
        ArrayList<Membership> memberships = getMemberDetailsResponse.getMemberships();
        Assert.assertEquals("Membership must be one", 1, memberships.size());
        ArrayList<Asset> assets = memberships.get(0).getAssets();
        Assert.assertNotNull("Member should have at least one asset", assets);

        Asset asset = memberships.get(0).findAssetByKey(assetKey);
        Assert.assertNotNull("Asset must exist", asset);
        Assert.assertEquals("Redeemable flag must be N", "N" ,asset.getRedeemable());
        Assert.assertTrue("Archived flag must be true",asset.getArchived());

        for(Asset assetFromResponse : assets){
            Assert.assertNotNull("Redeemable flag appear", assetFromResponse.getRedeemable());
            Assert.assertNotNull("Archived flag appear",asset.getArchived());

        }

        //Add to Cleanup
        objectsToClean.add(newAsset);
    }




    /**
     * C4579 -  'Get Member Details' with 2 members,both of them are the same members identifiers
     * Verify that the result returns the correct data - Only one member returns
     */
    @Test
    @Category({ServerRegression.class})
    public void testGetMemberDetailsWith2Members() throws Exception {

        //Create new customer
        JoinClubResponse newMember = createMember();

        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "None",newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());

        // Build membership details
        Customer customer1 = new Customer();
        customer1.setPhoneNumber(newMember.getPhoneNumber());
        Customer customer2 = new Customer();
        customer2.setPhoneNumber(newMember.getPhoneNumber()); //same user twice

        ArrayList customers = new ArrayList(Arrays.asList(customer1, customer2));
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.setCustomers(customers);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(true);
        getMemberDetails.setTestRedeemConditions(true);

        //Send the getMemberDetails request
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) getMemberDetails.SendRequestByApiKeyAndValidateResponse(apiKey, GetMemberDetailsResponse.class);

        Assert.assertEquals("Result must be Y", "Y", getMemberDetailsResponse.getResult());
        ArrayList<Membership> memberships = getMemberDetailsResponse.getMemberships();
        Assert.assertEquals("List of memberships must be one", 1, memberships.size());
        Membership membership = memberships.get(0);
        validateMembership(membership);
        ArrayList<Asset> assets = membership.getAssets();
        Assert.assertNotNull("Member should have at least one asset", assets);
        Assert.assertEquals("Member should have one asset", 1, assets.size());

        Asset asset = membership.findAssetByKey(newAsset1.getKey());
        Assert.assertNotNull("Asset must exist", asset);
        Assert.assertEquals("Redeemable flag must be Y", "Y" ,asset.getRedeemable());
        Assert.assertFalse("Archived flag must be false",asset.getArchived());

    }

    /**
     * C4580 -  'Get Member Details' with 3 members,two of them with the same members identifiers + one with different identifier
     * Verify that the result returns the correct data - that member doesn't appear twice
     */
    @Test
    public void testGetMemberDetailsWith3Members() throws Exception {


        //Adding 2 new members
        Map<String, Asset> member2Asset = new HashMap<>();
        //Create new customer
        JoinClubResponse newMember = createMember();
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "None", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());
        member2Asset.put(newMember.getPhoneNumber(), newAsset1);

        JoinClubResponse newMember2 = createMember();
        Asset newAsset2 = AssignAsset.assignAssetToMember("gift", "None", newMember2.getMembershipKey(), apiKey, locationId, serverToken, newMember2.getPhoneNumber());
        member2Asset.put(newMember2.getPhoneNumber(), newAsset2);

        // Build membership details
        Customer customer1 = new Customer();
        customer1.setPhoneNumber(newMember.getPhoneNumber());
        Customer customer2 = new Customer();
        customer2.setPhoneNumber(newMember2.getPhoneNumber());

        Customer customer3 = new Customer();
        customer3.setPhoneNumber(newMember2.getPhoneNumber());//Another customer entry but with the same number as customer2

        ArrayList customers = new ArrayList(Arrays.asList(customer1, customer2, customer3));
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.setCustomers(customers);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(true);
        getMemberDetails.setTestRedeemConditions(true);

        //Send the getMemberDetails request
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) getMemberDetails.SendRequestByApiKeyAndValidateResponse(apiKey, GetMemberDetailsResponse.class);
        Assert.assertEquals("Result must be Y", "Y", getMemberDetailsResponse.getResult());
        ArrayList<Membership> memberships = getMemberDetailsResponse.getMemberships();
        Assert.assertEquals("List of memberships must be two", 2, memberships.size());
        Membership membership1 = memberships.get(0);
        validateMembership(membership1);
        ArrayList<Asset> assets = membership1.getAssets();
        Assert.assertNotNull("Member should have at least one asset", assets);
        Assert.assertEquals("Member should have one asset", 1, assets.size());
        Asset asset1 = member2Asset.get(membership1.getPhoneNumber());
        Assert.assertNotNull("Member should have one an asset from assets created in this test" , asset1);
        Asset assetFromRequest = membership1.findAssetByKey(asset1.getKey());
        Assert.assertNotNull("Asset must exist", assetFromRequest);
        Assert.assertEquals("Redeemable flag must be Y", "Y" ,assetFromRequest.getRedeemable());

        Membership membership2 = memberships.get(1);
        validateMembership(membership2);
        assets = membership2.getAssets();
        Assert.assertNotNull("Member should have at least one asset", assets);
        Assert.assertEquals("Member should have one asset", 1, assets.size());
        Asset asset2 = member2Asset.get(membership2.getPhoneNumber());
        Assert.assertNotNull("Member should have one an asset from assets created in this test" , asset2);

        assetFromRequest = membership2.findAssetByKey(asset2.getKey());
        Assert.assertNotNull("Asset must exist", assetFromRequest);
        Assert.assertEquals("Redeemable flag must be Y", "Y" ,assetFromRequest.getRedeemable());
        Assert.assertFalse("Archived flag must be false",assetFromRequest.getArchived());

        Assert.assertNotEquals("Each member should have different asset", asset1.getKey(), asset2.getKey());

    }

    private static void validateMembership(Membership membership){
        Assert.assertNotNull("membership must exist", membership);
        Assert.assertNotNull("Status must exist", membership.getStatus());
        Assert.assertNotNull("UserKey must exist", membership.getUserKey());
    }

    /**
     * 	C5873  - Get member details positive test with TemporaryToken
     */
    @Ignore
    public void testGetMemberDetailsWithTemporaryToken (){

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("10000", userToken, locationId);
        GetMemberDetails getMemberDetails = createGetMemberDetails (tokenTemp);

        //Send the request
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse)getMemberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);
        Assert.assertEquals("Result must be Y", "Y", getMemberDetailsResponse.getResult());
        Assert.assertEquals("Membership must be one", 1,getMemberDetailsResponse.getMemberships().size());

    }

    /**
     * 	C5874  - Get member details negative test with TemporaryToken
     */
    @Test
    @Category({ServerRegression.class})
    public void testGetMemberDetailsWithTemporaryTokenNonValid () throws Exception{

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("1", userToken, locationId);
        Thread.sleep(2000);
        GetMemberDetails getMemberDetails = createGetMemberDetails (tokenTemp);

        //Send the request
        ZappServerErrorResponse getMemberDetailsResponse = (ZappServerErrorResponse)getMemberDetails.SendRequestByApiKey(apiKey, ZappServerErrorResponse.class);

        Assert.assertEquals("Result must be N", "N", getMemberDetailsResponse.getResult());
        Assert.assertEquals("ErrorMessage", "One or more customers not found",getMemberDetailsResponse.getErrorMessage());

    }

    /**
     * 	C5877	Get member details positive test with TemporaryToken use few times
     */

    @Test
    public void testGetMemberDetailsWithTemporaryTokenUseFewTimes (){

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("10000", userToken, locationId);
        GetMemberDetails getMemberDetails = createGetMemberDetails (tokenTemp);

        for (int i=0; i< 10; i++){

            //Send the request
            GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse)getMemberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);
            Assert.assertEquals("Result must be Y", "Y", getMemberDetailsResponse.getResult());
            Assert.assertEquals("Membership must be one", 1,getMemberDetailsResponse.getMemberships().size());

        }

    }


    /**
     * 	C5880	Get member Details Two customers when one token is not valid
     */

    @Test
    public void testGetMemberDetailsWithTemporaryToken2customers () throws Exception{

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("10000",userToken, locationId);
        String userToken2 = serverHelper.createNewMemberGetUserToken();
        String tokenTemp2 = serverHelper.generateTokenWebViewUserToken("1",userToken2, locationId);

        GetMemberDetails getMemberDetails = createGetMemberDetails2Customers(tokenTemp, tokenTemp2);
        Thread.sleep(2000);

        //Send the request
        ZappServerErrorResponse getMemberBenefitsResponse = (ZappServerErrorResponse)getMemberDetails.SendRequestByApiKey(apiKey, ZappServerErrorResponse.class);
        Assert.assertEquals("Result must be N", "N", getMemberBenefitsResponse.getResult());
        Assert.assertEquals("ErrorMessage", "One or more customers not found",getMemberBenefitsResponse.getErrorMessage());
    }

    /**
     * 	C5874  - Get member details negative test with TemporaryToken
     */
    @Test
    public void testGetMemberDetailsWithTemporaryTokenNonValidTokenNon () throws Exception{

        String tokenTemp = "1234";
        GetMemberDetails getMemberDetails = createGetMemberDetails (tokenTemp);

        //Send the request
        ZappServerErrorResponse getMemberDetailsResponse = (ZappServerErrorResponse)getMemberDetails.SendRequestByApiKey(apiKey, ZappServerErrorResponse.class);

        Assert.assertEquals("Result must be N", "N", getMemberDetailsResponse.getResult());
        Assert.assertEquals("ErrorMessage", "One or more customers not found",getMemberDetailsResponse.getErrorMessage());

    }


    public GetMemberDetails createGetMemberDetails (String tokenTemp) {

        Customer customer = new Customer();
        customer.setTemporaryToken(tokenTemp);
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.getCustomers().add(customer);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(false);

        return getMemberDetails;

    }

    public GetMemberDetails createGetMemberDetails2Customers (String tokenTemp, String tokenTemp2) {

        Customer customer = new Customer();
        customer.setTemporaryToken(tokenTemp);
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.getCustomers().add(customer);

        Customer customer2 = new Customer();
        customer2.setTemporaryToken(tokenTemp2);
        getMemberDetails.getCustomers().add(customer2);

        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(false);

        return getMemberDetails;

    }



}
