package server.v2_8;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import server.base.BaseServerTest;
import server.common.ApiClientBuilder;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.common.asset.AssignAsset;
import server.common.asset.NewAssetResponse;
import server.common.asset.conditions.AssetDateCondition;
import server.utils.RedeemErrorCodesMap;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Asset;
import server.v2_8.common.Customer;
import server.v2_8.common.RedeemItem;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

import static utils.StringUtils.StringToKey;


public class RedeemResponseDetailedCodesTest extends BaseServerTest {


    public final String STATUS = "Status";
    public final String REDEEM_STATE = "RedeemState";
    public final String ERROR_CODE = "ErrorCode";
    public final String RESULT = "Result";
    public final String ERROR_DISPLAY_STRING  = "ErrorDisplayString";
    public final String ERROR_MESSAGE = "ErrorMessage";

    private static String assetNotRedeemableAtThisLocation;


    @BeforeClass
    public static void _init() throws Exception {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        updateConfiguration = true;
        /**
         * We create a different location here to create assigned asset on other location to use in 2 tests.
         * After creating the asset, we set the firstTime variable to true and start the test again with new location.
         * We must varify that this test will run first in the suite.
         * */
        assetNotRedeemableAtThisLocation = getAssetFromDifferentLocation();
        firstTime = true;
        init();
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.DETAILED_REDEEM_RESPONSE.key(), true),
                ApiClientBuilder.newPair(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(), true));
    }




    /**
     *
     */
    @Test
    public void verifyRedeemCode5510ParalelRedeemLock() throws Exception {

        //the property must be false for this specific test
        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        try {
            JoinClubResponse newMember = createMember();

            Asset asset = AssignAsset.assignAssetToMember("gift", "None", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());
            String assetKey = asset.getKey();
            log4j.info("assetKey : " + assetKey);
            Key dataStoreAssetKey = StringToKey(assetKey);
            checkAssetStateInDB(dataStoreAssetKey);

            //Redeem first time and verify the error response for the second redeem request
            Map<String, String> redeemResponse = redeemAsset(assetKey);
            Assert.assertEquals("Verify the exception is as expected: ", 200, redeemResponse.get(RESULT));
            redeemResponse = redeemAsset(assetKey);
            Assert.assertEquals("Verify the exception is as expected: ", RedeemErrorCodesMap.ASSET_IS_LOCKED_FOR_REDEMPTION, redeemResponse.get(ERROR_CODE));
            Assert.assertEquals("Verify the exception is as expected: ", "N", redeemResponse.get(RESULT));
            Assert.assertEquals("Verify the exception is as expected: ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.ASSET_IS_LOCKED_FOR_REDEMPTION), redeemResponse.get(ERROR_DISPLAY_STRING));
            Assert.assertEquals("Verify the exception is as expected: ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.ASSET_IS_LOCKED_FOR_REDEMPTION), redeemResponse.get(ERROR_MESSAGE));
        }finally {
            //make sure the property is set as true for rest of the tests
            serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),true);
        }


    }


    @Test
    public void verifyRedeemCode5509AssetNotRedeemableAtThisLocation() throws IOException, InterruptedException {

        //Asset key for existing asset at other location
        Map<String,String> redeemResponse = redeemAsset(assetNotRedeemableAtThisLocation);

        Assert.assertEquals("Verify error code is as expected: ",RedeemErrorCodesMap.ASEET_NOT_REDEEMABLE_AT_LOCATION, redeemResponse.get(ERROR_CODE));
        Assert.assertEquals("Verify the result is as expected: ","N", redeemResponse.get(RESULT));
        Assert.assertEquals("Verify error display message is as expected: ","Asset not redeemable at this location", redeemResponse.get(ERROR_DISPLAY_STRING));
        Assert.assertEquals("Verify error message is as expected: ","Asset not redeemable at this location", redeemResponse.get(ERROR_MESSAGE));

    }


    @Test
    public void verifyRedeemCode5511AssetDeactivate() throws Exception {

        JoinClubResponse newMember = createMember();

        //create and deactivate asset
        Asset asset = AssignAsset.assignAssetToMember("gift", "None", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();
        serverHelper.deactivateAsset(assetKey);
       
        //Verify with DataStore
        Key dataStoreAssetKey = StringToKey(assetKey);
        checkAssetStateInDB(dataStoreAssetKey);

        //Redeem deactivated asset
        Map<String,String> redeemResponse = redeemAsset(assetKey);
        Assert.assertEquals("Verify the exception is as expected: ",RedeemErrorCodesMap.DEACTIVATE_ASSET, redeemResponse.get(ERROR_CODE));
        Assert.assertEquals("Verify the exception is as expected: ","N", redeemResponse.get(RESULT));
        Assert.assertEquals("Verify the exception is as expected: ","Deactivated asset", redeemResponse.get(ERROR_DISPLAY_STRING));
        Assert.assertEquals("Verify the exception is as expected: ","Deactivated asset", redeemResponse.get(ERROR_MESSAGE));

    }

    @Test
    public void verifyRedeemCode5501AssetAlreadyRedeemed() throws IOException, InterruptedException {

        //Redeem the asset
        String assetKey = redeemFromPOS();
        Thread.sleep(10000);
        //Redeem first time and verify the error response for the second redeem request
        Map<String,String> redeemResponse = redeemAsset(assetKey);
        Assert.assertEquals("Verify the exception is as expected: ",RedeemErrorCodesMap.ASSET_ALREADY_REDEEMED, redeemResponse.get(ERROR_CODE));
        Assert.assertEquals("Verify the exception is as expected: ","N", redeemResponse.get(RESULT));
        Assert.assertEquals("Verify the exception is as expected: ",RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.ASSET_ALREADY_REDEEMED), redeemResponse.get(ERROR_DISPLAY_STRING));
        Assert.assertEquals("Verify the exception is as expected: ",RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.ASSET_ALREADY_REDEEMED), redeemResponse.get(ERROR_MESSAGE));

    }

    @Test
    public void verifyRedeemCode5523DueDate() throws Exception {

        //Assign asset to member
        JoinClubResponse newMember = createMember();

        AssetDateCondition invalidDatesForAsset = serverHelper.getInvalidDatesForAsset();
        Asset asset = AssignAsset.assignAssetToMember("gift", "Bulk", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber(), invalidDatesForAsset);
        String assetKey = asset.getKey();

        //Verify with DataStore
        Key dataStoreAssetKey = StringToKey(assetKey);
        checkAssetStateInDB(dataStoreAssetKey);

        //Redeem first time and verify the error response for the second redeem request
        Map<String,String> redeemResponse = redeemAssetNoCustomer(assetKey);
        Assert.assertEquals("Verify the exception is as expected: ",RedeemErrorCodesMap.VIOLATION_OF_ASSET_NO_BENEFITS, redeemResponse.get(ERROR_CODE));
        Assert.assertEquals("Verify the exception is as expected: ","N", redeemResponse.get(RESULT));
        Assert.assertEquals("Verify the exception is as expected: ",RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION), redeemResponse.get(ERROR_DISPLAY_STRING));
        Assert.assertEquals("Verify the exception is as expected: ",RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION), redeemResponse.get(ERROR_MESSAGE));

    }

    @Ignore
    public void verifyRedeemCode5513DueDate() throws Exception {

        JoinClubResponse newMember = createMember();

        //Assign asset to member
        AssetDateCondition invalidDatesForAsset = getInvalidDatesForAsset();
        Asset asset = AssignAsset.assignAssetToMember("gift", "Bulk", newMember.getMembershipKey(), apiKey, locationId, serverToken, "1499106430813", invalidDatesForAsset);
        String assetKey = asset.getKey();

        //Verify with DataStore
        Key dataStoreAssetKey = StringToKey(assetKey);
        checkAssetStateInDB(dataStoreAssetKey);

        //Redeem first time and verify the error response for the second redeem request
        Map<String,String> redeemResponse = redeemAsset(assetKey);
        Assert.assertEquals("Verify the exception is as expected: ",RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION, redeemResponse.get(ERROR_CODE));
        Assert.assertEquals("Verify the exception is as expected: ","N", redeemResponse.get(RESULT));
        Assert.assertEquals("Verify the exception is as expected: ",RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION), redeemResponse.get(ERROR_DISPLAY_STRING));
        Assert.assertEquals("Verify the exception is as expected: ",RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION), redeemResponse.get(ERROR_MESSAGE));

    }

    @Test
    public void verifyRedeemCode6001RedeemCodeNotFound() throws IOException, InterruptedException {

        serverHelper.saveApiClient("DetailedRedeemResponse", false);
        //Asset key for existing asset at other location
        Map<String,String> redeemResponse = redeemAsset(assetNotRedeemableAtThisLocation);

        Assert.assertEquals("Verify error code is as expected: ",RedeemErrorCodesMap.REDEEM_CODE_NOT_FOUND, redeemResponse.get(ERROR_CODE));
        Assert.assertEquals("Verify the result is as expected: ","N", redeemResponse.get(RESULT));
        Assert.assertEquals("Verify error display message is as expected: ",RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.REDEEM_CODE_NOT_FOUND), redeemResponse.get(ERROR_DISPLAY_STRING));
        Assert.assertEquals("Verify error message is as expected: ",RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.REDEEM_CODE_NOT_FOUND), redeemResponse.get(ERROR_MESSAGE));
        serverHelper.saveApiClient("DetailedRedeemResponse", true);

    }


    @Test
    public void verifyRedeemCode5505AssetNotAssigned() throws IOException, InterruptedException {

        //Assign asset to member
        JoinClubResponse newMember = createMember();

        NewAssetResponse newAsset = serverHelper.buildAsset("gift","Bulk",1,locationId,serverToken,null);
        String assetKey = newAsset.getKey();

        Map<String,String> redeemResponse = redeemAsset(assetKey,newMember.getPhoneNumber());

        Assert.assertEquals("Verify error code is as expected: ",RedeemErrorCodesMap.ASSET_NOT_READY, redeemResponse.get(ERROR_CODE));
        Assert.assertEquals("Verify the result is as expected: ","N", redeemResponse.get(RESULT));
        Assert.assertEquals("Verify error display message is as expected: ",RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.ASSET_NOT_READY), redeemResponse.get(ERROR_DISPLAY_STRING));
        Assert.assertEquals("Verify error message is as expected: ",RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.ASSET_NOT_READY), redeemResponse.get(ERROR_MESSAGE));

    }


     //@Test
    //TODO check the redeem from client response
    public void verifyRedeemCode5520CodeNotFound() throws IOException, InterruptedException {

        //Create new  member
        JoinClubResponse newMember = createMember();

        //Assign asset to member
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "Auto", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        Key dataStoreAssetKey = StringToKey(newAsset1.getKey());

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        //Perform redeem asset (client)
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(newAsset1.getKey());
        redeemAsset.setMembershipKey(newMember.getMembershipKey());
        redeemAsset.setToken(serverToken);
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse)redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);

        //Get the redeemAsset response
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending", result2.getProperty("RedeemState"));

        //Perform client redeem (admin mode)
        ClientRedeem clientRedeem = new ClientRedeem();
        clientRedeem.setLocationId(locationId);
        clientRedeem.setRedeemCode("someWrongCode");
        IServerResponse response = clientRedeem.SendRequestByToken(serverToken, ClientRedeemResponse.class);
        if(!(response instanceof ZappServerErrorResponse)){
            Assert.fail("Expected an error!");
        }
        ZappServerErrorResponse errorResponse = (ZappServerErrorResponse) response;
        Assert.assertNotNull(errorResponse);
        Assert.assertEquals("Error message is: 'Code not found'", "Code not found",errorResponse.getRawObject().get("Error"));
        Assert.assertEquals("Result must be N", "N", errorResponse.getResult());
        Assert.assertEquals("ErrorCode is 5520", "5520",errorResponse.getErrorCode());
        Assert.assertEquals("Verify error display message is as expected: ","Code not found", errorResponse.getErrorMessage());
        Assert.assertEquals("Verify error message is as expected: ","Code not found", errorResponse.getErrorDisplayString());

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),true);
    }



    public void checkAssetStateInDB (Key dataStoreAssetKey) throws Exception{
        Thread.sleep(10000);
        //Verify with DataStore
        log4j.info("assetKey : " + dataStoreAssetKey.getId());
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned", "Assigned", result.getProperty(STATUS));
        Assert.assertEquals("Asset RedeemState must be Unused", "Unused", result.getProperty(REDEEM_STATE));
    }



    public String redeemFromPOS() throws IOException, InterruptedException {

        //Assign asset to member
        JoinClubResponse newMember = createMember();

        //serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(), true);
        Asset asset = AssignAsset.assignAssetToMember("gift", "Auto",newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();
        Key dataStoreAssetKey = StringToKey(assetKey);

        //Redeem asset
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(assetKey);
        redeemAsset.setMembershipKey(newMember.getMembershipKey());
        redeemAsset.setToken(serverToken);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) response;
        Assert.assertFalse("Response code must not be empty!", StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y", "Y", redeemAssetResponse.getResult());

        //Send redeem request in order to check redeem status
        RedeemItem redeemItem = new RedeemItem();
        String code = redeemAssetResponse.getCode().toString();
        redeemItem.setRedeemCode(code);

        Redeem redeem = new Redeem();
        redeem.setReturnExtendedItems(true);
        redeem.getRedeemItems().add(redeemItem);
        redeem.setMarkAsUsed(true);
        //Send the redeem request
        IServerResponse response2 = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (response2 instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response2).getErrorMessage());
        }

        Thread.sleep(3000);
        //Verify with DataStore
        Entity result3 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null", result3);
        Assert.assertEquals("Asset Status must be Redeemed", "Redeemed", result3.getProperty(STATUS));
        Assert.assertEquals("Asset RedeemState must be Burned", "Burned", result3.getProperty("RedeemState"));

        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action",Query.FilterOperator.EQUAL, "redeem"),
                new Query.FilterPredicate("AssetKey",Query.FilterOperator.EQUAL, dataStoreAssetKey))));

        List<Entity> resultUserAction = dataStore.findByQuery(qUserAction);
        Assert.assertEquals("Asset Source must be POS", "POS", resultUserAction.get(0).getProperty("Source"));

        return assetKey;
    }


    public Map redeemAsset(String assetKey) throws IOException, InterruptedException {


        Customer customer = new Customer();
        customer.setPhoneNumber("1499105194206");
        return redeemRequest(assetKey, customer);


    }

    public Map redeemAsset(String assetKey,String phoneNumber) throws IOException, InterruptedException {


        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        return redeemRequest(assetKey, customer);


    }



    public Map redeemAssetNoCustomer(String assetKey) throws IOException, InterruptedException {

        Redeem redeem = new Redeem();
        redeem.setTransactionId(UUID.randomUUID().toString());
        RedeemItem redeemItemPurchase = new RedeemItem();
        redeemItemPurchase.setAssetKey(assetKey);

        ArrayList<RedeemItem> redeemItemForPOS = new ArrayList<>();
        redeemItemForPOS.add(redeemItemPurchase);
        redeem.setRedeemItems(redeemItemForPOS);

        // Send the request
        IServerResponse response = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        HashMap map = new HashMap <String, String>();

        //Make sure the response is not Error response
        varifyError(response, map);
        return map;
    }

    private Map redeemRequest(String assetKey, Customer customer) {
        Redeem redeem = new Redeem();
        redeem.getCustomers().add(customer);
        redeem.setTransactionId(UUID.randomUUID().toString());

        RedeemItem redeemItemPurchase = new RedeemItem();
        redeemItemPurchase.setAssetKey(assetKey);

        ArrayList<RedeemItem> redeemItemForPOS = new ArrayList<>();
        redeemItemForPOS.add(redeemItemPurchase);
        redeem.setRedeemItems(redeemItemForPOS);

        // Send the request
        IServerResponse response = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);
        HashMap map = new HashMap <String, String>();

        //Make sure the response is not Error response
        varifyError(response, map);
        return map;
    }

    private void varifyError(IServerResponse response, HashMap map) {
        if (response instanceof ZappServerErrorResponse) {

            map.put(ERROR_CODE,((ZappServerErrorResponse) response).getErrorCode());
            map.put(RESULT,((ZappServerErrorResponse) response).getResult());
            map.put(ERROR_DISPLAY_STRING,((ZappServerErrorResponse) response).getErrorDisplayString());
            map.put(ERROR_MESSAGE,((ZappServerErrorResponse) response).getErrorMessage());

        }
        else {
            map.put(RESULT,response.getStatusCode());
        }
    }

    public AssetDateCondition getInValidFromDatesForAsset(){
        LocalDateTime from = LocalDateTime.now();
        Date fromCondition = Date.from(from.minusDays(10).toInstant(ZoneOffset.UTC));
        Date toCondition = Date.from(from.plusDays(1).toInstant(ZoneOffset.UTC));
        return new AssetDateCondition(fromCondition, toCondition);
    }


    public AssetDateCondition getInvalidDatesForAsset(){
        LocalDateTime from = LocalDateTime.now();
        Date fromCondition = Date.from(from.minusDays(100).toInstant(ZoneOffset.UTC));
        Date toCondition = Date.from(from.minusDays(1000).toInstant(ZoneOffset.UTC));
        return new AssetDateCondition(fromCondition, toCondition);
    }

}





