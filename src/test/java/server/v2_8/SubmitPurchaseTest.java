package server.v2_8;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import com.google.appengine.repackaged.com.google.gson.internal.LinkedTreeMap;
import hub.base.categories.serverCategories.ServerRegression;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.base.BasePurchaseTest;
import server.common.AnonymousPurchaseTypes;
import server.common.ApiClientBuilder;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.common.asset.AssignAsset;
import server.utils.Initializer;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.*;
import utils.JsonUtils;
import utils.KeyUtils;

import java.awt.*;
import java.beans.IntrospectionException;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class SubmitPurchaseTest extends BasePurchaseTest {


    /**
     * C2922 - Submit Purchase - NoIdentifiers
     **/
    @Test
    public void testSubmitPurchaseNoIdentifiers() throws Exception{

        //SaveAnonymousPurchaseOn
        serverHelper.saveApiClient(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(),AnonymousPurchaseTypes.ALWAYS.type());

        // Initialize a new SubmitPurchase
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setBranchId("1");
		submitPurchase.setPosId("10");
		submitPurchase.setTotalSum("1000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        Item item = new Item ();

        item.setItemCode("11223344");
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName("Prod1");
        item.setDepartmentCode("123");
        item.setDepartmentName("Dep1");
        item.setPrice(1000);

        item.getTags().add("DEP-1");
        item.getTags().add("tag 2");

        submitPurchase.getItems().add(item);

        //Send the request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        //Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        serverHelper.validatePurchaseResponse((SubmitPurchaseResponse) response,submitPurchase.getTotalSum(),"1");

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity>result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: "+ submitPurchase.getTransactionId(), q, 1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        Query qAction = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: "+ submitPurchase.getTransactionId(), qAction, 1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));

        /*//Response time must be under 5000
        Assert.assertTrue("Response time must be under 5000, time was: " + submitPurchaseResponse.getTime(), submitPurchaseResponse.getTime() <= 5000 );*/

    }

    /**
     * C3776 - Submit Purchase and AllowMultiplePurchasesForPOSIdentifiers= no
     **/
    @Test
    public void testSubmitPurchaseAndAllowMultiplePurchasesForPOSIdentifiersNo() throws Exception {

        //AllowMultiplePurchasesForPOSIdentifiers = No
        serverHelper.saveApiClient(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),false);

        //Assert that AllowMultiplePurchasesForPOSIdentifiers = no in the dataStore

        List<Entity> result = serverHelper.getBusinessFromApiClient(dataStore);
        Assert.assertEquals("AllowMultiplePurchasesForPOSIdentifiers is False", false, result.get(0).getProperty(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key()));

        JoinClubResponse newMember = serverHelper.createMember();

        //Initialize a new SubmitPurchase
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.getCustomers().add(customer);
        submitPurchase.setBranchId("2");
        submitPurchase.setPosId("12");
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        Item item = new Item ();

        item.setItemCode("1155");
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName("Prod1");
        item.setDepartmentCode("123");
        item.setDepartmentName("Dep1");
        item.setPrice(2000);

        item.getTags().add("DEP-1");
        item.getTags().add("tag 2");

        submitPurchase.getItems().add(item);

        //Send the request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        //Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;

        serverHelper.validatePurchaseResponse((SubmitPurchaseResponse) response,submitPurchase.getTotalSum(),"1");

        /*
        //Response time must be under 5000
        Assert.assertTrue("Response time must be under 5000, time was: " + submitPurchaseResponse.getTime(), submitPurchaseResponse.getTime() <= 5000 );
        */

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        serverHelper.queryWithWait("Must find one Purchase with the transaction ID: "+ submitPurchase.getTransactionId(), q, 1, dataStore);

        Query qAction = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: "+ submitPurchase.getTransactionId(), qAction,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));

        //Send the request again and make sure the purchase is failed
        IServerResponse responseFailed = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is failed response
        Assert.assertTrue("Response must be an error", responseFailed instanceof ZappServerErrorResponse);
        Assert.assertEquals("Must show error message: ", "Purchase with POS Identifiers already exists", ((ZappServerErrorResponse)responseFailed).getErrorMessage());

    }

    /**
     * C3777 - Submit Purchase with more than 1 customer
     **/
    @Test
    public void testSubmitPurchaseMoreThan1Customer() throws IOException, InterruptedException {

        JoinClubResponse newMember = serverHelper.createMember();
        JoinClubResponse newMember2 = serverHelper.createMember();

        //Initialize a new SubmitPurchase
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        Customer customer2 = new Customer();
        customer2.setPhoneNumber(newMember2.getPhoneNumber());
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.getCustomers().add(customer);
        submitPurchase.getCustomers().add(customer2);
        submitPurchase.setBranchId("1");
        submitPurchase.setPosId("10");
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        Item item = new Item();

        item.setItemCode("11223344");
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName("Prod1");
        item.setDepartmentCode("123");
        item.setDepartmentName("Dep1");
        item.setPrice(1000);

        item.getTags().add("DEP-1");
        item.getTags().add("tag 2");

        submitPurchase.getItems().add(item);
        submitPurchase.getItems().add(item);

        //Send the request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        serverHelper.validatePurchaseResponse((SubmitPurchaseResponse) response,submitPurchase.getTotalSum(),"2");


        //Response time must be under 5000
        //Assert.assertTrue("Response time must be under 5000, time was: " + submitPurchaseResponse.getTime(), submitPurchaseResponse.getTime() <= 5000 );

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find two UserAction with the transaction ID: " + submitPurchase.getTransactionId(),q, 2, dataStore);
        Assert.assertTrue("UID must be in the first user action ", ((Text)result.get(0).getProperty("Data")).getValue().contains(submitPurchaseResponse.getConfirmation()));
        Assert.assertTrue("UID must be in the second user action ", ((Text)result.get(1).getProperty("Data")).getValue().contains(submitPurchaseResponse.getConfirmation()));
    }

    /**
     * C4309 - Anonymous purchase with Save anonymous purchase= never and AllowMultiplePurchasesForPOSIdentifiers=yes
     **/

    @Test
    public void testAnonymousPurchaseEqualsToNever() throws Exception {
        /*
        Steps:
            1. change the flag SaveAnonymousPurchaseOn = Never
            2. send submit with no customers
            3. validate that server block the anonymous purchase No club members identifiers found
         */

        //SaveAnonymousPurchaseOn
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(), true));


        //Initialize a new SubmitPurchase
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setBranchId("1");
        submitPurchase.setPosId("10");
        submitPurchase.setTotalSum("1000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        Item item = new Item();

        item.setItemCode("11223344");
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName("Prod1");
        item.setDepartmentCode("123");
        item.setDepartmentName("Dep1");
        item.setPrice(1000);

        item.getTags().add("DEP-1");
        item.getTags().add("tag 2");

        submitPurchase.getItems().add(item);

        //Send the request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        Assert.assertTrue("Response must be an error", response instanceof ZappServerErrorResponse);
        Assert.assertEquals("No club members identifiers found", ((ZappServerErrorResponse) response).getErrorMessage());

    }

    /**
     * C4312 - Anonymous purchase with Save anonymous purchase= never and AllowMultiplePurchasesForPOSIdentifiers=no
     **/

    @Test
    public void testAnonymousPurchaseEqualsToNeverMultiplePurchasesNo() throws Exception {
        /*
        Steps:
            1. change the flag SaveAnonymousPurchaseOn = Never
            2. send submit with no customers
            3. validate that server block the anonymous purchase No club members identifiers found
            4. change the flag AllowMultiplePurchasesForPOSIdentifiers = true and verify if we get 200 ok for purchase request
         */

        JoinClubResponse newMember = serverHelper.createMember();

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(), false));


        //Initialize a new SubmitPurchase
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setBranchId("1");
        submitPurchase.setPosId("10");
        submitPurchase.setTotalSum("1000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        Item item = new Item();

        item.setItemCode("11223344");
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName("Prod1");
        item.setDepartmentCode("123");
        item.setDepartmentName("Dep1");
        item.setPrice(1000);

        item.getTags().add("DEP-1");
        item.getTags().add("tag 2");

        submitPurchase.getItems().add(item);

        //Send the request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is not Error response
        Assert.assertTrue("Response must be an error", response instanceof ZappServerErrorResponse);
        Assert.assertEquals("No club members identifiers found", ((ZappServerErrorResponse) response).getErrorMessage());

        //Add customer to the purchase context
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        submitPurchase.getCustomers().add(customer);

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NO_IDENTIFIER.type()),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(), true));

        //Send the submit purchase request
        IServerResponse response2 = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is not Error response
        if (response2 instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response2).getErrorMessage());
        }

        //Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response2;

        serverHelper.validatePurchaseResponse(submitPurchaseResponse,submitPurchase.getTotalSum(),"1");


        //Verify Purchase with DataStore ---- second submit purchase -----
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(),q,1,dataStore);
    }



    /**
     * C4310 - Anonymous purchase with Save anonymous purchase= always and AllowMultiplePurchasesForPOSIdentifiers= yes
     **/

    @Test
    @Category({ServerRegression.class})
    public void testAnonymousPurchaseEqualsToAlwaysMultiplePurchasesYes() throws Exception {
        /*
        Steps:
            1. change the flag SaveAnonymousPurchaseOn = Always & ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key() = "true"
            2. send submit with no customers twice
            3. verify if we get 200 ok in the second submit purchase
            4. validate that data is correct in the data store (UserAction,Purchase)
         */

        //SaveAnonymousPurchaseOn

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()),
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(), true));


        //Initialize a new SubmitPurchase
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setBranchId("1");
        submitPurchase.setPosId("10");
        submitPurchase.setTotalSum("1000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        Item item = new Item();

        item.setItemCode("11223344");
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName("Prod1");
        item.setDepartmentCode("123");
        item.setDepartmentName("Dep1");
        item.setPrice(1000);

        item.getTags().add("DEP-1");
        item.getTags().add("tag 2");

        submitPurchase.getItems().add(item);

        //Send the first request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        serverHelper.validatePurchaseResponse((SubmitPurchaseResponse) response,submitPurchase.getTotalSum(),"1");


        //Verify Purchase with DataStore ---- first submit purchase -----
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(),q, 1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(),q, 1, dataStore);
        Assert.assertTrue("UID must be inside", ((Text)result.get(0).getProperty("Data")).getValue().contains(submitPurchaseResponse.getConfirmation()));

        //Send the second request
        IServerResponse response2 = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);


        //Make sure the response is not Error response
        if (response2 instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response2).getErrorMessage());
        }

        //Assert SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse2 = (SubmitPurchaseResponse) response2;
        serverHelper.validatePurchaseResponse(submitPurchaseResponse2,submitPurchase.getTotalSum(),"1");


        //Verify Purchase with DataStore ---- second submit purchase -----
        q = new Query("Purchase");
        ArrayList<Query.Filter> filters = new ArrayList<>();
        filters.add(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        filters.add(new Query.FilterPredicate("BusinessID",Query.FilterOperator.EQUAL, locationId));
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, filters));
        List<Entity> result2 = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(),q,2,dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result2.get(0));
        serverHelper.validatePurchaseResult(submitPurchase, result2.get(1));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        List<Entity> resultUserAction = serverHelper.queryWithWait("Must find two UserAction with the transaction ID: " + submitPurchase.getTransactionId(), q,2, dataStore);

        boolean found = false;
        for (int i = 0; i < resultUserAction.size() ;i++){

            String value = ((Text)resultUserAction.get(i).getProperty("Data")).getValue();
            if(value.contains(submitPurchaseResponse2.getConfirmation())) {
                found = true;
                break;
            }

        }
        Assert.assertTrue("Confirmation must be inside", found);
    }

    /**
     * C4313 - Anonymous purchase with Save anonymous purchase= always and AllowMultiplePurchasesForPOSIdentifiers= no
     **/

    @Test
    public void testAnonymousPurchaseEqualsToAlwaysMultiplePurchasesNo() throws Exception {

        /*
        Steps:
        1. change the flag SaveAnonymousPurchaseOn = Always & "AllowMultiplePurchasesForPOSIdentifiers" = "false"
        2. send submit with no customers twice
        3. verify if we get error message for the second request
        4. validate that data correct in the data store (UserAction,Purchase)
         */

        //SaveAnonymousPurchaseOn
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()),
            ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(), false));


        //Initialize a new SubmitPurchase
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setBranchId("1");
        submitPurchase.setPosId("10");
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        Item item = new Item();

        item.setItemCode("11223344");
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName("Prod1");
        item.setDepartmentCode("123");
        item.setDepartmentName("Dep1");
        item.setPrice(1000);

        item.getTags().add("DEP-1");
        item.getTags().add("tag 2");

        submitPurchase.getItems().add(item);
        submitPurchase.getItems().add(item);

        //Send the first request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        serverHelper.validatePurchaseResponse((SubmitPurchaseResponse) response,submitPurchase.getTotalSum(),"2");


        /*//Response time must be under 5000
        Assert.assertTrue("Response time must be under 5000, time was: " + submitPurchaseResponse.getTime(), submitPurchaseResponse.getTime() <= 5000 );*/

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));

        //Send the second request
        IServerResponse response2 = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Verify if the response contains Error response
        Assert.assertTrue("Response must be an error", response2 instanceof ZappServerErrorResponse);
        Assert.assertEquals("Purchase with POS Identifiers already exists", ((ZappServerErrorResponse)response2).getErrorMessage());
        Assert.assertEquals("Business settings does not permit saving of multiple purchases for POS identifiers", ((ZappServerErrorResponse)response2).getErrorDisplayString());
    }

    /**
     * C4311 - Anonymous purchase with Save anonymous purchase= NoIdentifier and  AllowMultiplePurchasesForPOSIdentifiers= yes
     **/

    @Test
    public void testAnonymousPurchaseEqualsToNoIdentifierMultiplePurchasesYes() throws Exception {

        /*
        Steps:
        1. change the flag SaveAnonymousPurchaseOn = NoIdentifier & "AllowMultiplePurchasesForPOSIdentifiers" = "true"
        2. send submit with no customers twice
        3. verify if we get 200 ok for the second purchase request
        4. validate that data is correct in the data store (UserAction,Purchase)
         */
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NO_IDENTIFIER.type()),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(), true));


        //Initialize a new SubmitPurchase
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setBranchId("1");
        submitPurchase.setPosId("10");
        submitPurchase.setTotalSum("1000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        Item item = new Item();

        item.setItemCode("11223344");
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName("Prod1");
        item.setDepartmentCode("123");
        item.setDepartmentName("Dep1");
        item.setPrice(1000);

        item.getTags().add("DEP-1");
        item.getTags().add("tag 2");

        submitPurchase.getItems().add(item);

        //Send the first request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        serverHelper.validatePurchaseResponse((SubmitPurchaseResponse) response,submitPurchase.getTotalSum(),"1");


        /*//Response time must be under 5000
        Assert.assertTrue("Response time must be under 5000, time was: " + submitPurchaseResponse.getTime(), submitPurchaseResponse.getTime() <= 5000 );*/

        //Verify Purchase with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        //Verify UserAction with DataStore
        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(),q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));

        //Send the second request again in order to verify the AllowMultiplePurchasesForPOSIdentifiers = true
        IServerResponse response2 = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        /*//Response time must be under 5000
        Assert.assertTrue("Response time must be under 5000, time was: " + submitPurchaseResponse.getTime(), submitPurchaseResponse.getTime() <= 5000 );*/

        //Make sure the response is not Error response
        if (response2 instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response2).getErrorMessage());
        }
        SubmitPurchaseResponse submitPurchaseResponse2 = (SubmitPurchaseResponse)response;
        serverHelper.validatePurchaseResponse(submitPurchaseResponse2,submitPurchase.getTotalSum(),"1");


        /*//Response time must be under 5000
        Assert.assertTrue("Response time must be under 5000, time was: " + submitPurchaseResponse.getTime(), submitPurchaseResponse.getTime() <= 5000 );*/

        //Verify with DataStore
        q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result2 = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(),q,2, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result2.get(0));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        List<Entity> resultUserAction = serverHelper.queryWithWait("Must find two UserAction with the transaction ID: " + submitPurchase.getTransactionId(),q,2, dataStore);

        boolean found = false;
        for (int i = 0; i < resultUserAction.size() ;i++){

            String value = ((Text)resultUserAction.get(i).getProperty("Data")).getValue();
            if(value.contains(submitPurchaseResponse.getConfirmation())) {
                found = true;
                break;
            }

        }
        Assert.assertTrue("Confirmation must be inside", found);
    }

    /**
     * C4314 - Anonymous purchase with Save anonymous purchase= NoIdentifier and AllowMultiplePurchasesForPOSIdentifiers= no
     **/

    @Test
    public void testAnonymousPurchaseEqualsToNoIdentifierMultiplePurchasesNo() throws Exception {

        /*
        Steps:
        1. change the flag SaveAnonymousPurchaseOn = NoIdentifier & "AllowMultiplePurchasesForPOSIdentifiers" = "false"
        2. send submit with no customers twice
        3. verify if we get error message for the second request
        4. validate that data correct in the data store (UserAction,Purchase)
         */

        //SaveAnonymousPurchaseOn
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NO_IDENTIFIER.type()),
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(), false));


        //Initialize a new SubmitPurchase
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setBranchId("1");
        submitPurchase.setPosId("10");
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        Item item = new Item();

        item.setItemCode("11223344");
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName("Prod1");
        item.setDepartmentCode("123");
        item.setDepartmentName("Dep1");
        item.setPrice(1000);

        item.getTags().add("DEP-1");
        item.getTags().add("tag 2");

        submitPurchase.getItems().add(item);
        submitPurchase.getItems().add(item);

        //Send the first request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        serverHelper.validatePurchaseResponse(submitPurchaseResponse,submitPurchase.getTotalSum(),"2");


        /*//Response time must be under 5000
        Assert.assertTrue("Response time must be under 5000, time was: " + submitPurchaseResponse.getTime(), submitPurchaseResponse.getTime() <= 5000 );*/

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(),q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(),q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));

        //Send the second request
        IServerResponse response2 = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is not Error response
        Assert.assertTrue("Response must be an error", response2 instanceof ZappServerErrorResponse);
        Assert.assertEquals("Purchase with POS Identifiers already exists", ((ZappServerErrorResponse)response2).getErrorMessage()) ;
        Assert.assertEquals("Business settings does not permit saving of multiple purchases for POS identifiers", ((ZappServerErrorResponse)response2).getErrorDisplayString());

    }

    /**
     * C4316 Submit Purchase and AllowMultiplePurchasesForPOSIdentifiers= Yes
     **/

    @Test
    public void submitPurchaseAndAllowMultiplePurchasesForPOSIdentifiersIsYes() throws Exception{

        //AllowMultiplePurchasesForPOSIdentifiers=Yes
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(), true),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()));


        //Initialize a new SubmitPurchase
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setBranchId("3");
        submitPurchase.setPosId("13");
        submitPurchase.setTotalSum("4000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        Item item = new Item();
        item.setItemCode("4454");
        item.setQuantity(1);
        item.setAmount(1.24);
        item.setItemName("LiorItem1");
        item.setDepartmentCode("554");
        item.setItemName("Dep3");
        item.setPrice(4000);

        item.getTags().add("DEP-1");
        item.getTags().add("tag 2");

        submitPurchase.getItems().add(item);

        //Send the request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        //Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        serverHelper.validatePurchaseResponse(submitPurchaseResponse,submitPurchase.getTotalSum(),"1");

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result1 = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: "+ submitPurchase.getTransactionId(),q,1,dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result1.get(0));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        List<Entity> result2 = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: "+ submitPurchase.getTransactionId(),q,1,dataStore);
        Assert.assertTrue("UID must be inside", ((Text)result2.get(0).getProperty("Data")).getValue().contains(submitPurchaseResponse.getConfirmation()));

        //Send the request
        IServerResponse response2 = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is not Error response
        if(response2 instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response2).getErrorMessage());
        }

        // Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse2 = (SubmitPurchaseResponse) response2;
        serverHelper.validatePurchaseResponse(submitPurchaseResponse2,submitPurchase.getTotalSum(),"1");

        //Verify with DataStore
        q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity>  result3 = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: "+ submitPurchase.getTransactionId(),q,2,dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result3.get(0));
        serverHelper.validatePurchaseResult(submitPurchase, result3.get(1));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        List<Entity>  result4 = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: "+ submitPurchase.getTransactionId(),q, 2, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result4, submitPurchaseResponse.getConfirmation()));
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result4, submitPurchaseResponse2.getConfirmation()));

    }

    /**
     * C4340 Submit purchase with all POS integration field
     **/

    @Test
    @Category({ServerRegression.class})
    public void submitPurchaseWithAllPOSIntegrationField() throws InterruptedException, IOException, IllegalAccessException, AWTException, IntrospectionException {
        /*
            1. send submit with all POS fields
            2. validate in the data store if the  data saved correctly
         */

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift","None",memberKey,apiKey,locationId,serverToken,newMember.getPhoneNumber());
        String assetKey = asset.getKey();

        //Initialize a new SubmitPurchase
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.getCustomers().add(customer);
        submitPurchase.setBranchId("3");
        submitPurchase.setPosId("13");
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");
        submitPurchase.setTimeStamp(Long.valueOf(System.currentTimeMillis()));
        submitPurchase.setTotalGeneralDiscount(2000);
        submitPurchase.setOrderType("Delivery");
        submitPurchase.setTransactionSource("POS");

        RedeemItem redeemItem = new RedeemItem();
        redeemItem.setAssetKey(assetKey);

        Payment payment =  new Payment();
        payment.setType("credit_card");
        payment.setDetails("CreditPayment");
        payment.setSum("4000");

        ArrayList<Payment> payments = new ArrayList<>();
        payments.add(payment);


        Item item = new Item();
        item.setItemCode("4454");
        item.setQuantity(1);
        item.setAmount(1.24);
        item.setItemName("LiorItem1");
        item.setDepartmentCode("554");
        item.setItemName("Dep3");
        item.setPrice(4000);

        item.getTags().add("DEP-1");
        item.getTags().add("tag 2");

        submitPurchase.getItems().add(item);

        //Send the request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        //Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        Assert.assertEquals("TotalGeneralDiscount is equal", submitPurchase.getTotalGeneralDiscount(),result.get(0).getProperty("TotalGeneralDiscount"));
        Assert.assertEquals("OrderType is equal", submitPurchase.getOrderType(),result.get(0).getProperty("OrderType"));
        Assert.assertEquals("TransactionSource is equal", submitPurchase.getTransactionSource(),result.get(0).getProperty("TransactionSource"));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));

    }



    /**
     * C4219 Submit Purchase with "TransactionSourceName"
     **/

    @Test
    public void submitPurchaseWithTransactionSourceName() throws InterruptedException, IOException{

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        //Create new Submit purchase
        SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(newMember.getPhoneNumber());
        submitPurchase.setTotalGeneralDiscount(2000);
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionSource("nomore");

        //Send the request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        Assert.assertEquals("TransactionSource is equal", submitPurchase.getTransactionSource(),result.get(0).getProperty("TransactionSource"));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));
    }



    /**
     * C3810 Submit Purchase - ChainID, OrderType and TransactionSource with special characters
     **/

    @Test
    public void submitPurchaseWithSpecialCharacters() throws InterruptedException, IOException{

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();

        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        //Create new Submit purchase
        SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(newMember.getPhoneNumber());
        ArrayList<String> tags = new ArrayList<>();
        tags.add("jenny test SOAPUI !@#$%^&*()}{|?.,';~1234567890");
        submitPurchase.setTags(tags);
        submitPurchase.setOrderType("Eating at the Restaraunt !@#$%^&*()}{|?.,';~1234567890");
        submitPurchase.setTransactionSource("POS1234 !@#$%^&*()}{|?.,';~1234567890");
        submitPurchase.setChainId("soapUI !@#$%^&*()}{|?.,';~1234567890");

        //Send the request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        Assert.assertEquals("TransactionSource is equal", submitPurchase.getTransactionSource(),result.get(0).getProperty("TransactionSource"));
        Assert.assertEquals("OrderType is equal", submitPurchase.getOrderType(),result.get(0).getProperty("OrderType"));
        Object tagsResponse = result.get(0).getProperty("Tags");
        Assert.assertNotNull("Tags should not be null", tagsResponse);
        Assert.assertTrue("tags response should be Text object ", tagsResponse instanceof Text);
        Assert.assertTrue("Tags should be equal", ((Text)tagsResponse).getValue().contains(tags.get(0)));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));
    }

    /**
     * C3811 Submit Purchase - ChainID, OrderType and TransactionSource with long values
     **/

    @Test
    public void submitPurchaseWithLongValues() throws InterruptedException, IOException{

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        //Create new Submit purchase
        SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(newMember.getPhoneNumber());
        submitPurchase.setOrderType("Eating at the Restaraunt long !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!ffffffffffffffffffffffffffffffffffffffffffffffertertyrterterterttttrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad");
        submitPurchase.setTransactionSource("POS1234 ggggggggggggggggggggggggggggggggffffffffffffffffffffffffdddddddddddddddddddddddddddddddddddddddddddddddddddddgfggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggffffffffff gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad");
        submitPurchase.setChainId("soapUI gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsada gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad gggggggggggggggggggggggggggggggggggggjfhkjghjhhhhhhhhhhhhhhhhhhhhhhhhhhhhhsadsad");

        //Send the request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        Assert.assertEquals("TransactionSource should be equal", submitPurchase.getTransactionSource(), result.get(0).getProperty("TransactionSource"));
        Assert.assertEquals("OrderType should be equal", submitPurchase.getOrderType(), result.get(0).getProperty("OrderType"));
        Assert.assertEquals("ChainId should be equal", submitPurchase.getChainId(), result.get(0).getProperty("ChainID"));


        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));
    }


    /**
     * C4306 Get purchases with Status = Open
     *
     **/

    @Test
    public void getPurchasesWithStatusOpen() throws InterruptedException, IOException{


        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        //Create Submit purchase
        SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(newMember.getPhoneNumber());
        submitPurchase.setTotalGeneralDiscount(2000);
        submitPurchase.setTotalSum("2000");
        submitPurchase.setStatus("Open");

        //Send the request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        GetPurchase getPurchase = new GetPurchase(locationId, "Open");
        GetPurchaseResponse getPurchaseResponse = (GetPurchaseResponse) getPurchase.sendRequestByTokenAndValidateResponse(serverToken, GetPurchaseResponse.class);
        Assert.assertNotNull("GetPurchase should not return empty response", getPurchaseResponse);
        List<PurchaseResponse> data = getPurchaseResponse.getData();
        Assert.assertNotNull("GetPurchase should not return empty data", data);
        Assert.assertTrue("GetPurchase should return at least one purchase", data.size() > 0);
        final boolean[] purchaseFromThisTestFound = {false};
        data.forEach( purchase -> {
            Assert.assertEquals("Status for each purchase should be 'Open'", "Open", purchase.getStatus());
            Assert.assertNotNull("TransactionId should not be null", purchase.getTransactionId());
            if(purchase.getTransactionId().equals(submitPurchase.getTransactionId())){
                purchaseFromThisTestFound[0] = true;
            }

        });

        Assert.assertTrue("Purchase should be found in the list of purchases response", purchaseFromThisTestFound[0]);
    }


    /**
     * C4307 Get purchases with Status = Analyzed
     *
     **/

    @Test
    public void getPurchasesWithStatusAnalyzed() throws InterruptedException, IOException{


        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        //Create Submit purchase
        SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(newMember.getPhoneNumber());
        submitPurchase.setTotalGeneralDiscount(2000);
        submitPurchase.setTotalSum("2000");
        submitPurchase.setStatus("Analyzed");

        //Send the request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());
        Thread.sleep(3000);
        log4j.info("Checking if trnsaction: " + submitPurchase.getTransactionId() + " status is: " + submitPurchase.getStatus());

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()),
                new Query.FilterPredicate("Status", Query.FilterOperator.EQUAL, "Analyzed"))));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);


        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        GetPurchase getPurchase = new GetPurchase(locationId, "Analyzed");
        GetPurchaseResponse getPurchaseResponse = (GetPurchaseResponse) getPurchase.sendRequestByTokenAndValidateResponse(serverToken, GetPurchaseResponse.class);
        Assert.assertNotNull("GetPurchase should not return empty response", getPurchaseResponse);
        List<PurchaseResponse> data = getPurchaseResponse.getData();
        Assert.assertNotNull("GetPurchase should not return empty data", data);
        Assert.assertTrue("GetPurchase should return at least one purchase", data.size() > 0);
        final boolean[] purchaseFromThisTestFound = {false};
        log4j.info("Checking if trnsaction: " + submitPurchase.getTransactionId() + " appears in the list of purchases response");
        data.forEach( purchase -> {
            log4j.info("purchase TransactionId: " + purchase.getTransactionId());
            log4j.info("purchase Status: " + purchase.getStatus());
            Assert.assertEquals("Status for each purchase should be 'Analyzed'", "Analyzed", purchase.getStatus());
            Assert.assertNotNull("TransactionId should not be null", purchase.getTransactionId());
            if(purchase.getTransactionId().equals(submitPurchase.getTransactionId())){
                purchaseFromThisTestFound[0] = true;
            }

        });

        Assert.assertTrue("Purchase should be found in the list of purchases response", purchaseFromThisTestFound[0]);
    }


    /**
     * C4577 'Submit Purchase' with 2 members, both of them with the same members identifiers
     **/

    @Test
    @Category({ServerRegression.class})
    public void submitPurchaseWithSameMemberTwice() throws InterruptedException, IOException{

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        //Create new Submit purchase
        SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(newMember.getPhoneNumber());
        submitPurchase.setTotalGeneralDiscount(2000);
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionSource("nomore");

        //Add the same customer twice
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        submitPurchase.getCustomers().add(customer);

        //Send the request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        Assert.assertEquals("TransactionSource is equal", submitPurchase.getTransactionSource(),result.get(0).getProperty("TransactionSource"));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));
    }


    /**
     * C4578 'Submit Purchase' with 3 members: Two members with the same identifiers + one member with other identifier
     **/

    @Test
    public void submitPurchaseWithThreeMembersSameMemberTwice() throws InterruptedException, IOException{

        //Create 2 new customer
        JoinClubResponse newMember1 = serverHelper.createMember();
        Assert.assertNotNull(newMember1);
        Assert.assertNotNull(newMember1.getMembershipKey());
        JoinClubResponse newMember2 = serverHelper.createMember();
        Assert.assertNotNull(newMember2);
        Assert.assertNotNull(newMember2.getMembershipKey());

        //Create new Submit purchase
        SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(newMember1.getPhoneNumber());
        submitPurchase.setTotalGeneralDiscount(2000);
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionSource("nomore");
        List<String> newMembersToCheck = Lists.newArrayList(newMember1.getUserKey(), newMember2.getUserKey());

        //Add the same customer twice
        Customer customer1 = new Customer();
        customer1.setPhoneNumber(newMember1.getPhoneNumber());
        submitPurchase.getCustomers().add(customer1);
        Customer customer2 = new Customer();
        customer2.setPhoneNumber(newMember2.getPhoneNumber());
        submitPurchase.getCustomers().add(customer2);

        //Send the submit purchase request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        //Validate that number of members is 2
        Assert.assertEquals("TransactionSource is equal", submitPurchase.getTransactionSource(),result.get(0).getProperty("TransactionSource"));
        Assert.assertEquals("Number of members should be 2", Long.valueOf(2), result.get(0).getProperty("NumOfMembers"));
        Assert.assertNotNull("MembershipKeys should be 2", result.get(0).getProperty("MembershipKeys"));

        //Validate that there are 2 different user keys
        Assert.assertTrue("UserKeys should be array", result.get(0).getProperty("UserKeys") instanceof List);
        List userKeys = (List) result.get(0).getProperty("UserKeys");
        Assert.assertNotNull("UserKeys array should not be null", userKeys);
        Assert.assertEquals("UserKeys array size should be 2", 2, userKeys.size());
        Assert.assertTrue("Array should contain Key object", userKeys.get(0) instanceof Key);
        Assert.assertTrue("Array should contain Key object", userKeys.get(1) instanceof Key);
        Key userKey1 = (Key) userKeys.get(0);

        String userEncodedKey1 = KeyUtils.keyToString(userKey1);
        Key userKey2 = (Key) userKeys.get(1);
        String userEncodedKey2 =  KeyUtils.keyToString(userKey2);

        Assert.assertNotEquals("User Keys should be since there are 2 members", userEncodedKey1, userEncodedKey2);
        Assert.assertTrue("User keys should match created members" , newMembersToCheck.containsAll(Lists.newArrayList(userEncodedKey1, userEncodedKey2)));

        //Validate userAction data
        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(), q,2, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));
    }



    /**
     * C3814 Submit Purchase - with 10 items
     **/

    @Test
    public void submitPurchaseWith10Items() throws InterruptedException, IOException{

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        //Create new Submit purchase
        SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(newMember.getPhoneNumber());
        ArrayList<Item> items = new ArrayList<>();
        int sum= 0;
        for(int i = 1; i<= 10; i++){
            items.add(Initializer.buildPurchaseItem("Item code " + i,"Item name" + i,"Department" + i,"Department name" + i,i,i,Lists.newArrayList()));
            sum += i;
        }
        submitPurchase.setItems(items);
        submitPurchase.setTotalSum(String.valueOf(sum));

        //Send the request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));
        Assert.assertNotNull("Data should contain items data", result.get(0).getProperty("Data"));
        Assert.assertTrue("Data should be A text field", result.get(0).getProperty("Data") instanceof Text);
        Assert.assertNotNull("Data should contain purchase items", ((Text) result.get(0).getProperty("Data")).getValue());
        String dataAsString = ((Text) result.get(0).getProperty("Data")).getValue();

        //Validate all items appear correct as submitted in the submit-purchase
        String jsonData = dataAsString.substring(dataAsString.indexOf("{"));
        Map itemsMap = JsonUtils.convertJsonStringToMap(jsonData);
        Assert.assertNotNull("Data should be a map of items", itemsMap);
        Assert.assertNotNull("Data should be a map of items", itemsMap.get("Items"));
        Assert.assertTrue("Items should be a list of items", itemsMap.get("Items") instanceof ArrayList);
        List itemsFromResult = (List)itemsMap.get("Items");
        Assert.assertEquals("Number of items should be 10", 10 , itemsFromResult.size());
        for(int i = 0; i<10; i++){
            Assert.assertTrue("Items should be a LinkedTreeMap", itemsFromResult.get(i) instanceof LinkedTreeMap);
            LinkedTreeMap itemObject = (LinkedTreeMap)itemsFromResult.get(i);
            Assert.assertEquals("ItemCode from response should match submitted ItemCode", items.get(i).getItemCode(), itemObject.get("ItemCode"));
            Assert.assertEquals("Item Quantity from response should match submitted item Quantity", items.get(i).getQuantity().toString(), itemObject.get("Quantity"));
            Assert.assertTrue("Item Amount from response should match submitted item Amount", items.get(i).getAmount().equals(Double.parseDouble(itemObject.get("Amount").toString())));
            Assert.assertEquals("Item Name from response should match submitted Item Name", items.get(i).getItemName(), itemObject.get("ItemName"));
            Assert.assertEquals("DepartmentCode from response should match submitted DepartmentCode", items.get(i).getDepartmentCode(), itemObject.get("DepartmentCode"));
            Assert.assertTrue("Price from response should match submitted Item Price", items.get(i).getPrice().equals(Integer.parseInt(itemObject.get("Price").toString())));
        }
        Assert.assertEquals("TransactionSource is equal", submitPurchase.getTransactionSource(),result.get(0).getProperty("TransactionSource"));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));
    }

    /**
     * NO test number - Submit Purchase - with 10 items some of the item with price 0
     **/

    @Test
    public void submitPurchaseWith10ItemsSomeWithPrice0() throws InterruptedException, IOException{

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        //Create new Submit purchase
        SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(newMember.getPhoneNumber());
        ArrayList<Item> items = new ArrayList<>();
        int sum= 0;
        for(int i = 1; i<= 10; i++){
            // half of the items with price zero
            Item item = Initializer.buildPurchaseItem("Item code " + i,"Item name" + i,"Department" + i,"Department name" + i,(i % 2 == 0 ? i : 0),i,Lists.newArrayList());
            sum += item.getPrice();
            items.add(item);
        }
        submitPurchase.setItems(items);
        submitPurchase.setTotalSum(String.valueOf(sum));

        //Send the request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));
        Assert.assertNotNull("Data should contain items data", result.get(0).getProperty("Data"));
        Assert.assertTrue("Data should be A text field", result.get(0).getProperty("Data") instanceof Text);
        Assert.assertNotNull("Data should contain purchase items", ((Text) result.get(0).getProperty("Data")).getValue());
        String dataAsString = ((Text) result.get(0).getProperty("Data")).getValue();

        //Validate all items appear correct as submitted in the submit-purchase
        String jsonData = dataAsString.substring(dataAsString.indexOf("{"));
        Map itemsMap = JsonUtils.convertJsonStringToMap(jsonData);
        Assert.assertNotNull("Data should be a map of items", itemsMap);
        Assert.assertNotNull("Data should be a map of items", itemsMap.get("Items"));
        Assert.assertTrue("Items should be a list of items", itemsMap.get("Items") instanceof ArrayList);
        List itemsFromResult = (List)itemsMap.get("Items");
        Assert.assertEquals("Number of items should be 10", 10 , itemsFromResult.size());
        for(int i = 0; i<10; i++){
            Assert.assertTrue("Items should be a LinkedTreeMap", itemsFromResult.get(i) instanceof LinkedTreeMap);
            LinkedTreeMap itemObject = (LinkedTreeMap)itemsFromResult.get(i);
            Assert.assertEquals("ItemCode from response should match submitted ItemCode", items.get(i).getItemCode(), itemObject.get("ItemCode"));
            Assert.assertEquals("Item Quantity from response should match submitted item Quantity", items.get(i).getQuantity().toString(), itemObject.get("Quantity"));
            Assert.assertTrue("Item Amount from response should match submitted item Amount", items.get(i).getAmount().equals(Double.parseDouble(itemObject.get("Amount").toString())));
            Assert.assertEquals("Item Name from response should match submitted Item Name", items.get(i).getItemName(), itemObject.get("ItemName"));
            Assert.assertEquals("DepartmentCode from response should match submitted DepartmentCode", items.get(i).getDepartmentCode(), itemObject.get("DepartmentCode"));
            Assert.assertTrue("Price from response should match submitted Item Price", items.get(i).getPrice().equals(Integer.parseInt(itemObject.get("Price").toString())));
        }
        Assert.assertEquals("TransactionSource is equal", submitPurchase.getTransactionSource(),result.get(0).getProperty("TransactionSource"));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));
    }




    /**
     * NO test number - Submit Purchase - with 10 items some of the item with Quantity 0
     **/

    @Test
    public void submitPurchaseWith10ItemsSomeWithQuantity0() throws InterruptedException, IOException{

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        //Create new Submit purchase
        SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(newMember.getPhoneNumber());
        ArrayList<Item> items = new ArrayList<>();
        int sum= 0;
        for(int i = 1; i<= 10; i++){
            // half of the items with quantity zero
            Item item = Initializer.buildPurchaseItem("Item code " + i,"Item name" + i,"Department" + i,"Department name" + i,i,(i % 2 ==0 ? i : 0),Lists.newArrayList());
            items.add(item);
            sum += i;
        }
        submitPurchase.setItems(items);
        submitPurchase.setTotalSum(String.valueOf(sum));

        //Send the request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));
        Assert.assertNotNull("Data should contain items data", result.get(0).getProperty("Data"));
        Assert.assertTrue("Data should be A text field", result.get(0).getProperty("Data") instanceof Text);
        Assert.assertNotNull("Data should contain purchase items", ((Text) result.get(0).getProperty("Data")).getValue());
        String dataAsString = ((Text) result.get(0).getProperty("Data")).getValue();

        //Validate all items appear correct as submitted in the submit-purchase
        String jsonData = dataAsString.substring(dataAsString.indexOf("{"));
        Map itemsMap = JsonUtils.convertJsonStringToMap(jsonData);
        Assert.assertNotNull("Data should be a map of items", itemsMap);
        Assert.assertNotNull("Data should be a map of items", itemsMap.get("Items"));
        Assert.assertTrue("Items should be a list of items", itemsMap.get("Items") instanceof ArrayList);
        List itemsFromResult = (List)itemsMap.get("Items");
        Assert.assertEquals("Number of items should be 10", 10 , itemsFromResult.size());
        for(int i = 0; i<10; i++){
            Assert.assertTrue("Items should be a LinkedTreeMap", itemsFromResult.get(i) instanceof LinkedTreeMap);
            LinkedTreeMap itemObject = (LinkedTreeMap)itemsFromResult.get(i);
            Assert.assertEquals("ItemCode from response should match submitted ItemCode", items.get(i).getItemCode(), itemObject.get("ItemCode"));
            Assert.assertEquals("Item Quantity from response should match submitted item Quantity", items.get(i).getQuantity().toString(), itemObject.get("Quantity"));
            Assert.assertTrue("Item Amount from response should match submitted item Amount", items.get(i).getAmount().equals(Double.parseDouble(itemObject.get("Amount").toString())));
            Assert.assertEquals("Item Name from response should match submitted Item Name", items.get(i).getItemName(), itemObject.get("ItemName"));
            Assert.assertEquals("DepartmentCode from response should match submitted DepartmentCode", items.get(i).getDepartmentCode(), itemObject.get("DepartmentCode"));
            Assert.assertTrue("Price from response should match submitted Item Price", items.get(i).getPrice().equals(Integer.parseInt(itemObject.get("Price").toString())));
        }
        Assert.assertEquals("TransactionSource is equal", submitPurchase.getTransactionSource(),result.get(0).getProperty("TransactionSource"));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));
    }


    /**
     * C4303 Submit Purchase with status Open
     *
     **/

    @Test
    public void testSubmitPurchasesWithStatusOpen() throws Exception{

        submitPurchaseTotal("2000");

    }

    //C5884	Submit Purchase positive test with TemporaryToken -Automated
    @Test
    public void testSubmitPurchaseWithTemporaryToken() throws Exception{
        serverHelper.saveApiClient(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(),AnonymousPurchaseTypes.NEVER.type());
        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("100", userToken, locationId);

        //Create new Submit purchase
        SubmitPurchase submitPurchase = createSubmitPurchaseWithTempToken(tokenTemp);

        //Send the request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());
    }

    //C5885	Submit Purchase negative test with TemporaryToken -Automated
    @Test
    public void testSubmitPurchaseWithTemporaryTokenNegative() throws InterruptedException, IOException{

        serverHelper.saveApiClient(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(),AnonymousPurchaseTypes.NEVER.type());

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("1", userToken, locationId);
        Thread.sleep(2000);
        //Create new Submit purchase
        SubmitPurchase submitPurchase = createSubmitPurchaseWithTempToken(tokenTemp);


        //Send the request
        ZappServerErrorResponse response = (ZappServerErrorResponse)submitPurchase.SendRequestByApiKey(apiKey, ZappServerErrorResponse.class);
        Assert.assertEquals("Result must be N", "N", response.getResult());
        Assert.assertEquals("ErrorMessage", "One or more customers not found",response.getErrorMessage());
    }



    //	C5886	Submit Purchase positive test with TemporaryToken use few times
    @Test
    public void testSubmitPurchaseWithTemporaryTokenSendRequestFewTimes() throws Exception{


        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.NEVER.type()),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(), true));


        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("10000", userToken, locationId);

        //Create new Submit purchase
        SubmitPurchase submitPurchase = createSubmitPurchaseWithTempToken(tokenTemp);

        for (int i=0; i<10; i++) {
            //Send the request
            log4j.info("i = " + i);
            SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
            Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
            Assert.assertNotNull(submitPurchaseResponse.getConfirmation());
            Thread.sleep(500);
        }
    }


    //C5887	Submit Purchase Two customers when one token is not valid
    @Test
    public void testSubmitPurchaseWithTemporaryTokenTwocustomerOneNotValid() throws Exception{

        serverHelper.saveApiClient(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(),AnonymousPurchaseTypes.NEVER.type());
        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("100", userToken, locationId);

        String userToken2 = serverHelper.createNewMemberGetUserToken();
        String tokenTemp2 = serverHelper.generateTokenWebViewUserToken("1", userToken2, locationId);
        Thread.sleep(2000);

        //Create new Submit purchase
        SubmitPurchase submitPurchase = createSubmitPurchaseWithTempToken2Customers(tokenTemp,tokenTemp2);

        //Send the request
        ZappServerErrorResponse submitPurchaseResponse = (ZappServerErrorResponse)submitPurchase.SendRequestByApiKey(apiKey, ZappServerErrorResponse.class);
        Assert.assertEquals("Result must be N", "N", submitPurchaseResponse.getResult());
        Assert.assertEquals("ErrorMessage", "One or more customers not found",submitPurchaseResponse.getErrorMessage());
    }

    //C5888	Submit Purchase Non valid token
    @Test
    public void testSubmitPurchaseWithTemporaryTokenNonValidToken() throws Exception{
        serverHelper.saveApiClient(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(),AnonymousPurchaseTypes.NEVER.type());

        //Create new Submit purchase
        SubmitPurchase submitPurchase = createSubmitPurchaseWithTempToken("12345");

        //Send the request
        ZappServerErrorResponse submitPurchaseResponse = (ZappServerErrorResponse)submitPurchase.SendRequestByApiKey(apiKey, ZappServerErrorResponse.class);
        Assert.assertEquals("Result must be N", "N", submitPurchaseResponse.getResult());
        Assert.assertEquals("ErrorMessage", "One or more customers not found",submitPurchaseResponse.getErrorMessage());
    }



    @Test
    public void submitPurchaseWith3000ItemsWithQuantity1() throws InterruptedException, IOException{

        int count = 3000;
        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        //Create new Submit purchase
        SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(newMember.getPhoneNumber());
        ArrayList<Item> items = new ArrayList<>();
        int sum= 0;
        for(int i = 1; i<= count; i++){
            //all items with quantity 1
            Item item = Initializer.buildPurchaseItem("Item code " + i,"Item name" + i,"Department" + i,"Department name" + i,i,1,Lists.newArrayList());
            items.add(item);
            sum += i;
        }
        submitPurchase.setItems(items);
        submitPurchase.setTotalSum(String.valueOf(sum));

        //Send the request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));
        Assert.assertNotNull("Data should contain items data", result.get(0).getProperty("Data"));
        Assert.assertTrue("Data should be A text field", result.get(0).getProperty("Data") instanceof Text);
        Assert.assertNotNull("Data should contain purchase items", ((Text) result.get(0).getProperty("Data")).getValue());
        String dataAsString = ((Text) result.get(0).getProperty("Data")).getValue();

        //Validate all items appear correct as submitted in the submit-purchase
        String jsonData = dataAsString.substring(dataAsString.indexOf("{"));
        Map itemsMap = JsonUtils.convertJsonStringToMap(jsonData);
        Assert.assertNotNull("Data should be a map of items", itemsMap);
        Assert.assertNotNull("Data should be a map of items", itemsMap.get("Items"));
        Assert.assertTrue("Items should be a list of items", itemsMap.get("Items") instanceof ArrayList);
        List itemsFromResult = (List)itemsMap.get("Items");
        Assert.assertEquals("Number of items should be " + count, count , itemsFromResult.size());

        for(int i = 0; i<count; i++){
            Assert.assertTrue("Items should be a LinkedTreeMap", itemsFromResult.get(i) instanceof LinkedTreeMap);
            LinkedTreeMap itemObject = (LinkedTreeMap)itemsFromResult.get(i);
            Assert.assertEquals("ItemCode from response should match submitted ItemCode", items.get(i).getItemCode(), itemObject.get("ItemCode"));
            Assert.assertEquals("Item Quantity from response should match submitted item Quantity", items.get(i).getQuantity().toString(), itemObject.get("Quantity"));
            Assert.assertTrue("Item Amount from response should match submitted item Amount", items.get(i).getAmount().equals(Double.parseDouble(itemObject.get("Amount").toString())));
            Assert.assertEquals("Item Name from response should match submitted Item Name", items.get(i).getItemName(), itemObject.get("ItemName"));
            Assert.assertEquals("DepartmentCode from response should match submitted DepartmentCode", items.get(i).getDepartmentCode(), itemObject.get("DepartmentCode"));
            Assert.assertTrue("Price from response should match submitted Item Price", items.get(i).getPrice().equals(Integer.parseInt(itemObject.get("Price").toString())));
        }

        Assert.assertEquals("TransactionSource is equal", submitPurchase.getTransactionSource(),result.get(0).getProperty("TransactionSource"));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);
        Assert.assertTrue("Confirmation must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));
    }








    private SubmitPurchase createSubmitPurchaseWithTempToken (String tempToken){

        SubmitPurchase submitPurchase = new SubmitPurchase();
        Customer customer = new Customer();
        customer.setTemporaryToken(tempToken);
        submitPurchase.getCustomers().add(customer);
        submitPurchase.setBranchId("3");
        submitPurchase.setPosId("13");
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");
        submitPurchase.setTimeStamp(Long.valueOf(System.currentTimeMillis()));
        List<Payment> payments = new ArrayList();
        payments.add(new Payment("Cash", "initGenericSubmitPurchase", "2000"));
        payments.add(new Payment("CreditCard", "initGenericSubmitPurchase", "2000"));
        submitPurchase.setPayments(payments);


        ArrayList<Item> items = new ArrayList<>();
        items.add(Initializer.buildPurchaseItem("4454","LiorItem1","554","Dep3",4000,1,Lists.newArrayList("Tag1","Tag2")));

        submitPurchase.setTotalGeneralDiscount(2000);
        submitPurchase.setTransactionSource("nomore");
        submitPurchase.setItems(items);

        return submitPurchase;
    }

    private SubmitPurchase createSubmitPurchaseWithTempToken2Customers (String tempToken, String token2){

        SubmitPurchase submitPurchase = new SubmitPurchase();
        Customer customer = new Customer();
        customer.setTemporaryToken(tempToken);
        submitPurchase.getCustomers().add(customer);

        Customer customer2 = new Customer();
        customer2.setTemporaryToken(token2);
        submitPurchase.getCustomers().add(customer2);

        submitPurchase.setBranchId("3");
        submitPurchase.setPosId("13");
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");
        submitPurchase.setTimeStamp(Long.valueOf(System.currentTimeMillis()));
        List<Payment> payments = new ArrayList();
        payments.add(new Payment("Cash", "initGenericSubmitPurchase", "2000"));
        payments.add(new Payment("CreditCard", "initGenericSubmitPurchase", "2000"));
        submitPurchase.setPayments(payments);

        ArrayList<Item> items = new ArrayList<>();
        items.add(Initializer.buildPurchaseItem("4454","LiorItem1","554","Dep3",4000,1,Lists.newArrayList("Tag1","Tag2")));

        submitPurchase.setTotalGeneralDiscount((long) 2000);
        submitPurchase.setTransactionSource("nomore");
        submitPurchase.setItems(items);
        return submitPurchase;
    }

    /**
     *   C8358 remove validate items validation in POS 2.8 and 4.0 - Automated
     *   Items total sum is not validated
     */

    @Test
    public void testSubmitPurchasesTotalSumIsNotEqualsToItemsSum() throws Exception{

        submitPurchaseTotal("2222");

    }

    /**
     *   CNP-9735 line id is not saved on purchase
     *   Added Bug verification + more validations on item properties
     *   Also, validates bug CNP-10056
     */
    @Test
    @Category({ServerRegression.class})
    public void testSubmitPurchaseValidateItems () throws Exception {

        submitPurchaseWithItemsLineID(System.currentTimeMillis()+ "121");

    }


    private void submitPurchaseTotal (String totalSum) throws Exception {


        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        //Create Submit purchase - with status open
        SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(newMember.getPhoneNumber());
        submitPurchase.setTotalGeneralDiscount(2000);
        submitPurchase.setTotalSum(totalSum);
        submitPurchase.setStatus("Open");

        //Send the request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));
    }

    private void submitPurchaseWithItemsLineID (String lineId) throws Exception {


        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        //Create Submit purchase - with status open
        SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(newMember.getPhoneNumber());
        submitPurchase.setTotalGeneralDiscount(2000);
        submitPurchase.setTotalSum("2222");
        submitPurchase.setStatus("Final");

        Item item = Initializer.buildPurchaseItem("code1","name1","depcode1", "depName1", 100, 1, Lists.newArrayList("tag1"));
        item.setLineID(lineId);
        submitPurchase.setItems(Lists.newArrayList(item));

        //Send the request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse)submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());

        //Verify with DataStore
        Query q = new Query("PurchaseItem");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one PurchaseItem with the transaction ID: " + submitPurchase.getTransactionId(), q,1, dataStore);

        serverHelper.validatePurchaseItemResult(submitPurchase, item, result.get(0));
    }





}