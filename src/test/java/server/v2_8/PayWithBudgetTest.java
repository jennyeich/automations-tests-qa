package server.v2_8;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import hub.base.categories.serverCategories.ServerRegression;
import hub.base.categories.serverCategories.serverV2_8;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.base.BaseServerTest;
import server.common.*;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Customer;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.*;


/**
 * Created by israel on 30/11/2016.
 */
public class PayWithBudgetTest extends BaseServerTest {


    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        updateConfiguration = true;
        init();
    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            serverHelper.setConfigurationForPayment("false",PaymentType.POINTS.type(),"1","false");
            serverHelper.updateBusinessBackend(LocationBackendFields.DECIMAL_MODE.key(),true);
        }

    };
    /**
     * c2938 - Pay With Budget
     * */
    @Test
    public void payWithBudget() throws Exception {

        //Verify if the parameter in the Business Backend :"Payment Requires Verification Code" = No
        serverHelper.saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);

        Map<String, Object> props = new HashMap<>();
        props.put(LocationBackendFields.PAY_WITH_BUDGET_MIN_SUM.key(), "-1");
        props.put(LocationBackendFields.PAY_WITH_BUDGET_MAX_SUM.key(), "-1");
        props.put(LocationBackendFields.PAY_WITH_BUDGET_TYPE.key(),PaymentType.POINTS.type());
        serverHelper.updateBusinessBackend(props);

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        //Add points 100 to the user
        InvokeEventBasedActionRuleBuilder invokeEventBasedActionRuleBuilder = new InvokeEventBasedActionRuleBuilder();
        InvokeEventBasedActionRule invokeEventBasedActionRule = invokeEventBasedActionRuleBuilder.buildGivePointsRequest(newMember.getMembershipKey(), newMember.getUserKey(), locationId, "1000");
        IServerResponse response1 = invokeEventBasedActionRule.SendRequestByToken(serverToken, Response.class);

        // make sure the response is not Error response
        if (response1 instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response1).getErrorMessage(), false);
        }

        Thread.sleep(20000);

       int pointsBefore = getMembersPoints(customer);


        //Add points 100 to the user
        invokeEventBasedActionRule = invokeEventBasedActionRuleBuilder.buildGivePointsRequest(memberKey, newMember.getUserKey(), locationId, "10000");
        IServerResponse response = invokeEventBasedActionRule.SendRequestByToken(serverToken, Response.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response).getErrorMessage(), false);
        }

        Thread.sleep(10000);

        int pointsAfter = getMembersPoints(customer);

        Assert.assertEquals("Customer needs to receive 100 points",11000,pointsAfter);

        //Set pay with budget
        PayWithBudget payWithBudget = buildPayWithBudget(customer, "5000");

        // Send the request
        IServerResponse response2 = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);


        // make sure the response is not Error response
        if (response2 instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response2).getErrorMessage(), false);
        }

        // Cast to PayWithBudgetResponse object to fetch all response data
        PayWithBudgetResponse payWithBudgetResponse = (PayWithBudgetResponse) response2;
        Assert.assertEquals("status code must be 200",(Integer) 200, payWithBudgetResponse.getStatusCode());
        String compareTotalSum = "-" + payWithBudget.getTotalSum();
        Assert.assertEquals("Actual Charge", payWithBudgetResponse.getActualCharge(), compareTotalSum);


        //Verify with DataStore
        checkResultsForPay(payWithBudget,payWithBudgetResponse);

        // verify the customer spent 50 points
        int pointsAfterPurchase = getMembersPoints(customer);
        Assert.assertEquals("Customer needs to spend 50 points", -5000, pointsAfterPurchase-pointsAfter);

    }


    /**
     * C2494 - Min/Max Budget Payment - Sum>o & AllowPayWithBudgetCharge = True
     * */
    @Test
    public void MinMaxPayWithBudgetSumGreaterThan0() throws Exception {

        //Verify if the parameter in the Business Backend :"Payment Requires Verification Code" = No
        serverHelper.updateBusinessBackend("PayWithBudgetMinSum", "10000");
        serverHelper.updateBusinessBackend("PayWithBudgetMaxSum", "20000");
        serverHelper.updateBusinessBackend("AllowPayWithBudgetCharge","true");
        serverHelper.updateBusinessBackend("PayWithBudgetType","Points");

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        String memberKey = newMember.getMembershipKey();

        //Add points 100 to the user
        InvokeEventBasedActionRuleBuilder invokeEventBasedActionRuleBuilder = new InvokeEventBasedActionRuleBuilder();
        InvokeEventBasedActionRule invokeEventBasedActionRule = invokeEventBasedActionRuleBuilder.buildGivePointsRequest(memberKey, newMember.getUserKey(), locationId, "20000");
        IServerResponse response1 = invokeEventBasedActionRule.SendRequestByToken(serverToken, Response.class);

        // make sure the response is not Error response
        if (response1 instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response1).getErrorMessage(), false);
        }

        Thread.sleep(20000);

        int pointsBefore = getMembersPoints(customer);


        //Add points 100 to the user
        invokeEventBasedActionRule = invokeEventBasedActionRuleBuilder.buildGivePointsRequest(memberKey, newMember.getUserKey(), locationId, "10000");
        IServerResponse response = invokeEventBasedActionRule.SendRequestByToken(serverToken, Response.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response).getErrorMessage(), false);
        }

        Thread.sleep(10000);

        int pointsAfter = getMembersPoints(customer);

        Assert.assertEquals("Customer needs to receive 100 points", 10000, pointsAfter-pointsBefore);

        //Set pay with budget
        PayWithBudget payWithBudget = buildPayWithBudget(customer, "10000");

        // Send the request
        IServerResponse response2 = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);


        // make sure the response is not Error response
        if (response2 instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response2).getErrorMessage(), false);
        }

        // Cast to PayWithBudgetResponse object to fetch all response data
        PayWithBudgetResponse payWithBudgetResponse = (PayWithBudgetResponse) response2;
        Assert.assertEquals("status code must be 200", payWithBudgetResponse.getStatusCode(), (Integer) 200);
        String compareTotalSum = "-" + payWithBudget.getTotalSum();
        Assert.assertEquals("Actual Charge", payWithBudgetResponse.getActualCharge(), compareTotalSum);

        Query qUserAction = new Query("UserAction");
        ArrayList<Query.Filter> filters = new ArrayList<>();
        filters.add(new Query.FilterPredicate("BudgetTransactionUID",Query.FilterOperator.EQUAL,payWithBudgetResponse.getPaymentUID()));
        filters.add(new Query.FilterPredicate("Action",Query.FilterOperator.EQUAL, "budgettransaction"));
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, filters));

        List<Entity> resultUserAction = serverHelper.queryWithWait("Must find UserAction with the BudgetTransactionUID: " + payWithBudgetResponse.getPaymentUID(),
                qUserAction, 1, dataStore);
        Assert.assertTrue("Transaction ID must be in User Action dta", ((Text)resultUserAction.get(0).getProperty("Data")).getValue().contains(payWithBudget.getTransactionId()));
        Assert.assertTrue("User Action must contains Actual Charge ",serverHelper.dataContainsConfirmation(resultUserAction,payWithBudgetResponse.getActualCharge()));

        //Verify with DataStore
        checkResultsForPay(payWithBudget,payWithBudgetResponse);

    }


    //Test Pay With Budget - No verification code
    @Test
    public void testAddBudgetToGiftCardNoVerificationCode () throws Exception {

        giftcardWithVerificationCode("");
    }



    //Test Pay With Budget - Wrong verification code
    @Test
    @Category({ServerRegression.class})
    public void testAddBudgetToGiftCardWithWrongVerificationCode () throws Exception {

        giftcardWithVerificationCode("94854985734");

    }


    private void giftcardWithVerificationCode (String verificationCode) throws Exception{

        serverHelper.updateBusinessBackend(LocationBackendFields.ALLOW_PAY_WITH_BUDGET_CHARGE.key(), "true");
        serverHelper.saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), true);


        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        PayWithBudget payWithBudget = buildPayWithBudgetWithVerificatiobCode(customer, "100", verificationCode);
        IServerResponse response = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);

        Assert.assertEquals("The error message is not as expected","Missing or invalid verification code" ,((ZappServerErrorResponse) response).getErrorMessage());
        Assert.assertEquals("The error code is not as expected","7004" ,((ZappServerErrorResponse) response).getErrorCode());

    }

    private PayWithBudget buildPayWithBudget(Customer customer, String totalSum) {
        PayWithBudget payWithBudget = new PayWithBudget();
        payWithBudget.setAllowPartialPayment(true);
        payWithBudget.getCustomers().add(customer);
        payWithBudget.setTotalSum(totalSum);
        payWithBudget.setTimeStamp("1431430104");
        payWithBudget.setTransactionId(UUID.randomUUID().toString());
        payWithBudget.setBranchId("1");
        payWithBudget.setPosId("2");
        return payWithBudget;
    }

    private PayWithBudget buildPayWithBudgetWithVerificatiobCode (Customer customer, String totalSum, String verificationCode) {

        PayWithBudget payWithBudget = buildPayWithBudget(customer,totalSum);
        payWithBudget.setverificationCode(verificationCode);
        return payWithBudget;
    }

    //	C5894	Pay with budget positive test with TemporaryToken
    @Ignore
    public void testPayWithBudgetWithTemporaryToken () throws IOException, InterruptedException {

        //Verify if the parameter in the Business Backend :"Payment Requires Verification Code" = No
        serverHelper.saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("10000", userToken, locationId);

        //Set pay with budget
        PayWithBudget payWithBudget = createPayWithBudgetWithTemporaryToken(tokenTemp);

        // Send the request
        IServerResponse response2 = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);
        // make sure the response is not Error response
        if (response2 instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response2).getErrorMessage(), false);
        }

        // Cast to PayWithBudgetResponse object to fetch all response data
        PayWithBudgetResponse payWithBudgetResponse = (PayWithBudgetResponse) response2;
        Assert.assertEquals("status code must be 200",(Integer) 200, payWithBudgetResponse.getStatusCode());
        String compareTotalSum = "5000";
        Assert.assertEquals("Actual Charge", payWithBudgetResponse.getActualCharge(), compareTotalSum);

    }


    //	C5895	Pay with budget negative test with TemporaryToken -Automated
    @Test
    public void testPayWithBudgetWithTemporaryTokenNegative () throws IOException, InterruptedException {

        //Verify if the parameter in the Business Backend :"Payment Requires Verification Code" = No
        serverHelper.saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("1", userToken, locationId);
        Thread.sleep(2000);

        //Set pay with budget
        PayWithBudget payWithBudget = createPayWithBudgetWithTemporaryToken(tokenTemp);

        ZappServerErrorResponse payWithBudgetResponse = (ZappServerErrorResponse)payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);

        Assert.assertEquals("Result must be N", "N", payWithBudgetResponse.getResult());
        Assert.assertEquals("ErrorMessage", "One or more customers not found",payWithBudgetResponse.getErrorMessage());


    }

    //C5896	PayWith budget positive test with TemporaryToken use few times
    @Test
    @Category({ServerRegression.class})
    public void testPayWithBudgetWithTemporaryTokenFewTimes () throws IOException, InterruptedException {

        //Verify if the parameter in the Business Backend :"Payment Requires Verification Code" = No
        serverHelper.saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("1000", userToken, locationId);

        //Set pay with budget
        PayWithBudget payWithBudget = createPayWithBudgetWithTemporaryToken(tokenTemp);

        // Send the request
        IServerResponse response2 = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);
        // make sure the response is not Error response
        if (response2 instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response2).getErrorMessage(), false);
        }
        for (int i=0; i<10; i++) {
            // Cast to PayWithBudgetResponse object to fetch all response data
            PayWithBudgetResponse payWithBudgetResponse = (PayWithBudgetResponse) response2;
            Assert.assertEquals("status code must be 200", (Integer) 200, payWithBudgetResponse.getStatusCode());
            String compareTotalSum = "5000";
            Assert.assertEquals("Actual Charge", payWithBudgetResponse.getActualCharge(), compareTotalSum);
        }
    }

    //	C5897	Pay with budget Two customers when one token is not valid
    @Test
    @Category(serverV2_8.class)
    public void testPayWithBudgetWithTemporaryToken2Customers () throws IOException, InterruptedException {

        //Verify if the parameter in the Business Backend :"Payment Requires Verification Code" = No
        serverHelper.saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("10000", userToken, locationId);
        String userToken2 = serverHelper.createNewMemberGetUserToken();
        String tokenTemp2 = serverHelper.generateTokenWebViewUserToken("1",userToken2, locationId);
        Thread.sleep(2000);

        //Set pay with budget
        PayWithBudget payWithBudget = createPayWithBudgetWithTemporaryToken2Customers(tokenTemp, tokenTemp2);
        ZappServerErrorResponse payWithBudgetResponse = (ZappServerErrorResponse)payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);
        Assert.assertEquals("Result must be N", "N", payWithBudgetResponse.getResult());
        Assert.assertEquals("ErrorMessage", "One or more customers not found",payWithBudgetResponse.getErrorMessage());
    }



    //	C5898 - Pay With Budget Non valid token
    @Test
    public void testPayWithBudgetWithNonValidTemporaryTokenNegative () throws IOException, InterruptedException {

        //Verify if the parameter in the Business Backend :"Payment Requires Verification Code" = No
        serverHelper.saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);

        //Set pay with budget
        PayWithBudget payWithBudget = createPayWithBudgetWithTemporaryToken("ioiu!@#$%^&*()_+<>/;/");

        ZappServerErrorResponse payWithBudgetResponse = (ZappServerErrorResponse)payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);

        Assert.assertEquals("Result must be N", "N", payWithBudgetResponse.getResult());
        Assert.assertEquals("ErrorMessage", "One or more customers not found",payWithBudgetResponse.getErrorMessage());


    }


    private PayWithBudget createPayWithBudgetWithTemporaryToken (String tokenTemp){

        Customer customer = new Customer();
        customer.setTemporaryToken(tokenTemp);

        //Set pay with budget
        PayWithBudget payWithBudget = new PayWithBudget();
        payWithBudget.setAllowPartialPayment(true);
        payWithBudget.getCustomers().add(customer);
        payWithBudget.setTotalSum("-5000");
        payWithBudget.setTimeStamp("1431430104");
        payWithBudget.setTransactionId(UUID.randomUUID().toString());
        payWithBudget.setBranchId("1");

        return payWithBudget;
    }

    private PayWithBudget createPayWithBudgetWithTemporaryToken2Customers (String tokenTemp, String tokenTemp2){

        Customer customer = new Customer();
        customer.setTemporaryToken(tokenTemp);
        Customer customer2 = new Customer();
        customer2.setTemporaryToken(tokenTemp2);
        //Set pay with budget
        PayWithBudget payWithBudget = new PayWithBudget();
        payWithBudget.setAllowPartialPayment(true);
        payWithBudget.getCustomers().add(customer);
        payWithBudget.getCustomers().add(customer2);
        payWithBudget.setTotalSum("-5000");
        payWithBudget.setTimeStamp("1431430104");
        payWithBudget.setTransactionId(UUID.randomUUID().toString());
        payWithBudget.setBranchId("1");

        return payWithBudget;
    }



    private int getMembersPoints(Customer customer){//TODO: server helper
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.getCustomers().add(customer);
        getMemberDetails.setExpandAssets(false);
        getMemberDetails.setIncludeArchivedAssets(false);
        IServerResponse getMemberDetailsResponse = getMemberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);

        // make sure the response is not Error response
        if (getMemberDetailsResponse instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) getMemberDetailsResponse).getErrorMessage(), false);
        }
        int points = ((GetMemberDetailsResponse)getMemberDetailsResponse).getMemberships().get(0).getPoints();
        return points;
    }
    private void checkResultsForPay(PayWithBudget payment, PayWithBudgetResponse paymentResponse) throws Exception {

        log4j.info("Check BudgetTransaction query in data store for UID " + paymentResponse.getPaymentUID());
        Query q = serverHelper.buildQueryForBudgetTransaction(paymentResponse.getPaymentUID());
        List<Entity> result = serverHelper.queryWithWait("Must find one budgetTransaction with the UID: "+ paymentResponse.getPaymentUID(),q, 1, dataStore);
        Assert.assertEquals("ApiKey should be equal",apiKey, result.get(0).getProperty("ApiKey"));
        Assert.assertEquals("BranchID should be equal",payment.getBranchId(), result.get(0).getProperty("BranchID"));
        Assert.assertEquals("BusinessID should be equal",locationId, result.get(0).getProperty("LocationID"));
        Assert.assertEquals("PosId should be equal",payment.getPosId(), result.get(0).getProperty("PosID"));
        Assert.assertEquals("TransactionID should equal",payment.getTransactionId(), result.get(0).getProperty("TransactionID"));

    }

}
