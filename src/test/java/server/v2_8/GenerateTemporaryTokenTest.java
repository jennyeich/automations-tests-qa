package server.v2_8;

import hub.base.categories.serverCategories.ServerRegression;
import hub.base.categories.serverCategories.serverV2_8;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.base.BaseServerTest;
import server.common.IServerResponse;
import server.v2_8.base.ZappServerErrorResponse;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

/**
 * Created by Lior on 12/1/2016.
 */

public class GenerateTemporaryTokenTest extends BaseServerTest {

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        updateConfiguration = true;
        init();
    }

    //C5889	Generate token non valid location
    @Test
    @Category({ServerRegression.class})
    public void testTemporaryTokenNonValidLocationID (){

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("100", userToken, "stam");

        Assert.assertEquals("ErrorMessage", "HTTP 403 Forbidden",tokenTemp);

    }

    //	C5890	Generate token non valid user token
    @Test
    @Category(serverV2_8.class)
    public void testTemporaryTokenNonValidUserToken (){

        String tokenTemp = serverHelper.generateTokenWebViewUserToken("100", "NonValidUserToken", locationId);
        Assert.assertEquals("ErrorMessage", "HTTP 403 Forbidden",tokenTemp);

    }


    //C5891	Generate token valid user token another location - negative test
    @Test
    public void testTemporaryTokenValidLocationIDUserNotExisting () throws Exception{

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("100", userToken, "5257");
        Assert.assertEquals("ErrorMessage", "HTTP 403 Forbidden",tokenTemp);
    }

    //Generate token - the value is more than the minimum - returns default token for a day
    @Test
    public void testTemporaryTokenVeryLongTime () throws Exception{

        String userToken = serverHelper.createNewMemberGetUserToken();
        GenerateToken generateToken = new GenerateToken();
        generateToken.setTimeToLive("8640022");
        GenerateTokenResponse response2 = (GenerateTokenResponse)generateToken.sendRequestByUserTokenAndLocationID(userToken, locationId,GenerateTokenResponse.class);

        GetMemberBenefits getMemberBenefits = serverHelper.createGetMemberBenefits(response2.getToken());
        //Send the request
        IServerResponse response = getMemberBenefits.SendRequestByApiKey(apiKey, GetMemberBenefitsResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        // Result true assertion
        GetMemberBenefitsResponse getMemberBenefitsResponse = (GetMemberBenefitsResponse) response;
        Assert.assertEquals("Result must be Y", "Y", getMemberBenefitsResponse.getResult());
    }

    //C5893	Generate token with string time- negative test
    @Test
    public void testTemporaryString (){

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("gfhgfhgfhfghgf", userToken, locationId);
        Boolean ans = tokenTemp.contains("Can not deserialize value of type java.lang.Integer from String \"gfhgfhgfhfghgf\": not a valid Integer value");
        Assert.assertTrue("The token can be generated from String",ans);
    }






}


