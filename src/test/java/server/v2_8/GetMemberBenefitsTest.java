package server.v2_8;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import hub.base.categories.serverCategories.ServerRegression;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.base.BaseServerTest;
import server.common.*;
import server.common.asset.AssetBuilder;
import server.common.asset.NewAsset;
import server.common.asset.RedeemAction;
import server.common.asset.Scenario;
import server.common.asset.actions.DealCode;
import server.common.asset.actions.ItemCode;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Customer;
import server.v2_8.common.DeactivateAsset;
import server.v2_8.common.Item;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.lang.System.currentTimeMillis;

/**
 * Created by Lior on 12/1/2016.
 */
public class GetMemberBenefitsTest extends BaseServerTest {

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        updateConfiguration = true;
        init();
    }

    /**
     * C2933 - Get member benefits (one deal code)
     * */

    @Test
    @Category({ServerRegression.class})
    public void testGetMemberBenefits() throws Exception {

        // Create new club deal
        AssetBuilder assetBuilder=new AssetBuilder();
        NewAsset newAsset=assetBuilder.buildDefaultClubDeals(locationId);

        RedeemAction redeemAction = new RedeemAction();
        Scenario scenario1 = redeemAction.addScenario();
        String dealCodeRelated = UUID.randomUUID().toString();
        scenario1.addDealCodeAction().addDealCode(dealCodeRelated);
        newAsset.setRedeemAction(redeemAction);

        IServerResponse assetResponse = newAsset.SendRequestByToken(serverToken, Response.class);
        Assert.assertEquals("New asset has been sent and return with status code 200",(Integer)200,assetResponse.getStatusCode());

        JoinClubResponse customerPhone = createMember();

        Customer customer = new Customer();
        customer.setPhoneNumber(customerPhone.getPhoneNumber());
        GetMemberBenefits getMemberBenefits = new GetMemberBenefits();
        getMemberBenefits.getCustomers().add(customer);
        getMemberBenefits.setReturnExtendedItems(true);
        getMemberBenefits.setTotalSum("2000");
        //Add item
        Item item1 = new Item();
        item1.setLineID("1");
        item1.setItemCode("100");
        item1.setQuantity(1);
        item1.setItemName("prod1");
        item1.setDepartmentCode("123");
        item1.setDepartmentName("dep");
        item1.setPrice(2000);
        getMemberBenefits.getItems().add(item1);

        //Send the request
        IServerResponse response = getMemberBenefits.SendRequestByApiKey(apiKey, GetMemberBenefitsResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        // Result true assertion
        GetMemberBenefitsResponse getMemberBenefitsResponse = (GetMemberBenefitsResponse) response;
        Assert.assertEquals("Result must be Y", "Y", getMemberBenefitsResponse.getResult());

        //DealCodes and ItemCodes return
        ArrayList<String> dealCodes = ((DealCode)scenario1.getActions().get(0)).getDealCodes();
        for(String dealCode : dealCodes){
            Assert.assertTrue("member benefits must include the added deal codes", getMemberBenefitsResponse.getSimplefiedDealCodes().contains(dealCode));
        }

        DeactivateAsset.deactivate(apiKey, assetResponse.getRawObject().getString("Key"));
        //Assert.assertTrue("Response time must be under 30000, time was: " + getMemberBenefitsResponse.getTime(), getMemberBenefitsResponse.getTime() <= 30000 );

    }

    /**
     * C2934 - Get member benefits (More than 1 discount + more than 1 deal code + more than 1item codes)
     * */

    @Test
    @Category({ServerRegression.class})
    public void testGetMemberBenefitsMoreThanOneDiscountAndDealCodes() throws Exception {

        //TODO: Test not finished - need to add discount options to the asset builder
        //Create new club deal with more then one deal codes and item codes
        AssetBuilder assetBuilder = new AssetBuilder();
        NewAsset newAsset = assetBuilder.buildDefaultClubDeals(locationId);

        RedeemAction redeemAction = new RedeemAction();
        Scenario scenario1 = redeemAction.addScenario();
        scenario1.addDealCodeAction().addDealCode(UUID.randomUUID().toString()).addDealCode(UUID.randomUUID().toString());
        scenario1.addItemCodeAction().addItemCodes(UUID.randomUUID().toString()).addItemCodes(UUID.randomUUID().toString());
        newAsset.setRedeemAction(redeemAction);


        IServerResponse assetResponse = newAsset.SendRequestByToken(serverToken, Response.class);

        JoinClubResponse customerPhone = createMember();

        Thread.sleep(5000);
        Customer customer = new Customer();
        customer.setPhoneNumber(customerPhone.getPhoneNumber());
        GetMemberBenefits getMemberBenefits = new GetMemberBenefits();
        getMemberBenefits.getCustomers().add(customer);
        getMemberBenefits.setReturnExtendedItems(true);
        getMemberBenefits.setTotalSum("4000");
        //Add item1
        Item item1 = new Item();
        item1.setLineID("1");
        item1.setItemCode("123");
        item1.setQuantity(1);
        item1.setItemName("Test1");
        item1.setDepartmentCode("444");
        item1.setDepartmentName("dep");
        item1.setPrice(2000);
        getMemberBenefits.getItems().add(item1);
        //Add item2
        Item item2 = new Item();
        item2.setLineID("2");
        item2.setItemCode("999");
        item2.setQuantity(1);
        item2.setItemName("Test2");
        item2.setDepartmentCode("111");
        item2.setDepartmentName("dep2");
        item2.setPrice(2000);
        getMemberBenefits.getItems().add(item2);

        //Send the request
        IServerResponse response = getMemberBenefits.SendRequestByApiKey(apiKey, GetMemberBenefitsResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        // Result true assertion
        GetMemberBenefitsResponse getMemberBenefitsResponse = (GetMemberBenefitsResponse) response;
        Assert.assertEquals("Result must be Y", "Y", getMemberBenefitsResponse.getResult());

        //More than one DealCodes and ItemCodes return
        ArrayList<String> dealCodes = ((DealCode)scenario1.getActions().get(0)).getDealCodes();
        for(String dealCode : dealCodes){
            Assert.assertTrue("member benefits must include the added deal codes", getMemberBenefitsResponse.getSimplefiedDealCodes().contains(dealCode));
        }

        ArrayList<String> itemCodes = ((ItemCode)scenario1.getActions().get(1)).getItemCodes();
        for(String itemCode : itemCodes){
            Assert.assertTrue("member benefits must include the added item codes", getMemberBenefitsResponse.getSimplefiedItemCodes().contains(itemCode));
        }

        Assert.assertTrue("DealCodes must be more than one", getMemberBenefitsResponse.getDealCodes().size()>1);
        Assert.assertTrue("ItemCodes must be more than one", getMemberBenefitsResponse.getItemCodes().size()>1);

        DeactivateAsset.deactivate(apiKey, assetResponse.getRawObject().getString("Key"));



        //Assert.assertTrue("Response time must be under 50000, time was: " + getMemberBenefitsResponse.getTime(), getMemberBenefitsResponse.getTime() <= 50000 );

    }

    /**
     * C2935 - Get member benefits (More than 2 items in the Club Deal and 1 Deal Code)
     * */

    @Test
    public void testGetMemberBenefitsMoreThanTwoItemCodes() throws Exception {

        //Create new club deal with more then one deal codes and item codes
        AssetBuilder assetBuilder = new AssetBuilder();
        NewAsset newAsset = assetBuilder.buildDefaultClubDeals(locationId);

        RedeemAction redeemAction = new RedeemAction();
        Scenario scenario1 = redeemAction.addScenario();
        scenario1.addDealCodeAction().addDealCode(UUID.randomUUID().toString());
        scenario1.addItemCodeAction().addItemCodes(UUID.randomUUID().toString()).addItemCodes(UUID.randomUUID().toString()).addItemCodes(UUID.randomUUID().toString());
        newAsset.setRedeemAction(redeemAction);

        IServerResponse assetResponse = newAsset.SendRequestByToken(serverToken, Response.class);

        JoinClubResponse customerPhone = createMember();

        Customer customer = new Customer();
        customer.setPhoneNumber(customerPhone.getPhoneNumber());
        GetMemberBenefits getMemberBenefits = new GetMemberBenefits();
        getMemberBenefits.getCustomers().add(customer);
        getMemberBenefits.setReturnExtendedItems(true);
        getMemberBenefits.setTotalSum("6000");
        //Add item1
        Item item1 = new Item();
        item1.setLineID("1");
        item1.setItemCode("123");
        item1.setQuantity(1);
        item1.setItemName("Test1");
        item1.setDepartmentCode("444");
        item1.setDepartmentName("dep");
        item1.setPrice(3000);
        getMemberBenefits.getItems().add(item1);
        //Add item2
        Item item2 = new Item();
        item2.setLineID("2");
        item2.setItemCode("999");
        item2.setQuantity(1);
        item2.setItemName("Test2");
        item2.setDepartmentCode("111");
        item2.setDepartmentName("dep2");
        item2.setPrice(3000);
        getMemberBenefits.getItems().add(item2);

        //Send the request
        IServerResponse response = getMemberBenefits.SendRequestByApiKey(apiKey, GetMemberBenefitsResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        // Result true assertion
        GetMemberBenefitsResponse getMemberBenefitsResponse = (GetMemberBenefitsResponse) response;
        Assert.assertEquals("Result must be Y", "Y", getMemberBenefitsResponse.getResult());

        //Deal codes return
        ArrayList<String> dealCodes = ((DealCode)scenario1.getActions().get(0)).getDealCodes();
        for(String dealCode : dealCodes){
            Assert.assertTrue("member benefits must include the added deal codes", getMemberBenefitsResponse.getSimplefiedDealCodes().contains(dealCode));
        }

        //Item codes return
        ArrayList<String> itemCodes = ((ItemCode)scenario1.getActions().get(1)).getItemCodes();
        for(String itemCode : itemCodes){
            Assert.assertTrue("member benefits must include the added item codes", getMemberBenefitsResponse.getSimplefiedItemCodes().contains(itemCode));
        }

        //More than two ItemCodes returns
        Assert.assertTrue("ItemCodes must be more than one", getMemberBenefitsResponse.getItemCodes().size()>1);


        DeactivateAsset.deactivate(apiKey, assetResponse.getRawObject().getString("Key"));

        //Assert.assertTrue("Response time must be under 30000, time was: " + getMemberBenefitsResponse.getTime(), getMemberBenefitsResponse.getTime() <= 30000 );

    }

    //C3834
    @Ignore
    public void testCancelBudgetGiftCardUsingPaymentUID() throws Exception {

        serverHelper.saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);
        serverHelper.updateBusinessBackend("PayWithBudgetType","Points");

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        //Add points 100 to the user
        InvokeEventBasedActionRuleBuilder invokeEventBasedActionRuleBuilder = new InvokeEventBasedActionRuleBuilder();
        InvokeEventBasedActionRule invokeEventBasedActionRule = invokeEventBasedActionRuleBuilder.buildGivePointsRequest(memberKey, newMember.getUserKey(), locationId, "1000");
        IServerResponse response1 = invokeEventBasedActionRule.SendRequestByToken(serverToken, Response.class);

        // make sure the response is not Error response
        if (response1 instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response1).getErrorMessage(), false);
        }

        Thread.sleep(20000);

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        PayWithBudget payWithBudget = new PayWithBudget();
        payWithBudget.getCustomers().add(customer);
        payWithBudget.setAllowPartialPayment(true);
        payWithBudget.setTotalSum("20");
        payWithBudget.setTimeStamp(String.valueOf(System.currentTimeMillis()));
        payWithBudget.setTransactionId(UUID.randomUUID().toString());
        payWithBudget.setBranchId("1");

        IServerResponse response = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);


        if (response instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response).getErrorMessage(), false);
        }

        // Cast to PayWithBudgetResponse object to fetch all response data
        PayWithBudgetResponse payWithBudgetResponse = (PayWithBudgetResponse) response;
        Assert.assertEquals("Result = Y", "Y" ,payWithBudgetResponse.getResult());
        Assert.assertFalse("PaymentUID confirmation ID must not be empty", StringUtils.isEmpty(payWithBudgetResponse.getPaymentUID()));
        Assert.assertEquals("Actual Charge","-20", payWithBudgetResponse.getActualCharge());



        CancelBudgetPayment cancelBudgetPayment = new CancelBudgetPayment();
        cancelBudgetPayment.setPaymentUID(payWithBudgetResponse.getPaymentUID());

        IServerResponse responseCancel = cancelBudgetPayment.SendRequestByApiKey(apiKey, CancelBudgetPaymentResponse.class);

        // make sure the response is not Error response
        if (responseCancel instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) responseCancel).getErrorMessage());
        }


        //Verify with DataStore
        Query q = new Query("BudgetTransaction");
        q.setFilter(new Query.FilterPredicate("UID",Query.FilterOperator.EQUAL, payWithBudgetResponse.getPaymentUID()));
        List<Entity> result = serverHelper.queryWithWait("Must find one budgetTransaction with the UID: "+ payWithBudgetResponse.getPaymentUID(),q, 1, dataStore);
        Assert.assertEquals("ApiKey is equal",apiKey, result.get(0).getProperty("ApiKey"));
        Assert.assertEquals("BranchID is equal",payWithBudget.getBranchId(), result.get(0).getProperty("BranchID"));
        Assert.assertEquals("BusinessID is equal",locationId, result.get(0).getProperty("LocationID"));
        Assert.assertEquals("TransactionID is equal",payWithBudget.getTransactionId(), result.get(0).getProperty("TransactionID"));
        Assert.assertEquals("Status is equal","canceled", result.get(0).getProperty("Status"));
        Assert.assertEquals("RequestedSum is not like expected", (long)-20, result.get(0).getProperty("RequestedSum"));

    }


    /**
     * 	C5873  - Get member details positive test with TemporaryToken
     */

    @Test
    @Category({ServerRegression.class})
    public void testGetMemberBenefitsWithTemporaryToken (){

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("10000", userToken, locationId);
        GetMemberBenefits getMemberBenefits = serverHelper.createGetMemberBenefits(tokenTemp);

        //Send the request
        IServerResponse response = getMemberBenefits.SendRequestByApiKey(apiKey, GetMemberBenefitsResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        // Result true assertion
        GetMemberBenefitsResponse getMemberBenefitsResponse = (GetMemberBenefitsResponse) response;
        Assert.assertEquals("Result must be Y", "Y", getMemberBenefitsResponse.getResult());
    }

    /**
     * 	C5874  - Get member details negative test with TemporaryToken
     */
    @Test
    @Category({ServerRegression.class})
    public void testGetMemberBenefitsWithTemporaryTokenNonValid () throws Exception{

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("1",userToken, locationId);
        Thread.sleep(2000);

        GetMemberBenefits getMemberBenefits = serverHelper.createGetMemberBenefits(tokenTemp);

        //Send the request
        ZappServerErrorResponse getMemberBenefitsResponse = (ZappServerErrorResponse)getMemberBenefits.SendRequestByApiKey(apiKey, ZappServerErrorResponse.class);
        Assert.assertEquals("Result must be N", "N", getMemberBenefitsResponse.getResult());
        Assert.assertEquals("ErrorMessage", "One or more customers not found",getMemberBenefitsResponse.getErrorMessage());

    }

    /**
     *   C5878	Get member benefits positive test with TemporaryToken use few times
     */

    @Test
    public void testGetMemberBenefitsWithTemporaryTokenLongUseFewTimes (){

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("6000",userToken, locationId);
        GetMemberBenefits getMemberBenefits = serverHelper.createGetMemberBenefits(tokenTemp);


        for (int i=0; i<10; i++){
            //Send the request
            IServerResponse response = getMemberBenefits.SendRequestByApiKey(apiKey, GetMemberBenefitsResponse.class);
            if(response instanceof ZappServerErrorResponse ){
               log4j.info("ERROR: " + ((ZappServerErrorResponse) response).getErrorMessage());
               continue;
            }
            // Result true assertion
            GetMemberBenefitsResponse getMemberBenefitsResponse = (GetMemberBenefitsResponse) response;
            Assert.assertEquals("Result must be Y", "Y", getMemberBenefitsResponse.getResult());

        }
    }

    /**
     * C5879 Two customers when one token is not valid
     */

    @Test
    public void testGetMemberBenefitsWithTemporaryToken2customers () throws Exception{

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("10000",userToken, locationId);
        String userToken2 = serverHelper.createNewMemberGetUserToken();
        String tokenTemp2 = serverHelper.generateTokenWebViewUserToken("1",userToken2, locationId);

        GetMemberBenefits getMemberBenefits = createGetMemberBenefits2Customers(tokenTemp, tokenTemp2);
        Thread.sleep(2000);


        //Send the request
        ZappServerErrorResponse getMemberBenefitsResponse = (ZappServerErrorResponse)getMemberBenefits.SendRequestByApiKey(apiKey, ZappServerErrorResponse.class);
        Assert.assertEquals("Result must be N", "N", getMemberBenefitsResponse.getResult());
        Assert.assertEquals("ErrorMessage", "One or more customers not found",getMemberBenefitsResponse.getErrorMessage());
    }

    /**
     * 	  C5881	Get member benefits Non valid token
     */
    @Test
    public void testGetMemberBenefitsWithTemporaryTokenNonValidTokenForNonExistingMember () throws Exception{

        String tokenTemp = "1234";
        GetMemberBenefits getMemberBenefits = serverHelper.createGetMemberBenefits(tokenTemp);

        //Send the request
        ZappServerErrorResponse getMemberBenefitsResponse = (ZappServerErrorResponse)getMemberBenefits.SendRequestByApiKey(apiKey, ZappServerErrorResponse.class);
        Assert.assertEquals("Result must be N", "N", getMemberBenefitsResponse.getResult());
        Assert.assertEquals("ErrorMessage", "One or more customers not found",getMemberBenefitsResponse.getErrorMessage());

    }


    private GetMemberBenefits createGetMemberBenefits2Customers (String tokenTemp, String tokenTemp2) {

        Customer customer = new Customer();
        customer.setTemporaryToken(tokenTemp);
        GetMemberBenefits getMemberBenefits = new GetMemberBenefits();
        getMemberBenefits.getCustomers().add(customer);

        Customer customer2 = new Customer();
        customer2.setTemporaryToken(tokenTemp2);
        getMemberBenefits.getCustomers().add(customer2);

        getMemberBenefits.setReturnExtendedItems(true);


        return getMemberBenefits;

    }







}


