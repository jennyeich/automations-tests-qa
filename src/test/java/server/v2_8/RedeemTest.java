package server.v2_8;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import hub.base.categories.serverCategories.ServerRegression;
import hub.base.categories.serverCategories.serverV2_8;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.base.BaseServerTest;
import server.common.*;
import server.common.asset.AssignAsset;
import server.common.asset.conditions.AssetDateCondition;
import server.utils.RedeemErrorCodesMap;
import server.v2_8.base.IServerErrorResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Asset;
import server.v2_8.common.Customer;
import server.v2_8.common.Item;
import server.v2_8.common.RedeemItem;
import utils.KeyUtils;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static utils.StringUtils.StringToKey;


public class RedeemTest extends BaseServerTest {


    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        updateConfiguration = true;
        init();
        serverHelper.addPOSRedeemCodeBulkFromApi();
    }
    /**
     * C2470 - Redeem with Code Type = None (Auto Mark Redeem As Code Use = NO)
     */
    @Test
    public void testRedeemCodeTypeNoneAutoMarkRedeemAsCodeUseNO() throws IOException, InterruptedException {

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "None",memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();
        Key dataStoreAssetKey = StringToKey(assetKey);

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned", "Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused", "Unused", result.getProperty("RedeemState"));

        RedeemAsset redeemAsset = buildRedeemAsset(memberKey, assetKey);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);


        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) response;
        Assert.assertEquals("Result must be Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore
        result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Could not find asset with key: " + dataStoreAssetKey, result);
        Assert.assertEquals("Asset Status must be Assigned", "Redeemed", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Burned ","Burned", result.getProperty("RedeemState"));
    }

    /**
     * C2471 - Redeem with Code Type = Bulk (Auto Mark Redeem As Code Use = NO)
     */
    @Test
    public void testRedeemWithCodeTypeBulkAutoMarkRedeemAsCodeUseNO() throws IOException, InterruptedException {

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "Bulk",  memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();

        Key dataStoreAssetKey = StringToKey(assetKey);

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        RedeemAsset redeemAsset = buildRedeemAsset(memberKey, assetKey);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) response;
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Redeemed","Redeemed",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending", result2.getProperty("RedeemState"));

        //Send redeem request in order to check redeem status
        Redeem redeem = new Redeem();
        redeem.setReturnExtendedItems(true);
        RedeemItem redeemItem = new RedeemItem();
        String code = redeemAssetResponse.getCode().toString();
        redeemItem.setRedeemCode(code);
        redeem.getRedeemItems().add(redeemItem);

        //Send the redeem request
        IServerResponse response2 = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (response2 instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response2).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemResponse redeemResponse = (RedeemResponse) response2;

        String dealCode = AssignAsset.DEFAULT_DEAL_CODE;
        Assert.assertTrue("Deal code response must be equal",redeemResponse.getSimplefiedDealCodes().contains(dealCode));

        //Verify with DataStore
        Entity result3 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result3);
        Assert.assertEquals("Asset Status must be Redeemed", "Redeemed",result3.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be BenefitsRequested", "BenefitsRequested",result3.getProperty("RedeemState"));

        Key oneTime = (Key)result3.getProperty("RedeemCodeKey");
        Assert.assertNotNull("Data in the DB not null",oneTime);
        Entity resultOneTimeCode2 = dataStore.findOneByKey(oneTime);
        Assert.assertEquals("Asset OneTimeCode Status must be","Pending",resultOneTimeCode2.getProperty("Status"));

    }

    /**
     * C2472 - Redeem with Code Type = Auto Generated codes (Auto Mark Redeem As Code Use = NO)
     */
    @Test
    @Category({ServerRegression.class})
    public void testRedeemWithCodeTypeAutoAutoMarkRedeemAsCodeUseNo() throws IOException, InterruptedException {

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "Auto", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();
        Key dataStoreAssetKey = StringToKey(assetKey);

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned", "Assigned",result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        RedeemAsset redeemAsset = buildRedeemAsset(memberKey, assetKey);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) response;
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore
        Entity result1 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result1);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result1.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending",result1.getProperty("RedeemState"));


        //Send redeem request in order to check redeem status
        Redeem redeem = new Redeem();
        redeem.setReturnExtendedItems(true);
        RedeemItem redeemItem = new RedeemItem();
        redeemItem.setAssetKey(assetKey);
        redeem.getRedeemItems().add(redeemItem);

        //Send the redeem request
        IServerResponse redeemResponse = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (redeemResponse instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) redeemResponse).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemResponse redeemResponse2 = (RedeemResponse) redeemResponse;

        String dealCode = AssignAsset.DEFAULT_DEAL_CODE;
        Assert.assertTrue("Deal code response must be equal",redeemResponse2.getSimplefiedDealCodes().contains(dealCode));

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be BenefitsRequested","BenefitsRequested",result2.getProperty("RedeemState"));

    }

    /**
     * C2473 - Redeem with Code Type = None (Auto Mark Redeem As Code Use = YES)
     */
    @Test
    @Category({ServerRegression.class})
    public void testRedeemWithCodeTypeNoneMarkRedeemAsCodeUseYes() throws IOException, InterruptedException {

        //serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),true);

        //Create new customer
        JoinClubResponse newMember = createMember(); Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "None",memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();
        //Send redeem request in order to check redeem status
        Redeem redeem = new Redeem();
        redeem.setReturnExtendedItems(true);
        redeem.setMarkAsUsed(true);
        RedeemItem redeemItem = new RedeemItem();
        redeemItem.setAssetKey(assetKey);
        redeem.getRedeemItems().add(redeemItem);

        //Send the redeem request
        IServerResponse redeemResponse = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Get the redeem response
        RedeemResponse redeemAssetResponse = (RedeemResponse) redeemResponse;
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getSimplefiedDealCodes().get(0)));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Make sure the response is not Error response
        if (redeemResponse instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) redeemResponse).getErrorMessage());
        }

        Key dataStoreAssetKey = StringToKey(assetKey);

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        Assert.assertEquals("Asset Status must be Redeemed","Redeemed",result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Burned","Burned",result.getProperty("RedeemState"));

    }

    /**
     * C2474 - Redeem with Code Type = Bulk (Auto Mark Redeem As Code Use = YES)
     */
    @Test
    public void testRedeemWithCodeTypeBulkMarkRedeemAsCodeUseYes() throws IOException, InterruptedException  {

        //serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),true);

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "Bulk",memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();

        Key dataStoreAssetKey = StringToKey(assetKey);

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        RedeemAsset redeemAsset = buildRedeemAsset(memberKey, assetKey);
        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);

        //Get the redeem response
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) response;
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Verify with DataStore;
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Redeemed ","Redeemed",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending",result2.getProperty("RedeemState"));

        Key oneTime = (Key)result2.getProperty("RedeemCodeKey");
        Entity resultOneTimeCode = dataStore.findOneByKey(oneTime);
        Assert.assertNotNull("Data in the DB not null",resultOneTimeCode);
        Assert.assertEquals("Asset Status must be Pending","Pending",resultOneTimeCode.getProperty("Status"));

        //Send redeem request in order to check redeem status
        Redeem redeem = new Redeem();
        redeem.setReturnExtendedItems(true);
        redeem.setMarkAsUsed(true);
        RedeemItem redeemItem = new RedeemItem();
        redeemItem.setAssetKey(assetKey);
        redeem.getRedeemItems().add(redeemItem);

        //Send the redeem request
        IServerResponse redeemResponse = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (redeemResponse instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) redeemResponse).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemResponse redeemResponse2 = (RedeemResponse) redeemResponse;

        String dealCode = AssignAsset.DEFAULT_DEAL_CODE;
        Assert.assertTrue("Deal code response must be equal",redeemResponse2.getSimplefiedDealCodes().contains(dealCode));

        //Verify with DataStore
        Entity result3 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result3);
        Assert.assertEquals("Asset Status must be Redeemed","Redeemed", result3.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Burned","Burned",result3.getProperty("RedeemState"));

        Key oneTime2 = (Key)result3.getProperty("RedeemCodeKey");
        Assert.assertNotNull("Data in the DB not null",oneTime2);
        Entity resultOneTimeCode2 = dataStore.findOneByKey(oneTime2);
        Assert.assertEquals("Asset Status must be Pending","Pending",resultOneTimeCode2.getProperty("Status"));

    }

    /**
     * C2475 - Redeem with Code Type = Auto Generated codes (Auto Mark Redeem As Code Use = YES)
     */
    @Test
    public void testRedeemWithCodeTypeAutoMarkRedeemAsCodeUseYes() throws IOException, InterruptedException {

        //serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),true);

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "Auto", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();

        Key dataStoreAssetKey = StringToKey(assetKey);

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        RedeemAsset redeemAsset = buildRedeemAsset(memberKey, assetKey);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);

        //Get the redeem response
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) response;
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending",result2.getProperty("RedeemState"));

        //Send redeem request in order to check redeem status
        Redeem redeem = new Redeem();
        redeem.setReturnExtendedItems(true);
        redeem.setMarkAsUsed(true);
        RedeemItem redeemItem = new RedeemItem();
        redeemItem.setAssetKey(assetKey);
        redeem.getRedeemItems().add(redeemItem);

        //Send the redeem request
        IServerResponse redeemResponse = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);


        //Make sure the response is not Error response
        if (redeemResponse instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) redeemResponse).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemResponse redeemResponse2 = (RedeemResponse) redeemResponse;

        String dealCode = AssignAsset.DEFAULT_DEAL_CODE;
        Assert.assertTrue("Deal code response must be equal",redeemResponse2.getSimplefiedDealCodes().contains(dealCode));


        //Verify with DataStore
        Entity result3 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result3);
        Assert.assertEquals("Asset Status must be Redeemed","Redeemed",result3.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Burned","Burned",result3.getProperty("RedeemState"));

    }

    /**
     * C2476 - Redeem & Purchase with Code Type = Auto Generated codes (Auto Mark Redeem As Code Use  = NO)
     */
    @Test
    public void testRedeemPurchaseWithCodeTypeAutoMarkRedeemAsCodeUseNo() throws IOException, InterruptedException {

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "Auto", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();

        Key dataStoreAssetKey = StringToKey(assetKey);

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        RedeemAsset redeemAsset = buildRedeemAsset(memberKey, assetKey);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);

        //Get the redeem response
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) response;
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Verify Asset statues with DataStore;
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending",result2.getProperty("RedeemState"));


        //Send redeem request in order to check redeem status
        Redeem redeem = new Redeem();
        redeem.setReturnExtendedItems(true);
        RedeemItem redeemItem = new RedeemItem();
        String code = redeemAssetResponse.getCode().toString();
        redeemItem.setRedeemCode(code);
        redeem.getRedeemItems().add(redeemItem);

        //Send the redeem request
        IServerResponse redeemResponse = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (redeemResponse instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) redeemResponse).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemResponse redeemResponse2 = (RedeemResponse) redeemResponse;
        String dealCode = AssignAsset.DEFAULT_DEAL_CODE;
        Assert.assertTrue("Deal code response must be equal",redeemResponse2.getSimplefiedDealCodes().contains(dealCode));

        //Verify Asset statues with DataStore;
        Entity result3 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result3);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result3.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be BenefitsRequested" ,"BenefitsRequested", result3.getProperty("RedeemState"));

        // Initialize a new SubmitPurchase
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.getCustomers().add(customer);
        submitPurchase.setBranchId("2");
        submitPurchase.setPosId("12");
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        RedeemItem redeemItemPurchase = new RedeemItem();
        redeemItemPurchase.setAssetKey(assetKey);
        submitPurchase.getRedeemItems().add(redeemItemPurchase);

        Item item = new Item();
        item.setItemCode("1155");
        item.setItemCode("100");
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName("Prod1");
        item.setDepartmentCode("123");
        item.setDepartmentName("Dep1");
        item.setPrice(2000);

        ArrayList<String> tags = new ArrayList<>();
        tags.add("DEP-1");
        tags.add("tag 2");
        item.setTags(tags);

        submitPurchase.getItems().add(item);

        //Send the request
        IServerResponse responsePurchase = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is not Error response
        if (responsePurchase instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        serverHelper.validatePurchaseResponse((SubmitPurchaseResponse) responsePurchase,submitPurchase.getTotalSum(),"1");

        //Verify with DataStore;
        Entity result4 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result4);
        Assert.assertEquals("Asset Status must be Redeemed","Redeemed",result4.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Burned","Burned",result4.getProperty("RedeemState"));

    }

    /**
     * C2477 - Redeem & Purchase with Code Type = Auto Generated codes with deal code (Auto Mark Redeem As Code Use  = NO)
     */
    @Test
    public void testRedeemPurchaseWithCodeTypeAutoMarkRedeemAsCodeUseNoDealCode() throws IOException, InterruptedException {

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "Auto", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();

        Key dataStoreAssetKey = StringToKey(assetKey);

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        RedeemAsset redeemAsset = buildRedeemAsset(memberKey, assetKey);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }
        //Get the redeemAsset response
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) response;
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore;
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending", result2.getProperty("RedeemState"));

        //Send redeem request in order to check redeem status
        Redeem redeem = new Redeem();
        redeem.setReturnExtendedItems(true);
        RedeemItem redeemItem = new RedeemItem();
        String code = redeemAssetResponse.getCode().toString();
        redeemItem.setRedeemCode(code);
        redeem.getRedeemItems().add(redeemItem);

        //Send the redeem request
        IServerResponse redeemResponse = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (redeemResponse instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) redeemResponse).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemResponse redeemResponse2 = (RedeemResponse) redeemResponse;

        String dealCode = AssignAsset.DEFAULT_DEAL_CODE;
        Assert.assertTrue("Deal code response must be equal",redeemResponse2.getSimplefiedDealCodes().contains(dealCode));

        //Verify Asset statues with DataStore;
        Entity result3 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result3);
        Assert.assertEquals("Asset Status", result3.getProperty("Status"), "Assigned");
        Assert.assertEquals("Asset RedeemState", result3.getProperty("RedeemState"), "BenefitsRequested");

        // Initialize a new SubmitPurchase
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.getCustomers().add(customer);
        submitPurchase.setBranchId("2");
        submitPurchase.setPosId("12");
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        RedeemItem redeemItemPurchase = new RedeemItem();
        redeemItemPurchase.setAssetKey(assetKey);

        ArrayList<RedeemItem> redeemItemForPOS = new ArrayList<>();
        redeemItemForPOS.add(redeemItemPurchase);
        submitPurchase.setRedeemItems(redeemItemForPOS);

        Item item = new Item();
        item.setItemCode("1155");
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName("Prod1");
        item.setDepartmentCode("123");
        item.setDepartmentName("Dep1");
        item.setPrice(2000);

        ArrayList<String> tags = new ArrayList<>();
        tags.add("DEP-1");
        tags.add("tag 2");
        item.setTags(tags);

        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        submitPurchase.setItems(items);

        // Send the request
        IServerResponse responsePurchase = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is not Error response
        if (responsePurchase instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) responsePurchase).getErrorMessage());
        }

        serverHelper.validatePurchaseResponse((SubmitPurchaseResponse) responsePurchase,submitPurchase.getTotalSum(),"1");

        //Verify with DataStore;
        Entity result4 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result4);
        Assert.assertEquals("Asset Status must be Redeemed","Redeemed",result4.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Burned","Burned",result4.getProperty("RedeemState"));

    }

    /**
     * C4258 - Purchase with Code Type = Auto Generated codes (Auto Mark Redeem As Code Use  = YES)
     */
    @Test
    public void testPurchaseWithCodeTypeAutoGeneratedAutoMarkRedeemAsCodeUseYES() throws IOException, InterruptedException {

        //serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),true);

        //Create new customer
        JoinClubResponse newMember = createMember();

        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "Auto", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();

        Key dataStoreAssetKey = StringToKey(assetKey);

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused", result.getProperty("RedeemState"));

        RedeemAsset redeemAsset = buildRedeemAsset(memberKey, assetKey);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }
        //Get the redeemAsset response
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) response;
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore;
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Assigned", "Assigned", result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending",result2.getProperty("RedeemState"));

        //Send redeem request in order to check redeem status
        Redeem redeem = new Redeem();
        redeem.setReturnExtendedItems(true);
        redeem.setMarkAsUsed(true);
        RedeemItem redeemItem = new RedeemItem();
        String code = redeemAssetResponse.getCode().toString();
        redeemItem.setRedeemCode(code);
        redeem.getRedeemItems().add(redeemItem);

        //Send the redeem request
        IServerResponse redeemResponse = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (redeemResponse instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) redeemResponse).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemResponse redeemResponse2 = (RedeemResponse) redeemResponse;

        String dealCode = AssignAsset.DEFAULT_DEAL_CODE;
        Assert.assertTrue("Deal code response must be equal",redeemResponse2.getSimplefiedDealCodes().contains(dealCode));

        //Verify with DataStore;
        Entity result3 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result3);
        Assert.assertEquals("Asset Status must be Redeemed","Redeemed",result3.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Burned","Burned",result3.getProperty("RedeemState"));

        // Initialize a new SubmitPurchase
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.getCustomers().add(customer);
        submitPurchase.setBranchId("2");
        submitPurchase.setPosId("12");
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        RedeemItem redeemItemPurchase = new RedeemItem();
        redeemItemPurchase.setAssetKey(assetKey);
        ArrayList<RedeemItem> redeemItemForPOS = new ArrayList<>();
        redeemItemForPOS.add(redeemItemPurchase);
        submitPurchase.setRedeemItems(redeemItemForPOS);

        Item item = new Item();
        item.setItemCode("1155");
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName("Prod1");
        item.setDepartmentCode("123");
        item.setDepartmentName("Dep1");
        item.setPrice(2000);

        ArrayList<String> tags = new ArrayList<>();
        tags.add("DEP-1");
        tags.add("tag 2");
        item.setTags(tags);

        submitPurchase.getItems().add(item);


        // Send the request
        IServerResponse responsePurchase = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is not Error response
        if (responsePurchase instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        serverHelper.validatePurchaseResponse((SubmitPurchaseResponse) responsePurchase,submitPurchase.getTotalSum(),"1");

        //Verify with DataStore;
        Entity result4 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result4);
        Assert.assertEquals("Asset Status", result4.getProperty("Status"), "Redeemed");
        Assert.assertEquals("Asset RedeemState", result4.getProperty("RedeemState"), "Burned");

        //Verify with DataStore;
        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<Query.Filter>(Arrays.asList(
                new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"RedeemGetBenefits")))));
        List<Entity> resultUserAction = serverHelper.queryWithWait("Must find 1 UserAction: " + submitPurchase.getTransactionId(), qUserAction, 1, dataStore);
        Assert.assertTrue("UID must be inside", ((Text)resultUserAction.get(0).getProperty("Data")).getValue().contains(submitPurchase.getTransactionId()));
        Assert.assertTrue("membershipKey must be inside the data", ((Text)resultUserAction.get(0).getProperty("Data")).getValue().contains(memberKey));
    }

    /**
     * C4259 - Purchase with Code Type = Bulk (Auto Mark Redeem As Code Use  =YES)
     */
    @Test
    @Category({ServerRegression.class})
    public void testPurchaseWithCodeTypeBulkAutoMarkRedeemAsCodeUseYES() throws IOException, InterruptedException {

       // serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),true);


        //Create new customer
        JoinClubResponse newMember = createMember();

        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "Bulk", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();

        Key dataStoreAssetKey = StringToKey(assetKey);

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned", "Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused", "Unused", result.getProperty("RedeemState"));

        RedeemAsset redeemAsset = buildRedeemAsset(memberKey, assetKey);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) response;
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore;
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Redeemed", "Redeemed", result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be ", "Pending", result2.getProperty("RedeemState"));

        //Send redeem request in order to check redeem status
        Redeem redeem = new Redeem();
        redeem.setReturnExtendedItems(true);
        redeem.setMarkAsUsed(true);
        RedeemItem redeemItem = new RedeemItem();
        redeemItem.setAssetKey(assetKey);
        redeem.getRedeemItems().add(redeemItem);

        //Send the redeem request
        IServerResponse redeemResponse = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (redeemResponse instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) redeemResponse).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemResponse redeemResponse2 = (RedeemResponse) redeemResponse;

        String dealCode = AssignAsset.DEFAULT_DEAL_CODE;
        Assert.assertTrue("Deal code response must be equal",redeemResponse2.getSimplefiedDealCodes().contains(dealCode));

        //Verify with DataStore
        Entity result3 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result3);
        Assert.assertEquals("Asset Status must be Redeemed", "Redeemed",result3.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Burned", "Burned",result3.getProperty("RedeemState"));

        // Initialize a new SubmitPurchase
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.getCustomers().add(customer);
        submitPurchase.setBranchId("2");
        submitPurchase.setPosId("12");
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        RedeemItem redeemItemPurchase = new RedeemItem();
        redeemItemPurchase.setAssetKey(assetKey);
        ArrayList<RedeemItem> redeemItemForPOS = new ArrayList<>();
        redeemItemForPOS.add(redeemItemPurchase);
        submitPurchase.setRedeemItems(redeemItemForPOS);

        Item item = new Item();

        item.setItemCode("1155");
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName("Prod1");
        item.setDepartmentCode("123");
        item.setDepartmentName("Dep1");
        item.setPrice(2000);

        ArrayList<Item> items = new ArrayList<>();
        items.add(item);

        submitPurchase.setItems(items);

        // Send the request
        IServerResponse responsePurchase = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        //Make sure the response is not Error response
        if (responsePurchase instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) responsePurchase).getErrorMessage());
        }

        serverHelper.validatePurchaseResponse((SubmitPurchaseResponse) responsePurchase,submitPurchase.getTotalSum(),"1");

        //Verify with DataStore
        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<Query.Filter>(Arrays.asList(
                new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"RedeemGetBenefits")))));
        List<Entity> resultUserAction = serverHelper.queryWithWait("Must find 1 UserAction : " + submitPurchase.getTransactionId(), qUserAction, 1, dataStore);
        Assert.assertTrue("UID must be inside", ((Text)resultUserAction.get(0).getProperty("Data")).getValue().contains(submitPurchase.getTransactionId()));
        Assert.assertTrue("membershipKey must be inside the data", ((Text)resultUserAction.get(0).getProperty("Data")).getValue().contains(memberKey));
    }

    /**
     * C4290 - Redeem with Submit Purchase context
     */
    @Test
    public void testRedeemWithSubmitPurchaseContext() throws IOException, InterruptedException {

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "Bulk", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();

        Key dataStoreAssetKey = StringToKey(assetKey);

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned", "Assigned",result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused", "Unused",result.getProperty("RedeemState"));

        // Initialize a new SubmitPurchase
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        Redeem redeem = new Redeem();
        redeem.getCustomers().add(customer);
        redeem.setReturnExtendedItems(true);
        redeem.setBranchId("2");
        redeem.setPosId("12");
        redeem.setTotalSum("2000");
        redeem.setTransactionId(UUID.randomUUID().toString());
        redeem.setChainId("chainIDAutomation");
        redeem.setCashier("cashier1");

        RedeemItem redeemItemPurchase = new RedeemItem();
        redeemItemPurchase.setAssetKey(assetKey);
        ArrayList<RedeemItem> redeemItemForPOS = new ArrayList<>();
        redeemItemForPOS.add(redeemItemPurchase);
        redeem.setRedeemItems(redeemItemForPOS);

        Item item = new Item();
        item.setItemCode("1155");
        item.setQuantity(1);
        item.setAmount(1.2415);
        item.setItemName("Prod1");
        item.setDepartmentCode("123");
        item.setDepartmentName("Dep1");
        item.setPrice(2000);

        ArrayList<String> tags = new ArrayList<>();
        tags.add("DEP-1");
        tags.add("tag 2");
        item.setTags(tags);

        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        redeem.setItems(items);

        // Send the request
        IServerResponse response = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemResponse redeemResponse = (RedeemResponse) response;

        String dealCode = AssignAsset.DEFAULT_DEAL_CODE;
        Assert.assertTrue("Deal code response must be equal",redeemResponse.getSimplefiedDealCodes().contains(dealCode));

        //Verify with DataStore;
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Assigned", "Assigned", result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be BenefitsRequested", "BenefitsRequested", result2.getProperty("RedeemState"));

        //Verify with DataStore
        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, redeem.getTransactionId()),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"RedeemGetBenefits")))));
        List<Entity> resultUserAction = serverHelper.queryWithWait("Must find 1 UserAction : " + redeem.getTransactionId(), qUserAction, 1, dataStore);
        Assert.assertTrue("UID must be inside", ((Text)resultUserAction.get(0).getProperty("Data")).getValue().contains(redeem.getTransactionId()));
        Assert.assertTrue("membershipKey must be inside the data", ((Text)resultUserAction.get(0).getProperty("Data")).getValue().contains(memberKey));
    }

    /**
     * C3339 - Redeem asset from client admin mode
     * TODO: Currently asset bulk is not generated within test. Need to create a new bulk and then create a new asset from this bulk.
     */
    @Test
    public void testRedeemAssetFromClientAdminMode() throws IOException, InterruptedException {

        //Create new  member
        JoinClubResponse newMember = createMember();

        //Assign asset to member
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "Bulk",newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        Key dataStoreAssetKey = StringToKey(newAsset1.getKey());

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        //Perform redeem asset (client)
        RedeemAsset redeemAsset = buildRedeemAsset(newMember.getMembershipKey(), newAsset1.getKey());
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse)redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);

        //Get the redeemAsset response
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null", result2);
        Assert.assertEquals("Asset Status must be Redeemed","Redeemed", result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending", result2.getProperty("RedeemState"));

        //Perform client redeem (admin mode) by redeem code
        ClientRedeem clientRedeem = new ClientRedeem();
        clientRedeem.setLocationId(locationId);
        clientRedeem.setRedeemCode(redeemAssetResponse.getCode());
        ClientRedeemResponse clientRedeemResponse = (ClientRedeemResponse)clientRedeem.SendRequestByTokenAndValidateResponse(serverToken, ClientRedeemResponse.class);

        //Validate client redeem
        Key userKey = StringToKey(newMember.getUserKey());
        Key membershipKey = StringToKey(newMember.getMembershipKey());
        validateClientRedeemResponse(userKey, membershipKey, newAsset1, redeemAssetResponse.getCode(), clientRedeemResponse, "OneTimeCode");
    }


    /**
     * C3340 - Redeem asset from client admin mode invalid/incorrect redeem code
     */
    @Test
    public void testRedeemAssetFromClientAdminModeInvalidRedeemCode() throws IOException, InterruptedException {

        //Create new  member
        JoinClubResponse newMember = createMember();
        //Assign asset to member
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "Bulk", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        Key dataStoreAssetKey = StringToKey(newAsset1.getKey());

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        //Perform redeem asset (client)
        RedeemAsset redeemAsset = buildRedeemAsset(newMember.getMembershipKey(), newAsset1.getKey());
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse)redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);

        //Get the redeemAsset response
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Redeemed","Redeemed",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending", result2.getProperty("RedeemState"));

        //Perform client redeem (admin mode)
        ClientRedeem clientRedeem = new ClientRedeem();
        clientRedeem.setLocationId(locationId);
        clientRedeem.setRedeemCode(UUID.randomUUID().toString());
        IServerResponse response = clientRedeem.SendRequestByToken(serverToken, IServerResponse.class);
        if(!(response instanceof ZappServerErrorResponse)){
            Assert.fail("Expected an error!");
        }
        ZappServerErrorResponse errorResponse = (ZappServerErrorResponse) response;
        Assert.assertNotNull(errorResponse);
        Assert.assertEquals("Error message is: 'Code not found'", "Code not found",errorResponse.getRawObject().get("Error"));
        Assert.assertEquals("Result must be N", "N", errorResponse.getResult());
        Assert.assertEquals("ErrorCode is 5520", "5520",errorResponse.getErrorCode());

        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, newMember.getUserKey()),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"redeem")))));
        serverHelper.queryWithWait("Must find 0 UserAction: ", qUserAction, 0, dataStore);

        qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, newMember.getUserKey()),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"RedeemGetBenefits")))));
        serverHelper.queryWithWait("Must find 0 UserAction: ", qUserAction, 0, dataStore);
        //TODO: Currently bulk is not generated within test. Need to create a new bulk and then verify it.
    }


    /**
     * C3332
     */
    @Test
    @Category({ServerRegression.class})
    public void testRedeemAssetSendAssetToMultipleUsers() throws Exception{



        String templateResponse = AssignAsset.createAssetNoAssign("gift", "None", locationId,serverToken,null);
        for (int i=0; i<20;i++){

            //Create new  member
            JoinClubResponse newMember = createMember();


            //Build GetMemberDetails request in order to get UserKey
            Customer customer = new Customer();
            customer.setPhoneNumber(newMember.getPhoneNumber());
            GetMemberDetails getMemberDetails = new GetMemberDetails();
            getMemberDetails.getCustomers().add(customer);
            getMemberDetails.setExpandAssets(true);
            getMemberDetails.setIncludeArchivedAssets(false);

            //Send GetMemberDetails request
            IServerResponse response = getMemberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);

            // make sure the response is not Error response
            if (response instanceof ZappServerErrorResponse) {
                Assert.assertTrue(((ZappServerErrorResponse) response).getErrorMessage(), false);
            }


            //Build assign asset via InvokeEventBasedActionRule
            InvokeEventBasedActionRuleBuilder assignRequest = new InvokeEventBasedActionRuleBuilder();
            InvokeEventBasedActionRule request = assignRequest.buildAssignAssetRequest(newMember.getMembershipKey(), newMember.getUserKey(), templateResponse, locationId);
            //Send assign asset via InvokeEventBasedActionRule
            IServerResponse basedActionResponse = request.SendRequestByToken(serverToken, Response.class);

            // make sure the response is not Error response
            if (basedActionResponse instanceof ZappServerErrorResponse) {
                Assert.assertTrue(((ZappServerErrorResponse) basedActionResponse).getErrorMessage(), false);
            }


            //Validate userAction in datastore
            checkDataStoreUpdatedWithAssetSource(StringToKey(newMember.getUserKey()));


        }

    }

    /**
     * C3332 additional test for 3332, performs the send asset with new asset each time
     */
    @Test
    public void testRedeemAssetSendAssetToMultipleUsers2() throws Exception {


        for (int i=0; i<20;i++){


            //Create new  member
            JoinClubResponse newMember = createMember();

            //Build GetMemberDetails request in order to get UserKey
            Customer customer = new Customer();
            customer.setPhoneNumber(newMember.getPhoneNumber());
            GetMemberDetails getMemberDetails = new GetMemberDetails();
            getMemberDetails.getCustomers().add(customer);
            getMemberDetails.setExpandAssets(true);
            getMemberDetails.setIncludeArchivedAssets(false);

            //Send GetMemberDetails request
            IServerResponse response = getMemberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);

            // make sure the response is not Error response
            if (response instanceof ZappServerErrorResponse) {
                Assert.assertTrue(((ZappServerErrorResponse) response).getErrorMessage(), false);
            }


            Key userKey = StringToKey(newMember.getUserKey());

            //Assign asset to member
            AssignAsset.assignAssetToMember("gift", "Bulk", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());
            checkDataStoreUpdatedWithAssetSource(userKey);

        }

    }

    private void checkDataStoreUpdatedWithAssetSource(Key userKey) throws Exception {

        //Validate userAction in datastore
        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, userKey),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"receivedasset")))));
        List<Entity> res = serverHelper.queryWithWait("Must find 1 UserAction: ", qUserAction, 1, dataStore);
        Text t = ((Text)res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());
        Assert.assertTrue("Data must contain the assetType: ", t.getValue().contains("AssetType=\"gift\""));
        Assert.assertTrue("Data must contain the asset source: ", t.getValue().contains("Source=\"Hub\""));

        //check the source of the data:
        Assert.assertEquals("Source is hub","Hub",res.get(0).getProperty("Source"));

    }




    /**
     * C3341 - Redeem asset from client admin mode invalid asset (already used)
     * TODO: currently there is a bug in server that failing this test
     * (ComparisonFailure: Error message is: 'Asset Already Redeemed' , Expected :Asset Already Redeemed, Actual   :Code not found)
     */
    @Test
    public void testRedeemAssetFromClientAdminModeInvalidAsset() throws IOException, InterruptedException {


        //Create new  member
        JoinClubResponse newMember = createMember();


        //Assign asset to member
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "Bulk", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        Key dataStoreAssetKey = StringToKey(newAsset1.getKey());

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        //Perform redeem asset (client)
        RedeemAsset redeemAsset = buildRedeemAsset(newMember.getMembershipKey(), newAsset1.getKey());
        RedeemAssetResponse redeemAssetResponse1 = (RedeemAssetResponse)redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);

        //Get the redeemAsset response
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse1.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse1.getResult());

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Redeemed","Redeemed",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending", result2.getProperty("RedeemState"));

        //Perform client redeem (admin mode)
        ClientRedeem clientRedeem = new ClientRedeem();
        clientRedeem.setLocationId(locationId);
        clientRedeem.setRedeemCode(redeemAssetResponse1.getCode());
        ClientRedeemResponse clientRedeemResponse = (ClientRedeemResponse)clientRedeem.SendRequestByTokenAndValidateResponse(serverToken, ClientRedeemResponse.class);
        Key userKey = StringToKey(newMember.getUserKey());
        Key membershipKey = StringToKey(newMember.getMembershipKey());
        validateClientRedeemResponse(userKey, membershipKey, newAsset1, redeemAssetResponse1.getCode(), clientRedeemResponse, "OneTimeCode");

        //Create a new asset
        Asset newAssetNotGoingToBeBurned = AssignAsset.assignAssetToMember("gift", "Bulk", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());

        //Perform redeem asset (client)
        redeemAsset = new RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(newAssetNotGoingToBeBurned.getKey());
        redeemAsset.setMembershipKey(newMember.getMembershipKey());
        redeemAsset.setToken(serverToken);
        RedeemAssetResponse redeemAssetResponse2 = (RedeemAssetResponse) redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);

        //Get the redeemAsset response
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse2.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse2.getResult());

        //Perform client redeem (admin mode) with the asset that was already burned and not the new asset that was redeemed
        clientRedeem.setRedeemCode(redeemAssetResponse1.getCode());
        IServerResponse response = clientRedeem.SendRequestByToken(serverToken, IServerResponse.class);
        if(!(response instanceof ZappServerErrorResponse)){
            Assert.fail("Expected an error!");
        }
        ZappServerErrorResponse errorResponse = (ZappServerErrorResponse) response;
        Assert.assertNotNull(errorResponse);
        Assert.assertEquals("Error message is: 'Code not found", "Code not found",errorResponse.getRawObject().get("Error"));
        Assert.assertEquals("ErrorCode is 5520", "5520",errorResponse.getErrorCode());

        //Validate userAction in datastore
        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, userKey),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"redeem")))));
        serverHelper.queryWithWait("Must find 1 UserAction: ", qUserAction, 1, dataStore); // just validation that only 1 redeem still appears

        qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, userKey),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"RedeemGetBenefits")))));
        List<Entity> resultUserAction = serverHelper.queryWithWait("Must find 1 UserAction: ", qUserAction, 1, dataStore);
        validateClientRedeemResponseMap(membershipKey, newAsset1, resultUserAction.get(0));

        //Validate OneTimeCode in datastore
        qUserAction = new Query("OneTimeCode");
        qUserAction.setFilter(new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, userKey));
        resultUserAction = serverHelper.queryWithWait("Must find 2 OneTimeCode", qUserAction, 2, dataStore);
        for(Entity entity : resultUserAction){
            if(entity.getProperty("Code").equals(redeemAssetResponse1.getCode())){
                Assert.assertEquals("Status should be 'Used", "Used", entity.getProperty("Status").toString());
                validateClientRedeemResponseMap(membershipKey, newAsset1, entity);
            }
            else{
                Assert.assertEquals("Status should be 'Pending", "Pending", entity.getProperty("Status").toString());
                validateClientRedeemResponseMap(membershipKey, newAssetNotGoingToBeBurned, entity);
            }
        }
    }


    /**
     * C3342 - Redeem asset from client admin mode with an asset that has a date condition
     * TODO: Currently asset bulk is not generated within test. Need to create a new bulk and then create a new asset from this bulk.
     */
    @Test
    public void testRedeemAssetWithConditionFromClientAdminMode() throws IOException, InterruptedException {

        //Create new  member
        JoinClubResponse newMember = createMember();

        //Assign asset to member
        AssetDateCondition dateCondition = serverHelper.getValidDatesForAsset();
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "Bulk", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber(), dateCondition);

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        Key dataStoreAssetKey = StringToKey(newAsset1.getKey());

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        //Perform redeem asset (client)
        RedeemAsset redeemAsset = buildRedeemAsset(newMember.getMembershipKey(), newAsset1.getKey());
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse)redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);

        //Get the redeemAsset response
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null", result2);
        Assert.assertEquals("Asset Status must be Redeemed","Redeemed", result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending", result2.getProperty("RedeemState"));

        //Perform client redeem (admin mode) by redeem code
        ClientRedeem clientRedeem = new ClientRedeem();
        clientRedeem.setLocationId(locationId);
        clientRedeem.setRedeemCode(redeemAssetResponse.getCode());
        ClientRedeemResponse clientRedeemResponse = (ClientRedeemResponse)clientRedeem.SendRequestByTokenAndValidateResponse(serverToken, ClientRedeemResponse.class);
        Key userKey = StringToKey(newMember.getUserKey());
        Key membershipKey = StringToKey(newMember.getMembershipKey());
        validateClientRedeemResponse(userKey, membershipKey, newAsset1, redeemAssetResponse.getCode(), clientRedeemResponse, "OneTimeCode");
        //TODO: Currently bulk is not generated within test. Need to create a new bulk and then verify it.

    }

    /**
     * Negative test to C3342 - Redeem asset from client admin mode with an asset that has INVALID date condition
     * TODO: Currently asset bulk is not generated within test. Need to create a new bulk and then create a new asset from this bulk.
     * TODO: currently there is a bug in the server that failing this test (Expected an error but getting a success response) - CNP-6018
     */
//    @Test
//    @Ignore("Known Bug: CNP-6018")
    public void testRedeemAssetWithInvalidConditionFromClientAdminMode() throws IOException, InterruptedException {

        //Create new  member
        JoinClubResponse newMember = createMember();

        //Assign asset to member
        AssetDateCondition invalidDatesForAsset = serverHelper.getInvalidDatesForAsset();
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "Bulk",newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber(), invalidDatesForAsset);

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        Key dataStoreAssetKey = StringToKey(newAsset1.getKey());

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        //Perform redeem asset (client)
        RedeemAsset redeemAsset = buildRedeemAsset(newMember.getMembershipKey(), newAsset1.getKey());
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse)redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);

        //Get the redeemAsset response
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Redeemed","Redeemed",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending", result2.getProperty("RedeemState"));

        //Perform client redeem (admin mode) by redeem code
        ClientRedeem clientRedeem = new ClientRedeem();
        clientRedeem.setLocationId(locationId);
        clientRedeem.setRedeemCode(redeemAssetResponse.getCode());
        IServerResponse response = clientRedeem.SendRequestByToken(serverToken, ClientRedeemResponse.class);
        if(!(response instanceof ZappServerErrorResponse)){
            Assert.fail("Expected an error!");//TODO: currently a bug - the server returns Result = Y with an error message - "Violation of asset conditions"
        }
        ZappServerErrorResponse errorResponse = (ZappServerErrorResponse) response;
        Assert.assertNotNull(errorResponse);
        Assert.assertEquals("Error message is: 'Violation of asset conditions'", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION),errorResponse.getRawObject().get("Error"));
        Assert.assertEquals("ErrorCode is 5523", "5523",errorResponse.getErrorCode());

        //Validate userAction in datastore
        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, newMember.getUserKey()),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"redeem")))));
        serverHelper.queryWithWait("Must find 0 UserAction: ", qUserAction, 0, dataStore); // just validation that only 1 redeem still appears

        qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, newMember.getUserKey()),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"RedeemGetBenefits")))));
        serverHelper.queryWithWait("Must find 0 UserAction: ", qUserAction, 0, dataStore);

        //Validate OneTimeCode in datastore
        qUserAction = new Query("OneTimeCode");
        qUserAction.setFilter(new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, newMember.getUserKey()));
        serverHelper.queryWithWait("Must find 0 OneTimeCode", qUserAction, 0, dataStore);
        //TODO: Currently bulk is not generated within test. Need to create a new bulk and then verify it.
    }

    /**
     * C3343 - Redeem asset from client admin mode- Auto Generated Code for asset
     * TODO: there is a bug ClientRedeemResponse should return LocationCode data - CNP-6018
     */
//    @Test
//    @Ignore("Known Bug: CNP-6018")
    public void testRedeemAutoGeneratedAssetFromClientAdminMode() throws IOException, InterruptedException {

        //Create new  member
        JoinClubResponse newMember = createMember();

        //Assign asset to member
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "Auto", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        Key dataStoreAssetKey = StringToKey(newAsset1.getKey());

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        //Perform redeem asset (client)
        RedeemAsset redeemAsset = buildRedeemAsset(newMember.getMembershipKey(), newAsset1.getKey());
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse)redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);

        //Get the redeemAsset response
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending", result2.getProperty("RedeemState"));

        //Perform client redeem (admin mode)
        ClientRedeem clientRedeem = new ClientRedeem();
        clientRedeem.setLocationId(locationId);
        clientRedeem.setRedeemCode(redeemAssetResponse.getCode());
        ClientRedeemResponse clientRedeemResponse = (ClientRedeemResponse)clientRedeem.SendRequestByTokenAndValidateResponse(serverToken, ClientRedeemResponse.class);
        Key userKey = StringToKey(newMember.getUserKey());
        Key membershipKey = StringToKey(newMember.getMembershipKey());
        validateClientRedeemResponse(userKey, membershipKey, newAsset1, redeemAssetResponse.getCode(), clientRedeemResponse, "ShortAssetCode");
        //TODO: Currently bulk is not generated within test. Need to create a new bulk and then verify it.

    }


    /**
     * C3344 - Redeem asset from client admin mode invalid incorrect code
     */
    @Test
    public void testRedeemAutoGeneratedAssetFromClientAdminModeInvalidRedeemCode() throws IOException, InterruptedException {

        //Create new  member
        JoinClubResponse newMember = createMember();

        //Assign asset to member
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "Auto",newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        Key dataStoreAssetKey = StringToKey(newAsset1.getKey());

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        //Perform redeem asset (client)
        RedeemAsset redeemAsset = buildRedeemAsset(newMember.getMembershipKey(), newAsset1.getKey());
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse)redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);

        //Get the redeemAsset response
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending", result2.getProperty("RedeemState"));

        //Perform client redeem (admin mode)
        ClientRedeem clientRedeem = new ClientRedeem();
        clientRedeem.setLocationId(locationId);
        clientRedeem.setRedeemCode("someWrongCode");
        IServerResponse response = clientRedeem.SendRequestByToken(serverToken, ClientRedeemResponse.class);
        if(!(response instanceof ZappServerErrorResponse)){
            Assert.fail("Expected an error!");
        }
        ZappServerErrorResponse errorResponse = (ZappServerErrorResponse) response;
        Assert.assertNotNull(errorResponse);
        Assert.assertEquals("Error message is: 'Code not found'", "Code not found",errorResponse.getRawObject().get("Error"));
        Assert.assertEquals("Result must be N", "N", errorResponse.getResult());
        Assert.assertEquals("ErrorCode is 5520", "5520",errorResponse.getErrorCode());

        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, newMember.getUserKey()),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"redeem")))));
        List<Entity> resultUserAction = serverHelper.queryWithWait("Must find 0 UserAction: ", qUserAction, 0, dataStore);

        qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, newMember.getUserKey()),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"RedeemGetBenefits")))));
        resultUserAction = serverHelper.queryWithWait("Must find 0 UserAction: ", qUserAction, 0, dataStore);

        //TODO: Currently bulk is not generated within test. Need to create a new bulk and then verify it.
    }


    /**
     * C3345 - Redeem auto generated asset from client admin mode invalid asset (already used)
     * TODO: currently a bug in the server- Should be 1 RedeemGetBenefits.
     * TODO: another bug - ClientRedeemResponse should return LocationCode - CNP-6018
     */
//    @Test
//    @Ignore("Known Bug: CNP-6018")
    public void testRedeemAutoGeneratedAssetFromClientAdminModeInvalidAsset() throws IOException, InterruptedException {

        //Create new  member
        JoinClubResponse newMember = createMember();
        //Assign asset to member
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "Auto",newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        Key dataStoreAssetKey = StringToKey(newAsset1.getKey());

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        //Perform redeem asset (client)
        RedeemAsset redeemAsset = buildRedeemAsset(newMember.getMembershipKey(), newAsset1.getKey());
        RedeemAssetResponse redeemAssetResponse1 = (RedeemAssetResponse)redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);

        //Get the redeemAsset response
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse1.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse1.getResult());

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending", result2.getProperty("RedeemState"));

        //Perform client redeem (admin mode)
        ClientRedeem clientRedeem = new ClientRedeem();
        clientRedeem.setLocationId(locationId);
        clientRedeem.setRedeemCode(redeemAssetResponse1.getCode());
        ClientRedeemResponse clientRedeemResponse = (ClientRedeemResponse)clientRedeem.SendRequestByTokenAndValidateResponse(serverToken, ClientRedeemResponse.class);
        Key userKey = StringToKey(newMember.getUserKey());
        Key membershipKey = StringToKey(newMember.getMembershipKey());
        validateClientRedeemResponse(userKey, membershipKey, newAsset1, redeemAssetResponse1.getCode(), clientRedeemResponse, "ShortAssetCode");

        //Create a new asset
        Asset newAssetNotGoingToBeBurned = AssignAsset.assignAssetToMember("gift", "Auto", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());

        //Perform redeem asset (client)
        redeemAsset = new RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(newAssetNotGoingToBeBurned.getKey());
        redeemAsset.setMembershipKey(newMember.getMembershipKey());
        redeemAsset.setToken(serverToken);
        RedeemAssetResponse redeemAssetResponse2 = (RedeemAssetResponse) redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);

        //Get the redeemAsset response
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse2.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse2.getResult());

        //Perform client redeem (admin mode) with the asset that was burned
        clientRedeem.setRedeemCode(redeemAssetResponse1.getCode());
        IServerResponse response = clientRedeem.SendRequestByToken(serverToken, ClientRedeemResponse.class);

        if(!(response instanceof ZappServerErrorResponse)){
            Assert.fail("Expected an error!");
        }
        ZappServerErrorResponse errorResponse = (ZappServerErrorResponse) response;
        Assert.assertNotNull(errorResponse);
        Assert.assertEquals("Error message is: 'Asset Already Redeemed'", "Asset Already Redeemed",errorResponse.getRawObject().get("Error"));
        Assert.assertEquals("ErrorCode is 5501", "5501",errorResponse.getErrorCode());

        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, userKey),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"redeem")))));
        serverHelper.queryWithWait("Must find 1 UserAction: ", qUserAction, 1, dataStore); // just validation that only 1 redeem still appears

        qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, userKey),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"RedeemGetBenefits")))));
        List<Entity> resultUserAction = serverHelper.queryWithWait("Must find 1 UserAction: ", qUserAction, 1, dataStore);// TODO: currently a bug - Should be 1 RedeemGetBenefits
        validateClientRedeemResponseMap(membershipKey, newAsset1, resultUserAction.get(0));
    }


    /**
     * C3346 - Redeem asset from client admin mode- Auto Generated Code for asset with date condition
     * TODO: bug in server causes this test to fail (ClientRedeemResponse should return LocationCode data) - CNP-6018
     */
//    @Test
//    @Ignore("Known Bug: CNP-6018")
    public void testRedeemAutoGeneratedAssetWithConditionFromClientAdminMode() throws IOException, InterruptedException {

        //Create new  member
        JoinClubResponse newMember = createMember();
        AssetDateCondition validDatesForAsset = serverHelper.getValidDatesForAsset();
        //Assign asset to member
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "Auto",newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber(), validDatesForAsset);

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        Key dataStoreAssetKey = StringToKey(newAsset1.getKey());

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        //Perform redeem asset (client)
        RedeemAsset redeemAsset = buildRedeemAsset(newMember.getMembershipKey(), newAsset1.getKey());
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse)redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);

        //Get the redeemAsset response
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending", result2.getProperty("RedeemState"));

        //Perform client redeem (admin mode)
        ClientRedeem clientRedeem = new ClientRedeem();
        clientRedeem.setLocationId(locationId);
        clientRedeem.setRedeemCode(redeemAssetResponse.getCode());
        ClientRedeemResponse clientRedeemResponse = (ClientRedeemResponse)clientRedeem.SendRequestByTokenAndValidateResponse(serverToken, ClientRedeemResponse.class);
        Key userKey = StringToKey(newMember.getUserKey());
        Key membershipKey = StringToKey(newMember.getMembershipKey());
        validateClientRedeemResponse(userKey, membershipKey, newAsset1, redeemAssetResponse.getCode(), clientRedeemResponse, "ShortAssetCode");
        //TODO: Currently bulk is not generated within test. Need to create a new bulk and then verify it.

    }


    /**
     * Negative test to C3346 - Redeem asset from client admin mode- Auto Generated Code for asset with INVALID date condition
     * TODO: Currently asset bulk is not generated within test. Need to create a new bulk and then create a new asset from this bulk.
     */
    @Test
    public void testRedeemAutoGeneratedAssetWithInvalidConditionFromClientAdminMode() throws IOException, InterruptedException {

        //Create new  member
        JoinClubResponse newMember = createMember();       //Assign asset to member
        AssetDateCondition invalidDatesForAsset = serverHelper.getInvalidDatesForAsset();
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "Auto", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber(), invalidDatesForAsset);

        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);

        Key dataStoreAssetKey = StringToKey(newAsset1.getKey());

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        //Perform redeem asset (client)
        RedeemAsset redeemAsset = buildRedeemAsset(newMember.getMembershipKey(), newAsset1.getKey());
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse)redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);

        //Get the redeemAsset response
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null",result2);
        Assert.assertEquals("Asset Status must be Assigned","Assigned",result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Pending", result2.getProperty("RedeemState"));

        //Perform client redeem (admin mode) by redeem code
        ClientRedeem clientRedeem = new ClientRedeem();
        clientRedeem.setLocationId(locationId);
        clientRedeem.setRedeemCode(redeemAssetResponse.getCode());
        IServerResponse response = clientRedeem.SendRequestByToken(serverToken, ClientRedeemResponse.class);
        if(!(response instanceof ZappServerErrorResponse)){
            Assert.fail("Expected an error!");
        }
        ZappServerErrorResponse errorResponse = (ZappServerErrorResponse) response;
        Assert.assertNotNull(errorResponse);
        Assert.assertEquals("Error message is: 'Violation of asset conditions'", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.VIOLATION_OF_ASSET_CONDITION),errorResponse.getRawObject().get("Error"));
        Assert.assertEquals("ErrorCode is 5523", "5523",errorResponse.getErrorCode());

        //Validate userAction in datastore
        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, newMember.getUserKey()),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"redeem")))));
        serverHelper.queryWithWait("Must find 0 UserAction: ", qUserAction, 0, dataStore);

        qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, newMember.getUserKey()),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"RedeemGetBenefits")))));
        serverHelper.queryWithWait("Must find 0 UserAction: ", qUserAction, 0, dataStore);

        //Validate OneTimeCode in datastore
        qUserAction = new Query("OneTimeCode");
        qUserAction.setFilter(new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, newMember.getUserKey()));
        serverHelper.queryWithWait("Must find 0 OneTimeCode", qUserAction, 0, dataStore);
        //TODO: Currently bulk is not generated within test. Need to create a new bulk and then verify it.
    }

    private void validateClientRedeemResponse(Key userKey, Key membershipKey, Asset asset, String redeemCode, ClientRedeemResponse clientRedeemResponse, String operation) throws InterruptedException {

        Assert.assertNotNull("ClientRedeemResponse should return LocationCode data", clientRedeemResponse.getLocationCode()); // TODO: currently a bug for auto generated assests - different ClientRedeemResponse
        Assert.assertNotNull("ClientRedeemResponse should return asset result", clientRedeemResponse.getRedeemAssetResult());
        Assert.assertEquals("Status should be 'Used'", "Used", clientRedeemResponse.getLocationCode().getStatus());
        Assert.assertEquals("Asset key should be 'Used'", asset.getKey(), clientRedeemResponse.getLocationCode().getAssetKey());
        Assert.assertEquals("Location code should match redeem response", redeemCode, clientRedeemResponse.getLocationCode().getCode());
        Assert.assertEquals("Result should be Y", "Y", clientRedeemResponse.getResult());
        Assert.assertEquals("getRedeemAssetResult should be Y", "Y", clientRedeemResponse.getRedeemAssetResult().getResult());

        validateClientRedeemInDataStore(userKey, membershipKey, asset, redeemCode, operation);


    }

    private void validateClientRedeemInDataStore(Key userKey, Key membershipKey, Asset asset, String redeemCode, String operation) throws InterruptedException {

        //Verify 'redeem' with DataStore;
        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, userKey),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"redeem")))));
        List<Entity> resultUserAction = serverHelper.queryWithWait("Must find 1 UserAction: " + asset.getKey(), qUserAction, 1, dataStore);
        Entity entity = resultUserAction.get(0);

        validateClientRedeemResponseMap(membershipKey, asset, entity);
        Assert.assertEquals("Operation Id should match '" + operation + "'", operation, entity.getProperty("Operation"));
        Assert.assertEquals("Session token should match", serverToken, entity.getProperty("SessionToken"));
        Assert.assertTrue("'AdminMode' should be in data", ((Text)entity.getProperty("Data")).getValue().contains("AdminMode"));

        //Verify 'RedeemGetBenefits' with DataStore;
        qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, new ArrayList<>(Arrays.asList(
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, userKey),
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL,"RedeemGetBenefits")))));
        resultUserAction = serverHelper.queryWithWait("Must find 1 UserAction: " + asset.getKey(), qUserAction, 1, dataStore);
        entity = resultUserAction.get(0);
        validateClientRedeemResponseMap(membershipKey, asset, entity);
        Assert.assertEquals("Operation Id should match '" + operation + "'", operation, entity.getProperty("Operation"));
        Assert.assertEquals("Session token should match", serverToken, entity.getProperty("SessionToken"));
        Assert.assertTrue("'AdminMode' should be in data", ((Text)entity.getProperty("Data")).getValue().contains("AdminMode"));

        //Verify 'Asset' is burned with DataStore;
        qUserAction = new Query("Asset");
        qUserAction.setFilter(new Query.FilterPredicate("RedeemingMembershipKey", Query.FilterOperator.EQUAL, membershipKey));
        resultUserAction = serverHelper.queryWithWait("Must find 1 Asset: " + asset.getKey(), qUserAction, 1, dataStore);
        entity = resultUserAction.get(0);
        Assert.assertEquals("Asset status should be redeemed", "Redeemed", entity.getProperty("Status"));
        Assert.assertEquals("Asset should be archived", true, entity.getProperty("Archived"));
        Assert.assertEquals("Asset RedeemState should be Burned", "Burned", entity.getProperty("RedeemState"));

        //Verify 'OneTimeCode' with DataStore;
        if(operation.equals("OneTimeCode")){
            qUserAction = new Query("OneTimeCode");
            qUserAction.setFilter(new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, userKey));
            resultUserAction = serverHelper.queryWithWait("Must find 1 OneTimeCode", qUserAction, 1, dataStore);
            entity = resultUserAction.get(0);
            validateClientRedeemResponseMap(membershipKey, asset, entity);
            Assert.assertEquals("Redeem code should be the same", redeemCode, entity.getProperty("Code"));
            Assert.assertEquals("Redeem code length should match the 'Length' value", redeemCode.length(), Integer.parseInt(entity.getProperty("Length").toString()));
            Assert.assertEquals("Location id must match", locationId, entity.getProperty("BusinessID"));
            Assert.assertEquals("Status should be 'Used", "Used", entity.getProperty("Status").toString());
            /*
            TODO: add bulk related fields to test when bulk will be created within test
            1.BulkID - the correct Bulk
            7.Order - asset order in the bulk
            9.Tag - asset tag from the hub
            10.TemplateKey - the correct tamplate key (click on it in order to verify if the correct template)
             */
        }
    }

    private void validateClientRedeemResponseMap(Key membershipKey, Asset asset, Entity resultEntity) {

        //Check asset key
        Assert.assertTrue("Asset key should be A Key object", resultEntity.getProperty("AssetKey") instanceof Key);
        Key assetKeyFromUserAction = (Key) resultEntity.getProperty("AssetKey");
        Key assetKey = StringToKey(asset.getKey());
        Assert.assertEquals("Asset key should match", assetKey.getId() ,assetKeyFromUserAction.getId());

        //Check membership key
        Assert.assertTrue("Membership should be A Key object", resultEntity.getProperty("MembershipKey") instanceof Key);
        Key membershipKeyFromUserAction = (Key) resultEntity.getProperty("MembershipKey");
        Assert.assertEquals("Membership key should match", membershipKey.getId(), membershipKeyFromUserAction.getId());

        //Check locationId
        Assert.assertEquals("Location id must match", locationId, resultEntity.getProperty("LocationID").toString());
    }

    //C3349
    @Test
    @Category(serverV2_8.class)
    public void testRedeemFromPOS() throws IOException, InterruptedException {

        //Assign asset to member
        //serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(), true);

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "Auto",memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();
        Key dataStoreAssetKey = StringToKey(assetKey);

        //Redeem asset
        RedeemAsset redeemAsset = buildRedeemAsset(memberKey, assetKey);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Get the redeemAsset response
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) response;
        Assert.assertFalse("Response code must not be empty!", StringUtils.isEmpty(redeemAssetResponse.getCode()));
        Assert.assertEquals("Redeem result = Y", "Y", redeemAssetResponse.getResult());

        //Send redeem request in order to check redeem status
        RedeemItem redeemItem = new RedeemItem();
        String code = redeemAssetResponse.getCode().toString();
        redeemItem.setRedeemCode(code);

        Redeem redeem = new Redeem();
        redeem.setReturnExtendedItems(true);
        redeem.setMarkAsUsed(true);
        redeem.getRedeemItems().add(redeemItem);

        //Send the redeem request
        IServerResponse response2 = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is not Error response
        if (response2 instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response2).getErrorMessage());
        }
        Thread.sleep(3000);
        //Verify with DataStore
        Entity result3 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null", result3);
        Assert.assertEquals("Asset Status must be Redeemed", "Redeemed", result3.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Burned", "Burned", result3.getProperty("RedeemState"));

        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action",Query.FilterOperator.EQUAL, "redeem"),
                new Query.FilterPredicate("AssetKey",Query.FilterOperator.EQUAL, dataStoreAssetKey))));

        List<Entity> resultUserAction = dataStore.findByQuery(qUserAction);
        Assert.assertEquals("Asset Source must be POS", "POS", resultUserAction.get(0).getProperty("Source"));
    }



    //C3350
    @Test
    public void testRedeemFromClient() throws IOException, InterruptedException {

        String phoneMember = String.valueOf(System.currentTimeMillis());
        //signOn and get the user token
        String userToken = signOn();
        log4j.info("userToken: " + userToken);
        //join the club with the user token
        String memKey = serverHelper.joinClubWithUserToken(phoneMember, userToken); //returns the membershipKey

        UpdatedFields updatedFields = new UpdatedFields();
        updatedFields.setPhoneNumber(phoneMember);
        UpdateMembership updateMembership = new UpdateMembership();
        updateMembership.setLocationID(locationId);
        updateMembership.setMembershipKey(memKey);
        updateMembership.setToken(serverToken);
        updateMembership.setUpdatedFields(updatedFields);

        IServerResponse response = updateMembership.sendRequestByTokenAndLocation(userToken, locationId, UpdateMembershipResponse.class);
        log4j.info("updatemembership request send" + response.getStatusCode());

        //Assign asset to member
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "None", memKey, apiKey, locationId, serverToken, phoneMember);
        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);
        Key dataStoreAssetKey = StringToKey(newAsset1.getKey());
        log4j.info("Asset assigned to user");

        //Verify with DataStore
        Entity result = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Status in the DB not null",result);
        Assert.assertEquals("Asset Status must be Assigned","Assigned", result.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Unused","Unused",result.getProperty("RedeemState"));

        //Perform redeem asset (client)
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(newAsset1.getKey());
        redeemAsset.setMembershipKey(memKey);
        redeemAsset.setToken(userToken);
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse)redeemAsset.SendRequestByTokenAndValidateResponse(serverToken, RedeemAssetResponse.class);

        //Get the redeemAsset response
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

        //Verify with DataStore
        Entity result2 = dataStore.findOneByKey(dataStoreAssetKey);
        Assert.assertNotNull("Data in the DB not null", result2);
        Assert.assertEquals("Asset Status must be Redeemed","Redeemed", result2.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Pending","Burned", result2.getProperty("RedeemState"));


        Query qUserAction = new Query("UserAction");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action",Query.FilterOperator.EQUAL, "RedeemAttempt"),
                new Query.FilterPredicate("AssetKey",Query.FilterOperator.EQUAL, dataStoreAssetKey))));

        List<Entity> resultUserAction = serverHelper.queryWithWait("",qUserAction,1,dataStore);
        Assert.assertEquals("The source must be client", "Client", resultUserAction.get(0).getProperty("Source"));
    }

    private void performAnActionAttemptRedeemAsset(String membershipKey,String assetKey) {
        RedeemAsset redeemAsset = buildRedeemAsset(membershipKey, assetKey);

        //Send the redeemAsset request
        IServerResponse response = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);


        //Make sure the response is not Error response
        if (response instanceof IServerErrorResponse) {
            Assert.fail(((IServerErrorResponse) response).getErrorMessage());
        }
    }


    //Simulates the sign on from client device
    private String signOn() {

        SignOn signOn = new SignOn();
        signOn.setDeviceID( getRandomId());
        signOn.setInstallationID( getRandomId());

        IServerResponse response1 = signOn.sendRequest(SignOnResponse.class);

        // make sure the response is not Error response
        if (response1 instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response1).getErrorMessage());
        }
        log4j.info("SignOn with device and get user token..");
        SignOnResponse resp = (SignOnResponse) response1;
        String token = resp.getToken();
        log4j.info("userToken: " + token);
        return token;
    }




    //C3651
    @Test
    public void testAllowAssetToBeDisabledRedeemActiveFromHub() throws IOException, InterruptedException {

        JoinClubResponse newMember = createMember();

        //Assign asset to member
        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(), false);
        Asset asset = AssignAsset.assignAssetToMember("gift", "Bulk", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();
        String assetName = asset.getName();
        Key dataStoreAssetKey = StringToKey(assetKey);

        //Redeem asset - hub
        InvokeEventBasedActionRuleBuilder invokeEventBasedActionRuleBuilder = new InvokeEventBasedActionRuleBuilder();
        server.common.InvokeEventBasedActionRule invokeEventBasedActionRule = invokeEventBasedActionRuleBuilder.buildRedeemAssetRequest(newMember.getMembershipKey(), newMember.getUserKey(), locationId,assetKey);
        IServerResponse response = invokeEventBasedActionRule.SendRequestByToken(serverToken, Response.class);

        //Make sure the response is not Error response
        if (response instanceof IServerErrorResponse) {
            Assert.fail(((IServerErrorResponse) response).getErrorMessage());
        }

        Query qUserAction = new Query("Asset");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Name",Query.FilterOperator.EQUAL, assetName),
                new Query.FilterPredicate("Status",Query.FilterOperator.EQUAL, "Template"))));

        List<Entity> resultUserAction = dataStore.findByQuery(qUserAction);
        Assert.assertEquals("Asset Active  must be true", true, resultUserAction.get(0).getProperty("Active"));

        //wait 10 sec to allow asset become redeemed
        Thread.sleep(20000);

        Entity result1 = serverHelper.findByKeyWithWait("Data in the DB not null",dataStoreAssetKey, dataStore);
        Assert.assertEquals("Asset Status must be Redeemed", "Redeemed", result1.getProperty("Status"));
        Assert.assertEquals("Asset RedeemState must be Burned", "Burned", result1.getProperty("RedeemState"));

    }

    //C3652
    @Test
    @Category({ServerRegression.class})
    public void testAllowAssetToBeDisabledCantRedeemInactiveAssetFromHub() throws IOException, InterruptedException {

        //Assign asset to member
        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(), false);

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "None",memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();
        String assetName = asset.getName();
        Key dataStoreAssetKey = StringToKey(assetKey);

        //Asset TemplateKey
        Entity res = dataStore.findOneByKey(dataStoreAssetKey);
        Key templateKey = (Key) res.getProperty("TemplateKey");
        String assetEncodedKey = KeyUtils.keyToString(templateKey);

        //Deactivate Asset
        server.v2_8.UpdateAsset updateAsset = new server.v2_8.UpdateAsset();
        updateAsset.setActive("N");

        IServerResponse response = updateAsset.sendRequestByTokenAndAssetKey(serverToken.toString(), assetEncodedKey, UpdateAssetResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail("Expected Error response " + response.toString());
        }

        Query qUserAction = new Query("Asset");
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Name",Query.FilterOperator.EQUAL, assetName),
                new Query.FilterPredicate("Status",Query.FilterOperator.EQUAL, "Template"))));

        List<Entity> resultUserAction = dataStore.findByQuery(qUserAction);
        Assert.assertEquals("Asset Active  must be false", false, resultUserAction.get(0).getProperty("Active"));

        //Redeem asset - hub
        InvokeEventBasedActionRuleBuilder invokeEventBasedActionRuleBuilder = new InvokeEventBasedActionRuleBuilder();
        InvokeEventBasedActionRule invokeEventBasedActionRule = invokeEventBasedActionRuleBuilder.buildRedeemAssetRequest(memberKey, newMember.getUserKey(), locationId,assetKey);
        IServerResponse response1 = invokeEventBasedActionRule.SendRequestByToken(serverToken, Response.class);

    }

    //C3653
    @Test
    public void testAllowAssetToBeDisabledCantRedeemInactiveAssetFromClient() throws IOException, InterruptedException {

        //Assign asset to member
        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(), false);

        //Create new customer
        JoinClubResponse newMember = createMember();

        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "None",memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();
        String assetName = asset.getName();
        Key dataStoreAssetKey = StringToKey(assetKey);

        //Asset TemplateKey
        Entity res = dataStore.findOneByKey(dataStoreAssetKey);
        Key templateKey = (Key) res.getProperty("TemplateKey");
        String assetEncodedKey = KeyUtils.keyToString(templateKey);

        //Deactivate Asset
        server.v2_8.UpdateAsset updateAsset = new server.v2_8.UpdateAsset();
        updateAsset.setActive("N");

        IServerResponse response = updateAsset.sendRequestByTokenAndAssetKey(serverToken.toString(), assetEncodedKey, UpdateAssetResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Redeem asset - client
        RedeemAsset redeemAsset = buildRedeemAsset(memberKey, assetKey);

        //Send the redeemAsset request
        IServerResponse response1 = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);

        //Make sure the response is Error response
        if (!(response1 instanceof ZappServerErrorResponse)) {
            Assert.fail("Expected Error response " + response.toString());
        }

    }


    //C3643
    @Test
    public void testAllowAssetToBeDisabledRedeemDeactivatedAsset() throws IOException, InterruptedException {

        //Assign asset to member
        serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(), false);

        //Create new customer
        JoinClubResponse newMember = createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "None", memberKey, apiKey, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();
        String assetName = asset.getName();
        Key dataStoreAssetKey = StringToKey(assetKey);


        //Redeem asset - client
        RedeemAsset redeemAsset = buildRedeemAsset(memberKey, assetKey);

        //Send the redeemAsset request
        IServerResponse response1 = redeemAsset.SendRequestByToken(serverToken, RedeemAssetResponse.class);

        //Make sure the response is not Error response
        if (response1 instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response1).getErrorMessage());
        }

       // Get the redeemAsset response
        RedeemAssetResponse redeemAssetResponse = (RedeemAssetResponse) response1;
        Assert.assertEquals("Redeem result = Y", "Y", redeemAssetResponse.getResult());

        //Asset TemplateKey
        Entity res = dataStore.findOneByKey(dataStoreAssetKey);
        Key templateKey = (Key) res.getProperty("TemplateKey");
        String assetEncodedKey = KeyUtils.keyToString(templateKey);

        //Deactivate Asset
        server.v2_8.UpdateAsset updateAsset = new server.v2_8.UpdateAsset();
        updateAsset.setActive("N");

        IServerResponse response = updateAsset.sendRequestByTokenAndAssetKey(serverToken.toString(), assetEncodedKey, UpdateAssetResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Send redeem request in order to check redeem status
        RedeemItem redeemItem = new RedeemItem();

        Redeem redeem = new Redeem();
        redeem.setReturnExtendedItems(true);

        //Send the redeem request
        IServerResponse response2 = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Make sure the response is Error response
        if (!(response2 instanceof ZappServerErrorResponse)) {
            Assert.fail(((ZappServerErrorResponse) response2).getErrorMessage());
        }


        Query qUserAction = new Query("Asset"); //todo: check onetimecode
        qUserAction.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Name",Query.FilterOperator.EQUAL, assetName),
                new Query.FilterPredicate("Status",Query.FilterOperator.EQUAL, "Template"))));

        List<Entity> resultUserAction = dataStore.findByQuery(qUserAction);
        Assert.assertEquals("Asset Active  must be false", false, resultUserAction.get(0).getProperty("Active"));

    }

    @NotNull
    private RedeemAsset buildRedeemAsset(String memberKey, String assetKey) {
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(assetKey);
        redeemAsset.setMembershipKey(memberKey);
        redeemAsset.setToken(serverToken);
        return redeemAsset;
    }


    @Test
    public void testRedeemWithTemporaryToken () throws Exception{

        String phoneMember = String.valueOf(System.currentTimeMillis());
        //signOn and get the user token
        String userToken = signOn();
        //join the club with the user token
        String memKey = serverHelper.joinClubWithUserToken(phoneMember, userToken); //returns the membershipKey
        updateMembership(userToken,memKey,phoneMember);

        //Assign asset to member
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "None", memKey, apiKey, locationId, serverToken, phoneMember);
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("10000", userToken, locationId);
        Redeem redeem = createRedeem(tokenTemp,newAsset1);

        //Send the redeem request
        IServerResponse redeemResponse = redeem.SendRequestByApiKey(apiKey, RedeemResponse.class);

        //Get the redeem response
        RedeemResponse redeemAssetResponse = (RedeemResponse) redeemResponse;
        Assert.assertFalse("Response code must not be empty!",StringUtils.isEmpty(redeemAssetResponse.getSimplefiedDealCodes().get(0)));
        Assert.assertEquals("Redeem result = Y","Y", redeemAssetResponse.getResult());

    }

  //  @Test
    public void testRedeemWithTemporaryTokenNegative () throws Exception{

        String phoneMember = String.valueOf(System.currentTimeMillis());
        //signOn and get the user token
        String userToken = signOn();
        //join the club with the user token
        String memKey = serverHelper.joinClubWithUserToken(phoneMember, userToken); //returns the membershipKey
        updateMembership(userToken,memKey,phoneMember);

        //Assign asset to member
        Asset newAsset1 = AssignAsset.assignAssetToMember("gift", "None",memKey, apiKey, locationId, serverToken, phoneMember);
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("1", userToken, locationId);
        Redeem redeem = createRedeem(tokenTemp,newAsset1);
        Thread.sleep(2000);

        //Get the redeem response
        ZappServerErrorResponse redeemResponse = (ZappServerErrorResponse)redeem.SendRequestByApiKey(apiKey, ZappServerErrorResponse.class);
        Assert.assertEquals("Result must be N", "N", redeemResponse.getResult());
        Assert.assertEquals("ErrorMessage", "One or more customers not found",redeemResponse.getErrorMessage());

    }

    private Redeem createRedeem(String tokenTemp, Asset asset){

        String assetKey = asset.getKey();
        //Send redeem request in order to check redeem status
        Redeem redeem = new Redeem();
        redeem.setReturnExtendedItems(true);
        RedeemItem redeemItem = new RedeemItem();
        redeemItem.setAssetKey(assetKey);
        redeem.getRedeemItems().add(redeemItem);

        Customer customer = new Customer();
        customer.setTemporaryToken(tokenTemp);
        redeem.getCustomers().add(customer);

        return redeem;

    }

    private void updateMembership (String userToken, String memKey, String phoneMember){

        UpdatedFields updatedFields = new UpdatedFields();
        updatedFields.setPhoneNumber(phoneMember);
        UpdateMembership updateMembership = new UpdateMembership();
        updateMembership.setLocationID(locationId);
        updateMembership.setMembershipKey(memKey);
        updateMembership.setToken(serverToken);
        updateMembership.setUpdatedFields(updatedFields);

        IServerResponse response = updateMembership.sendRequestByTokenAndLocation(userToken, locationId, UpdateMembershipResponse.class);


    }


}





