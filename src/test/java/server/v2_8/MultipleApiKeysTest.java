package server.v2_8;

import hub.base.categories.serverCategories.ServerRegression;
import hub.base.categories.serverCategories.serverV2_8;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.base.BaseServerTest;
import server.common.*;
import server.common.asset.AssetBuilder;
import server.common.asset.AssignAsset;
import server.utils.Initializer;
import server.utils.ServerHelper;
import server.v2_8.base.ServicesErrorResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Asset;
import server.v2_8.common.Customer;
import server.v2_8.common.RedeemItem;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.UUID;

import static java.lang.System.currentTimeMillis;

/**
 * Created by Goni on 10/24/2017.
 */

public class MultipleApiKeysTest extends BaseServerTest {


    private static final boolean SHOULD_PASS = true;
    public static final String FORBIDDEN = "Forbidden";
    public static final String INACTIVE = "inactive";
    public static final String ACTIVE = "active";

    private static String inactiveApiKey;
    private static String activeApiKey;

    private AssetBuilder assetBuilder=new AssetBuilder();

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        updateConfiguration = true;
        init();
        try {
            inactiveApiKey = ServerHelper.addApiKeyAndValidate("inActiveKey", INACTIVE, dataStore);
            activeApiKey = ServerHelper.addApiKeyAndValidate("activeKey", ACTIVE,dataStore);
        } catch (Exception e) {
           log4j.info("Error while trying to create apiKey - " + e.getMessage());
        }
    }


    @Test
    @Category({ServerRegression.class})
    public void testSubmitPurchase() throws Exception{

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()),
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(), false));

        log4j.info("Testing alpha/api/SubmitPurchase");
        IServerResponse response = submitPurchase(inactiveApiKey);
        validateResponse(response,!SHOULD_PASS);
        response = submitPurchase(activeApiKey);
        validateResponse(response,SHOULD_PASS);
    }

    @Test
    public void testCancelPurchase() throws Exception{

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()),
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key(), true));

        log4j.info("Testing alpha/api/CancelPurchase");
        IServerResponse response = submitPurchase(activeApiKey);
        String confirmation = ((SubmitPurchaseResponse)response).getConfirmation();
        response = cancelPurchase(inactiveApiKey,confirmation);
        validateResponse(response,!SHOULD_PASS);
        response = cancelPurchase(activeApiKey,confirmation);
        validateResponse(response,SHOULD_PASS);
    }

    @Test
    public void testCancelBudget() throws Exception {

        serverHelper.setConfigurationForPayment("false",PaymentType.POINTS.type(),"1","true");

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false));

        log4j.info("Testing alpha/api/cancelBudget");
        PayWithBudget payWithBudget = buildPayWithBudget();
        IServerResponse response = cancelBudget(inactiveApiKey,payWithBudget);
        validateResponse(response,!SHOULD_PASS);
        response = cancelBudget(activeApiKey,payWithBudget);
        validateResponse(response,SHOULD_PASS);
    }

    @Test
    @Category({ServerRegression.class})
    public void testPayWithBudget() throws Exception {
        serverHelper.setConfigurationForPayment("false",PaymentType.POINTS.type(),"1","true");

        log4j.info("Testing alpha/api/PayWithBudget");
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false));
        PayWithBudget payWithBudget = buildPayWithBudget();
        IServerResponse response = payWithBudget.SendRequestByApiKey(inactiveApiKey, PayWithBudgetResponse.class);
        validateResponse(response,!SHOULD_PASS);
        payWithBudget = buildPayWithBudget();
        response = payWithBudget.SendRequestByApiKey(activeApiKey, PayWithBudgetResponse.class);
        validateResponse(response,SHOULD_PASS);
    }

    @Test
    @Category({ServerRegression.class})
    public void testGetMemberDetails() throws Exception {


        log4j.info("Testing alpha/api/GetMemberDetails");
        IServerResponse response = getMemberDetails(inactiveApiKey);
        validateResponse(response,!SHOULD_PASS);
        response = getMemberDetails(activeApiKey);
        validateResponse(response,SHOULD_PASS);
    }
    @Test
    @Category({serverV2_8.class})
    public void testGetMemberBenefits() throws Exception {

        log4j.info("Testing alpha/api/GetMemberBenefits");
        IServerResponse response = getMemberBenefitsWithTemporaryToken(inactiveApiKey);
        validateResponse(response,!SHOULD_PASS);
        response = getMemberBenefitsWithTemporaryToken(activeApiKey);
        validateResponse(response,SHOULD_PASS);
    }

    @Test
    public void testRedeem() throws Exception{

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.DETAILED_REDEEM_RESPONSE.key(), true),
                ApiClientBuilder.newPair(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(), true));

        log4j.info("Testing alpha/api/Redeem");
        IServerResponse response = redeemAsset(inactiveApiKey,activeApiKey);
        validateResponse(response,!SHOULD_PASS);
        response = redeemAsset(activeApiKey,activeApiKey);
        validateResponse(response,SHOULD_PASS);
    }

    private IServerResponse redeemAsset(String apiKeyToTest,String validApiKeyForAssignAsset) throws Exception{

        JoinClubResponse newMember = createMember();

        String memberKey = newMember.getMembershipKey();

        Asset asset = AssignAsset.assignAssetToMember("gift", "None",memberKey, validApiKeyForAssignAsset, locationId, serverToken, newMember.getPhoneNumber());
        String assetKey = asset.getKey();


        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        Redeem redeem = new Redeem();
        redeem.getCustomers().add(customer);
        redeem.setTransactionId(UUID.randomUUID().toString());

        RedeemItem redeemItemPurchase = new RedeemItem();
        redeemItemPurchase.setAssetKey(assetKey);

        ArrayList<RedeemItem> redeemItemForPOS = new ArrayList<>();
        redeemItemForPOS.add(redeemItemPurchase);
        redeem.setRedeemItems(redeemItemForPOS);

        // Send the request
        return redeem.SendRequestByApiKey(apiKeyToTest, RedeemResponse.class);
    }

    private IServerResponse getMemberBenefitsWithTemporaryToken(String apiKeyToTest) {

        String userToken = serverHelper.createNewMemberGetUserToken();
        String tokenTemp = serverHelper.generateTokenWebViewUserToken("10000", userToken, locationId);
        GetMemberBenefits getMemberBenefits = serverHelper.createGetMemberBenefits(tokenTemp);

        //Send the request
       return getMemberBenefits.SendRequestByApiKey(apiKeyToTest, GetMemberBenefitsResponse.class);
    }


    private IServerResponse getMemberDetails(String apiKeyToTest) throws Exception{
        //Create new customer
        JoinClubResponse newMember = createMember();

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.getCustomers().add(customer);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(false);

        return getMemberDetails.SendRequestByApiKey(apiKeyToTest, GetMemberDetailsResponse.class);
    }

    private IServerResponse cancelPurchase(String apiKeyToTest, String confirmation) {
        CancelPurchase cancelPurchase = new CancelPurchase();
        cancelPurchase.setConfirmation(confirmation);
        Assert.assertFalse("Confirmation must not be empty!", StringUtils.isEmpty(cancelPurchase.getConfirmation()));

        //Cancel the Purchase
        return cancelPurchase.SendRequestByApiKey(apiKeyToTest, CancelPurchaseResponse.class);
    }

    private IServerResponse submitPurchase(String apiKeyToTest) {

        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setBranchId("1");
        submitPurchase.setPosId("10");
        submitPurchase.setTotalSum("2000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        //Send the first request
        return submitPurchase.SendRequestByApiKey(apiKeyToTest, SubmitPurchaseResponse.class);
    }



    private PayWithBudget buildPayWithBudget() throws Exception{
        JoinClubResponse customerPhone = createMember();

        Customer customer = new Customer();
        customer.setPhoneNumber(customerPhone.getPhoneNumber());
        PayWithBudget payWithBudget = new PayWithBudget();
        payWithBudget.getCustomers().add(customer);
        payWithBudget.setTotalSum("-3000");
        payWithBudget.setTimeStamp(String.valueOf(System.currentTimeMillis()));
        payWithBudget.setTransactionId(UUID.randomUUID().toString());
        payWithBudget.setBranchId("1");
        payWithBudget.setPosId("2");

        return  payWithBudget;
    }



    @Test
    //api/{v1|external}/memberships/register
    public void testExternalRegisterMember() throws Exception{
        updateConfiguration = true;
        log4j.info("Testing api/{v1|external}/memberships/register");
        IServerResponse response = registerExternalMember(inactiveApiKey);
        validateResponse(response, !SHOULD_PASS);
        response = registerExternalMember(activeApiKey);
        validateResponse(response, SHOULD_PASS);
    }





    private IServerResponse registerExternalMember(String apiKeyToTest) throws Exception {
        if (updateConfiguration) {
            ConfigRegistration configRegistration = Initializer.initConfigRegistration(locationId);
            configRegistration.sendRequestByTokenAndValidateResponse(serverToken, Response.class);
        }
        RegisterMember registerMember = Initializer.initRegisterMember(String.valueOf(currentTimeMillis()));
        return registerMember.sendRequestByApiKey(apiKeyToTest, RegisterResponse.class);
    }


    private IServerResponse cancelBudget(String apiKeyToTest,PayWithBudget payWithBudget) throws Exception{

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_BUDGET_PAYMENT_BY_POS_IDENTIFIERS.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false));

        serverHelper.setConfigurationForPayment("false",PaymentType.BUDGET.type(),"1","true");
        //we send payWithBudget with default api do the request will pass
        IServerResponse response = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);
        String paymentUid = ((PayWithBudgetResponse)response).getPaymentUID();
        CancelBudgetPayment cancelBudgetPayment = new CancelBudgetPayment();
        cancelBudgetPayment.setPaymentUID(paymentUid);

       return cancelBudgetPayment.SendRequestByApiKey(apiKeyToTest, CancelBudgetPaymentResponse.class);
    }



    private void validateResponse(IServerResponse response,boolean shouldPass) {
        if(shouldPass){
            Assert.assertTrue(((response.getStatusCode().intValue() == httpStatusCode200.intValue()) ||
                    (response.getStatusCode()==httpStatusCode201.intValue())));
        }else {
            if(response instanceof ServicesErrorResponse){
                ServicesErrorResponse errorResponse =(ServicesErrorResponse)response;
                Assert.assertEquals(errorResponse.getErrorType().toUpperCase(), FORBIDDEN.toUpperCase());
                Assert.assertEquals(errorResponse.getStatusCode(), Integer.valueOf(403));
            }else if(response instanceof ZappServerErrorResponse){
                ZappServerErrorResponse errorResponse =(ZappServerErrorResponse)response;
                Assert.assertTrue("Error message should contain the following message: Client with api-key not found"  ,errorResponse.getErrorMessage().contains("Client with api-key not found"));
                Assert.assertEquals(errorResponse.getErrorCode(), "1003");
            }
        }
    }




}

