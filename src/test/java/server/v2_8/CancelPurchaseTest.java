package server.v2_8;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.serverCategories.ServerRegression;
import hub.base.categories.serverCategories.serverV2_8;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.base.BasePurchaseTest;
import server.common.AnonymousPurchaseTypes;
import server.common.ApiClientBuilder;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.utils.Initializer;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Customer;
import server.v2_8.common.Item;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Jenny on 12/11/2016.
 */
public class CancelPurchaseTest extends BasePurchaseTest {

    public static final String EMPTY_STRING = "";

    @BeforeClass
    public static void setup() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        // common initialization done once for all tests
        init();
    }


    /**
     * C2932 Cancel Purchase and Allow Analyzed Purchase Cancellation=Yes
     **/
    @Test
    @Category(serverV2_8.class)
    public void testCancelPurchaseAndAllowAnalyzedPurchaseCancellationIsYes() throws InterruptedException, IOException{
        //Preconditions - Save anonymous purchase = Always and AllowAnalyzedPurchaseCancellation = Yes
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(),AnonymousPurchaseTypes.ALWAYS.type()),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key(), true),
        ApiClientBuilder.newPair(ApiClientFields.AUTOMATION_DELAY_MS.key(),2000));


        //Assert that SaveAnonymousPurchaseOn = Always in the dataStore and AllowAnalyzedPurchaseCancellation = Yes
        Query q = new Query("ApiClient");
        q.setFilter(new Query.FilterPredicate("BusinessID", Query.FilterOperator.EQUAL, locationId));
        List<Entity> result = serverHelper.queryWithWait("Must have one result for business in ApiClient"+ locationId, q, 1, dataStore);
        Assert.assertEquals("SaveAnonymousPurchaseOn is set to Always", AnonymousPurchaseTypes.ALWAYS.type(), result.get(0).getProperty(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key()));
        Assert.assertEquals("AllowAnalyzedPurchaseCancellation is set to Yes", true, result.get(0).getProperty(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key()));

        // Initialize a new SubmitPurchase
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setBranchId("1");
        submitPurchase.setPosId("12");
        submitPurchase.setTotalSum("4000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setTransactionSource("TestAutomation");
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        Item item = new Item();
        item.setItemCode("4454");
        item.setQuantity(1);
        item.setAmount(1.24);
        item.setItemName("LiorItem1");
        item.setDepartmentCode("554");
        item.setItemName("Dep3");
        item.setPrice(4000);

        ArrayList<String> tags = new ArrayList<>();
        tags.add("Tag1");
        tags.add("Tag2");
        item.setTags(tags);

        ArrayList<Item> items = new ArrayList<>();
        items.add(item);

        submitPurchase.setItems(items);

        // Send the request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        //Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;

        serverHelper.validatePurchaseResponse(submitPurchaseResponse,submitPurchase.getTotalSum(),"1");

        //Response confirmation
        String confirmation = submitPurchaseResponse.getConfirmation();

        /*//Response time must be under 5000
        Assert.assertTrue("Response time must be under 5000, time was: " + submitPurchaseResponse.getTime(), submitPurchaseResponse.getTime() <= 10000 );*/

        //Verify with DataStore
        q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: "+ submitPurchase.getTransactionId(),
                q, 1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: "+ submitPurchase.getTransactionId(),
                q, 1,dataStore);
        Assert.assertTrue("UID must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));

        // Initialize a new CancelPurchase
        CancelPurchase cancelPurchase = new CancelPurchase();
        cancelPurchase.setConfirmation(confirmation);
        Assert.assertFalse("Confirmation must not be empty!", StringUtils.isEmpty(cancelPurchase.getConfirmation()));

        //Cancel the Purchase via Postman
        response = cancelPurchase.SendRequestByApiKey(apiKey, CancelPurchaseResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        //Cast to CancelPurchase object to fetch all response data
        CancelPurchaseResponse cancelPurchaseResponse = (CancelPurchaseResponse) response;
        Assert.assertEquals("status code must be 200", (Integer) 200, cancelPurchaseResponse.getStatusCode());
        Assert.assertEquals("Result must by Y", "Y", cancelPurchaseResponse.getResult());

        //Verify if the transaction status is Cancelled = true
        q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: "+ submitPurchase.getTransactionId(),
                q, 1, dataStore);
        Assert.assertEquals("Cancelled status must be true", true, result.get(0).getProperty("Cancelled"));

    }

    /**
     * C4317 Cancel Purchase and Allow Analyzed Purchase Cancellation= No
     **/

    @Test
    @Category({ServerRegression.class})
    public void testCancelPurchaseAndAllowAnalyzedPurchaseCancellationIsNo() throws InterruptedException, IOException{

        //The business must have 'Makes purchase' automation

        //Preconditions - Save anonymous purchase = Always and AllowAnalyzedPurchaseCancellation = Yes
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(),AnonymousPurchaseTypes.ALWAYS.type()),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key(), false),
        ApiClientBuilder.newPair(ApiClientFields.AUTOMATION_DELAY_MS.key(),2000));


        JoinClubResponse customerPhone = serverHelper.createMember();

        //Assert that SaveAnonymousPurchaseOn = 'Always' in the dataStore and AllowAnalyzedPurchaseCancellation = 'No'
        List<Entity> result = serverHelper.getBusinessFromApiClient(dataStore);
        Assert.assertEquals("AllowAnalyzedPurchaseCancellation should be set to false", false, result.get(0).getProperty(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key()));


        // Initialize a new SubmitPurchase
        SubmitPurchase submitPurchase = new SubmitPurchase();
        Customer customer = new Customer();
        customer.setPhoneNumber(customerPhone.getPhoneNumber());
        submitPurchase.getCustomers().add(customer);
        submitPurchase.setBranchId("3");
        submitPurchase.setPosId("13");
        submitPurchase.setTotalSum("4000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setTransactionSource("TestAutomation");
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");

        Item item = new Item();
        item.setItemCode("4454");
        item.setQuantity(1);
        item.setAmount(1.24);
        item.setItemName("LiorItem1");
        item.setDepartmentCode("554");
        item.setItemName("Dep3");
        item.setPrice(4000);

        ArrayList<String> tags = new ArrayList<>();
        tags.add("Tag1");
        tags.add("Tag2");
        item.setTags(tags);

        ArrayList<Item> items = new ArrayList<>();
        items.add(item);

        submitPurchase.setItems(items);

        // Send the request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        //Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        serverHelper.validatePurchaseResponse(submitPurchaseResponse,submitPurchase.getTotalSum(),"1");

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: "+ submitPurchase.getTransactionId(),
                q,1,dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: "+ submitPurchase.getTransactionId(),q, 1, dataStore);
        Assert.assertTrue("UID must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));

        // Initialize a new CancelPurchase
        String transactionId = submitPurchase.getTransactionId();
        String branchId = submitPurchase.getBranchId();
        String posId = submitPurchase.getPosId();
        CancelPurchase cancelPurchase = new CancelPurchase();
        cancelPurchase.setTransactionId(transactionId);
        cancelPurchase.setBranchId(branchId);
        cancelPurchase.setPosId(posId);

        //check if purchase already inn status analyzed
        checkIfPurchaseInAnalyzedStatus(submitPurchase);

        //Cancel the Purchase via Postman
        IServerResponse response1 = cancelPurchase.SendRequestByApiKey(apiKey, CancelPurchaseResponse.class);


        //Make sure if the response is not Error response (in this test we should get error message)
        if(!(response1 instanceof ZappServerErrorResponse)){
            Assert.fail("Expected an error!");
        }

        //Make sure the response returns correct Error response
        Assert.assertEquals("Error message is: Purchase with the supplied confirmation is not in cancellable status", "Purchase with the supplied confirmation is not in cancellable status",((ZappServerErrorResponse)response1).getErrorMessage());
        Assert.assertEquals("Result must be N", "N", ((ZappServerErrorResponse)response1).getResult());
        Assert.assertEquals("ErrorCode is 1010", "1010",((ZappServerErrorResponse)response1).getErrorCode());

        //Verify if the transaction status is Cancelled = false
        q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: "+ submitPurchase.getTransactionId(),q, 1, dataStore);
        Assert.assertEquals("Cancelled status must be false", false, result.get(0).getProperty("Cancelled"));
        Assert.assertEquals("Status must be Approved", "Analyzed", result.get(0).getProperty("Status"));

    }

    private void checkIfPurchaseInAnalyzedStatus(SubmitPurchase submitPurchase) throws InterruptedException {
        Thread.sleep(10000);
        Query q = new Query("Purchase");
        ArrayList<Query.Filter>  filters = new ArrayList<>();
        filters.add(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        filters.add(new Query.FilterPredicate("Status",Query.FilterOperator.EQUAL, "Analyzed"));
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, filters));
        List<Entity> result = serverHelper.queryWithWait("Must find Purchase with the transaction ID in analyzed status before cancel : "+ submitPurchase.getTransactionId(),
                q,1,dataStore);
        if(result.isEmpty()){
            Assert.fail("Could not find Purchase with the transaction ID: " + submitPurchase.getTransactionId() + " in analyzed status ");
        }
    }


    /**
     * C4305 Cancel Purchase with Purchase Status = Open
     **/

    @Test
    @Category({ServerRegression.class})
    public void testCancelPurchaseWithOpenPurchaseStatus() throws InterruptedException
    {

        JoinClubResponse customerPhone = serverHelper.createMember();

        // Initialize a new SubmitPurchase
        SubmitPurchase submitPurchase = Initializer.initSubmitPurchase(customerPhone.getPhoneNumber());
        submitPurchase.setStatus("Open");

        // Send the request
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey, SubmitPurchaseResponse.class);
        serverHelper.validatePurchaseResponse(submitPurchaseResponse, submitPurchase.getTotalSum(),"1");

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: "+ submitPurchase.getTransactionId(),
                q, 1, dataStore);
        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));
        Assert.assertEquals("Purchase status should be 'Open'", "Open", result.get(0).getProperty("Status"));

        Thread.sleep(3000);
        q = new Query("UserAction");
        ArrayList<Query.Filter> filters = new ArrayList<>();
        filters.add(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        filters.add(new Query.FilterPredicate("Action",Query.FilterOperator.EQUAL, "OpenPurchase"));
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, filters));
        result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: "+ submitPurchase.getTransactionId(),
                q, 1, dataStore);

        Assert.assertTrue("UID must be inside", serverHelper.dataContainsConfirmation(result, submitPurchaseResponse.getConfirmation()));

        // Initialize a new CancelPurchase
        String transactionId = submitPurchase.getTransactionId();
        String branchId = submitPurchase.getBranchId();
        String posId = submitPurchase.getPosId();

        CancelPurchase cancelPurchase = new CancelPurchase();
        cancelPurchase.setTransactionId(transactionId);
        cancelPurchase.setBranchId(branchId);
        cancelPurchase.setPosId(posId);

        //Cancel the Purchase via Postman
        IServerResponse response1 = cancelPurchase.SendRequestByApiKey(apiKey, CancelPurchaseResponse.class);
        Assert.assertTrue("Response should be an error response", response1 instanceof ZappServerErrorResponse);
        ZappServerErrorResponse error = (ZappServerErrorResponse) response1;
        Assert.assertEquals("Error should be : Failed to submit cancellation purchase", "Failed to submit cancellation purchase", error.getErrorMessage());
        Assert.assertEquals("Error code should be : 1012", "1012", error.getErrorCode());
        Assert.assertEquals("Result should be : N", "N", error.getResult());

        //Verify if the transaction status is Cancelled = false
        q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        result = dataStore.findByQuery(q);
        Assert.assertEquals("Must find one Purchase with the transaction ID: "+ submitPurchase.getTransactionId(), 1, result.size());
        Assert.assertEquals("Cancelled status must be false", false, result.get(0).getProperty("Cancelled"));

    }


    /**
     * C2795 Cancel by transaction ID + Branch ID + POS ID - when more than one purchase with the same Branch ID
     **/
    @Test
    @Category({ServerRegression.class})
    public void testCancelPurchaseByTransactionIDBranchIDPsoIDmoreThenSameBranchID() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key(), true));



        Item item = createItem("4454", "testItem", "dep1", "depName", 4000, 1);
        SubmitPurchase submitPurchase = createSubmitPurchaseRequest("branch123", "pos1", "4000", "chain787", "cashier1", "automation",UUID.randomUUID().toString(), Lists.newArrayList(item));
        sendSubmitPurchase(submitPurchase);

        //create another submitPurchase request:
        SubmitPurchase submitPurchase2 = createSubmitPurchaseRequest("branch123", "pos5", "4000", "chain787", "cashier1", "automation",UUID.randomUUID().toString(), Lists.newArrayList(item));
        sendSubmitPurchase(submitPurchase2);

        //check if purchase already inn status analyzed
        checkIfPurchaseInAnalyzedStatus(submitPurchase);

        //Cancel purchase
        cancelPurchaseRequest(EMPTY_STRING,submitPurchase.getPosId(),submitPurchase.getTransactionId(),submitPurchase.getBranchId());

        //Verify the submit purchase is canceled
        verifySubmitPurchaseIsCanceled (submitPurchase.getTransactionId());
        //Verify that the second purchase is not canceled
        verifySubmitPurchaseIsNOTCanceled(submitPurchase2.getTransactionId());

    }

    /**
     * C2796 Cancel by transaction ID + Branch ID + POS ID - when more than one purchase with the same POS ID
     **/
    @Test
    public void testCancelPurchaseByTransactionIDBranchIDPsoIDtheSamePOSID() throws Exception {

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()),
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key(), true));


        Item item = createItem("4454", "testItem", "dep1", "depName", 4000, 1);
        SubmitPurchase submitPurchase = createSubmitPurchaseRequest("branch12345", "pos1", "4000", "chain787", "cashier1", "automation",UUID.randomUUID().toString(), Lists.newArrayList(item));
        sendSubmitPurchase(submitPurchase);

        //create another submitPurchase request:
        SubmitPurchase submitPurchase2 = createSubmitPurchaseRequest("branch123", "pos1", "4000", "chain787", "cashier1", "automation",UUID.randomUUID().toString(), Lists.newArrayList(item));
        sendSubmitPurchase(submitPurchase2);

        //check if purchase already inn status analyzed
        checkIfPurchaseInAnalyzedStatus(submitPurchase);

        //Cancel purchase
        cancelPurchaseRequest(EMPTY_STRING,submitPurchase.getPosId(),submitPurchase.getTransactionId(),submitPurchase.getBranchId());
        //Verify the submit purchase is canceled
        verifySubmitPurchaseIsCanceled (submitPurchase.getTransactionId());
        //Verify that the second purchase is not canceled
        verifySubmitPurchaseIsNOTCanceled(submitPurchase2.getTransactionId());


    }


    private void verifySubmitPurchaseIsCanceled (String transactionID) throws Exception{


        //Verify if the transaction status is Cancelled = true
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, transactionID));
        List<Entity>  result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + transactionID,
                q, 1, dataStore);
        Assert.assertEquals("Cancelled status must be true", true, result.get(0).getProperty("Cancelled"));

    }

    private void verifySubmitPurchaseIsNOTCanceled (String transactionID) throws Exception{


        //Verify if the transaction status is Cancelled = true
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, transactionID));
        List<Entity>  result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + transactionID,
                q, 1, dataStore);
        Assert.assertEquals("Cancelled status must be true", false, result.get(0).getProperty("Cancelled"));

    }


    private void cancelPurchaseRequest (String confirmation, String posID, String transactionID, String branchID){

        // Initialize a new CancelPurchase
        CancelPurchase cancelPurchase = new CancelPurchase();
        if (!EMPTY_STRING.equals(confirmation))
            cancelPurchase.setConfirmation(confirmation);
        if (!EMPTY_STRING.equals(posID))
            cancelPurchase.setPosId(posID);
        if (!EMPTY_STRING.equals(transactionID))
            cancelPurchase.setTransactionId(transactionID);
        if (!EMPTY_STRING.equals(branchID))
            cancelPurchase.setBranchId(branchID);

        //Cancel the Purchase via Postman
        IServerResponse response = cancelPurchase.SendRequestByApiKey(apiKey, CancelPurchaseResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Cast to CancelPurchase object to fetch all response data
        CancelPurchaseResponse cancelPurchaseResponse = (CancelPurchaseResponse) response;
        Assert.assertEquals("status code must be 200", (Integer) 200, cancelPurchaseResponse.getStatusCode());
        Assert.assertEquals("Result must by Y", "Y", cancelPurchaseResponse.getResult());



    }


    private SubmitPurchase createSubmitPurchaseRequest(String branchID, String posID, String totalSum, String chainID, String cashier, String transactionSource,String transactionID, ArrayList<Item> items) throws InterruptedException {


        // Initialize a new SubmitPurchase
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setBranchId(branchID);
        submitPurchase.setPosId(posID);
        submitPurchase.setTotalSum(totalSum);
        submitPurchase.setTransactionId(transactionID);
        submitPurchase.setTransactionSource(transactionSource);
        submitPurchase.setChainId(chainID);
        submitPurchase.setCashier(cashier);
        submitPurchase.setItems(items);

        JoinClubResponse customerPhone = serverHelper.createMember();

        Customer customer = new Customer();
        customer.setPhoneNumber(customerPhone.getPhoneNumber());
        submitPurchase.getCustomers().add(customer);

        return submitPurchase;
    }


    private SubmitPurchaseResponse sendSubmitPurchase (SubmitPurchase submitPurchase) throws Exception{

        // Send the request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        //Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        serverHelper.validatePurchaseResponse(submitPurchaseResponse,submitPurchase.getTotalSum(),"1");
        String confirmation = submitPurchaseResponse.getConfirmation();

        //Verify with DataStore
        Query q = new Query("Purchase");
        q.setFilter(new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, submitPurchase.getTransactionId()));
        log4j.info(" Purchase :Transaction ID " + submitPurchase.getTransactionId());
        List<Entity> result = serverHelper.queryWithWait("Must find one Purchase with the transaction ID: " + submitPurchase.getTransactionId(),
                q, 1, dataStore);

        serverHelper.validatePurchaseResult(submitPurchase, result.get(0));

        q = getOueryForUserAction(submitPurchase.getTransactionId());
        log4j.info("UserAction: Transaction ID " + submitPurchase.getTransactionId());
        result = serverHelper.queryWithWait("Must find one UserAction with the transaction ID: " + submitPurchase.getTransactionId(),
                q, 1, dataStore);
        Assert.assertTrue("UID must be inside", serverHelper.dataContainsConfirmation(result, confirmation));

        //Return response confirmation
        return submitPurchaseResponse;

    }

    private Item createItem(String itemCode, String itemName, String depCode, String depName, Integer itemPrice, Integer quantity) {

        Item item = new Item();

        item.setItemCode(itemCode);
        item.setQuantity(quantity);
        item.setAmount(1.2415);
        item.setItemName(itemName);
        item.setDepartmentCode(depCode);
        item.setDepartmentName(depName);
        item.setPrice(itemPrice);
        item.setTags(Lists.newArrayList("DEP1"));

        return item;
    }

}
