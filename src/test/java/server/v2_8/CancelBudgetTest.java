package server.v2_8;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import hub.base.categories.serverCategories.ServerRegression;
import hub.base.categories.serverCategories.serverV2_8;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.base.BaseServerTest;
import server.common.ApiClientBuilder;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Customer;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;
import java.util.UUID;

/**
 * Created by israel on 02/03/2017.
 */
public class CancelBudgetTest extends BaseServerTest {


    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        updateConfiguration = true;
        init();
    }

    /**
     * C2939 - Cancel Budget Payment with Payment UID
     **/
    @Test
    @Category({ServerRegression.class})
    public void testCancelBudgetPaymentWithPaymentUID() throws Exception {
        /*
        steps:
        1. verify if the flag AllowCancelMemberBudgetPayment = true
        2. send pay with budget , verify if the response 200 ok
        3. cancel the budget by Payment UID
        4. verify if the  data in BudgetTransaction table correct
         */

        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),true),
        ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false));


        JoinClubResponse member = createMember();

        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        PayWithBudget payWithBudget = new PayWithBudget();
        payWithBudget.getCustomers().add(customer);
        payWithBudget.setTotalSum("-3000");
        payWithBudget.setTimeStamp(String.valueOf(System.currentTimeMillis()));
        payWithBudget.setTransactionId(UUID.randomUUID().toString());
        payWithBudget.setBranchId("1");
        payWithBudget.setPosId("2");

        IServerResponse response = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        String actualCharge = payWithBudget.getTotalSum();
        actualCharge = serverHelper.replaceCharAt(actualCharge, 0, "");

        PayWithBudgetResponse payWithBudgetResponse = (PayWithBudgetResponse) response;
        Assert.assertEquals("status code must be 200", (Integer) 200,payWithBudgetResponse.getStatusCode());
        Assert.assertEquals("Result = Y ", "Y",payWithBudgetResponse.getResult());
        Assert.assertFalse("Payment UID must not be empty!", StringUtils.isEmpty(payWithBudgetResponse.getPaymentUID()));
        Assert.assertEquals("Response Total Sum equals", actualCharge,payWithBudgetResponse.getActualCharge());
        //Verify if the transaction status is approved
        List<Entity> result = checkBudgetTransaction(payWithBudget, (PayWithBudgetResponse) response);
        Assert.assertEquals("Status status must be approved", result.get(0).getProperty("Status").toString(),"approved");


        String paymentUid = payWithBudgetResponse.getPaymentUID();
        CancelBudgetPayment cancelBudgetPayment = new CancelBudgetPayment();
        cancelBudgetPayment.setPaymentUID(paymentUid);

        IServerResponse responseCancel = cancelBudgetPayment.SendRequestByApiKey(apiKey, CancelBudgetPaymentResponse.class);

        // make sure the response is not Error response
        if (responseCancel instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) responseCancel).getErrorMessage());
        }
        //Verify with DataStore
        //Verify if the transaction status is canceled
        result = checkBudgetTransaction(payWithBudget, (PayWithBudgetResponse) response);
        Assert.assertEquals("Status status must be canceled", result.get(0).getProperty("Status").toString(),"canceled");

    }

    /**
     * C2803 - Cancel Budget by PosID, Branch ID, TransactionID
     */
    @Test
    @Category(serverV2_8.class)
    public  void testCancelBudgetByPosIDBranchIDTransactionIDShouldFail() throws Exception {
        /*
        steps:
        1. verify if the flag AllowCancelMemberBudgetPayment = true && AllowCancelBudgetPaymentByPosIdentifiers = true
        2. send pay with budget , verify if the response 200 ok
        3. cancel the budget by PosID + TransactionID + BranchID
        4. verify if the  data in BudgetTransaction table correct
         */
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(), true),
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_BUDGET_PAYMENT_BY_POS_IDENTIFIERS.key(), true),
                ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false));


        JoinClubResponse member = createMember();

        Customer customer = new Customer();
        customer.setPhoneNumber(member.getPhoneNumber());
        PayWithBudget payWithBudget = new PayWithBudget();
        payWithBudget.getCustomers().add(customer);
        payWithBudget.setTotalSum("-3000");
        payWithBudget.setTimeStamp(String.valueOf(System.currentTimeMillis()));
        payWithBudget.setTransactionId(UUID.randomUUID().toString());
        payWithBudget.setBranchId("1");
        payWithBudget.setPosId("2");

        IServerResponse response = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        String actualCharge = payWithBudget.getTotalSum();
        actualCharge = serverHelper.replaceCharAt(actualCharge, 0, "");

        PayWithBudgetResponse payWithBudgetResponse = (PayWithBudgetResponse) response;
        Assert.assertEquals("status code must be 200", (Integer) 200, payWithBudgetResponse.getStatusCode());
        Assert.assertEquals("Result = Y ", "Y", payWithBudgetResponse.getResult());
        Assert.assertFalse("Payment UID must not be empty!", StringUtils.isEmpty(payWithBudgetResponse.getPaymentUID()));
        Assert.assertEquals("Response Total Sum equals", actualCharge, payWithBudgetResponse.getActualCharge());

        //Verify if the transaction status is approved
        List<Entity> result = checkBudgetTransaction(payWithBudget, (PayWithBudgetResponse) response);
        Assert.assertEquals("Status status must be approved", result.get(0).getProperty("Status").toString(), "approved");

        CancelBudgetPayment cancelBudgetPayment = new CancelBudgetPayment();
        cancelBudgetPayment.setPosId(payWithBudget.getPosId());
        cancelBudgetPayment.setBranchId(payWithBudget.getBranchId());
        cancelBudgetPayment.setTransactionId(payWithBudget.getTransactionId());
        log4j.info("Cancel payment by transactionId: " + payWithBudget.getTransactionId());
        IServerResponse responseCancel = cancelBudgetPayment.SendRequestByApiKey(apiKey, CancelBudgetPaymentResponse.class);

        // make sure the response is not Error response
        if (!(responseCancel instanceof ZappServerErrorResponse)) {
            Assert.fail("Expected request to fail due to wrong params");
        }

        ZappServerErrorResponse responseCancel1 = (ZappServerErrorResponse) responseCancel;
        Assert.assertEquals("Result must be Incorrect params","Incorrect params", responseCancel1.getErrorMessage());

        //Verify with DataStore
        result = checkBudgetTransaction(payWithBudget, (PayWithBudgetResponse) response);
        Assert.assertNotEquals("Status should not be canceled", result.get(0).getProperty("Status").toString(),"canceled");

    }



    /**
     * C2804 - Cancel Budget by PosID, Branch ID, TransactionID, when customer didn't have transaction
     */
    @Test
    public  void testCancelBudgetByPosIDBranchIDTransactionIDCustomerFakeTransaction() throws Exception{

        /*
        steps:
        1. verify if the flag AllowCancelMemberBudgetPayment = true && AllowCancelBudgetPaymentByPosIdentifiers = true
        2. send pay with budget , verify if the response 200 ok
        3. cancel the budget by PosID +  TransactionID + BranchID
        4. verify if we get an error message   "ErrorMessage": "Budget transaction not found"
        5. verify if the status in the data store = approved
         */
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),true),
        ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_BUDGET_PAYMENT_BY_POS_IDENTIFIERS.key(),true),
        ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false));

        JoinClubResponse newMember = createMember();

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        PayWithBudget payWithBudget = new PayWithBudget();
        payWithBudget.getCustomers().add(customer);
        payWithBudget.setTotalSum("-3000");
        payWithBudget.setTimeStamp(String.valueOf(System.currentTimeMillis()));
        payWithBudget.setTransactionId(UUID.randomUUID().toString());
        payWithBudget.setBranchId(UUID.randomUUID().toString());
        payWithBudget.setPosId(UUID.randomUUID().toString());

        IServerResponse response = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        //Verify if the transaction status is approved
        List<Entity> result = checkBudgetTransaction(payWithBudget, (PayWithBudgetResponse) response);
        Assert.assertEquals("Status status must be approved", result.get(0).getProperty("Status").toString(),"approved");

        String actualCharge = payWithBudget.getTotalSum();
        actualCharge = serverHelper.replaceCharAt(actualCharge, 0, "");

        PayWithBudgetResponse payWithBudgetResponse = (PayWithBudgetResponse) response;
        Assert.assertEquals("status code must be 200", (Integer) 200,payWithBudgetResponse.getStatusCode());
        Assert.assertEquals("Result = Y ","Y",payWithBudgetResponse.getResult());
        Assert.assertFalse("Payment UID must not be empty!", StringUtils.isEmpty(payWithBudgetResponse.getPaymentUID()));
        Assert.assertEquals("Response Total Sum equals", actualCharge,payWithBudgetResponse.getActualCharge());

        CancelBudgetPayment cancelBudgetPayment = new CancelBudgetPayment();
        cancelBudgetPayment.setPosId(UUID.randomUUID().toString());
        cancelBudgetPayment.setBranchId(UUID.randomUUID().toString());
        cancelBudgetPayment.setTransactionId(UUID.randomUUID().toString());

        IServerResponse responseCancel = cancelBudgetPayment.SendRequestByApiKey(apiKey, CancelBudgetPaymentResponse.class);

        //Make sure the response is Error response
        Assert.assertTrue("Response must be an error", responseCancel instanceof ZappServerErrorResponse);
        Assert.assertEquals("Incorrect params", ((ZappServerErrorResponse)responseCancel).getErrorMessage()) ;

    }


    private List<Entity> checkBudgetTransaction(PayWithBudget payment, PayWithBudgetResponse paymentResponse) throws Exception {

        log4j.info("Check BudgetTransaction query in data store for UID " + paymentResponse.getPaymentUID());
        Query q = serverHelper.buildQueryForBudgetTransaction(paymentResponse.getPaymentUID());
        List<Entity> result = serverHelper.queryWithWait("Must find one budgetTransaction with the UID: "+ paymentResponse.getPaymentUID(),q, 1, dataStore);
        Assert.assertEquals("ApiKey should be equal",apiKey, result.get(0).getProperty("ApiKey"));
        Assert.assertEquals("BranchID should be equal",payment.getBranchId(), result.get(0).getProperty("BranchID"));
        Assert.assertEquals("BusinessID should be equal",locationId, result.get(0).getProperty("LocationID"));
        Assert.assertEquals("PosId should be equal",payment.getPosId(), result.get(0).getProperty("PosID"));
        Assert.assertEquals("TransactionID should equal",payment.getTransactionId(), result.get(0).getProperty("TransactionID"));
        return result;
    }
}


