package server.utils;

/**
 * Created by Goni on 6/8/2017.
 */
public class UserActionsConstants {

    //UserActions action name
    public static final String TAG_OPERATION = "tag_operation";
    public static final String RECEIVEDASSET = "receivedasset";
    public static final String PUSH_FAILED = "push_failed";
    public static final String PUSH_SENT = "push_sent";
    public static final String PUNCH = "punch";
    public static final String POINTSTRASACTION = "PointTransaction";
    public static final String EXCEEDED_PUNCH = "exceededPunch";
    public static final String UNREGISTER ="unregister";
    public static final String JOINED_CLUB ="joinedclub";
    public static final String UPDATE_MEMBERSHIP = "UpdateMembership";
    public static final String GAVE_CONSENT = "GaveConsent";
    public static final String MEMBER_SMS ="MemberSMS";
    public static final String SEND_POPUP_MSG = "AddMemberMessage";
    public static final String EVENT_EXPORTED ="event_exported";
    public static final String REDEEM_GET_BENEFITS = "RedeemGetBenefits";
    public static final String FORM_FILLED = "formFilled";
}
