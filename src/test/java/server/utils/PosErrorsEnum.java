package server.utils;

import java.util.List;

/**
 * Created by Goni on 9/13/2017.
 */
public enum PosErrorsEnum {


    //PROPRIETARY_PURCHASE_TOTAL_SUM_MISMATCH("4001006","totalSum does not match provided items"), this was removed, the total sum doesn't have to match total items
    PROPRIETARY_CUSTOMER_NOT_FOUND("4001012","One or more customers not found"),
    SINGLE_CUSTOMER_NOT_FOUND("4001012","Customer not found"),
    CUSTOMER_S_NOT_FOUND("4001012","Customer(s) not found"),
    PROPRIETARY_CANCEL_PURCHASE_OPEN_STATUS("4001012","Failed to submit cancellation purchase"),
    PROPRIETARY_PURCHASE_NO_CLUB_MEMBERS_FOUND("4001012","No club members identifiers found"),
    PROPRIETARY_CUSTOMER_MULTIPLE_ARE_NOT_SUPPORTED("4001013","Operation does not support multiple customers"),
    PROPRIETARY_PURCHASE_DUPLICATE_NOT_ALLOWED("4001016","Purchase with POS Identifiers already exists"),
    PROPRIETARY_PAYMENT_INSUFFICIENT_FUNDS("4007001","Insufficient balance"),
    PROPRIETARY_PAYMENT_VERIFICATION_INVALID("4007004","Invalid verification code"),
    PROPRIETARY_PAYMENT_PENDING_VERIFICATION_CODE("4007005","Pending verification code"),
    PROPRIETARY_PAYMENT_MISSING_IDENTIFICATION("4007006","Customer identification not provided and not encoded in verification code"),
    PROPRIETARY_PAYMENT_CHARGE_NOT_ALLOWED("4007008","Top up payment is not allowed"),
    PROPRIETARY_PAYMENT_OVER_MAXIMUM("4007009","Requested sum is over the allowed payment amount"),
    PROPRIETARY_PAYMENT_UNDER_MINIMUM("4007010","Requested sum is under the allowed payment amount"),
    PROPRIETARY_PAYMENT_MISSING_SUM("4007013","Payment sum must be supplied"),
    PROPRIETARY_PAYMENT_CODE_REQUIRED("4007014","Verification code is required"),
    PROPRIETARY_PAYMENT_TRANSACTION_ID_MISSING("4007015","purchase.transactionId is required"),
    PROPRIETARY_PAYMENT_METHOD_IS_NOT_ALLOWED("4007016","%s payment cannot be applied as %s"),
    PROPRIETARY_CANCEL_PAYMENT_NOT_FOUND("4007002","Payment transaction not found"),
    PROPRIETARY_CANCEL_PAYMENT_NOT_CANCELABLE("4007003","Payment transaction could not be canceled"),
    PROPRIETARY_CANCEL_PURCHASE_NOT_CANCELABLE("4001010","Purchase with the supplied confirmation is not in cancellable status"),
    PROPRIETARY_CANCEL_CONFIRMATION_EMPTY("4220000","confirmation may not be empty"),
    DEALS_MUST_CONTAIN_KEY_OR_CODE("4220000","deals[0] must contain either key or code"),
    DEALS_APPLIED_AMOUNT_MUST_CONTAIN_KEY_OR_CODE("4220000","deals[0].appliedAmount must not be null"),
    REDEEM_ASSET_APPLIED_AMOUNT_MUST_CONTAIN_KEY_OR_CODE ("4220000","redeemAssets[0].appliedAmount must not be null"),
    REDEEM_ASSET_MUST_CONTAIN_KEY_OR_CODE("4220000","redeemAssets[0] must contain either key or code");

    private String code;
    private String message;
    private List<String> validationStrings;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }


    // enum constructor - can not be public or protected
    PosErrorsEnum(final String code,final String message){
        this.code = code;
        this.message = message;
    }

    public boolean isValidMessageForCode(String msg){
            return this.message.equals(msg);

    }

}