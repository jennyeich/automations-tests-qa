package server.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Goni on 8/20/2017.
 */

public class RedeemErrorCodesMap {
    private static Map errorCodesMap;

    public static String ASEET_NOT_REDEEMABLE_AT_LOCATION = "5509";
    public static String ASSET_IS_LOCKED_FOR_REDEMPTION = "5510";
    public static String DEACTIVATE_ASSET = "5511";
    public static String ASSET_ALREADY_REDEEMED = "5501";
    public static String VIOLATION_OF_ASSET_NO_BENEFITS = "5523";
    public static String VIOLATION_OF_ASSET_CONDITION = "5513";
    public static String REDEEM_CODE_NOT_FOUND = "6001";
    public static String ASSET_NOT_READY = "5505";
    public static String ASSET_NOT_FOUND = "5525";


    public static Map getRedeemErrorCodesMap() {
        if(errorCodesMap == null)
            initMap();
        return errorCodesMap;
    }

    private static void initMap(){
        errorCodesMap = new HashMap();
        errorCodesMap.put(DEACTIVATE_ASSET,"Deactivated asset");
        errorCodesMap.put(ASSET_IS_LOCKED_FOR_REDEMPTION,"Asset is locked for redemption. Parallel request was received from another source");
        errorCodesMap.put(ASEET_NOT_REDEEMABLE_AT_LOCATION,"Asset not redeemable at this location");
        errorCodesMap.put(ASSET_ALREADY_REDEEMED,"Asset Already Redeemed");
        errorCodesMap.put(VIOLATION_OF_ASSET_NO_BENEFITS,"Violation of asset conditions (no benefits)");
        errorCodesMap.put(VIOLATION_OF_ASSET_CONDITION,"Violation of asset conditions");
        errorCodesMap.put(REDEEM_CODE_NOT_FOUND,"Redeem code not found");
        errorCodesMap.put(ASSET_NOT_READY,"Asset Not Ready");
        errorCodesMap.put(ASSET_NOT_FOUND,"Asset not found");

    }

    public static String getErrorMessage(String code){
       return (String)getRedeemErrorCodesMap().get(code);
    }

}

