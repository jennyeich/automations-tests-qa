package server.utils;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import org.jetbrains.annotations.NotNull;
import server.v2_8.GetMemberBenefits;
import server.v2_8.PayWithBudget;
import server.v2_8.RedeemAsset;
import server.v2_8.common.Customer;
import server.v2_8.common.Item;

import java.util.UUID;

public class V2_8Builder {

    private static String apiKey;

    public V2_8Builder(String apiKey) {
        this.apiKey = apiKey;
    }


    public Item buildItem(String itemCode, String itemName, String depCode, String depName, Integer itemPrice, Integer quantity) {

        Item item = new Item();

        item.setItemCode(itemCode);
        item.setQuantity(quantity);
        item.setAmount(1.2415);
        item.setItemName(itemName);
        item.setDepartmentCode(depCode);
        item.setDepartmentName(depName);
        item.setPrice(itemPrice);
        item.setTags(Lists.newArrayList("DEP1"));
        return item;
    }

    public Customer buildCustomer(String phoneNumber) {
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        return customer;
    }

    public GetMemberBenefits buildGetMemberBenefits( Customer customer,String totalSum,Item item,Item item2) {
        GetMemberBenefits getMemberBenefits = new GetMemberBenefits();
        getMemberBenefits.getCustomers().add(customer);
        getMemberBenefits.setReturnExtendedItems(true);
        getMemberBenefits.setTotalSum(totalSum);

        getMemberBenefits.getItems().add(item);
        getMemberBenefits.getItems().add(item2);
        return  getMemberBenefits;
    }

    public PayWithBudget buildPayWithBudget(Customer customer, String totalSum) {
        PayWithBudget payWithBudget = new PayWithBudget();
        payWithBudget.setAllowPartialPayment(true);
        payWithBudget.getCustomers().add(customer);
        payWithBudget.setTotalSum(totalSum);
        payWithBudget.setTimeStamp("1431430104");
        payWithBudget.setTransactionId(UUID.randomUUID().toString());
        payWithBudget.setBranchId("1");
        payWithBudget.setPosId("2");
        return payWithBudget;
    }


    public RedeemAsset buildRedeemAsset(String memberKey, String assetKey,String locationId,String serverToken) {
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setLocationID(locationId);
        redeemAsset.setAssetKey(assetKey);
        redeemAsset.setMembershipKey(memberKey);
        redeemAsset.setToken(serverToken);
        return redeemAsset;
    }
}
