package server.utils;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import server.v2_8.*;
import server.v2_8.common.*;
import server.v4.RegisterMemberV4;
import server.v4.common.models.RegistrationData;

import java.util.*;

import static java.lang.System.currentTimeMillis;

/**
 * Created by amit.levi on 25/04/2017.
 */
public class Initializer {

    private static final String EMPLOYEE = "Employee";
    private static final String POS_ID = "1";
    private static final String BRANCH_ID = "1";
    private static final String DEPARTMENT_CODE ="123";
    private static final String ITEM_NAME_PREFIX = "item_";
    private static final String CHAIN_ID = "chain123";
    private static final String TOTAL_SUM = "100";
    private static final int NUM_OF_ITEMS = 2;
    public static final String FIRST_NAME = "FirstName";
    public static final String LAST_NAME = "LastName";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String MEMBER_ID ="MemberID";
    public static final String GOV_ID = "GovID";
    public static final String CAR_NUMBER = "GenericInteger1";
    public static final String EMAIL = "Email";
    public static final String EXPIRATION_DATE = "ExpirationDate";
    public static final String BIRTHDAY = "Birthday";
    public static final String ZIP_CODE = "AddressZipCode";
    public static final String DEFAULT_BIRTHDAY = "01.01.1980 08:00:00";
    public static final String ALLOW_SMS = "AllowSMS";
    public static final String ALLOW_EMAIL = "AllowEmail";

    /**
     * Create getMemberDetails Request with purchase content
     * @param customers
     * @param totalSum
     * @param numOfItems
     * @param tags
     * @return
     */
    public static GetMemberDetails initialGetMemberDetailsRequest(List<Customer> customers, String totalSum, int numOfItems, List<String> tags){
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.setPosId(POS_ID);
        getMemberDetails.setBranchId(BRANCH_ID);
        getMemberDetails.setTotalSum(totalSum);
        String timestamp = String.valueOf(System.currentTimeMillis());
        getMemberDetails.setTransactionId(timestamp);
        getMemberDetails.setChainId(CHAIN_ID);
        getMemberDetails.setCashier(EMPLOYEE + timestamp.toString());

        getMemberDetails.setCustomers(customers);

        ArrayList<Item> items = new ArrayList<>();
        int itemPrice = (Integer.valueOf(totalSum) / numOfItems);
        for (int i = 0; i < numOfItems; i++) {
            Item item = new Item();

            item.setItemCode(timestamp.toString());
            item.setQuantity(1);
            item.setAmount(1.2415);
            item.setItemName(ITEM_NAME_PREFIX + (i+1));
            item.setDepartmentCode(DEPARTMENT_CODE);
            item.setDepartmentName("Dep1");
            item.setPrice(itemPrice);

            item.setTags(Lists.newArrayList("DEP1"));
            items.add(item);
        }
        getMemberDetails.setTags(tags);
        getMemberDetails.setItems(items);

        return getMemberDetails;
    }

    public static GetMemberDetails initialGetMemberDetailsRequest(List<Customer> customers){
        List<String> tags = new ArrayList<>();
        tags.add("tag1");
        return initialGetMemberDetailsRequest(customers, TOTAL_SUM, NUM_OF_ITEMS, tags);
    }


    public static ConfigRegistrationV2 initConfigRegistrationV2(String locationId){

        //Configure registration form before adding members
        ConfigRegistrationV2 configRegistrationV2 = new ConfigRegistrationV2();
        configRegistrationV2.setLocationId(locationId);
        configRegistrationV2.addFieldName(new FieldConfig(PHONE_NUMBER, true));
        configRegistrationV2.addFieldName(new FieldConfig(FIRST_NAME, true));
        configRegistrationV2.addFieldName(new FieldConfig(LAST_NAME, true));
        configRegistrationV2.addFieldName(new FieldConfig(GOV_ID, true));
        configRegistrationV2.addFieldName(new FieldConfig(MEMBER_ID, true));
        configRegistrationV2.addFieldName(new FieldConfig(CAR_NUMBER, true));
        configRegistrationV2.addFieldName(new FieldConfig(EMAIL, true));
        configRegistrationV2.addFieldName(new FieldConfig(EXPIRATION_DATE, true));
        configRegistrationV2.addFieldName(new FieldConfig(BIRTHDAY, true));
        configRegistrationV2.addFieldName(new FieldConfig(ALLOW_SMS, true));
        configRegistrationV2.addFieldName(new FieldConfig(ALLOW_EMAIL, true));

        Validations validations = new Validations();
        Map<String,Object> map = new HashMap<>();
        map.put("type","string");
        map.put("pattern","^[0-9]*$");
        validations.addProperties(PHONE_NUMBER,map);

        map = new HashMap<>();
        map.put("type","string");
        validations.addProperties(FIRST_NAME,map);
        validations.addProperties(LAST_NAME,map);
        validations.addProperties(GOV_ID,map);
        validations.addProperties(MEMBER_ID,map);

        map = new HashMap<>();
        map.put("type","string");
        map.put("format","email");
        validations.addProperties(EMAIL,map);

        map = new HashMap<>();
        map.put("type","number");
        validations.addProperties(CAR_NUMBER,map);



        map = new HashMap<>();
        map.put("type","string");
        map.put("pattern","^[0-9]{2}.[0-9]{2}.[0-9]{4}?($| [0-9]{2}:[0-9]{2}:[0-9]{2}$)");
        validations.addProperties(BIRTHDAY,map);
        validations.addProperties(EXPIRATION_DATE,map);

        map = new HashMap<>();
        map.put("type","boolean");
        validations.addProperties(ALLOW_SMS, map);
        validations.addProperties(ALLOW_EMAIL, map);

        configRegistrationV2.setValidations(validations);
        return configRegistrationV2;
    }



    public static RegisterMember initRegisterMember(Map<String,Object> map) {
        RegisterMember registerMember = new RegisterMember();
        for (String key: map.keySet()) {
            registerMember.addFieldNameMap(key, map.get(key));
        }
        return registerMember;
    }


    public static RegisterMember initRegisterMember(String phoneNumber) {
        RegisterMember registerMember = new RegisterMember();
        long timeStamp = currentTimeMillis();
        registerMember.addFieldNameMap(FIRST_NAME, FIRST_NAME + "_" +timeStamp);
        registerMember.addFieldNameMap(LAST_NAME, LAST_NAME + "_" +timeStamp);
        registerMember.addFieldNameMap(PHONE_NUMBER, phoneNumber);
        registerMember.addFieldNameMap(EMAIL, LAST_NAME + "@como.com");
        registerMember.addFieldNameMap(BIRTHDAY, DEFAULT_BIRTHDAY);
        return registerMember;
    }


    public static RegisterMember initRegisterMember(String phoneNumber,String govID, String memberID) {
        RegisterMember registerMember = initRegisterMember(phoneNumber);
        registerMember.addFieldNameMap(GOV_ID,govID);
        registerMember.addFieldNameMap(MEMBER_ID,memberID);
        registerMember.addFieldNameMap(EMAIL,phoneNumber+ "@como.com");
        return registerMember;
    }


    public static RegisterMember initRegisterMember(String phoneNumber,String govID, String memberID, String email, String carNumber) {
        RegisterMember registerMember = initRegisterMember(phoneNumber, govID, memberID);
        registerMember.addFieldNameMap(EMAIL,email);
        registerMember.addFieldNameMap(CAR_NUMBER,carNumber);
        return registerMember;
    }


    public static RegisterMemberV4 initRegisterMemberV4(String apiKey,String phoneNumber) {
        RegisterMemberV4 registerMember = new RegisterMemberV4(apiKey);
        long timeStamp = currentTimeMillis();
        RegistrationData registrationData = new RegistrationData();
        Map<String,Object> fieldMap = new HashMap<>();
        fieldMap.put(Initializer.FIRST_NAME,FIRST_NAME + "_" +timeStamp);
        fieldMap.put(Initializer.LAST_NAME,LAST_NAME + "_" +timeStamp);
        fieldMap.put(Initializer.PHONE_NUMBER, phoneNumber);
        fieldMap.put( Initializer.EMAIL,LAST_NAME + "@como.com");
        registrationData.setFieldNameMap(fieldMap);
        registerMember.setRegistrationData(registrationData);
        return registerMember;
    }

    public static SubmitPurchase initSubmitPurchase(String phoneNumber){
        SubmitPurchase submitPurchase = new SubmitPurchase();
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        submitPurchase.getCustomers().add(customer);
        submitPurchase.setBranchId("3");
        submitPurchase.setPosId("13");
        submitPurchase.setTotalSum("4000");
        submitPurchase.setTransactionId(UUID.randomUUID().toString());
        submitPurchase.setChainId("chainIDAutomation");
        submitPurchase.setCashier("cashier1");
        submitPurchase.setTimeStamp(Long.valueOf(System.currentTimeMillis()));
        List<Payment> payments = new ArrayList();
        payments.add(new Payment("Cash", "initGenericSubmitPurchase", "2000"));
        payments.add(new Payment("CreditCard", "initGenericSubmitPurchase", "2000"));
        submitPurchase.setPayments(payments);

        ArrayList<Item> items = new ArrayList<>();
        items.add(buildPurchaseItem("4454","LiorItem1","554","Dep3",4000,1,Lists.newArrayList("Tag1","Tag2")));

        submitPurchase.setItems(items);

        return submitPurchase;
    }

    public static Item buildPurchaseItem(String code,String name,String depCode,String depName,Integer price, Integer quantity,ArrayList<String> tags){
        Item item = new Item();
        item.setItemCode(code);
        item.setQuantity(quantity);
        item.setAmount((double) 1);
        item.setItemName(name);
        item.setDepartmentCode(depCode);
        item.setDepartmentName(depName);
        item.setPrice(price);
        item.setLineID(UUID.randomUUID().toString());
        item.setTags(tags);
        return item;
    }

    public static ConfigRegistration initConfigRegistration(String locationId) {
        ConfigRegistration configRegistration = new ConfigRegistration();
        configRegistration.setLocationId(locationId);
        configRegistration.setIdentificationField(Initializer.PHONE_NUMBER);
        Map<String,String> fieldMap = new HashMap<>();
        fieldMap.put(Initializer.PHONE_NUMBER,Initializer.PHONE_NUMBER);
        fieldMap.put(Initializer.FIRST_NAME,Initializer.FIRST_NAME);
        fieldMap.put(Initializer.LAST_NAME,Initializer.LAST_NAME);
        fieldMap.put(Initializer.EMAIL,Initializer.EMAIL);
        fieldMap.put(Initializer.MEMBER_ID, Initializer.MEMBER_ID);
        fieldMap.put(Initializer.GOV_ID, Initializer.GOV_ID);
        fieldMap.put(Initializer.CAR_NUMBER, Initializer.CAR_NUMBER);
        fieldMap.put(Initializer.EXPIRATION_DATE,Initializer.EXPIRATION_DATE);
        configRegistration.setFieldNameMap(fieldMap);
        return  configRegistration;
    }

    public static ConfigRegistration initConfigRegistration(String locationId, Map<String,String> fieldMap) {
        ConfigRegistration configRegistration = new ConfigRegistration();
        configRegistration.setLocationId(locationId);
        configRegistration.setIdentificationField(Initializer.PHONE_NUMBER);
        configRegistration.setFieldNameMap(fieldMap);
        return  configRegistration;
    }

    public static Map<String,String> initUnsubscribeConfigRegistration() {
        Map<String,String> fieldMap = new HashMap<>();
        fieldMap.put(Initializer.PHONE_NUMBER, Initializer.PHONE_NUMBER);
        fieldMap.put(Initializer.FIRST_NAME, Initializer.FIRST_NAME);
        fieldMap.put(Initializer.LAST_NAME, Initializer.LAST_NAME);
        fieldMap.put(Initializer.EMAIL, Initializer.EMAIL);
        fieldMap.put(Initializer.ALLOW_SMS, Initializer.ALLOW_SMS);
        fieldMap.put(Initializer.ALLOW_EMAIL,Initializer.ALLOW_EMAIL);
        return fieldMap;
    }


    public static Map<String,String> initConfigRegistrationNoEmailSMS() {

        Map<String,String> fieldMap = new HashMap<>();
        fieldMap.put(Initializer.PHONE_NUMBER, Initializer.PHONE_NUMBER);
        fieldMap.put(Initializer.FIRST_NAME, Initializer.FIRST_NAME);
        fieldMap.put(Initializer.LAST_NAME, Initializer.LAST_NAME);
        fieldMap.put(Initializer.EMAIL, Initializer.EMAIL);

        return fieldMap;
    }
}
