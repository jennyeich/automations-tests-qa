package server.utils;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import common.BaseTest;
import org.junit.Assert;
import server.common.IServerResponse;
import server.v4.*;
import server.v4.common.enums.OrderType;
import server.v4.common.enums.Status;
import server.v4.common.enums.Type;
import server.v4.common.models.*;

import java.util.*;

/**
 * Created by Goni on 7/2/2017.
 */
public class V4Builder {

    private static String apiKey;

    public V4Builder(String apiKey){
        this.apiKey = apiKey;
    }

    public SubmitPurchase buildSubmitPurchase(String customerPhone, Purchase purchase) {
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setDefauldHeaderParams();
        Customer customer = new Customer();
        customer.setPhoneNumber(customerPhone);
        submitPurchase.setCustomers(Lists.newArrayList(customer));
        submitPurchase.setPurchase(purchase);
        return submitPurchase;
    }

    public SubmitPurchase buildSubmitPurchase(List<Customer> customers, Purchase purchase) {
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setDefauldHeaderParams();
        submitPurchase.setCustomers(customers);
        submitPurchase.setPurchase(purchase);
        return submitPurchase;
    }

    public PaymentAPI buildPaymentAPI(PaymentQueryParams queryParams, Customer customer, int amount , Purchase purchase ,String verificationCode){
        PaymentAPI payment = new PaymentAPI();
        payment.setQueryParameters(queryParams.getParams());
        payment.setApiKey(apiKey);
        payment.setDefauldHeaderParams();
        payment.setCustomer(customer);
        payment.setPurchase(purchase);
        payment.setAmount(amount);
        payment.setVerificationCode(verificationCode);
        return payment;
    }

    public PaymentAPI buildPaymentAPI(PaymentQueryParams queryParams, Customer customer, int amount , Purchase purchase){
       return buildPaymentAPI(queryParams,customer,amount,purchase,null);
    }

    public CancelPayment buildCancelPaymentRequest(String confirmation , Purchase purchase){
        CancelPayment cancelPayment = new CancelPayment();
        cancelPayment.setConfirmation(confirmation);
        cancelPayment.setPurchase(purchase);
        cancelPayment.setDefauldHeaderParams();
        cancelPayment.setApiKey(apiKey);
        return cancelPayment;
    }

    public Purchase buildPurchase(String uuid, int totalAmount, List<PurchaseItem> items){
        Purchase purchase = new Purchase();
        purchase.setOpenTime(System.currentTimeMillis());
        purchase.setTransactionId(uuid);
        purchase.setTotalAmount(totalAmount);
        purchase.setItems(items);

        return purchase;
    }

    public Purchase buildPurchaseWithOpenTime(String uuid, int totalAmount, List<PurchaseItem> items, String openTime){
        Purchase purchase = new Purchase();
        purchase.setOpenTime(Long.valueOf(openTime));
        purchase.setTransactionId(uuid);
        purchase.setTotalAmount(totalAmount);
        purchase.setItems(items);

        return purchase;
    }


    public SubmitPurchase submitPurchaseRequest (List<Customer> customers, List<RedeemAsset> redeemAssets, List<Deal> deals, Purchase purchase) throws Exception{

        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setDefauldHeaderParams();
        submitPurchase.setCustomers(customers);


        if (redeemAssets.size() > 0)
            submitPurchase.setRedeemAssets(redeemAssets);
        if (deals.size() > 0)
            submitPurchase.setDeals(deals);

        submitPurchase.setPurchase(purchase);

        return submitPurchase;

    }

    public Purchase buildPurchase(String uuid,int totalAmount,List<PurchaseItem> items,List<Payment> payments,int totalGeneralDiscount) {
        Purchase purchase = new Purchase();
        purchase.setOpenTime(System.currentTimeMillis());
        purchase.setTransactionId(uuid);
        purchase.setTotalAmount(totalAmount);
        purchase.setItems(items);
        purchase.setMeansOfPayment(payments);
        purchase.setStatus(Status.FINAL);
        purchase.setCancellation(true);
        purchase.setOrderType(OrderType.TAKE_AWAY);
        purchase.setTotalGeneralDiscount(totalGeneralDiscount);
        return purchase;
    }

    public  PurchaseItem buildPurchaseItem(String code, int quantity, int netAmount ){
        PurchaseItem purchaseItem = new PurchaseItem();
        purchaseItem.setCode(code);
        purchaseItem.setQuantity(quantity);
        purchaseItem.setNetAmount(netAmount);
        purchaseItem.setName("item1");
        purchaseItem.setLineId(UUID.randomUUID().toString());
        return purchaseItem;
    }

    public  PurchaseItem buildPurchaseItem(String itemCode){
        PurchaseItem purchaseItem = new PurchaseItem();
        purchaseItem.setCode(itemCode);
        purchaseItem.setDepartmentCode("DEP1");
        purchaseItem.setDepartmentName("DEP1");
        purchaseItem.setQuantity(1);
        purchaseItem.setNetAmount(1);
        purchaseItem.setGrossAmount(1);
        purchaseItem.setName("item1");
        purchaseItem.setLineId(UUID.randomUUID().toString());
        return purchaseItem;
    }

    public  PurchaseItem buildPurchaseItem(String code, int quantity, int netAmount, String name ){
        PurchaseItem purchaseItem = buildPurchaseItem (code, quantity, netAmount, "111", "Dep1");
        purchaseItem.setName(name);
        purchaseItem.setGrossAmount(quantity);
        purchaseItem.setLineId(UUID.randomUUID().toString());
        return purchaseItem;
    }


    public  PurchaseItem buildPurchaseItem(String code, int quantity, int netAmount, String depCode, String depName){
        PurchaseItem purchaseItem = buildPurchaseItem(code,quantity,netAmount);
        purchaseItem.setCode(code);
        purchaseItem.setDepartmentCode(depCode);
        purchaseItem.setDepartmentName(depName);
        purchaseItem.setQuantity(quantity);
        purchaseItem.setNetAmount(netAmount);
        purchaseItem.setGrossAmount(netAmount);
        return purchaseItem;
    }

    public PurchaseItem buildPurchaseItem(int price,int quantity){
        PurchaseItem item = new PurchaseItem();
        item.setCode("item123");
        item.setName("item1");
        item.setDepartmentCode("111");
        item.setDepartmentName("Dep1");
        item.setQuantity(quantity);
        item.setNetAmount(price);
        item.setGrossAmount(quantity);
        item.setLineId(UUID.randomUUID().toString());
        //item.setTags(Lists.newArrayList("tag1","tag2"));
        return item;
    }

    public Payment buildPayment(Type type,int sum) {
        Payment payment = new Payment();
        payment.setType(type);
        payment.setSum(sum);
        return payment;
    }

    public Purchase buildPurchase(String uuid) {
        Purchase purchase = new Purchase();
        purchase.setOpenTime(System.currentTimeMillis());
        purchase.setTransactionId(uuid);
        purchase.setTotalAmount(100);
        return purchase;
    }

    public Purchase buildDefaultPurchase(String uuid) {
        Purchase purchase = buildPurchase(uuid);
        PurchaseItem purchaseItem = buildPurchaseItem(100,1);
        purchase.setItems(Lists.newArrayList(purchaseItem));
        purchase.setTotalAmount(100);
        purchaseItem.setLineId(UUID.randomUUID().toString());
        return purchase;
    }

    public Membership getMember(Customer customer){

        GetMemberDetailsResponse getMemberDetailsResponse = getGetMemberDetailsResponse(customer);
        Assert.assertNotNull("GetMemberDetailsResponse should have one membership",getMemberDetailsResponse.getMembership());
        Membership member = getMemberDetailsResponse.getMembership();
        return member;
    }

    public GetMemberDetailsResponse getGetMemberDetailsResponse(Customer customer) {
        GetMemberDetails memberDetails = buildMemberDetailsRequest(customer);
        return getGetMemberDetailsResponse(memberDetails);
    }

    public int getMembersPoints(Customer customer){

        GetMemberDetailsResponse getMemberDetailsResponse = getGetMemberDetailsResponse(customer);
        Assert.assertNotNull("GetMemberDetailsResponse should have one membership",getMemberDetailsResponse.getMembership());
        int points = getMemberDetailsResponse.getMembership().getPointsBalance().getBalance().getMonetary();
        return points;
    }

    public  GetMemberDetailsResponse getGetMemberDetailsResponse(GetMemberDetails memberDetails) {

        return (GetMemberDetailsResponse)memberDetails.SendRequestByApiKeyAndValidateResponse(apiKey,GetMemberDetailsResponse.class);
    }

    public  SubmitEventResponse submitEvent(Customer customer) {
        SubmitEvent submitEvent = new SubmitEvent(apiKey);
        Event event = buildEvent();
        submitEvent.setEvent(event);
        submitEvent.setCustomers(Lists.newArrayList(customer));
        return (SubmitEventResponse)submitEvent.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitEventResponse.class);
    }

    public GetBenefitsResponse getMemberBenefitsResponse (GetBenefits getBenefits){
        //Send the request
        IServerResponse response = getBenefits.SendRequestByApiKey(apiKey, GetBenefitsResponse.class);

        // make sure the response is not Error response
        if(response instanceof server.v4.base.ZappServerErrorResponse){
            Assert.fail(((server.v4.base.ZappServerErrorResponse)response).getErrorMessage());
        }

        // Result true assertion
        GetBenefitsResponse getBenefitsResponse = (GetBenefitsResponse) response;
        Assert.assertEquals("Status code must be 200", BaseTest.httpStatusCode200, getBenefitsResponse.getStatusCode());
        return getBenefitsResponse;

    }


    public GetBenefits buildGetMemberBenefitsRequest(Customer customer) {
        GetBenefits getBenefits = new GetBenefits(apiKey);
        getBenefits.setCustomers(Lists.newArrayList(customer));
        Purchase purchase = buildPurchase(UUID.randomUUID().toString());
        PurchaseItem purchaseItem = buildPurchaseItem(100,1);
        purchase.setItems(Lists.newArrayList(purchaseItem));
        purchase.setTotalAmount(100);
        getBenefits.setPurchase(purchase);
        return getBenefits;
    }

    public GetBenefits buildGetBenefitsWithAssetKey(Customer customer,ArrayList<RedeemAsset>redeemAssets,Purchase purchase) {

        GetBenefits getBenefits = new GetBenefits(apiKey);
        getBenefits.setCustomers(Lists.newArrayList(customer));
        getBenefits.setPurchase(purchase);
        if (redeemAssets.size()>0)
            getBenefits.setRedeemAssets(redeemAssets);
        return getBenefits;

    }

    public UpdateMemberResponse updateMemberResponse (Customer customer) {

        UpdateMember updateMember = new UpdateMember(apiKey);
        updateMember.setCustomer(customer);
        RegistrationData registrationData = new RegistrationData();
        Map<String,Object> fieldMap = new HashMap<>();
        fieldMap.put(Initializer.EMAIL,"email@como.com");
        registrationData.setFieldNameMap(fieldMap);
        updateMember.setRegistrationData(registrationData);
        return (UpdateMemberResponse) updateMember.SendRequestByApiKeyAndValidateResponse(apiKey,UpdateMemberResponse.class);


    }


    public GetMemberDetailsResponse getGetMemberDetailsNotValidateResponse(Customer customer) {
        GetMemberDetails memberDetails = new GetMemberDetails();
        memberDetails.setApiKey(apiKey);
        memberDetails.setDefauldHeaderParams();
        memberDetails.setCustomer(customer);
        return (GetMemberDetailsResponse)memberDetails.SendRequestByApiKey(apiKey,GetMemberDetailsResponse.class);
    }

    public GetMemberDetailsResponse getMemberDetailsWithQueryParamsResponse(Customer customer,MemberDetailsQueryParams setQueryParams) {
        GetMemberDetails memberDetails = new GetMemberDetails();
        memberDetails.setApiKey(apiKey);
        memberDetails.setBranchId("123");
        memberDetails.setPosId("1324");
        memberDetails.setxSourceName("123424");
        memberDetails.setxSourceVersion("2332");
        memberDetails.setxSourceType("234346");
        memberDetails.setCustomer(customer);
        memberDetails.setQueryParameters(setQueryParams.getParams());
        return (GetMemberDetailsResponse)memberDetails.SendRequestByApiKeyAndValidateResponse(apiKey,GetMemberDetailsResponse.class);
    }

    public SubmitPurchaseResponse submitPurchaseResponse(Customer customer) {
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setDefauldHeaderParams();
        submitPurchase.setCustomers(Lists.newArrayList(customer));


        //build purchase
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = buildPurchaseItem(500,1);
        Purchase purchase = buildPurchase(uuid,500, Lists.newArrayList(purchaseItem1)) ;

        submitPurchase.setPurchase(purchase);
        return (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
    }

    public int getMembersCredit(Customer customer){

        GetMemberDetailsResponse getMemberDetailsResponse = getGetMemberDetailsResponse(customer);
        Assert.assertNotNull("GetMemberDetailsResponse should have one membership",getMemberDetailsResponse.getMembership());
        int credit = getMemberDetailsResponse.getMembership().getCreditBalance().getBalance().getMonetary();
        return credit;
    }

    public Event buildEvent (){

        Event event = new Event();
        event.setType("fax");
        event.setSubType("received");
        Date date = new Date();
        date.setTime(55555555);
        event.setTime(date);

        Data data = new Data ();
        data.setContent("0101001010000010010001001010010001010010100001");
        data.setOverLine("08-9458495775");
        data.setSound("la lala");
        data.setSentTimeInMinutes(343);
        data.setProtocol("smartFax 3.0");
        event.setData(data);

        return event;

    }

    public GetMemberDetails buildMemberDetailsRequest(Customer customer) {
        GetMemberDetails memberDetails = new GetMemberDetails();
        memberDetails.setApiKey(apiKey);
        memberDetails.setDefauldHeaderParams();
        memberDetails.setCustomer(customer);
        return memberDetails;
    }

    public PaymentProviderConfigurations buildPaymentProviderConfigurations(String paymentMethod,boolean allowPartialPayment){
        PaymentProviderConfigurations paymentProviderConfigurations = new PaymentProviderConfigurations();
        paymentProviderConfigurations.setPaymentWallet(new PaymentConfig(paymentMethod,allowPartialPayment));
        paymentProviderConfigurations.setCreditCard(new CreditCardPaymentConfig(paymentMethod,false));//allways false
        paymentProviderConfigurations.setGiftCard(new PaymentConfig(paymentMethod,allowPartialPayment));
        return  paymentProviderConfigurations;
    }

    public SubmitPurchaseResponse submitPurchaseResponse(Customer customer, List<RedeemAsset> redeemAssets) {
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setDefauldHeaderParams();
        submitPurchase.setCustomers(Lists.newArrayList(customer));
        submitPurchase.setRedeemAssets(redeemAssets);
        //build purchase
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = buildPurchaseItem(500,1);
        Purchase purchase = buildPurchase(uuid,500, Lists.newArrayList(purchaseItem1)) ;

        submitPurchase.setPurchase(purchase);
        return (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
    }


    public SubmitPurchaseResponse submitPurchaseResponseDeals(Customer customer, List<Deal> deals) {
        SubmitPurchase submitPurchase = new SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setDefauldHeaderParams();
        submitPurchase.setCustomers(Lists.newArrayList(customer));
        submitPurchase.setDeals(deals);
        //build purchase
        String uuid = UUID.randomUUID().toString();
        PurchaseItem purchaseItem1 = buildPurchaseItem(500,1);
        Purchase purchase = buildPurchase(uuid,500, Lists.newArrayList(purchaseItem1)) ;

        submitPurchase.setPurchase(purchase);
        return (SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
    }

    public List<PurchaseItem> buildSubmitPurchaseItemsList(){
        List<PurchaseItem> items = Lists.newArrayList();
        for(int i = 1; i<= 10; i++){
            items.add(buildPurchaseItem("item"+i,1,500, "itemName" + i));
        }
        return items;
    }


}
