package server.utils;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import com.mashape.unirest.http.HttpResponse;
import common.BaseTest;
import common.LocalDatastore;
import hub.common.objects.member.NewMember;
import hub.services.assets.codes.POSRedeemCodesService;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import server.common.*;
import server.common.asset.*;
import server.common.asset.conditions.AssetDateCondition;
import server.internal.*;
import server.internal.SignOnResponse;
import server.internal.friendReferral.ReferralCodeGenerate;
import server.internal.friendReferral.ReferralCodeValidate;
import server.v2_8.*;
import server.v2_8.SignOn;
import server.v2_8.SubmitPurchase;
import server.v2_8.SubmitPurchaseResponse;
import server.v2_8.UpdateAsset;
import server.v2_8.base.ServicesErrorResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Customer;
import server.v2_8.common.GiftShopItem;
import server.v2_8.common.Item;
import server.v2_8.common.Membership;
import server.v4.GetMemberDetails;
import server.v4.GetMemberDetailsResponse;
import server.v4.*;
import server.v4.common.enums.ReturnAssetsStatus;
import server.v4.common.models.*;
import server.v4.common.models.RedeemAsset;
import utils.PropsReader;

import java.io.IOException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;
import java.util.stream.Collectors;

import static java.lang.System.currentTimeMillis;


/**
 * Created by israel on 06/04/2017.
 */
public class ServerHelper {

    public static final String APP = "App";
    private static String locationId;
    private static String apiKey;
    private static String serverToken;
    private final String MEMBERS_RELATION = "MembersRelation";


    public static final Logger log4j = Logger.getLogger(ServerHelper.class);

    public ServerHelper(String locationId, String apiKey, String serverToken) {
        this.locationId = locationId;
        this.apiKey = apiKey;
        this.serverToken = serverToken;

    }

    public List<Entity> getBusinessFromApiClient(LocalDatastore datastore) throws InterruptedException {
        Query q = new Query("ApiClient");
        q.setFilter(new Query.FilterPredicate("BusinessID", Query.FilterOperator.EQUAL, locationId));
        return queryWithWait("Must have one result for business in ApiClient" + locationId, q, 1, datastore);
    }

    public void saveApiClient(String flag, Object value){

        UpdateApiClient saveApiClientRequest = new UpdateApiClient(serverToken);
        saveApiClientRequest.addProp(flag, value);
        IServerResponse saveApiClientResponse = saveApiClientRequest.sendRequestByLocation(locationId, Response.class);
        Assert.assertEquals("status code must be 200", saveApiClientResponse.getStatusCode(), (Integer) 200);
        //sleep to wait for data store to update
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
           log4j.error(e);
        }
    }

    public void updateBusinessBackend(String flag, Object value) {

        UpdateBusinessBackendBuilder updateBusinessBackendBuilder = new UpdateBusinessBackendBuilder();
        UpdateBusinessBackend updateBusinessBackendRequest = updateBusinessBackendBuilder.build(locationId);
        updateBusinessBackendRequest.getProp().put(flag, value);

        IServerResponse updateBusinessBackendResponse = updateBusinessBackendRequest.SendRequestByToken(serverToken, Response.class);
        Assert.assertEquals("status code must be 200", updateBusinessBackendResponse.getStatusCode(), (Integer) 200);

    }

    public void updateBusinessBackend(Map<String,Object> props) {

        UpdateBusinessBackendBuilder updateBusinessBackendBuilder = new UpdateBusinessBackendBuilder();
        UpdateBusinessBackend updateBusinessBackendRequest = updateBusinessBackendBuilder.build(locationId);
        updateBusinessBackendRequest.setMap(props);

        IServerResponse updateBusinessBackendResponse = updateBusinessBackendRequest.SendRequestByToken(serverToken, Response.class);
        Assert.assertEquals("status code must be 200", updateBusinessBackendResponse.getStatusCode(), (Integer) 200);

    }

    public void updateBusinessBackend(ApiPair... pairs) {

        Map<String, Object> props = new HashMap<>();
        for (ApiPair pair : pairs)
        {
            props.put(pair.getKey(),pair.getValue());
        }

        updateBusinessBackend(props);
    }

    public void setDefaultConfigurationForPayment(String PayWithBudgetType) throws Exception{
        setConfigurationForPayment("false",PayWithBudgetType,"1","false");
    }

    public void setConfigurationForPayment(String PaymentRequiresVerificationCode,String PayWithBudgetType,String PayWithBudgetRatio,String allowNegativePointBalance){

        saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),Boolean.valueOf(PaymentRequiresVerificationCode));
        Map<String, Object> props = new HashMap<>();
        props.put(LocationBackendFields.PAY_WITH_BUDGET_MIN_SUM.key(), "-1");
        props.put(LocationBackendFields.PAY_WITH_BUDGET_MAX_SUM.key(), "-1");
        props.put(LocationBackendFields.PAY_WITH_BUDGET_TYPE.key(),PayWithBudgetType);
        props.put(LocationBackendFields.PAY_WITH_BUDGET_RATIO.key(),PayWithBudgetRatio);
        props.put(LocationBackendFields.ALLOW_NEGATIVE_POINT_BALANCE.key(),allowNegativePointBalance);
        updateBusinessBackend(props);
    }

    public void configureDefaultBackendForPayWithBudget(){
        saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);
        Map<String, Object> props = new HashMap<>();
        props.put(LocationBackendFields.SUBMIT_PURCHASE_BUDGET_TYPE.key(), PaymentType.BUDGET.type());
        props.put(LocationBackendFields.PAY_WITH_BUDGET_TYPE.key(), PaymentType.BUDGET.type());
        props.put(LocationBackendFields.PURCHASE_ASSET_BUDGET_TYPE.key(), PaymentType.BUDGET.type());
        props.put(LocationBackendFields.PAY_WITH_BUDGET_MIN_SUM.key(), "-1");
        props.put(LocationBackendFields.PAY_WITH_BUDGET_MAX_SUM.key(), "-1");
        updateBusinessBackend(props);
    }

    public void configureBackendForPointsPayment(){
        saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);
        Map<String, Object> props = new HashMap<>();
        props.put(LocationBackendFields.SUBMIT_PURCHASE_BUDGET_TYPE.key(), PaymentType.POINTS.type());
        props.put(LocationBackendFields.PAY_WITH_BUDGET_TYPE.key(), PaymentType.POINTS.type());
        props.put(LocationBackendFields.PAY_WITH_BUDGET_MIN_SUM.key(), "-1");
        props.put(LocationBackendFields.PAY_WITH_BUDGET_MAX_SUM.key(), "-1");
        updateBusinessBackend(props);
    }
    public void validatePurchaseResponse(SubmitPurchaseResponse submitPurchaseResponse, String totalSum, String totalItems) {

        //Cast to SubmitPurchaseResponse object to fetch all response data
        Assert.assertEquals("status code must be 200", (Integer) 200, submitPurchaseResponse.getStatusCode());
        Assert.assertEquals("Result = Y ", "Y", submitPurchaseResponse.getResult());
        Assert.assertFalse("Confirmation must not be empty!", StringUtils.isEmpty(submitPurchaseResponse.getConfirmation()));
        Assert.assertEquals("Response Total Sum equals", totalSum, submitPurchaseResponse.getExpectedTotalSum());
        Assert.assertEquals("TotalItems contains the correct number of item ", totalItems, submitPurchaseResponse.getTotalItems());
    }

    public static String replaceCharAt(String s, int pos, String c) {
        return s.substring(0, pos) + c + s.substring(pos + 1);
    }

    public void validatePurchaseResult(SubmitPurchase submitPurchase, Entity res) throws InterruptedException {

        Assert.assertEquals("ApiKey should be equal", apiKey, res.getProperty("ApiKey"));
        Assert.assertEquals("ChainID should be equal", submitPurchase.getChainId(), res.getProperty("ChainID"));
        Assert.assertEquals("BranchID should be equal", submitPurchase.getBranchId(), res.getProperty("BranchID"));
        Assert.assertEquals("PosID should be equal", submitPurchase.getPosId(), res.getProperty("PosID"));
        Assert.assertEquals("BusinessID should be equal", locationId, res.getProperty("BusinessID"));
        Assert.assertEquals("Cashier should be equal", submitPurchase.getCashier(), res.getProperty("Cashier"));
        Assert.assertEquals("TotalSum should be equal", Long.valueOf(submitPurchase.getTotalSum()), res.getProperty("TotalSum"));
        Assert.assertEquals("TransactionID should be equal", submitPurchase.getTransactionId(), res.getProperty("TransactionID"));
    }

    public void validatePurchaseItemResult (SubmitPurchase submitPurchase, Item item, Entity res) throws Exception {

        Assert.assertEquals("ApiKey should be equal", apiKey, res.getProperty("ApiKey"));
        Assert.assertEquals("LineID must be equal: ", item.getLineID(), res.getProperty("LineID"));
        Assert.assertEquals("BranchID should be equal", submitPurchase.getBranchId(), res.getProperty("BranchID"));
        Assert.assertEquals("PosID should be equal", submitPurchase.getPosId(), res.getProperty("PosID"));
        Assert.assertEquals("BusinessID should be equal", locationId, res.getProperty("BusinessID"));
        Assert.assertEquals("DepCode should be equal", item.getDepartmentCode(), res.getProperty("DepartmentCode"));
        Assert.assertEquals("DepName should be equal", item.getDepartmentName(), res.getProperty("DepartmentName"));
        Assert.assertEquals("ItemCode should be equal", item.getItemCode(), res.getProperty("ItemCode"));
        Assert.assertEquals("ItemName should be equal", item.getItemName(), res.getProperty("ItemName"));
        Assert.assertEquals("TransactionID should be equal", submitPurchase.getTransactionId(), res.getProperty("TransactionID"));


    }


    public void validatePurchaseItemResultv4 (server.v4.SubmitPurchase submitPurchase, PurchaseItem item, Entity res) throws Exception {

        Assert.assertEquals("ApiKey should be equal", apiKey, res.getProperty("ApiKey"));
        Assert.assertEquals("LineID must be equal: ", item.getLineId(), res.getProperty("LineID"));
        Assert.assertEquals("BranchID should be equal", submitPurchase.getBranchId(), res.getProperty("BranchID"));
        Assert.assertEquals("PosID should be equal", submitPurchase.getPosId(), res.getProperty("PosID"));
        Assert.assertEquals("BusinessID should be equal", locationId, res.getProperty("BusinessID"));
        Assert.assertEquals("DepCode should be equal", item.getDepartmentCode(), res.getProperty("DepartmentCode"));
        Assert.assertEquals("DepName should be equal", item.getDepartmentName(), res.getProperty("DepartmentName"));
        Assert.assertEquals("ItemCode should be equal", item.getCode(), res.getProperty("ItemCode"));
        Assert.assertEquals("ItemName should be equal", item.getName(), res.getProperty("ItemName"));
        Assert.assertEquals("TransactionID should be equal", submitPurchase.getPurchase().getTransactionId(), res.getProperty("TransactionID"));


    }

    public void validatePurchaseResult(server.v4.SubmitPurchase submitPurchase, Entity res) throws InterruptedException {

        Assert.assertEquals("ApiKey should be equal", apiKey, res.getProperty("ApiKey"));
        verifyPurchase(submitPurchase, res);

    }

    private void verifyPurchase(server.v4.SubmitPurchase submitPurchase, Entity res) {
        Assert.assertEquals("ChainID should be equal", submitPurchase.getChainId(), res.getProperty("ChainID"));
        Assert.assertEquals("BranchID should be equal", submitPurchase.getBranchId(), res.getProperty("BranchID"));
        Assert.assertEquals("PosID should be equal", submitPurchase.getPosId(), res.getProperty("PosID"));
        Assert.assertEquals("BusinessID should be equal", locationId, res.getProperty("BusinessID"));
        Assert.assertEquals("TotalSum should be equal", Long.valueOf(submitPurchase.getPurchase().getTotalAmount()), res.getProperty("TotalSum"));
        Assert.assertEquals("TransactionID should be equal", submitPurchase.getPurchase().getTransactionId(), res.getProperty("TransactionID"));
        Assert.assertEquals("TransactionSource should be equal", submitPurchase.getRequestHeaderParams().get("X-Source-Type"), res.getProperty("TransactionSource"));
    }

    public void validatePurchaseResult(server.v4.SubmitPurchase submitPurchase, Entity res,String newApiKey) throws InterruptedException {

        Assert.assertEquals("ApiKey should be equal", newApiKey, res.getProperty("ApiKey"));
        verifyPurchase(submitPurchase, res);

    }

    public server.v4.SubmitPurchase buildSubmitPurchaseRequest (ArrayList<server.v4.common.models.Customer> customers, ArrayList<RedeemAsset> redeemAssets, ArrayList<Deal> deals, Purchase purchase) throws Exception{

        server.v4.SubmitPurchase submitPurchase = new server.v4.SubmitPurchase();
        submitPurchase.setApiKey(apiKey);
        submitPurchase.setDefauldHeaderParams();
        submitPurchase.setCustomers(customers);


        if (redeemAssets.size() > 0)
            submitPurchase.setRedeemAssets(redeemAssets);
        if (deals.size() > 0)
            submitPurchase.setDeals(deals);

        submitPurchase.setPurchase(purchase);

        return submitPurchase;

    }

    public IServerResponse submitPurchaseResponse (server.v4.SubmitPurchase submitPurchase, String newApiKey){

        IServerResponse response =  (server.v4.SubmitPurchaseResponse) submitPurchase.SendRequestByApiKeyAndValidateResponse(newApiKey, server.v4.SubmitPurchaseResponse.class);
        if(!(((server.v4.SubmitPurchaseResponse) response).getStatus().equals("ok")))
            Assert.fail(getError(response).get(BaseTest.MESSAGE).toString());
        //Verify the returned customer is as expected
        Assert.assertNotNull("Verify the confirmation is not null",((server.v4.SubmitPurchaseResponse)response).getConfirmation());

        return response;
    }

    public JSONObject getError(IServerResponse response){
        return ((JSONObject)((JSONArray)(response.getRawObject().get(BaseTest.ERRORS))).get(0));
    }

    public boolean dataContainsConfirmation(List<Entity> entities, String confirmation) {
        for (Entity entity : entities) {
            if (((Text) entity.getProperty("Data")).getValue().contains(confirmation)) {
                return true;
            }
        }
        return false;
    }

    public static List<Entity> queryWithWaitUntilResultEqOrGreat(String errorMessage, Query q, int expectedNumberOfResult, LocalDatastore datastore, int timeoutMillsec) throws InterruptedException {
        int maxTimeoutmillsec = timeoutMillsec;
        int intervals = 1000;
        List<Entity> result = new ArrayList<>();
        while (maxTimeoutmillsec > 0) {
            result = datastore.findByQuery(q);
            if (result.size() >= expectedNumberOfResult) {
                return result;
            }
            intervals = intervals*2;
            Thread.sleep(intervals);
            maxTimeoutmillsec -= intervals;
        }
        Assert.fail(errorMessage);
        return null;
    }

    public static List<Entity> queryWithWait(String errorMessage, Query q, int expectedNumberOfResult, LocalDatastore datastore, int timeoutMillsec) throws InterruptedException {
        int maxTimeoutmillsec = timeoutMillsec;
        int intervals = 1000;
        List<Entity> result = new ArrayList<>();
        while (maxTimeoutmillsec > 0) {
            result = datastore.findByQuery(q);
            if (result.size() == expectedNumberOfResult) {
                return result;
            }
            intervals = intervals*2;
            Thread.sleep(intervals);
            maxTimeoutmillsec -= intervals;
        }
        Assert.fail(errorMessage);
        return null;
    }

    public static Entity findByKeyWithWait(String errorMessage, Key key , LocalDatastore datastore, int timeoutMillsec) throws InterruptedException {
        int maxTimeoutmillsec = timeoutMillsec;
        int intervals = 1000;
        Entity result = null;
        while (maxTimeoutmillsec > 0) {
            result = datastore.findOneByKey(key);
            if (result != null) {
                return result;
            }
            //every retry we wait time*2 for effective wait
            intervals = intervals*2;
            Thread.sleep(intervals);
            maxTimeoutmillsec -= intervals;
        }
        Assert.fail(errorMessage);
        return null;
    }

    //Be careful with this update method
    public static void updateEntity (LocalDatastore datastore, Entity e, String property, Object newValue) throws Exception {

        datastore.update(e,property,newValue);

    }

    public static Entity findByKeyWithWait(String errorMessage, Key key , LocalDatastore datastore) throws InterruptedException {
        return findByKeyWithWait(errorMessage,key, datastore, 10000);
    }

    public static List<Entity> queryWithWait(String errorMessage, Query q, int expectedNumberOfResult, LocalDatastore datastore) throws InterruptedException {
        return queryWithWait(errorMessage,q,expectedNumberOfResult, datastore, 200000);
    }

    public RegisterResponse createNewMember(String phoneNumber, boolean createConfiguration) {
        if(createConfiguration){
            setRegistrationConfiguration();
        }

        return getRegisterResponse(phoneNumber, apiKey);
    }

    public RegisterResponse createNewMember(Map<String,Object> map, boolean createConfiguration) {
        if(createConfiguration){
            setRegistrationConfiguration();
        }

        return getRegisterResponse(map, apiKey);
    }
    public RegisterResponse registerMemberExternal(Map<String, Object> map, boolean createConfiguration, Map<String,String> configFieldMap) throws Exception {
        if (createConfiguration) {
            ConfigRegistration configRegistration = Initializer.initConfigRegistration(locationId, configFieldMap);
            configRegistration.sendRequestByTokenAndValidateResponse(serverToken, Response.class);
        }
        return getRegisterResponse(map, apiKey);
    }

    public UpdateMembershipV2Response updateMemberExternal(String customerPhone, Boolean allowEmail, Boolean allowSMS, boolean createConfiguration, Map<String,String> configFieldMap ) {
        if (createConfiguration) {
            ConfigRegistration configRegistration = Initializer.initConfigRegistration(locationId, configFieldMap);
            configRegistration.sendRequestByTokenAndValidateResponse(serverToken, Response.class);
        }
        UpdateMembershipV2Response updateMembershipResponse = getUpdateMembershipExternal(customerPhone, allowEmail, allowSMS);
        return updateMembershipResponse;
    }



    public IServerResponse updateMemberExternalWithoutValidation(String customerPhone, Boolean allowEmail, Boolean allowSMS ) {
        UpdateMembershipExternal updateMembership = new UpdateMembershipExternal(customerPhone);
        updateMembership.setAllowEmail(allowEmail);
        updateMembership.setAllowSMS(allowSMS);

        IServerResponse response = updateMembership.sendRequestByApiKey(apiKey, UpdateMembershipV2Response.class);
        return response;
    }

    public IServerResponse updateMemberExternal(String customerPhone,String lastName) {
        UpdateMembershipExternal updateMembership = new UpdateMembershipExternal(customerPhone);
        updateMembership.setLastName(lastName);
        IServerResponse response = updateMembership.sendRequestByApiKey(apiKey, UpdateMembershipV2Response.class);
        return response;
    }

    public RegisterMemberV4Response registerMemberV4(String phoneNumber, Boolean allowSMS, Boolean allowEmail, boolean createConfiguration) {
        if (createConfiguration) {
            setRegistrationConfigurationV2();
        }
        RegisterMemberV4Response registerMemberV4Response = getRegisterMemberV4Response(phoneNumber,allowEmail,allowSMS);
        return registerMemberV4Response;
    }

    public RegisterMemberV4Response registerMemberV4WithoutConsent(String phoneNumber, boolean createConfiguration) {
        if (createConfiguration) {
            setRegistrationConfigurationV2();
        }
        Map <String, Object> fieldsHashMap = new HashMap<>();
        fieldsHashMap.put(Initializer.PHONE_NUMBER, phoneNumber);
        RegisterMemberV4Response registerMemberV4Response = getRegisterMemberV4Response(fieldsHashMap);
        return registerMemberV4Response;
    }

    public RegisterMemberV4Response registerMemberV4WithAllowSMSAllowEmail(String phoneNumber, boolean createConfiguration, boolean AllowSMS, boolean AllowEmail) {
        if (createConfiguration) {
            setRegistrationConfigurationV2();
        }
        Map <String, Object> fieldsHashMap = new HashMap<>();
        fieldsHashMap.put(Initializer.PHONE_NUMBER, phoneNumber);
        fieldsHashMap.put(Initializer.EMAIL, phoneNumber + "@gmail.com");
        fieldsHashMap.put(Initializer.ALLOW_SMS, AllowSMS);
        fieldsHashMap.put(Initializer.ALLOW_EMAIL, AllowEmail);
        RegisterMemberV4Response registerMemberV4Response = getRegisterMemberV4Response(fieldsHashMap);
        return registerMemberV4Response;
    }



    public IServerResponse updateMemberV4(String phoneNumber, Boolean allowSMS,Boolean allowEmail) {
        IServerResponse response = getUpdateMemberV4Response(phoneNumber, allowEmail,allowSMS);
        return response;
    }


    private RegisterResponse getRegisterResponse(String phoneNumber, String apiKey) {
        //Adding new member
        RegisterMember registerMember = Initializer.initRegisterMember(phoneNumber);
        RegisterResponse registerResponse = (RegisterResponse) registerMember.sendRequestByApiKeyAndValidateResponse(apiKey, RegisterResponse.class);
        Assert.assertNotNull(registerResponse);
        Assert.assertNotNull(registerResponse.getMembershipKey());
        Assert.assertNotNull(registerResponse.getMembershipStatus());
        Assert.assertNotNull(registerResponse.getUserKey());
        updateMembership(registerResponse.getMembershipKey(),APP);
        return registerResponse;
    }

    public RegisterResponse getRegisterResponse(Map<String, Object> map, String apiKey) {
        //Adding new member
        RegisterMember registerMember = Initializer.initRegisterMember(map);
        RegisterResponse registerResponse = (RegisterResponse) registerMember.sendRequestByApiKeyAndValidateResponse(apiKey, RegisterResponse.class);
        updateMembership(registerResponse.getMembershipKey(), APP);
        return registerResponse;
    }



    public RegisterResponse getRegisterResponse(String phoneNumber,String govId,String memberId, String apiKey) {
        //Adding new member
        RegisterMember registerMember = Initializer.initRegisterMember(phoneNumber,govId,memberId);
        RegisterResponse registerResponse = (RegisterResponse) registerMember.sendRequestByApiKeyAndValidateResponse(apiKey, RegisterResponse.class);
        updateMembership(registerResponse.getMembershipKey(),APP);
        return registerResponse;
    }



    public UpdateMembershipV2Response getUpdateMembershipExternal(String phoneNumber, Boolean allowEmail, Boolean allowSMS) {
        UpdateMembershipExternal updateMembership = new UpdateMembershipExternal(phoneNumber);
        updateMembership.setAllowEmail(allowEmail);
        updateMembership.setAllowSMS(allowSMS);

        UpdateMembershipV2Response updateMembershipV2Response = (UpdateMembershipV2Response)updateMembership.sendRequestByApiKeyAndValidateResponse(apiKey, UpdateMembershipV2Response.class);
        return updateMembershipV2Response;
    }

    public RegisterMemberV4Response getRegisterMemberV4Response(String phoneNumber, Boolean allowSMS,Boolean allowEmail) {
        RegisterMemberV4 registerMemberV4 = new RegisterMemberV4(apiKey);
        RegistrationData registrationData = new RegistrationData();
        Map <String, Object> fieldsHashMap = new HashMap<>();
        fieldsHashMap.put(Initializer.PHONE_NUMBER, phoneNumber);
        fieldsHashMap.put(Initializer.ALLOW_EMAIL,allowEmail);
        fieldsHashMap.put(Initializer.ALLOW_SMS,allowSMS);
        registrationData.setFieldNameMap(fieldsHashMap);
        registerMemberV4.setRegistrationData(registrationData);

        RegisterMemberV4Response registerMemberV4Response = (RegisterMemberV4Response) registerMemberV4.SendRequestByApiKeyAndValidateResponse(apiKey, RegisterMemberV4Response.class);
        return registerMemberV4Response;
    }

    public RegisterMemberV4Response getRegisterMemberV4Response( Map <String, Object> fieldsHashMap) {
        RegisterMemberV4 registerMemberV4 = new RegisterMemberV4(apiKey);
        RegistrationData registrationData = new RegistrationData();
        registrationData.setFieldNameMap(fieldsHashMap);
        registerMemberV4.setRegistrationData(registrationData);

        RegisterMemberV4Response registerMemberV4Response = (RegisterMemberV4Response) registerMemberV4.SendRequestByApiKeyAndValidateResponse(apiKey, RegisterMemberV4Response.class);
        return registerMemberV4Response;
    }


    public IServerResponse getUpdateMemberV4Response(String phoneNumber, Boolean allowSMS,Boolean allowEmail) {
        UpdateMember updateMember = new UpdateMember(apiKey);
        server.v4.common.models.Customer customer = new server.v4.common.models.Customer();
        customer.setPhoneNumber(phoneNumber);
        updateMember.setCustomer(customer);
        RegistrationData registrationData = new RegistrationData();
        Map <String, Object> fieldsHashMap = new HashMap<>();
        fieldsHashMap.put(Initializer.ALLOW_EMAIL,allowEmail);
        fieldsHashMap.put(Initializer.ALLOW_SMS,allowSMS);
        registrationData.setFieldNameMap(fieldsHashMap);
        updateMember.setRegistrationData(registrationData);

        IServerResponse response = updateMember.SendRequestByApiKeyAndValidateResponse(apiKey, UpdateMemberResponse.class);
        return response;
    }

    public AssetDateCondition getValidDatesForAsset(){
        LocalDateTime from = LocalDateTime.now();
        Date fromCondition = Date.from(from.toInstant(ZoneOffset.UTC));
        Date toCondition = Date.from(from.plusDays(1).toInstant(ZoneOffset.UTC));
        return new AssetDateCondition(fromCondition, toCondition);
    }


    public AssetDateCondition getInvalidDatesForAsset(){
        LocalDateTime from = LocalDateTime.now();
        Date fromCondition = Date.from(from.minusDays(10).toInstant(ZoneOffset.UTC));
        Date toCondition = Date.from(from.minusDays(1).toInstant(ZoneOffset.UTC));
        return new AssetDateCondition(fromCondition, toCondition);
    }

    public void validatePurchaseWithTaxAmount (server.v4.SubmitPurchase submitPurchase, Entity res){

        verifyPurchase(submitPurchase, res);
        Long l = (Long) res.getProperty("TotalTaxSum");
        Assert.assertEquals("TotalTaxAmount should be equal", submitPurchase.getPurchase().getTotalTaxAmount(),  (Integer)l.intValue());


    }


    public static void performAnActionSubmitPurchase(SubmitPurchase submitPurchase) throws Exception {

        log4j.info("send request submit purchase");
        // Send the request
        IServerResponse response = submitPurchase.SendRequestByApiKey(apiKey, SubmitPurchaseResponse.class);


        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        // Cast to SubmitPurchaseResponse object to fetch all response data
        SubmitPurchaseResponse submitPurchaseResponse = (SubmitPurchaseResponse) response;
        Assert.assertEquals("status code must be 200", submitPurchaseResponse.getStatusCode(), new Integer(200));
        Assert.assertNotNull(submitPurchaseResponse.getConfirmation());
        log4j.info("status code: " + submitPurchaseResponse.getStatusCode());

    }

    public static void checkAutomationRunAndTagMemberByTransaction(LocalDatastore datastore,Key userKey,int numOfRaws, String transactionId,String tag) throws Exception {
        log4j.info("check Data Store Updated with Tag");
        List<Entity> res = ServerHelper.queryWithWait("Must find " + numOfRaws + " UserAction with the TransactionID: " + transactionId.toString(),
                buildQuery(UserActionsConstants.TAG_OPERATION, transactionId,userKey), numOfRaws, datastore);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info("ExportData contains the following data : " + t.getValue());
        Assert.assertTrue("Data must contain the tag: " + tag, t.getValue().contains(tag));
    }

    public static void checkAutomationRunNotRunByTransaction(LocalDatastore datastore,Key userKey,String transactionId) throws Exception {
        log4j.info("check Data Store Updated with Tag");
        List<Entity> res = datastore.findByQuery(buildQuery(UserActionsConstants.TAG_OPERATION, transactionId,userKey));

        Assert.assertTrue("There should not be a user action fro this transaction: " + transactionId,res.isEmpty());
    }


    private static Query buildQuery(String actionName, String transactionId,Key userKey){
        return buildQuery(actionName,new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, transactionId),userKey);
    }


    public static Query buildQuery (String actionName,Query.FilterPredicate predicate,Key userKey) {
        return buildQuery("UserAction", Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, actionName),
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL, userKey),
                predicate));
    }

    // At least one predicate is needed.
    public static Query buildQuery (String queryName,List<Query.FilterPredicate> predicates) {
        Query q = new Query(queryName);
        if(predicates.size() == 1){
            q.setFilter(predicates.get(0));
        } else if(predicates.size()>1) {
            q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, predicates.stream().collect(Collectors.toList())));
        }
        return q;
    }

    public static Query buildQueryForBudgetTransaction (String paymentUID) {
        Query q = new Query("BudgetTransaction");
        q.setFilter(new Query.FilterPredicate("UID",Query.FilterOperator.EQUAL, paymentUID));
        return q;
    }

    /**
     * generateTokenWebViewUserToken
     * @param timeToLive - time in seconds the token is valid
     * @return
     */
    public static String generateTokenWebViewUserToken (String timeToLive, String userToken, String locationId){

        GenerateToken generateToken = new GenerateToken();
        generateToken.setTimeToLive(timeToLive);

        IServerResponse response = generateToken.sendRequestByUserTokenAndLocationID(userToken, locationId,GenerateTokenResponse.class);

        //Make sure the response is not Error response
        if (response instanceof ServicesErrorResponse) {
            return  ((ServicesErrorResponse) response).getErrorMessage();

        }

        GenerateTokenResponse generateTokenResponse = (GenerateTokenResponse) response;
        return generateTokenResponse.getToken();
    }

    public String createNewMemberGetUserToken (String phoneNumber,String origin){

        //signOn and get the user token
        String userToken = signOn();
        log4j.info("userToken: " + userToken);
        //join the club with the user token
        joinClubWithUserToken(phoneNumber, userToken,origin);
        return userToken;

    }

    public HashMap createNewMemberGetUserTokenAndMembership (String phoneNumber,String origin){

        HashMap map = new HashMap();
        //signOn and get the user token
        String userToken = signOn();
        log4j.info("userToken: " + userToken);
        //join the club with the user token
        String membershipKey = joinClubWithUserToken(phoneNumber, userToken,origin);
        map.put("membershipKey",membershipKey);
        map.put("userToken",userToken);
        log4j.info("Created member with membershipKey: " + membershipKey);
        return map;

    }

    public String signON () {

        HashMap map = new HashMap();
        //signOn and get the user token
        String userToken = signOn();
        log4j.info("userToken: " + userToken);
        return userToken;

    }





    public String createNewMemberGetUserToken (){

        String phoneNumber = String.valueOf(System.currentTimeMillis());
        return  createNewMemberGetUserToken(phoneNumber,APP);

    }


    //Simulates the user join the club from the client application using the token that was received from signOn
    public String joinClubWithUserToken(String phoneNumber, String userToken) {

        return joinClubWithUserToken(phoneNumber,userToken,APP);
    }

    //Simulates the user join the club from the client application using the token that was received from signOn
    public String joinClubWithUserToken(String phoneNumber, String userToken,String origin) {

        if(!Boolean.valueOf(PropsReader.getPropValuesForEnv(Consent.REGISTER_MEMBERS_BY_CONSENT)).booleanValue()) {
            origin = null;
        }
        //Join Club
        JoinClub joinClub = buildJoinClubRequest(origin);
        joinClub.setPhoneNumber(phoneNumber);
        IServerResponse response = joinClub.sendRequestByTokenAndLocation(userToken, locationId, JoinClubResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        String membershipKey = (String) ((JoinClubResponse)response).getRawObject().get("Key");
        log4j.info("Join club and get membership key...");
        return membershipKey;
    }

    //Simulates the user join the club from the client application using the token that was received from signOn
    public String joinClubWithUserTokenAndReferralCode(String phoneNumber, String userToken,String origin,String code) {


        if(!Boolean.valueOf(PropsReader.getPropValuesForEnv(Consent.REGISTER_MEMBERS_BY_CONSENT)).booleanValue()) {
            origin = null;
        }
        //Join Club
        JoinClub joinClub = buildJoinClubRequest(origin);
        joinClub.setPhoneNumber(phoneNumber);
        joinClub.setReferralCode(code);
        IServerResponse response = joinClub.sendRequestByTokenAndLocation(userToken, locationId, JoinClubResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        String membershipKey = (String) ((JoinClubResponse)response).getRawObject().get("Key");
        log4j.info("Join club and get membership key...");
        return membershipKey;
    }




    //Simulates the sign on from client device
    public static String signOn() {

        SignOn signOn = new SignOn();
        signOn.setDeviceID( getRandomId());
        signOn.setInstallationID( getRandomId());

        IServerResponse response1 = signOn.sendRequest(SignOnResponse.class);

        // make sure the response is not Error response
        if (response1 instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response1).getErrorMessage());
        }
        log4j.info("SignOn with device and get user token..");
        SignOnResponse resp = (SignOnResponse) response1;

        String token = resp.getToken();
        return token;
    }

    private JoinClubResponse registerMemberByJoinClub(UpdatedFields updatedFields,String origin){

        String userToken = signOn();

        //Simulates the user join the club from the client application using the token that was received from signOn

        //if registerMembersByConsent is false we create user as there is no consent feature
        if(!Boolean.valueOf(PropsReader.getPropValuesForEnv(Consent.REGISTER_MEMBERS_BY_CONSENT)).booleanValue()){
            origin = null;
        }
        JoinClub joinClub = buildJoinClubRequest(origin);
        joinClub.setUpdatedFields(updatedFields);
        IServerResponse response = joinClub.sendRequestByTokenAndLocation(userToken, locationId, JoinClubResponse.class);
        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }
        JoinClubResponse joinClubResponse = (JoinClubResponse) response;
        Assert.assertNotNull(joinClubResponse);
        Assert.assertNotNull(joinClubResponse.getMembershipKey());
        Assert.assertNotNull(joinClubResponse.getMembershipStatus());
        Assert.assertNotNull(joinClubResponse.getUserKey());

        return (JoinClubResponse) response;

    }

    private JoinClub buildJoinClubRequest(String origin) {
        JoinClub joinClub = new JoinClub();
        joinClub.setLocationID(locationId);
        joinClub.setOrigin(origin);
        return joinClub;
    }

    public UpdateMembershipResponse updateMembership( String membershipKey, UpdatedFields updatedFields,String origin) {

        UpdateMembership updateMembership = new UpdateMembership();
        updateMembership.setLocationID(locationId);
        updateMembership.setMembershipKey(membershipKey);
        updateMembership.setToken(serverToken);
        updateMembership.setOrigin(origin);
        updateMembership.setUpdatedFields(updatedFields);

        return (UpdateMembershipResponse) updateMembership.sendRequestByTokenAndLocation(serverToken, locationId, UpdateMembershipResponse.class);
    }
    public static UpdateMembershipResponse updateMembership( String membershipKey,String origin) {

        UpdateMembership updateMembership = new UpdateMembership();
        updateMembership.setLocationID(locationId);
        updateMembership.setMembershipKey(membershipKey);
        updateMembership.setToken(serverToken);
        updateMembership.setOrigin(origin);
        updateMembership.setUpdatedFields(new UpdatedFields());
        return (UpdateMembershipResponse) updateMembership.sendRequestByTokenAndLocation(serverToken, locationId, UpdateMembershipResponse.class);
    }

    public static UpdateMembershipResponse updateMembership( String membershipKey,UpdatedFields updatedFields) {

        UpdateMembership updateMembership = new UpdateMembership();
        updateMembership.setLocationID(locationId);
        updateMembership.setMembershipKey(membershipKey);
        updateMembership.setToken(serverToken);
        updateMembership.setUpdatedFields(updatedFields);
        return (UpdateMembershipResponse) updateMembership.sendRequestByTokenAndLocation(serverToken, locationId, UpdateMembershipResponse.class);
    }
    private static String getRandomId() {
        return String.valueOf(System.currentTimeMillis());
    }

    public GetMemberBenefits  createGetMemberBenefits (String tokenTemp){

        Customer customer = new Customer();
        customer.setTemporaryToken(tokenTemp);
        GetMemberBenefits getMemberBenefits = new GetMemberBenefits();
        getMemberBenefits.getCustomers().add(customer);
        getMemberBenefits.setReturnExtendedItems(true);

        return getMemberBenefits;
    }


    public static NewAssetResponse buildAsset(String assetType, String codeType, Integer numOfCode, String locationId , String token, AssetDateCondition dateCondition) throws IOException, InterruptedException {

        String DEFAULT_DEAL_CODE="11223344";
        String EXPIRATION_SECONDS="6048000";

        //Build new Asset type = gift
        NewAsset newAsset = null;
        AssetBuilder assetBuilder = new AssetBuilder();
        switch (codeType) {
            case "None":
                newAsset = assetBuilder.buildDefaultAsset(locationId, assetType, dateCondition);
                break;

            case "Bulk":
                assetBuilder.buildDefaultAsset(locationId, assetType, dateCondition);
                HashMap<String, Object> objBulk = new HashMap();
                objBulk.put("CodeID", DEFAULT_DEAL_CODE);
                assetBuilder.setEnumeratorData(objBulk);
                newAsset = assetBuilder.getAsset();
                break;

            case "Auto":
                newAsset = assetBuilder.buildDefaultAsset(locationId, assetType, dateCondition);
                HashMap<String, Object> objAuto = new HashMap();
                objAuto.put("ExpirationSeconds", EXPIRATION_SECONDS);
                assetBuilder.setShortCodeData(objAuto);
        }
        NewAssetResponse newAssetResponse = (NewAssetResponse) newAsset.SendRequestByToken(token, NewAssetResponse.class);


        // Copy asset in order to create the code bluk according to numOfAsset
        CopyAsset copyAsset = new CopyAsset();
        copyAsset.setKey(newAssetResponse.getKey());
        copyAsset.setCopies(numOfCode);
        copyAsset.SendRequestByToken(token, Response.class);
        return  newAssetResponse;

    }

    public server.v4.common.models.Asset waitForAsset(GetMemberDetails memberDetails, String apiKey, String assetName, int maxSecWait) throws InterruptedException {

        int maxTimeout = (maxSecWait*1000);
        GetMemberDetailsResponse getMemberDetailsResponse;
        //Get Asset Name from getMemberDetailsResponse
        while (maxTimeout > 0) {
            getMemberDetailsResponse=(GetMemberDetailsResponse)memberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);
            Asset asset=null;
            try {
                asset = getMemberDetailsResponse.getMembership().findAssetByName(assetName);
            }catch (Exception e){

            }
            if(asset!=null)
                return asset;
            Thread.sleep(1000);
            maxTimeout -= 1000;
        }
        Assert.fail("Asset "+ assetName +" didn't found in GetMemberDetailsResponse");
        return null;
    }

    public RegisterResponse createMemberWithPoints(String amountOfPoints) throws Exception {
        long currentTimeMillis = currentTimeMillis();
        String customerPhone = String.valueOf(currentTimeMillis);
        RegisterResponse newMember = createMemberWithAllIdentifiers(customerPhone,null,null,true);

        if(!amountOfPoints.equals("0"))
            addPointsToMember(amountOfPoints, newMember.getMembershipKey(),newMember.getUserKey());
        return newMember;
    }



    public JoinClubResponse createMember(String customerPhone) {
        UpdatedFields updatedFields = new UpdatedFields();
        updatedFields.setPhoneNumber(customerPhone);
        return registerMemberByJoinClub(updatedFields,APP);

    }

    public JoinClubResponse createMember(String customerPhone,String joiningCode) {
        UpdatedFields updatedFields = new UpdatedFields();
        updatedFields.setPhoneNumber(customerPhone);

        String userToken = signOn();

        //Simulates the user join the club from the client application using the token that was received from signOn
        JoinClub joinClub = buildJoinClubRequest(APP);
        joinClub.setUpdatedFields(updatedFields);
        joinClub.setCode(joiningCode);
        IServerResponse response = joinClub.sendRequestByTokenAndLocation(userToken, locationId, JoinClubResponse.class);
        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }
        JoinClubResponse joinClubResponse = (JoinClubResponse) response;
        Assert.assertNotNull(joinClubResponse);
        Assert.assertNotNull(joinClubResponse.getMembershipKey());
        Assert.assertNotNull(joinClubResponse.getMembershipStatus());
        Assert.assertNotNull(joinClubResponse.getUserKey());

        return (JoinClubResponse) response;
    }


    public RegisterResponse createMemberWithBudget(String amountOfPoints) throws Exception {

        long currentTimeMillis = currentTimeMillis();
        String customerPhone = String.valueOf(currentTimeMillis);
        RegisterResponse newMember = createMemberWithAllIdentifiers(customerPhone,null,null,true);

        if(!amountOfPoints.equals("0"))
            addCreditToMember(amountOfPoints, newMember.getMembershipKey(),newMember.getUserKey());
        return newMember;
    }

    public JoinClubResponse createMember( UpdatedFields updatedFields) throws InterruptedException {

        log4j.info("Creating member with phone number: " + updatedFields.getPhoneNumber());
        JoinClubResponse registerResponse;
        if(Boolean.valueOf(PropsReader.getPropValuesForEnv(Consent.REGISTER_MEMBERS_BY_CONSENT)).booleanValue()) {
            registerResponse = registerMemberByJoinClub(updatedFields, APP);
        }else{
            registerResponse = registerMemberByJoinClub(updatedFields, null);
        }

        return registerResponse;
    }

    public JoinClubResponse createMemberWithoutConsent() throws InterruptedException {

        String customerPhone = String.valueOf(currentTimeMillis());
        log4j.info("Creating member with phone number: " + customerPhone);
        UpdatedFields updatedFields = new UpdatedFields();
        updatedFields.setPhoneNumber(customerPhone);
        JoinClubResponse registerResponse = registerMemberByJoinClub(updatedFields,null);

        return registerResponse;
    }

    public JoinClubResponse createMember() throws InterruptedException {
        Thread.sleep(10000);
        JoinClubResponse registerResponse;
        if(Boolean.valueOf(PropsReader.getPropValuesForEnv(Consent.REGISTER_MEMBERS_BY_CONSENT)).booleanValue()){
            String customerPhone = String.valueOf(currentTimeMillis());
            log4j.info("Creating member with phone number: " + customerPhone);
            UpdatedFields updatedFields = new UpdatedFields();
            updatedFields.setPhoneNumber(customerPhone);
            updatedFields.setAllowSMS(Boolean.TRUE);
            updatedFields.setAllowEmail(Boolean.TRUE);
            registerResponse = registerMemberByJoinClub(updatedFields, APP);

        }else {
            registerResponse = createMemberWithoutConsent();
        }

        return registerResponse;
    }

    public void addCreditToMember(String amountOfPoints, String membershipKey,String userKey) throws Exception {

        log4j.info("Add member  " + amountOfPoints + " points to credit");
        //Add 100 to the user credit (budget)
        InvokeEventBasedActionRuleBuilder invokeEventBasedActionRuleBuilder = new InvokeEventBasedActionRuleBuilder();
        InvokeEventBasedActionRule invokeEventBasedActionRule = invokeEventBasedActionRuleBuilder.buildGiveCreditRequest(membershipKey, userKey, locationId, amountOfPoints);
        IServerResponse response = invokeEventBasedActionRule.SendRequestByToken(serverToken, Response.class);

        // make sure the response is not Error response
        if (response instanceof server.v4.base.ZappServerErrorResponse) {
            Assert.assertTrue(((server.v4.base.ZappServerErrorResponse) response).getErrorMessage(), false);
        }

    }


    public RegisterResponse createMemberWithAllIdentifiers(String customerPhone,String govId,String memberId,boolean update ) throws InterruptedException {
       if(update){
           setRegistrationConfiguration();
           setRegistrationConfigurationV2();
       }
        log4j.info("Creating member with phone number: " + customerPhone);
        RegisterResponse registerResponse = getRegisterResponse(customerPhone, govId,memberId, apiKey);
        Assert.assertNotNull(registerResponse);
        Assert.assertNotNull(registerResponse.getMembershipKey());
        Assert.assertNotNull(registerResponse.getMembershipStatus());
        Assert.assertNotNull(registerResponse.getUserKey());
        Thread.sleep(10000);

        if(Boolean.valueOf(PropsReader.getPropValuesForEnv(Consent.REGISTER_MEMBERS_BY_CONSENT)).booleanValue()) {
            updateMembership(registerResponse.getMembershipKey(),APP);
        }

        return registerResponse;
    }

    public void setRegistrationConfiguration() {
        ConfigRegistration configRegistration = Initializer.initConfigRegistration(locationId);
        IServerResponse servicesResponse = configRegistration.sendRequestByTokenAndValidateResponse(serverToken, Response.class);
        Assert.assertNotNull("Response should be not null", servicesResponse);
        Assert.assertEquals("Configuration update should return status code 204", 204, servicesResponse.getStatusCode().intValue());
    }

    public void setRegistrationConfiguration(Map<String,String> fieldMap){
        ConfigRegistration configRegistration = Initializer.initConfigRegistration(locationId,fieldMap);
        IServerResponse servicesResponse = configRegistration.sendRequestByTokenAndValidateResponse(serverToken, Response.class);
        Assert.assertNotNull("Response should be not null", servicesResponse);
        Assert.assertEquals("Configuration update should return status code 204", 204, servicesResponse.getStatusCode().intValue());
    }

    public void setRegistrationConfigurationV2() {
        ConfigRegistrationV2 configRegistration = Initializer.initConfigRegistrationV2(locationId);
        IServerResponse servicesResponse = configRegistration.sendRequestByTokenAndValidateResponse(serverToken, Response.class);
        Assert.assertNotNull("Response should be not null", servicesResponse);
        Assert.assertEquals("Configuration update should return status code 204", 200, servicesResponse.getStatusCode().intValue());
    }


    public void addPointsToMember(String amountOfPoints, String membershipKey,String userKey) throws Exception {

        log4j.info("Add member  " + amountOfPoints + " points to points");
        //Add points 100 to the user
        InvokeEventBasedActionRuleBuilder invokeEventBasedActionRuleBuilder = new InvokeEventBasedActionRuleBuilder();
        InvokeEventBasedActionRule invokeEventBasedActionRule = invokeEventBasedActionRuleBuilder.buildGivePointsRequest(membershipKey,userKey, locationId, amountOfPoints);
        IServerResponse response = invokeEventBasedActionRule.SendRequestByToken(serverToken, Response.class);

        // make sure the response is not Error response
        if (response instanceof server.v4.base.ZappServerErrorResponse) {
            Assert.assertTrue(((server.v4.base.ZappServerErrorResponse) response).getErrorMessage(), false);
        }
    }

    public void tagMember(String tag, String membershipKey,String userKey) throws Exception {

        log4j.info("Tag member with " + tag );
        //Add points 100 to the user
        InvokeEventBasedActionRuleBuilder invokeEventBasedActionRuleBuilder = new InvokeEventBasedActionRuleBuilder();
        InvokeEventBasedActionRule invokeEventBasedActionRule = invokeEventBasedActionRuleBuilder.buildTagMemberRequest(membershipKey,userKey, locationId, tag);
        IServerResponse response = invokeEventBasedActionRule.SendRequestByToken(serverToken, Response.class);

        // make sure the response is not Error response
        if (response instanceof server.v4.base.ZappServerErrorResponse) {
            Assert.assertTrue(((server.v4.base.ZappServerErrorResponse) response).getErrorMessage(), false);
        }
    }

    public server.v4.common.models.Customer createMemberWithPointsOrBudget(String amountOfPoints, String budgetType,LocalDatastore datastore) throws Exception {
        long currentTimeMillis = currentTimeMillis();
        String customerPhone = String.valueOf(currentTimeMillis);
        RegisterResponse newMember = createMemberWithAllIdentifiers(customerPhone, null, null,true);

        server.v4.common.models.Customer customer = new server.v4.common.models.Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());

        if (!amountOfPoints.equals("0")){
            if(budgetType.equals(PaymentType.POINTS.type()))
                addPointsToMember(amountOfPoints, newMember.getMembershipKey(), newMember.getUserKey());
            else if(budgetType.equals(PaymentType.BUDGET.type()))
                addCreditToMember(amountOfPoints, newMember.getMembershipKey(), newMember.getUserKey());

            checkPointsTransaction(newMember.getUserKey(),datastore);
        }

        return customer;
    }

    public void addPOSRedeemCodeBulk() {
        try {
            POSRedeemCodesService.addPOSRedeemCodeBulk("ServerAutomation", AssignAsset.DEFAULT_DEAL_CODE,"1000","6");
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
            Assert.fail(e.getMessage());
        }
    }

    public static String getShortUUID() {
        String uuid = UUID.randomUUID().toString();
        return uuid.substring(0,uuid.indexOf("-"));
    }

    public void addPOSRedeemCodeBulkFromApi() {

        String shortUUID = getShortUUID();
        SaveOneTimeCodeTemplate oneTimeCodeTemplate = new SaveOneTimeCodeTemplate(serverToken,locationId,shortUUID,AssignAsset.DEFAULT_DEAL_CODE);
        IServerResponse response = oneTimeCodeTemplate.sendRequest(Response.class);
        Assert.assertEquals("status code must be 200", response.getStatusCode(), (Integer) 200);

        generateBulk(shortUUID);

    }

    public void add3rdPartyRedeemCodeBulkFromApi(String assetTemplateKey) {

        String shortUUID = getShortUUID();
        SaveOneTimeCodeTemplate oneTimeCodeTemplate = new SaveOneTimeCodeTemplate(serverToken,locationId,shortUUID,null,assetTemplateKey);
        IServerResponse response = oneTimeCodeTemplate.sendRequest(Response.class);
        Assert.assertEquals("status code must be 200", response.getStatusCode(), (Integer) 200);

        generateBulk(shortUUID);

    }

    private void generateBulk(String shortUUID) {
        String bulkId = getShortUUID();
        GenerateOneTimeCodes generateOneTimeCodes = new GenerateOneTimeCodes(shortUUID,serverToken,locationId,bulkId,"2000","7");
        IServerResponse response = generateOneTimeCodes.sendRequest(Response.class);
        Assert.assertEquals("status code must be 200", response.getStatusCode(), (Integer) 200);
    }

    public void deactivateAsset (String assetKey){

        //Deactivate Asset
        UpdateAsset updateAsset = new UpdateAsset();
        updateAsset.setActive("N");
        IServerResponse response = updateAsset.sendRequestByTokenAndAssetKey(serverToken.toString(), assetKey, UpdateAssetResponse.class);
        //Make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());}
    }

    public void checkPointsTransaction(String uKey,LocalDatastore dataStore) throws InterruptedException {
        DataStoreKeyUtility dataStoreKeyUtility = new DataStoreKeyUtility();
        HttpResponse response = dataStoreKeyUtility.SendRequest(uKey);
        Key key =  dataStoreKeyUtility.userKey((String)response.getBody());

        Query q = new Query("UserAction");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, "PointTransaction"),
                new Query.FilterPredicate("UserKey", Query.FilterOperator.EQUAL,key))));
        log4j.info("Check member get the point transaction in datastore: userkey=" +key );
        queryWithWait("Member did not get the points ",q,1,dataStore);
    }

	public String getMemberAssetKey(String assetName, String customerPhone) throws Exception{

		//get member details to get the assigned asset key for getBenefits request
		server.v4.common.models.Customer customer = new server.v4.common.models.Customer();
		customer.setPhoneNumber(customerPhone);
		MemberDetailsQueryParams queryParams = new MemberDetailsQueryParams();
		queryParams.setReturnAssets(ReturnAssetsStatus.ASSET_STATUS_ACTIVE.toString());

		//Send GetMemberDetails request
		GetMemberDetails memberDetails = new GetMemberDetails();
		memberDetails.setApiKey(apiKey);
		memberDetails.setDefauldHeaderParams();
		memberDetails.setCustomer(customer);
		memberDetails.setQueryParameters(queryParams.getParams());
		IServerResponse response = memberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);

		// make sure the response is not Error response
		if (response instanceof server.v4.base.ZappServerErrorResponse) {
			Assert.assertTrue(((server.v4.base.ZappServerErrorResponse) response).getErrorMessage(), false);
		}

		// Cast to getMemberDetailsResponse object to fetch all response data
		GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) response;
        server.v4.common.models.Membership membership =  getMemberDetailsResponse.getMembership();
        Assert.assertNotNull(membership);
        //wait for assigned asset to return in getMemberDetails
        Asset assignedAsset = waitForAsset(memberDetails,apiKey,assetName,500);

        log4j.info("assetKey: " + assignedAsset.getKey());
		return assignedAsset.getKey();

	}

    public String getAssetKey(String memberPhone)  throws InterruptedException{
        Customer customer = new Customer();
        customer.setPhoneNumber(memberPhone);
        server.v2_8.GetMemberDetails getMemberDetails = new server.v2_8.GetMemberDetails();
        getMemberDetails.getCustomers().add(customer);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(false);

        //Send the request
        server.v2_8.GetMemberDetailsResponse getMemberDetailsResponse = (server.v2_8.GetMemberDetailsResponse)getMemberDetails.SendRequestByApiKey(apiKey, server.v2_8.GetMemberDetailsResponse.class);
        ArrayList<Membership> memberships = getMemberDetailsResponse.getMemberships();
        Assert.assertEquals("Membership must be one", 1, memberships.size());
        Membership membership = memberships.get(0);
        ArrayList<server.v2_8.common.Asset> assets = membership.getAssets();
        return assets.get(0).getKey();
    }


    public void saveApiClient(Map<String, Object> props) {
        UpdateApiClient saveApiClientRequest = new UpdateApiClient(serverToken);

        saveApiClientRequest.setProps(props);
        IServerResponse saveApiClientResponse = saveApiClientRequest.sendRequestByLocation(locationId, Response.class);
        Assert.assertEquals("status code must be 200", saveApiClientResponse.getStatusCode(), (Integer) 200);
        //sleep to wait for data store to update
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            log4j.error(e);
        }
    }

    public void saveApiClient(ApiPair... pairs) {

        Map<String, Object> props = new HashMap<>();
        for (ApiPair pair : pairs)
        {
            props.put(pair.getKey(),pair.getValue());
        }

        saveApiClient(props);
    }

    public void checkApiClientUpdated(String key,Object value) throws InterruptedException {

        GetApiClient getApiClient = new GetApiClient(serverToken);
        getApiClient.addProp(key,value);
        IServerResponse response = getApiClient.sendRequestByLocation(locationId, ApiClientResponse.class);
        Assert.assertEquals("status code must be 200", response.getStatusCode(), (Integer) 200);

        try {
            Class c = Class.forName(ApiClientResponse.class.getName());
            Method m[] = c.getDeclaredMethods();
            for (int i = 0; i < m.length; i++) {
               if(!(m[i].getName().contains("set")) && m[i].getName().contains(key)){
                   String respValue = (String) m[i].invoke(((ApiClientResponse) response),null);
                   log4j.info("value: " + value + " , response: " + respValue);
                   Assert.assertEquals("value must be " + value, String.valueOf(value), respValue);
                   return;
               }
            }
        }
        catch (Throwable e) {
            System.err.println(e);
        }

    }

    public void checkResultsForPay(PaymentAPI payment, PaymentAPIResponse paymentResponse,LocalDatastore datastore) throws Exception {

        log4j.info("Check BudgetTransaction query in data store for UID: " + paymentResponse.getConfirmation());
        Query q = buildQueryForBudgetTransaction(paymentResponse.getConfirmation());
        List<Entity> result = queryWithWait("Must find one budgetTransaction with the UID: "+ paymentResponse.getConfirmation(),q, 1, datastore);
        Assert.assertEquals("ApiKey should be equal",apiKey, result.get(0).getProperty("ApiKey"));
        Assert.assertEquals("BranchID should be equal",payment.getBranchId(), result.get(0).getProperty("BranchID"));
        Assert.assertEquals("BusinessID should be equal",locationId, result.get(0).getProperty("LocationID"));
        Assert.assertEquals("PosId should be equal",payment.getPosId(), result.get(0).getProperty("PosID"));
        Assert.assertEquals("TransactionID should equal",payment.getPurchase().getTransactionId(), result.get(0).getProperty("TransactionID"));

    }

    public static IServerResponse registerMember(Map<String,String> map, boolean createConfiguration)
    {
        if (createConfiguration) {
            ConfigRegistration configRegistration = Initializer.initConfigRegistration(locationId);
            configRegistration.sendRequestByTokenAndValidateResponse(serverToken, Response.class);
        }

        RegisterMember registerMember = new RegisterMember();
        registerMember.setFieldNameMap(map);
        IServerResponse response = registerMember.sendRequestByApiKey(apiKey,RegisterResponse.class);
        Assert.assertNotNull(response);
        updateMembership(((RegisterResponse)response).getMembershipKey(),APP);
        return response;
    }

    public static IServerResponse registerMemberWithoutConsent(Map<String,String> map, boolean createConfiguration)
    {
        if (createConfiguration) {
            ConfigRegistration configRegistration = Initializer.initConfigRegistration(locationId);
            configRegistration.sendRequestByTokenAndValidateResponse(serverToken, Response.class);
        }

        RegisterMember registerMember = new RegisterMember();
        registerMember.setFieldNameMap(map);
        IServerResponse response = registerMember.sendRequestByApiKey(apiKey,RegisterResponse.class);
        Assert.assertNotNull(response);
        // make sure the response is not Error response
        if (response instanceof ServicesErrorResponse) {
            Assert.fail(((ServicesErrorResponse) response).getErrorMessage());
        }
        return response;
    }

    public static String addApiKeyAndValidate(String name, String status, LocalDatastore dataStore) {
        String genApiKey = getnerateNewApiKey();
        UpdateApiClient updateApiClient = new UpdateApiClient(serverToken);
        ApiKey apiKeyObj = new ApiKey(name, status,genApiKey);
        updateApiClient.addProp(ApiClientFields.API_KEYS.key(), Lists.newArrayList(apiKeyObj));
        IServerResponse updateResponse = updateApiClient.sendRequestByLocation(locationId, ApiClientResponse.class);
        if(updateResponse instanceof ServicesErrorResponse )
            Assert.fail("Add api key failed: " + ((ServicesErrorResponse) updateResponse).getErrorMessage());

        checkApiKeyInDatastore(((ApiClientResponse)updateResponse).getApiKeys(),genApiKey, dataStore);
        return genApiKey;
    }

    public static String getnerateNewApiKey(){
        GenerateApiKey generateApiKey = new GenerateApiKey(serverToken);
        IServerResponse response = generateApiKey.sendRequestByTokenAndValidateResponse(serverToken,GenerateApiKeyResponse.class);
        if(response instanceof ServicesErrorResponse)
            Assert.fail("Generate api key failed: " + ((ServicesErrorResponse) response).getErrorMessage());

        String newApiKey = ((GenerateApiKeyResponse)response).getApiKey();
        return newApiKey;
    }

    public static void checkApiKeyInDatastore(List<ApiKey> apiKeys , List<String> newApiKeys, LocalDatastore dataStore) throws Exception {

        for (String newApiKey: newApiKeys) {
            checkApiKeyInDatastore(apiKeys ,newApiKey,dataStore);
        }
    }
    private static void checkApiKeyInDatastore(List<ApiKey> apiKeys ,String newApiKey,LocalDatastore dataStore)  {
        try {
            Query q = new Query("ApiKey");
            q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                    new Query.FilterPredicate("apiKey", Query.FilterOperator.EQUAL, newApiKey),
                    new Query.FilterPredicate("locationId", Query.FilterOperator.EQUAL, locationId))));
            List<Entity> res = queryWithWait("Must find 1 record with the apikey: " + newApiKey,
                    q,1, dataStore);
            Entity entity = res.get(0);
            ApiKey apiKeyTpSearch = findApiKeyInList( (String)entity.getProperty("apiKey"),apiKeys);
            Assert.assertNotNull("POS Key must be in the list of api keys",apiKeyTpSearch);
            Assert.assertEquals("Status must be: " + apiKeyTpSearch.getStatus(),apiKeyTpSearch.getStatus().toUpperCase(), entity.getProperty("status"));
            Assert.assertEquals("LocationId must be: " + apiKeyTpSearch.getLocationId(),apiKeyTpSearch.getLocationId(), entity.getProperty("locationId"));
            Assert.assertEquals("Name must be: " + apiKeyTpSearch.getName(),apiKeyTpSearch.getName(), entity.getProperty("name"));
            Assert.assertEquals("IsDefault must be: " + apiKeyTpSearch.getIsDefault(),apiKeyTpSearch.getIsDefault(), entity.getProperty("isDefault"));
        } catch (InterruptedException e) {
            log4j.error("ERROR while trying reading from data store");
        }
    }

    private static ApiKey findApiKeyInList(String apiKeyTpSearch,List<ApiKey> apiKeys) {
        for (ApiKey apiKey1:apiKeys) {
            if(apiKey1.getApiKey().equals(apiKeyTpSearch))
                return apiKey1;
        }
        return null;
    }


    public Map createAndValidateCode () throws Exception {


        Map map = createMemberWithUserToken();

        String userToken = map.get("userToken").toString();
        String code = generateReferralCode(userToken);
        Assert.assertNotEquals("The code is not generated: ", "", code);
        log4j.info("The code is: " + code);
        map.put("code",code);

        return map;
    }

    public Map createMemberWithUserToken () throws Exception {


        String phoneNumber = String.valueOf(System.currentTimeMillis());
        Map map = createNewMemberGetUserTokenAndMembership(phoneNumber,"App");

        return map;

    }


    public static void performAnActionUsePoints(NewMember member, String numOfPoints ,String transactionId) throws Exception {
        log4j.info("perform action use points");
        PayWithBudget payWithBudget = new PayWithBudget();
        Customer customer1 = new Customer();
        customer1.setPhoneNumber(member.getPhoneNumber());
        payWithBudget.getCustomers().add(customer1);

        payWithBudget.setTotalSum(numOfPoints);
        String TotalSum = payWithBudget.getTotalSum();
        payWithBudget.setAllowPartialPayment(true);
        payWithBudget.setPosId("2");
        payWithBudget.setBranchId("1");
        payWithBudget.setTransactionId(transactionId);
        payWithBudget.setTimeStamp(String.valueOf(System.currentTimeMillis()));

        log4j.info("send request pay with budget -transaction id:" + transactionId);
        // Send the request
        IServerResponse response = payWithBudget.SendRequestByApiKey(apiKey, PayWithBudgetResponse.class);


        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        // Cast to PayWithBudgetResponse object to fetch all response data
        PayWithBudgetResponse payWithBudgetResponse = (PayWithBudgetResponse) response;
        Assert.assertEquals("status code must be 200", payWithBudgetResponse.getStatusCode(), BaseTest.httpStatusCode200);
        log4j.info("status code: " + payWithBudgetResponse.getStatusCode());
        log4j.info("actual charge: " + payWithBudgetResponse.getActualCharge());
    }


    public static void performAnActionUsePoints(NewMember member, String numOfPoints) throws Exception {
        performAnActionUsePoints(member,numOfPoints,String.valueOf(System.currentTimeMillis()));
    }

    public GiftShopItem getGiftShopItem(String version) {
        GetResource getResource = new GetResource("location_" + locationId,version);
        IServerResponse getResourceResponse = (GetResourceResponse) getResource.sendRequest(GetResourceResponse.class);
        Assert.assertNotNull("GetResourceResponse should not return empty response", getResourceResponse);
        List<GiftShopItem> giftShopItems = ((GetResourceResponse) getResourceResponse).getGiftsShopItem();
        Assert.assertTrue("giftShopItems list should not be empty, check GetResourceResponse request!!", !giftShopItems.isEmpty());
        return giftShopItems.get(0);
    }


    public boolean initConfiguration(String locationId, Map<String, String> configFieldMap) {
        ConfigRegistration configRegistration = Initializer.initConfigRegistration(locationId, configFieldMap);
        configRegistration.sendRequestByTokenAndValidateResponse(serverToken, Response.class);
        return false;
    }

    public static Key getDSKey(String sKey) {

        DataStoreKeyUtility dataStoreKeyUtility = new DataStoreKeyUtility();
        log4j.info("get key : " + sKey);
        HttpResponse response = dataStoreKeyUtility.SendRequest(sKey);
        return dataStoreKeyUtility.userKey((String)response.getBody());
    }

    public void updateConsentFlag (boolean consentIsOn){
        log4j.info("Set server flag 'registerMembersByConsent' to " + consentIsOn);
        Consent consent = new Consent(locationId);
        consent.setConsentIsOn(consentIsOn);
        consent.sendRequestByTokenAndValidateResponse(serverToken,ConsentResponse.class);

    }


    public static void updateConsentFlag(String locationId, boolean consentIsOn){
        log4j.info("Set server flag 'registerMembersByConsent' to " + consentIsOn);
        Consent consent = new Consent(locationId);
        consent.setConsentIsOn(consentIsOn);
        consent.sendRequestByTokenAndValidateResponse(serverToken,ConsentResponse.class);

    }

    public static String generateReferralCode (String userToken) throws Exception{

        ReferralCodeGenerate referralCodeGenerate = new ReferralCodeGenerate();
        String referralCodeGenerateResponse = referralCodeGenerate.sendRequestByUserTokenAndLocationID(userToken,locationId);
        return referralCodeGenerateResponse;

    }


    public static String getUserAtLocationReturnFriendReferralCode (String userToken) {

        GetUserAtLocation getUserAtLocation = new GetUserAtLocation();
        getUserAtLocation.setLocationID(locationId);
        GetUserAtLocationResponse getUserAtLocationResponse = (GetUserAtLocationResponse) getUserAtLocation.sendRequestByUserToken(userToken,GetUserAtLocationResponse.class);
        return getUserAtLocationResponse.getReferralCode();
    }


    public static String validateReferralCode (String userToken,String code) throws Exception {

        ReferralCodeValidate referralCodeValidate = new ReferralCodeValidate();
        return referralCodeValidate.sendRequestByUserTokenAndLocationIDAndCode(userToken,locationId,code);


    }

    public void checkDataStoreUpdatedMembersRelation(String advocateKey, String referredKey, int numOfActions, LocalDatastore dataStore) throws Exception {
        log4j.info("check Data Store Updated with " + MEMBERS_RELATION);
        Query q = new Query(MEMBERS_RELATION);
        Query.CompositeFilter filter = new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("advocateKey", Query.FilterOperator.EQUAL, advocateKey),
                new Query.FilterPredicate("referredKey", Query.FilterOperator.EQUAL,referredKey)));
        q.setFilter(filter);
        List<Entity> res = ServerHelper.queryWithWait("Must find"  + numOfActions + " " + MEMBERS_RELATION  + "with the relation: " + MEMBERS_RELATION,
                q, numOfActions, dataStore);

        Assert.assertTrue("Must find " + numOfActions +  "relation with the action: " + MEMBERS_RELATION,res.size() == numOfActions);
        log4j.info("Number of relations is as expected,  relation between advocateKey: " + advocateKey + "and referredKey: "+ referredKey );



    }


}
