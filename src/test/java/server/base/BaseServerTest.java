package server.base;

import com.mashape.unirest.http.Unirest;
import common.BaseTest;
import hub.services.member.MembersService;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContexts;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.*;
import server.common.asset.AssignAsset;
import server.internal.ApiClientResponse;
import server.internal.CreateApiClient;
import server.internal.NewLocation;
import server.utils.ServerHelper;
import server.v2_8.JoinClubResponse;
import server.v2_8.RegisterResponse;
import server.v2_8.common.Asset;
import utils.EnvProperties;
import utils.PropsReader;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import static org.apache.http.conn.ssl.SSLConnectionSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

/**
 * Created by gili on 11/2/16.
 */
public class BaseServerTest extends BaseTest {

    public static final String AUTOMATION_LOCATION_PREFIX = "auto_";
    public static final String APP_NAME_PREFIX = "MyAutoApp_";
    public static String deviceId;
    public static String installationId;


    public static boolean updateConfiguration = true;

    public static void init() throws IOException {
        logPrefix = "Server-" + logPrefix;

        if(firstTime) {
            try {
                initBase();

                if (isNewApplication) {
                    BaseServerTest.initServerNewLocation();
                }

                MembersService.setLogger(log4j);
                BaseServerTest.initServer();
                firstTime = false;
            } catch (Exception e) {
                log4j.error(e);
                firstTime = false;
            }
        }

    }

    @Rule
    public TestWatcher methodsWatcher = new TestWatcher(){


        @Override
        protected void starting(Description description){
            setApiClientDefaults();
        }
    };
    private static void setApiClientDefaults() {
        log4j.info("Set defaults for POS Client properties");
        serverHelper.saveApiClient(
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),false),
                ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()),
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key(),true),
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_BUDGET_PAYMENT_BY_POS_IDENTIFIERS.key(),false),
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT.key(),false),
                ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(),false),
                ApiClientBuilder.newPair(ApiClientFields.AUTOMATION_DELAY_MS.key(),2000),
                ApiClientBuilder.newPair(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(), false));
    }

    protected static String getAssetFromDifferentLocation() throws Exception {
        init();
        JoinClubResponse newMember = serverHelper.createMember();

        //create asset key for existing asset at other location
        Asset asset = AssignAsset.assignAssetToMember("gift", "None", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());
        return asset.getKey();
    }

    private static void initServerNewLocation() {
        String shortUUID = serverHelper.getShortUUID();
        String autoLocationId = AUTOMATION_LOCATION_PREFIX + shortUUID;
        String appName = APP_NAME_PREFIX + shortUUID;
        serverToken = PropsReader.getPropValuesForEnv("serverToken");
        log4j.info("serverToken: " + serverToken);
        log4j.info("Creating new location: " + autoLocationId);
        createNewLocation(autoLocationId,appName);

        String createdApiKey = createNewApiClient(autoLocationId,appName);
        log4j.info("New location created -  apiKey: " + createdApiKey);
        EnvProperties envProperties = EnvProperties.getInstance();
        envProperties.setApiKey(createdApiKey);
        envProperties.setLocationId(autoLocationId);
        envProperties.setServerToken(serverToken);

        log4j.info("ApiKey:" + createdApiKey);
        log4j.info("LocationId: " + autoLocationId);
        log4j.info("appName: " + appName);
    }




    private static void createNewLocation(String autoLocationId, String appName) {

        NewLocation newLocation = new NewLocation();
        newLocation.setToken(PropsReader.getPropValuesForEnv("serverToken"));
        newLocation.setName(appName);
        newLocation.setLocationID(autoLocationId);
        newLocation.setTimeZone("GMT+247");
        newLocation.setSecretCode(autoLocationId);
        newLocation.setLiveZappVersion(484);
        newLocation.setMimeType("application/json");
        newLocation.setZappType("Generic1_LTR");
        newLocation.setCountryCode("972");

        IServerResponse newLocationResponse = newLocation.sendRequest(Response.class);
        Assert.assertEquals("status code must be 200", newLocationResponse.getStatusCode(), (Integer) 200);

    }

    private static String createNewApiClient(String autoLocationId, String appName) {

        CreateApiClient createApiClient = new CreateApiClient();
        createApiClient.setBusinessId(autoLocationId);
        createApiClient.setName(appName);
        IServerResponse saveApiClientResponse = createApiClient.sendRequestByUserTokenAndLocationID(serverToken,autoLocationId, ApiClientResponse.class);
        Assert.assertEquals("status code must be 200", saveApiClientResponse.getStatusCode(), (Integer) 200);

        return ((ApiClientResponse)saveApiClientResponse).getApiKeys().get(0).getApiKey();
    }

    public static void initServer() throws IOException {
        SSLContext sslcontext = null;
        try {
            sslcontext = SSLContexts.custom()
                    .loadTrustMaterial(null, new TrustSelfSignedStrategy())
                    .build();
        } catch (KeyManagementException e) {
            log4j.error(e);
        } catch (NoSuchAlgorithmException e) {
            log4j.error(e);
        } catch (KeyStoreException e) {
            log4j.error(e);
        }

        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, ALLOW_ALL_HOSTNAME_VERIFIER);
        CloseableHttpClient httpclient = HttpClients.custom()
                .setSSLSocketFactory(sslsf)
                .build();
        Unirest.setHttpClient(httpclient);

        initProperties();

    }

    private static void initProperties() throws IOException {
        if(isNewApplication) {
            apiKey = EnvProperties.getInstance().getApiKey();
            locationId = EnvProperties.getInstance().getLocationId();
            serverToken = EnvProperties.getInstance().getServerToken();

        }else {
            //takes the properties data from the hub.config.properties
            apiKey = PropsReader.getPropValuesForEnv("apiKey");
            locationId = PropsReader.getPropValuesForEnv("locationID");
            serverToken = PropsReader.getPropValuesForEnv("serverToken");
        }

        deviceId = getRandomId();
        installationId = getRandomId();
        serverHelper = new ServerHelper(locationId, apiKey, serverToken);
        setServerFlags();
    }

    protected JoinClubResponse createMember() throws InterruptedException {
        JoinClubResponse response = serverHelper.createMember();
        return response;
    }

    public static String getRandomId(){
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            log4j.error(e);
        }
        return String.valueOf(System.currentTimeMillis());
    }

}
