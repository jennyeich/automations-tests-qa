package server.base;

import com.google.appengine.api.datastore.Query;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import server.common.IServerResponse;
import server.v2_8.SaveEventBasedActionRule;
import server.v2_8.SaveEventBasedActionRuleResponse;
import server.v2_8.SubmitPurchase;
import server.v2_8.base.ZappServerErrorResponse;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.ArrayList;

public class BasePurchaseTest extends BaseServerTest {


    protected String transactionId;
    public static final Logger log4j = Logger.getLogger(BasePurchaseTest.class);
    private static boolean purchaseAutomationCreated = false;

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        updateConfiguration = true;
        init();
        if(!purchaseAutomationCreated) {
            //For all tests that required that purchases will be in status 'Analyzed'
            setSmartAutomationMakeAPurchase();
            purchaseAutomationCreated = true;
        }
    }

    public static void setSmartAutomationMakeAPurchase(){
        try {


            SaveEventBasedActionRule eventBasedActionRule = new SaveEventBasedActionRule(locationId,locationId,"purchaseAnalyzed");


            log4j.info("send request for creating purchaseAnalyzed automation rule");
            // Send the request
            IServerResponse response = eventBasedActionRule.SendRequestByToken(serverToken, SaveEventBasedActionRuleResponse.class);

            // make sure the response is not Error response
            if (response instanceof ZappServerErrorResponse) {
                Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
            }
            Thread.sleep(7000);
            // Cast to SubmitPurchaseResponse object to fetch all response data
            SaveEventBasedActionRuleResponse eventBasedActionRuleResponse = (SaveEventBasedActionRuleResponse) response;
            log4j.info("Status of automation  " + eventBasedActionRule.getRuleId() + " :" + eventBasedActionRuleResponse.getStatus());

        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
    }

    protected Query getOueryForUserAction(String transactionId) {
        Query q = new Query("UserAction");
        ArrayList<Query.Filter> filters = new ArrayList<>();
        filters.add(new Query.FilterPredicate("TransactionID",Query.FilterOperator.EQUAL, transactionId));
        filters.add(new Query.FilterPredicate("Action",Query.FilterOperator.EQUAL, "purchase"));
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, filters));
        return q;
    }

}
