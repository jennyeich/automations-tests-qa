package server.internal;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import server.base.BaseServerTest;
import server.common.IServerResponse;
import server.v2_8.base.ServicesErrorResponse;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

/**
 * Created by ariel on 6/11/17.
 */
public class GetLocationStateTest  extends BaseServerTest {

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }

    /**
     * C2448 - New API - GetLocationState - Valid
     */

    @Test
    public void testGetLocationState() throws IOException, InterruptedException {
        // Get Location State
        GetLocationState getLocationState = new GetLocationState(locationId);
        IServerResponse response = getLocationState.sendRequest(GetLocationStateResponse.class);

        // Make sure the response is not Error response
        if(response instanceof ServicesErrorResponse){
            Assert.fail(((ServicesErrorResponse)response).getErrorMessage());
        }

        String mustNotBeNull = " must not be null";
        String mismatch = " mismatch";

        GetLocationStateResponse getLocationStateResponse = (GetLocationStateResponse) response;
        Assert.assertNotNull(GetLocationStateResponse.LOCATION_ID +  mustNotBeNull, getLocationStateResponse.getLocationId());
        Assert.assertEquals(GetLocationStateResponse.LOCATION_ID +  mismatch, locationId, getLocationStateResponse.getLocationId());
        Assert.assertNotNull(GetLocationStateResponse.NAME +  mustNotBeNull, getLocationStateResponse.getName());
        Assert.assertNotNull(GetLocationStateResponse.KEY +  mustNotBeNull, getLocationStateResponse.getKey());
        Assert.assertNotNull(GetLocationStateResponse.KIND +  mustNotBeNull, getLocationStateResponse.getKind());
        Assert.assertNotNull(GetLocationStateResponse.LIVE_ZAPP_VERSION +  mustNotBeNull, getLocationStateResponse.getLiveZappVersion());
        Assert.assertNotNull(GetLocationStateResponse.ZAPP_TYPE +  mustNotBeNull, getLocationStateResponse.getZappType());
        Assert.assertNotNull(GetLocationStateResponse.CREATED_ON +  mustNotBeNull, getLocationStateResponse.getCreatedOn());
        Assert.assertNotNull(GetLocationStateResponse.TIME_ZONE +  mustNotBeNull, getLocationStateResponse.getTimeZone());
    }

    /**
     * C2454 - New API - GetLocationState - Invalid
     */

    @Test
    public void testGetLocationStateInvalid() throws IOException, InterruptedException {
        // Get Location State
        String invalidLocationId = "invalidLocationId";
        GetLocationState getLocationState = new GetLocationState(invalidLocationId);
        IServerResponse response = getLocationState.sendRequest(GetLocationStateResponse.class);

        // Make sure the response is an Error response
        Assert.assertTrue("Response should be an error", response instanceof ServicesErrorResponse);
        Assert.assertEquals("Status error code should be " + httpStatusCode404,  httpStatusCode404, response.getStatusCode());
    }
}
