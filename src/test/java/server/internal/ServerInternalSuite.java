package server.internal;


import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import server.base.BaseServerTest;

import java.io.IOException;

/**
 * Created by Goni on 7/10/2017.
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
        GetMemberAssetsTest.class,
        GetZappTest.class,
        ServerInfraTest.class,
        SignOnTest.class,
        GetLocationStateTest.class,
        UnsubscribeV1Test.class,
        UnsubscribeV4Test.class,
        FriendReferralTest.class
})
public class ServerInternalSuite {
    public static final Logger log4j = Logger.getLogger(ServerInternalSuite.class);


    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseServerTest.init();
    }

    @AfterClass
    public static void tearDown(){
        BaseServerTest.cleanBase();
    }

}
