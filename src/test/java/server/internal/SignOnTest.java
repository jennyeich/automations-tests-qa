package server.internal;

import server.base.BaseServerTest;
import server.common.IServerResponse;
import server.v2_8.base.ServicesErrorResponse;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import java.io.IOException;
import java.lang.invoke.MethodHandles;

/**
 * Created by ariel on 6/15/17.
 */
public class SignOnTest  extends BaseServerTest {

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }
    /**
     * C2536 New API - SignOn - Valid
     */

    @Test
    public void testSignOn() throws IOException, InterruptedException {
        // SignOn
        SignOn signOn = new SignOn();
        signOn.setDeviceId(deviceId);
        signOn.setInstallationId(installationId);
        IServerResponse response = signOn.sendRequest(SignOnResponse.class);

        // Make sure the response is not Error response
        if(response instanceof ServicesErrorResponse){
            Assert.fail(((ServicesErrorResponse)response).getErrorMessage());
        }

        String mustNotBeNull = " must not be null";
        SignOnResponse signOnResponse = (SignOnResponse) response;
        Assert.assertNotNull(SignOnResponse.TOKEN + mustNotBeNull, signOnResponse.getToken());
        Assert.assertNotNull(SignOnResponse.USER_DATA + mustNotBeNull, signOnResponse.getUserData());
        Assert.assertNotNull(SignOnResponse.SUCCESS + mustNotBeNull, signOnResponse.getSuccess());
        Assert.assertEquals(SignOnResponse.SUCCESS + " must be " + true, true, signOnResponse.getSuccess());
    }

    /**
     * C2537 New API - SignOn - Invalid
     */

    @Test
    public void testSignOnInvalid() throws IOException, InterruptedException {
        // SignOn
        SignOn signOn = new SignOn();
        signOn.setDeviceId("");
        signOn.setInstallationId("");
        IServerResponse response = signOn.sendRequest(SignOnResponse.class);

        // Make sure the response is an Error response
        Assert.assertTrue("Response should be an error", response instanceof ServicesErrorResponse);
        Assert.assertEquals("Status error code should be " + httpStatusCode500,  httpStatusCode500, response.getStatusCode());
    }
}
