package server.internal;

import server.base.BaseServerTest;
import server.common.IServerResponse;
import server.v2_8.base.ServicesErrorResponse;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.lang.invoke.MethodHandles;

/**
 * Created by ariel on 6/12/17.
 */
public class GetZappTest  extends BaseServerTest {

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }
    /**
     * C2449 New API - GetZapp - Valid
     */

    @Test
    public void testGetZapp() throws IOException, InterruptedException {
        // Call GetLocationState to get ZappVersion and ZappType
        GetLocationState getLocationState = new GetLocationState(locationId);
        IServerResponse response = getLocationState.sendRequest(GetLocationStateResponse.class);

        // Make sure the response is not Error response
        if(response instanceof ServicesErrorResponse){
            Assert.fail(((ServicesErrorResponse)response).getErrorMessage());
        }

        String mustNotBeNull = " must not be null";
        String mismatch = " mismatch";
        String zappKind = "Zapp";

        GetLocationStateResponse getLocationStateResponse = (GetLocationStateResponse) response;
        Assert.assertNotNull(GetLocationStateResponse.LOCATION_ID + mustNotBeNull, getLocationStateResponse.getLocationId());
        String zappVersion = getLocationStateResponse.getLiveZappVersion();
        String zappType = getLocationStateResponse.getZappType();
        String zappVariation = "normal";

        // Get Zapp
        GetZapp getZapp = new GetZapp(zappVersion, zappType, zappVariation);
        response = getZapp.sendRequest(GetZappResponse.class);

        // Make sure the response is not Error response
        if(response instanceof ServicesErrorResponse){
            Assert.fail(((ServicesErrorResponse)response).getErrorMessage());
        }

        GetZappResponse getZappResponse = (GetZappResponse) response;
        Assert.assertNotNull(GetZappResponse.VERSION + mustNotBeNull, getZappResponse.getVersion());
        Assert.assertNotNull(GetZappResponse.KEY + mustNotBeNull, getZappResponse.getKey());
        Assert.assertNotNull(GetZappResponse.CREATED_ON + mustNotBeNull, getZappResponse.getCreatedOn());
        Assert.assertNotNull(GetZappResponse.LAST_UPDATE + mustNotBeNull, getZappResponse.getLastUpdate());
        Assert.assertNotNull(GetZappResponse.KIND + mustNotBeNull, getZappResponse.getKind());
        Assert.assertNotNull(GetZappResponse.TYPE + mustNotBeNull, getZappResponse.getType());
        Assert.assertNotNull(GetZappResponse.VARIATION + mustNotBeNull, getZappResponse.getVariation());
        Assert.assertNotNull(GetZappResponse.VALID_XML + mustNotBeNull, getZappResponse.getValidXml());
        Assert.assertNotNull(GetZappResponse.NOTES + mustNotBeNull, getZappResponse.getNotes());
        Assert.assertNotNull(GetZappResponse.COMPRESSED + mustNotBeNull, getZappResponse.getCompressed());
        Assert.assertNotNull(GetZappResponse.APP_FILE + mustNotBeNull, getZappResponse.getAppFile());
        Assert.assertEquals(GetZappResponse.KIND + " must be " + zappKind, zappKind, getZappResponse.getKind());
        Assert.assertEquals(GetZappResponse.VERSION + mismatch, zappVersion, getZappResponse.getVersion());
        Assert.assertEquals(GetZappResponse.TYPE + mismatch, zappType, getZappResponse.getType());
        Assert.assertEquals(GetZappResponse.VARIATION + mismatch, zappVariation, getZappResponse.getVariation());
    }

    @Test
    public void testGetZappInvalid() throws IOException, InterruptedException {

        String zappVersion = "invalidZappVersion";
        String zappType = "invalidZappType";
        String zappVariation = "normal";

        // Get Zapp
        GetZapp getZapp = new GetZapp(zappVersion, zappType, zappVariation);
        IServerResponse response = getZapp.sendRequest(GetZappResponse.class);

        // Make sure the response is an Error response
        Assert.assertTrue("Response should be an error", response instanceof ServicesErrorResponse);
        Assert.assertEquals("Status error code should be " + httpStatusCode404,  httpStatusCode404, response.getStatusCode());
    }
}
