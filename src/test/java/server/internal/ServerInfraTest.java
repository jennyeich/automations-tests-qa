package server.internal;

import com.google.appengine.api.datastore.Entity;
import hub.base.categories.serverCategories.ServerRegression;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.base.BaseServerTest;
import server.common.AnonymousPurchaseTypes;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.common.Response;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;


public class ServerInfraTest extends BaseServerTest {
    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }

    @Test
    @Category(ServerRegression.class)
    public void testSaveApiClient() throws Exception{
        //SaveAnonymousPurchaseOn
        UpdateApiClient saveApiClientRequest = new UpdateApiClient(serverToken);
        saveApiClientRequest.addProp(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type());
        IServerResponse saveApiClientResponse = saveApiClientRequest.sendRequestByLocation(locationId, Response.class);
        Assert.assertEquals("status code must be 200", saveApiClientResponse.getStatusCode(), (Integer) 200);

        //Verify 'SaveAnonymousPurchaseOn' status with DataStore
        List<Entity> result = serverHelper.getBusinessFromApiClient(dataStore);
        Assert.assertEquals(AnonymousPurchaseTypes.ALWAYS.type() + "should be Always", AnonymousPurchaseTypes.ALWAYS.type(), result.get(0).getProperty(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key()));
    }

}
