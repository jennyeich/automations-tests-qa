package server.internal;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import hub.utils.HubMemberCreator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import server.base.BaseServerTest;
import server.utils.ServerHelper;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class FriendReferralTest extends BaseServerTest {

    private final String GENERATE_CODE_REFERRAL = "GenerateReferralCode";
    private final String JOIN_CLUB = "joinedclub";
    private final String FRIEND_REFERRAL = "FriendReferral";
    private final String CODE = "code";
    private final String USER_TOKEN = "userToken";
    private final String MEMBERSHIP_KEY = "membershipKey";



    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }

    @Test
    //@Category(ServerRegression.class)
    public void testGenerateReferralCode() throws Exception {

        String userToken = serverHelper.createNewMemberGetUserToken();
        String code = ServerHelper.generateReferralCode(userToken);
        Assert.assertNotEquals("The code is not generated: ", "", code);
        log4j.info("The code is: " + code);

    }


    @Test
    //@Category(ServerRegression.class)
    public void testGenerateReferralCodeAndValidateTheCode() throws Exception {


        Map map = serverHelper.createAndValidateCode();
        String userToken = map.get(USER_TOKEN).toString();
        String code = map.get(CODE).toString();

        String validate = serverHelper.validateReferralCode(userToken,code);
        Assert.assertEquals("The code was not as expected: " , "success", validate);
        String membershipKey = map.get(MEMBERSHIP_KEY).toString();
        log4j.info("The membershipKey is: " + membershipKey);
        Key membershipKeyDataStore = HubMemberCreator.convertUSerKeyToDataStoreKey(membershipKey);
        checkDataStoreUpdatedWithActionByLastUserKey(membershipKeyDataStore,1,code);

    }

    @Test
    public void testGenerateReferralCodeFewTimesUserActionIsOnce() throws Exception {

        Map map = serverHelper.createAndValidateCode();
        String userToken = map.get(USER_TOKEN).toString();
        String code = map.get(CODE).toString();

        String validate = serverHelper.validateReferralCode(userToken,code);
        Assert.assertEquals("The code was not as expected: " , "success", validate);
        for (int i=0; i<5 ; i++)
            serverHelper.generateReferralCode(userToken);

        String membershipKey = map.get(MEMBERSHIP_KEY).toString();
        log4j.info("The membershipKey is: " + membershipKey);
        Key membershipKeyDataStore = HubMemberCreator.convertUSerKeyToDataStoreKey(membershipKey);
        checkDataStoreUpdatedWithActionByLastUserKey(membershipKeyDataStore,1,code);

    }



    @Test
    public void testJoinWithReferralCode () throws Exception {

        Map map = serverHelper.createAndValidateCode();
        String code = map.get(CODE).toString();
        String advocateMembership = map.get(MEMBERSHIP_KEY).toString();
        joinClubWithRefferalCode(code,advocateMembership,1);

    }


    @Test
    public void testMembersRelation () throws Exception {


        Map map = serverHelper.createAndValidateCode();
        String code = map.get(CODE).toString();
        String advocateMembership = map.get(MEMBERSHIP_KEY).toString();

        String phoneNumber = String.valueOf(System.currentTimeMillis());
        String userToken = serverHelper.signON ();

        String membershipKey = serverHelper.joinClubWithUserTokenAndReferralCode(phoneNumber,userToken,"App",code);
        log4j.info("Created member through referral code with membershipKey: " + membershipKey);

        serverHelper.checkDataStoreUpdatedMembersRelation(advocateMembership,membershipKey,1,dataStore);



    }

    @Test
    public void testJoinWithReferralCodeFewMembersWithTheSameCode () throws Exception {

        Map map = serverHelper.createAndValidateCode();
        String code = map.get(CODE).toString();
        String advocateMembership = map.get(MEMBERSHIP_KEY).toString();

        for (int i=1; i<6; i++) {
            joinClubNoDataStoreValidation(code, advocateMembership,i);
            log4j.info("User num " + i +  " joined the club through referral code: " + code);

        }
    }



    @Test
    public void testGenerateReferralCodeAndGetUserAtLocation() throws Exception {

        Map map = serverHelper.createAndValidateCode();
        String userToken = map.get(USER_TOKEN).toString();
        String code = map.get(CODE).toString();
        log4j.info("The code is: " + code);

        String codeFromUserAtLocation = getUserAtLocattion (userToken);
        Assert.assertEquals("The code is not as expected", code,codeFromUserAtLocation);
        log4j.info("The codeFromUserAtLocation is: " + codeFromUserAtLocation);

    }


    @Test
    public void testGetUserAtLocationWithNoReferralCode() throws Exception {

        Map map = serverHelper.createMemberWithUserToken();
        String userToken = map.get(USER_TOKEN).toString();


        String codeFromUserAtLocation = getUserAtLocattion (userToken);
        Assert.assertEquals("The code is not as expected", null,codeFromUserAtLocation);
        log4j.info("The codeFromUserAtLocation is: " + codeFromUserAtLocation);

    }


    private String getUserAtLocattion (String userToken) {

        return serverHelper.getUserAtLocationReturnFriendReferralCode(userToken);

    }


    private void joinClubWithRefferalCode (String code, String advocateMembership, int i) throws Exception{

        String phoneNumber = String.valueOf(System.currentTimeMillis());
        String userToken = serverHelper.signON ();

        String membershipKey = serverHelper.joinClubWithUserTokenAndReferralCode(phoneNumber,userToken,"App",code);
        log4j.info("Created member through referral code with membershipKey: " + membershipKey);
        Key membershipKeyDataStore = HubMemberCreator.convertUSerKeyToDataStoreKey(membershipKey);
        checkDataStoreUpdatedJoinedClubUserAction(membershipKeyDataStore,1,code,advocateMembership);

        Key advocateMembershipDataStore = HubMemberCreator.convertUSerKeyToDataStoreKey(advocateMembership);
        checkDataStoreUpdatedFriendReferralUserAction(advocateMembershipDataStore,i,membershipKey);

    }


    private void joinClubNoDataStoreValidation (String code, String advocateMembership, int i) throws Exception{


        String phoneNumber = String.valueOf(System.currentTimeMillis());
        String userToken = serverHelper.signON ();

        String membershipKey = serverHelper.joinClubWithUserTokenAndReferralCode(phoneNumber,userToken,"App",code);
        log4j.info("Created member through referral code with membershipKey: " + membershipKey);
        Key membershipKeyDataStore = HubMemberCreator.convertUSerKeyToDataStoreKey(membershipKey);
        checkDataStoreUpdatedJoinedClubUserAction(membershipKeyDataStore,1,code,advocateMembership);

        Key advocateMembershipDataStore = HubMemberCreator.convertUSerKeyToDataStoreKey(advocateMembership);
        checkDataStoreUpdatedFriendReferralUserActionNoDataValidation(advocateMembershipDataStore,i);

    }





    private void checkDataStoreUpdatedWithActionByLastUserKey(Key membershipKeyDataStore,int numOfActions, String code) throws Exception {
        log4j.info("check Data Store Updated with " + GENERATE_CODE_REFERRAL);
        Query q = new Query("UserAction");
        Query.CompositeFilter filter = new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, GENERATE_CODE_REFERRAL),
                new Query.FilterPredicate("MembershipKey", Query.FilterOperator.EQUAL,membershipKeyDataStore)));
        q.setFilter(filter);
        List<Entity> res = ServerHelper.queryWithWait("Must find 1 UserAction with the action: " + GENERATE_CODE_REFERRAL,
                q, numOfActions, dataStore);

        Assert.assertTrue("Must find 1 UserAction with the action: " + GENERATE_CODE_REFERRAL,res.size() == numOfActions);

        Text t = ((Text)res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());
        Assert.assertTrue("Data must contain the code: ", t.getValue().contains(code));


    }



    private void checkDataStoreUpdatedJoinedClubUserAction(Key membershipKeyDataStore,int numOfActions, String code, String advocateMembership) throws Exception {
        log4j.info("check Data Store Updated with " + JOIN_CLUB);
        Query q = new Query("UserAction");
        Query.CompositeFilter filter = new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, JOIN_CLUB),
                new Query.FilterPredicate("MembershipKey", Query.FilterOperator.EQUAL,membershipKeyDataStore)));
        q.setFilter(filter);
        List<Entity> res = ServerHelper.queryWithWait("Must find 1 UserAction with the action: " + JOIN_CLUB,
                q, numOfActions, dataStore);

        Assert.assertTrue("Must find 1 UserAction with the action: " + JOIN_CLUB,res.size() == numOfActions);

        Text t = ((Text)res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());
        Assert.assertTrue("Data must contain the AdvocateMembershipKey: ", t.getValue().contains("AdvocateMembershipKey=\"" + advocateMembership));


    }

    private void checkDataStoreUpdatedFriendReferralUserAction(Key membershipKeyDataStore,int numOfActions, String advocateMembership) throws Exception {

        List<Entity> res = checkDataStoreUpdatedFriendReferralUserActionNoDataValidation(membershipKeyDataStore,numOfActions);

        Text t = ((Text)res.get(0).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());
        Assert.assertTrue("Data must contain the ReferredMembershipKey: ", t.getValue().contains("ReferredMembershipKey=\"" + advocateMembership));


    }


    private List<Entity>  checkDataStoreUpdatedFriendReferralUserActionNoDataValidation(Key membershipKeyDataStore,int numOfActions) throws Exception {

        log4j.info("check Data Store Updated with " + FRIEND_REFERRAL);
        Query q = new Query("UserAction");
        Query.CompositeFilter filter = new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, FRIEND_REFERRAL),
                new Query.FilterPredicate("MembershipKey", Query.FilterOperator.EQUAL,membershipKeyDataStore)));
        q.setFilter(filter);
        List<Entity> res = ServerHelper.queryWithWait("Must find 1 UserAction with the action: " + FRIEND_REFERRAL,
                q, numOfActions, dataStore);

        Assert.assertTrue("Must find 1 UserAction with the action: " + FRIEND_REFERRAL,res.size() == numOfActions);

        return res;

    }




}
