package server.internal;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import server.base.BaseServerTest;
import server.common.IServerResponse;
import server.common.Response;
import server.utils.Initializer;
import server.v2_8.ConfigRegistration;
import server.v4.RegisterMemberV4Response;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;

public class UnsubscribeV4Test extends BaseServerTest {

    private static Map<String,String> configFieldMap = new HashMap<>();
    private static final String SUBSCRIBE_NOT_ALLOWED_MESSAGE = "Subscribing existing members to SMS/EMAIL service isn't allowed";


    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
        serverHelper.setRegistrationConfigurationV2();

    }

    // register member without setting allow email and allow sms - both will be null
    @Test
    public void testRegisterWithoutAlllowEmailAndAllowSMS() throws Exception{
        String customerPhone = String.valueOf(System.currentTimeMillis());
        log4j.info("Creating member with phone number: " + customerPhone);

        RegisterMemberV4Response registerMemberV4Response = serverHelper.registerMemberV4(customerPhone, null, null, updateConfiguration);
        Assert.assertNotNull(registerMemberV4Response);
        String actualStatusCode = registerMemberV4Response.getStatusCode().toString();
        Assert.assertEquals(String.format("Response code expected to be 200 but actuallt was %s", actualStatusCode) , "200", actualStatusCode);
        //TODO: add verification in data store

        ConfigRegistration configRegistration = Initializer.initConfigRegistration(locationId, configFieldMap);
        configRegistration.sendRequestByTokenAndValidateResponse(serverToken, Response.class);
        updateConfiguration = false;

    }

    @Test
    public void testRegisterAllowSMSFalse() throws Exception{
        String customerPhone = String.valueOf(System.currentTimeMillis());
        log4j.info("Creating member with phone number: " + customerPhone);
        createMember(customerPhone, Boolean.FALSE, Boolean.TRUE);
        //TODO: add verification in data store

    }

    @Test
    public void testRegisterAllowEmailFalse() throws Exception{
        String customerPhone = String.valueOf(System.currentTimeMillis());
        log4j.info("Creating member with phone number: " + customerPhone);
        createMember(customerPhone, Boolean.TRUE, Boolean.FALSE);
        //TODO: add verification in data store
    }

    @Test
    public void testUpdateUnsubscribeSMSAndEmail() throws Exception{
        updateConfiguration = true;
        String customerPhone = String.valueOf(System.currentTimeMillis());
        log4j.info("Creating member with phone number: " + customerPhone);
        createMember(customerPhone, Boolean.TRUE, Boolean.TRUE);

        IServerResponse updateMemberResponse = serverHelper.updateMemberV4(customerPhone, Boolean.FALSE, Boolean.FALSE);
        Assert.assertNotNull(updateMemberResponse);
        String actualStatusCode = updateMemberResponse.getStatusCode().toString();
        Assert.assertEquals(String.format("Response code expected to be 200 but actuallt was %s", actualStatusCode) , "200", actualStatusCode);
        //TODO: add verification in data store
    }

    @Test
    public void testNegativeUpdateSubscribeEmail() throws Exception{
        String customerPhone = String.valueOf(System.currentTimeMillis());
        log4j.info("Creating member with phone number: " + customerPhone);
        createMember(customerPhone, Boolean.TRUE,  Boolean.FALSE);

        IServerResponse updateMemberResponse = serverHelper.updateMemberV4(customerPhone, Boolean.FALSE, Boolean.TRUE);
        Assert.assertNotNull(updateMemberResponse);
        String actualStatusCode = updateMemberResponse.getStatusCode().toString();
        Assert.assertEquals(String.format("Response code expected to be 400 but actually was %s", actualStatusCode) , "400", actualStatusCode);

        JSONObject jsonObject = ((JSONObject)((JSONArray)(updateMemberResponse.getRawObject().get(ERRORS))).get(0));
        String errorMessage = jsonObject.get(MESSAGE).toString();
        String code = jsonObject.get(CODE).toString();
        Assert.assertEquals("Response error message was not as expected" , SUBSCRIBE_NOT_ALLOWED_MESSAGE, errorMessage);
        Assert.assertEquals("Response error code was not as expected" , "4000000", code);
        //TODO: add verification in data store
    }

    @Test
    public void testNegativeUpdateSubscribeSMS() throws Exception{
        updateConfiguration = true;
        String customerPhone = String.valueOf(System.currentTimeMillis());
        log4j.info("Creating member with phone number: " + customerPhone);
        createMember(customerPhone, Boolean.FALSE, Boolean.TRUE);

        IServerResponse updateMemberResponse = serverHelper.updateMemberV4(customerPhone, Boolean.TRUE, Boolean.FALSE);
        Assert.assertNotNull(updateMemberResponse);
        String actualStatusCode = updateMemberResponse.getStatusCode().toString();
        Assert.assertEquals(String.format("Response code expected to be 400 but actually was %s", actualStatusCode) , "400", actualStatusCode);

        JSONObject jsonObject = ((JSONObject)((JSONArray)(updateMemberResponse.getRawObject().get(ERRORS))).get(0));
        String errorMessage = jsonObject.get(MESSAGE).toString();
        String code = jsonObject.get(CODE).toString();
        Assert.assertEquals("Response error message was not as expected" , SUBSCRIBE_NOT_ALLOWED_MESSAGE, errorMessage);
        Assert.assertEquals("Response error code was not as expected" , "4000000", code);
        //TODO: add verification in data store
    }

    private RegisterMemberV4Response createMember(String phoneNumber, Boolean allowSMS, Boolean allowEmail) throws Exception{
        RegisterMemberV4Response registerMemberV4Response = serverHelper.registerMemberV4(phoneNumber, allowSMS, allowEmail, updateConfiguration);
        Assert.assertNotNull(registerMemberV4Response);
        String actualStatusCode = registerMemberV4Response.getStatusCode().toString();
        Assert.assertEquals(String.format("Response code expected to be 200 but actually was %s", actualStatusCode) , "200", actualStatusCode);

        return registerMemberV4Response;
    }
}
