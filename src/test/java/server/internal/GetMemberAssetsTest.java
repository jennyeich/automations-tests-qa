package server.internal;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import server.base.BaseServerTest;
import server.common.asset.AssignAsset;
import server.v2_8.JoinClubResponse;
import server.v2_8.RegisterResponse;
import server.v2_8.base.ServicesErrorResponse;
import server.v2_8.common.Asset;
import server.v2_8.common.AssetNew;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.List;

/**
 * Created by ariel on 13/06/17.
 */

public class GetMemberAssetsTest  extends BaseServerTest {


    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
    }
    /**
     * C2450 New API - GetMemberAssets - Valid
     */

    //CNP-8947 - fails because of the bug
    @Test
    public void testGetMemberAssets() throws IOException, InterruptedException {

        JoinClubResponse newMember = createMember();

        Asset newAsset = AssignAsset.assignAssetToMember("gift", "Bulk", newMember.getMembershipKey(), apiKey, locationId, serverToken, newMember.getPhoneNumber());

        // Get Member Assets
        GetMemberAssets getMemberAssets = new GetMemberAssets(newMember.getMembershipKey(), locationId, serverToken);
        Object response = getMemberAssets.sendRequestByClass(AssetNew.class);

        // Make sure the response is not Error response
        if(response instanceof ServicesErrorResponse){
            Assert.fail(((ServicesErrorResponse)response).getErrorMessage());
        }

        List<AssetNew> assets = (List<AssetNew>)response;

        String mustNotBeNull = " must not be null";
        String assetKind = "Asset";

        for(AssetNew asset : assets){
            Assert.assertNotNull(AssetNew.KEY + mustNotBeNull, asset.getKey());
            Assert.assertNotNull(AssetNew.CREATED_ON + mustNotBeNull, asset.getCreateOn());
            Assert.assertNotNull(AssetNew.LAST_UPDATE + mustNotBeNull, asset.getLastUpdate());
            Assert.assertNotNull(AssetNew.KIND + mustNotBeNull, asset.getKind());
            Assert.assertNotNull(AssetNew.ENTITY_ID + mustNotBeNull, asset.getEntityId());
            Assert.assertNotNull(AssetNew.DESCRIPTION + mustNotBeNull, asset.getDescription());
            Assert.assertNotNull(AssetNew.NAME + mustNotBeNull, asset.getName());
            Assert.assertNotNull(AssetNew.TYPE + mustNotBeNull, asset.getType());
            Assert.assertNotNull(AssetNew.COST + mustNotBeNull, asset.getCost());
            Assert.assertNotNull(AssetNew.VALUE + mustNotBeNull, asset.getValue());
            Assert.assertNotNull(AssetNew.STATUS + mustNotBeNull, asset.getStatus());
            Assert.assertNotNull(AssetNew.RANDOM + mustNotBeNull, asset.getRandom());
            Assert.assertNotNull(AssetNew.ARCHIVED + mustNotBeNull, asset.getArchived());
            Assert.assertNotNull(AssetNew.ACTIVE + mustNotBeNull, asset.getActive());
            Assert.assertNotNull(AssetNew.TEMPLATE + mustNotBeNull, asset.getTemplate());
            Assert.assertNotNull(AssetNew.TEMPLATE_KEY + mustNotBeNull, asset.getTemplateKey());
            Assert.assertNotNull(AssetNew.DATA + mustNotBeNull, asset.getData());
            Assert.assertNotNull(AssetNew.TAG + mustNotBeNull, asset.getTag());
            Assert.assertNotNull(AssetNew.VALID_AT_LOCATION_IDS + mustNotBeNull, asset.getValidAtLocationIds());
            Assert.assertNotNull(AssetNew.OWNING_USER + mustNotBeNull, asset.getOwningUser());
            Assert.assertNotNull(AssetNew.EXTERNAL_BENEFITS + mustNotBeNull, asset.getExternalBenefits());
            Assert.assertNotNull(AssetNew.ASSET_SOURCE + mustNotBeNull, asset.getAssetSource());
            Assert.assertNotNull(AssetNew.LAST_ASSIGN_DATE + mustNotBeNull, asset.getLastAssignDate());
//            Assert.assertNotNull(AssetNew.IMAGE + mustNotBeNull, asset.getImage());
            Assert.assertNotNull(AssetNew.REDEEM_ACTION + mustNotBeNull, asset.getRedeemAction());

            Assert.assertEquals(GetZappResponse.KIND + " must be " + assetKind, assetKind, asset.getKind());
        }
    }

    /**
     * C2453 New API - GetMemberAssets - Invalid
     */
    //BUG CNP-9100

    @Test
    public void testGetMemberAssetsInvalid() throws IOException, InterruptedException {
        // Get Member Assets

        GetMemberAssets getMemberAssets = new GetMemberAssets("InvalidMembership", locationId, serverToken);
        Object response = getMemberAssets.sendRequestByClass(AssetNew.class);

        if (response instanceof ServicesErrorResponse)
            Assert.fail("Response should not be an internal error - should return empty array: " +((ServicesErrorResponse) response).getErrorMessage());
        List<AssetNew> assets = (List<AssetNew>)response;
        Assert.assertTrue("Response should return an empty array",  assets.isEmpty());
    }
}
