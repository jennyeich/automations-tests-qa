package server.internal;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import server.base.BaseServerTest;
import server.common.IServerResponse;
import server.common.Response;
import server.utils.Initializer;
import server.v2_8.ConfigRegistration;
import server.v2_8.RegisterResponse;
import server.v2_8.UpdateMembershipV2Response;
import server.v2_8.UpdatedFields;

import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.util.HashMap;
import java.util.Map;

public class UnsubscribeV1Test extends BaseServerTest {

    private static Map<String,String> configFieldMap = new HashMap<>();

    @BeforeClass
    public static void _init() throws IOException {
        logPrefix = MethodHandles.lookup().lookupClass().getSimpleName();
        init();
        configFieldMap = Initializer.initUnsubscribeConfigRegistration();
        serverHelper.initConfiguration(locationId,configFieldMap);
    }
    // register member without setting allow email and allow sms - both will be null
    @Test
    public void testRegisterWithoutAlllowEmailAndAllowSMS() throws Exception{
        String customerPhone = String.valueOf(System.currentTimeMillis());
        log4j.info("Creating member with phone number: " + customerPhone);
        Map<String,Object> fieldsMap = new HashMap<>();
        updateConfiguration = true;
        fieldsMap.put(Initializer.PHONE_NUMBER,customerPhone);

        Map<String,String> configFieldMapWithoutAllow = new HashMap<>();
        configFieldMapWithoutAllow.put(Initializer.PHONE_NUMBER, Initializer.PHONE_NUMBER);
        configFieldMapWithoutAllow.put(Initializer.FIRST_NAME, Initializer.FIRST_NAME);
        configFieldMapWithoutAllow.put(Initializer.LAST_NAME, Initializer.LAST_NAME);

        RegisterResponse registerResponse = serverHelper.registerMemberExternal(fieldsMap, updateConfiguration, configFieldMapWithoutAllow);
        Assert.assertNotNull(registerResponse);
        Assert.assertEquals(String.format("Expected allow email to be True but actually was %s.", registerResponse.getAllowEmail()),null, registerResponse.getAllowEmail());
        Assert.assertEquals(String.format("Expected allow sms to be True but actually was %s.", registerResponse.getAllowSMS()),null, registerResponse.getAllowSMS());

        ConfigRegistration configRegistration = Initializer.initConfigRegistration(locationId, configFieldMap);
        configRegistration.sendRequestByTokenAndValidateResponse(serverToken, Response.class);
        updateConfiguration = false;

    }

    @Test
    public void testRegisterAllowSMSFalse() throws Exception{
        String customerPhone = String.valueOf(System.currentTimeMillis());
        log4j.info("Creating member with phone number: " + customerPhone);
        RegisterResponse registerResponse = createMember(customerPhone, Boolean.FALSE, Boolean.TRUE);
        Assert.assertEquals(String.format("Expected allow email to be True but actually was %s.", registerResponse.getAllowEmail()), Boolean.TRUE, registerResponse.getAllowEmail());
        Assert.assertEquals(String.format("Expected allow sms to be false but actually was %s.", registerResponse.getAllowSMS()),Boolean.FALSE, registerResponse.getAllowSMS());

    }

    @Test
    public void testRegisterAllowEmailFalse() throws Exception{
        String customerPhone = String.valueOf(System.currentTimeMillis());
        log4j.info("Creating member with phone number: " + customerPhone);
        RegisterResponse registerResponse = createMember(customerPhone, Boolean.TRUE, Boolean.FALSE);
        Assert.assertEquals(String.format("Expected allow email to be false but actually was %s.", registerResponse.getAllowEmail()), Boolean.FALSE, registerResponse.getAllowEmail());
        Assert.assertEquals(String.format("Expected allow sms to be True but actually was %s.", registerResponse.getAllowSMS()),Boolean.TRUE, registerResponse.getAllowSMS());
    }

    @Test
    public void testUpdateUnsubscribeSMSAndEmail() throws Exception{
        updateConfiguration = true;
        String customerPhone = String.valueOf(System.currentTimeMillis());
        log4j.info("Creating member with phone number: " + customerPhone);
        createMember(customerPhone, Boolean.TRUE, Boolean.TRUE);


        UpdateMembershipV2Response updateMembershipV2Response = serverHelper.updateMemberExternal(customerPhone, Boolean.FALSE, Boolean.FALSE, updateConfiguration, configFieldMap);
        Assert.assertNotNull(updateMembershipV2Response);
        Assert.assertEquals(String.format("Expected allow email to be false but actually was %s.", updateMembershipV2Response.getAllowEmail()), Boolean.FALSE, updateMembershipV2Response.getAllowEmail());
        Assert.assertEquals(String.format("Expected allow sms to be false but actually was %s.", updateMembershipV2Response.getAllowSMS()),Boolean.FALSE, updateMembershipV2Response.getAllowSMS());

    }

    @Test
    public void testNegativeUpdateSubscribeEmail() throws Exception{
        String customerPhone = String.valueOf(System.currentTimeMillis());
        log4j.info("Creating member with phone number: " + customerPhone);
        RegisterResponse registerResponse = createMember(customerPhone, Boolean.TRUE,  Boolean.FALSE);


        IServerResponse updateMembershipV2Response = serverHelper.updateMemberExternalWithoutValidation(customerPhone, Boolean.TRUE, Boolean.TRUE);
        Assert.assertNotNull(updateMembershipV2Response);
        Assert.assertTrue("Subscribe to email by update member in Reg api (v2.8) should not be allowed.", updateMembershipV2Response.getStatusCode() != 200);
    }

   @Test
    public void testNegativeUpdateSubscribeSMS() throws Exception{
        updateConfiguration = true;
        String customerPhone = String.valueOf(System.currentTimeMillis());
        log4j.info("Creating member with phone number: " + customerPhone);
        RegisterResponse registerResponse =createMember(customerPhone, Boolean.FALSE, Boolean.TRUE);

        UpdatedFields updatedFields = new UpdatedFields();
        updatedFields.setAllowEmail(Boolean.TRUE);
        IServerResponse updateMembershipV2Response = serverHelper.updateMemberExternalWithoutValidation(customerPhone, Boolean.FALSE, Boolean.TRUE);
        Assert.assertNotNull(updateMembershipV2Response);
        Assert.assertTrue("Subscribe to SMS by update member in Reg api (v2.8) should not be allowed.", updateMembershipV2Response.getStatusCode() != 200);
    }

    private RegisterResponse createMember(String phoneNumber, Boolean allowSMS, Boolean allowEmail) throws Exception{
        Map<String,Object> fieldsMap = new HashMap<>();
        fieldsMap.put(Initializer.PHONE_NUMBER,phoneNumber);
        fieldsMap.put(Initializer.ALLOW_SMS, allowSMS);
        fieldsMap.put(Initializer.ALLOW_EMAIL,allowEmail);
        RegisterResponse registerResponse = serverHelper.registerMemberExternal(fieldsMap, updateConfiguration, configFieldMap);
        Assert.assertNotNull(registerResponse);
        return registerResponse;
    }

}
