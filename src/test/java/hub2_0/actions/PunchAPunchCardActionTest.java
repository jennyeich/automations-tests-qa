package hub2_0.actions;

import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.base.categories.hub2Categories.hub2Rules;
import hub.base.categories.hub2Categories.hub2Sanity;
import hub.common.objects.member.NewMember;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.PointsCondition;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.SelectedItemsDialogButtons;
import hub.hub2_0.common.items.SelectItems;
import hub.services.member.MembersService;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.ApiClientFields;
import server.common.LocationBackendFields;
import server.common.PaymentType;
import server.utils.ServerHelper;
import server.utils.UserActionsConstants;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PunchAPunchCardActionTest extends BaseActivityActionTest {

    public static String quantity = "1";

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            if(smartPunchCard == null) {
                //create assets
                try {
                    smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");

                } catch (Exception e) {
                    log4j.error("error while creating asset", e);
                }
            }
        }
    };


    @Override
    protected ActivityAction getConcreteAction() throws Exception {
        timestamp = String.valueOf(System.currentTimeMillis());
        ActivityAction activityAction = ActionsBuilder.buildPunchAPunchCardAction(smartPunchCard.getTitle(), quantity);
        return activityAction;
    }



    /**
     * Test trigger Makes a purchase, no condition ,Action punch a punch card
     */
    @Test
    public void testTriggerMakesAPurchaseAndActionPunchAPunchCard() throws Exception {

        quantity = "1";

        //Create new rule
        createRule(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        //send the punch card so it will activate the automation to punch it
        performAnActionSendPunchCard(smartPunchCard);

        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithPunchByRuleID();

        log4j.info(smartPunchCard.getTitle() + " was punched ");

    }


    /**
     * Test trigger Makes a purchase, for each 2 items of 2233 send punch a punch card twice (occurrences)
     */
    @Test
    @Category({Hub2Regression.class})
    public void testTriggerMakesAPurchasePerOccurrenceModeItemCodeQuantity2AndActionPunchAPunchCard2Times() throws Exception {

        quantity = "2";

        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        selectItems.setItemCodes("2233");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItems);
        occurrencesCondition.setConditionKey(OccurrencesCondition.Condition.IN_THE_QUANTITY_OF);
        occurrencesCondition.setConditionValue("2");

        //Create new rule
        createRulePerOccurrences(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList(), Lists.newArrayList(occurrencesCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        //send the punch card so it will activate the automation to punch it
        performAnActionSendPunchCard(smartPunchCard);

        Item item = createItem("2233","Coke","3", "Drinks", 10000);
        Item item2 = createItem("2233","Coke","3", "Drinks", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, items);

        //DataStore check
        checkAutomationRunAndPunchedThePunchCard(smartPunchCard.getTitle(), 2);


        log4j.info(smartPunchCard.getTitle() + " was punched ");

    }


    /**
     * Test trigger uses points, number of pints is greater than, punch a punch card - 3 punches
     */
    @Test
    @Category( {hub2Sanity.class, hub2Rules.class} )
    public void testTriggerUsesPointsWithConditionNumberOfPointsIsGreaterThanAndActionPunch3Times() throws Exception {

        quantity = "3";

        serverHelper.saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);
        Map<String, Object> props = new HashMap<>();
        props.put(LocationBackendFields.PAY_WITH_BUDGET_TYPE.key(), PaymentType.POINTS.type());
        serverHelper.updateBusinessBackend(props);

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("20");

        //Create new rule
        createRule(ActivityTrigger.UsePoints.toString(), Lists.newArrayList(pointsCondition));

        //Create member
        NewMember newMember = createNewMember();
        //send the punch card so it will activate the automation to punch it
        performAnActionSendPunchCard(smartPunchCard);
        //Add points
        String actionTag = MembersService.performSmartActionAddPoints("5000",timestamp);
        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedPoints = "3000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(newMember, usedPoints);

        //DataStore check
        checkAutomationRunAndPunchedThePunchCard(smartPunchCard.getTitle(), 3);


        log4j.info(smartPunchCard.getTitle() + " was punched ");


    }






}
