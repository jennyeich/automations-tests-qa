package hub2_0.actions;

import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.base.categories.hub2Categories.hub2Rules;
import hub.base.categories.hub2Categories.hub2Sanity;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.NewMember;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.gifts.Gift;
import hub.hub2_0.basePages.loyalty.gifts.GiftDisplay;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountEntireTicketAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.builders.GiftsBuilder;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.PointsCondition;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.ApiClientFields;
import server.common.PaymentType;
import server.utils.ServerHelper;
import server.utils.UserActionsConstants;
import server.v2_8.common.Item;

import java.util.*;

import static integration.base.BaseHub2DealTest.buildAmountOffDiscountOnEntireTicket;

public class SendBenefitActionTest extends BaseActivityActionTest {

    public static String quantity = "1";
    public static Gift newHub2Gift;
    public static GiftDisplay giftDisplay;
    GiftsBuilder giftsBuilder = new GiftsBuilder();

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            createSmartGift();
        }
    };

    @Override
    protected ActivityAction getConcreteAction() throws Exception {
        timestamp = String.valueOf(System.currentTimeMillis());
        ActivityAction activityAction = ActionsBuilder.buildSendBenefitAction(smartGift.getTitle(), quantity);
        return activityAction;
    }


    /**
     * Test trigger joined the club, no condition ,Action send benefit
     */
    @Test
    public void testTriggerJoinedTheClubAndActionSendBenefit() throws Exception {

        //Create new rule
        createRule(ActivityTrigger.JoinTheClub.toString(), Lists.newArrayList());

        //Create member
        createNewMember();
        //DataStore check
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());


        log4j.info(smartGift.getTitle() + " was added to the member ");

    }

    @Test
    public void testTriggerJoinedTheClubAndActionSendHub2Benefit() throws Exception {

        createHub2Gift();

        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.JoinTheClub.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(ActivityActions.SendBenefit), newHub2Gift.getActivityName());
        log4j.info("rule name:" + rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        createNewMember();
        //DataStore check
        checkAutomationRunAndSendAssetToMember(newHub2Gift.getActivityName());

        log4j.info(newHub2Gift.getActivityName() + " was added to the member ");
    }


    /**
     * Test trigger joined the club, no condition ,Action send benefit with quantity 2
     */
    @Test
    public void testTriggerJoinedTheClubAndActionSendBenefitQuantity2() throws Exception {
        quantity = "2";

        //Create new rule
        createRule(ActivityTrigger.JoinTheClub.toString(), Lists.newArrayList());

        //Create member
        createNewMember();
        //DataStore check
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle(), 2);


        log4j.info(smartGift.getTitle() + " was added to the member ");

    }


    /**
     * Test trigger makes a purchase, with condition value of purchase is greater than 50 ,Action send benefit with quantity 3
     */
    @Test
    @Category( {hub2Sanity.class, hub2Rules.class} )
    public void testTriggerMakesAPurchaseAndConditionValueOfPurchaseGreaterThanAndActionSendBenefitQuantity3() throws Exception {

        quantity = "3";

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("50");

        //Create new rule
        createRule(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList(madeAPurchaseCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        //DataStore check
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle(), 3);


        log4j.info(smartGift.getTitle() + " was added to the member ");

    }


    /**
     * Test trigger Uses points, with condition number of points is greater than 20 ,Action send benefit
     */
    @Test
    @Category({Hub2Regression.class})
    public void testTriggerUsesPointsAndConditionNumOfPointsGreaterThanAndActionSendBenefit() throws Exception{

        serverHelper.saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);
        Map<String, Object> props = new HashMap<>();
        props.put("PayWithBudgetType",PaymentType.POINTS.type());
        serverHelper.updateBusinessBackend(props);

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("20");

        //Create new rule
        createRule(ActivityTrigger.UsePoints.toString(), Lists.newArrayList(pointsCondition));

        //Create member
        NewMember newMember = createNewMember();
        //Add points
        String actionTag = MembersService.performSmartActionAddPoints("5000",timestamp);
        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedPoints = "3000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(newMember, usedPoints);

        //DataStore check
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());


        log4j.info(smartGift.getTitle() + " was added to the member ");

    }


    /**
     * Test trigger makes a purchase, with condition value of purchase is less than 200 , for every 2 Coke send benefit (Occurrences)
     */
    @Test
    public void testTriggerMakesAPurchaseAndConditionValueOfPurchaseLessThanPerOccurrencesItemCodeQuantity2AndActionSendBenefit() throws Exception{

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("200");

        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        selectItems.setItemCodes("Coke");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItems);
        occurrencesCondition.setConditionKey(OccurrencesCondition.Condition.IN_THE_QUANTITY_OF);
        occurrencesCondition.setConditionValue("2");

        //Create new rule
        createRulePerOccurrences(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(occurrencesCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("Coke","Coke","3", "Drinks", 2000);
        Item item2 = createItem("Coke","Coke","3", "Drinks", 2000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "4000", new ArrayList<>(), transactionId, items);

        //DataStore check
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());
        log4j.info(smartGift.getTitle() + " was added to the member ");

    }

    @Test
    @Category({Hub2Regression.class})
    public void testTriggerMakesAPurchaseAndConditionBranchIDIsExactlyPerOccurrencesAnyItemInTotalSpendOfAndActionSendBenefit() throws Exception{

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("1");


        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItems);
        occurrencesCondition.setConditionKey(OccurrencesCondition.Condition.IN_THE_TOTAL_SPEND_OF);
        occurrencesCondition.setConditionValue("50");

        //Create new rule
        createRulePerOccurrences(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(occurrencesCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        Item item = createItem("333","DDD","3", "Drinks", 5000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "5000", new ArrayList<>(), transactionId, items);

        //DataStore check
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());
        log4j.info(smartGift.getTitle() + " was added to the member ");


    }


    private void createHub2Gift() throws Exception {

        GiftDisplay giftDisplay = new GiftDisplay();
        giftDisplay.setDisplayTab("");
        giftDisplay.setImageUpload("");
        giftDisplay.setSelectExistImage("");
        giftDisplay.setImageSave("");

        String amountDiscount = "50";
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(),Lists.newArrayList(amountOffDiscountEntireTicketAction),giftDisplay);

        LoyaltyService.returnToCampaignCenterPage();
    }


    private Gift createEntireTicketDiscountGift(List<ApplyCondition> conditions, List<ActivityAction> activityActionList, GiftDisplay giftDisplay) throws Exception {

        Gift gift = giftsBuilder.buildEntireTicketDiscountActivityGift("entireTicketGift_",timestamp.toString(), conditions, activityActionList,giftDisplay);
        gift.clickPublishActivityBtn();
        LoyaltyService.createGift(gift);

        return gift;


    }


}
