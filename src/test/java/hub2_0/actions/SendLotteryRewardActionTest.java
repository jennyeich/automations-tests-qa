package hub2_0.actions;

import com.beust.jcommander.internal.Lists;
import hub.common.objects.benefits.LotteryReward;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartGift;
import hub.discounts.DiscountsConstants;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.PointsCondition;
import hub.hub2_0.common.conditions.TagCondition;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.SelectedItemsDialogButtons;
import hub.hub2_0.common.items.SelectItems;
import hub.services.member.MembersService;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.ApiClientFields;
import server.common.LocationBackendFields;
import server.common.PaymentType;
import server.utils.ServerHelper;
import server.utils.UserActionsConstants;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Deprecated
 //lottery was removed from hub
public class SendLotteryRewardActionTest extends BaseActivityActionTest {

//    public static String quantity = "1";
//    private static LotteryReward lotteryReward;
//    private static LotteryReward lotteryReward1;
//    private static SmartGift smartGift;


//    @Rule
//    public TestWatcher methodsWatcher = new TestWatcher(){
//
//        @Override
//        protected void starting(Description description){
//            timestamp = String.valueOf(System.currentTimeMillis());
//
//            try {
//                //create 2 lottery - because of the new development that if there's only one option
//                // (1 lottery for this business in this case) it's automatically chosen without drop down
//                smartGift = hubAssetsCreator.createSmartGift(timestamp);
//                //lotteryReward = hubAssetsCreator.createLotteryReward(lotteryReward,smartGift.getTitle(),timestamp);
//                lotteryReward = hubAssetsCreator.createLotteryReward(timestamp,smartGift.getTitle(),"50","50");
//                lotteryReward1 = hubAssetsCreator.createLotteryReward(lotteryReward1,smartGift.getTitle(),String.valueOf(System.currentTimeMillis()));
//
//
//
//            } catch (Exception e) {
//                log4j.error("error while creating asset",e);
//            }
//        }
//    };
//
//
//
    @Override
    protected ActivityAction getConcreteAction() throws Exception {
        //ActivityAction activityAction = ActionsBuilder.buildSendLotteryAction(lotteryReward.getTitle(), quantity);
        //return activityAction;
        return null;
    }
//
//    /**
//     * Test trigger joined the club, no condition ,Action send lottery
//     */
//    @Test
//    public void testTriggerJoinedTheClubAndActionSendLottery() throws Exception {
//
//        //Create new rule
//        createRule(ActivityTrigger.JoinTheClub.toString(), Lists.newArrayList());
//
//        //Create member
//        createNewMember();
//        //DataStore check
//        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());
//
//
//        log4j.info(smartGift.getTitle() + " was send to the member ");
//
//    }
//
//    /**
//     * Test trigger Makes A Purchase,condition ValueOfPurchase GreaterThanOrEqualTo And BranchID Exactly,Action send lottery
//     */
//    @Test
//    public void testTriggerMakesAPurchaseAndConditionValueOfPurchaseGreaterThanOrEqualToAndBranchIDExactlyAndActionSendLottery() throws Exception {
//
//
//        MadeAPurchaseCondition condition1 = new MadeAPurchaseCondition();
//        condition1.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
//                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
//                .setConditionValue("50");
//
//        MadeAPurchaseCondition condition2 = new MadeAPurchaseCondition();
//        condition2.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
//                .setOperatorKey(IntOperators.IS_EXACTLY)
//                .setConditionValue(BRANCH_ID);
//
//        //Create new rule
//        createRule(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList(condition1,condition2));
//
//        //Create member
//        NewMember newMember = createNewMember();
//        String transactionId = UUID.randomUUID().toString();
//        performAnActionSubmitPurchase(newMember, "20000", transactionId);
//        //DataStore check
//        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());
//
//
//        log4j.info(smartGift.getTitle() + " was send to the member ");
//
//    }
//
//    /**
//     * Test trigger Makes A Purchase,PerOccurrence ItemCode TotalSpendOf ,Action send lottery
//     */
//    @Test
//    public void testTriggerMakesAPurchasePerOccurrenceItemCodeTotalSpendOfAndActionSendLottery() throws Exception {
//
//
//        SelectItems selectItems = new SelectItems();
//        selectItems.clickItemCodesRadioButton();
//        selectItems.setItemCodes(DiscountsConstants.ITEM_CODE_111);
//        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
//
//        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
//        occurrencesCondition.setSelectItemsDialog(selectItems);
//        occurrencesCondition.setConditionKey(OccurrencesCondition.Condition.IN_THE_TOTAL_SPEND_OF);
//        occurrencesCondition.setConditionValue("200");
//
//        //Create new rule
//        createRulePerOccurrences(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList(), Lists.newArrayList(occurrencesCondition));
//
//        //Create member
//        NewMember newMember = createNewMember();
//        String transactionId = UUID.randomUUID().toString();
//        Item item = createItem(DiscountsConstants.ITEM_CODE_111,"Coke","3", "Drinks", 10000);
//        Item item2 = createItem(DiscountsConstants.ITEM_CODE_111,"Coke","3", "Drinks", 10000);
//        ArrayList<Item> items = new ArrayList<>();
//        items.add(item);
//        items.add(item2);
//        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000", new ArrayList<>(), transactionId, items);
//        //DataStore check
//        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());
//
//        log4j.info(smartGift.getTitle() + " was send to the member ");
//
//    }
//
//    /**
//     * Test trigger Uses Points,Condition Num Of Points GreaterThan,Action send lottery
//     */
//    @Test
//    public void testTriggerUsesPointsAndConditionNumOfPointsGreaterThanAndActionSendLottery() throws Exception{
//
//        serverHelper.saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);
//        Map props = new HashMap();
//        props.put(LocationBackendFields.PAY_WITH_BUDGET_TYPE.key(), PaymentType.POINTS.type());
//        props.put(LocationBackendFields.PURCHASE_ASSET_BUDGET_TYPE.key(),PaymentType.POINTS.type());
//        serverHelper.updateBusinessBackend(props);
//
//        PointsCondition pointsCondition = new PointsCondition();
//        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
//                setOperatorKey(IntOperators.IS_GREATER_THAN)
//                .setConditionValue("20");
//
//        //Create new rule
//        createRule(ActivityTrigger.UsePoints.toString(), Lists.newArrayList(pointsCondition));
//
//        //Create member
//        NewMember member = createNewMember();
//
//        String actionTag =MembersService.performSmartActionAddPoints("1000",timestamp);
//        //Verify the points were added
//        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);
//
//        String usedPoints = "3000";
//        // perform pay with budget to use members points
//        ServerHelper.performAnActionUsePoints(member, usedPoints);
//
//        //DataStore check
//        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());
//
//
//        log4j.info(smartGift.getTitle() + " was send to the member ");
//    }
//
//    /**
//     * Test trigger Tag member,Condition Num Of Points GreaterThan,Action send lottery
//     */
//    @Test
//    public void testTriggerTaggedAndConditionTagIsExactlyAndActionSendLottery() throws Exception {
//
//        tag = "lottery_" +timestamp;
//        TagCondition tagCondition = new TagCondition();
//        tagCondition.setOperatorKey(InputOperators.IS_EXACTLY)
//                .setConditionValue(tag);
//
//        //Create new rule
//        createRule(ActivityTrigger.Tagged.toString(), Lists.newArrayList(tagCondition));
//
//        //Create member
//        createNewMember();
//
//        //should activate the tag trigger
//        MembersService.performTagMemberSmartAction(tag, "TagMember");
//
//        Assert.assertTrue("The member did not get the tag sent, so the trigger did not started", MembersService.isTagExists(tag));
//        log4j.info("The member got the tag , so the trigger started");
//        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());
//    }

}
