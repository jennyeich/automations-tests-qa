package hub2_0.actions;


import com.google.common.collect.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.ActivityTrigger;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.UUID;

public class RegisterActionTest extends BaseActivityActionTest {

    ActivityAction activityAction;

    @Override
    protected ActivityAction getConcreteAction() throws Exception {
        if (activityAction == null)
             activityAction = ActionsBuilder.buildRegisterAction();
        return activityAction;
    }

    /**
     * Test trigger of make a purchase and  action of Register member.
     * in this test we first register new member and than unregister him. afterward he mage a purchase and we expect that he will be Register again due to the Action.
     * @throws Exception
     */
    @Test
    @Category({Hub2Regression.class})
    public void testTriggerMakesPurchaseAndActionRegisterMember() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickAllMembersBtn();

        createRule(ActivityTrigger.MadeAPurchase.toString(),Lists.newArrayList(globalCondition));
        NewMember member = createNewMember();
        automationsHelper.unregisterMember();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", Lists.newArrayList(), transactionId);
        checkAutomationRunAndRegisterMember();
    }


    @Test
    public void testTriggerMakesPurchaseWithConditionValueOfPurchseIsGreaterThanAndActionRegisterMember() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickAllMembersBtn();
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("10");
        createRule(ActivityTrigger.MadeAPurchase.toString(),Lists.newArrayList(globalCondition));
        NewMember newMember = createNewMember();
        automationsHelper.unregisterMember();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        checkAutomationRunAndRegisterMember();
    }


}
