package hub2_0.actions;

import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.operators.ShoppingCartOperators;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.SelectedItemsDialogButtons;
import hub.hub2_0.common.items.SelectItems;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.UUID;

public class ExportEventActionTest extends BaseActivityActionTest {

    ActivityAction activityAction;
    private static final String DESTINATION_URL = "https://services-dot-comoqa.appspot.com/echo";

    @Override
    protected ActivityAction getConcreteAction() throws Exception {
        timestamp = String.valueOf(System.currentTimeMillis());
        if (activityAction == null)
            activityAction = ActionsBuilder.buildExportEventAction("ExportAction_" + timestamp,DESTINATION_URL);
        return activityAction;
    }

    /**
     * Test trigger Join the club, no condition ,Action Export Event
     */
    @Test
    @Category(Hub2Regression.class)
    public void testTriggerJoinedTheClubAndActionExportEvent() throws Exception {


        //Create new rule
        createRule(ActivityTrigger.JoinTheClub.toString(), Lists.newArrayList());
        //Create member
        NewMember newMember = createNewMember();

        //DataStore check
        checkDataStoreWithEventExport(ActivityTrigger.JoinTheClub.toString().toLowerCase(),newMember.getPhoneNumber().toString(),DESTINATION_URL);

    }


    /**
     * Test trigger Join the club, no condition ,Action Export Event
     */
    @Test
    public void testTriggerMadeAPurchaseWithConditionShoppingCartContainsAndActionExportEvent() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_EXECTLY)
                .setConditionValue("1")
                .setSelectItems(selectItems);
        //Create new rule
        createRule(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList(madeAPurchaseCondition));
        //Create member
        NewMember newMember = createNewMember();

        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244","itemA","depA", "depAA", 10000);
        Item item2 = createItem("3000","itemA","depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000", new ArrayList<>(), transactionId, items);

        //DataStore check
        checkDataStoreWithEventExport("purchase",newMember.getPhoneNumber().toString(),DESTINATION_URL);

    }
}
