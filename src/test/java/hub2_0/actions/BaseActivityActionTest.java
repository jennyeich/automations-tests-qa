package hub2_0.actions;

import com.beust.jcommander.internal.Lists;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.PointsAccumulationAction;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.enums.ActivityStatus;
import hub.hub2_0.common.enums.ActivityType;
import hub.hub2_0.common.enums.SelectedItemsDialogButtons;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.smartAutomations.BaseAutomationTest;
import integration.base.BaseRuleHub2Test;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.List;

public abstract class BaseActivityActionTest extends BaseRuleHub2Test {

    protected static Box box;
    public static final Logger log4j = Logger.getLogger(BaseActivityActionTest.class);

    protected abstract ActivityAction getConcreteAction() throws Exception;

    @Override
    public ITrigger getTrigger() {
        //NO use in this implementation
        return null;
    }

    @Override
    public String getAutomationName() {
        //NO use in this implementation
        return null;
    }

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @AfterClass
    public static void tearDown() {
        BaseAutomationTest.tearDown();
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };


    @After
    public void cleanData() throws Exception {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }

    protected void createRule(String trigger, List<ApplyCondition> conditions) throws Exception {
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, trigger);
        Activity activity = ruleBuilder.buildActivity(trigger, box, rule, conditions, Lists.newArrayList(getConcreteAction()));

        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
    }

    protected void createRule(String trigger, List<ApplyCondition> conditions, List<ActivityAction> actions) throws Exception {
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, trigger);
        Activity activity = ruleBuilder.buildActivity(trigger, box, rule, conditions, actions);

        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        log4j.info("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
    }

    protected void createRuleWithCases(String trigger, List<ApplyCondition> conditions, List<Case> cases) throws Exception {
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, trigger);
        Activity activity = ruleBuilder.buildActivityWithCases(trigger, box, rule, conditions, cases);

        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
    }


    protected void createRulePerOccurrences(String trigger, List<ApplyCondition> conditions, List<OccurrencesCondition> OccurrenceCondition) throws Exception {
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, trigger);
        Activity activity = ruleBuilder.buildActivity(trigger, box, rule, conditions, OccurrenceCondition, Lists.newArrayList(getConcreteAction()));

        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
    }

    protected void createRulePerOccurrences(String trigger, List<ApplyCondition> conditions, List<OccurrencesCondition> OccurrenceCondition, List<ActivityAction> actions) throws Exception {
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, trigger);
        Activity activity = ruleBuilder.buildActivity(trigger, box, rule, conditions, OccurrenceCondition, actions);

        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
    }

    protected PointsAccumulationAction getAccumulationActionFix(String numOfPoints, PointsAccumulationAction.AccumulationUnits wallet) {
          PointsAccumulationAction pointsAccumulationAction = getAccumulationActionFix(numOfPoints);
          pointsAccumulationAction.setAccumulationUnits(wallet);
          return pointsAccumulationAction;
    }

/* this method support the case that wallet configured to be wallet/creadit instead of both */
    protected PointsAccumulationAction getAccumulationActionFix(String numOfPoints) {
        PointsAccumulationAction pointsAccumulationAction = getBasicPointsAccumulation(numOfPoints);
        pointsAccumulationAction.setAccumulationType(PointsAccumulationAction.AccumulationType.ACCUMULATE_FIXED_AMOUNT_OF_POINTS);
        return pointsAccumulationAction;
    }

    protected PointsAccumulationAction getAccumulationActionFixSingleOption(String numOfPoints, PointsAccumulationAction.AccumulationUnits wallet) {
        return getBasicPointsAccumulation(numOfPoints, wallet);
    }

    protected PointsAccumulationAction getAccumulationActionByPercentage(String percentage, PointsAccumulationAction.AccumulationUnits units, SelectItems selectItems, SelectItems excludeItems) {
        PointsAccumulationAction pointsAccumulationAction = getBasicPointsAccumulation(percentage, units);
        pointsAccumulationAction.clickSelectItemsButton();
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        pointsAccumulationAction.setSelectItems(selectItems);
        if (excludeItems != null) {
            excludeItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
            pointsAccumulationAction.setSelectItemsToExclude(excludeItems);
        }
        return pointsAccumulationAction;
    }

    private PointsAccumulationAction getBasicPointsAccumulation(String numOfPoints, PointsAccumulationAction.AccumulationUnits units) {
          PointsAccumulationAction pointsAccumulationAction = getBasicPointsAccumulation(numOfPoints);
          pointsAccumulationAction.setAccumulationUnits(units);
          return pointsAccumulationAction;
    }

    /* this method is used in case of setting wallet to Points instead of Both */
    private PointsAccumulationAction getBasicPointsAccumulation(String numOfPoints) {
        PointsAccumulationAction pointsAccumulationAction = new PointsAccumulationAction();
        pointsAccumulationAction.clickPointsAccumulationBtn();
        pointsAccumulationAction.setNumOfPoints(numOfPoints);
        return pointsAccumulationAction;
    }
}
