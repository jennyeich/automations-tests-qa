package hub2_0.actions.pointsAccumulations;


import hub.base.BaseHubTest;
import hub.smartAutomations.actions.BaseSmartActionTest;
import integration.base.BaseRuleHub2Test;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.IOException;


@RunWith(Suite.class)
@Suite.SuiteClasses({
        PointsAccumulationActionTest1.class,
        PointsAccumulationActionTest2.class
})
public class PointsAccumulationSuite {
    public static final Logger log4j = Logger.getLogger(PointsAccumulationSuite.class);

    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseRuleHub2Test.init();
    }

    @AfterClass
    public static void tearDown(){

        log4j.info("close browser after finish test");
        BaseSmartActionTest.cleanBase();
        BaseHubTest.cleanHub();
    }
}