package hub2_0.actions.pointsAccumulations;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Text;
import com.google.common.collect.Lists;
import common.BaseTest;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.operations.PointsCreditSettingsPage;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AmountType;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.common.actions.DelayAction;
import hub.hub2_0.common.actions.PointsAccumulationAction;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.ShoppingCartOperators;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.DelayUnits;
import hub.hub2_0.common.enums.SelectedItemsDialogButtons;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.AnonymousPurchaseTypes;
import server.common.ApiClientBuilder;
import server.common.ApiClientFields;
import server.utils.ServerHelper;
import server.v2_8.common.Item;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class PointsAccumulationActionTest1 extends BasePointsAccumulationTest {

    @BeforeClass
    public static void createNewBox() throws Exception {
        BaseTest.sqlDB.setHub2Wallet(resolveLocationId(locationId), PointsCreditSettingsPage.BusinessWallet.Both.dbValue());
        //Preconditions - Save anonymous purchase = Always and AllowAnalyzedPurchaseCancellation = Yes
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()),
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key(), true));

        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };


    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- accumulate by percentage with shoping cart condition and exclude items.")
    @Test
    @Category({Hub2Regression.class})
    public void testPointsAccumulateByPercentageWithShoppingCartCondition() throws Exception {
        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();

        SelectItems excludeItems = new SelectItems();
        excludeItems.clickItemCodesRadioButton();
        excludeItems.setItemCodes("1111", "5555 ");
        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionByPercentage("10", PointsAccumulationAction.AccumulationUnits.POINTS, selectItems, excludeItems);

        SelectItems selectItems1 = new SelectItems();
        selectItems1.clickDepartmentCodesRadioButton();
        selectItems1.setDepartmentCodes("depA");
        selectItems1.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition()
                .setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_AT_LEAST)
                .setConditionValue("1")
                .setSelectItems(selectItems1);
        createRule(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();

        Item itemA = createItem("1111","itemA","depA", "depAA", 10);
        Item itemB = createItem("2222","itemB","depA", "depAA", 20);
        submitPurchaseAndValidate(newMember, Lists.newArrayList(itemA), "2", AmountType.Points, false);
        submitPurchaseAndValidate(newMember, Lists.newArrayList(itemA, itemB), "2", AmountType.Points, true);
    }

    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- accumulate by percentage with item codes and delay.")
    @Test
    public void testPointsAccumulateAddPointsInDelayMode() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();

        SelectItems excludeItems = new SelectItems();
        excludeItems.clickItemCodesRadioButton();
        excludeItems.setItemCodes("1111", "5555 ");
        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionByPercentage("10", PointsAccumulationAction.AccumulationUnits.POINTS, selectItems, excludeItems);
        DelayAction delayAction = new DelayAction();
        delayAction.selectDelayType(DelayAction.AFTER).setAfterInput("2").setAfterUnits(DelayUnits.Minutes).clickFinish(DelayAction.SAVE);
        pointsAccumulationAction.setDelayAction(delayAction);
        createRule(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();

        Item itemA = createItem("1111","itemA","depA", "depAA", 10);
        Item itemB = createItem("2222","itemB","depA", "depAA", 20);

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "30",  Lists.newArrayList("DEP-1"), transactionId,  Lists.newArrayList(itemA, itemB));

        Thread.sleep(100000);
        checkDataStoreNegativeCheckByRuleID(automationRuleId);
        Thread.sleep(200000);
        //DataStore check
        checkDataStoreUpdatedWithPointsByRuleID("2", AmountType.Points);
    }

    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- accumulate by percentage with membership tag condition.")
    //Points accumulation based on member's tier + extra points on specific department
    @Test
    public void testPointsAccumulateByMemberTier() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();
        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionByPercentage("15", PointsAccumulationAction.AccumulationUnits.POINTS, selectItems, null);

        createRule(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList(globalCondition), Lists.newArrayList(pointsAccumulationAction));


        // Create rule that will accumulate points only 12% for item '1111'
        SelectItems selectItems2 = new SelectItems();
        selectItems2.clickItemCodesRadioButton();
        selectItems2.setItemCodes("1111");

        PointsAccumulationAction pointsAccumulationAction2 = getAccumulationActionByPercentage("12", PointsAccumulationAction.AccumulationUnits.POINTS, selectItems2, null);
        createRule(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(pointsAccumulationAction2));

        //Create member
        NewMember newMember = createNewMember();
        Item itemA = createItem("1111","itemA","depA", "depAA", 10000);
        Item itemB = createItem("2222","itemB","depA", "depAA", 20000);
        submitPurchaseAndValidate(newMember, Lists.newArrayList(itemA, itemB), "1200", AmountType.Points, true);

        performAnActionTagMember(newMember, "Gold");
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "30",  Lists.newArrayList("DEP-1"), transactionId, Lists.newArrayList(itemA, itemB));

        Thread.sleep(5000);
        List<Entity> res = ServerHelper.queryWithWaitUntilResultEqOrGreat("Must find at least 3 UserAction ",
                buildQueryWithSort(POINTSTRASACTION) , 3, dataStore, 120000);

        if(((String)res.get(0).getProperty("RuleID")).equals(automationRuleId)) {  // 12% on item 1111
            checkData(res, 0, 1200);
            checkData(res, 1, 4500);
        } else {  // 15% on item 2222
            checkData(res, 1, 1200);
            checkData(res, 0, 4500);
        }
    }

    private void checkData(List<Entity> res, int index, int pointsAmount) {
        Text t = ((Text)res.get(index).getProperty("Data"));
        log4j.info("Data contains the following data : " +   t.getValue());
        Assert.assertTrue("Data must contain the Points : " + pointsAmount, t.getValue().contains("Points=\"" + pointsAmount + "\" "));

    }


    @Test
    @Category({Hub2Regression.class})
    public void testPointsAccumulateFixSpecificItemsWithPerOccurrencesMode() throws Exception {

        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionFixSingleOption("15", PointsAccumulationAction.AccumulationUnits.POINTS);
        SelectItems selectItems = new SelectItems();
        selectItems.clickDepartmentCodesRadioButton();
        selectItems.setDepartmentCodes("depA", "depB");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItems);
        occurrencesCondition.setConditionValue("2");
        createRulePerOccurrences(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(occurrencesCondition), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();

        Item itemA = createItem("898","itemA","depA", "depAA", 23);
        Item itemB = createItem("333","itemB","depB", "depBB", 17);
        submitPurchaseAndValidate(newMember, Lists.newArrayList(itemA, itemB), "1500", AmountType.Points, true);
    }




    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }
}
