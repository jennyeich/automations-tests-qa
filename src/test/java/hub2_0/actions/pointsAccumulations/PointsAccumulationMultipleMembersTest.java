package hub2_0.actions.pointsAccumulations;

import com.google.common.collect.Lists;
import common.BaseTest;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.operations.PointsCreditSettingsPage;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.cases.RuleCase;
import hub.hub2_0.common.actions.PointsAccumulationAction;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.cases.MembersCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.conditions.operators.ShoppingCartOperators;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.SelectedItemsDialogButtons;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.hub2_0.common.builders.CasesConditionsBuilder;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.AnonymousPurchaseTypes;
import server.common.ApiClientBuilder;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.v2_8.JoinClubResponse;
import server.v2_8.common.Item;
import server.v4.GetMemberDetailsResponse;
import server.v4.SubmitPurchase;
import server.v4.SubmitPurchaseResponse;
import server.v4.common.models.Customer;
import server.v4.common.models.MemberDetailsQueryParams;
import server.v4.common.models.Purchase;
import server.v4.common.models.PurchaseItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PointsAccumulationMultipleMembersTest extends BasePointsAccumulationTest {

    private static final String ITEM_CODE_1111 = "1111";
    private static final String ITEM_CODE_2222 = "2222";
    private static final String ITEM_CODE_3333 = "3333";

    public static final String VIP = "VIP";
    public static final String GOLD = "GOLD";
    public static final String ACTIVE = "Active";
    public static final String PAY_PAL = "PayPal";

    @BeforeClass
    public static void createNewBox() throws Exception {
        BaseTest.sqlDB.setHub2Wallet(resolveLocationId(locationId), PointsCreditSettingsPage.BusinessWallet.Both.dbValue());
        //Preconditions - Save anonymous purchase = Always and AllowAnalyzedPurchaseCancellation = Yes
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()),
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key(), true));

        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.MadeAPurchase;
    }


    @ru.yandex.qatools.allure.annotations.Description("C12292: Set accumulation of 10% on entire purchase (any item) with exclude item 2222")
    @Test
    @Category({Hub2Regression.class})
    public void testAccumulationOf10PrecentOnEntirePurchaseWithExcludeItem() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();

        SelectItems excludeItems = new SelectItems();
        excludeItems.clickItemCodesRadioButton();
        excludeItems.setItemCodes(ITEM_CODE_2222);
        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionByPercentage("10", PointsAccumulationAction.AccumulationUnits.POINTS, selectItems, excludeItems);

        SelectItems selectItems1 = new SelectItems();
        selectItems1.clickDepartmentCodesRadioButton();
        selectItems1.setDepartmentCodes("depA");
        selectItems1.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition()
                .setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_AT_LEAST)
                .setConditionValue("1")
                .setSelectItems(selectItems1);
        createRule(getTrigger().toString(), Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        Customer member1 = createMember();
        Customer member2 = createMember();
        Customer member3 = createMember();

        Map<Customer,Integer> customersPoints = new HashMap<>();//hash map that holds for each customer phone its num of points to receive
        customersPoints.put(member1,100);
        customersPoints.put(member2,100);
        customersPoints.put(member3,100);

        PurchaseItem itemA = v4Builder.buildPurchaseItem(ITEM_CODE_1111,1,3000,"depA", "depAA");
        PurchaseItem itemB =  v4Builder.buildPurchaseItem(ITEM_CODE_2222,1,2000,"depA", "depAA");

        submitPurchaseAndValidate(Lists.newArrayList(member1,member2,member3), Lists.newArrayList(itemA, itemB),5000, customersPoints);
    }

    @ru.yandex.qatools.allure.annotations.Description("C12303: Set global condition for specific members (VIP) with accumulation of 20%")
    @Test
    @Category({Hub2Regression.class})
    public void testGlobalConditionSpecificMembers_VIP_WithAccumulation20Precent() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString()).setOperatorKey(InputOperators.IS_ONE_OF).setConditionValue(VIP);
        globalCondition.setSpecificMembers(SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition));

        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();
        PointsAccumulationAction pointsAccumulationActionVIP = getAccumulationActionByPercentage("20", PointsAccumulationAction.AccumulationUnits.POINTS, selectItems,null);

        createRule(getTrigger().toString(), Lists.newArrayList(globalCondition), Lists.newArrayList(pointsAccumulationActionVIP));

        //Create member
        JoinClubResponse memberVIP = joinClub();
        JoinClubResponse member2 = joinClub();


        Map<Customer,Integer> customersPoints = new HashMap<>();//hash map that holds for each customer phone its num of points to receive
        Customer customerVIP = new Customer(memberVIP.getPhoneNumber());
        Customer customer2 = new Customer(member2.getPhoneNumber());
        customersPoints.put(customerVIP,1000);
        customersPoints.put(customer2,0);

        serverHelper.tagMember(VIP,memberVIP.getMembershipKey(),memberVIP.getUserKey());

        PurchaseItem itemA = v4Builder.buildPurchaseItem(ITEM_CODE_1111,1,5000,"depA", "depAA");
        PurchaseItem itemB =  v4Builder.buildPurchaseItem(ITEM_CODE_2222,1,5000,"depA", "depAA");

        submitPurchaseAndValidate(Lists.newArrayList(customerVIP,customer2), Lists.newArrayList(itemA, itemB),10000, customersPoints);
    }

    @ru.yandex.qatools.allure.annotations.Description("C12306: Set a few cases with 'Members identified' condition - with different accumulation for each case" +
            "1. members accumulate 10% of the purchase (Membership tag: 'Active') " +
            "2. members accumulate 20% of the purchase (Membership tag: 'VIP') " +
            "3. members accumulate 30% of the purchase (Membership tag: 'GOLD') " +
            "4. don't accumulate on - members that paying with PayPal (Purchase tag: 'PayPal')" +
            "5. on item 1111 set 50%")
    @Test
    public void testDifferentAccumulationsOnMembersWithCases() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString()).setOperatorKey(InputOperators.IS_NOT_ONE_OF).setConditionValue(PAY_PAL);
        globalCondition.setSpecificMembers(SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition));

        PointsAccumulationAction pointsAccumulationActionActive = getAccumulationActionByPercentage("10", PointsAccumulationAction.AccumulationUnits.POINTS, buildDefaultSelectedItems(),null);
        PointsAccumulationAction pointsAccumulationActionVIP = getAccumulationActionByPercentage("20", PointsAccumulationAction.AccumulationUnits.POINTS, buildDefaultSelectedItems(),null);
        PointsAccumulationAction pointsAccumulationActionGOLD = getAccumulationActionByPercentage("30", PointsAccumulationAction.AccumulationUnits.POINTS, buildDefaultSelectedItems(),null);


        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        selectItems.setItemCodes(ITEM_CODE_1111);
        PointsAccumulationAction pointsAccumulationActionItemCode1111 = getAccumulationActionByPercentage("5", PointsAccumulationAction.AccumulationUnits.POINTS, selectItems,null);


        /****case ACTIVE****/
        MembersCondition membersConditionActive = CasesConditionsBuilder.buildMembersCondition(MemberNonRegistrationFormFields.Tags, InputOperators.IS_ONE_OF, ACTIVE);
        RuleCase aCase1 = new RuleCase();
        aCase1.setDescription("case Active");
        aCase1.setConditions(Lists.newArrayList(membersConditionActive));
        aCase1.setActions(Lists.newArrayList(pointsAccumulationActionActive));


        /****case VIP****/
        MembersCondition membersConditionVIP = CasesConditionsBuilder.buildMembersCondition(MemberNonRegistrationFormFields.Tags, InputOperators.IS_ONE_OF, VIP);
        RuleCase aCase2 = new RuleCase();
        aCase2.setDescription("case VIP");
        aCase2.setConditions(Lists.newArrayList(membersConditionVIP));
        aCase2.setActions(Lists.newArrayList(pointsAccumulationActionVIP));


        /****case GOLD****/
        MembersCondition membersConditionGOLD = CasesConditionsBuilder.buildMembersCondition(MemberNonRegistrationFormFields.Tags, InputOperators.IS_ONE_OF, GOLD);
        RuleCase aCase3 = new RuleCase();
        aCase3.setDescription("case GOLD");
        aCase3.setConditions(Lists.newArrayList(membersConditionGOLD));
        aCase3.setActions(Lists.newArrayList(pointsAccumulationActionGOLD));


        /****case itemCode1111****/
        MembersCondition membersCondition = new MembersCondition();
        membersCondition.clickDelete();
        RuleCase aCase4 = new RuleCase();
        aCase4.setDescription("case Item1111");
        aCase4.setConditions(Lists.newArrayList(membersCondition));
        aCase4.setActions(Lists.newArrayList(pointsAccumulationActionItemCode1111));

        //create rule with 5 cases
        createRuleWithCases(getTrigger().toString(), Lists.newArrayList(globalCondition), Lists.newArrayList(aCase1,aCase2,aCase3,aCase4));

        //Create member
        JoinClubResponse memberActive = joinClub();
        JoinClubResponse memberVIP = joinClub();
        JoinClubResponse memberGold = joinClub();
        JoinClubResponse memberPaypal = joinClub();
        JoinClubResponse member = joinClub();


        Map<Customer,Integer> customersPoints = new HashMap<>();//hash map that holds for each customer phone its num of points to receive
        Customer customerActive = new Customer(memberActive.getPhoneNumber());
        Customer customerVIP = new Customer(memberVIP.getPhoneNumber());
        Customer customerGold = new Customer(memberGold.getPhoneNumber());
        Customer customerPaypal = new Customer(memberPaypal.getPhoneNumber());
        Customer customer = new Customer(member.getPhoneNumber());
        customersPoints.put(customerActive,200);
        customersPoints.put(customerVIP,400);
        customersPoints.put(customerGold,600);
        customersPoints.put(customerPaypal,0);
        customersPoints.put(customer,40);

        serverHelper.tagMember(ACTIVE,memberActive.getMembershipKey(),memberActive.getUserKey());
        serverHelper.tagMember(VIP,memberVIP.getMembershipKey(),memberVIP.getUserKey());
        serverHelper.tagMember(GOLD,memberGold.getMembershipKey(),memberGold.getUserKey());
        serverHelper.tagMember(PAY_PAL,memberPaypal.getMembershipKey(),memberPaypal.getUserKey());

        PurchaseItem itemA = v4Builder.buildPurchaseItem(ITEM_CODE_1111,1,4000,"depA", "depAA");
        PurchaseItem itemB =  v4Builder.buildPurchaseItem(ITEM_CODE_2222,1,6000,"depA", "depAA");

        submitPurchaseAndValidate(Lists.newArrayList(customerActive,customerVIP,customerGold,customerPaypal,customer), Lists.newArrayList(itemA, itemB),10000, customersPoints);
    }

    @ru.yandex.qatools.allure.annotations.Description("C12296: Set accumulation by rate - add 1 point for every 1$ on specific items separated by 'or' (with 4 items)")
    @Test
    public void testAccumulationByRateAdd1PointsForEvery1$OnSpecificItemsSeperatedByOr() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        selectItems.setItemCodes(ITEM_CODE_1111,ITEM_CODE_3333);

        PointsAccumulationAction pointsAccumulationAction = ruleBuilder.getAccumulationActionByRate("1","1", PointsAccumulationAction.AccumulationUnits.POINTS, selectItems, null);
        createRule(getTrigger().toString(), Lists.newArrayList(), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        Customer member1 = createMember();
        Customer member2 = createMember();
        Customer member3 = createMember();

        Map<Customer,Integer> customersPoints = new HashMap<>();//hash map that holds for each customer phone its num of points to receive
        //every member should have 30.00 points(3000) - accumulate by rate - for each item from selectedItems code (item1111,item3333) - accumulate 1 point for each 1$ spent
        customersPoints.put(member1,3000);
        customersPoints.put(member2,3000);
        customersPoints.put(member3,3000);


        PurchaseItem itemA = v4Builder.buildPurchaseItem(ITEM_CODE_1111,1,3000,"depA", "depAA");
        PurchaseItem itemB =  v4Builder.buildPurchaseItem(ITEM_CODE_2222,1,1000,"depA", "depAA");
        PurchaseItem itemC =  v4Builder.buildPurchaseItem(ITEM_CODE_3333,1,6000,"depA", "depAA");

        submitPurchaseAndValidate(Lists.newArrayList(member1,member2,member3), Lists.newArrayList(itemA, itemB, itemC),10000, customersPoints);
    }

    @ru.yandex.qatools.allure.annotations.Description("C12300:Set accumulation by rate - add 1 point for every 1$ don't accumulate on specific members (member tag) and specific items")
    @Test
    public void testAccumulationByRateAdd1PointsForEvery1$AndDontAccumulateOnSpecificMembersAndSpecificItems() throws Exception {


        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString()).setOperatorKey(InputOperators.IS_NOT_ONE_OF).setConditionValue(PAY_PAL);
        globalCondition.setSpecificMembers(SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition));

        SelectItems excludedItems = new SelectItems();
        excludedItems.clickItemCodesRadioButton();
        excludedItems.setItemCodes(ITEM_CODE_1111,ITEM_CODE_3333);
        PointsAccumulationAction pointsAccumulationAction = ruleBuilder.getAccumulationActionByRate("1","1", PointsAccumulationAction.AccumulationUnits.POINTS, buildDefaultSelectedItems(), excludedItems);

        createRule(getTrigger().toString(), Lists.newArrayList(globalCondition), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        JoinClubResponse member1 = joinClub();
        JoinClubResponse member2 = joinClub();
        JoinClubResponse memberPaypal = joinClub();


        Map<Customer,Integer> customersPoints = new HashMap<>();//hash map that holds for each customer phone its num of points to receive
        Customer customer1 = new Customer(member1.getPhoneNumber());
        Customer customer2 = new Customer(member2.getPhoneNumber());
        Customer customerPaypal = new Customer(memberPaypal.getPhoneNumber());
        customersPoints.put(customer1,1000);
        customersPoints.put(customer2,1000);
        customersPoints.put(customerPaypal,0);

        serverHelper.tagMember(PAY_PAL,memberPaypal.getMembershipKey(),memberPaypal.getUserKey());

        PurchaseItem itemA = v4Builder.buildPurchaseItem(ITEM_CODE_1111,1,1000,"depA", "depAA");
        PurchaseItem itemB =  v4Builder.buildPurchaseItem(ITEM_CODE_2222,1,3000,"depA", "depAA");
        PurchaseItem itemC =  v4Builder.buildPurchaseItem(ITEM_CODE_3333,1,2000,"depA", "depAA");

        submitPurchaseAndValidate(Lists.newArrayList(customer1,customer2,customerPaypal), Lists.newArrayList(itemA, itemB, itemC),6000, customersPoints);
    }

    @ru.yandex.qatools.allure.annotations.Description("C12304: Set global condition total spend greater than 1000 with accumulation of fixed amount 5 points")
    @Test
    public void testWithGlobalConditionOfTotalSpendGreaterThanWithAccumulationOfFixedAmount() throws Exception {


        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("10");

        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionFix("5", PointsAccumulationAction.AccumulationUnits.POINTS);

        createRule(getTrigger().toString(), Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        Customer member1 = createMember();
        Customer member2 = createMember();


        Map<Customer,Integer> customersPoints = new HashMap<>();//hash map that holds for each customer phone its num of points to receive
        customersPoints.put(member1,0);
        customersPoints.put(member2,0);

        PurchaseItem itemA = v4Builder.buildPurchaseItem(ITEM_CODE_1111,1,40,"depA", "depAA");
        PurchaseItem itemB =  v4Builder.buildPurchaseItem(ITEM_CODE_2222,1,20,"depA", "depAA");

        //negative test - total sum not as condition required - no points accumulated
        submitPurchaseAndValidate(Lists.newArrayList(member1,member2), Lists.newArrayList(itemA, itemB),60, customersPoints);

        customersPoints.put(member1,250);
        customersPoints.put(member2,250);

        //positive test - total sum is as condition required - each member will get 2.5 points (5 points devided to 2 members)
        itemA = v4Builder.buildPurchaseItem(ITEM_CODE_1111,1,6000,"depA", "depAA");
        submitPurchaseAndValidate(Lists.newArrayList(member1,member2), Lists.newArrayList(itemA, itemB),6000, customersPoints);

    }

    @ru.yandex.qatools.allure.annotations.Description("test Accumulation Of FixedAmount Not Performed No Membership")
    @Test
    public void testAccumulationOfFixedAmountNotPerformedNoMembership() throws Exception {

        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionFix("5", PointsAccumulationAction.AccumulationUnits.POINTS);

        createRule(getTrigger().toString(), Lists.newArrayList(), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        Customer memberRegistered = createMember();
        Customer customerNotRegistered = new Customer("05255554854");


        Map<Customer,Integer> customersPoints = new HashMap<>();//hash map that holds for each customer phone its num of points to receive
        customersPoints.put(memberRegistered,0);

        PurchaseItem itemA = v4Builder.buildPurchaseItem(ITEM_CODE_1111,1,4000,"depA", "depAA");

        //total sum as condition required - no points accumulated since one of customers is not registered
        submitPurchaseAndValidate(Lists.newArrayList(memberRegistered,customerNotRegistered), Lists.newArrayList(itemA),4000, customersPoints);
    }

    @ru.yandex.qatools.allure.annotations.Description("test accumulation of Fixed Amount performed for both members - with and without consent,but in grace period")
    @Test
    @Category({Hub2Regression.class})
    public void testAccumulationOfFixedAmountNotPerformedNoConsentGracePeriod() throws Exception {

        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionByPercentage("15", PointsAccumulationAction.AccumulationUnits.POINTS,buildDefaultSelectedItems(),null);

        createRule(getTrigger().toString(), Lists.newArrayList(), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        JoinClubResponse newMember = serverHelper.createMemberWithoutConsent();
        Customer memberRegisteredNoConsent = new Customer(newMember.getPhoneNumber());
        Customer memberRegisteredWithConsent = createMember();


        Map<Customer,Integer> customersPoints = new HashMap<>();//hash map that holds for each customer phone its num of points to receive
        customersPoints.put(memberRegisteredNoConsent,300);
        customersPoints.put(memberRegisteredWithConsent,300);
        PurchaseItem itemA = v4Builder.buildPurchaseItem(ITEM_CODE_1111,1,4000,"depA", "depAA");

        //total sum as condition required - no points accumulated since one of customers has no consent
        submitPurchaseAndValidate(Lists.newArrayList(memberRegisteredNoConsent,memberRegisteredWithConsent), Lists.newArrayList(itemA),4000, customersPoints);
    }

    @ru.yandex.qatools.allure.annotations.Description("test accumulation of Fixed Amount performed for both members - with and without consent")
    @Test
    public void testAccumulationOfFixedAmountNotPerformedNoConsent() throws Exception {

        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionByPercentage("15", PointsAccumulationAction.AccumulationUnits.POINTS,buildDefaultSelectedItems(),null);

        createRule(getTrigger().toString(), Lists.newArrayList(), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        JoinClubResponse newMember = serverHelper.createMemberWithoutConsent();
        Customer memberRegisteredNoConsent = new Customer(newMember.getPhoneNumber());
        Customer memberRegisteredWithConsent = createMember();

        updateDateForMember(memberRegisteredNoConsent.getPhoneNumber(),hubAssetsCreator.date1WeekBack());

        Map<Customer,Integer> customersPoints = new HashMap<>();//hash map that holds for each customer phone its num of points to receive
        customersPoints.put(memberRegisteredNoConsent,0);
        customersPoints.put(memberRegisteredWithConsent,0);
        PurchaseItem itemA = v4Builder.buildPurchaseItem(ITEM_CODE_1111,1,4000,"depA", "depAA");

        //total sum as condition required - no points accumulated since one of customers has no consent
        submitPurchaseAndValidate(Lists.newArrayList(memberRegisteredNoConsent,memberRegisteredWithConsent), Lists.newArrayList(itemA),4000, customersPoints);
    }

    @Test
    public void testPointsAccumulateMultipleMembersByPercentageSubmitPurchase2_8() throws Exception {

        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionByPercentage("15", PointsAccumulationAction.AccumulationUnits.POINTS,buildDefaultSelectedItems(),null);

        createRule(getTrigger().toString(), Lists.newArrayList(), Lists.newArrayList(pointsAccumulationAction));
        //Create member
        NewMember newMember1 = createNewMember();
        timestamp = String.valueOf(System.currentTimeMillis());
        NewMember newMember2 = createNewMember();
        timestamp = String.valueOf(System.currentTimeMillis());
        NewMember newMember3 = createNewMember();

        Item itemA = createItem(ITEM_CODE_1111,"itemA","depA", "depAA", 2000);

        String transactionId = UUID.randomUUID().toString();
       performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember1,newMember2,newMember3), "2000",  Lists.newArrayList("DEP-1"), transactionId, Lists.newArrayList(itemA));

        Map<Customer,Integer> customersPoints = new HashMap<>();//hash map that holds for each customer phone its num of points to receive
        customersPoints.put(new Customer(newMember1.getPhoneNumber()),100);
        customersPoints.put(new Customer(newMember2.getPhoneNumber()),100);
        customersPoints.put(new Customer(newMember3.getPhoneNumber()),100);
        checkPointsForMembers(customersPoints);
    }

    private SelectItems buildDefaultSelectedItems() {
        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();
        return selectItems;
    }

    private void submitPurchaseAndValidate(ArrayList<Customer> customers, ArrayList<PurchaseItem> items,int totalAmount , Map<Customer,Integer> customersPoints) throws Exception{

        submitPurchase(customers, items, totalAmount);

        checkPointsForMembers(customersPoints);

    }

    private void submitPurchase(ArrayList<Customer> customers, ArrayList<PurchaseItem> items, int totalAmount) {
        String transactionId = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(transactionId,totalAmount, items) ;
        SubmitPurchase submitPurchase = v4Builder.buildSubmitPurchase(customers,purchase);

        IServerResponse response =  submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);

        if(!(((SubmitPurchaseResponse) response).getStatus().equals("ok")))
            Assert.fail(getError(response).get(MESSAGE).toString());

        SubmitPurchaseResponse purchaseResponse =(SubmitPurchaseResponse)response;
        //Verify the returned customer is as expected
        Assert.assertNotNull("Verify the confirmation is not null",purchaseResponse.getConfirmation());
    }

    private void checkPointsForMembers(Map<Customer, Integer> customersPoints) throws Exception {

        for (Customer customer:customersPoints.keySet()) {

            int expectedPoints = customersPoints.get(customer);
            GetMemberDetailsResponse getMemberDetailsResponse = getMemberDetails(customer);
            int actualPoints = getMemberDetailsResponse.getMembership().getPointsBalance().getBalance().getMonetary();
            Assert.assertEquals("Points are not as expected",expectedPoints,actualPoints);
        }
    }

    private GetMemberDetailsResponse getMemberDetails (Customer customer)throws Exception{

        Thread.sleep(5000);
        MemberDetailsQueryParams queryParams =  new MemberDetailsQueryParams();
        IServerResponse response = v4Builder.getMemberDetailsWithQueryParamsResponse(customer,queryParams);
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) response;
        if(!getMemberDetailsResponse.getStatus().equals("ok")) {
            log4j.info(getError(response).get(MESSAGE).toString());
        }
        return getMemberDetailsResponse;
    }

    private Customer createMember() throws Exception{
        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        return customer;
    }

    private JoinClubResponse joinClub() throws Exception{
        //Create new customer
        JoinClubResponse newMember = serverHelper.createMember();
        Assert.assertNotNull(newMember);
        Assert.assertNotNull(newMember.getMembershipKey());
        return newMember;
    }

    protected JSONObject getError(IServerResponse response){
        return ((JSONObject)((JSONArray)(response.getRawObject().get(ERRORS))).get(0));
    }

    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }
}
