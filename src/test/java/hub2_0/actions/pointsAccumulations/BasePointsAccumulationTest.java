package hub2_0.actions.pointsAccumulations;

import com.google.common.collect.Lists;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AmountType;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub2_0.actions.BaseActivityActionTest;
import server.v2_8.SubmitPurchaseResponse;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.UUID;

public class BasePointsAccumulationTest extends BaseActivityActionTest {

    @Override
    protected ActivityAction getConcreteAction() throws Exception {
        return ActionsBuilder.buildPointsAccumulationAction("10");
    }

    protected SubmitPurchaseResponse submitPurchaseAndValidate(NewMember newMember, ArrayList<Item> items, String pointsAmount, AmountType amountType, boolean isPointsAccumulated) throws Exception{
        String transactionId = UUID.randomUUID().toString();
        SubmitPurchaseResponse submitPurchaseResponse = performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "30",  Lists.newArrayList("DEP-1"), transactionId, items);

        Thread.sleep(5000);
        if(isPointsAccumulated)
            checkDataStoreUpdatedWithPointsByRuleID(pointsAmount, amountType);
        else
            checkDataStoreNegativeCheckByRuleID(POINTSTRASACTION);

        return submitPurchaseResponse;
    }

}
