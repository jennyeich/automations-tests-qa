package hub2_0.actions.pointsAccumulations;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Text;
import com.google.common.collect.Lists;
import common.BaseTest;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.common.Gender;
import hub.common.objects.common.RegistrationField;
import hub.common.objects.member.NewMember;
import hub.common.objects.operations.PointsCreditSettingsPage;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AmountType;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.common.actions.PointsAccumulationAction;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.AdvancedRuleConditionKeys;
import hub.hub2_0.common.enums.SelectedItemsDialogButtons;
import hub.hub2_0.common.items.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub.utils.HubMemberCreator;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.common.AnonymousPurchaseTypes;
import server.common.ApiClientBuilder;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.utils.ServerHelper;
import server.v2_8.CancelPurchase;
import server.v2_8.CancelPurchaseResponse;
import server.v2_8.SubmitPurchaseResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class PointsAccumulationActionTest2 extends BasePointsAccumulationTest {

    static ArrayList items;

    @BeforeClass
    public static void createNewBox() throws Exception {

        BaseTest.sqlDB.setHub2Wallet(resolveLocationId(locationId), PointsCreditSettingsPage.BusinessWallet.Both.dbValue());
        //Preconditions - Save anonymous purchase = Always and AllowAnalyzedPurchaseCancellation = Yes
        serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.SAVE_ANONYMOUS_PURCHASE_ON.key(), AnonymousPurchaseTypes.ALWAYS.type()),
                ApiClientBuilder.newPair(ApiClientFields.ALLOW_ANALYZED_PURCHASE_CANCELLATION.key(), true));

        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);

        items = new ArrayList();
        items.add(createItem("111", -1, 1.0, -2000, null));
        items.add(createItem("111", 1, 1.0, 2000, "A1"));
        items.add(createItem("222", 5, 1.0, 5000, null));
        items.add(createItem("333", 2, 1.0, 1000, null));
        items.add(createItem("333", 4, 1.0, 4000, null));
        items.add(createItem("444", 1, 1.0, -1500, "A1"));
        items.add(createItem("555", -1, 1.0, 2000, null));
        items.add(createItem("666", -2, 1.0, -1000, null));
        items.add(createItem("777", 1, 1.0, 1234, null));
        items.add(createItem("888", 1, 1.0, 50, "A1"));


    }



    @ru.yandex.qatools.allure.annotations.Description("Test Points accumulation with Join club trigger- fix accumulation for female members only.")
    @Test
    public void testPointsAccumulateJoinClubWithSpecificMember() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(RegistrationField.Gender.toString()).setConditionSelectValue("female");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers()
                .setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);
        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionFixSingleOption("200", PointsAccumulationAction.AccumulationUnits.POINTS);
        createRule(ActivityTrigger.JoinTheClub.toString(), Lists.newArrayList(globalCondition), Lists.newArrayList(pointsAccumulationAction));

        //Create male member
        NewMember maleMember = HubMemberCreator.buildMember(timestamp, "maleUser_", "maleUser_");
        maleMember.setGender(Gender.MALE.toString());
        MembersService.createNewMember(maleMember);
        checkDataStoreNegativeCheckByRuleID(automationRuleId);

        //Create female member
        timestamp = String.valueOf(System.currentTimeMillis());
        createNewMember();
        checkDataStoreUpdatedWithPointsByRuleID("20000", AmountType.Points);
    }

    @ru.yandex.qatools.allure.annotations.Description("Test Points accumulation- per occurrences with fix accumulation and no matching items.")
    @Test
    public void testPointsAccumulateFixPerOccurrencesModeNoMatch() throws Exception {

        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionFixSingleOption("50", PointsAccumulationAction.AccumulationUnits.POINTS);
        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        selectItems.setItemCodes("9832475");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItems);
        createRulePerOccurrences(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(occurrencesCondition), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchaseAndValidate(newMember,items, "0", AmountType.Points, false);
        cancelPurchaseAndValidate(submitPurchaseResponse.getConfirmation(), "0", AmountType.Points, false);
    }

    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- per occurrences with fix accumulation and some matching items, but sum of the items is zero.")
    @Test
    public void testPointsAccumulateFixPerOccurrencesModePartialMatch() throws Exception {

        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionFixSingleOption("50", PointsAccumulationAction.AccumulationUnits.POINTS);
        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        selectItems.setItemCodes("111");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItems);
        createRulePerOccurrences(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(occurrencesCondition), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchaseAndValidate(newMember,items, "0", AmountType.Points, false);
        cancelPurchaseAndValidate(submitPurchaseResponse.getConfirmation(), "0", AmountType.Points, false);
    }

    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- per occurrences with fix accumulation and.")
    @Test
    public void testPointsAccumulateFixPerOccurrencesModePartialQuantity() throws Exception {

        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionFixSingleOption("50", PointsAccumulationAction.AccumulationUnits.POINTS);
        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        selectItems.setItemCodes("222");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItems);
        occurrencesCondition.setConditionValue("2");
        createRulePerOccurrences(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(occurrencesCondition), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchaseAndValidate(newMember,items, "10000", AmountType.Points, true);
        cancelPurchaseAndValidate(submitPurchaseResponse.getConfirmation(), "-10000", AmountType.Points, true);
    }


    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- per occurrences with fix accumulation and spend = 1.00.")
    @Test
    public void testPointsAccumulateFixPerOccurrencesModeAllItemSpend() throws Exception {

        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionFixSingleOption("1", PointsAccumulationAction.AccumulationUnits.CREDIT);
        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItems);
        occurrencesCondition.setConditionKey(OccurrencesCondition.Condition.IN_THE_TOTAL_SPEND_OF);
        occurrencesCondition.setConditionValue("1.00");
        createRulePerOccurrences(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(occurrencesCondition), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchaseAndValidate(newMember,items, "10700", AmountType.Credit, true);
        cancelPurchaseAndValidate(submitPurchaseResponse.getConfirmation(), "-10700", AmountType.Credit, true);
    }


    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- simple mode with fix accumulation.")
    @Test
    public void testPointsAccumulateByFixSimpleMode() throws Exception {

        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionFix("100", PointsAccumulationAction.AccumulationUnits.CREDIT);
        createRule(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();

        SubmitPurchaseResponse submitPurchaseResponse = submitPurchaseAndValidate(newMember,items, "10000", AmountType.Credit, true);
        cancelPurchaseAndValidate(submitPurchaseResponse.getConfirmation(), "10000", AmountType.Credit, true);
    }


    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- accumulate by percentage with no matching items.")
    @Test
    @Category({Hub2Regression.class})
    public void testPointsAccumulateByPercentageWithNoMatchingItems() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        selectItems.setItemCodes("9832745");
        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionByPercentage("10", PointsAccumulationAction.AccumulationUnits.POINTS, selectItems, null);
        createRule(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchaseAndValidate(newMember,items, "0", AmountType.Points, false);
        cancelPurchaseAndValidate(submitPurchaseResponse.getConfirmation(), "0", AmountType.Points, false);
    }


    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- accumulate by percentage- 120% accumulation with predefined group.")
    @Test
    public void testPointsAccumulateByPercentageOver100() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        CodesGroup itemCodes = new CodesGroup();
        itemCodes.setCodes(CodesGroup.ITEM_CODES_GROUP,"333");
        String groupName = "group_" + timestamp;
        itemCodes.clickSaveAsGroupBtn(groupName, NewGroup.SAVE);
        selectItems.setItemCodes(itemCodes);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionByPercentage("120", PointsAccumulationAction.AccumulationUnits.POINTS, selectItems, null);
        createRule(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchaseAndValidate(newMember,items, "6000", AmountType.Points, true);
        cancelPurchaseAndValidate(submitPurchaseResponse.getConfirmation(), "-6000", AmountType.Points, true);
    }

    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- accumulate by percentage with excluded item.")
    @Test
    public void testPointsAccumulateWithExclude() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();
        SelectItems excludeItems = new SelectItems();
        excludeItems.clickItemCodesRadioButton();
        excludeItems.setItemCodes("666");
        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionByPercentage("10", PointsAccumulationAction.AccumulationUnits.CREDIT, selectItems, excludeItems);
        createRule(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchaseAndValidate(newMember,items, "1178", AmountType.Credit, true);
        cancelPurchaseAndValidate(submitPurchaseResponse.getConfirmation(), "-1178", AmountType.Credit, true);
    }


    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- accumulate by percentage- with small number of accumulation.")
    @Test
    public void testPointsAccumulateByPercentageSmallAccumulation() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        selectItems.setItemCodes("777");
        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionByPercentage("17", PointsAccumulationAction.AccumulationUnits.POINTS, selectItems, null);
        createRule(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchaseAndValidate(newMember,items, "210", AmountType.Points, true);
        cancelPurchaseAndValidate(submitPurchaseResponse.getConfirmation(), "-210", AmountType.Points, true);
    }

    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- accumulate by rate with predefined group- items with codes 333, 444, 999.")
    @Test
    public void testPointsAccumulateByRateExcludePredefinedGroup() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        selectItems.setItemCodes("333", "444", "999");

        SelectItems excludeItems = new SelectItems();
        excludeItems.clickItemCodesRadioButton();
        CodesGroup itemCodes1 = new CodesGroup();
        itemCodes1.setCodes(CodesGroup.ITEM_CODES_GROUP,"333", "444");
        String groupName = "group_" + timestamp;
        itemCodes1.clickSaveAsGroupBtn(groupName, NewGroup.SAVE);

        excludeItems.setItemCodes(itemCodes1);

        PointsAccumulationAction pointsAccumulationAction = ruleBuilder.getAccumulationActionByRate("10","1", PointsAccumulationAction.AccumulationUnits.POINTS, selectItems, excludeItems);
        createRule(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchaseAndValidate(newMember,items, "0", AmountType.Points, false);
        cancelPurchaseAndValidate(submitPurchaseResponse.getConfirmation(), "0", AmountType.Points, false);
    }


    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- accumulate by rate (100 for every 1000) items with tag A1.")
    @Test
    public void testPointsAccumulateByRateWithAdvancedRuleNotAnswered() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickAdvancedRulesRadioButton();
        AdvancedRuleCondition advancedRuleCondition = new AdvancedRuleCondition();
        advancedRuleCondition.setConditionKey(AdvancedRuleConditionKeys.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("A1");
        AdvancedRule advancedRule = AdvancedRule.newRule().setAdvancedRuleConditions(advancedRuleCondition);
        selectItems.setAdvancedRule(advancedRule);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        PointsAccumulationAction pointsAccumulationAction = ruleBuilder.getAccumulationActionByRate("100","10", PointsAccumulationAction.AccumulationUnits.POINTS, selectItems, null);
        createRule(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchaseAndValidate(newMember,items, "0", AmountType.Points, false);
        cancelPurchaseAndValidate(submitPurchaseResponse.getConfirmation(), "0", AmountType.Points, false);
    }

    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- accumulate by rate items with tag A1 and exclude items with code 888..")
    @Test
    public void testPointsAccumulateByRateWithAdvancedRuleAnswered() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickAdvancedRulesRadioButton();
        AdvancedRuleCondition advancedRuleCondition = new AdvancedRuleCondition();
        advancedRuleCondition.setConditionKey(AdvancedRuleConditionKeys.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("A1");
        AdvancedRule advancedRule = AdvancedRule.newRule().setAdvancedRuleConditions(advancedRuleCondition);
        advancedRule.clickSaveAsGroupBtn("group_" + timestamp, NewGroup.SAVE);
        selectItems.setAdvancedRule(advancedRule);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        SelectItems excludeItems = new SelectItems();
        excludeItems.clickItemCodesRadioButton();
        CodesGroup itemCodes1 = new CodesGroup();
        itemCodes1.setCodes(CodesGroup.ITEM_CODES_GROUP,"888");
        String groupName = "group_" + timestamp;
        itemCodes1.clickSaveAsGroupBtn(groupName, NewGroup.SAVE);

        excludeItems.setItemCodes(itemCodes1);
        excludeItems.clickButton(SelectedItemsDialogButtons.Apply.toString());


        PointsAccumulationAction pointsAccumulationAction = ruleBuilder.getAccumulationActionByRate("10","1", PointsAccumulationAction.AccumulationUnits.POINTS, selectItems, excludeItems);
        createRule(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchaseAndValidate(newMember,items, "5000", AmountType.Points, true);
        cancelPurchaseAndValidate(submitPurchaseResponse.getConfirmation(), "-5000", AmountType.Points, true);
    }


    @ru.yandex.qatools.allure.annotations.Description("Points accumulation- accumulate by rate any item with remainder.")
    @Test
    public void testPointsAccumulateByRateWithRemainder() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();
        PointsAccumulationAction pointsAccumulationAction = ruleBuilder.getAccumulationActionByRate("10","3.33", PointsAccumulationAction.AccumulationUnits.CREDIT, selectItems, null);
        createRule(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(pointsAccumulationAction));

        //Create member
        NewMember newMember = createNewMember();
        SubmitPurchaseResponse submitPurchaseResponse = submitPurchaseAndValidate(newMember,items, "32000", AmountType.Credit, true);
        cancelPurchaseAndValidate(submitPurchaseResponse.getConfirmation(), "-32000", AmountType.Credit, true);
    }


    private void cancelPurchaseAndValidate(String confirmation, String pointsAmount, AmountType wallet, boolean isPointsAccumulated) throws Exception{
        Thread.sleep(10000);
        String transactionId = UUID.randomUUID().toString();
        CancelPurchase cancelPurchase = new CancelPurchase();
        cancelPurchase.setConfirmation(confirmation);
        cancelPurchase.setTransactionId(transactionId);
        Assert.assertFalse("Confirmation must not be empty!", StringUtils.isEmpty(cancelPurchase.getConfirmation()));

        //Cancel the Purchase via Postman
        IServerResponse response = cancelPurchase.SendRequestByApiKey(apiKey, CancelPurchaseResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        //Cast to CancelPurchase object to fetch all response data
        CancelPurchaseResponse cancelPurchaseResponse = (CancelPurchaseResponse) response;
        Assert.assertEquals("status code must be 200", (Integer) 200, cancelPurchaseResponse.getStatusCode());

        Thread.sleep(10000);
        if(isPointsAccumulated) {
            log4j.info("check Data Store Updated with points");

            List<Entity> res = ServerHelper.queryWithWaitUntilResultEqOrGreat("Must find at least 2 UserAction ",
                    buildQueryWithSort(POINTSTRASACTION) , 2, dataStore, 120000);

            Text t = ((Text)res.get(0).getProperty("Data"));
            log4j.info("Data contains the following data : " +   t.getValue());

            if(AmountType.Points.equals(wallet)){
                Assert.assertTrue("Data must contain the Points : " + pointsAmount, t.getValue().contains("Points=\"" + pointsAmount + "\" "));
            }else{
                Assert.assertTrue("Data must contain the Points : " + pointsAmount, t.getValue().contains("BudgetWeighted=\"" + pointsAmount + "\" "));
            }
        } else {
            checkDataStoreNegativeCheckByRuleID(POINTSTRASACTION);
        }
    }


    private static Item  createItem(String itemCode, int quantity, double amount, int price, String tag) {
        Item item = new Item();
        item.setItemCode(itemCode);
        item.setItemName(itemCode);
        item.setDepartmentCode("DEP1");
        item.setDepartmentName("DEP1");
        item.setQuantity(quantity);
        item.setAmount(amount);
        item.setPrice(price);
        if(tag != null) {
            item.setTags(Lists.newArrayList(tag));
        }
        return item;
    }


    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }

}
