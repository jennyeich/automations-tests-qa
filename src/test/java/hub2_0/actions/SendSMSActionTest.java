package hub2_0.actions;

import com.beust.jcommander.internal.Lists;
import com.mashape.unirest.http.HttpResponse;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.AssetCondition;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.TagCondition;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.conditions.operators.ListOperators;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.services.member.MembersService;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import server.v2_8.DataStoreKeyUtility;
import server.v2_8.JoinClubResponse;

import java.util.UUID;

/**
 * This class test using different triggers  to test all available uses of the 'Send SMS' action in Activity
 */
public class SendSMSActionTest extends BaseActivityActionTest{

    String message = "Send member SMS ";

    @Override
    protected ActivityAction getConcreteAction() throws Exception {

        timestamp = String.valueOf(System.currentTimeMillis());
        ActivityAction activityAction = ActionsBuilder.buildSendTextMessageAction(message);
        return activityAction;
    }

    /**
     * Test trigger make purchase, no condition ,Action send member SMS
     */
    @Test
    @Category({Hub2Regression.class})
    public void testTriggerMakesPurchaseAndActionSendSMS() throws Exception {

        message = "Send member SMS from: testTriggerMakesPurchaseAndActionSendSMS";
        //Create new rule
        createRule(ActivityTrigger.MadeAPurchase.toString(),Lists.newArrayList());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "1000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithSendAction(transactionId,MEMBER_SMS_OPERATION);
    }

    /**
     * Test trigger make purchase, with condition ValueOfPurchaseIsGreaterThan ,Action send member SMS
     */
    @Test
    public void testTriggerMakesPurchasWithConditionValueOfPurchaseIsGreaterThanAndActionSendSMS() throws Exception {

        message = "Send member SMS from: testTriggerMakesPurchasWithConditionValueOfPurchaseIsGreaterThanAndActionSendSMS";

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("10");
        //Create new rule
        createRule(ActivityTrigger.MadeAPurchase.toString(),Lists.newArrayList(madeAPurchaseCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithSendAction(transactionId,MEMBER_SMS_OPERATION);
    }

    /**
     * Test triggerJoin the club, no condition ,Action send member SMS
     */
    @Test
    public void testTriggerJoinedTheClubAndActionSendSMS() throws Exception {

        message = "Send member SMS from: testTriggerJoinedTheClubAndActionSendSMS";
        //Create new rule
        createRule(ActivityTrigger.JoinTheClub.toString(),Lists.newArrayList());

        //Create member using signOn due to automation is triggered and consent is true
        JoinClubResponse joinClubResponse = serverHelper.createMember();
        DataStoreKeyUtility dataStoreKeyUtility = new DataStoreKeyUtility();
        HttpResponse response = dataStoreKeyUtility.SendRequest(joinClubResponse.getUserKey());
        key =  dataStoreKeyUtility.userKey((String)response.getBody());

        //DataStore check
        checkAutomationRunAndSendMemberSMS();
    }

    /**
     * Test trigger Update Membership Details, no condition ,Action send member SMS
     */
    @Test
    public void testTriggerUpdateMembershipDetailsAndActionSendSMS() throws Exception {

        message = "Send member SMS from: testTriggerUpdateMembershipDetailsAndActionSendSMS";

        NewMember newMember = createNewMember();
        //Create new rule
        createRule(ActivityTrigger.UpdatedMembershipDetails.toString(),Lists.newArrayList());

        performUpdateMemberDetails(newMember);
        checkAutomationRunAndSendMemberSMS();
        log4j.info("member received the sms");
    }

    /**
     * Test trigger Tag member, with condition TagIsOneOf,Action send member SMS
     */
    @Test
    public void testTriggerTaggedWithConditionTagIsOneOfAndActionSendSMS() throws Exception {

        message = "Send member SMS from: testTriggerTaggedWithConditionTagIsOneOfAndActionSendSMS";
        tag = ActionsBuilder.TAG + timestamp.toString();
        TagCondition tagCondition = new TagCondition();
        tagCondition.setConditionKey(TagCondition.ConditionKey.TAG)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue(tag);
        //Create new rule
        createRule(ActivityTrigger.Tagged.toString(),Lists.newArrayList());
        createNewMember();
        MembersService.performTagMemberSmartAction(tag, "TagMember");
        checkAutomationRunAndSendMemberSMS();
        log4j.info("member received the sms");
    }

    /**
     * Test trigger Receives An Asset, with condition Benefit IsExactly,Action send member SMS
     */
    @Test
    public void testTriggerReceivesAnAssetWithConditionBenefitIsExactlyAndActionSendSMS() throws Exception {

        message = "Send member SMS from: testTriggerReceivesAnAssetWithConditionBenefitIsExactlyAndActionSendSMS";
        smartGift = hubAssetsCreator.createSmartGift(timestamp);

        AssetCondition assetCondition = new AssetCondition();
        assetCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setConditionValue(smartGift.getTitle());
        //Create new rule
        createRule(ActivityTrigger.ReceivesAnAsset.toString(),Lists.newArrayList(assetCondition));
        createNewMember();
        performAnActionSendGift(smartGift);
        checkAutomationRunAndSendMemberSMS();
        log4j.info("member received the sms");
    }
}

