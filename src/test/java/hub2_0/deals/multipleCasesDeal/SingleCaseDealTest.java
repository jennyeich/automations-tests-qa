package hub2_0.deals.multipleCasesDeal;

import com.google.common.collect.Lists;
import hub.common.objects.member.NewMember;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.cases.DealCase;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.conditions.cases.MembersCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub.utils.HubMemberCreator;
import hub.hub2_0.common.builders.CasesConditionsBuilder;
import integration.base.BaseHub2DealTest;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.common.Item;
import server.v4.common.models.PurchaseItem;

import java.util.ArrayList;
import java.util.List;

public class SingleCaseDealTest extends BaseHub2DealTest {

    public static Box box;
    public static final Logger log4j = Logger.getLogger(SingleCaseDealTest.class);

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = dealBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };


    @Test
    public void singleCaseDealTest() throws Exception {
        String tag = "tag_" + timestamp;
        MembersCondition membersCondition = CasesConditionsBuilder.buildMembersCondition(MemberNonRegistrationFormFields.Tags, InputOperators.IS_ONE_OF, tag);
        DealCase case1 = new DealCase();
        case1.setConditions(Lists.newArrayList(membersCondition));
        case1.clickSpecificItemsDiscount();
        ActivityAction activityAction1 = buildSetSpecialPriceAction("1", dealBuilder.selectItemsByItemCode("1245"));
        case1.setActions(Lists.newArrayList(activityAction1));

        createMultipleCasesDeal(box, Lists.newArrayList(), Lists.newArrayList(case1));

        Item item1 = createItem("1245", "item1", "Drink", "Dep1", 500, 1);
        Item item2 = createItem("1245", "item2", "Drink", "Dep1", 800, 1);
        Item item3 = createItem("3344", "item3", "Drink", "Dep1", 600, 1);

        NewMember newMember = HubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1, item2), "1300", null);
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1, item2, item3), "1900", null);
        MembersService.performTagMemberSmartAction(tag, "action_tag");
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1, item2, item3), "1900", "-1100");

        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("1245", 1, 500, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("1245", 1, 800, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("3344", 1, 600, "depCode", "depName"));
        performPurchaseV4(newMember, items, 1900, -1100);
    }

}
