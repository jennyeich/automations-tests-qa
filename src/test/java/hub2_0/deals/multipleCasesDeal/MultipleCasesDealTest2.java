package hub2_0.deals.multipleCasesDeal;

import com.google.common.collect.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.cases.DealCase;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountEntireTicketAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.SendCodeToPOSAction;
import hub.hub2_0.common.actions.dealActions.SetSpecialPriceAction;
import hub.hub2_0.common.builders.CasesConditionsBuilder;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.cases.CaseMadeAPurchaseCondition;
import hub.hub2_0.common.conditions.cases.GeneralCondition;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.conditions.operators.ShoppingCartOperators;
import hub.hub2_0.common.enums.ItemOperation;
import hub.hub2_0.common.items.CodesGroup;
import hub.hub2_0.common.items.NewGroup;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.ActivityService;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.utils.HubMemberCreator;
import integration.base.BaseHub2DealTest;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.common.Item;
import server.v4.common.models.PurchaseItem;

import java.util.ArrayList;
import java.util.List;

public class MultipleCasesDealTest2 extends BaseHub2DealTest {

    public static Box box;
    public static final Logger log4j = Logger.getLogger(MultipleCasesDealTest.class);
    public static NewMember newMember;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = dealBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);

        GeneralCondition generalCondition1 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchase("43211", MadeAPurchaseCondition.ConditionKey.SHOPPING_CART, ShoppingCartOperators.CONTAINS_AT_LEAST, "1");

        DealCase case1 = new DealCase();
        case1.setConditions(Lists.newArrayList(generalCondition1));
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket("5");
        SendCodeToPOSAction sendCodeToPOSAction = buildSendCodeToPOS(SendCodeToPOSAction.CodeType.ITEM_CODE, "55555");
        case1.setActions(Lists.newArrayList(amountOffDiscountEntireTicketAction, sendCodeToPOSAction));


        GeneralCondition generalCondition2 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND, IntOperators.IS_LESS_THAN, "15");

        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction2 = new AmountOffDiscountEntireTicketAction();
        amountOffDiscountEntireTicketAction2.setDiscountAmount("4");
        SendCodeToPOSAction deleteSendCodeToPOSAction = new SendCodeToPOSAction();
        deleteSendCodeToPOSAction.clickDeleteAction();

        DealCase case2 = new DealCase();
        case2.setConditions(Lists.newArrayList(generalCondition2));
        case2.setActions(Lists.newArrayList(amountOffDiscountEntireTicketAction2, deleteSendCodeToPOSAction));

        GeneralCondition generalCondition3 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchase("8778", MadeAPurchaseCondition.ConditionKey.SHOPPING_CART, ShoppingCartOperators.CONTAINS_AT_LEAST, "2");

        SelectItems selectFruitItems = new SelectItems();
        selectFruitItems.clickItemCodesRadioButton();
        CodesGroup itemCodes = new CodesGroup();
        itemCodes.setCodes(CodesGroup.ITEM_CODES_GROUP, "6543", "8877", "9989", "1222");
        String groupName = "group_Fruits";
        itemCodes.clickSaveAsGroupBtn(groupName, NewGroup.SAVE);
        selectFruitItems.setItemCodes(itemCodes);
        selectFruitItems.clickApply();
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction("2", selectFruitItems);


        SelectItems selectJunkItems = new SelectItems();
        selectJunkItems.clickItemCodesRadioButton();
        CodesGroup itemCodes2 = new CodesGroup();
        itemCodes2.setCodes(CodesGroup.ITEM_CODES_GROUP, "3334", "4445", "5556", "7778");
        String groupName2 = "group_Junk";
        itemCodes2.clickSaveAsGroupBtn(groupName2, NewGroup.SAVE);
        selectJunkItems.setItemCodes(itemCodes2);
        selectJunkItems.clickApply();
        PercentOffDiscountAction percentOffDiscountAction = buildPercentOffDiscount("10", selectJunkItems);

        DealCase case3 = new DealCase();
        case3.clickSpecificItemsDiscount();
        case3.setConditions(Lists.newArrayList(generalCondition3));
        case3.setActions(Lists.newArrayList(setSpecialPriceAction, percentOffDiscountAction));

        CaseMadeAPurchaseCondition madeAPurchaseCondition = new CaseMadeAPurchaseCondition();
        madeAPurchaseCondition.clickDelete();
        GeneralCondition generalCondition4 = madeAPurchaseCondition;

        DealCase case4 = new DealCase();
        case4.clickEntireTicketDiscount();
        case4.setConditions(Lists.newArrayList(generalCondition4));
        case4.setActions(Lists.newArrayList(buildAmountOffDiscountOnEntireTicket("10")));


        createMultipleCasesDeal(box, Lists.newArrayList(), Lists.newArrayList(case1, case2, case3, case4));
        timestamp = String.valueOf(System.currentTimeMillis());
        newMember = HubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    @Test
    public void multipleCaseDealCase1ActionsPerformedTest() throws Exception {
        Item item1 = createItem("43211", "item1", "DEP_1", "Dep1", 1400, 2);
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1), "1400", "-500");
    }

    @Test
    public void multipleCaseDealCase1ActionsPerformedTest_v4() throws Exception {
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("43211", 2, 1400, "depCode", "depName"));
        performPurchaseV4(newMember, items, 1400, -500);
    }


    @Test
    public void multipleCaseDealCase2ActionsPerformedTest() throws Exception {
        Item item1 = createItem("8778", "item1", "DEP_1", "Dep1", 1200, 3);
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1), "1200", "-400");
    }

    @Test
    public void multipleCaseDealCase2ActionsPerformedTest_v4() throws Exception {
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("8778", 3, 1200, "depCode", "depName"));
        performPurchaseV4(newMember, items, 1200, -400);
    }

    @Test
    public void multipleCaseDealCase3ActionsPerformedTest() throws Exception {
        Item item1 = createItem("8778", "item1", "DEP_1", "Dep1", 2400, 3);
        Item item2 = createItem("1222", "item2", "DEP_1", "Dep1", 2000, 1);
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1, item2), "4400", "-1800");
    }

    @Test
    @Category({Hub2Regression.class})
    public void multipleCaseDealCase3ActionsPerformedTest_v4() throws Exception {
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("8778", 3, 2400, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("1222", 1, 2000, "depCode", "depName"));
        performPurchaseV4(newMember, items, 4400, -1800);
    }

    @Test
    public void multipleCaseDealCase4ActionsPerformedTest() throws Exception {
        Item item1 = createItem("5532", "item1", "DEP_1", "Dep1", 6000, 3);
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1), "6000", "-1000");
    }

    @Test
    public void multipleCaseDealCase4ActionsPerformedTest_v4() throws Exception {
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("5532", 3, 6000, "depCode", "depName"));
        performPurchaseV4(newMember, items, 6000, -1000);
    }

    @After
    public void deactivateDeals() throws Exception {
        log4j.info("No need to deactivate deals after each test.");
    }

    @AfterClass
    public static void deactivateMultipleCasesDeal() throws Exception {
        try {
            log4j.info("Starting deactivating deal " + deal.getActivityName() + " .");
            ActivityService.executeOperation(deal.getActivityName(), ItemOperation.STOP);
        } catch (Exception e) {
            log4j.info(e.getStackTrace());
        }

    }

}
