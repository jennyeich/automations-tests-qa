package hub2_0.deals.multipleCasesDeal;

import com.google.common.collect.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.discounts.v4.GetBenefitsHelper;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.cases.DealCase;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.dealActions.MakeItemsFreeAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountAction;
import hub.hub2_0.common.builders.CasesConditionsBuilder;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.cases.CaseMadeAPurchaseCondition;
import hub.hub2_0.common.conditions.cases.GeneralCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.ItemOperation;
import hub.hub2_0.common.items.BundleLine;
import hub.hub2_0.common.items.SelectBundle;
import hub.hub2_0.services.loyalty.ActivityService;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.utils.HubMemberCreator;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseHub2DealTest;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.common.Item;
import server.v4.GetBenefits;
import server.v4.GetBenefitsResponse;
import server.v4.base.BaseZappServerAPI;
import server.v4.common.models.Purchase;
import server.v4.common.models.PurchaseItem;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MultipleCasesDealTest extends BaseHub2DealTest {

    public static Box box;
    public static final Logger log4j = Logger.getLogger(MultipleCasesDealTest.class);
    public static NewMember newMember;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = dealBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);

        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());
        DatePicker datePickerEnd = DatePicker.newDatePicker().setSelectEndDay(DateTimeUtility.getTodayPlusXDays(2));
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime().setDatePickerStart(datePickerStart).setDatePickerEnd(datePickerEnd);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        GeneralCondition generalCondition1 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND, IntOperators.IS_GREATER_THAN, "5");
        GeneralCondition generalCondition2 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.POS_ID, InputOperators.IS_ONE_OF, "10");

        DealCase caseX = new DealCase();
        caseX.setConditions(Lists.newArrayList(generalCondition1, generalCondition2));
        caseX.clickAdvancedDiscount();
        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickEntireRecurringBundleButton();
        selectBundle.clickApplyButton();
        ActivityAction activityAction1 = buildAmountOffDiscount("10", selectBundle);
        OccurrencesCondition occurrencesCondition1 = new OccurrencesCondition();
        occurrencesCondition1.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("5583"));
        occurrencesCondition1.setConditionKey(OccurrencesCondition.Condition.IN_THE_TOTAL_SPEND_OF);
        occurrencesCondition1.setConditionValue("100");
        caseX.setOccurrencesConditions(Lists.newArrayList(occurrencesCondition1));
        caseX.setActions(Lists.newArrayList(activityAction1));

        DealCase caseZ = new DealCase();
        GeneralCondition generalCondition5 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, "10");
        CaseMadeAPurchaseCondition madeAPurchaseCondition = new CaseMadeAPurchaseCondition();
        madeAPurchaseCondition.clickDelete();
        GeneralCondition generalCondition4 = madeAPurchaseCondition;
        caseZ.setConditions(Lists.newArrayList(generalCondition5, generalCondition4));
        OccurrencesCondition occurrencesCondition3 = new OccurrencesCondition();
        occurrencesCondition3.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("5574"));
        occurrencesCondition3.setConditionKey(OccurrencesCondition.Condition.IN_THE_TOTAL_SPEND_OF);
        occurrencesCondition3.setConditionValue("20");
        caseZ.setOccurrencesConditions(Lists.newArrayList(occurrencesCondition3));
        SelectBundle selectBundle1 = new SelectBundle();
        selectBundle1.clickSpecificItemsFromBundleButton();
        selectBundle1.clickApplyButton();
        MakeItemsFreeAction makeItemsFreeAction = buildMakeItemFreeAction(selectBundle1);
        caseZ.setActions(Lists.newArrayList(makeItemsFreeAction));
        caseZ.clickOrderUp();


        DealCase caseY = new DealCase();
        GeneralCondition generalCondition3 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND, IntOperators.IS_LESS_THAN, "50");
        CaseMadeAPurchaseCondition madeAPurchaseCondition2 = new CaseMadeAPurchaseCondition();
        madeAPurchaseCondition2.clickDelete();
        GeneralCondition generalCondition6 = madeAPurchaseCondition2;
        caseY.setConditions(Lists.newArrayList(generalCondition3, generalCondition6));
        OccurrencesCondition occurrencesCondition2 = new OccurrencesCondition();
        occurrencesCondition2.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("3322", "5544"));
        occurrencesCondition2.setConditionKey(OccurrencesCondition.Condition.IN_THE_QUANTITY_OF);
        occurrencesCondition2.setConditionValue("2");
        caseY.setOccurrencesConditions(Lists.newArrayList(occurrencesCondition2));
        SelectBundle selectBundleY = new SelectBundle();
        selectBundleY.clickSpecificItemsFromBundleButton();
        BundleLine bundleLine = new BundleLine();
        bundleLine.selectLineQuantity("2");
        selectBundleY.setBundleLines(Lists.newArrayList(bundleLine));
        selectBundleY.clickApplyButton();
        PercentOffDiscountAction percentOffDiscountAction = buildPercentOffDiscount("50", selectBundleY);
        caseY.setActions(Lists.newArrayList(percentOffDiscountAction));
        caseY.clickOrderUp();

        createMultipleCasesDeal(box, Lists.newArrayList(dateTimeGlobalCondition), Lists.newArrayList(caseX, caseZ, caseY));

        timestamp = String.valueOf(System.currentTimeMillis());
        newMember = HubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    @After
    public void deactivateDeals() throws Exception {
        log4j.info("No need to deactivate deals after each test.");
    }

    @Test
    public void multipleCaseDealPOSAndBranchDontMatchTest() throws Exception {
        Item item1 = createItem("5583", "item1", "DEP_1", "Dep1", 7500, 3);
        getMemberBenefitsWithPodIdAndBranch(newMember.getPhoneNumber(), "7500", Lists.newArrayList(item1), "9", "8", null);
    }

    @Test
    public void multipleCaseDealPOSAndBranchDontMatchTest_v4() throws Exception {
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("5583", 3, 7500, "depCode", "depName"));
        performPurchaseV4(newMember, items, "9", "8", 7500, 0);
    }

    @Test
    public void multipleCaseDealCase1ActionsPerformedTest() throws Exception {
        Item item1 = createItem("5583", "item1", "DEP_1", "Dep1", 20000, 4);
        Item item2 = createItem("5544", "item2", "DEP_1", "Dep1", 8000, 2);
        Item item3 = createItem("5574", "item3", "DEP_1", "Dep1", 4500, 3);
        getMemberBenefitsWithPodIdAndBranch(newMember.getPhoneNumber(), "32500", Lists.newArrayList(item1, item2, item3), "10", "10", "-3000");
    }

    @Test
    public void multipleCaseDealCase1ActionsPerformedTest_v4() throws Exception {
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("5583", 4, 20000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("5544", 2, 8000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("5574", 3, 4500, "depCode", "depName"));
        performPurchaseV4(newMember, items, "10", "10", 32500, -3000);
    }

    @Test
    @Category({Hub2Regression.class})
    public void multipleCaseDealCase2ActionsPerformedTest() throws Exception {
        Item item1 = createItem("5544", "item1", "DEP_1", "Dep1", 4000, 5);
        getMemberBenefitsWithPodIdAndBranch(newMember.getPhoneNumber(), "4000", Lists.newArrayList(item1), "10", "3", "-1600");
    }

    @Test
    @Category({Hub2Regression.class})
    public void multipleCaseDealCase2ActionsPerformedTest_v4() throws Exception {
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("5544", 5, 4000, "depCode", "depName"));
        performPurchaseV4(newMember, items, "10", "3", 4000, -1600);
    }

    @Test
    public void multipleCaseDealCase3ActionsPerformedTest() throws Exception {
        Item item1 = createItem("5583", "item1", "DEP_1", "Dep1", 20000, 4);
        getMemberBenefitsWithPodIdAndBranch(newMember.getPhoneNumber(), "20000", Lists.newArrayList(item1), "10", "3", "-2000");
    }

    @Test
    public void multipleCaseDealCase3ActionsPerformedTest_v4() throws Exception {
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("5583", 4, 20000, "depCode", "depName"));
        performPurchaseV4(newMember, items, "10", "3", 20000, -2000);
    }


    public GetBenefitsResponse performPurchaseV4(NewMember newMember, List<PurchaseItem> items, String posId, String branchId, int totalSum, int expected) throws Exception {
        //Create purchase
        Purchase purchase = v4Builder.buildPurchase(UUID.randomUUID().toString(), totalSum, items);

        //Send request & get response
        GetBenefits getBenefits = GetBenefitsHelper.getInstance().getBenefitsRequest(apiKey, newMember.getPhoneNumber(), purchase, null, null);
        getBenefits.addHeaderParameter(BaseZappServerAPI.POS_ID_HEADER_PARAM, posId);
        getBenefits.addHeaderParameter(BaseZappServerAPI.BRANCH_ID_HEADER_PARAM, branchId);
        GetBenefitsResponse getBenefitsResponse = (GetBenefitsResponse) getBenefits.SendRequestByApiKey(apiKey, GetBenefitsResponse.class);
        checkResponseV4(getBenefitsResponse, expected);
        return getBenefitsResponse;
    }


    @AfterClass
    public static void deactivateMultipleCasesDeal() throws Exception {
        try {
            log4j.info("Starting deactivating deal " + deal.getActivityName() + " .");
            ActivityService.executeOperation(deal.getActivityName(), ItemOperation.STOP);
        } catch (Exception e) {
            log4j.info(e.getStackTrace());
        }

    }
}
