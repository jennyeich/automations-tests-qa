package hub2_0.deals.multipleCasesDeal;

import hub.base.BaseHubTest;
import integration.base.BaseIntegrationTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.IOException;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        SingleCaseDealTest.class,
        MultipleCasesDealTest.class,
        MultipleCasesDealTest2.class
})

public class MultipleCasesDealSuite {

    public static final Logger log4j = Logger.getLogger(MultipleCasesDealSuite.class);

    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseIntegrationTest.initSuite(true);
    }


    @AfterClass
    public static void tearDown() {
        log4j.info("close browser after finish all Suite tests");
        BaseIntegrationTest.cleanBase();
        BaseHubTest.cleanHub();
    }

}
