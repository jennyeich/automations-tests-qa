package hub2_0.deals;

import com.google.common.collect.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.common.actions.dealActions.*;
import hub.hub2_0.common.items.BundleLine;
import hub.hub2_0.common.items.SelectBundle;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.utils.HubMemberCreator;
import integration.base.BaseHub2DealTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.GetMemberBenefitsResponse;
import server.v2_8.common.Item;
import server.v4.common.models.PurchaseItem;

import java.util.ArrayList;
import java.util.List;

public class DealActionLimitsTest extends BaseHub2DealTest {

    public static Box box;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = dealBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    @Test
    @Category({Hub2Regression.class})
    public void testEntireTicketDiscount10percentWithLimit() throws Exception {
        String percentDiscount = "10";
        String amountLimit = "100";
        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = buildPercentOffDiscountOnEntireTicket(percentDiscount, NO_ITEM_EXCLUDED, amountLimit);
        createEntireTicketDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(percentOffDiscountEntireTicketAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 90000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 50000, 2);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "140000", "-10000");
    }

    @Test
    public void testEntireTicketDiscount10percentWithLimit_V4() throws Exception {
        String percentDiscount = "10";
        String amountLimit = "100";
        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = buildPercentOffDiscountOnEntireTicket(percentDiscount, NO_ITEM_EXCLUDED, amountLimit);
        createEntireTicketDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(percentOffDiscountEntireTicketAction));

        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("1111", 1, 90000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("2222", 2, 50000, "depCode", "depName"));
        performPurchaseV4(items, 140000, -10000);
    }

    @Test
    public void testEntireTicket5percentDiscountWithLimitAndExcludeItem() throws Exception {
        String percentDiscount = "5";
        String amountLimit = "50";
        SelectItems excludeItems = dealBuilder.selectItemsByItemCode("2222");
        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = buildPercentOffDiscountOnEntireTicket(percentDiscount, excludeItems, amountLimit);
        createEntireTicketDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(percentOffDiscountEntireTicketAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 70000, 2);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 40000, 1);
        Item item3 = createItem("3333", "Prod1", "123", "Dep1", 40000, 1);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2, item3), "150000", "-5000");
    }

    @Test
    public void testSpecificItemsAmountOffDiscountsWithLimitAmountAndTimes() throws Exception {
        String amountLimit = "80";
        String timesLimit = "2";
        String discountAmount = "50";
        AmountOffDiscountAction amountOffDiscountAction = buildAmountOffDiscount(discountAmount, dealBuilder.selectItemsByItemCode("2222"), amountLimit, timesLimit);
        createSpecificItemsDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(amountOffDiscountAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 20000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 30000, 3);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "50000", "-8000");
    }

    @Test
    public void testSpecificItemsPercentOffDiscountsWithLimitAmountAndTimes() throws Exception {
        String amountLimit = "50";
        String timesLimit = "1";
        String discountPercent = "20";
        PercentOffDiscountAction percentOffDiscountAction = buildPercentOffDiscount(discountPercent, dealBuilder.selectItemsByItemCode("2222"), amountLimit, timesLimit);
        createSpecificItemsDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(percentOffDiscountAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 20000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 20000, 2);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "40000", "-2000");
    }

    @Test
    public void testSpecificItemsSpecialPriceDiscountsWithLimitAmountAndTimes() throws Exception {
        String amountLimit = "80";
        String timesLimit = "3";
        String specialPrice = "20";
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction(specialPrice, dealBuilder.selectItemsByItemCode("2222"), amountLimit, timesLimit);
        createSpecificItemsDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(setSpecialPriceAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 25000, 5);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "35000", "-8000");

        timestamp = String.valueOf(System.currentTimeMillis());
        Item item3 = createItem("1111", "Prod1", "123", "Dep1", 10000, 1);
        Item item4 = createItem("2222", "Prod1", "123", "Dep1", 20000, 5);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item3, item4), "30000", "-6000");
    }

    @Test
    public void testSpecificItemsMakeItemFreeDiscountsWithLimitAmountAndTimes() throws Exception {
        String amountLimit = "100";
        String timesLimit = "2";
        MakeItemsFreeAction makeItemsFreeAction = buildMakeItemFreeAction(dealBuilder.selectItemsByItemCode("555"), amountLimit, timesLimit);
        createSpecificItemsDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(makeItemsFreeAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("555", "Prod1", "123", "Dep1", 9000, 3);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "19000", "-6000");

        timestamp = String.valueOf(System.currentTimeMillis());
        Item item3 = createItem("1111", "Prod1", "123", "Dep1", 10000, 1);
        Item item4 = createItem("555", "Prod1", "123", "Dep1", 18000, 3);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item3, item4), "28000", "-10000");

    }

    @Test
    public void testSpecificItemsAmountOffAndPercentOffDiscountsWithLimits() throws Exception {
        String amountDiscount = "40";
        String amountOffTimesLimit = "2";
        String amountOffAmountLimit = "60";
        AmountOffDiscountAction amountOffDiscountAction = buildAmountOffDiscount(amountDiscount, dealBuilder.selectItemsByItemCode("1111"), amountOffAmountLimit, amountOffTimesLimit);

        String percentDiscount = "5";
        String percentOffAmountLimit = "100";
        PercentOffDiscountAction percentOffDiscountAction = buildPercentOffDiscount(percentDiscount, dealBuilder.selectItemsByItemCode("2222"), percentOffAmountLimit, NO_DISCOUNT_TIMES_LIMIT);
        createSpecificItemsDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(amountOffDiscountAction, percentOffDiscountAction));

        Item item1 = createItem("7777", "Prod1", "123", "Dep1", 10000, 1);
        NewMember newMember = HubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        GetMemberBenefitsResponse getMemberBenefitsResponseNegativeTest = getMemberBenefits(newMember.getPhoneNumber(), "10000", Lists.newArrayList(item1));
        Assert.assertEquals("Verify discount's sum is as expected", null, getMemberBenefitsResponseNegativeTest.getTotalSumDiscount());

        timestamp = String.valueOf(System.currentTimeMillis());
        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 18000, 3);
        Item item3 = createItem("2222", "Prod1", "123", "Dep1", 240000, 6);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item2, item3), "258000", "-16000");
    }

    @Test
    public void testAdvancedDealGetFreeItemWithAmountLimit() throws Exception {

        String amountLimit = "75";
        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickSpecificItemsFromBundleButton();
        BundleLine bundleLine = new BundleLine();
        selectBundle.setBundleLines(Lists.newArrayList(bundleLine));
        selectBundle.clickApplyButton();

        MakeItemsFreeAction makeItemsFreeAction = buildMakeItemFreeAction(selectBundle, amountLimit, NO_DISCOUNT_TIMES_LIMIT);
        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("2222"));
        occurrencesCondition.setConditionValue("2");
        createAdvancedDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(occurrencesCondition), Lists.newArrayList(makeItemsFreeAction));

        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 3000, 3);
        Item item3 = createItem("2222", "Prod1", "123", "Dep1", 25000, 5);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item2, item3), "28000", "-7500");
    }

    @Test
    public void testAdvancedDealGetFreeItemWithTimesLimit() throws Exception {

        String timesLimit = "2";
        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickSpecificItemsFromBundleButton();
        BundleLine bundleLine = new BundleLine();
        selectBundle.setBundleLines(Lists.newArrayList(bundleLine));
        selectBundle.clickApplyButton();

        MakeItemsFreeAction makeItemsFreeAction = buildMakeItemFreeAction(selectBundle, NO_DISCOUNT_AMOUNT_LIMIT, timesLimit);
        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("2222"));
        occurrencesCondition.setConditionValue("2");
        createAdvancedDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(occurrencesCondition), Lists.newArrayList(makeItemsFreeAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 3000, 3);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 35000, 7);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "38000", "-10000");
    }

    @Test
    public void testAdvancedDealAmountOffDiscountOnItemWithTimesLimit() throws Exception {
        String amountDiscount = "10";
        String limitTimes = "2";
        OccurrencesCondition occurrencesCondition1 = new OccurrencesCondition();
        occurrencesCondition1.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("1111"));
        occurrencesCondition1.setConditionValue("1");

        OccurrencesCondition occurrencesCondition2 = new OccurrencesCondition();
        occurrencesCondition2.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("2222"));
        occurrencesCondition2.setConditionValue("1");

        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickSpecificItemsFromBundleButton();
        BundleLine bundleLine1 = new BundleLine();
        BundleLine bundleLine2 = new BundleLine();
        bundleLine2.clickLineCheckbox();
        selectBundle.setBundleLines(Lists.newArrayList(bundleLine1, bundleLine2));
        selectBundle.clickApplyButton();

        AmountOffDiscountAction amountOffDiscountAction = buildAmountOffDiscount(amountDiscount, selectBundle, NO_DISCOUNT_AMOUNT_LIMIT, limitTimes);

        createAdvancedDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(occurrencesCondition1, occurrencesCondition2), Lists.newArrayList(amountOffDiscountAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 6000, 3);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 12000, 3);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "15000", "-2000");
    }

    @Test
    public void testAdvancedDealAmountOffDiscountOnItemWithAmountLimit() throws Exception {
        String amountDiscount = "10";
        String limitAmount = "35";
        OccurrencesCondition occurrencesCondition1 = new OccurrencesCondition();
        occurrencesCondition1.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("1111"));
        occurrencesCondition1.setConditionValue("1");

        OccurrencesCondition occurrencesCondition2 = new OccurrencesCondition();
        occurrencesCondition2.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("2222"));
        occurrencesCondition2.setConditionValue("1");

        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickSpecificItemsFromBundleButton();
        BundleLine bundleLine1 = new BundleLine();
        BundleLine bundleLine2 = new BundleLine();
        bundleLine2.clickLineCheckbox();
        selectBundle.setBundleLines(Lists.newArrayList(bundleLine1, bundleLine2));
        selectBundle.clickApplyButton();

        AmountOffDiscountAction amountOffDiscountAction = buildAmountOffDiscount(amountDiscount, selectBundle, limitAmount, NO_DISCOUNT_TIMES_LIMIT);

        createAdvancedDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(occurrencesCondition1, occurrencesCondition2), Lists.newArrayList(amountOffDiscountAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 4000, 4);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 12000, 4);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "16000", "-3500");
    }

    @Test
    public void testAdvancedDealPercentOffDiscountOnItemWithTimesLimit() throws Exception {
        String percentDiscount = "10";
        String limitTimes = "1";
        OccurrencesCondition occurrencesCondition1 = new OccurrencesCondition();
        occurrencesCondition1.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("1111"));
        occurrencesCondition1.setConditionValue("1");

        OccurrencesCondition occurrencesCondition2 = new OccurrencesCondition();
        occurrencesCondition2.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("2222"));
        occurrencesCondition2.setConditionValue("2");

        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickSpecificItemsFromBundleButton();
        BundleLine bundleLine1 = new BundleLine();
        BundleLine bundleLine2 = new BundleLine();
        bundleLine2.clickLineCheckbox();
        bundleLine2.selectLineQuantity("2");
        selectBundle.setBundleLines(Lists.newArrayList(bundleLine1, bundleLine2));
        selectBundle.clickApplyButton();

        PercentOffDiscountAction percentOffDiscountAction = buildPercentOffDiscount(percentDiscount, selectBundle, NO_DISCOUNT_AMOUNT_LIMIT, limitTimes);

        createAdvancedDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(occurrencesCondition1, occurrencesCondition2), Lists.newArrayList(percentOffDiscountAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 60000, 3);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 150000, 5);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "210000", "-6000");
    }

    @Test
    public void testAdvancedDealPercentOffDiscountOnItemWithAmountLimit() throws Exception {
        String percentDiscount = "10";
        String limitAmount = "60";
        OccurrencesCondition occurrencesCondition1 = new OccurrencesCondition();
        occurrencesCondition1.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("1111"));
        occurrencesCondition1.setConditionValue("2");

        OccurrencesCondition occurrencesCondition2 = new OccurrencesCondition();
        occurrencesCondition2.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("2222"));
        occurrencesCondition2.setConditionValue("2");

        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickSpecificItemsFromBundleButton();
        BundleLine bundleLine1 = new BundleLine();
        bundleLine1.clickLineCheckbox();
        BundleLine bundleLine2 = new BundleLine();
        bundleLine2.clickLineCheckbox();
        bundleLine2.selectLineQuantity("2");
        selectBundle.setBundleLines(Lists.newArrayList(bundleLine1, bundleLine2));
        selectBundle.clickApplyButton();

        PercentOffDiscountAction percentOffDiscountAction = buildPercentOffDiscount(percentDiscount, selectBundle, limitAmount, NO_DISCOUNT_TIMES_LIMIT);

        createAdvancedDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(occurrencesCondition1, occurrencesCondition2), Lists.newArrayList(percentOffDiscountAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 80000, 4);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 150000, 5);
        Item item3 = createItem("3333", "Prod1", "123", "Dep1", 2000, 5);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2, item3), "232000", "-6000");
    }

    @Test
    public void testAdvancedDealSpecialPriceDiscountOnItemWithTimesLimit() throws Exception {
        String specialPrice = "100";
        String limitTimes = "2";
        OccurrencesCondition occurrencesCondition1 = new OccurrencesCondition();
        occurrencesCondition1.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("1111"));
        occurrencesCondition1.setConditionValue("1");

        OccurrencesCondition occurrencesCondition2 = new OccurrencesCondition();
        occurrencesCondition2.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("2222"));
        occurrencesCondition2.setConditionValue("2");

        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickSpecificItemsFromBundleButton();
        BundleLine bundleLine2 = new BundleLine();
        bundleLine2.clickLineCheckbox();
        bundleLine2.selectLineQuantity("2");
        selectBundle.setBundleLines(Lists.newArrayList(new BundleLine(), bundleLine2));
        selectBundle.clickApplyButton();

        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction(specialPrice, selectBundle, NO_DISCOUNT_AMOUNT_LIMIT, limitTimes);

        createAdvancedDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(occurrencesCondition1, occurrencesCondition2), Lists.newArrayList(setSpecialPriceAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 80000, 4);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 210000, 7);
        Item item3 = createItem("3333", "Prod1", "123", "Dep1", 2000, 5);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2, item3), "292000", "-100000");
    }

    @Test
    public void testAdvancedDealSpecialPriceDiscountOnItemWithAmountLimit() throws Exception {
        String specialPrice = "150";
        String limitAmount = "80";
        OccurrencesCondition occurrencesCondition1 = new OccurrencesCondition();
        occurrencesCondition1.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("1111"));
        occurrencesCondition1.setConditionValue("2");

        OccurrencesCondition occurrencesCondition2 = new OccurrencesCondition();
        occurrencesCondition2.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("2222"));
        occurrencesCondition2.setConditionValue("2");

        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickSpecificItemsFromBundleButton();
        BundleLine bundleLine1 = new BundleLine();
        bundleLine1.clickLineCheckbox();
        bundleLine1.selectLineQuantity("2");
        selectBundle.setBundleLines(Lists.newArrayList(bundleLine1));
        selectBundle.clickApplyButton();

        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction(specialPrice, selectBundle, limitAmount, NO_DISCOUNT_TIMES_LIMIT);

        createAdvancedDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(occurrencesCondition1, occurrencesCondition2), Lists.newArrayList(setSpecialPriceAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 80000, 4);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 150000, 5);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "230000", "-8000");
    }

    @Test
    public void testAdvancedDealSpecialPriceDiscountOnItemWithAmountAndTimesLimit() throws Exception {
        String specialPrice = "150";
        String limitAmount = "250";
        String limitTimes = "2";
        OccurrencesCondition occurrencesCondition1 = new OccurrencesCondition();
        occurrencesCondition1.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("1111"));
        occurrencesCondition1.setConditionValue("2");

        OccurrencesCondition occurrencesCondition2 = new OccurrencesCondition();
        occurrencesCondition2.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("2222"));
        occurrencesCondition2.setConditionValue("3");

        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickSpecificItemsFromBundleButton();
        BundleLine bundleLine2 = new BundleLine();
        bundleLine2.clickLineCheckbox();
        bundleLine2.selectLineQuantity("2");
        selectBundle.setBundleLines(Lists.newArrayList(new BundleLine(), bundleLine2));
        selectBundle.clickApplyButton();

        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction(specialPrice, selectBundle, limitAmount, limitTimes);

        createAdvancedDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(occurrencesCondition1, occurrencesCondition2), Lists.newArrayList(setSpecialPriceAction));

        Item item1Negative = createItem("3333", "Prod1", "123", "Dep1", 120000, 6);
        Item item2Negative = createItem("4444", "Prod1", "123", "Dep1", 330000, 11);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1Negative, item2Negative), "430000", null);

        timestamp = String.valueOf(System.currentTimeMillis());
        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 120000, 6);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 330000, 11);
        Item item3 = createItem("3333", "Prod1", "123", "Dep1", 330000, 11);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2, item3), "430000", "-25000");
    }

}
