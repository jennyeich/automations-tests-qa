package hub2_0.deals;

import com.google.common.collect.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.MakeItemsFreeAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.SetSpecialPriceAction;
import hub.hub2_0.services.loyalty.LoyaltyService;
import integration.base.BaseHub2DealTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.common.Item;

public class SpecificItemsDealTest extends BaseHub2DealTest {

    public static Box box;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = dealBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    @Test
    public void testSpecificItemsAmountOffDeal() throws Exception {
        String amountDiscount = "10";
        AmountOffDiscountAction amountOffDiscountAction = buildAmountOffDiscount(amountDiscount, dealBuilder.selectItemsByItemCode("1111"));
        createSpecificItemsDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(amountOffDiscountAction));

        Item item1 = createItem("7777", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 18000, 3);
        Item item3 = createItem("2222", "Prod1", "123", "Dep1", 10000, 2);

        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2, item3), "38000", "-3000");
    }

    @Test
    public void testSpecificItemsPercentOffDeal() throws Exception {
        String percentDiscount = "20";
        PercentOffDiscountAction percentOffDiscountAction = buildPercentOffDiscount(percentDiscount, dealBuilder.selectItemsByItemCode("1111"));
        createSpecificItemsDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(percentOffDiscountAction));

        Item item1 = createItem("7777", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 18000, 3);
        Item item3 = createItem("2222", "Prod1", "123", "Dep1", 10000, 2);

        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2, item3), "38000", "-3600");
    }

    @Test
    public void testSpecificItemsSpecialPriceDeal() throws Exception {
        String specialPrice = "1";
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction(specialPrice, dealBuilder.selectItemsByItemCode("1111"));
        createSpecificItemsDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(setSpecialPriceAction));

        Item item1 = createItem("7777", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 9000, 2);
        Item item3 = createItem("2222", "Prod1", "123", "Dep1", 10000, 2);

        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2, item3), "29000", "-8800");
    }

    @Test
    @Category({Hub2Regression.class})
    public void testSpecificItemsSpecialPriceZeroDeal() throws Exception {
        String specialPrice = "0";
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction(specialPrice, dealBuilder.selectItemsByItemCode("1111"));
        createSpecificItemsDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(setSpecialPriceAction));

        Item item1 = createItem("7777", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 9000, 2);
        Item item3 = createItem("2222", "Prod1", "123", "Dep1", 10000, 2);

        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2, item3), "29000", "-9000");
    }

    @Test
    @Category({Hub2Regression.class})
    public void testSpecificItemsFreeItemDeal() throws Exception {
        MakeItemsFreeAction makeItemsFreeAction = buildMakeItemFreeAction(dealBuilder.selectItemsByItemCode("1111"));
        createSpecificItemsDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(makeItemsFreeAction));

        Item item1 = createItem("7777", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 9000, 2);
        Item item3 = createItem("2222", "Prod1", "123", "Dep1", 10000, 2);

        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2, item3), "29000", "-9000");
    }

}
