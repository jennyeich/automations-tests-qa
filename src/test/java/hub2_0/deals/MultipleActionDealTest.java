package hub2_0.deals;

import com.google.common.collect.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.base.categories.hub2Categories.hub2Deals;
import hub.base.categories.hub2Categories.hub2Sanity;
import hub.common.objects.member.NewMember;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.common.actions.dealActions.*;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.utils.HubMemberCreator;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseHub2DealTest;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.GetMemberBenefitsResponse;
import server.v2_8.common.Item;
import server.v4.GetBenefitsResponse;
import server.v4.common.models.PurchaseItem;

import java.util.ArrayList;
import java.util.List;

public class MultipleActionDealTest extends BaseHub2DealTest {

    public static Box box;


    @BeforeClass
    public static void createNewBox() throws Exception {
        box = dealBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    @Test
    @Category( {hub2Sanity.class, hub2Deals.class} )
    public void testEntireTicketDiscount10percentAndSendItemCodeWithMemberCondition() throws Exception {
        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.AllowEmail.toString()).toggleBooleanValue(ActivityCondition.BOOLEAN_YES);
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = buildPercentOffDiscountOnEntireTicket("10");
        SendCodeToPOSAction sendCodeToPOSAction = buildSendCodeToPOS(SendCodeToPOSAction.CodeType.ITEM_CODE, "1234");
        createEntireTicketDiscountDeal(box, Lists.newArrayList(globalCondition), Lists.newArrayList(percentOffDiscountEntireTicketAction, sendCodeToPOSAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 9000, 2);
        GetMemberBenefitsResponse getMemberBenefitsResponse = getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "12000", "-1200");
        Assert.assertEquals("Item code is not as expected.", "1234", getMemberBenefitsResponse.getItemCodes().get(0).getCode());
    }


    @Test
    @Category({Hub2Regression.class})
    public void testEntireTicketDiscountAmountOffAndSendDealCode() throws Exception {
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("130");
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket("20");
        SendCodeToPOSAction sendCodeToPOSAction = buildSendCodeToPOS(SendCodeToPOSAction.CodeType.DEAL_CODE, "5678");
        createEntireTicketDiscountDeal(box, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(amountOffDiscountEntireTicketAction, sendCodeToPOSAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 7000, 2);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 6000, 2);

        NewMember newMember = HubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        GetMemberBenefitsResponse getMemberBenefitsResponseNegativeTest = getMemberBenefits(newMember.getPhoneNumber(), "7000", Lists.newArrayList(item1));
        Assert.assertEquals("Verify discount's sum is as expected", null, getMemberBenefitsResponseNegativeTest.getTotalSumDiscount());

        timestamp = String.valueOf(System.currentTimeMillis());
        GetMemberBenefitsResponse getMemberBenefitsResponse = getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "13000", "-2000");
        Assert.assertEquals("Item code is not as expected.", "5678", getMemberBenefitsResponse.getDealCodes().get(0).getCode());
    }

    @Test
    @Category({Hub2Regression.class})
    public void testSpecificItemsAmountOffAndPercentOffDiscounts() throws Exception {
        AmountOffDiscountAction amountOffDiscountAction = buildAmountOffDiscount("5", dealBuilder.selectItemsByDepartmentCode("Drink"));
        PercentOffDiscountAction percentOffDiscountAction = buildPercentOffDiscount("10", dealBuilder.selectItemsByItemCode("111"));
        createSpecificItemsDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(amountOffDiscountAction, percentOffDiscountAction));

        Item item1 = createItem("111", "item1", "Drink", "Dep1", 3000, 1);
        Item item2 = createItem("222", "item2", "Drink", "Dep1", 4000, 1);
        Item item3 = createItem("111", "item3", "Food", "Dep2", 5000, 1);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2, item3), "12000", "-1800");
    }


    @Test
    public void testSpecificItemsDiscountsTripleDiscountActions() throws Exception {
        PercentOffDiscountAction percentOffDiscountAction = buildPercentOffDiscount("10", dealBuilder.selectItemsByItemCode("111"));
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction("10", dealBuilder.selectItemsByItemCode("666"));
        MakeItemsFreeAction makeItemsFreeAction = buildMakeItemFreeAction(dealBuilder.selectItemsByItemCode("555"));
        createSpecificItemsDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(percentOffDiscountAction, setSpecialPriceAction, makeItemsFreeAction));

        Item item1 = createItem("111", "item1", "Drink", "Dep1", 3000, 1);
        Item item2 = createItem("222", "item2", "Drink", "Dep1", 4000, 1);
        Item item3 = createItem("555", "item3", "Food", "Dep2", 5000, 1);
        Item item4 = createItem("666", "item4", "Food", "Dep2", 7000, 1);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2, item3, item4), "19000", "-11300");
    }


    @Test
    @Category({Hub2Regression.class})
    public void testSpecificItemsFreeItemAndSpecialPriceWithCondition() throws Exception {
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setConditionValue("4");

        MakeItemsFreeAction makeItemsFreeAction = buildMakeItemFreeAction(dealBuilder.selectItemsByItemCode("555"));
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction("10", dealBuilder.selectItemsByItemCode("666"));
        createSpecificItemsDiscountDeal(box, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(makeItemsFreeAction, setSpecialPriceAction));

        Item item1 = createItem("555", "item1", "Drink", "Dep1", 3000, 1);
        Item item2 = createItem("666", "item2", "Drink", "Dep1", 9000, 2);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "12000", "-10000");
    }


    @Test
    public void testSpecificItemsFreeItemAndSpecialPriceWithSpecificDatesCondition() throws Exception {
        //Add condition
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();

        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());
        DatePicker datePickerEnd = DatePicker.newDatePicker().setSelectEndDay(DateTimeUtility.getTodayPlusXDays(2));
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime().setDatePickerStart(datePickerStart).setDatePickerEnd(datePickerEnd);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        MakeItemsFreeAction makeItemsFreeAction = buildMakeItemFreeAction(dealBuilder.selectItemsByItemCode("555"));
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction("10", dealBuilder.selectItemsByItemCode("666"));
        createSpecificItemsDiscountDeal(box, Lists.newArrayList(dateTimeGlobalCondition), Lists.newArrayList(makeItemsFreeAction, setSpecialPriceAction));

        Item item1 = createItem("555", "item1", "Drink", "Dep1", 3000, 1);
        Item item2 = createItem("666", "item2", "Drink", "Dep1", 9000, 1);
        Item item3 = createItem("777", "item2", "Drink", "Dep1", 2000, 1);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2, item3), "14000", "-11000");
    }

    @Test
    @Category({Hub2Regression.class})
    public void testEntireTicketDiscount10percentAndSendItemCodeWithMemberCondition_V4() throws Exception {
        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.AllowEmail.toString()).toggleBooleanValue(ActivityCondition.BOOLEAN_YES);
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = buildPercentOffDiscountOnEntireTicket("10");
        SendCodeToPOSAction sendCodeToPOSAction = buildSendCodeToPOS(SendCodeToPOSAction.CodeType.ITEM_CODE, "1234");
        createEntireTicketDiscountDeal(box, Lists.newArrayList(globalCondition), Lists.newArrayList(percentOffDiscountEntireTicketAction, sendCodeToPOSAction));

        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("1111", 1, 3000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("2222", 2, 9000, "depCode", "depName"));

        GetBenefitsResponse getBenefitsResponse = performPurchaseV4(items, 12000, -1200);
        Assert.assertEquals("1234", getBenefitsResponse.getDeals().get(0).getBenefits().get(1).getCode());
        Assert.assertEquals("itemCode", getBenefitsResponse.getDeals().get(0).getBenefits().get(1).getType());
    }

}
