package hub2_0.deals;

import com.google.common.collect.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.common.RegistrationField;
import hub.common.objects.member.NewMember;
import hub.hub1_0.services.OperationService;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountEntireTicketAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountEntireTicketAction;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.MemberRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub.utils.HubMemberCreator;
import integration.base.BaseHub2DealTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.common.Item;

import java.util.Calendar;

public class EntireTicketDealTest extends BaseHub2DealTest {

    public static Box box;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = dealBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };


    @Test
    public void testEntireTicketDiscountAmountOffWithMembershipConditionOfCustomTextFieldWithDropDownList() throws Exception {

        OperationService.createRegistrationFormWithCustomField("Option 1", "Option 1", "Option 2", "Option 2", RegistrationField.CustomText2);
        log4j.info("new field was added" + RegistrationField.CustomText2);

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberRegistrationFormFields.GenericString2.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionSelectValue("Option 1", "Option 2");

        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        String amountDiscount = "50";
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount);
        createEntireTicketDiscountDeal(box, Lists.newArrayList(globalCondition), Lists.newArrayList(amountOffDiscountEntireTicketAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 8000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 10000, 2);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "18000", "-5000");
    }

    @Test
    public void testEntireTicketDiscountAmountOffWithMembershipConditionOfCustomIntegerFieldWithDropDowånList() throws Exception {

        OperationService.createRegistrationFormWithCustomField("3", "3", "2", "2", RegistrationField.GenericInteger2);
        log4j.info("new field was added" + RegistrationField.GenericInteger2);

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberRegistrationFormFields.GenericInteger2.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionSelectValue("2");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        String amountDiscount = "50";
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount);
        createEntireTicketDiscountDeal(box, Lists.newArrayList(globalCondition), Lists.newArrayList(amountOffDiscountEntireTicketAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 8000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 10000, 2);
        NewMember newMember = HubMemberCreator.buildMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        newMember.setGenericInteger2("2");
        MembersService.createNewMember(newMember);
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1, item2), "18000", "-5000");
    }

    @Test
    public void testEntireTicketDiscountAmountOffWithBirthdayMonthIsOneOfMembershipCondition() throws Exception {

        Calendar calendar = Calendar.getInstance();
        int currentMonth = calendar.get(Calendar.MONTH);
        currentMonth++;
        String newCurrentMonth = toString().valueOf(currentMonth);

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.BirthdayMonth.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionSelectValue(newCurrentMonth);
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        String amountDiscount = "50";
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount);
        createEntireTicketDiscountDeal(box, Lists.newArrayList(globalCondition), Lists.newArrayList(amountOffDiscountEntireTicketAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 8000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 10000, 2);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "18000", "-5000");
    }


    @Test
    public void testEntireTicketDiscountAmountOffWithCondition() throws Exception {
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("100");
        String amountDiscount = "50";
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount);
        createEntireTicketDiscountDeal(box, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(amountOffDiscountEntireTicketAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 8000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 10000, 2);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "18000", "-5000");
    }

    @Test
    @Category({Hub2Regression.class})
    public void testEntireTicketDiscountAmountOffWithExclude() throws Exception {

        String amountDiscount = "50";
        SelectItems excludeItems = dealBuilder.selectItemsByItemCode("2222");
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount, excludeItems);
        createEntireTicketDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(amountOffDiscountEntireTicketAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 10000, 2);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "13000", "-3000");
    }

    @Test
    public void testEntireTicketDiscount50percentOff() throws Exception {
        String percentDiscount = "50";
        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = buildPercentOffDiscountOnEntireTicket(percentDiscount);
        createEntireTicketDiscountDeal(box, Lists.newArrayList(), Lists.newArrayList(percentOffDiscountEntireTicketAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 90000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 50000, 2);
        getMemberBenefitsAndVerifyDiscount(Lists.newArrayList(item1, item2), "140000", "-70000");
    }


}
