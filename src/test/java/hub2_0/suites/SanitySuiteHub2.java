package hub2_0.suites;

import hub.base.BaseHubTest;
import hub.base.categories.hub1Categories.hub1Sanity;
import hub.base.categories.hub2Categories.hub2Sanity;
import integration.base.BaseIntegrationTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.IOException;
import org.apache.log4j.Logger;


@RunWith(Categories.class)

@Categories.IncludeCategory( hub2Sanity.class)
/*
this class allow to create suites which contains tests from different classes by using the Category lables.
 */
@Suite.SuiteClasses({
        hub2_0.deals.MultipleActionDealTest.class,
        hub2_0.actions.SendBenefitActionTest.class,
        hub2_0.actions.PunchAPunchCardActionTest.class,
        hub2_0.templates.TemplatesManagerTest.class
        })


public class SanitySuiteHub2 {

    public static final Logger log4j = Logger.getLogger(SanitySuiteHub2.class);

    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseIntegrationTest.initSuite(true);
    }


    @AfterClass
    public static void tearDown(){
        log4j.info("close browser after finish all Suite tests");
        BaseIntegrationTest.cleanBase();
        BaseHubTest.cleanHub();
    }


}




