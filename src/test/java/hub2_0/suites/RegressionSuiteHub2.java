package hub2_0.suites;

import hub.base.BaseHubTest;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub2_0.actions.SendBenefitActionTest;
import hub2_0.actions.pointsAccumulations.PointsAccumulationActionTest1;
import hub2_0.actions.pointsAccumulations.PointsAccumulationMultipleMembersTest;
import hub2_0.activityActions.ActivityActionsTest;
import hub2_0.deals.MultipleActionDealTest;
import hub2_0.templates.TemplatesManagerTest;
import hub2_0.triggers.*;
import integration.base.BaseIntegrationTest;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.experimental.categories.Categories;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import java.io.IOException;

@RunWith(Categories.class)

@Categories.IncludeCategory( {Hub2Regression.class} )
/*
this class run suites which contains web tests regarding hub2
 */
@Suite.SuiteClasses({
        MultipleActionDealTest.class,
        SendBenefitActionTest.class,
        TemplatesManagerTest.class,
        MakesAPurchaseTriggerTest.class,
        MakesAPurchaseTriggerTest2.class,
        PaysWithPointsAtPOSTrigger.class,
        OverflowsPunchcardTriggerTest.class,
        SplitCasesTest.class,
        JoinTheClubTriggerTest.class,
        ActivityActionsTest.class,
        PointsAccumulationActionTest1.class,
        PointsAccumulationMultipleMembersTest.class,
        UpdateMembershipDetailsTriggerTest.class


})
public class RegressionSuiteHub2 {

    public static final Logger log4j = Logger.getLogger(RegressionSuiteHub2.class);

    @BeforeClass
    public static void setup() throws IOException {
        // common initialization done once for all tests
        BaseIntegrationTest.initSuite(true);
    }


    @AfterClass
    public static void tearDown(){
        log4j.info("close browser after finish all Suite tests");
        BaseIntegrationTest.cleanBase();
        BaseHubTest.cleanHub();
    }
}
