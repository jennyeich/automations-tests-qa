package hub2_0.settings;

import com.beust.jcommander.internal.Lists;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.operations.AppSettingsPage;
import hub.common.objects.operations.CountryCode_APP_SETTINGS;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.services.Navigator;
import hub.hub1_0.services.OperationService;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Deal;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.PointsAccumulationAction;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountEntireTicketAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.builders.DealBuilder;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.Monetary;
import hub.hub2_0.common.conditions.PaysWithPointsAtPOSCondition;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import integration.base.BaseHub2DealTest;
import integration.base.BaseRuleHub2Test;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.List;

public class CurrencyTest extends BaseRuleHub2Test{

    public static final String ISO_CODE_PROPERTY = "IsoCode";
    private static Box box;
    public static final Logger log4j = Logger.getLogger(CurrencyTest.class);

    public static DealBuilder dealBuilder = new DealBuilder();


    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {


        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };


    @Test
    @Category(Hub2Regression.class)
    public void testUS_CurrencyInMadePurchaseCondition() throws Exception {

        //set countryCode
        changeCountryCode(CountryCode_APP_SETTINGS.US);
        log4j.info("Set currency to : " + CountryCode_APP_SETTINGS.US.toString());

        tag = ActionsBuilder.TAG + timestamp.toString();

        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("10");
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        LoyaltyService.getRuleID(rule.getActivityName());

        checkCurrencyLabel(Currency.US,madeAPurchaseCondition);
    }

    @Test
    public void testUK_CurrencyInMadePurchaseAddPointsByRateAction() throws Exception {

        //set countryCode
        changeCountryCode(CountryCode_APP_SETTINGS.UK);
        log4j.info("Set currency to : " + CountryCode_APP_SETTINGS.UK.toString());

        tag = ActionsBuilder.TAG + timestamp.toString();

        //Create new rule
        Rule rule = (hub.hub2_0.basePages.loyalty.Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add action
        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();
        PointsAccumulationAction pointsAccumulationAction = ruleBuilder.getAccumulationActionByRate("10","3.33",  selectItems, null);
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(pointsAccumulationAction));
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        LoyaltyService.getRuleID(rule.getActivityName());

        checkCurrencyLabel(Currency.GB,pointsAccumulationAction);
    }

    @Test
    public void testIL_Currency2PlacesWithCurrency() throws Exception {

        //set countryCode
        changeCountryCode(CountryCode_APP_SETTINGS.IL);
        log4j.info("Set currency to : " + CountryCode_APP_SETTINGS.IL.toString());
        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (hub.hub2_0.basePages.loyalty.Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("10");

        //Add action
        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();
        PointsAccumulationAction pointsAccumulationAction = ruleBuilder.getAccumulationActionByRate("10","3.33",  selectItems, null);
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(pointsAccumulationAction));
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        LoyaltyService.getRuleID(rule.getActivityName());

        Assert.assertTrue("One of currency label is not as expected: " + Currency.IL.getSign(),pointsAccumulationAction.checkIfAllCurrencyLabelsCurrect(Currency.IL.getSign(),2));
        checkDataStore(Currency.IL);
    }

    @Test
    public void testTR_CurrencyInPaysWithPointsAtPos() throws Exception {

        //set countryCode
        changeCountryCode(CountryCode_APP_SETTINGS.TR);
        log4j.info("Set currency to : " + CountryCode_APP_SETTINGS.TR.toString());

        tag = ActionsBuilder.TAG + timestamp.toString();
        ActivityAction action = ActionsBuilder.buildTagMemberAction(tag);
        //Create new rule
        Rule rule = (hub.hub2_0.basePages.loyalty.Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        PaysWithPointsAtPOSCondition paysWithPointsAtPOSCondition = new PaysWithPointsAtPOSCondition();
        paysWithPointsAtPOSCondition.setConditionKey(PaysWithPointsAtPOSCondition.ConditionKey.PAYMENT_AMOUNT)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("300");
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.PaysWithPointsAtPOS.toString(), box, rule, Lists.newArrayList(paysWithPointsAtPOSCondition), Lists.newArrayList(action));
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        LoyaltyService.getRuleID(rule.getActivityName());

        checkCurrencyLabel(Currency.TR,paysWithPointsAtPOSCondition);
    }

    @Test
    @Category(Hub2Regression.class)
    public void testUK_CheckDealEntireTicketDiscountAmountOff() throws Exception {

        //set countryCode
        changeCountryCode(CountryCode_APP_SETTINGS.UK);
        log4j.info("Set currency to : " + CountryCode_APP_SETTINGS.UK.toString());

        tag = ActionsBuilder.TAG + timestamp.toString();
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = BaseHub2DealTest.buildAmountOffDiscountOnEntireTicket("50");
        Deal deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ActivityStatus.Active, "entireTicketDeal_");
        Activity activity = dealBuilder.buildEntireTicketDiscountActivity(box, deal, Lists.newArrayList(), Lists.newArrayList(amountOffDiscountEntireTicketAction));

        LoyaltyService.createActivityInBox(activity);

        LoyaltyService.getDealID(activity.getActivityName());

        checkCurrencyLabel(Currency.GB,amountOffDiscountEntireTicketAction);
    }

    @Test
    public void testAE_CheckDealWith2PlacesWithCurrency() throws Exception {

        //set countryCode
        changeCountryCode(CountryCode_APP_SETTINGS.AE);
        log4j.info("Set currency to : " + CountryCode_APP_SETTINGS.AE.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("100");


        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = BaseHub2DealTest.buildAmountOffDiscountOnEntireTicket("50");
        Deal deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ActivityStatus.Active, "entireTicketDeal_");
        Activity activity = dealBuilder.buildEntireTicketDiscountActivity(box, deal, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(amountOffDiscountEntireTicketAction));

        LoyaltyService.createActivityInBox(activity);

        LoyaltyService.getDealID(activity.getActivityName());

        Assert.assertTrue("One of currency label is not as expected: " + Currency.AE.getSign(),madeAPurchaseCondition.checkIfAllCurrencyLabelsCurrect(Currency.AE.getSign(),2));
        checkDataStore(Currency.AE);
    }

    @Test
    public void testUK_CheckCurrencyPreview() throws Exception {

        //set countryCode
        changeCountryCode(CountryCode_APP_SETTINGS.UK);
        log4j.info("Set currency to : " + CountryCode_APP_SETTINGS.UK.toString());

        tag = ActionsBuilder.TAG + timestamp.toString();

        //Create new rule
        Rule rule = (hub.hub2_0.basePages.loyalty.Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add action
        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();
        PointsAccumulationAction pointsAccumulationAction = ruleBuilder.getAccumulationActionByRate("10","3.33",  selectItems, null);
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(pointsAccumulationAction));
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        LoyaltyService.getRuleID(rule.getActivityName());

        //need to navigate to campaign tab
        Navigator.contentTab();
        Navigator.loyaltyTabPage();
        Assert.assertTrue("Currency preview is not as expected: " + Currency.GB.getSign(),pointsAccumulationAction.checkIfAllCurrencyPreviewCorrect(Currency.GB.getSign()));
    }

    private void checkCurrencyLabel(Currency currency, Monetary monetary) {
       Assert.assertEquals("Currency label is not as expected: " + currency.getSign(),currency.getSign(),monetary.getCurrency());
       checkDataStore(currency);
    }

    //check that the 'LocationBackend' contains the correct isoCode as we expect
    private void checkDataStore(Currency currency){
        Query q = new Query("LocationBackend");
        q.setFilter(new Query.FilterPredicate("LocationID", Query.FilterOperator.EQUAL, locationId));
        List<Entity> results = dataStore.findByQuery(q);
        Assert.assertTrue(results.size() == 1);
        Entity entity = results.get(0);
        Assert.assertEquals("IsoCode is not as expected: " + currency.toString(),currency.toString(),entity.getProperty(ISO_CODE_PROPERTY));
    }

    private  void changeCountryCode(CountryCode_APP_SETTINGS countryCode) throws Exception {
        AppSettingsPage appSettingsPage = new AppSettingsPage();
        appSettingsPage.setCountryCode(countryCode.toString());
        OperationService.updateAppSettings(appSettingsPage);


    }


    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.MadeAPurchase;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.MadeAPurchase.toString() + "_" + timestamp;
    }
}
