package hub2_0.settings;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import common.BaseTest;
import hub.base.BaseService;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.common.Tags;
import hub.common.objects.operations.PermissionSettingsPage;
import hub.common.objects.operations.PointsCreditSettingsPage;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.services.Navigator;
import hub.hub1_0.services.OperationService;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.basePages.loyalty.cases.RuleCase;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.PointsAccumulationAction;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.ActivityType;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.common.triggers.When;
import hub.hub2_0.services.loyalty.BaseHub2Service;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.AcpService;
import integration.base.BaseRuleHub2Test;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;

public class POSAuthorizationTest extends BaseRuleHub2Test {

    private static Box box;
    private static String businessPlanName = null;

    @BeforeClass
    public static void createNewBox() throws Exception {
        BaseTest.sqlDB.setHub2Wallet(resolveLocationId(locationId), PointsCreditSettingsPage.BusinessWallet.Both.dbValue());
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    @After
    public void deleteBusinessPlan() throws Exception {
        PermissionSettingsPage permissionSettingsPageDeletePlan = new PermissionSettingsPage();
        permissionSettingsPageDeletePlan.setDeleteBusinessPlan(businessPlanName);
        OperationService.updatePermissionSettings(permissionSettingsPageDeletePlan);
        AcpService.deleteBusinessPlan(businessPlanName);
    }


    @Test
    public void testBudgetTriggersAuthorization() throws Exception {
        businessPlanName = "PayWithBudgetTriggers" + timestamp;
        AcpService.addBusinessPlan(businessPlanName, Lists.newArrayList(Tags.HUB2_BUDGET_TYPE_TRIGGERS.toString()));

        PermissionSettingsPage permissionSettingsPage = new PermissionSettingsPage();
        permissionSettingsPage.setBusinessPlan(businessPlanName);
        permissionSettingsPage.clickAddBusinessPlan();
        OperationService.updatePermissionSettings(permissionSettingsPage);


        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.MadeAPurchase.toString(), "some description");
        Rule activity = (Rule) ruleBuilder.buildActivity(null, box, rule, Collections.emptyList(), Collections.emptyList());
        When when = new When();
        when.clickWhen();
        activity.setWhen(when);

        LoyaltyService.createActivityInBox(activity);
        checkElementDisabled((MessageFormat.format(When.SELECT_TRIGGER, ActivityTrigger.PaysWithPointsAtPOS.toString())));
        checkElementDisabled((MessageFormat.format(When.SELECT_TRIGGER, ActivityTrigger.PaysWithCreditAtPOS.toString())));
        checkElementDisabled((MessageFormat.format(When.SELECT_TRIGGER, ActivityTrigger.PaysWithAppCreditCard.toString())));
        checkElementDisabled((MessageFormat.format(When.SELECT_TRIGGER, ActivityTrigger.AppCreditCardPaymentIsDeclined.toString())));

        rule.clickCancel();
    }

    @Test
    @Category({Hub2Regression.class})
    public void testRulePurchaseBasketAuthorization() throws Exception {
        businessPlanName = "rulePurchaseBasket" + timestamp;
        AcpService.addBusinessPlan(businessPlanName, Lists.newArrayList(Tags.HUB2_RULE_PURCHASE_BASKET.toString()));

        PermissionSettingsPage permissionSettingsPage = new PermissionSettingsPage();
        permissionSettingsPage.setBusinessPlan(businessPlanName);
        permissionSettingsPage.clickAddBusinessPlan();
        OperationService.updatePermissionSettings(permissionSettingsPage);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.MadeAPurchase.toString(), "some description");
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(), box, rule, Lists.newArrayList(new MadeAPurchaseCondition()), getPointsAccumulationActions());
        LoyaltyService.createActivityInBox(activity);

        BaseHub2Service.scrollUp();
        String clickConditionKey = MessageFormat.format(MadeAPurchaseCondition.CONDITION_KEY.substring(0, MadeAPurchaseCondition.CONDITION_KEY.lastIndexOf('.')) + "\"]", "0");
        BaseService.getDriver().findElement(By.xpath(clickConditionKey)).click();
        checkElementDisabled(MessageFormat.format(MadeAPurchaseCondition.CONDITION_KEY, 0, MadeAPurchaseCondition.ConditionKey.SHOPPING_CART));
        checkElementDisabled(MessageFormat.format(MadeAPurchaseCondition.CONDITION_KEY, 0, MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS));

        checkPointsAccumulation(ActivityAction.ACTIVITY_PREFIX, 0);
        checkPointsAccumulation(ActivityAction.ACTIVITY_PREFIX, 1);
        checkElementDisabled(MessageFormat.format(BaseService.HUB2_AUTOMATION_ID_PATTERN, (MessageFormat.format(Case.PER_OCCURRENCE_MODE, ActivityAction.ACTIVITY_PREFIX))));

        rule.clickCancel();
    }

    @Test
    public void testRuleSplitCasesPurchaseBasketAuthorization() throws Exception {
        businessPlanName = "rulePurchaseBasket" + timestamp;
        AcpService.addBusinessPlan(businessPlanName, Lists.newArrayList(Tags.HUB2_RULE_PURCHASE_BASKET.toString()));

        PermissionSettingsPage permissionSettingsPage = new PermissionSettingsPage();
        permissionSettingsPage.setBusinessPlan(businessPlanName);
        permissionSettingsPage.clickAddBusinessPlan();
        OperationService.updatePermissionSettings(permissionSettingsPage);

        Case aCase = new RuleCase();
        aCase.setActions(Lists.newArrayList((ActivityAction) getPointsAccumulationActions().get(1)));

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.MadeAPurchase.toString(), "some description");
        Activity activity = ruleBuilder.buildActivityWithCases(ActivityTrigger.MadeAPurchase.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(aCase));
        LoyaltyService.createActivityInBox(activity);

        WebElement perOccurrences = BaseService.findElementByAutomationId(MessageFormat.format(Case.PER_OCCURRENCE_MODE, "activity.cases.0."));
        ((JavascriptExecutor) BaseService.getDriver()).executeScript("arguments[0].scrollIntoView(true);", perOccurrences);

        checkElementDisabled(MessageFormat.format(BaseService.HUB2_AUTOMATION_ID_PATTERN, (MessageFormat.format(Case.PER_OCCURRENCE_MODE, "activity.cases.0."))));
        checkPointsAccumulation("activity.cases.0.", 0);

        WebElement caseOrderUp = BaseService.getDriver().findElement(By.xpath(MessageFormat.format(BaseService.HUB2_AUTOMATION_ID_PATTERN, MessageFormat.format(Case.ORDER_UP, "activity.cases.0."))));
        ((JavascriptExecutor) BaseService.getDriver()).executeScript("arguments[0].scrollIntoView(true);", caseOrderUp);

        rule.clickCancel();
    }

    @Test
    public void testPOSAuthorizationWhenSwitchingLocation() throws Exception {
        businessPlanName = "POSAuthorization" + timestamp;
        AcpService.addBusinessPlan(businessPlanName, Lists.newArrayList(Tags.HUB2_BUDGET_TYPE_TRIGGERS.toString()));

        PermissionSettingsPage permissionSettingsPage = new PermissionSettingsPage();
        permissionSettingsPage.setBusinessPlan(businessPlanName);
        permissionSettingsPage.clickAddBusinessPlan();
        OperationService.updatePermissionSettings(permissionSettingsPage);


        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.MadeAPurchase.toString(), "some description");
        Rule activity = (Rule) ruleBuilder.buildActivity(null, box, rule, Collections.emptyList(), Collections.emptyList());
        When when = new When();
        when.clickWhen();
        activity.setWhen(when);

        LoyaltyService.createActivityInBox(activity);
        checkElementDisabled((MessageFormat.format(When.SELECT_TRIGGER, ActivityTrigger.PaysWithPointsAtPOS.toString())));
        rule.clickCancel();

        // Check That in other location the "pay with budget" triggers are enabled.
        Navigator.selectApplicationFromEnv();
        LoyaltyService.createActivityInBox(activity);

        WebElement element = BaseService.getDriver().findElement(By.xpath(MessageFormat.format(When.SELECT_TRIGGER, ActivityTrigger.PaysWithPointsAtPOS.toString())));
        String classes = element.getAttribute("class");
        Assert.assertFalse(classes.contains("disabled"));
        rule.clickCancel();

        // back to the original location- triggers still disabled.
        Navigator.selectApplicationByLocationId(locationId);
        LoyaltyService.createActivityInBox(activity);
        checkElementDisabled((MessageFormat.format(When.SELECT_TRIGGER, ActivityTrigger.PaysWithPointsAtPOS.toString())));
        rule.clickCancel();
    }

    private List getPointsAccumulationActions() {
        PointsAccumulationAction byPercentagePointsAccumulation = new PointsAccumulationAction();
        byPercentagePointsAccumulation.clickPointsAccumulationBtn();
        PointsAccumulationAction byRatePointsAccumulation = new PointsAccumulationAction();
        byRatePointsAccumulation.clickPointsAccumulationBtn();
        byRatePointsAccumulation.setAccumulationType(PointsAccumulationAction.AccumulationType.ACCUMULATE_BY_RATE);
        return Lists.newArrayList(byPercentagePointsAccumulation, byRatePointsAccumulation);
    }

    private void checkPointsAccumulation(String prefix, int index) throws Exception {
        String excludeXpath = MessageFormat.format(BaseService.HUB2_AUTOMATION_ID_PATTERN, "disabled-exclude-points-container");
        checkElementDisabled(excludeXpath);
        String selectXpath = MessageFormat.format(PointsAccumulationAction.SELECT_ITEMS_BUTTON, prefix, index);
        BaseService.getDriver().findElement(By.xpath(selectXpath)).click();
        checkSelectItems(index);
    }

    private void checkSelectItems(int index) throws Exception {
        String xpathSuffix = "/div/div";
        checkElementDisabled(MessageFormat.format(SelectItems.ITEM_CODES + xpathSuffix, index));
        checkElementDisabled(MessageFormat.format(SelectItems.DEPARTMENT_CODES + xpathSuffix, index));
        checkElementDisabled(MessageFormat.format(SelectItems.PREDEFINED_GROUPS + xpathSuffix, index));
        checkElementDisabled(MessageFormat.format(SelectItems.ADVANCED_RULE + xpathSuffix, index));

        BaseService.getDriver().findElement(By.xpath(MessageFormat.format(SelectItems.FINISH_ACTION_BUTTON, "apply"))).click();
    }

    private void checkElementDisabled(String xpath) throws Exception {
        WebElement element = BaseService.getDriver().findElement(By.xpath(xpath));
        String classes = element.getAttribute("class");
        Assert.assertTrue(classes.contains("disabled") || classes.contains("disabled-exclude-points-container"));
        WebElement innerElement;
        try {
            innerElement = BaseService.getDriver().findElement(By.xpath(xpath + "//*[contains(@data-automation-id, 'disabled-icon')]"));
        } catch (Exception e) {
            // get inner element on per-occurrences element
            innerElement = BaseService.getDriver().findElement(By.xpath(xpath + "//following::i" +
                    "[contains(@data-automation-id, 'disabled-icon')]"));
        }
        String iconClasses = innerElement.getAttribute("class");
        Assert.assertTrue(iconClasses.contains("como-ic-unavailable"));
        Assert.assertTrue(iconClasses.contains("como-ic-unavailable-tooltip-trigger"));
        //hover and display the tool-tip.
        BaseService.scrollToElement(innerElement);
        Thread.sleep(2000);
        Assert.assertTrue(BaseService.findElementByAutomationId("disabled-tooltip.pos").isDisplayed());
    }

    @Override
    public ITrigger getTrigger() {
        return null;
    }

    @Override
    public String getAutomationName() {
        return null;
    }
}
