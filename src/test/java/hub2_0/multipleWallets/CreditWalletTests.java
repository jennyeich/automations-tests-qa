package hub2_0.multipleWallets;

import com.google.common.collect.Lists;
import common.BaseTest;
import hub.common.objects.member.NewMember;
import hub.common.objects.operations.PointsCreditSettingsPage;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AmountType;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.PointsAccumulationAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.enums.ActivityStatus;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.ActivityType;
import hub.hub2_0.common.enums.SelectedItemsDialogButtons;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.actions.BaseActivityActionTest;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import server.v2_8.SubmitPurchaseResponse;
//import server.v4.SubmitPurchaseResponse;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

public class CreditWalletTests extends BaseActivityActionTest {

    @BeforeClass
    public static void createNewBox() throws Exception {
        BaseTest.sqlDB.setHub2Wallet(resolveLocationId(locationId), PointsCreditSettingsPage.BusinessWallet.Budget.dbValue());
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }


    //C10634
    @ru.yandex.qatools.allure.annotations.Description("Check Credit wallet - run automation of accumulate by rate verify that points transaction is in points")
    @Test
    public void testCreditWalletWithPointsAccumulateByRateWithShoppingCartCondition() throws Exception {

        timestamp = String.valueOf(System.currentTimeMillis());
        tag =  ActivityTrigger.MadeAPurchase.toString() + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, tag);

        PointsAccumulationAction byRatePointsAccumulation = new PointsAccumulationAction();
        byRatePointsAccumulation.clickPointsAccumulationBtn();
        byRatePointsAccumulation.setAccumulationType(PointsAccumulationAction.AccumulationType.ACCUMULATE_BY_RATE);
        byRatePointsAccumulation.setNumOfPoints("10");
        byRatePointsAccumulation.setPointsRate("100");
        byRatePointsAccumulation.clickSelectItemsButton();

        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        byRatePointsAccumulation.setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity( ActivityTrigger.MadeAPurchase.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(byRatePointsAccumulation));
        LoyaltyService.createActivityInBox(activity);

        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        NewMember newMember = createNewMember();

        Item itemA = createItem("111","itemA","depA", "department1", 200000);
        Item itemB = createItem("222","itemB","depB", "department2", 300000);
        SubmitPurchaseAndValidate(newMember, Lists.newArrayList(itemA, itemB), "50000", "500000",AmountType.Credit, true);
    }

    private SubmitPurchaseResponse SubmitPurchaseAndValidate(NewMember newMember, ArrayList<Item> items, String pointsAmount, String totalSum,AmountType amountType, boolean isPointsAccumulated) throws Exception{
        log4j.info("Going to submit a purchase with total sum of:" + totalSum);
        String transactionId = UUID.randomUUID().toString();
        SubmitPurchaseResponse submitPurchaseResponse =performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), totalSum,  Lists.newArrayList(), transactionId, items);

        Thread.sleep(5000);
        if(isPointsAccumulated)
            checkDataStoreUpdatedWithPointsByRuleID(pointsAmount, amountType);
        else
            checkDataStoreNegativeCheckByRuleID(POINTSTRASACTION);

        return submitPurchaseResponse;
    }


    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }


    @Override
    protected ActivityAction getConcreteAction() throws Exception {
        return ActionsBuilder.buildPointsAccumulationAction("10");
    }



}
