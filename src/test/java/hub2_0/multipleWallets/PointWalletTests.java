package hub2_0.multipleWallets;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Text;
import com.google.common.collect.Lists;
import common.BaseTest;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.NewMember;
import hub.common.objects.operations.PointsCreditSettingsPage;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AmountType;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.PointsAccumulationAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.enums.ActivityStatus;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.ActivityType;
import hub.hub2_0.common.enums.SelectedItemsDialogButtons;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.actions.BaseActivityActionTest;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.common.LocationBackendFields;
import server.common.PaymentType;
import org.junit.experimental.categories.Category;
import server.common.*;
import server.utils.ServerHelper;
import server.v2_8.CancelPurchase;
import server.v2_8.CancelPurchaseResponse;
import server.v2_8.SubmitPurchaseResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Item;

import java.util.*;

public class PointWalletTests extends BaseActivityActionTest {

    @BeforeClass
    public static void createNewBox() throws Exception {
        BaseTest.sqlDB.setHub2Wallet(resolveLocationId(locationId), PointsCreditSettingsPage.BusinessWallet.Points.dbValue());
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }


    //C10616
    //C10628
    @ru.yandex.qatools.allure.annotations.Description("check Point wallet based on accumulate fixed amount of points and verify that the transaction and cancellation are in points")
    @Test
    @Category({Hub2Regression.class})
    public void testPointsWalletWithPointsAccumulateByFixAmountWithShoppingCartCondition() throws Exception {
        PointsAccumulationAction pointsAccumulationAction = getAccumulationActionFix("100");
        SelectItems selectItems = new SelectItems();
        selectItems.clickDepartmentCodesRadioButton();
        selectItems.setDepartmentCodes("depA", "depB");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItems);
        occurrencesCondition.setConditionValue("2");
        createRulePerOccurrences(ActivityTrigger.MadeAPurchase.toString(), Collections.emptyList(), Lists.newArrayList(occurrencesCondition), Lists.newArrayList(pointsAccumulationAction));
        //Create member
        NewMember newMember = createNewMember();

        Item itemA = createItem("111","itemA","depA", "department1", 10);
        Item itemB = createItem("222","itemB","depB", "department2", 20);
        SubmitPurchaseResponse submitPurchaseResponse = SubmitPurchaseAndValidate(newMember, Lists.newArrayList(itemA, itemB), "10000", "30",AmountType.Points, true);
        cancelPurchaseAndValidate(submitPurchaseResponse.getConfirmation(), "-10000", AmountType.Points, true);
    }

    //C10631
    @ru.yandex.qatools.allure.annotations.Description("Check Points wallet - run automation of accumulate X% points verify that points transaction is in points")
    @Test
    public void testPointsWalletWithPointsAccumulateByPercentageWithShoppingCartCondition() throws Exception {

        timestamp = String.valueOf(System.currentTimeMillis());
        tag =  ActivityTrigger.MadeAPurchase.toString() + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, tag);

        PointsAccumulationAction pointsAccumulationAction = new PointsAccumulationAction();
        pointsAccumulationAction.clickPointsAccumulationBtn();
        pointsAccumulationAction.setAccumulationType(PointsAccumulationAction.AccumulationType.ACCUMULATE_PERCENTAGE_OF_AMOUNT_SPENT);
        pointsAccumulationAction.setNumOfPoints("10");
        pointsAccumulationAction.clickSelectItemsButton();

        SelectItems selectItems = new SelectItems();
        selectItems.clickAnyItemRadioButton();
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        pointsAccumulationAction.setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity( ActivityTrigger.MadeAPurchase.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(pointsAccumulationAction));
        LoyaltyService.createActivityInBox(activity);

        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        NewMember newMember = createNewMember();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "1000", transactionId);
        SubmitPurchaseAndValidate(newMember, Lists.newArrayList(), "100", "1000",AmountType.Points, true);
    }


    //C10619
    @ru.yandex.qatools.allure.annotations.Description("")
    @Test
    public void testPointsWalletPayWithBudgetCall() throws Exception {

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        setPaymentConfiguration();

        //add points so user can use them
        MembersService.performSmartActionAddPoints("1000",timestamp);
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);


        String usedPoints = "200";
        // perform pay with budget to use members points
        String transactionId = UUID.randomUUID().toString();
        ServerHelper.performAnActionUsePoints(member, usedPoints,transactionId);
        //wait for data store to update
        Thread.sleep(5000);
        checkDataStoreUpdatedWithBudget(transactionId ,AmountType.Points,"-200"  );
    }


    private static void setPaymentConfiguration(){
        serverHelper.saveApiClient(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);

        Map<String, Object> props = new HashMap<>();
        props.put(LocationBackendFields.PAY_WITH_BUDGET_MIN_SUM.key(), "-1");
        props.put(LocationBackendFields.PAY_WITH_BUDGET_MAX_SUM.key(), "-1");
        props.put(LocationBackendFields.PAY_WITH_BUDGET_TYPE.key(), PaymentType.POINTS.type());
        serverHelper.updateBusinessBackend(props);
    }

    private SubmitPurchaseResponse SubmitPurchaseAndValidate(NewMember newMember, ArrayList<Item> items, String pointsAmount, String totalSum,AmountType amountType, boolean isPointsAccumulated) throws Exception{
        String transactionId = UUID.randomUUID().toString();
        SubmitPurchaseResponse submitPurchaseResponse =performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), totalSum,  Lists.newArrayList(), transactionId, items);

        Thread.sleep(5000);
        if(isPointsAccumulated)
            checkDataStoreUpdatedWithPointsByRuleID(pointsAmount, amountType);
        else
            checkDataStoreNegativeCheckByRuleID(POINTSTRASACTION);

        return submitPurchaseResponse;
    }

    private void cancelPurchaseAndValidate(String confirmation, String pointsAmount, AmountType wallet, boolean isPointsAccumulated) throws Exception{
        Thread.sleep(10000);
        String transactionId = UUID.randomUUID().toString();
        CancelPurchase cancelPurchase = new CancelPurchase();
        cancelPurchase.setConfirmation(confirmation);
        cancelPurchase.setTransactionId(transactionId);
        Assert.assertFalse("Confirmation must not be empty!", org.apache.commons.lang3.StringUtils.isEmpty(cancelPurchase.getConfirmation()));

        //Cancel the Purchase via API
        IServerResponse response = cancelPurchase.SendRequestByApiKey(apiKey, CancelPurchaseResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        //Cast to CancelPurchase object to fetch all response data
        CancelPurchaseResponse cancelPurchaseResponse = (CancelPurchaseResponse) response;
        Assert.assertEquals("status code must be 200", (Integer) 200, cancelPurchaseResponse.getStatusCode());

        Thread.sleep(10000);
        if(isPointsAccumulated) {
            log4j.info("check Data Store Updated with points");

            List<Entity> res = ServerHelper.queryWithWaitUntilResultEqOrGreat("Must find at least 2 UserAction ",
                    buildQueryWithSort(POINTSTRASACTION) , 2, dataStore, 120000);

            Text t = ((Text)res.get(0).getProperty("Data"));
            log4j.info("Data contains the following data : " +   t.getValue());

            if(AmountType.Points.equals(wallet)){
                Assert.assertTrue("Data must contain the Points : " + pointsAmount, t.getValue().contains("Points=\"" + pointsAmount + "\" "));
            }else{
                Assert.assertTrue("Data must contain the Points : " + pointsAmount, t.getValue().contains("BudgetWeighted=\"" + pointsAmount + "\" "));
            }
        } else {
            checkDataStoreNegativeCheckByRuleID(POINTSTRASACTION);
        }
    }


    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }


    @Override
    protected ActivityAction getConcreteAction() throws Exception {
        return ActionsBuilder.buildPointsAccumulationAction("10");
    }

}
