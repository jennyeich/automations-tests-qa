package hub2_0.activityActions;

import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.services.Navigator;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.common.filters.CampaignFilter;
import hub.hub2_0.services.loyalty.ActivityService;
import hub.hub2_0.services.loyalty.DeleteService;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.concurrent.TimeUnit;

public class ActivityActionsTest extends BaseRuleHub2Test {

    private static Box box;
    private static String boxName;

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            boxName  = "BOX" +  String.valueOf(System.currentTimeMillis());
            try {
                box = ruleBuilder.buildBox(boxName);
                LoyaltyService.createBox(box);
            } catch (Exception e) {
                log4j.error("error while creating box ",e);
            }
        }
    };

    @Test
    // C10544
    @ru.yandex.qatools.allure.annotations.Description("this test verify that it's impossible to change campaign status to archived while it contain active rules")
    public void testBoxArchiveAction() throws Exception {
        CampaignFilter campaignFilter = new CampaignFilter();
        campaignFilter.toggleFilters(false, CampaignFilterView.ARCHIVED_CAMPAIGNS); // set filter to false in order to verify that archive campaign is not appear
        tag = ActionsBuilder.TAG + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(ActivityActions.Tag), tag);
        LoyaltyService.createActivityInBox(activity);
        // attempt to move campaign to Arhcive (should fail since there are active rules inside)
        DeleteService.archiveBox(boxName);
        Assert.assertTrue("Warnning Message of moving box to Archeive should appear",DeleteService.isArchiveWarningWindowsAppear());
        log4j.info("Warning Message of moving box to Archive appear as expected ");
        //stop the activity in order to move the campaign to archive.
        ActivityService.executeOperation(rule.getActivityName(),ItemOperation.STOP);
        DeleteService.archiveBox(boxName);
        Assert.assertFalse("Box <" + boxName +"> moved to Archive and should not appear in the view with the current filters!",LoyaltyService.isBoxExists(boxName));
        campaignFilter.toggleFilters(true, CampaignFilterView.ARCHIVED_CAMPAIGNS);
        Assert.assertTrue("Filters set to show archive Box, Box <" + boxName + "> should appear in the view with the  current filters!",LoyaltyService.isBoxExists(boxName));
        // attempt to activate rule in archeive campaign.
        ActivityService.executeOperation(rule.getActivityName(), ItemOperation.ACTIVATE);
        log4j.info("verify that Confirmation Message of unarchive box appear");
        Assert.assertTrue("Confirmation Message of unarchive box should appear",ActivityService.isConfirmationWindowsAppearAndRespond(SelectedItemsDialogButtons.Cancel));
    }

    @Test
    //C10584
    @ru.yandex.qatools.allure.annotations.Description("this test Move Active rule to another campaign")
    @Category({Hub2Regression.class})
    public void testMoveRuleToAnotherCampaign() throws Exception {
        CampaignFilter campaignFilter = new CampaignFilter();
        campaignFilter.toggleFilters(true, CampaignFilterView.ARCHIVED_CAMPAIGNS);
        tag = ActionsBuilder.TAG + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(ActivityActions.Tag), tag);
        LoyaltyService.createActivityInBox(activity);
        // create another box
        String box2Name  = "BOX" +  String.valueOf(System.currentTimeMillis());
        Box newBox = ruleBuilder.buildBox(box2Name);
        LoyaltyService.createBox(newBox);
        // Add dummy rule into Box
        Rule box2rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity box2activity = ruleBuilder.buildActivity(getTrigger().toString(), newBox, box2rule, Lists.newArrayList(ActivityActions.Tag), box2Name);
        LoyaltyService.createActivityInBox(box2activity);
        int numOfBox2Rules = ActivityService.getNumberOfRulesInBox(box2Name);
        //log4j.info("After getting number of rules");
        log4j.info("Number of rules in <" + box2Name + "> before move rule is:" + numOfBox2Rules );
        Assert.assertEquals("1 rule should be found in the new campaign: <" +  box2Name + ">", 1, numOfBox2Rules);
        ActivityService.moveToActivity(rule.getActivityName(),box2Name);
        numOfBox2Rules = ActivityService.getNumberOfRulesInBox(box2Name);
        int numOfBox1Rules = ActivityService.getNumberOfRulesInBox(boxName);
        log4j.info("Number of rules in <" + box2Name + "> after move rule is:" + numOfBox2Rules );
        Assert.assertEquals("Number of rules in <" + boxName + "> after move rule must be 0", 0, numOfBox1Rules);
        Assert.assertEquals("Number of rules in <" + box2Name + "> after move rule must be 2", 2, numOfBox2Rules);
    }


    @Test
    //C10557
    @ru.yandex.qatools.allure.annotations.Description("This test try to change activity in Archive campaign from stop to active. The test verify that changing it to active cause the campaign to become un-archived")
    public void testArchivedCampaignChangeActivityFromStoppedToActive() throws Exception {
        CampaignFilter campaignFilter = new CampaignFilter();

        tag = ActionsBuilder.TAG + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(ActivityActions.Tag), tag);
        LoyaltyService.createActivityInBox(activity);
        ActivityService.executeOperation(rule.getActivityName(), ItemOperation.STOP);
        DeleteService.archiveBox(boxName);
        campaignFilter.toggleFilters(true, CampaignFilterView.ARCHIVED_CAMPAIGNS);
        ActivityService.executeOperation(rule.getActivityName(), ItemOperation.ACTIVATE);
        log4j.info("Verify that confirmation windows appear and dismiss it");
        ActivityService.isConfirmationWindowsAppearAndRespond(SelectedItemsDialogButtons.Cancel);

        Assert.assertTrue("Archive state should be:Unarchive" , DeleteService.UNARCHIVE_TEXT_SUB_MENU.equalsIgnoreCase(DeleteService.getArchiveState(boxName)));
        ActivityService.executeOperation(rule.getActivityName(), ItemOperation.ACTIVATE);
        ActivityService.isConfirmationWindowsAppearAndRespond(SelectedItemsDialogButtons.Apply);
        Assert.assertTrue("Archive state should be:archive" , DeleteService.ARCHIVE_TEXT_SUB_MENU.equalsIgnoreCase(DeleteService.getArchiveState(boxName)));
    }


    @Test
    //C10577 , C10572 ,C10551
    @ru.yandex.qatools.allure.annotations.Description("This test verify that the filter show/hide the rules according to their state: ACTIVE/STOP ")
    @Category({Hub2Regression.class})
    public void testfilterActiveStopAtivityStatus() throws Exception {
        CampaignFilter campaignFilter = new CampaignFilter();
        tag = ActionsBuilder.TAG + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(ActivityActions.Tag), tag);
        LoyaltyService.createActivityInBox(activity);
        campaignFilter.toggleFilters(false, CampaignFilterView.STOPPED);
        ActivityService.executeOperation(rule.getActivityName(), ItemOperation.STOP);
        log4j.info("Verify that activity doesn't appear in the view after changing <stopped> view filter to <false> ");
        Assert.assertFalse("Activity <" + rule.getActivityName() + "> should not appear since the filter of <Stopped> rule is <false>" ,ActivityService.isActiviyExists(rule.getActivityName()));
        campaignFilter.toggleFilters(true, CampaignFilterView.STOPPED);
        Assert.assertTrue("Rule state appear as <" + ActivityService.getActivityStatus(rule.getActivityName()).toLowerCase() + "> instead of <" +  CampaignFilterView.STOPPED.name().toLowerCase() +">",ActivityService.getActivityStatus(rule.getActivityName()).toLowerCase().equals(CampaignFilterView.STOPPED.name().toLowerCase()));
        log4j.info("Verify that activity appear in the view after changing <stopped> view filter to <true>");
        Assert.assertTrue("Activity <" + rule.getActivityName() + "> should appear since the filter of <Stopped> rule is <true>" ,ActivityService.isActiviyExists(rule.getActivityName()));
        campaignFilter.toggleFilters(false, CampaignFilterView.RULE);
        log4j.info("Verify that activity from type rules doesn't appear in the view after changing <Rule> view filter to <false>");
        Assert.assertFalse("Activity <" + rule.getActivityName() + "> should not appear since the filter of <Rule> is in state of <false> and filter <Stop> is <true>" ,ActivityService.isActiviyExists(rule.getActivityName()));
        campaignFilter.toggleFilters(true, CampaignFilterView.RULE);
        campaignFilter.toggleFilters(false, CampaignFilterView.ACTIVE);
        ActivityService.executeOperation(rule.getActivityName(), ItemOperation.ACTIVATE);
        log4j.info("Verify that activity from type rules doesn't appear in the view when <Rule> state is <true> and <Active> filter in state <false>");
        Assert.assertFalse("Activity <" + rule.getActivityName() + "> should not appear since the filter of <Active> rule is <false>" ,ActivityService.isActiviyExists(rule.getActivityName()));
        campaignFilter.toggleFilters(true, CampaignFilterView.ACTIVE);
        log4j.info("Verify that activity from type rule appear in the view when <Rule> and <Active> filters are in state <true>");
        Assert.assertTrue("Activity <" + rule.getActivityName() + "> should appear since the filter of <Stopped> and <Active> are in state of <true>" ,ActivityService.isActiviyExists(rule.getActivityName()));
    }

    @Test // C10576,C10571

    @ru.yandex.qatools.allure.annotations.Description("This test verify that the filter show/hide  rules with state: completed")
    @Category({Hub2Regression.class})
    public void testfilterCompletedActivity() throws Exception {
        tag = ActionsBuilder.TAG + timestamp.toString();
        CampaignFilter campaignFilter = new CampaignFilter();
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().clickStartNow();
        String timeToSet[] = DateTimeUtility.getTimePlusXMinutes(5);
        TimeSelector.TimeMeridiem currentAmPm = timeToSet[2].toLowerCase().equals(TimeSelector.TimeMeridiem.AM.toString())? TimeSelector.TimeMeridiem.AM:TimeSelector.TimeMeridiem.PM;
        DatePicker datePickerEnd = DatePicker.newDatePicker().setSelectEndDay(DateTimeUtility.getTodayAsNumber()).setEndAtHour(timeToSet[0]).setEndAtMin(timeToSet[1]).setEndAtAmPm(currentAmPm);
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDatePickerStart(datePickerStart).setDatePickerEnd(datePickerEnd);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName() );
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition),Lists.newArrayList(ActivityActions.Tag), tag);
        LoyaltyService.createActivityInBox(activity);
        // wait until activity will change state to complete
        log4j.info("Going to sleep for 5 minutes until Activity will be change to Complete");
        TimeUnit.MINUTES.sleep(5); // wait 3 minutes until activity will change state to complete - status not appear until refresh by design.
        Navigator.refresh();
        TimeUnit.SECONDS.sleep(30); // Verify that page finished refreshing.
        campaignFilter.toggleFilters(false, CampaignFilterView.COMPLETED);
        log4j.info("Verify that activity with state <complete> doesn't appear in the view when <complete> filter state is <false>");
        Assert.assertFalse("Activity <" + rule.getActivityName() + "> should not appear since the filter of <complete> is <false>" ,ActivityService.isActiviyExists(rule.getActivityName()));
        campaignFilter.toggleFilters(true, CampaignFilterView.COMPLETED);
        log4j.info("Verify that activity with state <complete> appear in the view when <complete> filter state is <true>");
        Assert.assertTrue("Activity <" + rule.getActivityName() + "> should appear since the filter of <complete> is <true>" ,ActivityService.isActiviyExists(rule.getActivityName()));
        log4j.info("try to change complete activity to <stop> state");
        ActivityService.executeOperation(rule.getActivityName(), ItemOperation.STOP);
        Assert.assertTrue("Rule state appear as <" + ActivityService.getActivityStatus(rule.getActivityName()).toLowerCase() + "> instead of <" +  CampaignFilterView.STOPPED.name().toLowerCase() +">", ActivityService.getActivityStatus(rule.getActivityName()).toLowerCase().equals(CampaignFilterView.STOPPED.name().toLowerCase()));
    }

    @Test //C10566
    @ru.yandex.qatools.allure.annotations.Description("This test verify that the filter show/hide  rules with state: future")
    public void testfilterFutureActivity() throws Exception {
        tag = ActionsBuilder.TAG + timestamp.toString();
        CampaignFilter campaignFilter = new CampaignFilter();
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayPlusXDays(3));
        String endDay =DateTimeUtility.getTodayPlusXDays(5);
        DatePicker datePickerEnd = DatePicker.newDatePicker().setSelectEndDay(endDay);
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDatePickerStart(datePickerStart).setDatePickerEnd(datePickerEnd);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName() );
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition),Lists.newArrayList(ActivityActions.Tag), tag);
        LoyaltyService.createActivityInBox(activity);
        Assert.assertTrue("Rule state appear as <" + ActivityService.getActivityStatus(rule.getActivityName()).toLowerCase() + "> instead of <" +  CampaignFilterView.FUTURE.name().toLowerCase() +">", ActivityService.getActivityStatus(rule.getActivityName()).toLowerCase().equals(CampaignFilterView.FUTURE.name().toLowerCase()));
        campaignFilter.toggleFilters(false, CampaignFilterView.FUTURE);
        log4j.info("Verify that activity with state <future> doesn't appear in the view when <future> filter state is <false>");
        Assert.assertFalse("Activity <" + rule.getActivityName() + "> should not appear since the filter of <future> is <false>" ,ActivityService.isActiviyExists(rule.getActivityName()));
        campaignFilter.toggleFilters(true, CampaignFilterView.FUTURE);
        log4j.info("Verify that activity with state <future> appear in the view when filter state is <true>");
        Assert.assertTrue("Activity <" + rule.getActivityName() + "> should appear since the filter of <future> is <true>" ,ActivityService.isActiviyExists(rule.getActivityName()));
    }


    @Test //C10564
    @ru.yandex.qatools.allure.annotations.Description("This test verify that the filter show/hide  rules with state: draft")
    public void testfilterDraftActivity() throws Exception {
        tag = ActionsBuilder.TAG + timestamp.toString();
        CampaignFilter campaignFilter = new CampaignFilter();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Draft, getAutomationName() );
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(ActivityActions.Tag), tag);
        LoyaltyService.createActivityInBox(activity);
        Assert.assertTrue("Rule state appear as <" + ActivityService.getActivityStatus(rule.getActivityName()).toLowerCase() + "> instead of <" +  CampaignFilterView.DRAFT.name().toLowerCase() +">", ActivityService.getActivityStatus(rule.getActivityName()).toLowerCase().equals(CampaignFilterView.DRAFT.name().toLowerCase()));
        campaignFilter.toggleFilters(false, CampaignFilterView.DRAFT);
        log4j.info("Verify that activity with state <draft> doesn't appear in the view when <draft> filter state is <false>");
        Assert.assertFalse("Activity <" + rule.getActivityName() + "> should not appear since the filter of <draft> is <false>" ,ActivityService.isActiviyExists(rule.getActivityName()));
        campaignFilter.toggleFilters(true, CampaignFilterView.DRAFT);
        log4j.info("Verify that activity with state <draft> appear in the view when filter state is <true>");
        Assert.assertTrue("Activity <" + rule.getActivityName() + "> should appear since the filter of <draft> is <true>" ,ActivityService.isActiviyExists(rule.getActivityName()));
    }

    @Override
    public ITrigger getTrigger(){return ActivityTrigger.Tagged;}

    @Override
    public String getAutomationName(){return ActivityTrigger.Tagged.toString() +"_";}
}
