package hub2_0.gifts;

import com.google.common.collect.Lists;
import hub.hub2_0.basePages.loyalty.gifts.GiftDisplay;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountEntireTicketAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountEntireTicketAction;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.items.SelectItems;
import org.junit.Assert;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import server.v2_8.common.Item;
import server.v4.GetBenefits;
import server.v4.GetBenefitsResponse;
import server.v4.common.models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class EntireTicketGiftTest extends BaseGiftTest {


    @Description("Gift - give 50$ discount on entire purchase")
    @Test
    public void testGiftWithEntireTicketDiscountAmountOff() throws Exception {

        String amountDiscount = "50";
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountEntireTicketAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item = createItem("ITEM_CODE_111", "Prod2", "123", "Dep1", 5000, 1);
        Item item2 = createItem("ITEM_CODE_222", "Prod3", "123", "Dep1", 5000, 1);
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "10000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-5000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Description("Gift - give 50% discount on entire purchase except from item '2222' ")
    @Test
    public void testEntireTicketDiscountAmountOffWithExclude() throws Exception {

        String amountDiscount = "50";
        SelectItems excludeItems = dealBuilder.selectItemsByItemCode("2222");
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount, excludeItems);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountEntireTicketAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 3000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 10000, 2);
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "13000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-3000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Description("Gift - give 50% discount on entire purchase")
    @Test
    public void testEntireTicketDiscount50percentOff() throws Exception {

        String percentDiscount = "50";
        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = buildPercentOffDiscountOnEntireTicket(percentDiscount);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(percentOffDiscountEntireTicketAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 90000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 50000, 2);
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "140000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-70000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    /*@Test
    public void testEntireTicketDiscountAmountOffWithCondition() throws Exception {

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("100");
        String amountDiscount = "50";
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountEntireTicketAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 8000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 10000, 2);
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "18000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-5000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }*/



}
