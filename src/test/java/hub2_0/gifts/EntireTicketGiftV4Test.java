package hub2_0.gifts;

import com.google.common.collect.Lists;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountEntireTicketAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountEntireTicketAction;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.items.SelectItems;
import org.junit.Assert;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import server.v4.GetBenefits;
import server.v4.GetBenefitsResponse;
import server.v4.common.models.PurchaseItem;
import server.v4.common.models.RedeemAsset;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class EntireTicketGiftV4Test extends BaseGiftTest {

    @Description("Gift - give 50$ discount on entire purchase")
    @Test
    public void testGiftWithEntireTicketDiscountAmountOff_V4() throws Exception {

        String amountDiscount = "50";
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountEntireTicketAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        //create RedeemAsset fot getBenefits request
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        log4j.info("redeem an asset");
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("ITEM_CODE_111", 1, 5000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("ITEM_CODE_222", 1, 5000, "depCode", "depName"));

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items), 10000, UUID.randomUUID().toString());

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Verify discount's sum is as expected","-5000", getBenefitsResponse.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse.getTotalDiscountSum().toString());
    }

    @Description("Gift - give 50% discount on entire purchase except from item '2222' ")
    @Test
    public void testEntireTicketDiscountAmountOffWithExclude_V4() throws Exception {

        String amountDiscount = "50";
        SelectItems excludeItems = dealBuilder.selectItemsByItemCode("2222");
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount, excludeItems);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountEntireTicketAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        //create RedeemAsset fot getBenefits request
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        log4j.info("redeem an asset");
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("1111", 1, 3000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("2222", 2, 10000, "depCode", "depName"));

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items), 13000, UUID.randomUUID().toString());

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Verify discount's sum is as expected","-3000", getBenefitsResponse.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse.getTotalDiscountSum().toString());
    }

    @Description("Gift - give 50% discount on entire purchase")
    @Test
    public void testEntireTicketDiscount50percentOff_V4() throws Exception {

        String percentDiscount = "50";
        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = buildPercentOffDiscountOnEntireTicket(percentDiscount);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(percentOffDiscountEntireTicketAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        //create RedeemAsset fot getBenefits request
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        log4j.info("redeem an asset");
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("1111", 1, 90000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("2222", 2, 50000, "depCode", "depName"));

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items), 140000, UUID.randomUUID().toString());

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Verify discount's sum is as expected","-70000", getBenefitsResponse.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse.getTotalDiscountSum().toString());
    }

    /*@Test
    public void testEntireTicketDiscountAmountOffWithCondition_V4() throws Exception {

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("100");
        String amountDiscount = "50";
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountEntireTicketAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        //create RedeemAsset fot getBenefits request
        server.v4.common.models.RedeemAsset redeemAsset = new server.v4.common.models.RedeemAsset();
        redeemAsset.setKey(assetKey);
        log4j.info("redeem an asset");
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("1111", 1, 8000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("2222", 2, 10000, "depCode", "depName"));

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items), 18000, UUID.randomUUID().toString());

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Verify discount's sum is as expected","-5000", getBenefitsResponse.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse.getTotalDiscountSum().toString());

    }*/


}
