package hub2_0.gifts;

import com.google.common.collect.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.MakeItemsFreeAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.SetSpecialPriceAction;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.operators.IntOperators;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.yandex.qatools.allure.annotations.Description;
import server.v2_8.common.Item;
import server.v4.GetBenefits;
import server.v4.GetBenefitsResponse;
import server.v4.common.models.PurchaseItem;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class SpecificItemsGiftTest extends BaseGiftTest {


    @Description("Gift - give 10$ discount on item '1111'")
    @Test
    @Category(Hub2Regression.class)
    public void testSpecificItemsAmountOffGift() throws Exception {

        String amountDiscount = "10";
        AmountOffDiscountAction amountOffDiscountAction = buildAmountOffDiscount(amountDiscount, giftsBuilder.selectItemsByItemCode("1111"));
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("7777", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 20000, 1);

        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "30000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-1000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Description("Gift - give 10% discount on item '1111'")
    @Test
    public void testSpecificItemsPercentOffGift() throws Exception {

        String percentDiscount = "10";
        PercentOffDiscountAction percentOffDiscountAction = buildPercentOffDiscount(percentDiscount, giftsBuilder.selectItemsByItemCode("1111"));
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(percentOffDiscountAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("7777", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 20000, 1);

        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "30000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-2000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @Description("Gift - give special price of 10$ on item '1111'")
    @Test
    public void testSpecificItemsSpecialPriceGift() throws Exception {

        String specialPrice = "10";
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction(specialPrice, giftsBuilder.selectItemsByItemCode("1111"));
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(setSpecialPriceAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("7777", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 9000, 2);

        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "19000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-7000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @Description("Gift - give special price of 0$ on item '1111'")
    @Test
    public void testSpecificItemsSpecialPriceZeroGift() throws Exception {

        String specialPrice = "0";
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction(specialPrice, dealBuilder.selectItemsByItemCode("1111"));
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(setSpecialPriceAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("7777", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 9000, 2);

        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "19000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-9000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Description("Gift - give item '1111' for free")
    @Test
    @Category(Hub2Regression.class)
    public void testSpecificItemsFreeItem() throws Exception {

        MakeItemsFreeAction makeItemsFreeAction = buildMakeItemFreeAction(dealBuilder.selectItemsByItemCode("1111"));
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(makeItemsFreeAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("7777", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 9000, 2);

        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "19000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-9000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    /*@Test
    public void testSpecificItemsDiscountAmountOffWithCondition() throws Exception {

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("100");
        String amountDiscount = "10";
        AmountOffDiscountAction amountOffDiscountAction = buildAmountOffDiscount(amountDiscount, giftsBuilder.selectItemsByItemCode("1111"));
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("7777", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 20000, 1);

        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "30000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-1000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }*/




}
