package hub2_0.gifts;

import com.google.common.collect.Lists;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.items.SelectBundle;
import org.junit.Assert;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import server.utils.RedeemErrorCodesMap;
import server.v2_8.common.Item;

import java.util.UUID;

public class AdvancedDiscountGiftTest extends BaseGiftTest {

    @Description("test gift with buy X get Y discount - for each 100$ spend on item: 5583 and quantity 2 of item: 1122 get 10$ off")
    @Test
    public void testGiftWithAdvancedDiscountAmountOff() throws Exception {

        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickEntireRecurringBundleButton();
        selectBundle.clickApplyButton();
        ActivityAction activityAction1 = buildAmountOffDiscount("10", selectBundle);
        //Line A
        OccurrencesCondition occurrencesCondition1 = new OccurrencesCondition();
        occurrencesCondition1.setSelectItemsDialog(selectItemsByItemCode("5583"));
        occurrencesCondition1.setConditionKey(OccurrencesCondition.Condition.IN_THE_TOTAL_SPEND_OF);
        occurrencesCondition1.setConditionValue("100");
        //Line B
        OccurrencesCondition occurrencesCondition2 = new OccurrencesCondition();
        occurrencesCondition2.setSelectItemsDialog(selectItemsByItemCode("1122"));
        occurrencesCondition2.setConditionKey(OccurrencesCondition.Condition.IN_THE_QUANTITY_OF);
        occurrencesCondition2.setConditionValue("2");

        newHub2Gift = createAdvancedDiscountGift(Lists.newArrayList(), Lists.newArrayList(occurrencesCondition1,occurrencesCondition2), Lists.newArrayList(activityAction1));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item = createItem("5583", "AAA", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1122", "CCC", "123", "Dep1", 5000, 2);
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "15000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-1000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Description("test gift with buy X get Y discount - for each 100$ spend on item: 5583 and quantity 2 of item: 1122 get 10$ off with mark asset as used - " +
            "send the redeem call twice and make sure that in the second time there's an 'ASSET_ALREADY_REDEEMED' error")
    @Test
    public void testGiftWithAdvancedDiscountAmountOffWithBurnAsset() throws Exception {

        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickEntireRecurringBundleButton();
        selectBundle.clickApplyButton();
        ActivityAction activityAction1 = buildAmountOffDiscount("10", selectBundle);
        //Line A
        OccurrencesCondition occurrencesCondition1 = new OccurrencesCondition();
        occurrencesCondition1.setSelectItemsDialog(selectItemsByItemCode("5583"));
        occurrencesCondition1.setConditionKey(OccurrencesCondition.Condition.IN_THE_TOTAL_SPEND_OF);
        occurrencesCondition1.setConditionValue("100");
        //Line B
        OccurrencesCondition occurrencesCondition2 = new OccurrencesCondition();
        occurrencesCondition2.setSelectItemsDialog(selectItemsByItemCode("1122"));
        occurrencesCondition2.setConditionKey(OccurrencesCondition.Condition.IN_THE_QUANTITY_OF);
        occurrencesCondition2.setConditionValue("2");

        newHub2Gift = createAdvancedDiscountGift(Lists.newArrayList(), Lists.newArrayList(occurrencesCondition1,occurrencesCondition2), Lists.newArrayList(activityAction1));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        String transactionID = UUID.randomUUID().toString();
        log4j.info("redeem an asset");
        Item item = createItem("5583", "AAA", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1122", "CCC", "123", "Dep1", 5000, 2);
        String discountSum = redeemAssetWithMarkAsUsed(newMember.getPhoneNumber(), transactionID, assetKey, item, item2, "15000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-1000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

        log4j.info("redeem an asset 2");
        Item item3 = createItem("5583", "AAA", "123", "Dep1", 10000, 1);
        Item item4 = createItem("1122", "CCC", "123", "Dep1", 5000, 2);
        String discountSum2 = redeemAssetWithMarkAsUsed(newMember.getPhoneNumber(), transactionID, assetKey, item3, item4, "15000");

        //Check discount's sum
        Assert.assertEquals("Asset already redeemed is expected ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.ASSET_ALREADY_REDEEMED), discountSum2);
        log4j.info("Redeem code not found is expected : " + discountSum2);


    }


}
