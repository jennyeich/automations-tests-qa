package hub2_0.gifts;

import com.google.common.collect.Lists;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountEntireTicketAction;
import hub.hub2_0.common.actions.dealActions.SetSpecialPriceAction;
import hub.hub2_0.common.items.SelectItems;
import org.junit.Assert;
import org.junit.Test;
import server.v4.GetBenefits;
import server.v4.GetBenefitsResponse;
import server.v4.common.models.PurchaseItem;
import server.v4.common.models.RedeemAsset;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class GiftActionLimitV4Test extends BaseGiftTest {

    @Test
    public void testEntireTicketDiscount10percentWithLimit_V4() throws Exception {

        String percentDiscount = "50";
        String amountLimit = "100";
        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = buildPercentOffDiscountOnEntireTicket(percentDiscount, NO_ITEM_EXCLUDED, amountLimit);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(percentOffDiscountEntireTicketAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        //create RedeemAsset fot getBenefits request
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        log4j.info("redeem an asset");
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("1111", 1, 90000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("2222", 2, 50000, "depCode", "depName"));

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items), 140000, UUID.randomUUID().toString());

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Verify discount's sum is as expected","-10000", getBenefitsResponse.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse.getTotalDiscountSum().toString());

    }

    @Test
    public void testEntireTicket5percentDiscountWithLimitAndExcludeItem_V4() throws Exception {

        String percentDiscount = "5";
        String amountLimit = "50";
        SelectItems excludeItems = dealBuilder.selectItemsByItemCode("2222");
        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = buildPercentOffDiscountOnEntireTicket(percentDiscount, excludeItems, amountLimit);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(percentOffDiscountEntireTicketAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        //create RedeemAsset fot getBenefits request
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        log4j.info("redeem an asset");
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("1111", 2, 200000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("2222", 1, 80000, "depCode", "depName"));

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items), 280000, UUID.randomUUID().toString());

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Verify discount's sum is as expected","-5000", getBenefitsResponse.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse.getTotalDiscountSum().toString());

    }

    @Test
    public void testSpecificItemsAmountOffDiscountsWithLimitAmountAndTimes_V4() throws Exception {

        String amountLimit = "80";
        String timesLimit = "2";
        String discountAmount = "50";
        AmountOffDiscountAction amountOffDiscountAction = buildAmountOffDiscount(discountAmount, dealBuilder.selectItemsByItemCode("2222"), amountLimit, timesLimit);
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        //create RedeemAsset fot getBenefits request
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        log4j.info("redeem an asset");
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("1111", 1, 20000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("2222", 3, 30000, "depCode", "depName"));

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items), 50000, UUID.randomUUID().toString());

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Verify discount's sum is as expected","-8000", getBenefitsResponse.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse.getTotalDiscountSum().toString());

    }

    @Test
    public void testSpecificItemsPercentOffDiscountsWithLimitAmountAndTimes_V4() throws Exception {

        String amountLimit = "50";
        String timesLimit = "1";
        String discountPercent = "20";
        PercentOffDiscountAction percentOffDiscountAction = buildPercentOffDiscount(discountPercent, dealBuilder.selectItemsByItemCode("2222"), amountLimit, timesLimit);
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(percentOffDiscountAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        //create RedeemAsset fot getBenefits request
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        log4j.info("redeem an asset");
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("1111", 1, 20000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("2222", 2, 20000, "depCode", "depName"));

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items), 40000, UUID.randomUUID().toString());

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Verify discount's sum is as expected","-2000", getBenefitsResponse.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse.getTotalDiscountSum().toString());

    }

    @Test
    public void testSpecificItemsSpecialPriceDiscountsWithLimitAmountAndTimes_V4() throws Exception {

        String amountLimit = "80";
        String timesLimit = "3";
        String specialPrice = "20";
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction(specialPrice, dealBuilder.selectItemsByItemCode("2222"), amountLimit, timesLimit);
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(setSpecialPriceAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        String transactionID = UUID.randomUUID().toString();
        String openTime = String.valueOf(System.currentTimeMillis());
        //create RedeemAsset fot getBenefits request
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        log4j.info("redeem an asset");
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("1111", 1, 10000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("2222", 5, 25000, "depCode", "depName"));

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAssetWithOpenTime(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items), 35000, transactionID, openTime);

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Verify discount's sum is as expected", "-8000", getBenefitsResponse.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse.getTotalDiscountSum().toString());

        log4j.info("redeem an asset 2");
        List<PurchaseItem> items2 = new ArrayList<>();
        items2.add(v4Builder.buildPurchaseItem("1111", 1, 10000, "depCode", "depName"));
        items2.add(v4Builder.buildPurchaseItem("2222", 5, 20000, "depCode", "depName"));

        GetBenefits getBenefits2 = createGetBenefitsRequestForRedeemAssetWithOpenTime(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items2), 30000, transactionID, openTime);

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse1 = getGetBenefitsResponse(getBenefits2);
        Assert.assertEquals("Verify discount's sum is as expected", "-6000", getBenefitsResponse1.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse1.getTotalDiscountSum().toString());

    }


}
