package hub2_0.gifts;

import com.google.appengine.api.datastore.Key;
import com.google.common.collect.Lists;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.hub1_0.services.Navigator;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.gifts.GiftDisplay;
import hub.hub2_0.basePages.loyalty.gifts.ExpirationSelect;
import hub.hub2_0.basePages.loyalty.gifts.Gift;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.GiftsBuilder;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.services.loyalty.Gifts.GiftService;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub.utils.HubMemberCreator;
import integration.base.BaseHub2DealTest;
import org.junit.Assert;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.v4.GetBenefits;
import server.v4.GetBenefitsResponse;
import server.v4.base.ZappServerErrorResponse;
import server.v4.common.enums.ExpandTypes;
import server.v4.common.enums.ReturnedBenefits;
import server.v4.common.models.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;



public class BaseGiftTest extends BaseHub2DealTest {

    public static Gift newHub2Gift;
    public static NewMember newMember;
    public static Key key = null;

    GiftsBuilder giftsBuilder = new GiftsBuilder();


    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);


        }
    };



    protected Gift createEntireTicketDiscountGift (List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        GiftDisplay giftDisplay = new GiftDisplay();
        giftDisplay.setDisplayTab("");
        giftDisplay.setImageUpload("");
        giftDisplay.setSelectExistImage("");
        giftDisplay.setImageSave("");
        Gift gift = giftsBuilder.buildEntireTicketDiscountActivityGift("entireTicketGift_",timestamp.toString(), conditions, activityActionList, giftDisplay);

        gift.clickPublishActivityBtn();
        LoyaltyService.createGift(gift);

        return gift;

    }

    protected Gift createAdvancedDiscountGift(List<ApplyCondition> conditions, List<OccurrencesCondition> occurrencesConditionList, List<ActivityAction> activityActionList) throws Exception {

        GiftDisplay giftDisplay = new GiftDisplay();
        giftDisplay.setDisplayTab("");
        giftDisplay.setImageUpload("");
        giftDisplay.setSelectExistImage("");
        giftDisplay.setImageSave("");
        Gift gift = giftsBuilder.buildAdvancedDiscountGift("advancedDiscountGift_",timestamp.toString(), conditions, occurrencesConditionList, activityActionList, giftDisplay);
        gift.clickPublishActivityBtn();
        LoyaltyService.createGift(gift);

        return gift;
    }


    protected Gift createEntireTicketDiscountGift(List<ApplyCondition> conditions, List<ActivityAction> activityActionList, ExpirationSelect expirationSelect) throws Exception {

        GiftDisplay giftDisplay = new GiftDisplay();
        giftDisplay.setDisplayTab("");
        giftDisplay.setImageUpload("");
        giftDisplay.setSelectExistImage("");
        giftDisplay.setImageSave("");

        Gift gift = giftsBuilder.buildEntireTicketDiscountActivityGift("entireTicketGift_",timestamp.toString(), conditions, activityActionList,giftDisplay);
        gift.setExpirationSelect(expirationSelect);
        gift.clickPublishActivityBtn();
        LoyaltyService.createGift(gift);

        return gift;

    }

    protected Gift createSpecificItemsDiscountGift(List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {

        GiftDisplay giftDisplay = new GiftDisplay();
        giftDisplay.setDisplayTab("");
        giftDisplay.setImageUpload("");
        giftDisplay.setSelectExistImage("");
        giftDisplay.setImageSave("");

        Gift gift = giftsBuilder.buildSpecificItemsDiscountGift("specificItemsGift_",timestamp.toString(), conditions, activityActionList, giftDisplay);
        gift.clickPublishActivityBtn();
        LoyaltyService.createGift(gift);

        return gift;
    }

    protected Gift createSpecificItemsDiscountGift(List<ApplyCondition> conditions, List<ActivityAction> activityActionList, ExpirationSelect expirationSelect) throws Exception {

        GiftDisplay giftDisplay = new GiftDisplay();
        giftDisplay.setDisplayTab("");
        giftDisplay.setImageUpload("");
        giftDisplay.setSelectExistImage("");
        giftDisplay.setImageSave("");

        Gift gift = giftsBuilder.buildSpecificItemsDiscountGift("specificItemsGift_",timestamp.toString(), conditions, activityActionList, giftDisplay);
        gift.setExpirationSelect(expirationSelect);
        gift.clickPublishActivityBtn();
        LoyaltyService.createGift(gift);

        return gift;
    }

    protected void createNewMemberAndSendTheGift() throws Exception{
        // Create a new member
        newMember = HubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        MembersService.performSmartActionSendAnAsset(newHub2Gift.getActivityName(), timestamp);
        Assert.assertTrue("Member did not get the gift", MembersService.isGiftExists(newHub2Gift.getActivityName()));

    }

    protected NewMember createNewMember() throws Exception {
        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        return newMember;
    }

    protected void sendGiftToMember() throws Exception{
        //Send gift to member
        MembersService.performSmartActionSendAnAsset(newHub2Gift.getActivityName(), timestamp);
        Assert.assertTrue("Member did not get the gift", MembersService.isGiftExists(newHub2Gift.getActivityName()));

    }

    protected void openGiftAnalysis(String giftName) throws Exception {

        Navigator.loyaltyTabPageAndClickOnGiftPageAndOpenGiftAnalysis(giftName);
    }

    protected void numOfGiftThatWasSent(String numOfSentGift) throws Exception {

        GiftService.numOfSentGift(numOfSentGift);

    }

    protected void performUpdateMemberDetails(NewMember member) throws Exception {
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setPhoneNumber(member.getPhoneNumber());
        memberDetails.setFirstName(member.getFirstName() + "_Updated");
        MembersService.updateMemberDetails(memberDetails);
        TimeUnit.SECONDS.sleep(7);

    }

    protected GetBenefits createGetBenefitsRequestForRedeemAsset(ArrayList<RedeemAsset> redeemAssets, ArrayList<Customer> customers, List<PurchaseItem> purchaseItems, int totalAmount, String transId ){
        GetBenefits getBenefits = new GetBenefits(apiKey);
        getBenefits.setRedeemAssets(redeemAssets);

        getBenefits.setCustomers(customers);

        Purchase purchase = v4Builder.buildPurchase(transId,totalAmount ,purchaseItems);
        log4j.info("TransactionID: " + purchase.getTransactionId());
        getBenefits.setPurchase(purchase);

        //add query parameters
        setQueryParams(Lists.newArrayList(ExpandTypes.DISCOUNT_BY_DISCOUNT.toString()),false, ReturnedBenefits.REDEEM_ASSETS.toString(),getBenefits);

        return getBenefits;
    }

    protected GetBenefits createGetBenefitsRequestForRedeemAssetWithOpenTime(ArrayList<RedeemAsset> redeemAssets, ArrayList<Customer> customers, List<PurchaseItem> purchaseItems, int totalAmount, String transId, String openTime ){
        GetBenefits getBenefits = new GetBenefits(apiKey);
        getBenefits.setRedeemAssets(redeemAssets);

        getBenefits.setCustomers(customers);

        Purchase purchase = v4Builder.buildPurchaseWithOpenTime(transId,totalAmount ,purchaseItems, openTime);
        log4j.info("TransactionID: " + purchase.getTransactionId());
        getBenefits.setPurchase(purchase);

        //add query parameters
        setQueryParams(Lists.newArrayList(ExpandTypes.DISCOUNT_BY_DISCOUNT.toString()),false, ReturnedBenefits.REDEEM_ASSETS.toString(),getBenefits);

        return getBenefits;
    }

    protected void setQueryParams(List<String> expand, boolean markAssetsAsUsed, String returnBenefits, GetBenefits getBenefits) {
        GetBenefitsQueryParams queryParams =  new GetBenefitsQueryParams();
        queryParams.setExpand(Lists.newArrayList(expand));
        queryParams.setMarkAssetsAsUsed(markAssetsAsUsed);
        queryParams.setReturnBenefits(returnBenefits);
        getBenefits.setQueryParameters(queryParams.getParams());
    }

    protected GetBenefitsResponse getGetBenefitsResponse(GetBenefits getBenefits) {
        //Send the request
        IServerResponse response = getBenefits.SendRequestByApiKey(apiKey, GetBenefitsResponse.class);

        // make sure the response is not Error response
        if(response instanceof ZappServerErrorResponse){
            Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
        }

        // Result true assertion
        GetBenefitsResponse getBenefitsResponse = (GetBenefitsResponse) response;
        Assert.assertEquals("Status code must be 200",httpStatusCode200, getBenefitsResponse.getStatusCode());
        return getBenefitsResponse;
    }
}
