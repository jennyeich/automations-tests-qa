package hub2_0.gifts;

import com.google.common.collect.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.MakeItemsFreeAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.SetSpecialPriceAction;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.yandex.qatools.allure.annotations.Description;
import server.v4.GetBenefits;
import server.v4.GetBenefitsResponse;
import server.v4.common.models.PurchaseItem;
import server.v4.common.models.RedeemAsset;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class SpecificItemsGiftV4Test extends BaseGiftTest {

    @Description("Gift - give 10$ discount on item '1111'")
    @Test
    public void testSpecificItemsAmountOffGift_V4() throws Exception {

        String amountDiscount = "10";
        AmountOffDiscountAction amountOffDiscountAction = buildAmountOffDiscount(amountDiscount, giftsBuilder.selectItemsByItemCode("1111"));
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        //create RedeemAsset fot getBenefits request
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        log4j.info("redeem an asset");
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("7777", 1, 10000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("1111", 1, 20000, "depCode", "depName"));

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items), 30000, UUID.randomUUID().toString());

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Verify discount's sum is as expected","-1000", getBenefitsResponse.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse.getTotalDiscountSum().toString());

    }

    @Description("Gift - give 10% discount on item '1111'")
    @Test
    public void testSpecificItemsPercentOffGift_V4() throws Exception {

        String percentDiscount = "10";
        PercentOffDiscountAction percentOffDiscountAction = buildPercentOffDiscount(percentDiscount, giftsBuilder.selectItemsByItemCode("1111"));
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(percentOffDiscountAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        //create RedeemAsset fot getBenefits request
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        log4j.info("redeem an asset");
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("7777", 1, 10000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("1111", 1, 20000, "depCode", "depName"));

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items), 30000, UUID.randomUUID().toString());

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Verify discount's sum is as expected","-2000", getBenefitsResponse.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse.getTotalDiscountSum().toString());

    }

    @Description("Gift - give special price of 10$ on item '1111'")
    @Test
    public void testSpecificItemsSpecialPriceGift_V4() throws Exception {

        String specialPrice = "10";
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction(specialPrice, giftsBuilder.selectItemsByItemCode("1111"));
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(setSpecialPriceAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        //create RedeemAsset fot getBenefits request
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        log4j.info("redeem an asset");
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("7777", 1, 10000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("1111", 2, 9000, "depCode", "depName"));

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items), 19000, UUID.randomUUID().toString());

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Verify discount's sum is as expected","-7000", getBenefitsResponse.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse.getTotalDiscountSum().toString());

    }

    @Description("Gift - give special price of 0$ on item '1111'")
    @Test
    public void testSpecificItemsSpecialPriceZeroGift_V4() throws Exception {

        String specialPrice = "0";
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction(specialPrice, dealBuilder.selectItemsByItemCode("1111"));
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(setSpecialPriceAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        //create RedeemAsset fot getBenefits request
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        log4j.info("redeem an asset");
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("7777", 1, 10000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("1111", 2, 9000, "depCode", "depName"));

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items), 19000, UUID.randomUUID().toString());

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Verify discount's sum is as expected","-9000", getBenefitsResponse.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse.getTotalDiscountSum().toString());

    }

    @Description("Gift - give item '1111' for free")
    @Test
    public void testSpecificItemsFreeItem_V4() throws Exception {

        MakeItemsFreeAction makeItemsFreeAction = buildMakeItemFreeAction(dealBuilder.selectItemsByItemCode("1111"));
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(makeItemsFreeAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        //create RedeemAsset fot getBenefits request
        RedeemAsset redeemAsset = new RedeemAsset();
        redeemAsset.setKey(assetKey);
        log4j.info("redeem an asset");
        List<PurchaseItem> items = new ArrayList<>();
        items.add(v4Builder.buildPurchaseItem("7777", 1, 10000, "depCode", "depName"));
        items.add(v4Builder.buildPurchaseItem("1111", 2, 9000, "depCode", "depName"));

        GetBenefits getBenefits = createGetBenefitsRequestForRedeemAsset(Lists.newArrayList(redeemAsset), Lists.newArrayList(), Lists.newArrayList(items), 19000, UUID.randomUUID().toString());

        //Get the redeemAsset response
        GetBenefitsResponse getBenefitsResponse = getGetBenefitsResponse(getBenefits);
        Assert.assertEquals("Verify discount's sum is as expected","-9000", getBenefitsResponse.getTotalDiscountSum().toString());
        log4j.info("Total discount sum is as expected : " + getBenefitsResponse.getTotalDiscountSum().toString());

    }




}
