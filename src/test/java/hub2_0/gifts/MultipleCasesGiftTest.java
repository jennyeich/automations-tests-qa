package hub2_0.gifts;

import com.google.common.collect.Lists;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.basePages.loyalty.cases.DealCase;
import hub.hub2_0.basePages.loyalty.gifts.Gift;
import hub.hub2_0.basePages.loyalty.gifts.GiftDisplay;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountEntireTicketAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.SendCodeToPOSAction;
import hub.hub2_0.common.actions.dealActions.SetSpecialPriceAction;
import hub.hub2_0.common.builders.CasesConditionsBuilder;
import hub.hub2_0.common.builders.GiftsBuilder;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.cases.CaseMadeAPurchaseCondition;
import hub.hub2_0.common.conditions.cases.GeneralCondition;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.conditions.operators.ShoppingCartOperators;
import hub.hub2_0.common.enums.ActivityActions;
import hub.hub2_0.common.enums.ActivityStatus;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.ActivityType;
import hub.hub2_0.common.items.CodesGroup;
import hub.hub2_0.common.items.NewGroup;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub.utils.HubMemberCreator;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.ApiClientFields;
import server.common.IServerResponse;
import server.v2_8.RedeemAssetResponse;
import server.v2_8.RedeemResponse;
import server.v2_8.common.Item;
import server.v4.GetBenefits;
import server.v4.GetBenefitsResponse;
import server.v4.common.models.PurchaseItem;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static integration.base.BaseRuleHub2Test.ruleBuilder;

public class MultipleCasesGiftTest extends BaseGiftTest {

    public static NewMember newMember;
    public static Gift newHub2Gift;
    public static String assetKey;
    public static String transactionID;

    static GiftsBuilder giftsBuilder = new GiftsBuilder();

    @BeforeClass
    public static void createNewBox() throws Exception {

        GeneralCondition generalCondition1 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchase("43211", MadeAPurchaseCondition.ConditionKey.SHOPPING_CART, ShoppingCartOperators.CONTAINS_AT_LEAST, "1");

        DealCase case1 = new DealCase();
        case1.setConditions(Lists.newArrayList(generalCondition1));
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket("5");
        SendCodeToPOSAction sendCodeToPOSAction = buildSendCodeToPOS(SendCodeToPOSAction.CodeType.ITEM_CODE, "55555");
        case1.setActions(Lists.newArrayList(amountOffDiscountEntireTicketAction, sendCodeToPOSAction));

        GeneralCondition generalCondition2 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND, IntOperators.IS_LESS_THAN, "15");

        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction2 = new AmountOffDiscountEntireTicketAction();
        amountOffDiscountEntireTicketAction2.setDiscountAmount("4");
        SendCodeToPOSAction deleteSendCodeToPOSAction = new SendCodeToPOSAction();
        deleteSendCodeToPOSAction.clickDeleteAction();

        DealCase case2 = new DealCase();
        case2.setConditions(Lists.newArrayList(generalCondition2));
        case2.setActions(Lists.newArrayList(amountOffDiscountEntireTicketAction2, deleteSendCodeToPOSAction));

        GeneralCondition generalCondition3 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchase("8778", MadeAPurchaseCondition.ConditionKey.SHOPPING_CART, ShoppingCartOperators.CONTAINS_AT_LEAST, "2");

        SelectItems selectFruitItems = new SelectItems();
        selectFruitItems.clickItemCodesRadioButton();
        CodesGroup itemCodes = new CodesGroup();
        itemCodes.setCodes(CodesGroup.ITEM_CODES_GROUP, "6543", "8877", "9989", "1222");
        String groupName = "group_Fruits";
        itemCodes.clickSaveAsGroupBtn(groupName, NewGroup.SAVE);
        selectFruitItems.setItemCodes(itemCodes);
        selectFruitItems.clickApply();
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction("2", selectFruitItems);


        SelectItems selectJunkItems = new SelectItems();
        selectJunkItems.clickItemCodesRadioButton();
        CodesGroup itemCodes2 = new CodesGroup();
        itemCodes2.setCodes(CodesGroup.ITEM_CODES_GROUP, "3334", "4445", "5556", "7778");
        String groupName2 = "group_Junk";
        itemCodes2.clickSaveAsGroupBtn(groupName2, NewGroup.SAVE);
        selectJunkItems.setItemCodes(itemCodes2);
        selectJunkItems.clickApply();
        PercentOffDiscountAction percentOffDiscountAction = buildPercentOffDiscount("10", selectJunkItems);

        DealCase case3 = new DealCase();
        case3.clickSpecificItemsDiscount();
        case3.setConditions(Lists.newArrayList(generalCondition3));
        case3.setActions(Lists.newArrayList(setSpecialPriceAction, percentOffDiscountAction));

        CaseMadeAPurchaseCondition madeAPurchaseCondition = new CaseMadeAPurchaseCondition();
        madeAPurchaseCondition.clickDelete();

        GeneralCondition generalCondition4 = madeAPurchaseCondition;

        DealCase case4 = new DealCase();
        case4.clickEntireTicketDiscount();
        case4.setConditions(Lists.newArrayList(generalCondition4));
        case4.setActions(Lists.newArrayList(buildAmountOffDiscountOnEntireTicket("10")));

        GiftDisplay giftDisplay = new GiftDisplay();
        giftDisplay.setDisplayTab("");
        giftDisplay.setImageUpload("");
        giftDisplay.setSelectExistImage("");
        giftDisplay.setImageSave("");

        timestamp = String.valueOf(System.currentTimeMillis());
        newHub2Gift = createMultipleCasesGift(Lists.newArrayList(), Lists.newArrayList(case1, case2, case3, case4), giftDisplay);

        createNewMemberAndSendTheGift1();

        assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());
        transactionID = UUID.randomUUID().toString();
    }


    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            serverHelper.saveApiClient(ApiClientFields.AUTO_MARK_REDEEM_CODES_AS_USED.key(),false);
        }
    };


    @Test
    public void testMultipleCaseDealCase1ActionsPerformed() throws Exception {

        log4j.info("redeem an asset");
        Item item1 = createItem("43211", "item1", "DEP_1", "Dep1", 1400, 2);
        Item item2 = createItem("2233", "item2", "DEP_2", "Dep2", 1400, 1);

        IServerResponse response = getRedeemResponse(newMember.getPhoneNumber(),transactionID, assetKey, item1, item2, "2800");

        //Get the redeemAsset response
        RedeemResponse redeemResponse = (RedeemResponse) response;
        //Check discount's sum
        Assert.assertEquals("Verify received item codes are as expected", "55555", redeemResponse.getItemCodes().get(0).getCode());
        Assert.assertEquals("Verify discount's sum is as expected", "-500", redeemResponse.getTotalDiscountsSum());
        log4j.info("Total discount sum is as expected : " + redeemResponse.getItemCodes().get(0).getCode());
        log4j.info("Received item codes are as expected : " + redeemResponse.getTotalDiscountsSum());

    }

    @Test
    public void testMultipleCaseDealCase2ActionsPerformed() throws Exception {
        log4j.info("redeem an asset");
        Item item1 = createItem("8778", "item1", "DEP_1", "Dep1", 1000, 3);
        Item item2 = createItem("2233", "item2", "DEP_2", "Dep2", 200, 1);

        String discountSum = redeemAsset(newMember.getPhoneNumber(),transactionID, assetKey, item1, item2, "1200");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-400", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @Test
    public void testMultipleCaseDealCase3ActionsPerformed() throws Exception {
        log4j.info("redeem an asset");
        Item item1 = createItem("8778", "item1", "DEP_1", "Dep1", 2400, 3);
        Item item2 = createItem("1222", "item2", "DEP_1", "Dep1", 2000, 1);

        String discountSum = redeemAsset(newMember.getPhoneNumber(),transactionID, assetKey, item1, item2, "4400");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-1800", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @Test
    public void testMultipleCaseDealCase4ActionsPerformed() throws Exception {
        log4j.info("redeem an asset");
        Item item1 = createItem("5532", "item1", "DEP_1", "Dep1", 6000, 3);
        Item item2 = createItem("4656", "item1", "DEP_2", "Dep2", 2000, 1);

        String discountSum = redeemAsset(newMember.getPhoneNumber(),transactionID, assetKey, item1, item2, "8000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-1000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }








    private static Gift createMultipleCasesGift(List<ApplyCondition> conditions, List<Case> cases, GiftDisplay giftDisplay) throws Exception {
        Gift gift = giftsBuilder.buildMultipleCasesGift("multipleCasesGift_",timestamp.toString(), conditions, cases, giftDisplay);
        gift.clickPublishActivityBtn();
        LoyaltyService.createGift(gift);

        return gift;
    }

    private static void createNewMemberAndSendTheGift1() throws Exception{
        // Create a new member
        newMember = HubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        //Send gift to member
        MembersService.performSmartActionSendAnAsset(newHub2Gift.getActivityName(), timestamp);
        Assert.assertTrue("Member did not get the gift", MembersService.isGiftExists(newHub2Gift.getActivityName()));

    }

    protected static void performUpdateMemberDetails1(NewMember member) throws Exception {
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setPhoneNumber(member.getPhoneNumber());
        memberDetails.setFirstName(member.getFirstName() + "_Updated");
        MembersService.updateMemberDetails(memberDetails);
        TimeUnit.SECONDS.sleep(5);

    }
}
