package hub2_0.gifts;

import com.google.common.collect.Lists;
import hub.hub2_0.basePages.loyalty.gifts.ExpirationSelect;
import hub.hub2_0.basePages.loyalty.gifts.GiftDisplay;
import hub.hub2_0.basePages.loyalty.gifts.SelectDialog;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountEntireTicketAction;
import hub.hub2_0.common.actions.dealActions.MakeItemsFreeAction;
import hub.hub2_0.common.actions.dealActions.SetSpecialPriceAction;
import hub.hub2_0.common.conditions.global.limits.RollingPeriod;
import org.junit.Assert;
import org.junit.Test;
import server.utils.RedeemErrorCodesMap;
import server.v2_8.common.Item;

import java.util.UUID;
import java.util.concurrent.TimeUnit;


public class GiftExpirationTest extends BaseGiftTest {


    @ru.yandex.qatools.allure.annotations.Description("Test gift with entire ticket discount amount off with activate the gift on time and deactivate after 1 week, " +
            "make sure the gift discount is as expected")
    @Test
    public void testGiftWithEntireTicketDiscountAmountOffWithExpirationSetting() throws Exception {

        ExpirationSelect expirationSelect = new ExpirationSelect();
        expirationSelect.setAfter(SelectDialog.create().setSelectRadio(SelectDialog.ON_TIME))
                .setUpTo(SelectDialog.create().setSelectRadio(SelectDialog.TIME).setInput("1").setUnits(RollingPeriod.Week.toString()));

        String amountDiscount = "50";
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountEntireTicketAction),expirationSelect);

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item = createItem("ITEM_CODE_111", "Prod2", "123", "Dep1", 5000, 1);
        Item item2 = createItem("ITEM_CODE_222", "Prod3", "123", "Dep1", 5000, 1);
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "10000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-5000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @ru.yandex.qatools.allure.annotations.Description("Test gift with entire ticket discount amount off with activate the gift after 1 day and deactivate on time" +
            " - Negative test, make sure that the member can't redeem the gift")
    @Test
    public void testGiftWithEntireTicketDiscountAmountOffWithExpirationSettingNegativeTest() throws Exception {

        ExpirationSelect expirationSelect = new ExpirationSelect();
        expirationSelect.setAfter(SelectDialog.create().setSelectRadio(SelectDialog.TIME).setInput("1").setUnits(RollingPeriod.Day.toString()))
                .setUpTo(SelectDialog.create().setSelectRadio(SelectDialog.ON_TIME));

        String amountDiscount = "50";
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountEntireTicketAction),expirationSelect);

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item = createItem("ITEM_CODE_111", "Prod2", "123", "Dep1", 5000, 1);
        Item item2 = createItem("ITEM_CODE_222", "Prod3", "123", "Dep1", 5000, 1);
        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item, item2, "10000");

        //Check discount's sum
        Assert.assertEquals("Redeem code not found is expected ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.REDEEM_CODE_NOT_FOUND), discountSum);
        log4j.info("Redeem code not found is expected : " + discountSum);

    }

    @ru.yandex.qatools.allure.annotations.Description("Test gift with entire ticket discount amount off with activate the gift after 2 minutes and deactivate on time, " +
            "make sure the gift discount is as expected")
    @Test
    public void testGiftWithSpecificItemsAmountOffWithExpirationSettingAfter2MinTest() throws Exception {

        ExpirationSelect expirationSelect = new ExpirationSelect();
        expirationSelect.setAfter(SelectDialog.create().setSelectRadio(SelectDialog.TIME).setInput("2").setUnits(RollingPeriod.Minute.toString()))
                .setUpTo(SelectDialog.create().setSelectRadio(SelectDialog.ON_TIME));

        String amountDiscount = "10";
        AmountOffDiscountAction amountOffDiscountAction = buildAmountOffDiscount(amountDiscount, giftsBuilder.selectItemsByItemCode("1111"));
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountAction),expirationSelect);

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        String transactionID = UUID.randomUUID().toString();
        log4j.info("redeem an asset");
        Item item = createItem("1111", "Prod", "123", "Dep1", 5000, 1);
        Item item2 = createItem("2222", "Prod2", "123", "Dep1", 5000, 1);
        //Redeem the asset with asset's key
        String discountSum = redeemAsset(newMember.getPhoneNumber(), transactionID, assetKey, item, item2, "10000");

        //Check discount's sum
        Assert.assertEquals("Redeem code not found is expected ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.REDEEM_CODE_NOT_FOUND), discountSum);
        log4j.info("Redeem code not found is expected : " + discountSum);

        TimeUnit.MINUTES.sleep(3);
        //redeem again after 2 minutes
        String discountSum2 = redeemAsset(newMember.getPhoneNumber(), transactionID, assetKey, item, item2, "10000");
        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-1000", discountSum2);
        log4j.info("Total discount sum is as expected : " + discountSum2);

    }

    @ru.yandex.qatools.allure.annotations.Description("Test gift with specific items discount special price with activate the gift on time and deactivate after 1 hour, " +
            "make sure the gift discount is as expected")
    @Test
    public void testGiftWithSpecificItemsDiscountSpecialPriceWithExpirationSetting() throws Exception {

        ExpirationSelect expirationSelect = new ExpirationSelect();
        expirationSelect.setAfter(SelectDialog.create().setSelectRadio(SelectDialog.ON_TIME))
                .setUpTo(SelectDialog.create().setSelectRadio(SelectDialog.TIME).setInput("1").setUnits(RollingPeriod.Hour.toString()));

        String specialPrice = "10";
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction(specialPrice, giftsBuilder.selectItemsByItemCode("1111"));
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(setSpecialPriceAction),expirationSelect);

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("7777", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 9000, 2);

        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "19000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-7000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }


    @ru.yandex.qatools.allure.annotations.Description("Test gift with specific items discount free item with activate the gift after 1 hour and deactivate after 2 days" +
            " - Negative test, make sure that the member can't redeem the gift")
    @Test
    public void testGiftWithSpecificItemDiscountFreeItemWithExpirationSettingNegativeTest() throws Exception {

        ExpirationSelect expirationSelect = new ExpirationSelect();
        expirationSelect.setAfter(SelectDialog.create().setSelectRadio(SelectDialog.TIME).setInput("1").setUnits(RollingPeriod.Hour.toString()))
                .setUpTo(SelectDialog.create().setSelectRadio(SelectDialog.TIME).setInput("2").setUnits(RollingPeriod.Day.toString()));

        MakeItemsFreeAction makeItemsFreeAction = buildMakeItemFreeAction(dealBuilder.selectItemsByItemCode("1111"));
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(makeItemsFreeAction),expirationSelect);

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("7777", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("1111", "Prod1", "123", "Dep1", 9000, 2);

        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "19000");

        //Check discount's sum
        Assert.assertEquals("Redeem code not found is expected ", RedeemErrorCodesMap.getErrorMessage(RedeemErrorCodesMap.REDEEM_CODE_NOT_FOUND), discountSum);
        log4j.info("Redeem code not found is expected : " + discountSum);

    }

}
