package hub2_0.gifts;

import com.google.common.collect.Lists;
import hub.hub2_0.basePages.loyalty.gifts.GiftDisplay;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountEntireTicketAction;
import hub.hub2_0.common.actions.dealActions.SetSpecialPriceAction;
import hub.hub2_0.common.items.SelectItems;
import org.junit.Assert;
import org.junit.Test;
import server.v2_8.common.Item;
import java.util.UUID;


public class GiftActionLimitTest extends BaseGiftTest {


    @Test
    public void testEntireTicketDiscount10percentWithLimit() throws Exception {

        String percentDiscount = "50";
        String amountLimit = "100";
        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = buildPercentOffDiscountOnEntireTicket(percentDiscount,NO_ITEM_EXCLUDED, amountLimit);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(percentOffDiscountEntireTicketAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 90000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 50000, 2);
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "140000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-10000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @Test
    public void testEntireTicket5percentDiscountWithLimitAndExcludeItem() throws Exception {

        String percentDiscount = "5";
        String amountLimit = "50";
        SelectItems excludeItems = dealBuilder.selectItemsByItemCode("2222");
        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = buildPercentOffDiscountOnEntireTicket(percentDiscount, excludeItems, amountLimit);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(percentOffDiscountEntireTicketAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 200000, 2);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 80000, 1);
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "280000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-5000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSpecificItemsAmountOffDiscountsWithLimitAmountAndTimes() throws Exception {

        String amountLimit = "80";
        String timesLimit = "2";
        String discountAmount = "50";
        AmountOffDiscountAction amountOffDiscountAction = buildAmountOffDiscount(discountAmount, dealBuilder.selectItemsByItemCode("2222"), amountLimit, timesLimit);
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 20000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 30000, 3);
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "50000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-8000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);

    }

    @Test
    public void testSpecificItemsPercentOffDiscountsWithLimitAmountAndTimes() throws Exception {

        String amountLimit = "50";
        String timesLimit = "1";
        String discountPercent = "20";
        PercentOffDiscountAction percentOffDiscountAction = buildPercentOffDiscount(discountPercent, dealBuilder.selectItemsByItemCode("2222"), amountLimit, timesLimit);
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(percentOffDiscountAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        log4j.info("redeem an asset");
        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 20000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 20000, 2);
        String discountSum = redeemAsset(newMember.getPhoneNumber(), assetKey, item1, item2, "40000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-2000", discountSum);
        log4j.info("Total discount sum is as expected : " + discountSum);
    }

    @Test
    public void testSpecificItemsSpecialPriceDiscountsWithLimitAmountAndTimes() throws Exception {

        String amountLimit = "80";
        String timesLimit = "3";
        String specialPrice = "20";
        SetSpecialPriceAction setSpecialPriceAction = buildSetSpecialPriceAction(specialPrice, dealBuilder.selectItemsByItemCode("2222"), amountLimit, timesLimit);
        newHub2Gift = createSpecificItemsDiscountGift(Lists.newArrayList(), Lists.newArrayList(setSpecialPriceAction));

        createNewMemberAndSendTheGift();

        //Get gift's asset key
        log4j.info("get assetKey");
        String assetKey = serverHelper.getMemberAssetKey(newHub2Gift.getActivityName(), newMember.getPhoneNumber());

        String transactionID = UUID.randomUUID().toString();
        log4j.info("redeem an asset");
        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 10000, 1);
        Item item2 = createItem("2222", "Prod1", "123", "Dep1", 25000, 5);
        String discountSum1 = redeemAsset(newMember.getPhoneNumber(),transactionID, assetKey, item1, item2, "35000");
        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-8000", discountSum1);
        log4j.info("Total discount sum is as expected : " + discountSum1);

        log4j.info("redeem an asset 2");
        Item item3 = createItem("1111", "Prod1", "123", "Dep1", 10000, 1);
        Item item4 = createItem("2222", "Prod1", "123", "Dep1", 20000, 5);
        String discountSum2 = redeemAsset(newMember.getPhoneNumber(),transactionID, assetKey, item3, item4, "30000");

        //Check discount's sum
        Assert.assertEquals("Verify discount's sum is as expected", "-6000", discountSum2);
        log4j.info("Total discount sum is as expected : " + discountSum2);
    }



}
