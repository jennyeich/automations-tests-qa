package hub2_0.utils;

import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.TimeZone;

import hub.hub2_0.common.enums.Days;
import org.apache.log4j.Logger;

public class DateTimeUtility {

    private static final TimeZone timeZone = TimeZone.getTimeZone(System.getProperty("user.timezone"));
    public static final Logger log4j = Logger.getLogger(DateTimeUtility.class);


    public static Days resolveDay(int CurrentDay) {
        Days day=null;
        switch (CurrentDay) {
            case 1:
                day = Days.SUNDAY;
                break;
            case 2:
                day = Days.MONDAY;
                break;
            case 3:
                day = Days.TUESDAY;
                break;
            case 4:
                day = Days.WEDNESDAY;
                break;
            case 5:
                day = Days.THURSDAY;
                break;
            case 6:
                day =Days.FRIDAY;
                break;
            case 7:
                day = Days.SATURDAY;
                break;
        }
        return day;
    }

    public static String getTodayAsNumber(){

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
        dateFormat.setTimeZone(timeZone);
        Calendar cal = Calendar.getInstance();
        String day = dateFormat.format(cal.getTime());
        if(day.startsWith("0"))
            day = day.replace("0","");
        return day;
    }


    public static String getTodayPlusXDays(int numOfDaysToAdd){

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd");
        dateFormat.setTimeZone(timeZone);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_WEEK,numOfDaysToAdd);
        String day = dateFormat.format(cal.getTime());
        if(day.startsWith("0"))
            day = day.replace("0","");
        return day;
    }

    public static String[] getTimePlusXMinutes(int minutesToAdd){

        SimpleDateFormat dateFormat1 = new SimpleDateFormat("hh:mm a");
        dateFormat1.setTimeZone(timeZone);
        Calendar cal = Calendar.getInstance();
        log4j.info("current time is:" + dateFormat1.format(cal.getTime()));
        cal.add(Calendar.MINUTE, minutesToAdd);
        String time = dateFormat1.format(cal.getTime());

        String[] arr = new String[3];
        String currentTime = time.split(" ")[0]; // split the current time from AM/PM
        String amPm = time.split(" ")[1];
        String hh = currentTime.split(":")[0];
        String mm = currentTime.split(":")[1];
        arr[0] = hh;
        arr[1] = mm;
        arr[2] = amPm;
        log4j.info("time after adding " + minutesToAdd + " minutes:" + hh +":" + mm + " " + amPm );
        return arr;
    }

    public static int randomInt(int min, int max) {

        // Usually this can be a field rather than a method variable
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

}
