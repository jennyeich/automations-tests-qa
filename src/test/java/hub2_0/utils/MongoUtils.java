package hub2_0.utils;

import com.google.appengine.repackaged.com.google.gson.Gson;
import common.LocalMongoDB;
import org.apache.log4j.Logger;
import utils.PropsReader;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Set;

public class MongoUtils {

    private static final String goalsJsonFile = "src" + File.separator + "main" + File.separator + "resources" + File.separator + "mongo" + File.separator + "goals.json";
    private static LocalMongoDB localMongoDB;
    private static final String hubDomain = PropsReader.getPropValuesForEnv("hub.domain");

    private static final Logger log4j = Logger.getLogger(MongoUtils.class);

    public static void createGoal(LocalMongoDB mongoDB, String locationId, String goalName, Set<String> ruleProductTemplates, Set<String> dealProductTemplates) throws Exception {

        localMongoDB = mongoDB;

        String urlStr = "https://" + hubDomain + "/api/private/User/Login";
        URL url = new URL(urlStr);
        log4j.info("************Connection Request url: " + urlStr + "****************");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

        StringBuffer buffer = new StringBuffer();
        buffer.append("email=").append(PropsReader.getPropValuesForEnv("user")).append("&password=").append(PropsReader.getPropValuesForEnv("password"));
        String POST_PARAMS = buffer.toString();
        log4j.info("************POST_PARAMS: " + POST_PARAMS + "****************");
        // For POST only - START
        con.setDoOutput(true);
        OutputStream os = con.getOutputStream();
        os.write(POST_PARAMS.getBytes());
        os.flush();
        os.close();
        // For POST only - END

        int responseCode = con.getResponseCode();
        log4j.info("POST Response Code : " + responseCode);

        if (responseCode == HttpURLConnection.HTTP_OK) { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            LoginData loginData = new Gson().fromJson(response.toString(), LoginData.class);
            // Show it.
            String comoToken = loginData.getData().getToken();
            log4j.info("************comoToken: " + comoToken + "****************");

            String templatesRulesPointers = getTemplatesPointers("ruleTemplates", "RuleProductTemplate", ruleProductTemplates);
            //TODO - add support for deal template
            String templatesDealsPointers = getTemplatesPointers("dealTemplates","DealProductTemplate",dealProductTemplates);
            insertGoal(locationId, comoToken, goalName, templatesRulesPointers, templatesDealsPointers);
        } else {
            log4j.info("POST request  failed: " + con.getResponseMessage());
            throw new Exception(con.getResponseMessage());
        }

    }

    public static long deleteGoal(LocalMongoDB mongoDB, String goalName) {
        localMongoDB = mongoDB;
        return localMongoDB.removeGoal(goalName);
    }

    public static void archiveRuleProductTemplates(LocalMongoDB mongoDB) {
        localMongoDB = mongoDB;
        localMongoDB.queryArchiveRuleProductTemplates();
    }

    public static void archiveDealProductTemplates(LocalMongoDB mongoDB) {
        localMongoDB = mongoDB;
        localMongoDB.queryArchiveDealProductTemplates();
    }

    private static String getTemplatesPointers(String collectionName, String filter, Set<String> productTemplates) {
        List<String> templates = localMongoDB.getProductTemplatesByCollection(collectionName, filter, productTemplates);
        StringBuffer buffer = new StringBuffer();
        for (String id : templates) {
            buffer.append("\"").append(id).append("\"").append(",");
        }
        if (buffer.length() > 0)
            buffer = buffer.delete(buffer.length() - 1, buffer.length());
        return buffer.toString();
    }


    private static void insertGoal(String locationId, String comoToken, String goalName, String templatesRulesPointers, String templatesDealsPointers) throws Exception {

        String cookieString = "ComoToken=" + comoToken + "; path=/; domain=." + hubDomain + ";";
        String urlStr = "https://hub2." + hubDomain + "/api/Goals";
        URL url = new URL(urlStr);
        log4j.info("************Creating new goal with request: " + urlStr + "****************");
        HttpURLConnection con1 = (HttpURLConnection) url.openConnection();
        con1.setRequestMethod("POST");
        con1.setRequestProperty("Accept", "application/json");
        con1.setRequestProperty("Content-Type", "application/json");
        con1.setRequestProperty("hub-auth-location-id", locationId);

        con1.setRequestProperty("Cookie", cookieString);

        String body = readFileAsString(goalsJsonFile);
        String POST_PARAMS = String.format(body, goalName, templatesRulesPointers, templatesDealsPointers);

        log4j.info("************Creating new goal with body: " + POST_PARAMS + "****************");
        // For POST only - START
        con1.setDoOutput(true);
        OutputStream os = con1.getOutputStream();
        os.write(POST_PARAMS.getBytes());
        os.flush();
        os.close();
        // For POST only - END

        int responseCode = con1.getResponseCode();
        System.out.println("POST Response Code :: " + responseCode);

        if (responseCode == HttpURLConnection.HTTP_OK) { //success
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    con1.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

        } else {
            System.out.println("POST request not worked");
        }
    }


    private static String readFileAsString(String filePath) throws IOException {
        InputStream is = new FileInputStream(filePath);
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));
        String line = buf.readLine();
        StringBuilder sb = new StringBuilder();
        while (line != null) {
            sb.append(line).append("\n");
            line = buf.readLine();
        }
        return sb.toString();
    }

    private class LoginData {

        private String status;
        private Data data;
        private String code;
        private String codeStatus;


        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public Data getData() {
            return data;
        }

        public void setData(Data data) {
            this.data = data;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCodeStatus() {
            return codeStatus;
        }

        public void setCodeStatus(String codeStatus) {
            this.codeStatus = codeStatus;
        }

        class Data {
            private String token;
            private String location_id;
            private String location_version;


            public String getToken() {
                return token;
            }

            public void setToken(String token) {
                this.token = token;
            }

            public String getLocation_id() {
                return location_id;
            }

            public void setLocation_id(String location_id) {
                this.location_id = location_id;
            }

            public String getLocation_version() {
                return location_version;
            }

            public void setLocation_version(String location_version) {
                this.location_version = location_version;
            }
        }

    }
}
