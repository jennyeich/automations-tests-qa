package hub2_0.utils;

import common.BaseTest;
import hub.common.objects.operations.AppSettingsPage;
import hub.hub1_0.services.OperationService;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.GoalWizard;
import hub.hub2_0.basePages.loyalty.templates.TemplateVerticals;
import hub.hub2_0.services.loyalty.LoyaltyService;
import org.apache.log4j.Logger;

public class ProductTemplateUtility {

    public static final Logger log4j = Logger.getLogger(ProductTemplateUtility.class);

    public static String setBusinessVerticalToService(String locationId) throws Exception {
        BaseTest.sqlDB.setHub2BusinessVertical(locationId, TemplateVerticals.Services.getDBValue());
        return getBusinessVertical();
    }

    public static String getBusinessVertical() throws Exception {
        AppSettingsPage appSettingsPage = new AppSettingsPage();
        OperationService.loadAppSettings(appSettingsPage);
        String vertical = appSettingsPage.getVertical();
        log4j.info("Business vertical is: < " + appSettingsPage.getVertical() + " >");
        return vertical;
    }

    public static String getDifferentVertical(String name) {
        int verticals = TemplateVerticals.values().length;
        for (int i = 0; i < verticals; i++) {
            TemplateVerticals vertical = TemplateVerticals.values()[i];
            if (!vertical.name().equals(name))
                return vertical.name();
        }
        return "Meat Market";
    }

    public static String createActivityFromGoal(String goalName, String productTemplateName, Activity activity) throws Exception {
        GoalWizard goalWizard = new GoalWizard();
        goalWizard.clickOnGoal(goalName, productTemplateName);
        LoyaltyService.createActivityFromGoal(goalWizard);
        String activityName = LoyaltyService.loadFirstActivity();
        activity.clickPublishActivityBtn();
        LoyaltyService.publishDaftActivity(activity);
        log4j.info("Activity created: " + activityName);
        return activityName;
    }

}
