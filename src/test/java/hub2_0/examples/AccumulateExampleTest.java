package hub2_0.examples;

import com.beust.jcommander.internal.Lists;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.actions.PointsAccumulationAction;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import integration.base.BaseRuleHub2Test;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class AccumulateExampleTest extends BaseRuleHub2Test {

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };


    @Test
    public void testPointsAccumulationDefault() throws Exception {
        Box box = ruleBuilder.buildBox("Box_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(InputOperators.IS_EXACTLY)
                .setConditionValue("1214");

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(ActivityActions.PointsAccumulation),"9");

        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
    }


    @Test
    public void testAccumulateByRate() throws Exception {
        Box box = ruleBuilder.buildBox("Box_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(InputOperators.IS_EXACTLY)
                .setConditionValue("1214");

        PointsAccumulationAction pointsAccumulationAction = new PointsAccumulationAction();
        pointsAccumulationAction.clickPointsAccumulationBtn();
        pointsAccumulationAction.setAccumulationType(PointsAccumulationAction.AccumulationType.ACCUMULATE_BY_RATE);
        pointsAccumulationAction.setNumOfPoints("52");
        pointsAccumulationAction.setPointsRate("3");
        pointsAccumulationAction.clickSelectItemsButton();

        SelectItems selectItems = new SelectItems();
        selectItems.clickDepartmentCodesRadioButton();
        selectItems.setDepartmentCodes("412", "356");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        pointsAccumulationAction.setSelectItems(selectItems);
        pointsAccumulationAction.setAccumulationUnits(PointsAccumulationAction.AccumulationUnits.CREDIT);
        pointsAccumulationAction.clickExpandExclude();
        pointsAccumulationAction.clickExcludeItemsCheckbox();
        pointsAccumulationAction.clickExcludeSelectItemsButton();
        pointsAccumulationAction.setSelectItemsToExclude(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(pointsAccumulationAction));

        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.MadeAPurchase;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.MadeAPurchase.toString() +"_";
    }


}
