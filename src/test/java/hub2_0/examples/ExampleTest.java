package hub2_0.examples;

import com.beust.jcommander.internal.Lists;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.cases.RuleCase;
import hub.hub2_0.basePages.loyalty.gifts.Gift;
import hub.hub2_0.basePages.loyalty.gifts.GiftDisplay;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.DelayAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.builders.GiftsBuilder;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.PointsCondition;
import hub.hub2_0.common.conditions.cases.*;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.*;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.limits.RollingPeriod;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.MembershipStatus;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.conditions.operators.ShoppingCartOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.common.items.*;
import hub.hub2_0.services.loyalty.EditService;
import hub.hub2_0.services.loyalty.Gifts.GiftsGalleryService;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.hub2_0.common.builders.CasesConditionsBuilder;
import hub2_0.gifts.BaseGiftTest;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.Assert;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Arrays;
import java.util.Collections;


public class ExampleTest extends BaseRuleHub2Test {

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };


    // @Test
    public void testNoConditionTagMemberWithDelay() throws Exception {
        Box box = ruleBuilder.buildBox("testGLC_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        ActivityAction activityAction = ActionsBuilder.buildTagMemberAction(timestamp.toString());
        DelayAction delayAction = new DelayAction();
        delayAction.selectDelayType(DelayAction.AFTER).setAfterInput("2").setAfterUnits(DelayUnits.Seconds).clickFinish(DelayAction.SAVE);
        activityAction.setDelayAction(delayAction);

        ActivityAction activityAction2 = ActionsBuilder.buildTagMemberAction("tag2");
        DelayAction delayAction2 = new DelayAction();
        delayAction2.selectDelayType(DelayAction.AFTER).setAfterInput("3").setAfterUnits(DelayUnits.Hour).clickFinish(DelayAction.SAVE);
        activityAction2.setDelayAction(delayAction2);
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(activityAction, activityAction2));
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
//        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
//        key = hubMemberCreator.getMemberKeyToUserKey();
//        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
//        checkAutomationRunAndTagMember();
//        log4j.info("member received the tag");
//        log4j.info(tag + " was added to the member ");

    }


    @Test
    public void testRuleWithGlobalCondition() throws Exception {
        Box box = ruleBuilder.buildBox("testGLC_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, ActivityTrigger.JoinTheClub.toString() + timestamp);


        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.AllowEmail.toString()).toggleBooleanValue(ActivityCondition.BOOLEAN_YES);
        MemberFieldCondition memberFieldCondition1 = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.AllowSMS.toString()).toggleBooleanValue(ActivityCondition.BOOLEAN_YES);
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition, memberFieldCondition1);
        specificMembers.setMembershipStatus(MembershipStatus.newMembershipStatus().setRegister(MembershipStatus.CHECK).setPending(MembershipStatus.UNCHECK));
        globalCondition.setSpecificMembers(specificMembers);
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.JoinTheClub.toString(), box, rule, Arrays.asList(globalCondition), Lists.newArrayList(ActivityActions.Tag), timestamp.toString());
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);

        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        log4j.info("automationRuleId: " + automationRuleId);

    }


    // example of global condition of Limit (Infinite/Limited number)
    //@Test
    public void testRuleWithGlobalConditionLimits() throws Exception {

        Box box = ruleBuilder.buildBox("testGLC_Limits_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, ActivityTrigger.JoinTheClub.toString() + timestamp);
        LoyaltyService.createBox(box);
        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        // example of Limit of period other than Rolling period.
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("4").setUpToTimesByPeriod(true).setTimePerPeriodVal("2").setTimesByPeriod(RollingPeriod.Month.toString());
        // example of rolling period
        //LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("10").setUpToTimesByPeriod(true).setTimePerPeriodVal("8").setTimesByPeriod(RollingPeriod.RollingPeriod.toString()).setPeriodMultiplier("5");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.JoinTheClub.toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.Tag), timestamp.toString());
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        log4j.info("automationRuleId: " + automationRuleId);
    }

    //@Test
    public void testRuleWithGlobalConditionDaysAndTime() throws Exception {
        Box box = ruleBuilder.buildBox("testGLC_DT_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, ActivityTrigger.JoinTheClub.toString() + timestamp);
        LoyaltyService.createBox(box);

        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();

        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());
        DatePicker datePickerEnd = DatePicker.newDatePicker().setSelectEndDay(DateTimeUtility.getTodayPlusXDays(2));
        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(Days.SATURDAY, Days.WEDNESDAY).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        DaysCondition daysCondition1 = DaysCondition.newDayCondition().unSelectDays(Days.MONDAY, Days.THURSDAY)
                .setTimeSelector(TimeSelector.newTimeSelector().setFromHourInput("00").setFromMinuteInput("01").setFromAmPm(TimeSelector.TimeMeridiem.AM).
                        setToHourInput("11").setToMinuteInput("59").setToAmPm(TimeSelector.TimeMeridiem.PM));

        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition, daysCondition1).setDatePickerStart(datePickerStart).setDatePickerEnd(datePickerEnd);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.JoinTheClub.toString(), box, rule, Arrays.asList(dateTimeGlobalCondition), Lists.newArrayList(ActivityActions.Tag), timestamp.toString());
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        log4j.info("automationRuleId: " + automationRuleId);

    }

    // @Test
    public void testRuleWithGlobalConditionDaysAndTime2() throws Exception {
        Box box = ruleBuilder.buildBox("testGLC_DT_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, ActivityTrigger.JoinTheClub.toString() + timestamp);
        LoyaltyService.createBox(box);

        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().clickStartNow();
        DatePicker datePickerEnd = DatePicker.newDatePicker().clickEndForever();
        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(Days.SATURDAY, Days.WEDNESDAY).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        DaysCondition daysCondition1 = DaysCondition.newDayCondition().unSelectDays(Days.MONDAY, Days.THURSDAY);

        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition, daysCondition1).setDatePickerStart(datePickerStart).setDatePickerEnd(datePickerEnd);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.JoinTheClub.toString(), box, rule, Arrays.asList(dateTimeGlobalCondition), Lists.newArrayList(ActivityActions.Tag), timestamp.toString());
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        log4j.info("automationRuleId: " + automationRuleId);


    }

    // @Test
    public void testEditSameAction() throws Exception {
        Box box = ruleBuilder.buildBox("testEditSameAction");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        LoyaltyService.createBox(box);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Arrays.asList(), Lists.newArrayList(ActivityActions.SendTextMessage), timestamp.toString());
        //execute the selenium job
        LoyaltyService.createBox(box);


        //edit similiar activity
        ActivityAction activityAction2 = ActionsBuilder.buildAction(ActivityActions.SendTextMessage.toString(), "update sms");
        EditService.editActivityAction(activity, activityAction2, 1);
        Assert.assertTrue(true);

    }

    // @Test
    public void testEditAction() throws Exception {
        Box box = ruleBuilder.buildBox("testEditAction");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());


        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Arrays.asList(), Lists.newArrayList(ActivityActions.Tag), timestamp.toString());
        //execute the selenium job
        LoyaltyService.createBox(box);

        //edit different activity
        ActivityAction activityAction2 = ActionsBuilder.buildAction(ActivityActions.SendAPushNotification.toString(), "push");
        EditService.editActivityAction(activity, activityAction2, 1);
        Assert.assertTrue(true);
    }

    // @Test
    public void testEditAction2() throws Exception {
        Box box = ruleBuilder.buildBox("testEditAction");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());


        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Arrays.asList(), Lists.newArrayList(ActivityActions.Tag, ActivityActions.SendTextMessage), timestamp.toString());
        //execute the selenium job
        LoyaltyService.createBox(box);

        //edit different activity
        ActivityAction activityAction2 = ActionsBuilder.buildAction(ActivityActions.SendAPushNotification.toString(), "push");
        EditService.editActivityAction(activity, activityAction2, 2);
        Assert.assertTrue(true);
    }

    // @Test
    public void testEditCondition() throws Exception {
        Box box = ruleBuilder.buildBox("testEditCondition");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("10");

        MadeAPurchaseCondition condition2 = new MadeAPurchaseCondition();
        condition2.setConditionKey(MadeAPurchaseCondition.ConditionKey.NUMBER_OF_MEMBERS)
                .setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("22");

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Arrays.asList(condition, condition2), Lists.newArrayList(ActivityActions.Tag), timestamp.toString());


        //execute the selenium job
        LoyaltyService.createBox(box);

        MadeAPurchaseCondition conditionNew = new MadeAPurchaseCondition();
        conditionNew.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("1");
        EditService.editActivityCondition(activity, conditionNew, 2);
    }

    //@Test
    public void testRuleActivity() throws Exception {
        Box box = ruleBuilder.buildBox("Conditions example1");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        LoyaltyService.createBox(box);
        // **********************************************************************
        // To set a new condition we need:
        // 1. create an instance of the wanted condition
        // 2. Set Condition key, Condition Operator, Condition value.
        // 3. Send the conditions list to buildActivity method.
        //  Each condition contains the set of condition keys inside the class.
        //  Operators are in different class, splited by types:  InputOperators, IntOperators,
        //  ListOperators, BooleanOperators, DateOperators, ShoppingCartOperators
        //************************************************************************

//        PunchAPunchCardCondition condition1 = new PunchAPunchCardCondition();
//        condition1.setConditionKey(PunchAPunchCardCondition.ConditionKey.NUMBER_OF_PUNCHES)
//                .setOperatorKey(IntOperators.IS_GREATER_THAN)
//                .setConditionValue("5");
//
//        PunchAPunchCardCondition condition2 = new PunchAPunchCardCondition();
//        condition2.setConditionKey(PunchAPunchCardCondition.ConditionKey.BENEFIT)
//                .setOperatorKey(ListOperators.IS_ONE_OF)
//                .setBenefitName("myGift");

//        PunchAPunchCardCondition condition3 = new PunchAPunchCardCondition();
//        condition3.setConditionKey(PunchAPunchCardCondition.ConditionKey.IS_LAST_PUNCH)
//                .setOperatorKey(BooleanOperators.IS_NOT)
//                .toggleIsLastPunch();

//        ExternalEventSubmittedCondition externalEventSubmittedCondition = new ExternalEventSubmittedCondition();
//        externalEventSubmittedCondition.setConditionKey(ExternalEventSubmittedCondition.ConditionKey.DATE_VALUE_1)
//                .setOperatorKey(DateOperators.DATE_IS_AFTER)
//                .setConditionValue("12122025");

        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        CodesGroup itemsCodesGroup = new CodesGroup();
        itemsCodesGroup.setCodes(CodesGroup.ITEM_CODES_GROUP, "111", "222", "333");
        selectItems.setItemCodes(itemsCodesGroup);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("1")
                .setSelectItems(selectItems);

        SelectItems selectItems2 = new SelectItems();
        selectItems2.clickDepartmentCodesRadioButton();
        CodesGroup itemsDepartmentGroup = new CodesGroup();
        itemsDepartmentGroup.setCodes(CodesGroup.DEPARTMENT_CODES_GROUP, "aaa", "bbb");
        selectItems2.setDepartmentCodes(itemsDepartmentGroup);
        selectItems2.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition2 = new MadeAPurchaseCondition();
        condition2.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_MORE_THAN)
                .setConditionValue("1")
                .setSelectItems(selectItems2);

//        SelectItems selectItems3 = new SelectItems();
//        selectItems3.clickDepartmentCodesRadioButton();
//        selectItems3.setDepartmentCodes("aaa", "bbb");
//        selectItems3.clickButton(SelectedItemsDialogButtons.Apply.toString());

//        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
//        occurrencesCondition.setSelectItemsDialog(selectItems3);
//        occurrencesCondition.setConditionKey(OccurrencesCondition.Condition.IN_THE_TOTAL_SPEND_OF);
//        occurrencesCondition.setConditionValue("5");


        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(condition, condition2), Lists.newArrayList(ActionsBuilder.buildTagMemberAction("GONI")));
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");


//        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
//        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
//                .setOperatorKey(InputOperators.CONTAINS)
//                .setConditionValue("1514");


//        ****PointsCondition relevant for both Receive points and Use Points triggers****
//        PointsCondition receivesPointsCondition = new PointsCondition();
//        receivesPointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS)
//                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
//                .setConditionValue("55");


//        ExternalEventSubmittedCondition externalEventSubmittedCondition = new ExternalEventSubmittedCondition();
//        externalEventSubmittedCondition.setConditionKey(ExternalEventSubmittedCondition.ConditionKey.DATE_VALUE_1)
//                .setOperatorKey(DateOperators.DATE_IS_AFTER)
//                .setConditionValue("12122025");

//        PaidWithComoWalletCondition paidWithComoWalletCondition = new PaidWithComoWalletCondition();
//        paidWithComoWalletCondition.setConditionKey(PaidWithComoWalletCondition.ConditionKey.PAYMENT_TYPE)
//                .setOperatorKey(ListOperators.IS_ONE_OF)
//                .setPaymentType(PaidWithComoWalletCondition.PaymentType.GIFT_CARD);

        // ** external payment relevant for "External payment succeeded" and "External payment rejected"  **//
//        ExternalPaymentCondition externalPaymentCondition = new ExternalPaymentCondition();
//        externalPaymentCondition.setConditionKey(ExternalPaymentCondition.ConditionKey.PROVIDER_NAME)
//                .setOperatorKey(ListOperators.IS_EXACTLY)
//                .setProvider(ExternalPaymentCondition.Provider.ZOOZ);


//        ReceivedBeaconSignalCondition receivedBeaconSignalCondition  = new ReceivedBeaconSignalCondition();
//        receivedBeaconSignalCondition.setConditionKey(ReceivedBeaconSignalCondition.ConditionKey.UUID)
//                .setOperatorKey(ListOperators.IS_ONE_OF)
//                .setConditionValue("test");

    }

    //@Test
    public void testRuleWithAdvancedRuleCondition() throws Exception {
        Box box = ruleBuilder.buildBox("Conditions example1");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        LoyaltyService.createBox(box);
        AdvancedRuleCondition advancedRuleCondition = new AdvancedRuleCondition();
        advancedRuleCondition.setConditionKey(AdvancedRuleConditionKeys.ItemCode.toString())
                .setOperatorKey(InputOperators.IS_EXACTLY)
                .setConditionValue("11111");
        AdvancedRuleCondition advancedRuleCondition1 = new AdvancedRuleCondition();
        advancedRuleCondition1.setConditionKey(AdvancedRuleConditionKeys.DepartmentCode.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("33333,44444");

        SelectItems selectItems = new SelectItems();
        selectItems.clickAdvancedRulesRadioButton();
        AdvancedRule advancedRule = AdvancedRule.newRule().setAdvancedRuleConditions(advancedRuleCondition, advancedRuleCondition1)
                .setConditionRelations(ConditionRelations.Any.toString());
        selectItems.setAdvancedRule(advancedRule);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("1")
                .setSelectItems(selectItems);


        AdvancedRuleCondition advancedRuleCondition2 = new AdvancedRuleCondition();
        advancedRuleCondition2.setConditionKey(AdvancedRuleConditionKeys.ItemCode.toString())
                .setOperatorKey(InputOperators.IS_EXACTLY)
                .setConditionValue("2222");

        SelectItems selectItems2 = new SelectItems();
        selectItems2.clickAdvancedRulesRadioButton();
        AdvancedRule advancedRule2 = AdvancedRule.newRule().setAdvancedRuleConditions(advancedRuleCondition2)
                .setConditionRelations(ConditionRelations.All.toString());
        selectItems2.setAdvancedRule(advancedRule2);
        selectItems2.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition2 = new MadeAPurchaseCondition();
        condition2.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_MORE_THAN)
                .setConditionValue("1")
                .setSelectItems(selectItems2);

        SelectItems selectItems3 = new SelectItems();
        selectItems3.clickItemCodesRadioButton();
        CodesGroup itemCodes = new CodesGroup();
        itemCodes.setCodes(CodesGroup.ITEM_CODES_GROUP, "111", "222", "333");
        selectItems3.setItemCodes(itemCodes);
        selectItems3.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition3 = new MadeAPurchaseCondition();
        condition3.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("1")
                .setSelectItems(selectItems3);

        SelectItems selectItems4 = new SelectItems();
        selectItems4.clickDepartmentCodesRadioButton();
        CodesGroup departmentCodes = new CodesGroup();
        departmentCodes.setCodes(CodesGroup.DEPARTMENT_CODES_GROUP, "aaa", "bbb");
        selectItems4.setDepartmentCodes(departmentCodes);
        selectItems4.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition4 = new MadeAPurchaseCondition();
        condition4.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_MORE_THAN)
                .setConditionValue("1")
                .setSelectItems(selectItems4);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(condition, condition2, condition3, condition4), Lists.newArrayList(ActionsBuilder.buildTagMemberAction("GONI")));
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
    }


    //@Test
    public void testRuleWithAdvancedRuleAndDepartmentCodesSaveAsGroups() throws Exception {
        Box box = ruleBuilder.buildBox("Box_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        AdvancedRuleCondition advancedRuleCondition = new AdvancedRuleCondition();
        advancedRuleCondition.setConditionKey(AdvancedRuleConditionKeys.ItemCode.toString())
                .setOperatorKey(InputOperators.IS_EXACTLY)
                .setConditionValue("11111");


        SelectItems selectItems = new SelectItems();
        selectItems.clickAdvancedRulesRadioButton();
        AdvancedRule advancedRule = AdvancedRule.newRule().setAdvancedRuleConditions(advancedRuleCondition)
                .setConditionRelations(ConditionRelations.Any.toString());
        advancedRule.clickSaveAsGroupBtn("group_" + timestamp, NewGroup.SAVE);
        selectItems.setAdvancedRule(advancedRule);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("1")
                .setSelectItems(selectItems);

        SelectItems selectItems1 = new SelectItems();
        selectItems1.clickDepartmentCodesRadioButton();
        CodesGroup departmentCodes = new CodesGroup();
        departmentCodes.setCodes(CodesGroup.DEPARTMENT_CODES_GROUP, "aaa", "bbb");
        departmentCodes.clickSaveAsGroupBtn("department_" + timestamp, NewGroup.SAVE);
        selectItems1.setDepartmentCodes(departmentCodes);
        selectItems1.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition4 = new MadeAPurchaseCondition();
        condition4.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_MORE_THAN)
                .setConditionValue("1")
                .setSelectItems(selectItems1);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(condition, condition4), Lists.newArrayList(ActionsBuilder.buildTagMemberAction("GONI22222")));
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
    }

    //@Test
    public void testRuleWithDepartmentCodesSaveAsGroupsAndOtherConditionBasedOnGroup() throws Exception {
        Box box = ruleBuilder.buildBox("Box_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());


        SelectItems selectItems = new SelectItems();
        selectItems.clickDepartmentCodesRadioButton();
        CodesGroup departmentCodes = new CodesGroup();
        departmentCodes.setCodes(CodesGroup.DEPARTMENT_CODES_GROUP, "aaa", "bbb");
        String groupName = "group_" + timestamp;
        departmentCodes.clickSaveAsGroupBtn(groupName, NewGroup.SAVE);
        selectItems.setDepartmentCodes(departmentCodes);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("1")
                .setSelectItems(selectItems);

        SelectItems selectItems1 = new SelectItems();
        selectItems1.clickPredefinedGroupsRadioButton();
        selectItems1.setPreDefinedGroup(new PreDefinedGroups(groupName));
        selectItems1.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition4 = new MadeAPurchaseCondition();
        condition4.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_MORE_THAN)
                .setConditionValue("1")
                .setSelectItems(selectItems1);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(condition, condition4), Lists.newArrayList(ActionsBuilder.buildTagMemberAction("GONI22222")));
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
    }


    //@Test
    public void testRuleActivityPerOccurrencesWithActionLimit() throws Exception {

        Box box = ruleBuilder.buildBox("PerOccurrences");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        LoyaltyService.createBox(box);

        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        CodesGroup itemsCodesGroup = new CodesGroup();
        itemsCodesGroup.setCodes(CodesGroup.ITEM_CODES_GROUP, "111", "222", "333");
        selectItems.setItemCodes(itemsCodesGroup);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("1")
                .setSelectItems(selectItems);


        SelectItems selectItems2 = new SelectItems();
        selectItems2.clickDepartmentCodesRadioButton();
        selectItems2.setDepartmentCodes("aaa", "bbb");
        selectItems2.clickButton(SelectedItemsDialogButtons.Apply.toString());

        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItems2);
        occurrencesCondition.setConditionKey(OccurrencesCondition.Condition.IN_THE_TOTAL_SPEND_OF);
        occurrencesCondition.setConditionValue("5");


        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Collections.singletonList(condition), Collections.singletonList(occurrencesCondition), Lists.newArrayList(ActionsBuilder.buildTagMemberAction("GONI")));
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
    }

    @Test
    public void testSplitCasesWithDatesAndTimeConditions() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        Box box = ruleBuilder.buildBox("splitCases_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        LoyaltyService.createBox(box);


        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(Days.SATURDAY, Days.WEDNESDAY).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        DaysCondition daysCondition1 = DaysCondition.newDayCondition().setTimeSelector(TimeSelector.newTimeSelector().setFromHourInput("00").setFromMinuteInput("01").setFromAmPm(TimeSelector.TimeMeridiem.AM).
                setToHourInput("11").setToMinuteInput("59").setToAmPm(TimeSelector.TimeMeridiem.PM));


        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());
        DatePicker datePickerEnd = DatePicker.newDatePicker().setSelectEndDay(DateTimeUtility.getTodayPlusXDays(2));
        BetweenDates betweenDates = BetweenDates.newBetweenDates().setDatePickerStart(datePickerStart).setDatePickerEnd(datePickerEnd);
        DatesAndTimeCondition datesAndTimeCondition = new DatesAndTimeCondition();
        datesAndTimeCondition.setBetweenDates(betweenDates);

        DatesAndTimeCondition datesAndTimeCondition1 = new DatesAndTimeCondition();
        datesAndTimeCondition1.setOnDays(daysCondition, daysCondition1);

        RuleCase aCase = new RuleCase();
        aCase.setConditions(Lists.newArrayList(datesAndTimeCondition, datesAndTimeCondition1));


        ActivityAction activityAction = ActionsBuilder.buildTagMemberAction("GONI_" + tag);

        aCase.setActions(Lists.newArrayList(activityAction));
        Activity activity = ruleBuilder.buildActivityWithCases(ActivityTrigger.UsePoints.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(aCase));

        log4j.info("rule name:" + rule.getActivityName());


        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

    }

    @Test
    public void testSplitCasesWithGeneralCondition() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        Box box = ruleBuilder.buildBox("splitCases_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        LoyaltyService.createBox(box);


        /*********general condition***************/
        PointsCondition receivesPointsCondition = new PointsCondition();
        receivesPointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("55");

        GeneralCondition generalCondition = new CasePointsCondition();
        generalCondition.setGeneralCondition(receivesPointsCondition);
        RuleCase aCase = new RuleCase();
        aCase.setConditions(Lists.newArrayList(generalCondition));

        ActivityAction activityAction = ActionsBuilder.buildTagMemberAction("GONI_" + tag);

        aCase.setActions(Lists.newArrayList(activityAction));
        Activity activity = ruleBuilder.buildActivityWithCases(ActivityTrigger.UsePoints.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(aCase));

        log4j.info("rule name:" + rule.getActivityName());


        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

    }

    @Test
    public void testSplitCasesWith2Cases() throws Exception {


        tag = ActionsBuilder.TAG + timestamp.toString();
        Box box = ruleBuilder.buildBox("splitCases_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        LoyaltyService.createBox(box);

        /*********general condition***************/
        AdvancedRuleCondition advancedRuleCondition = new AdvancedRuleCondition();
        advancedRuleCondition.setConditionKey(AdvancedRuleConditionKeys.ItemCode.toString())
                .setOperatorKey(InputOperators.IS_EXACTLY)
                .setConditionValue("11111");
        AdvancedRuleCondition advancedRuleCondition1 = new AdvancedRuleCondition();
        advancedRuleCondition1.setConditionKey(AdvancedRuleConditionKeys.DepartmentCode.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("33333,44444");

        SelectItems selectItems = new SelectItems();
        selectItems.clickAdvancedRulesRadioButton();
        AdvancedRule advancedRule = AdvancedRule.newRule().setAdvancedRuleConditions(advancedRuleCondition, advancedRuleCondition1)
                .setConditionRelations(ConditionRelations.Any.toString());
        selectItems.setAdvancedRule(advancedRule);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("1")
                .setSelectItems(selectItems);

        GeneralCondition generalCondition = new CaseMadeAPurchaseCondition();
        generalCondition.setGeneralCondition(condition);
        RuleCase aCase = new RuleCase();
        aCase.setConditions(Lists.newArrayList(generalCondition));

        ActivityAction activityAction = ActionsBuilder.buildSendTextMessageAction("GONI_send-sms_" + tag);

        aCase.setActions(Lists.newArrayList(activityAction));


        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());
        DatePicker datePickerEnd = DatePicker.newDatePicker().setSelectEndDay(DateTimeUtility.getTodayPlusXDays(2));
        BetweenDates betweenDates = BetweenDates.newBetweenDates().setDatePickerStart(datePickerStart).setDatePickerEnd(datePickerEnd);
        DatesAndTimeCondition datesAndTimeCondition = new DatesAndTimeCondition();
        datesAndTimeCondition.setBetweenDates(betweenDates);


        RuleCase aCase1 = new RuleCase();
        aCase1.setConditions(Lists.newArrayList(datesAndTimeCondition));

        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction("GONI_" + tag);
        aCase1.setActions(Lists.newArrayList(activityAction1));


        Activity activity = ruleBuilder.buildActivityWithCases(ActivityTrigger.MadeAPurchase.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(aCase, aCase1));

        log4j.info("rule name:" + rule.getActivityName());


        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");


    }

    @Test
    public void testSplitCasesWithMembersCondition() throws Exception {


        tag = ActionsBuilder.TAG + timestamp.toString();
        Box box = ruleBuilder.buildBox("splitCases_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        LoyaltyService.createBox(box);


        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.AllowEmail.toString()).toggleBooleanValue(ActivityCondition.BOOLEAN_YES);


        MembersCondition membersCondition = new MembersCondition();
        membersCondition.setMembersCondition(memberFieldCondition);

        MemberFieldCondition memberFieldCondition1 = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.AllowSMS.toString()).toggleBooleanValue(ActivityCondition.BOOLEAN_YES);

        MembersCondition membersCondition1 = new MembersCondition();
        membersCondition1.setMembersCondition(memberFieldCondition1);

        RuleCase aCase = new RuleCase();
        aCase.setConditions(Lists.newArrayList(membersCondition, membersCondition1));


        ActivityAction activityAction = ActionsBuilder.buildTagMemberAction("GONI_" + tag);

        aCase.setActions(Lists.newArrayList(activityAction));
        Activity activity = ruleBuilder.buildActivityWithCases(ActivityTrigger.UsePoints.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(aCase));

        log4j.info("rule name:" + rule.getActivityName());


        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

    }

    @Test
    public void testSplitCasesWith2CasesAndTriggerCondition() throws Exception {


        tag = ActionsBuilder.TAG + timestamp.toString();
        Box box = ruleBuilder.buildBox("splitCases_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        LoyaltyService.createBox(box);

        /***********global condition***************/
        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.AllowEmail.toString()).toggleBooleanValue(ActivityCondition.BOOLEAN_YES);
        MemberFieldCondition memberFieldCondition1 = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.AllowSMS.toString()).toggleBooleanValue(ActivityCondition.BOOLEAN_YES);
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition, memberFieldCondition1);
        specificMembers.setMembershipStatus(MembershipStatus.newMembershipStatus().setRegister(MembershipStatus.CHECK).setPending(MembershipStatus.UNCHECK));
        globalCondition.setSpecificMembers(specificMembers);

        /***********activity condition***************/
        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("20");


        /*********general condition***************/
        GeneralCondition generalCondition = CasesConditionsBuilder.buildGeneralConditionPoints(PointsCondition.ConditionKey.NUMBER_OF_POINTS, IntOperators.IS_GREATER_THAN, "20");

        MemberFieldCondition memberFieldConditionCase = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.AllowEmail.toString()).toggleBooleanValue(ActivityCondition.BOOLEAN_YES);


        MembersCondition membersCondition = new MembersCondition();
        membersCondition.setMembersCondition(memberFieldConditionCase);


        RuleCase aCase = new RuleCase();
        aCase.setConditions(Lists.newArrayList(generalCondition, membersCondition));

        ActivityAction activityAction = ActionsBuilder.buildSendTextMessageAction("GONI_send-sms_" + tag);

        aCase.setActions(Lists.newArrayList(activityAction));


        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());
        DatePicker datePickerEnd = DatePicker.newDatePicker().setSelectEndDay(DateTimeUtility.getTodayPlusXDays(2));
        BetweenDates betweenDates = BetweenDates.newBetweenDates().setDatePickerStart(datePickerStart).setDatePickerEnd(datePickerEnd);
        DatesAndTimeCondition datesAndTimeCondition = new DatesAndTimeCondition();
        datesAndTimeCondition.setBetweenDates(betweenDates);

        RuleCase aCase1 = new RuleCase();
        aCase1.setConditions(Lists.newArrayList(datesAndTimeCondition));

        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction("GONI_" + tag);
        aCase1.setActions(Lists.newArrayList(activityAction1));


        Activity activity = ruleBuilder.buildActivityWithCases(ActivityTrigger.UsePoints.toString(), box, rule, Lists.newArrayList(globalCondition, pointsCondition), Lists.newArrayList(aCase, aCase1));

        log4j.info("rule name:" + rule.getActivityName());


        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");


    }

    @Test
    public void testSplitCasesWithMembersConditionPerOccurrencesWithActionLimit() throws Exception {


        tag = ActionsBuilder.TAG + timestamp.toString();
        Box box = ruleBuilder.buildBox("splitCases_" + timestamp);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        LoyaltyService.createBox(box);


//        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
//                setFieldsSelect(MemberNonRegistrationFormFields.AllowEmail.toString()).toggleBooleanValue(ActivityCondition.BOOLEAN_YES);
//
//
//        MembersCondition membersCondition = new MembersCondition();
//        membersCondition.setMembersCondition(memberFieldCondition);

        SelectItems selectItems2 = new SelectItems();
        selectItems2.clickDepartmentCodesRadioButton();
        selectItems2.setDepartmentCodes("aaa", "bbb");
        selectItems2.clickButton(SelectedItemsDialogButtons.Apply.toString());

        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItems2);
        occurrencesCondition.setConditionKey(OccurrencesCondition.Condition.IN_THE_TOTAL_SPEND_OF);
        occurrencesCondition.setConditionValue("5");

        RuleCase aCase = new RuleCase();
        // aCase.setConditions(Lists.newArrayList(membersCondition));
        aCase.setOccurrencesConditions(Lists.newArrayList(occurrencesCondition));

        ActivityAction activityAction = ActionsBuilder.buildTagMemberAction("GONI_" + tag);

        aCase.setActions(Lists.newArrayList(activityAction));
        Activity activity = ruleBuilder.buildActivityWithCases(ActivityTrigger.MadeAPurchase.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(aCase));

        log4j.info("rule name:" + rule.getActivityName());


        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

    }


    @Test
    public void giftGalleryTest() throws Exception {

        GiftsGalleryService.deleteImage("gift2","cat.jpg");
        GiftsGalleryService.uploadFile("gift2","cat.jpg");
        GiftsGalleryService.isImageExists("entireTicketGift_1532950943467","entireTicketGift_1532942296482");
        String amountDiscount = "50";
        GiftsBuilder giftsBuilder = new GiftsBuilder();
        GiftDisplay giftDisplay = giftsBuilder.newDisplay().setGiftDescription("This is gift for you!").setGiftName("your gift");
        // need to create the Gift with the  giftDisplay
}

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.MadeAPurchase;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.MadeAPurchase.toString() +"_";
    }

}
