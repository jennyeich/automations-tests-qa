package hub2_0.examples;

import com.beust.jcommander.internal.Lists;
import edu.emory.mathcs.backport.java.util.Collections;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.templates.TemplatesLibrary;
import hub.hub2_0.common.actions.SendBenefitAction;
import hub.hub2_0.common.actions.TagUntagMemberAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.ActivityActions;
import hub.hub2_0.common.enums.ActivityStatus;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.ActivityType;
import hub.hub2_0.services.loyalty.LoyaltyService;
import integration.base.BaseRuleHub2Test;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Arrays;

public class TemplatesExampleTest extends BaseRuleHub2Test {

    private static Box box;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);

    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    // start from scratch- create as usual, and save rule as template.
    @Test
    public void testTemplateStartFromScratchAndSaveToTemplate() throws Exception {
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, "madeAPurchaseRule1");

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("1214");

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(), box, rule, Arrays.asList(condition), Lists.newArrayList(ActivityActions.Tag), timestamp.toString());
        activity.clickSaveAsTemplateAndSave("MadeAPurchaseTemplate1", "This is Make a purchase template with BranchID condition and tag action.");
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
    }


    //start from template without making changes.
    @Test
    public void testTemplateCreateRuleFromTemplate() throws Exception {
        //setting null is possible in order to use rule name from template.
        // when setting name to rule which created from template, the name will be concat to the rule name in template.
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, "_updated1");

        //only update the name and publish
        Activity activity = ruleBuilder.buildActivity(null, box, rule, Collections.emptyList(), Collections.emptyList());
        //execute the selenium job - template created in previous test
        LoyaltyService.createRuleFromTemplateInBox(activity, "MadeAPurchaseTemplate1");
    }


    @Test
    public void testTemplateLoadAnother() throws Exception {

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.MadeAPurchase.toString(), null);
        Activity activity = ruleBuilder.buildActivity(null, box, rule, Collections.emptyList(), Collections.emptyList());
        activity.clickSaveAsProductTemplate("purim", null, "productTemplate", "this is product template");
        activity.clickLoadAnother();
        //execute the selenium job - load from template and click load another - back to Templates library.
        LoyaltyService.createRuleFromTemplateInBox(activity, "MadeAPurchaseTemplate1");

        TemplatesLibrary.clickBack();
    }

    @Test
    public void testTemplateSaveAsTemplate() throws Exception {
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.MadeAPurchase.toString(), null);

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("123");
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(), box, rule, Collections.emptyList(), Lists.newArrayList(ActivityActions.Tag, ActivityActions.SendBenefit), null);
        activity.clickSaveAsTemplateAndSave("myTemplate", "Some description that no one reads.");

        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();

        // Back in loyalty tab
        Rule rule2 = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.MadeAPurchase.toString(), null);
        Activity activity2 = ruleBuilder.buildActivity(ActivityTrigger.JoinTheClub.toString(), box, rule2, Collections.emptyList(), Lists.newArrayList(ActivityActions.Tag), "testSaveAndReplace");
        activity2.clickSaveAsTemplateAndReplace(null, "new description");


        LoyaltyService.createRuleFromTemplateInBox(activity2, "myTemplate");
        activity2.clickBack();
    }

    @Test
    public void testTemplateEditTemplateNameAndDeleteTemplate() throws Exception {

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.MadeAPurchase.toString(), "some description");

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("1214");

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(), box, rule, Arrays.asList(condition), Lists.newArrayList(ActivityActions.Tag), timestamp.toString());
        activity.clickSaveAsTemplateAndSave("MadeAPurchaseTemplate5", "This is Make a purchase template with BranchID condition and tag action.");
        activity.clickCancel(); // dont save the rule.
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);

        // Back in Loyalty tab.
        LoyaltyService.clickAddActivity(activity);
        TemplatesLibrary.editTemplateName("MadeAPurchaseTemplate5", "templateToDelete", "This template will be deleted in the next step.");
        TemplatesLibrary.deleteTemplate("templateToDelete");

    }

    @Test
    public void testTemplateLoadFromTemplateAndFillData() throws Exception {

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.MadeAPurchase.toString(), null);

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("66");
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(), box, rule, Collections.emptyList(), Lists.newArrayList(ActivityActions.Tag, ActivityActions.SendBenefit), null);
        activity.clickSaveAsTemplateAndSave("uncompletedTemplate", "Some description... ");

        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();


        Rule rule2 = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, "updatedRule");

        MadeAPurchaseCondition condition2 = new MadeAPurchaseCondition();
        condition2.setConditionValue("555");
        MadeAPurchaseCondition condition3 = new MadeAPurchaseCondition();
        condition3.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("63");
        //only update the name and publish

        TagUntagMemberAction tagUntagMemberAction = new TagUntagMemberAction();
        tagUntagMemberAction.setTagText("testUncompletedTemplate");

        SendBenefitAction sendBenefitAction = new SendBenefitAction();
        sendBenefitAction.setSelectBenefit("MyGift");

        Activity activity2 = ruleBuilder.buildActivity(null, box, rule2, Lists.newArrayList(condition3), Lists.newArrayList(tagUntagMemberAction, sendBenefitAction, ActionsBuilder.buildSendPushAction("Hi member")));

        LoyaltyService.createRuleFromTemplateInBox(activity2, "uncompletedTemplate");
    }


    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.MadeAPurchase;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.MadeAPurchase.toString() + "_";
    }

}
