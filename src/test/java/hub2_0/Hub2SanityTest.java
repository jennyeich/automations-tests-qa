package hub2_0;

import com.beust.jcommander.internal.Lists;
import common.BaseTest;
import hub.base.BaseHubTest;
import hub.common.objects.common.RegistrationField;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.DelayAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MembershipStatus;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.conditions.operators.ListOperators;
import hub.hub2_0.common.conditions.operators.ShoppingCartOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.common.items.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import integration.base.BaseRuleHub2Test;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.base.BaseServerTest;

import java.util.Arrays;
import java.util.Collections;

public class Hub2SanityTest extends BaseRuleHub2Test {

    public static CharSequence timestamp;

    @BeforeClass
    public static void setup() throws Exception {

        BaseTest.initBase(false);
        BaseHubTest.initHub(false);
        MembersService.setLogger(log4j);
        BaseServerTest.initServer();
        initProperties();
    }

    @AfterClass
    public static void tearDown(){

        log4j.info("Finish test : " + Hub2SanityTest.class.getName());
        try {
            checkSmartAutomationsAfterTestsFinished();
        } catch (Exception e) {
            log4j.error(e.getMessage(),e);
        }
        log4j.info("close browser after finish test");
        cleanBase();
        BaseHubTest.cleanHub();
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    /**
     * Test trigger make purchase with condition BranchID is exactly 1, then tag member
     */
    @Test
    public void testMadeAPurchaseWithConditionBranchIDIsExactlyAndTagAction() throws Exception{

        tag = ActionsBuilder.TAG + timestamp.toString();
        Box box = ruleBuilder.buildBox("ConditionBranchIDIsExactlyAndTagAction_" + timestamp);
        LoyaltyService.createBox(box);
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("1");
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(),box,rule,Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

    }

    @Test
    public void testJoinTheClubGlobalMemberConditionTagMemberWithDelay() throws Exception {

        Box box = ruleBuilder.buildBox("GlobalMemberConditionTagMemberWithDelay_" + timestamp);
        LoyaltyService.createBox(box);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(RegistrationField.Gender.toString()).setOperatorKey(InputOperators.IS_ONE_OF).setConditionSelectValue("male");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        specificMembers.setMembershipStatus(MembershipStatus.newMembershipStatus().setRegister(MembershipStatus.UNCHECK).setPending(MembershipStatus.CHECK));
        globalCondition.setSpecificMembers(specificMembers);

        //Add action
        ActivityAction activityAction = ActionsBuilder.buildTagMemberAction(timestamp.toString());
        DelayAction delayAction = new DelayAction();
        delayAction.selectDelayType(DelayAction.AFTER).setAfterInput("2").setAfterUnits(DelayUnits.Seconds).clickFinish(DelayAction.SAVE);
        activityAction.setDelayAction(delayAction);

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.JoinTheClub.toString(), box, rule, Arrays.asList(globalCondition), Lists.newArrayList(activityAction));
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
    }

    @Test
    public void testMadeAPurchaseRuleActivityPerOccurrencesWithActionLimit() throws Exception {

        Box box = ruleBuilder.buildBox("PerOccurrencesWithActionLimit_" + timestamp);
        LoyaltyService.createBox(box);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        CodesGroup itemsCodesGroup = new CodesGroup();
        itemsCodesGroup.setCodes(CodesGroup.ITEM_CODES_GROUP, "111", "222", "333");
        selectItems.setItemCodes(itemsCodesGroup);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("1")
                .setSelectItems(selectItems);


        SelectItems selectItems2 = new SelectItems();
        selectItems2.clickDepartmentCodesRadioButton();
        selectItems2.setDepartmentCodes("aaa", "bbb");
        selectItems2.clickButton(SelectedItemsDialogButtons.Apply.toString());

        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItems2);
        occurrencesCondition.setConditionKey(OccurrencesCondition.Condition.IN_THE_TOTAL_SPEND_OF);
        occurrencesCondition.setConditionValue("5");


        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(), box, rule, Collections.singletonList(condition), Collections.singletonList(occurrencesCondition), Lists.newArrayList(ActionsBuilder.buildTagMemberAction(timestamp.toString())));
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
    }

    @Test
    public void testMadeAPurchaseRuleWithAdvancedRuleAndDepartmentCodesSaveAsGroups() throws Exception {

        Box box = ruleBuilder.buildBox("AdvancedRuleAndDepartmentCodesSaveAsGroups_" + timestamp);
        LoyaltyService.createBox(box);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        AdvancedRuleCondition advancedRuleCondition = new AdvancedRuleCondition();
        advancedRuleCondition.setConditionKey(AdvancedRuleConditionKeys.ItemCode.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("11111");


        SelectItems selectItems = new SelectItems();
        selectItems.clickAdvancedRulesRadioButton();
        AdvancedRule advancedRule = AdvancedRule.newRule().setAdvancedRuleConditions(advancedRuleCondition)
                .setConditionRelations(ConditionRelations.Any.toString());
        advancedRule.clickSaveAsGroupBtn("group_" + timestamp, NewGroup.SAVE);
        selectItems.setAdvancedRule(advancedRule);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("1")
                .setSelectItems(selectItems);

        SelectItems selectItems1 = new SelectItems();
        selectItems1.clickDepartmentCodesRadioButton();
        CodesGroup departmentCodes = new CodesGroup();
        departmentCodes.setCodes(CodesGroup.DEPARTMENT_CODES_GROUP,"aaa", "bbb");
        departmentCodes.clickSaveAsGroupBtn("department_" + timestamp, NewGroup.SAVE);
        selectItems1.setDepartmentCodes(departmentCodes);
        selectItems1.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition2 = new MadeAPurchaseCondition();
        condition2.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_MORE_THAN)
                .setConditionValue("1")
                .setSelectItems(selectItems1);

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(), box, rule, Lists.newArrayList(condition,condition2), Lists.newArrayList(ActionsBuilder.buildTagMemberAction(timestamp.toString())));
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
    }


    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.MadeAPurchase;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.MadeAPurchase.toString();
    }

}
