package hub2_0.triggers;


import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.common.RegistrationField;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Arrays;
import java.util.Calendar;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class UsedMyRefferedCodeTriggerTest extends BaseRuleHub2Test {

    private static Box box;



    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = tag + timestamp.toString();
            createSmartGift();
        }
    };

    @After
    public void cleanData() throws Exception{
        deactivateAutomation();
    }

    @Test
    public void testNoConditionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule, Lists.newArrayList(ActivityActions.Tag),tag);
        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        String membershipKey = joinClubthroughFriendReferralCodeReturnMembershipKeyForAdvocate ();

        key = hubMemberCreator.getMemberKeyToUserKey();
        Assert.assertTrue("The tag was not found for member with membershipKey: " + membershipKey, MembersService.isTagExists(tag));//check that user received the correct tag
        checkAutomationRunAndTagMember();
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }


    @Test
    public void testNoConditionSendBenefit() throws Exception {

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule, Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());
        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        joinClubthroughFriendReferralCodeReturnMembershipKeyForAdvocate ();
        key = hubMemberCreator.getMemberKeyToUserKey();
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());
        log4j.info("member received the benefit");
        log4j.info(smartGift.getTitle() + " was added to the member ");

    }


    @Test
    public void testNoConditionSendPush() throws Exception {

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule, Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());
        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        joinClubthroughFriendReferralCodeReturnMembershipKeyForAdvocate ();
        key = hubMemberCreator.getMemberKeyToUserKey();
        checkAutomationRunAndSendPushNotification();
        log4j.info("member received the push");

    }

    @Test
    public void testNoConditionSendBenefitAndSendPushNotification() throws Exception {

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        ActivityAction activityAction1 = ActionsBuilder.buildAction( ActivityActions.SendBenefit.toString(), smartGift.getTitle());
        ActivityAction activityAction2 = ActionsBuilder.buildAction( ActivityActions.SendAPushNotification.toString(), "push " + timestamp);
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(),Lists.newArrayList(activityAction1,activityAction2));

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");


        joinClubthroughFriendReferralCodeReturnMembershipKeyForAdvocate ();
        key = hubMemberCreator.getMemberKeyToUserKey();
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());
        log4j.info("member received the benefit");
        log4j.info(smartGift.getTitle() + " was added to the member ");

        checkAutomationRunAndSendPushNotification();
        log4j.info("member received the push");

    }






    @Test
    @Category({Hub2Regression.class})
    public void testWithConditionGenderIsAndActionTagMember()throws Exception {
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, ActivityTrigger.JoinTheClub.toString() + timestamp);

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(RegistrationField.Gender.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionSelectValue("female");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.JoinTheClub.toString(), box, rule, Arrays.asList(globalCondition), Lists.newArrayList(ActivityActions.Tag), timestamp.toString());
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        joinClubthroughFriendReferralCodeReturnMembershipKeyForAdvocate ();
        key = hubMemberCreator.getMemberKeyToUserKey();
        checkDataStoreUpdatedWithTagByRuleID(timestamp.toString());
        log4j.info("member was tagged successfully");
    }


    @Test
    public void testWithConditionAllowSMSIsExactlyAndActionSendBenefitAndTagMember()throws Exception {

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, ActivityTrigger.JoinTheClub.toString() + timestamp);

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.AllowSMS.toString()).toggleBooleanValue(ActivityCondition.BOOLEAN_YES);
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.JoinTheClub.toString(), box, rule, Arrays.asList(globalCondition), Lists.newArrayList(ActionsBuilder.buildSendBenefitAction(smartGift.getTitle()), ActionsBuilder.buildTagMemberAction(timestamp.toString())));
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);

        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        joinClubthroughFriendReferralCodeReturnMembershipKeyForAdvocate ();
        key = hubMemberCreator.getMemberKeyToUserKey();
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());
        log4j.info("member received the benefit");
        log4j.info(smartGift.getTitle() + " was added to the member ");

        checkDataStoreUpdatedWithTagByRuleID(timestamp.toString());
        key = hubMemberCreator.getMemberKeyToUserKey();
        log4j.info("member was tagged successfully");
    }





    @ru.yandex.qatools.allure.annotations.Description("Test trigger of join the club with global condition of between dates and not on the current day. The action is tag member. the test verify that the member didn't get the tag")
    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        joinClubthroughFriendReferralCodeReturnMembershipKeyForAdvocate ();
        key = hubMemberCreator.getMemberKeyToUserKey();
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }


    @Override
     public ITrigger getTrigger() {
        return ActivityTrigger.UsedMyReferredCode;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.UsedMyReferredCode.toString() +"_";
    }
}
