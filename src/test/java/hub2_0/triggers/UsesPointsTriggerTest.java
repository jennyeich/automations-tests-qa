package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.conditions.PointsCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.utils.ServerHelper;
import server.utils.UserActionsConstants;

import java.util.Calendar;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class UsesPointsTriggerTest extends BaseRuleHub2Test {

    private static Box box;
    private static boolean firstTest = true;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            if(firstTest ) {
                serverHelper.configureBackendForPointsPayment();
                firstTest = false;
            }
            createSmartGift();
        }
    };



    /**
     * Test trigger use points condition that the used points is greater than , Action of Tag member.
     */
    @Test
    public void  testConditionNumberOfPointsIsGreaterThanAndTagMemberAction() throws Exception {
        tag = tag + timestamp.toString();

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("20");


        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember member = createNewMember();
        String actionTag =  MembersService.performSmartActionAddPoints("100",timestamp);

        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedPoints = "2100";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);

        checkAutomationRunAndTagMember();
    }

    /**
     * test trigger of use point, condition that used points are less than 200 and action of tag
     */
    @Test
    public void  testConditionNumberOfPointsIsLessThanAndTagMemberAction() throws Exception {
        tag = tag + timestamp.toString();

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("200");

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember member = createNewMember();

        String actionTag = MembersService.performSmartActionAddPoints("1000",timestamp);

        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedPoints = "100";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);

        checkAutomationRunAndTagMember();
    }

    /**
    /* Test the use points trigger.  condition is :number of point is greater than or equal to 200 and action is send asset to member.
     * new member use 200 points.
     * */

    @Test
    public void testConditionNumberOfPointsIsGreaterThanOrEqualToAndSendBenefitAction() throws Exception {

        tag = tag + timestamp.toString();

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("200");

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember member = createNewMember();
        //add points so user can use them
        String actionTag = MembersService.performSmartActionAddPoints("1000",timestamp);
        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedPoints = "20000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);

        checkAutomationRunAndSendAssetToMember();

    }


    /**
     *
     * @throws Exception
     */
     /* Test the use points trigger. condition is :number of point is less than or equal to 1000 and action is send asset to member.
     /* new member use 800 points.
     */
    @Test
    public void testConditionNumberOfPointsIsLessThanOrEqualToAndSendBenefitAction() throws Exception {

        tag = tag + timestamp.toString();

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("1000");

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //add points so user can use them
        //MembersService.performAnActionAddPoints("1000");
        MembersService.performSmartActionAddPoints("1000",timestamp);

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "800";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);
        checkAutomationRunAndSendAssetToMember();
    }

    /**
     /* Test the use points trigger. condition is :number of point is Exactly 200 and action is tagging the member.
     /* new member use 200 points Exactly.
     */
    @Test
    @Category({Hub2Regression.class})
    public void  testConditionNumberOfPointsIsExactlyAndTagMemberAction() throws Exception {
        tag = tag + timestamp.toString();

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("200");


        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Add points
        MembersService.performSmartActionAddPoints("1000",timestamp);
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "20000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);
        checkAutomationRunAndTagMember();

    }

    @Test
    @Category({Hub2Regression.class})
    public void testConditionNewBalanceValueIsGreaterThanAndTagMemberAction() throws Exception{

        tag = tag + timestamp.toString();

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NEW_BALANACE).
                setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("200");

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Add points
        MembersService.performSmartActionAddPoints("1000",timestamp);
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "1000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionNewBalanceValueIsLessThanAndTagMemberAction() throws Exception {

        tag = tag + timestamp.toString();

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NEW_BALANACE).
                setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("500");

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Add points
        MembersService.performSmartActionAddPoints("1000",timestamp);
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "60000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionNewBalanceValueIsGreaterThanOrEqualToAndSendBenefitAction() throws Exception {

        tag = tag + timestamp.toString();

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NEW_BALANACE).
                setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("200");

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Add points
        MembersService.performSmartActionAddPoints("1000",timestamp);
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "1000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);
        checkAutomationRunAndSendAssetToMember();

    }

    @Test
    public void testConditionNewBalanceValueIsLessThanOrEqualToAndSendBenefitAction() throws Exception {

        tag = tag + timestamp.toString();

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NEW_BALANACE).
                setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("500");

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Add points
        MembersService.performSmartActionAddPoints("1000",timestamp);
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "70000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);
        checkAutomationRunAndSendAssetToMember();

    }

    @Test
    public void testConditionNewBalanceValueIsExactlyAndTagMemberAction() throws Exception {

        tag = tag + timestamp.toString();

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NEW_BALANACE).
                setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("500");

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Add points
        MembersService.performSmartActionAddPoints("1000",timestamp);
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "50000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);
        checkAutomationRunAndSendAssetToMember();
    }

    @Test
    public void testWithGlobalConditionSpecificMembersAndActionTagMember() throws Exception {

        tag = tag + timestamp.toString();

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(globalCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Add points
        MembersService.performSmartActionAddPoints("1000",timestamp);
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);
        performAnActionTagMember(member, "Gold");


        String usedPoints = "5000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);

        checkAutomationRunAndTagMember();
    }

    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        tag = tag + timestamp.toString();

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        MembersService.performSmartActionAddPoints("1000",timestamp);
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints = "5000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedPoints);

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }

    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        tag = tag + timestamp.toString();

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        MembersService.performSmartActionAddPoints("1000",timestamp);
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(newMember.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String usedPoints1 = "1000";
        String usedPoints2 = "2000";
        String usedPoints3 = "3000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(newMember, usedPoints1);
        ServerHelper.performAnActionUsePoints(newMember, usedPoints2);
        ServerHelper.performAnActionUsePoints(newMember, usedPoints3);

        checkDataStoreUpdatedWithSendPushNotification(2);
    }

    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.UsePoints;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.UsePoints.toString() +"_";
    }

}
