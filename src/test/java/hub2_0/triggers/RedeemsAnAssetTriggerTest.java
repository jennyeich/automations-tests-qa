package hub2_0.triggers;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.gifts.Gift;
import hub.hub2_0.basePages.loyalty.gifts.GiftDisplay;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountEntireTicketAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.builders.GiftsBuilder;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.conditions.AssetCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.ListOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Calendar;
import java.util.List;

import static hub2_0.utils.DateTimeUtility.resolveDay;
import static integration.base.BaseHub2DealTest.buildAmountOffDiscountOnEntireTicket;

public class RedeemsAnAssetTriggerTest extends BaseRuleHub2Test {

    private static Box box;
    public static Gift newHub2Gift;
    GiftsBuilder giftsBuilder = new GiftsBuilder();

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = ActionsBuilder.TAG + timestamp;
            createSmartGift();
        }
    };


    //************************ asset conditions *******************************************

    @Test
    public void testConditionBenefitIsNotAndTagAction() throws Exception {

        SmartGift smartGift1 = hubAssetsCreator.createSmartGift(String.valueOf(System.currentTimeMillis()));
        //Add condition
        AssetCondition assetCondition = new AssetCondition();
        assetCondition.setOperatorKey(ListOperators.IS_NOT)
                .setConditionValue(smartGift1.getTitle());
        createRuleRedeemAsset(smartGift.getTitle(), ActivityActions.Tag, assetCondition);


        createMemberAssignBenefitAndRedeem();

        checkAutomationRunAndTagMember();
    }

    @Test
    public void testConditionBenefitIsExactlyTagAction() throws Exception {

        //Add condition
        AssetCondition assetCondition = new AssetCondition();
        assetCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setConditionValue(smartGift.getTitle());
        createRuleRedeemAsset(smartGift.getTitle(), ActivityActions.Tag, assetCondition);


        createMemberAssignBenefitAndRedeem();

        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionHub2BenefitIsExactlyTagAction() throws Exception {

        createHub2Gift();
        AssetCondition assetCondition = new AssetCondition();
        assetCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setConditionValue(newHub2Gift.getActivityName());
        createRuleRedeemAsset(newHub2Gift.getActivityName(), ActivityActions.Tag, assetCondition);

        createMemberAssignHub2BenefitAndRedeem();

        checkAutomationRunAndTagMember();

    }


    @Test
    public void testRedeemPunchCardConditionBenefitIsExactlyTagAction() throws Exception {

        smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");
        //Add condition
        AssetCondition assetCondition = new AssetCondition();
        assetCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setConditionValue(smartPunchCard.getTitle());
        createRuleRedeemAsset(smartPunchCard.getTitle(), ActivityActions.Tag, assetCondition);

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform smartPunchCard
        performAnActionPunchAPunchCard(smartPunchCard, 5);

        String assetKey = getAssetKey(member.getPhoneNumber());

        //redeem asset
        performAnActionRedeemAsset(membershipKey, userKey, assetKey);

        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionBenefitIsExactlySendBenefitAction() throws Exception {

        AssetCondition assetCondition = new AssetCondition();
        assetCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setConditionValue(smartGift.getTitle());
        createRuleRedeemAsset(smartGift.getTitle(), ActivityActions.SendBenefit, assetCondition);

        createMemberAssignBenefitAndRedeem();

        checkAutomationRunAndSendAssetToMember();
    }


    //asset condition NOT equals  - negative test
    @Test
    public void testConditionBenefitIsNotTagActionNegative() throws Exception {

        SmartGift smartGift1 = hubAssetsCreator.createSmartGift(String.valueOf(System.currentTimeMillis()));

        AssetCondition assetCondition = new AssetCondition();
        assetCondition.setOperatorKey(ListOperators.IS_NOT)
                .setConditionValue(smartGift1.getTitle());

        createRuleRedeemAsset(smartGift.getTitle(), ActivityActions.Tag, assetCondition);


        createMemberAssignBenefitAndRedeem();

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @Test
    @Category({Hub2Regression.class})
    @ru.yandex.qatools.allure.annotations.Description("Test trigger of redeems an asset with global condition of specific member. The action is tag member")
    public void testWithGlobalConditionSpecificMembersAndActionTagMember() throws Exception {

        MembershipGlobalCondition membershipGlobalCondition = new MembershipGlobalCondition();
        membershipGlobalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        membershipGlobalCondition.setSpecificMembers(specificMembers);

        createRuleRedeemAssetWithGlobalCondition(smartGift.getTitle(), ActivityActions.Tag, membershipGlobalCondition);

        createMemberWithMemberTagAssignBenefitAndRedeem();

        checkAutomationRunAndTagMember();

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of redeems an asset with global condition of between dates and not on the current day. The action is tag member. the test verify that the member didn't get the tag")
    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay = resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, ActivityTrigger.RedeemsAnAsset.toString());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        createMemberAssignBenefitAndRedeem();

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of redeems an asset with global condition of limit up to 2 times in total per member. The action is tag member. the test verify that the member received the push only 2 times")
    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.SendAPushNotification), timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();


        sendGiftToMemberAndRedeem(newMember, membershipKey, userKey);
        sendGiftToMemberAndRedeem(newMember, membershipKey, userKey);
        sendGiftToMemberAndRedeem(newMember, membershipKey, userKey);

        checkDataStoreUpdatedWithSendPushNotification(2);

    }


    private void sendGiftToMemberAndRedeem(NewMember newMember, String membershipKey, String userKey) throws Exception {
        //Send gift to member
        MembersService.performSmartActionSendAnAsset(smartGift.getTitle(), timestamp);

        String assetKey = serverHelper.getMemberAssetKey(smartGift.getTitle(), newMember.getPhoneNumber());
        //redeem asset
        performAnActionRedeemAsset(membershipKey, userKey, assetKey);
    }


    private void createRuleRedeemAsset(String assetName, ActivityActions action, AssetCondition condition) throws Exception {

        tag = assetName;
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, ActivityTrigger.RedeemsAnAsset.toString());

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.RedeemsAnAsset.toString(), box, rule, Lists.newArrayList(condition), Lists.newArrayList(action), assetName);
        log4j.info("Created rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

    }

    private void createRuleRedeemAssetWithGlobalCondition(String assetName, ActivityActions action, MembershipGlobalCondition globalCondition) throws Exception {

        tag = assetName;
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, ActivityTrigger.RedeemsAnAsset.toString());

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.RedeemsAnAsset.toString(), box, rule, Lists.newArrayList(globalCondition), Lists.newArrayList(action), assetName);
        log4j.info("Created rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

    }

    private void createMemberAssignBenefitAndRedeem() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        MembersService.performSmartActionSendAnAsset(smartGift.getTitle(), timestamp);
        Assert.assertTrue("Member did not get the gift", MembersService.isGiftExists(smartGift.getTitle()));

        String assetKey = getAssetKey(member.getPhoneNumber());

        //redeem asset
        performAnActionRedeemAsset(membershipKey, userKey, assetKey);

    }

    private void createMemberAssignHub2BenefitAndRedeem() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send gift to member
        MembersService.performSmartActionSendAnAsset(newHub2Gift.getActivityName(), timestamp);
        Assert.assertTrue("Member did not get the gift", MembersService.isGiftExists(newHub2Gift.getActivityName()));

        String assetKey = getAssetKey(member.getPhoneNumber());

        //redeem asset
        performAnActionRedeemAsset(membershipKey, userKey, assetKey);

    }

    private void createMemberWithMemberTagAssignBenefitAndRedeem() throws Exception {
        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionTagMember(member, "Gold");
        //Send gift to member
        MembersService.performSmartActionSendAnAsset(smartGift.getTitle(), timestamp);
        Assert.assertTrue("Member did not get the gift", MembersService.isGiftExists(smartGift.getTitle()));

        String assetKey = getAssetKey(member.getPhoneNumber());

        //redeem asset
        performAnActionRedeemAsset(membershipKey, userKey, assetKey);

    }

    private void createHub2Gift() throws Exception {

        GiftDisplay giftDisplay = new GiftDisplay();
        giftDisplay.setDisplayTab("");
        giftDisplay.setImageUpload("");
        giftDisplay.setSelectExistImage("");
        giftDisplay.setImageSave("");

        String amountDiscount = "50";
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = buildAmountOffDiscountOnEntireTicket(amountDiscount);
        newHub2Gift = createEntireTicketDiscountGift(Lists.newArrayList(), Lists.newArrayList(amountOffDiscountEntireTicketAction), giftDisplay);

        LoyaltyService.returnToCampaignCenterPage();
    }


    private Gift createEntireTicketDiscountGift(List<ApplyCondition> conditions, List<ActivityAction> activityActionList, GiftDisplay giftDisplay) throws Exception {

        Gift gift = giftsBuilder.buildEntireTicketDiscountActivityGift("entireTicketGift_",timestamp.toString(), conditions, activityActionList, giftDisplay);
        gift.clickPublishActivityBtn();
        LoyaltyService.createGift(gift);

        return gift;


    }

    @After
    public void cleanData() throws Exception {
        deactivateAutomation();
    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.RedeemsAnAsset;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.RedeemsAnAsset.toString() + "_";
    }
}
