package hub2_0.triggers;

import com.google.common.collect.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.conditions.operators.ShoppingCartOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.common.items.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import integration.base.BaseRuleHub2Test;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

public class SelectItemsBoxTest extends BaseRuleHub2Test {

    private static Box box;


    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }


    @ru.yandex.qatools.allure.annotations.Description("When member makes a purchase and the shopping cart contains 1 specific item and is one of item name (advanced rule) then tag member")
    @Test
    public void testWithConditionShoppingCartIsAndWithAdvancedRuleAndTagMemberAction() throws Exception{

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active,getAutomationName());
        //Add condition
        AdvancedRuleCondition advancedRuleCondition = new AdvancedRuleCondition();
        advancedRuleCondition.setConditionKey(AdvancedRuleConditionKeys.ItemCode.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("11111");
        AdvancedRuleCondition advancedRuleCondition2 = new AdvancedRuleCondition();
        advancedRuleCondition2.setConditionKey(AdvancedRuleConditionKeys.ItemName.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("itemA,aaaa");

        SelectItems selectItems = new SelectItems();
        selectItems.clickAdvancedRulesRadioButton();
        AdvancedRule advancedRule = AdvancedRule.newRule().setAdvancedRuleConditions(advancedRuleCondition,advancedRuleCondition2)
                .setConditionRelations(ConditionRelations.All.toString());
        selectItems.setAdvancedRule(advancedRule);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_EXECTLY)
                .setConditionValue("1")
                .setSelectItems(selectItems);


        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        log4j.info("rule name:" +rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);

        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("11111","itemA","depA", "depAA", 10000);
        Item item2 = createItem("2244","itemA","depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000", new ArrayList<>(), transactionId, items);

        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    @ru.yandex.qatools.allure.annotations.Description("Negative test - purchase shopping cart contain item '5555' & '2244', " +
            "When member makes a purchase and the shopping cart contains 1 specific item '11111' AND is one of item name 'itemA,aaaa' (advanced rule) then tag member")
    @Test
    public void testWithConditionShoppingCartIsAndWithAdvancedRuleAndTagMemberActionNegative() throws Exception{

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active,getAutomationName());
        //Add condition
        AdvancedRuleCondition advancedRuleCondition = new AdvancedRuleCondition();
        advancedRuleCondition.setConditionKey(AdvancedRuleConditionKeys.ItemCode.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("11111");
        AdvancedRuleCondition advancedRuleCondition2 = new AdvancedRuleCondition();
        advancedRuleCondition2.setConditionKey(AdvancedRuleConditionKeys.ItemName.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("itemA,aaaa");

        SelectItems selectItems = new SelectItems();
        selectItems.clickAdvancedRulesRadioButton();
        AdvancedRule advancedRule = AdvancedRule.newRule().setAdvancedRuleConditions(advancedRuleCondition,advancedRuleCondition2)
                .setConditionRelations(ConditionRelations.All.toString());
        selectItems.setAdvancedRule(advancedRule);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_EXECTLY)
                .setConditionValue("1")
                .setSelectItems(selectItems);


        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        log4j.info("rule name:" +rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("5555","itemA","depA", "depAA", 10000);
        Item item2 = createItem("2244","itemA","depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000", new ArrayList<>(), transactionId, items);

        //check that automation did not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

        log4j.info("member didn't received the tag");

    }

    @Test
    public void testConditionShoppingCartContainsAtLeastAndDepartmentCode() throws Exception{

        SelectItems selectItems = new SelectItems();
        selectItems.clickDepartmentCodesRadioButton();
        selectItems.setDepartmentCodes("depB");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_AT_LEAST)
                .setConditionValue("2")
                .setSelectItems(selectItems);

        createRule(Lists.newArrayList(madeAPurchaseCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item1 = createItem("5555","itemA","depA", "depAA", 10000);
        Item item2 = createItem("2244","itemB","depB", "depBB", 10000);
        Item item3 = createItem("2245","itemC","depB", "depBB", 10000);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "30000", new ArrayList<>(), transactionId, Lists.newArrayList(item1, item2, item3));

        //check that automation run
        checkDataStoreUpdatedWithTagByRuleID(tag);
    }


    @Test
    public void testConditionShoppingCartContainsLessThanAndPreDefinedGroupItemCode() throws Exception{

        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        CodesGroup itemCodes = new CodesGroup();
        itemCodes.setCodes(CodesGroup.ITEM_CODES_GROUP,"6767", "5454");
        selectItems.setItemCodes(itemCodes);
        String groupName = "group_" + timestamp;
        itemCodes.clickSaveAsGroupBtn(groupName, NewGroup.SAVE);

        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_LESS_THAN)
                .setConditionValue("4")
                .setSelectItems(selectItems);

        createRule(Lists.newArrayList(madeAPurchaseCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item1 = createItem("6767","itemA","depA", "depAA", 10000);
        Item item2 = createItem("6767","itemB","depB", "depBB", 10000);
        Item item3 = createItem("5454","itemC","depB", "depBB", 10000);
        Item item4 = createItem("2245","itemC","depB", "depBB", 10000);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "40000", new ArrayList<>(), transactionId, Lists.newArrayList(item1, item2, item3, item4));

        //check that automation run
        checkDataStoreUpdatedWithTagByRuleID(tag);
    }

    @Test
    @Category({Hub2Regression.class})
    public void testConditionShoppingCartContainsLessThanAndPreDefinedGroupDepartmentCode() throws Exception{

        SelectItems selectItems = new SelectItems();
        selectItems.clickDepartmentCodesRadioButton();
        CodesGroup departmentCodes = new CodesGroup();
        departmentCodes.setCodes(CodesGroup.DEPARTMENT_CODES_GROUP,"depB", "depA");
        selectItems.setDepartmentCodes(departmentCodes);
        String groupName = "group_" + timestamp;
        departmentCodes.clickSaveAsGroupBtn(groupName, NewGroup.SAVE);

        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_MORE_THAN)
                .setConditionValue("3")
                .setSelectItems(selectItems);

        createRule(Lists.newArrayList(madeAPurchaseCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item1 = createItem("6767","itemA","depA", "depAA", 10000);
        Item item2 = createItem("6767","itemB","depB", "depBB", 10000);
        Item item3 = createItem("5454","itemC","depB", "depBB", 10000);
        Item item4 = createItem("2245","itemC","depB", "depBB", 10000);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "40000", new ArrayList<>(), transactionId, Lists.newArrayList(item1, item2, item3, item4));

        //check that automation run
        checkDataStoreUpdatedWithTagByRuleID(tag);
    }

    @Test
    public void testConditionShoppingCartContainsAndPreDefinedGroupAdvancedRule() throws Exception{

        AdvancedRuleCondition advancedRuleCondition = new AdvancedRuleCondition();
        advancedRuleCondition.setConditionKey(AdvancedRuleConditionKeys.Price.toString())
                .setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("101");
        SelectItems selectItems = new SelectItems();
        selectItems.clickAdvancedRulesRadioButton();
        AdvancedRule advancedRule = AdvancedRule.newRule().setAdvancedRuleConditions(advancedRuleCondition)
                .setConditionRelations(ConditionRelations.Any.toString());
        advancedRule.clickSaveAsGroupBtn("group_" + timestamp, NewGroup.SAVE);
        selectItems.setAdvancedRule(advancedRule);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_EXECTLY)
                .setConditionValue("2")
                .setSelectItems(selectItems);

        createRule(Lists.newArrayList(madeAPurchaseCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item1 = createItem("6767","itemA","depA", "depAA", 10000);
        Item item2 = createItem("5767","itemB","depB", "depBB", 10100);
        Item item3 = createItem("5454","itemC","depB", "depBB", 20000);
        Item item4 = createItem("2245","itemC","depB", "depBB", 10200);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "50300", new ArrayList<>(), transactionId, Lists.newArrayList(item1, item2, item3, item4));

        //check that automation run
        checkDataStoreUpdatedWithTagByRuleID(tag);
    }

    @Test
    public void testConditionItemSpendLessThanAndPreDefinedGroupAdvancedRule() throws Exception{

        AdvancedRuleCondition advancedRuleCondition = new AdvancedRuleCondition();
        advancedRuleCondition.setConditionKey(AdvancedRuleConditionKeys.ItemName.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("itemB,itemC");
        SelectItems selectItems = new SelectItems();
        selectItems.clickAdvancedRulesRadioButton();
        AdvancedRule advancedRule = AdvancedRule.newRule().setAdvancedRuleConditions(advancedRuleCondition);
        advancedRule.clickSaveAsGroupBtn("group_" + timestamp, NewGroup.SAVE);
        selectItems.setAdvancedRule(advancedRule);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("30.50")
                .setSelectItems(selectItems);

        createRule(Lists.newArrayList(madeAPurchaseCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item1 = createItem("6767","itemA","depA", "depAA", 10000);
//        Item item2 = createItem("5767","itemB","depB", "depBB", 3000);
        Item item3 = createItem("5454","itemC","depB", "depBB", 3040);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "16040", new ArrayList<>(), transactionId, Lists.newArrayList(item1, item3));

        //check that automation run
        checkDataStoreUpdatedWithTagByRuleID(tag);
    }

    @Test
    public void testConditionItemSpendIsWithAdvancedRule() throws Exception{
        AdvancedRuleCondition advancedRuleCondition = new AdvancedRuleCondition();
        advancedRuleCondition.setConditionKey(AdvancedRuleConditionKeys.DepartmentCode.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("depB");
        SelectItems selectItems = new SelectItems();
        selectItems.clickAdvancedRulesRadioButton();
        AdvancedRule advancedRule = AdvancedRule.newRule().setAdvancedRuleConditions(advancedRuleCondition);
        selectItems.setAdvancedRule(advancedRule);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("20")
                .setSelectItems(selectItems);

        createRule(Lists.newArrayList(madeAPurchaseCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item1 = createItem("6767","itemA","depA", "depAA", 10000);
        Item item2 = createItem("5767","itemB","depB", "depBB", 2000);
//        Item item3 = createItem("5454","itemC","depB", "depBB", 2000);
        Item item4 = createItem("2245","itemC","depC", "depCC", 20000);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "34000", new ArrayList<>(), transactionId, Lists.newArrayList(item1, item2, item4));

        //check that automation run
        checkDataStoreUpdatedWithTagByRuleID(tag);
    }

    @Test
    public void testWithConditionItemSpendIsGreaterThanOrEqualToWithDepartmentCodesAndTagMemberAction() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickDepartmentCodesRadioButton();
        selectItems.setDepartmentCodes("depB");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("10")
                .setSelectItems(selectItems);

        createRule(Lists.newArrayList(madeAPurchaseCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item1 = createItem("6767","itemA","depA", "depAA", 10000);
        Item item2 = createItem("5767","itemB","depB", "depBB", 2000);
        Item item3 = createItem("2245","itemC","depC", "depCC", 20000);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "34000", new ArrayList<>(), transactionId, Lists.newArrayList(item1, item2, item3));

        //check that automation run
        checkDataStoreUpdatedWithTagByRuleID(tag);

    }

    @Test
    public void testWithConditionItemSpendIsGreaterThanWithPreDefinedGroupOfItemCodesAndTagMemberAction() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        CodesGroup itemCodes = new CodesGroup();
        itemCodes.setCodes(CodesGroup.ITEM_CODES_GROUP,"4455", "6677");
        selectItems.setItemCodes(itemCodes);
        String groupName = "group_" + timestamp;
        itemCodes.clickSaveAsGroupBtn(groupName, NewGroup.SAVE);

        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("20")
                .setSelectItems(selectItems);

        createRule(Lists.newArrayList(madeAPurchaseCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item1 = createItem("4455","itemA","depA", "depAA", 10000);
        Item item2 = createItem("6677","itemB","depB", "depBB", 20000);
        Item item3 = createItem("2245","itemC","depC", "depCC", 20000);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "34000", new ArrayList<>(), transactionId, Lists.newArrayList(item1, item2, item3));

        //check that automation run
        checkDataStoreUpdatedWithTagByRuleID(tag);

    }

    @Test
    public void testWithConditionItemSpendIsLessThanOrEqualToWithPreDefinedGroupOfDepartmentCodesAndTagMemberAction() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickDepartmentCodesRadioButton();
        CodesGroup departmentCodes = new CodesGroup();
        departmentCodes.setCodes(CodesGroup.DEPARTMENT_CODES_GROUP,"depB", "depA");
        selectItems.setDepartmentCodes(departmentCodes);
        String groupName = "group_" + timestamp;
        departmentCodes.clickSaveAsGroupBtn(groupName, NewGroup.SAVE);

        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("100")
                .setSelectItems(selectItems);

        createRule(Lists.newArrayList(madeAPurchaseCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item1 = createItem("4455","itemA","depA", "depAA", 2000);
        Item item2 = createItem("6677","itemB","depB", "depBB", 1000);
        Item item3 = createItem("2245","itemC","depC", "depCC", 20000);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "34000", new ArrayList<>(), transactionId, Lists.newArrayList(item1, item2, item3));

        //check that automation run
        checkDataStoreUpdatedWithTagByRuleID(tag);

    }

    @Test
    public void testWithConditionItemSpendIsAndWithAdvancedRuleAndTagMemberActionNegative() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickDepartmentCodesRadioButton();
        CodesGroup departmentCodes = new CodesGroup();
        departmentCodes.setCodes(CodesGroup.DEPARTMENT_CODES_GROUP,"depB", "depA");
        selectItems.setDepartmentCodes(departmentCodes);
        String groupName = "group_" + timestamp;
        departmentCodes.clickSaveAsGroupBtn(groupName, NewGroup.SAVE);

        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("100")
                .setSelectItems(selectItems);

        createRule(Lists.newArrayList(madeAPurchaseCondition));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item1 = createItem("4455","itemA","depA", "depAA", 20000);
        Item item2 = createItem("6677","itemB","depB", "depBB", 30000);
        Item item3 = createItem("2245","itemC","depC", "depCC", 20000);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "34000", new ArrayList<>(), transactionId, Lists.newArrayList(item1, item2, item3));

        //check that automation run
        checkDataStoreUpdatedWithTagByRuleID(tag);

    }

    @ru.yandex.qatools.allure.annotations.Description("Creates a rule with item group in the condition with tag action +" +
            "Second rule with makes a purchase trigger who uses the item group from the first rule")
    @Test
    public void testReuseItemGroup () throws Exception{
        String groupName = "Item group" + timestamp;

        //Item group of item codes(punch items)
        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        CodesGroup itemCodes = new CodesGroup();
        itemCodes.setCodes(CodesGroup.ITEM_CODES_GROUP, "1111", "8877");
        itemCodes.clickSaveAsGroupBtn(groupName, NewGroup.SAVE);
        selectItems.setItemCodes(itemCodes);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        //Activity condition "Shopping cart" "Contains at least" "1" "punch items" (The item group)
        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_EXECTLY)
                .setConditionValue("6")
                .setSelectItems(selectItems);

        //Rule build
        createRule(Lists.newArrayList(condition));

        //Predefined item group condition
        SelectItems selectItems2 = new SelectItems();
        selectItems2.clickPredefinedGroupsRadioButton();
        PreDefinedGroups preDefinedGroups = new PreDefinedGroups(groupName);
        selectItems2.clickPredefinedGroupsRadioButton();
        selectItems2.setPreDefinedGroup(preDefinedGroups);
        selectItems2.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition condition2 = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_AT_LEAST)
                .setConditionValue("1")
                .setSelectItems(selectItems2);

        //Build rule 2 (Use the item group that was created in rule 1)
        createRule(Lists.newArrayList(condition2));

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        //Shopping cart
        Item item1 = createItem("8877","itemA","depA", "depAA", 2000);
        Item item2 = createItem("1111","itemB","depB", "depBB", 3000);
        Item item3 = createItem("4525","itemB","depB", "depBB", 1000);

        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "6000", new ArrayList<>(), transactionId, Lists.newArrayList(item1, item2, item3));

        //check that automation run
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }

    private void createRule(List<ApplyCondition> conditions) throws Exception {
        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getTrigger().toString());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, conditions, Lists.newArrayList(ActivityActions.Tag),tag);

        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
    }






    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.MadeAPurchase;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.MadeAPurchase.toString() +"_";
    }
}
