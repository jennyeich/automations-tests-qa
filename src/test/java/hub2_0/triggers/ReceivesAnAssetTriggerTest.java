package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AmountType;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.PointsAccumulationAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.AssetCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.MemberRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.ListOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.utils.HubMemberCreator;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Arrays;
import java.util.Calendar;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class ReceivesAnAssetTriggerTest extends BaseRuleHub2Test {

    private static Box box;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = tag + timestamp.toString();
            createSmartGift();
        }
    };

    @After
    public void cleanData() throws Exception{
        deactivateAutomation();
    }


    @Test
    public void testConditionBenefitEqualsConditionTagAction() throws Exception {

        AssetCondition assetCondition = new AssetCondition();
        assetCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setConditionValue(smartGift.getTitle());

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Arrays.asList(assetCondition), Arrays.asList(ActivityActions.Tag), timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member and Get member userKey
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSendGift(smartGift);
        checkDataStoreUpdatedWithTagByRuleID(timestamp.toString());
    }

    @Test
    public void testConditionBenefitNOTEqualsConditionTagActionNegativeTest() throws Exception {

        AssetCondition assetCondition = new AssetCondition();
        assetCondition.setOperatorKey(ListOperators.IS_NOT)
                .setConditionValue(smartGift.getTitle());

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Arrays.asList(assetCondition), Arrays.asList(ActivityActions.Tag), timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member and Get member userKey
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSendGift(smartGift);
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of receives an asset with global condition of specific member. The action is tag member.")
    @Test
    public void testConditionBenefitIsNotAndTagMemberAction() throws Exception {

        SmartGift newGift = hubAssetsCreator.createSmartGift(String.valueOf(System.currentTimeMillis()));
        AssetCondition assetCondition = new AssetCondition();
        assetCondition.setOperatorKey(ListOperators.IS_NOT)
                .setConditionValue(newGift.getTitle());

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Arrays.asList(assetCondition), Arrays.asList(ActivityActions.Tag), timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member and Get member userKey
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSendGift(smartGift);
        checkDataStoreUpdatedWithTagByRuleID(timestamp.toString());
    }


    @ru.yandex.qatools.allure.annotations.Description("Test trigger of receive a benefit and global condition of Limit up to 2 times in total per member. The action is accumlate points")
    @Test
    @Category({Hub2Regression.class})
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionAccumlatePointsPerformTheTrigger3Times() throws Exception {



        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        PointsAccumulationAction pointsAccumulationAction = new PointsAccumulationAction();
        pointsAccumulationAction.clickPointsAccumulationBtn();
        pointsAccumulationAction.setNumOfPoints("100");

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Arrays.asList(ActivityActions.PointsAccumulation),"100");

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member and Get member userKey
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        key = hubMemberCreator.getMemberKeyToUserKey();


        performAnActionSendGift(smartGift);
        log4j.info("Send the first gift to member and expect to see point accumlation of 10000 ");
        checkDataStoreUpdatedWithPointsByRuleID("10000" , AmountType.Points);

        performAnActionSendGift(smartGift);
        log4j.info("Send the second gift to member and verify that automation run ");
        checkDataStoreUpdatedWithPointsByRuleID(2);

        performAnActionSendGift(smartGift);
        log4j.info("Send the third gift to member and verify that number of Automation that ran didn't changed (limit to 2 Automation)");
        checkDataStoreUpdatedWithPointsByRuleID(2);

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of receive a benefit with global condition of specific member. The action is tag member.")
    @Test
    public void testWithGlobalConditionSpecificMembersAndActionTagMember() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(globalCondition), Arrays.asList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //Tag member
        performAnActionTagMember(newMember, "Gold");

        performAnActionSendGift(smartGift);
        checkAutomationRunAndTagMember();

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of receive a benefit with global condition of between dates and not on the current day. The action is tag member. the test verify that the member didn't get the tag")
    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), Arrays.asList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        createNewMember();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        performAnActionSendGift(smartGift);
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of receive a benefit with condition of asset equal. The action is add 20 points. the test verify that the member got 20 points")
    @Test
    public void testConditionAssetEqualsConditionAddPoints() throws Exception {

        AssetCondition assetCondition = new AssetCondition();
        assetCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setConditionValue(smartGift.getTitle());

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Arrays.asList(assetCondition), Arrays.asList(ActivityActions.PointsAccumulation), "20");
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member and Get member userKey
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSendGift(smartGift);
        checkDataStoreUpdatedWithPointsByRuleID("2000", AmountType.Points);
    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.ReceivesAnAsset;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.ReceivesAnAsset.toString() +"_";
    }

}
