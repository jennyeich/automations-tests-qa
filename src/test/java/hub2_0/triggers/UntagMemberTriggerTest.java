package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.conditions.TagCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Arrays;
import java.util.Calendar;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class UntagMemberTriggerTest extends BaseRuleHub2Test {

    private static Box box;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());

            createSmartGift();
        }
    };

    @After
    public void cleanData() throws Exception{
        deactivateAutomation();
    }

    @Test
    public void testConditionTagIsNotOneOfAndSendBenefitAction() throws Exception {
        tag = tag + timestamp.toString();//set the tag that satisfied the condition
        TagCondition tagCondition = new TagCondition();
        tagCondition.setOperatorKey(InputOperators.IS_NOT_ONE_OF)
                .setConditionValue(timestamp.toString());

        createActivityTriggerAndUntag(tagCondition, ActivityActions.SendBenefit, smartGift.getTitle());
        checkAutomationRunAndSendAssetToMember();
    }

    @Ignore("'Is' operator is not supported any more for Tagged condition.")
    //@Test
    public void testConditionTagIsExactlyAndSendBenefitAction() throws Exception {
        tag = tag + timestamp.toString();//set the tag that satisfied the condition
        TagCondition tagCondition = new TagCondition();
        tagCondition.setOperatorKey(InputOperators.IS_EXACTLY)
                .setConditionValue(tag);

        createActivityTriggerAndUntag(tagCondition, ActivityActions.SendBenefit, smartGift.getTitle());
        checkAutomationRunAndSendAssetToMember();
    }


    @Test
    @Category({Hub2Regression.class})
    public void testConditionTagIsOneOfAndTagMemberAction() throws Exception {
        tag = tag + timestamp.toString();//set the tag that satisfied the condition
        TagCondition tagCondition = new TagCondition();
        tagCondition.setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue(tag + ", someTestTag");

        createActivityTriggerAndUntag(tagCondition, ActivityActions.Tag, "UNTAGGED_" + timestamp.toString());
        checkDataStoreUpdatedWithTagByRuleID("UNTAGGED_" + timestamp.toString());
    }


    @Test
    public void testWithGlobalConditionSpecificMembersAndActionTagMember() throws Exception  {

        tag = tag + timestamp.toString();

        //Create member
        NewMember newMember = createNewMember();
        String memberPhoneNumber = newMember.getPhoneNumber();
        performAnActionTagMember(newMember, "Gold");

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(globalCondition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        MembersService.findAndGetMember(memberPhoneNumber);

        //should activate the tag trigger
        MembersService.performTagMemberSmartAction(tag, "TagMember");
        Assert.assertTrue("The member did not get the tag sent", MembersService.isTagExists(tag));
        MembersService.performUntagMemberSmartAction(tag, "UnTagMember");

        checkAutomationRunAndSendAssetToMember();

    }

    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        tag = tag + timestamp.toString();

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member and perform tag & untag
        createMemberAndPerformTagAndUntag();

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        tag = tag + timestamp.toString();

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        createNewMember();

        performTagAndUntag();
        performTagAndUntag();
        performTagAndUntag();

        checkDataStoreUpdatedWithSendPushNotification(2);

    }


    private void createActivityTriggerAndUntag(TagCondition tagCondition, ActivityActions activityActions, String text) throws Exception{
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Arrays.asList(tagCondition), Arrays.asList(activityActions), text);
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        createMemberAndPerformTagAndUntag();
    }

    private void createMemberAndPerformTagAndUntag() throws Exception {
        createNewMember();
        //should activate the tag trigger
        MembersService.performTagMemberSmartAction(tag, "TagMember");
        Assert.assertTrue("The member did not get the tag sent", MembersService.isTagExists(tag));
        MembersService.performUntagMemberSmartAction(tag, "UnTagMember");
    }

    private void performTagAndUntag() throws Exception {

        //should activate the tag trigger
        MembersService.performTagMemberSmartAction(tag, "TagMember");
        Assert.assertTrue("The member did not get the tag sent", MembersService.isTagExists(tag));
        MembersService.performUntagMemberSmartAction(tag, "UnTagMember");
    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.Untagged;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.Untagged.toString() +"_";
    }
}
