package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import hub.base.basePages.BaseContentPage;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.common.FormFieldAnswerType;
import hub.common.objects.common.YesNoList;
import hub.common.objects.content.forms.FormFieldPage;
import hub.common.objects.content.forms.FormPage;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.services.Navigator;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.FilledOutAFormCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.ListOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.ContentService;
import hub.services.member.MembersService;
import hub.utils.HubFormsCreator;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class FillOutFormTriggerTest extends BaseRuleHub2Test {

    private static Box box;
    private static FormPage formPage;
    private static String userToken;//the user token that we receive after sign on
    private static String userKey; //the userKey that returns from JoinClub request
    private static String phoneNumber; //the phone number that we use to find the user for all tests

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @AfterClass
    public static void deleteForm() throws Exception {
        Navigator.contentTabGeneralForms();
        ContentService.deleteForm(formPage.getTitle());
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            if(formPage == null) {
                //create form
                try {
                    createForm();
                    FormPage formPage1 = HubFormsCreator.createForm(String.valueOf(System.currentTimeMillis()),HubFormsCreator.AUTO_TITLE_PREFIX, HubFormsCreator.AUTO_RECIPIENTS,
                            HubFormsCreator.AUTO_THANK_YOU_TITLE_PREFIX, HubFormsCreator.AUTO_THANK_YOU_CONTENT_PREFIX, HubFormsCreator.AUTO_DESCRIPTION_PREFIX);
                    createUserAndUpdateForTests();
                } catch (Exception e) {
                    log4j.error(e);
                    Assert.fail("error while creating form");
                }
            }


        }
    };



    //Create the user as a mobile client to simulate the fillm out form trigger from web
    private void createUserAndUpdateForTests() throws Exception {
        //signOn and get the user token
        phoneNumber = String.valueOf(System.currentTimeMillis());
        userToken = serverHelper.signOn();
        log4j.info("userToken: " + userToken);
        //join the club with the user token
        userKey = automationsHelper.joinClubWithUserToken(phoneNumber, userToken);

        log4j.info("userKey: " + userKey);
        FindMember findMember = new FindMember();
        findMember.setUserKey(userKey);
        MemberDetails member = MembersService.findAndGetMember(findMember);
        automationsHelper.performUpdateMemberDetails(phoneNumber);
        Navigator.waitForLoad();
        log4j.info("Update user with relevant details(phone,name,birthday)");
        key = automationsHelper.getKey(userKey);
        log4j.info("userKey: " + key);

    }

    /**
     * Test triggerFill out form with condition Form name is Exactly the form, then tag member
     */
    @Test
    @Category({Hub2Regression.class})
    public void testConditionFormNameIsExactlyAndActionRegisterMember() throws Exception {

        createUserAndUpdateForTests();
        automationsHelper.unregisterMember();

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickAllMembersBtn();
        tag = ActionsBuilder.TAG + timestamp.toString();
        FilledOutAFormCondition condition = buildFormCondition(ListOperators.IS_EXACTLY);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(globalCondition,condition), Lists.newArrayList(ActionsBuilder.buildRegisterAction()));
        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        openFormFillAndSubmit();

        checkDataStoreUpdatedWithRegisterByRuleID();

    }


    /**
    * Test triggerFill out form with condition Form name is Exactly the form, then tag member
     */
    @Test
    public void testConditionFormNameIsExactlyAndTagMemberAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        FilledOutAFormCondition condition = buildFormCondition(ListOperators.IS_EXACTLY);
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(condition), Lists.newArrayList(ActivityActions.Tag),tag);
        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        openFormFillAndSubmit();

        checkDataStoreUpdatedWithTagByRuleID();
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }


    /**
     * Test triggerFill out form with condition Form name is not the form, then send benefit
     */
    @Test
    public void testConditionFormNameIsNotAndSendBenefitAction() throws Exception {

        createSmartGift();

        FormPage formPage1 = HubFormsCreator.createForm(String.valueOf(System.currentTimeMillis()),HubFormsCreator.AUTO_TITLE_PREFIX, HubFormsCreator.AUTO_RECIPIENTS,
                HubFormsCreator.AUTO_THANK_YOU_TITLE_PREFIX, HubFormsCreator.AUTO_THANK_YOU_CONTENT_PREFIX, HubFormsCreator.AUTO_DESCRIPTION_PREFIX);
        FilledOutAFormCondition condition = buildFormCondition(ListOperators.IS_NOT);
        condition.setConditionValue(formPage1.getTitle());

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(condition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());
        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        openFormFillAndSubmit();

        checkDataStoreUpdatedWithAssetByRuleID();
        log4j.info("member received the asset");
        log4j.info(smartGift.getTitle() + " was send to the member ");
        Navigator.contentTabGeneralForms();
        ContentService.deleteForm(formPage1.getTitle());

    }

    /**
     * Test triggerFill out form with condition Form name is not the form, then send benefit
     */
    @Test
    public void testConditionFormNameIsExactlyAndSendBenefitAction() throws Exception {

        createSmartGift();

        FilledOutAFormCondition condition = buildFormCondition(ListOperators.IS_EXACTLY);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(condition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());
        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        openFormFillAndSubmit();

        checkDataStoreUpdatedWithAssetByRuleID();
        log4j.info("member received the asset");
        log4j.info(smartGift.getTitle() + " was send to the member ");


    }

    @Test
    public void testWithGlobalConditionSpecificMembersAndActionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(globalCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        MembersService.findAndGetMember(phoneNumber);
        Navigator.waitForLoad();
        performAnActionTagMember("Gold");

        openFormFillAndSubmit();

        checkDataStoreUpdatedWithTagByRuleID();
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        openFormFillAndSubmit();

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        openFormFillAndSubmit();
        openFormFillAndSubmit();
        openFormFillAndSubmit();

        checkDataStoreUpdatedWithSendPushNotification(2);

    }



    private FilledOutAFormCondition buildFormCondition(String operator) throws InterruptedException {
        FilledOutAFormCondition filledOutAFormCondition = new FilledOutAFormCondition();
        filledOutAFormCondition.setOperatorKey(operator)
                .setConditionValue(formPage.getTitle());
        TimeUnit.SECONDS.sleep(3);

        return filledOutAFormCondition;
    }



    private void createForm() throws Exception {
        System.out.println("in create form");;
        formPage = HubFormsCreator.createForm(timestamp,HubFormsCreator.AUTO_TITLE_PREFIX, HubFormsCreator.AUTO_RECIPIENTS,
                HubFormsCreator.AUTO_THANK_YOU_TITLE_PREFIX, HubFormsCreator.AUTO_THANK_YOU_CONTENT_PREFIX, HubFormsCreator.AUTO_DESCRIPTION_PREFIX);
        String formTitle = formPage.getTitle();

        //add field
        FormFieldPage fieldPage = HubFormsCreator.addFormField(timestamp.toString(), FormFieldAnswerType.FREE_TEXT_LINE, YesNoList.YES.toString());

        ContentService.updateForm(formTitle,fieldPage);

    }

    private void openFormFillAndSubmit() throws Exception {
        Navigator.contentTabGeneralForms();
        String url = ContentService.extractFormUrl(formPage.getTitle());
        ContentService.openFormInWebWithUserToken(url, BaseContentPage.FormSubmitButton,userToken);

        String input = "This is a test for form trigger" ;
        ContentService.fillForm(input ,FormFieldPage.TEXT.toString());

        ContentService.submitForm(formPage);
        ContentService.closeFormTab();
        Thread.sleep(10000);
    }

    @After
    public void cleanData() throws Exception{
        deactivateAutomation();
    }


    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.FilledOutForm;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.FilledOutForm.toString() +"_";
    }
}
