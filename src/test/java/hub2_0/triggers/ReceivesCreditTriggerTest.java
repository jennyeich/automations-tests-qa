package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import common.BaseTest;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.operations.PointsCreditSettingsPage;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.IntOperator;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.conditions.CreditCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Calendar;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class ReceivesCreditTriggerTest extends BaseRuleHub2Test {


    private static Box box;
    private static boolean firstTest = true;

    @BeforeClass
    public static void createNewBox() throws Exception {
        BaseTest.sqlDB.setHub2Wallet(resolveLocationId(locationId), PointsCreditSettingsPage.BusinessWallet.Budget.dbValue());
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = tag + timestamp.toString();
            createSmartGift();
            if(firstTest ) {
                serverHelper.configureDefaultBackendForPayWithBudget();
                firstTest = false;
            }

        }
    };


    @Test
    public void testConditionNumberOfCreditIsGreaterThanAndTagMemberAction() throws Exception {

        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.CREDIT_AMOUNT).
                setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("200");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(creditCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenCredit = "400";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionNumberOfCreditIsLessThanAndTagMemberAction() throws Exception {

        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.CREDIT_AMOUNT).
                setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("200");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(creditCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenCredit = "100";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionNumberOfCreditIsGreaterThanOrEqualToAndSendBenefitAction() throws Exception {

        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.CREDIT_AMOUNT).
                setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("300");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(creditCondition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenCredit = "300";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);

        checkDataStoreUpdatedWithAssetByRuleID();
        log4j.info("member received the asset");
        log4j.info(smartGift.getTitle() + " was send to the member ");

    }

    @Test public void testConditionNumberOfCreditIsLessThanOrEqualToAndSendBenefitAction() throws Exception {


        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.CREDIT_AMOUNT).
                setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("300");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(creditCondition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenCredit = "50";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);
        checkDataStoreUpdatedWithAssetByRuleID();

    }

    @Test
    @Category(Hub2Regression.class)
    public void testConditionNumberOfCreditIsExactlyAndTagMemberAction() throws Exception {

        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.CREDIT_AMOUNT).
                setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("200");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(creditCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenCredit = "200";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionTotalSumValueIsGreaterThanAndTagMemberAction() throws Exception {

        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.NEW_BALANCE).
                setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("200");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(creditCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenCredit = "300";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);
        checkAutomationRunAndTagMember();

    }

    @Test
    @Category(Hub2Regression.class)
    public void testConditionTotalSumValueIsLessThanAndTagMemberAction() throws Exception {

        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.NEW_BALANCE).
                setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("200");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(creditCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenCredit = "50";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionTotalSumValueIsGreaterThanOrEqualToAndSendBenefitAction() throws Exception {

        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.NEW_BALANCE).
                setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("200");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(creditCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenCredit = "300";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testConditionTotalSumValueIsLessThanOrEqualToAndTagAction() throws Exception {

        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.NEW_BALANCE).
                setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("200");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(creditCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenCredit = "200";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);
        checkAutomationRunAndTagMember();

    }

    @Test
    @Category({Hub2Regression.class})
    public void testConditionTotalSumValueIsExactlyAndTagMemberAction() throws Exception {

        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.NEW_BALANCE).
                setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("300");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(creditCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenCredit = "300";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testWithGlobalConditionSpecificMembersAndActionTagMember() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(globalCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        NewMember newMember = createNewMember();

        performAnActionTagMember(newMember, "Gold");
        String givenCredit = "300";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);
        checkAutomationRunAndTagMember();

    }

    @Test
    @Category({Hub2Regression.class})
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        createNewMember();

        String givenCredit = "300";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of receives credit with global condition of limit up to 2 times in total per member. The action is tag member. the test verify that the member received the push only 2 times")
    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        createNewMember();

        String givenCredit1 = "100";
        String givenCredit2 = "200";
        String givenCredit3 = "300";

        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit1,timestamp);
        MembersService.performSmartActionAddCredit(givenCredit2,timestamp);
        MembersService.performSmartActionAddCredit(givenCredit3,timestamp);

        checkDataStoreUpdatedWithSendPushNotification(2);
    }



    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.ReceivesCredit;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.ReceivesCredit.toString() +"_";
    }


}
