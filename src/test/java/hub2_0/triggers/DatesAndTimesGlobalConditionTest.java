package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.enums.ActivityActions;
import hub.hub2_0.common.enums.ActivityStatus;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.ActivityType;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Arrays;
import java.util.UUID;

public class DatesAndTimesGlobalConditionTest extends BaseRuleHub2Test {

    private static Box box;


    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };


    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }



    @ru.yandex.qatools.allure.annotations.Description("When member makes a purchase with date&time condition is set to 2 days from now, then tag member. " +
            "Negative test - purchase performed now")
    @Test
    public void testWithConditionBetweenDatesToFutureDateAndTagMemberActionNegative() throws Exception{

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active,getAutomationName());
        //Add condition
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();

        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayPlusXDays(2));
        DatePicker datePickerEnd = DatePicker.newDatePicker().setSelectEndDay(DateTimeUtility.getTodayPlusXDays(4));
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime().setDatePickerStart(datePickerStart).setDatePickerEnd(datePickerEnd);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Arrays.asList(dateTimeGlobalCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" +rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "1000", transactionId);
        //check that automation did not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

        log4j.info("member didn't received the tag");

    }



    @Test
    @Category({Hub2Regression.class})
    public void testWithConditionBetweenDatesFromNowToForeverAndTagMemberAction() throws Exception{

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active,getAutomationName());
        //Add condition
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();

        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime().setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Arrays.asList(dateTimeGlobalCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" +rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "1000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }




    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.MadeAPurchase;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.MadeAPurchase.toString() +"_";
    }
}
