package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.services.Navigator;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.conditions.operators.ShoppingCartOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import integration.base.BaseRuleHub2Test;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class MakesAPurchaseTriggerTest extends BaseRuleHub2Test {

    private static Box box;


    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };


    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }



    /**
     * Test trigger make purchase with condition value of purchase is greater than 10, then tag member
     */
    @Test
    @Category({Hub2Regression.class})
    public void testWithConditionValueOfPurchaseIsGreaterThanAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("10");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition value of purchase is less than 2000, then tag member
     */
    @Test
    public void testWithConditionValueOfPurchaseIsLessThanAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("200000");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "10000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition value of purchase is greater than or equal to 10, then tag member
     */
    @Test
    public void testWithConditionValueOfPurchaseIsGreaterThanOrEqualToAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("10");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());


        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "1000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition value of purchase is less than or equal to 1000, then tag member
     */
    @Test
    public void testWithConditionValueOfPurchaseIsLessThanOrEqualToAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("100000");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());


        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "10000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition value of purchase is exactly 100, then tag member
     */
    @Test
    public void testWithConditionValueOfPurchaseIsExactlyAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("100");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        //perform submit purchase
        performAnActionSubmitPurchase(newMember, "10000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }



    /**
     * Test trigger make purchase with condition Purchase Tags is not one of 'test1','test2','test3', then send benefit
     */
    @Test
    @Category({Hub2Regression.class})
    public void testWithConditionPurchaseTagsIsNotOneOfAndSendBenefitAction() throws Exception {

        smartGift = hubAssetsCreator.createSmartGift(timestamp);

        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.PURCHASE_TAGS)
                .setOperatorKey(InputOperators.IS_NOT_ONE_OF)
                .setConditionValue("test1, test2, test3");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.SendBenefit), smartGift.getTitle());
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        //perform submit purchase
        performAnActionSubmitPurchase(newMember, "2000", (ArrayList<String>) Lists.newArrayList("test5"), transactionId);
        //DataStore check
        checkDataStoreUpdated(transactionId);

    }

    /**
     * Test trigger make purchase with condition Purchase Tags is one of 'test1','test2','test3', then send benefit
     */
    @Test
    public void testWithConditionPurchaseTagsIsOneOfAndSendBenefitAction() throws Exception {

        smartGift = hubAssetsCreator.createSmartGift(timestamp);

        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.PURCHASE_TAGS)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("test1,test2,test3");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.SendBenefit), smartGift.getTitle());
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        //perform submit purchase
        performAnActionSubmitPurchase(newMember, "2000", (ArrayList<String>) Lists.newArrayList("test3"), transactionId);
        //DataStore check
        checkDataStoreUpdated(transactionId);

    }


    /**
     * Test trigger make purchase with condition shopping cart contains '2244', then send benefit
     */
    @Test
    public void testWithConditionShoppingCartContainsAndSendBenefitAction() throws Exception {

        smartGift = hubAssetsCreator.createSmartGift(timestamp);

        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_EXECTLY)
                .setConditionValue("1")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.SendBenefit), smartGift.getTitle());
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 10000);
        Item item2 = createItem("3000", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000", new ArrayList<>(), transactionId, items);

        //DataStore check
        checkDataStoreUpdated(transactionId);

    }

    /**
     * Test trigger make purchase with condition shopping cart does not contains '2244', then send benefit
     */
    @Test
    public void testWithConditionShoppingCartDoesNotContainsAndSendBenefitAction() throws Exception {

        smartGift = hubAssetsCreator.createSmartGift(timestamp);

        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.DOES_NOT_CONTAIN)
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.SendBenefit), smartGift.getTitle());
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2255", "itemA", "depA", "depAA", 10000);
        Item item2 = createItem("3000", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000", new ArrayList<>(), transactionId, items);

        //DataStore check
        checkDataStoreUpdated(transactionId);

    }

    /**
     * Negative test - purchase shopping cart contain item '2244'
     * Test trigger make purchase with condition shopping cart does not contains '2244', then tag member
     */
    @Test
    public void testWithConditionShoppingCartDoesNotContainsAndSendBenefitActionNegative() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.DOES_NOT_CONTAIN)
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 10000);
        Item item2 = createItem("3000", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000", new ArrayList<>(), transactionId, items);

        //check that automation did not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

        log4j.info("member didn't received the tag");

    }

    /**
     * Test trigger make purchase with condition shopping cart contains more than 1 of '2244' item, then send tag member
     */
    @Test
    @Category({Hub2Regression.class})
    public void testWithConditionShoppingCartContainsMoreThanAndTagMemberAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_MORE_THAN)
                .setConditionValue("1")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 10000);
        Item item2 = createItem("2244", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000", new ArrayList<>(), transactionId, items);

        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition shopping cart contains less than 2 of '2244' item, then send tag member
     */
    @Test
    public void testWithConditionShoppingCartContainsLessThanAndTagMemberAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_LESS_THAN)
                .setConditionValue("2")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, items);

        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Negative test - purchase shopping cart contain more than 2 of item '2244'
     * Test trigger make purchase with condition shopping cart contains less than 2 of '2244' item, then send tag member
     */
    @Test
    public void testWithConditionShoppingCartContainsLessThanAndTagMemberActionNegative() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_LESS_THAN)
                .setConditionValue("2")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 10000);
        Item item2 = createItem("2244", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000", new ArrayList<>(), transactionId, items);

        //check that automation did not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

        log4j.info("member didn't received the tag");

    }

    /**
     * Test trigger make purchase with condition shopping cart contains at least 2 of '2244' item, then send tag member
     */
    @Test
    public void testWithConditionShoppingCartContainsAtLeastAndTagMemberAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_AT_LEAST)
                .setConditionValue("2")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 10000);
        Item item2 = createItem("2244", "itemA", "depA", "depAA", 10000);
        Item item3 = createItem("5455", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        items.add(item3);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000", new ArrayList<>(), transactionId, items);

        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Negative test - purchase shopping cart contain 1 of item '2244'
     * Test trigger make purchase with condition shopping cart contains at least 2 of '2244' item, then send tag member
     */
    @Test
    public void testWithConditionShoppingCartContainsAtLeastAndTagMemberActionNegative() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_AT_LEAST)
                .setConditionValue("2")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000", new ArrayList<>(), transactionId, items);

        //check that automation did not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

        log4j.info("member didn't received the tag");

    }

    /**
     * Test trigger make purchase with condition shopping cart contains at least 2 of '2244' item, then send tag member
     */
    @Test
    public void testWithConditionShoppingCartContainsNoMoreThanAndTagMemberAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_NO_MORE_THAN)
                .setConditionValue("2")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, items);

        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Negative test - purchase shopping cart contain more than 2 of item '2244'
     * Test trigger make purchase with condition shopping cart contains at least 2 of '2244' item, then send tag member
     */
    @Test
    public void testWithConditionShoppingCartContainsNoMoreThanAndTagMemberActionNegative() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_NO_MORE_THAN)
                .setConditionValue("2")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 10000);
        Item item2 = createItem("2244", "itemA", "depA", "depAA", 10000);
        Item item3 = createItem("2244", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        items.add(item2);
        items.add(item3);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, items);

        //check that automation did not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

        log4j.info("member didn't received the tag");

    }

    /**
     * Test trigger make purchase with condition value of specific item '2244' is greater than 10, then tag member
     */
    @Test
    public void testWithConditionValueOfSpecificItemsIsGreaterThanAndActionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("10")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, items);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);


        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition value of specific item '2244' is less than 600, then tag member
     */
    @Test
    @Category({Hub2Regression.class})
    public void testWithConditionValueOfSpecificItemsIsLessThanAndActionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("600")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, items);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);


        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Negative test - the item value is 100
     * Test trigger make purchase with condition value of specific item '2244' is greater than or equal to 200, then tag member
     */
    @Test
    public void testWithConditionValueOfSpecificItemsIsGreaterThanOrEqualToAndActionTagMemberNegative() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("200")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, items);
        //check that automation did not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

        log4j.info("member didn't received the tag");

    }

    /**
     * Test trigger make purchase with condition value of specific item '2244' is greater than or equal to 400, then tag member
     */
    @Test
    public void testWithConditionValueOfSpecificItemsIsGreaterThanOrEqualToAndActionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("400")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 40000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "40000", new ArrayList<>(), transactionId, items);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);


        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition value of specific item '2244' is less than or equal to 800, then tag member
     */
    @Test
    public void testWithConditionValueOfSpecificItemsIsLessThanOrEqualToAndActionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("800")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 40000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "40000", new ArrayList<>(), transactionId, items);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);


        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition value of specific item '2244' is exactly 800, then tag member
     */
    @Test
    public void testWithConditionValueOfSpecificItemsIsExactlyAndActionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("800")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2244", "itemA", "depA", "depAA", 80000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "80000", new ArrayList<>(), transactionId, items);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);


        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");
    }

    private void checkDataStoreUpdated(String transactionId) throws Exception {
        checkDataStoreUpdatedWithAsset(transactionId);

        Navigator.refresh();

        Assert.assertTrue("The member did not get the gift sent by the automation in the hub,but it appears in the dataStore.", MembersService.isGiftExists(smartGift.getTitle()));
        log4j.info("member received the gift");
        log4j.info(smartGift.getTitle() + " was added to the member ");
    }

    private void buildRuleAndActivity(List<ApplyCondition> conditions,List<ActivityActions> activityActions,String freeText ) throws Exception {
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(conditions), Lists.newArrayList(activityActions), freeText);
        log4j.info("rule name:" + rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
    }


    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.MadeAPurchase;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.MadeAPurchase.toString() +"_";
    }
}
