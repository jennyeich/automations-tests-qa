package hub2_0.triggers;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.conditions.ExternalEventSubmittedCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.IServerResponse;
import server.v2_8.SubmitEvent;
import server.v2_8.SubmitEventResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Customer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class ExternalEventSubmittedTriggerTest extends BaseRuleHub2Test {

    private static Box box;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = tag + timestamp.toString();

        }
    };


    @Test
    @Category({Hub2Regression.class})
    public void testConditionExternalEventTypeIsOneOfActionTagMember() throws Exception {

        ExternalEventSubmittedCondition externalEventCondition = new ExternalEventSubmittedCondition();
        externalEventCondition.setConditionKey(ExternalEventSubmittedCondition.ConditionKey.TYPE)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Beacon");
        createActivityTrigger(Arrays.asList(externalEventCondition), ActivityActions.Tag, timestamp.toString());

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Enter",new ArrayList<>());

        checkDataStoreUpdatedWithTagByRuleID(timestamp.toString());
    }

    @Test
    public void testConditionExternalEventSubTypeIsOneOfActionTagMember() throws Exception {

        ExternalEventSubmittedCondition externalEventCondition = new ExternalEventSubmittedCondition();
        externalEventCondition.setConditionKey(ExternalEventSubmittedCondition.ConditionKey.SUBTYPE)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Enter");
        createActivityTrigger(Arrays.asList(externalEventCondition), ActivityActions.Tag, timestamp.toString());

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Enter",new ArrayList<>());

        checkDataStoreUpdatedWithTagByRuleID(timestamp.toString());
    }


    @Test
    public void testConditionExternalEventTypeIsOneOfActionTagMemberNegative() throws Exception {

        ExternalEventSubmittedCondition externalEventCondition = new ExternalEventSubmittedCondition();
        externalEventCondition.setConditionKey(ExternalEventSubmittedCondition.ConditionKey.SUBTYPE)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Enter , Exit");
        createActivityTrigger(Arrays.asList(externalEventCondition), ActivityActions.Tag, timestamp.toString());

             //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Aborted",new ArrayList<>());

        //check that the automation did not run
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }

    @Test
    public void  testWithConditionExternalEventTypeOneOfANDSubTypeOneOfTagMember() throws Exception {

        ExternalEventSubmittedCondition externalEventCondition1 = new ExternalEventSubmittedCondition();
        externalEventCondition1.setConditionKey(ExternalEventSubmittedCondition.ConditionKey.TYPE)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Beacon");
        ExternalEventSubmittedCondition externalEventCondition2 = new ExternalEventSubmittedCondition();
        externalEventCondition2.setConditionKey(ExternalEventSubmittedCondition.ConditionKey.SUBTYPE)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Enter");

        createActivityTrigger(Arrays.asList(externalEventCondition1, externalEventCondition2), ActivityActions.Tag, timestamp.toString());

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Enter",new ArrayList<>());

        checkDataStoreUpdatedWithTagByRuleID(timestamp.toString());
    }

    @Test
    public void testConditionExternalEventSubTypeNotEqualsActionTagMember() throws Exception {

        ExternalEventSubmittedCondition externalEventCondition = new ExternalEventSubmittedCondition();
        externalEventCondition.setConditionKey(ExternalEventSubmittedCondition.ConditionKey.SUBTYPE)
                .setOperatorKey(InputOperators.IS_NOT_ONE_OF)
                .setConditionValue("Enter");

        createActivityTrigger(Arrays.asList(externalEventCondition), ActivityActions.Tag, timestamp.toString());

        //Create member
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        performAnActionSubmitEvent(Lists.newArrayList(member),"Beacon","Exit",new ArrayList<>());

        checkDataStoreUpdatedWithTagByRuleID(timestamp.toString());
    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of external event submitted with global condition of specific member. The action is tag member.")
    @Test
    public void testWithGlobalConditionSpecificMembersAndActionTagMember() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, com.beust.jcommander.internal.Lists.newArrayList(globalCondition), com.beust.jcommander.internal.Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        NewMember newMember = createNewMember();
        performAnActionTagMember(newMember, "Gold");

        performAnActionSubmitEvent(Lists.newArrayList(newMember),"Beacon","Exit",new ArrayList<>());

        checkAutomationRunAndTagMember();
    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of external event submitted with global condition of between dates and not on the current day. The action is tag member. the test verify that the member didn't get the tag")
    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, com.beust.jcommander.internal.Lists.newArrayList(dateTimeGlobalCondition), com.beust.jcommander.internal.Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();

        performAnActionSubmitEvent(Lists.newArrayList(newMember),"Beacon","Exit",new ArrayList<>());
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of external event submitted with global condition of limit up to 2 times in total per member. The action is tag member. the test verify that the member received the push only 2 times")
    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, com.beust.jcommander.internal.Lists.newArrayList(limitsGlobalCondition), com.beust.jcommander.internal.Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();

        performAnActionSubmitEvent(Lists.newArrayList(newMember),"Beacon","Enter",new ArrayList<>());
        performAnActionSubmitEvent(Lists.newArrayList(newMember),"Beacon","Exit",new ArrayList<>());
        Thread.sleep(5000);
        performAnActionSubmitEvent(Lists.newArrayList(newMember),"Beacon","Aborted",new ArrayList<>());
        Thread.sleep(3000);
        checkDataStoreUpdatedWithSendPushNotification(2);
    }



    private void createActivityTrigger(List<ApplyCondition> externalEventSubmittedConditions, ActivityActions activityActions, String text) throws Exception{
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, externalEventSubmittedConditions,
                Arrays.asList(activityActions), text);
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
    }

    private void performAnActionSubmitEvent(List<NewMember> members, String type, String subType, ArrayList<String> tags) {

        SubmitEvent submitEvent = new SubmitEvent();
        submitEvent.setEventTime(timestamp.toString());
        submitEvent.setType(type);
        submitEvent.setSubType(subType);
        submitEvent.setTags(tags);

        for (int i = 0; i < members.size(); i++) {
            Customer customer = new Customer();
            customer.setPhoneNumber(members.get(i).getPhoneNumber());
            submitEvent.getCustomers().add(customer);
            log4j.info("send request submit event on member: " + customer.getPhoneNumber());
        }

        log4j.info("send request submit event with type: " + type + ", subType: " + subType);
        // Send the request
        IServerResponse response = submitEvent.SendRequestByApiKey(apiKey, SubmitEventResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

    }


    @After
    public void cleanData() throws Exception{
        deactivateAutomation();
    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.ExternalEventSubmitted;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.ExternalEventSubmitted.toString() +"_";
    }

}
