package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.conditions.PaysWithPointsAtPOSCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.utils.UserActionsConstants;

import java.util.Calendar;
import java.util.UUID;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class PaysWithPointsAtPOSTrigger extends BaseRuleHub2Test {

    private static Box box;

    @BeforeClass
    public static void createNewBox() throws Exception {

        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };


    @Test
    @Category({Hub2Regression.class})
    public void testPaysWithPointsAndPaymentGreaterEqualThanActionTag() throws Exception {

        PaysWithPointsAtPOSCondition paysWithPointsAtPOSCondition = new PaysWithPointsAtPOSCondition();
        paysWithPointsAtPOSCondition.setConditionKey(PaysWithPointsAtPOSCondition.ConditionKey.PAYMENT_AMOUNT)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("300");
        createAutomationAndVerify(paysWithPointsAtPOSCondition, "50000");
    }

    @Test
    @Category({Hub2Regression.class})
    public void testPaysWithPointsAndPaymentLessThanActionTag() throws Exception {

        PaysWithPointsAtPOSCondition paysWithPointsAtPOSCondition = new PaysWithPointsAtPOSCondition();
        paysWithPointsAtPOSCondition.setConditionKey(PaysWithPointsAtPOSCondition.ConditionKey.PAYMENT_AMOUNT)
                .setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("100");
        createAutomationAndVerify(paysWithPointsAtPOSCondition, "5000");
    }

    @Test
    public void testPaysWithPointsAndMemberPointsGreaterThanActionTag() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.Points.toString())
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("100");

        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        createAutomationAndVerify(globalCondition, "50000");
    }

    @Test
    public void testConditionPaymentAmountIsGreaterThanAndTagMember() throws Exception {

        PaysWithPointsAtPOSCondition paysWithPointsAtPOSCondition = new PaysWithPointsAtPOSCondition();
        paysWithPointsAtPOSCondition.setConditionKey(PaysWithPointsAtPOSCondition.ConditionKey.PAYMENT_AMOUNT)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("100");
        createAutomationAndVerify(paysWithPointsAtPOSCondition, "20000");

    }

    @Test
    public void testConditionPaymentAmountIsLessThanOrEqualToAndTagMember() throws Exception {

        PaysWithPointsAtPOSCondition paysWithPointsAtPOSCondition = new PaysWithPointsAtPOSCondition();
        paysWithPointsAtPOSCondition.setConditionKey(PaysWithPointsAtPOSCondition.ConditionKey.PAYMENT_AMOUNT)
                .setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("100");
        createAutomationAndVerify(paysWithPointsAtPOSCondition, "1000");

    }

    @Test
    public void testConditionPaymentAmountIsAndTagMember() throws Exception {

        PaysWithPointsAtPOSCondition paysWithPointsAtPOSCondition = new PaysWithPointsAtPOSCondition();
        paysWithPointsAtPOSCondition.setConditionKey(PaysWithPointsAtPOSCondition.ConditionKey.PAYMENT_AMOUNT)
                .setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("100");
        createAutomationAndVerify(paysWithPointsAtPOSCondition, "10000");

    }

    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        String actionTag = MembersService.performSmartActionAddPoints("70000",timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);
        performPayWithBudget(newMember.getPhoneNumber(), "5000", transactionId);

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }

    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        tag = tag + timestamp.toString();

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String actionTag = MembersService.performSmartActionAddPoints("70000",timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);
        performPayWithBudget(newMember.getPhoneNumber(), "1000", transactionId);
        performPayWithBudget(newMember.getPhoneNumber(), "2000", transactionId);
        performPayWithBudget(newMember.getPhoneNumber(), "3000", transactionId);

        checkDataStoreUpdatedWithSendPushNotification(2);
    }



    private void createAutomationAndVerify(ApplyCondition condition, String spentPoints) throws Exception {
        tag = ActionsBuilder.TAG + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule, Lists.newArrayList(condition), Lists.newArrayList(ActivityActions.Tag),tag);
        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        String actionTag = MembersService.performSmartActionAddPoints("70000",timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(newMember.getPhoneNumber(), spentPoints, transactionId);
        checkAutomationRunAndTagMember();
    }


    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.PaysWithPointsAtPOS;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.PaysWithPointsAtPOS.toString();
    }

}
