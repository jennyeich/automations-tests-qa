package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AmountType;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.services.Navigator;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.cases.RuleCase;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.PointsAccumulationAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.cases.CaseMadeAPurchaseCondition;
import hub.hub2_0.common.conditions.cases.GeneralCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.MembershipStatus;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.conditions.operators.ShoppingCartOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub.hub2_0.common.builders.CasesConditionsBuilder;
import integration.base.BaseRuleHub2Test;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.ApiClientFields;
import server.utils.ServerHelper;
import server.v2_8.SubmitPurchaseResponse;
import server.v2_8.common.Item;

import java.util.*;

public class SplitCasesTest extends BaseRuleHub2Test {

    private static Box box;


    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
        serverHelper.saveApiClient(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(),true);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            createSmartGift();

        }
    };



    @ru.yandex.qatools.allure.annotations.Description("Rule with 'Makes a purchase' trigger, global condition and case1 with shopping cart condition + 2 actions(tag,send asset)")
    @Test
    @Category({Hub2Regression.class})
    public void testSplitCasesWithGMC1Case1CaseCondition2Actions() throws Exception {

        final String ITEM_CODE = "4321";


        tag = ActionsBuilder.TAG + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());


        /***********global condition***************/
        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.AllowEmail.toString()).toggleBooleanValue(ActivityCondition.BOOLEAN_YES);
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        specificMembers.setMembershipStatus(MembershipStatus.newMembershipStatus().setRegister(MembershipStatus.CHECK).setPending(MembershipStatus.UNCHECK));
        globalCondition.setSpecificMembers(specificMembers);


        GeneralCondition generalCondition = CasesConditionsBuilder.buildGeneralConditionMadeAPurchase(ITEM_CODE, MadeAPurchaseCondition.ConditionKey.SHOPPING_CART, ShoppingCartOperators.CONTAINS_AT_LEAST, "1");

        RuleCase aCase = new RuleCase();
        aCase.setConditions(Lists.newArrayList(generalCondition));

        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction(tag);
        ActivityAction activityAction2 = ActionsBuilder.buildSendBenefitAction(smartGift.getTitle());

        aCase.setActions(Lists.newArrayList(activityAction1,activityAction2));
        Activity activity = ruleBuilder.buildActivityWithCases(getTrigger().toString(), box, rule, Lists.newArrayList(globalCondition), Lists.newArrayList(aCase));

        LoyaltyService.createActivityInBox(activity);
        log4j.info("rule name:" +rule.getActivityName());
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();

        //trigger submit with condition result false - check asset and tag not received
        String transactionId = buildSubmitPurchaseRequest("5555",newMember);
        //check negative data store
        checkDataStoreNegative(RECEIVEDASSET,transactionId);
        checkDataStoreNegative(TAG_OPERATION,transactionId);


        //again- trigger submit with condition result true - check asset and tag received
        transactionId = buildSubmitPurchaseRequest(ITEM_CODE,newMember);
        //DataStore check
        checkDataStoreUpdatedWithAsset(transactionId);
        checkDataStoreUpdatedWithTag(transactionId);

        Navigator.refresh();

        Assert.assertTrue("The member did not get the gift sent by the automation in the hub,but it appears in the dataStore.", MembersService.isGiftExists(smartGift.getTitle()));
        log4j.info("member received the gift");
        log4j.info(smartGift.getTitle() + " was added to the member ");
    }



    @ru.yandex.qatools.allure.annotations.Description("Rule with 'Makes a purchase' trigger, global condition and " +
            "case1:shopping cart condition + 1 tag action," +
            "case2: shopping cart condition + 1 send asset action" +
            "Results: case2 passes and send asset")
    @Test
    public void testSplitCasesWith2Cases1CaseCondition1ActionsEachCase_Case2True() throws Exception {

        final String ITEM_CODE = "4321";

        tag = ActionsBuilder.TAG + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        /****case1****/
        GeneralCondition generalCondition = CasesConditionsBuilder.buildGeneralConditionMadeAPurchase(ITEM_CODE,MadeAPurchaseCondition.ConditionKey.SHOPPING_CART, ShoppingCartOperators.CONTAINS_AT_LEAST, "2");
        RuleCase aCase = new RuleCase();
        aCase.setConditions(Lists.newArrayList(generalCondition));
        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction(tag);
        aCase.setActions(Lists.newArrayList(activityAction1));


        /****case2****/
        GeneralCondition generalCondition2 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchase(ITEM_CODE,MadeAPurchaseCondition.ConditionKey.SHOPPING_CART, ShoppingCartOperators.CONTAINS_NO_MORE_THAN, "2");
        RuleCase aCase2 = new RuleCase();
        aCase2.setConditions(Lists.newArrayList(generalCondition2));
        ActivityAction activityAction2 = ActionsBuilder.buildSendBenefitAction(smartGift.getTitle());
        aCase2.setActions(Lists.newArrayList(activityAction2));

        //create activity with 2 cases
        Activity activity = ruleBuilder.buildActivityWithCases(getTrigger().toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(aCase,aCase2));

        LoyaltyService.createActivityInBox(activity);
        log4j.info("rule name:" +rule.getActivityName());
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();

        //trigger submit with codition result true for case 2 - check asset received but not tag
        String transactionId = buildSubmitPurchaseRequest(ITEM_CODE,newMember);
        //DataStore check
        checkDataStoreUpdatedWithAsset(transactionId);
        checkDataStoreNegative(TAG_OPERATION,transactionId);

        Navigator.refresh();

        Assert.assertTrue("The member did not get the gift sent by the automation in the hub,but it appears in the dataStore.", MembersService.isGiftExists(smartGift.getTitle()));
        log4j.info("member received the gift");
        log4j.info(smartGift.getTitle() + " was added to the member ");
    }


    @ru.yandex.qatools.allure.annotations.Description("Rule with 'Makes a purchase' trigger," +
            "case1:shopping cart condition + 1 tag action," +
            "case2: shopping cart condition + 1 send asset action" +
            "Results: case1 passes and tag member, not sending asset")
    @Test
    public void testSplitCasesWith2CasesAllTrue_OnlyCase1ActionPerform() throws Exception {

        final String ITEM_CODE = "4321";

        tag = ActionsBuilder.TAG + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        /****case1****/
        GeneralCondition generalCondition = CasesConditionsBuilder.buildGeneralConditionMadeAPurchase(ITEM_CODE,MadeAPurchaseCondition.ConditionKey.SHOPPING_CART, ShoppingCartOperators.CONTAINS_AT_LEAST, "1");
        RuleCase aCase = new RuleCase();
        aCase.setConditions(Lists.newArrayList(generalCondition));
        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction(tag);
        aCase.setActions(Lists.newArrayList(activityAction1));


        /****case2****/
        GeneralCondition generalCondition2 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchase(ITEM_CODE,MadeAPurchaseCondition.ConditionKey.SHOPPING_CART, ShoppingCartOperators.CONTAINS_NO_MORE_THAN, "2");
        RuleCase aCase2 = new RuleCase();
        aCase2.setConditions(Lists.newArrayList(generalCondition2));
        ActivityAction activityAction2 = ActionsBuilder.buildSendBenefitAction(smartGift.getTitle());
        aCase2.setActions(Lists.newArrayList(activityAction2));

        //create activity with 2 cases
        Activity activity = ruleBuilder.buildActivityWithCases(getTrigger().toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(aCase,aCase2));

        LoyaltyService.createActivityInBox(activity);
        log4j.info("rule name:" +rule.getActivityName());
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();

        //trigger submit with codition result true for case 2 - check tag received
        String transactionId = buildSubmitPurchaseRequest(ITEM_CODE,newMember);
        //DataStore check
        checkDataStoreUpdatedWithTag(transactionId);
        checkDataStoreNegative(RECEIVEDASSET,transactionId);

        Navigator.refresh();

    }
    @ru.yandex.qatools.allure.annotations.Description("Rule with 'Makes a purchase' trigger," +
            "case1:shopping cart condition + 1 tag action," +
            "case2: shopping cart condition + 1 send asset action" +
            "Results: case1 passes and tag member , Change order so case 2 will be the first, purchase again and check asset sent ")
    @Test
    @Category({Hub2Regression.class})
    public void testSplitCasesWith2CasesChangeOrder() throws Exception {

        final String ITEM_CODE = "4321";

        tag = ActionsBuilder.TAG + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        /****case1 --- Should pass****/
        GeneralCondition generalCondition = CasesConditionsBuilder.buildGeneralConditionMadeAPurchase(ITEM_CODE,MadeAPurchaseCondition.ConditionKey.SHOPPING_CART, ShoppingCartOperators.CONTAINS_EXECTLY, "1");
        RuleCase aCase = new RuleCase();
        aCase.setDescription("case 1");
        aCase.setConditions(Lists.newArrayList(generalCondition));
        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction(tag);
        aCase.setActions(Lists.newArrayList(activityAction1));


        /****case2 --- Should pass****/
        GeneralCondition generalCondition2 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchase(ITEM_CODE,MadeAPurchaseCondition.ConditionKey.SHOPPING_CART, ShoppingCartOperators.CONTAINS_EXECTLY, "1");
        RuleCase aCase2 = new RuleCase();
        aCase2.setDescription("case 2");
        aCase2.setConditions(Lists.newArrayList(generalCondition2));
        ActivityAction activityAction2 = ActionsBuilder.buildSendBenefitAction(smartGift.getTitle());
        aCase2.setActions(Lists.newArrayList(activityAction2));

        //create activity with 2 cases
        Activity activity = ruleBuilder.buildActivityWithCases(getTrigger().toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(aCase,aCase2));

        LoyaltyService.createActivityInBox(activity);
        log4j.info("rule name:" +rule.getActivityName());
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();

        //trigger submit with codition result true for both cases - check tag recieved but not asset
        String transactionId = buildSubmitPurchaseRequest(ITEM_CODE,newMember);
        //DataStore check
        checkDataStoreUpdatedWithTag(transactionId);
        checkDataStoreNegative(RECEIVEDASSET,transactionId);


        LoyaltyService.getRuleID(rule.getActivityName());

        //update rule with case 1 change order to be case 2
        aCase2 = new RuleCase();
        aCase2.clickOrderUp();
        Rule rule2 = new Rule( ActivityStatus.Active);
        rule2.clickPublishActivityBtn();
        Activity updateActivity = ruleBuilder.buildActivityWithCases(null, box, rule2, Lists.newArrayList(), Lists.newArrayList(new RuleCase(),aCase2),true);
        //update rule - click on case 2 up button to make it first case
        LoyaltyService.updateActivity(updateActivity);

        //trigger again submit with codition result true for both cases - check asset recieved but tag not
        transactionId = buildSubmitPurchaseRequest(ITEM_CODE,newMember);
        //DataStore check
        checkDataStoreUpdatedWithAsset(transactionId);
        checkDataStoreNegative(TAG_OPERATION,transactionId);


    }

    @ru.yandex.qatools.allure.annotations.Description("Rule with 'Makes a purchase' trigger, no condition  + 1 action tag member" +
            "Then split and add new case with shopping cart condition + send asset action," +
            "Result: first case with no condition is performed-tag member")
    @Test
    public void testSplitCases_splitAndAddCaseFirstCasePerformed() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(ActivityActions.Tag),tag);
        log4j.info("rule name:" + rule.getActivityName());


        LoyaltyService.createActivityInBox(activity);
        log4j.info("rule name:" +rule.getActivityName());
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        /****case2 --- Should pass****/
        GeneralCondition generalCondition2 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID);
        RuleCase aCase2 = new RuleCase();
        aCase2.clickAddCase();
        aCase2.setDescription("case 2");
        aCase2.setConditions(Lists.newArrayList(generalCondition2));
        ActivityAction activityAction2 = ActionsBuilder.buildSendBenefitAction(smartGift.getTitle());
        aCase2.setActions(Lists.newArrayList(activityAction2));
        Rule rule2 = new Rule( ActivityStatus.Active);
        rule2.clickPublishActivityBtn();
        rule2.clickSplitCases();
        Activity updateActivity = ruleBuilder.buildActivityWithCases(null, box, rule2, Lists.newArrayList(), Lists.newArrayList(new RuleCase(),aCase2),true);
        //update rule - click on case 2 up button to make it first case
        LoyaltyService.updateActivity(updateActivity);

        //Create member
        NewMember newMember = createNewMember();

        //trigger submit with codition result true - check only tag recieved (first case)
        String transactionId = UUID.randomUUID().toString();

        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, new ArrayList<>(),BRANCH_ID);
        //check negative data store
        checkDataStoreNegative(RECEIVEDASSET,transactionId);
        checkDataStoreUpdatedWithTag(transactionId);

    }

    @ru.yandex.qatools.allure.annotations.Description("Rule with 'Makes a purchase' trigger, 3 cases " +
            "case1: condition branch = 1 + tag action tag_1," +
            "case2: condition branch = 2 + tag action tag_2," +
            "case3: condition branch = 3 + tag action tag_3," +
            "run 3 times - each time one case performed and the others not.")
    @Test
    @Category({Hub2Regression.class})
    public void testSplitCasesWith3Cases_3BranchCondition_Perform3Times() throws Exception {

        final String BRANCH_ID_1 = "1";
        final String BRANCH_ID_2 = "2";
        final String BRANCH_ID_3 = "3";

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        /****case1****/
        GeneralCondition generalCondition = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID_1);
        RuleCase aCase = new RuleCase();
        aCase.setConditions(Lists.newArrayList(generalCondition));
        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction("TAG_1");
        aCase.setActions(Lists.newArrayList(activityAction1));


        /****case2****/
        GeneralCondition generalCondition2 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID_2);
        RuleCase aCase2 = new RuleCase();
        aCase2.setConditions(Lists.newArrayList(generalCondition2));
        ActivityAction activityAction2 = ActionsBuilder.buildTagMemberAction("TAG_2");
        aCase2.setActions(Lists.newArrayList(activityAction2));


        /****case3****/
        GeneralCondition generalCondition3 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID_3);
        RuleCase aCase3 = new RuleCase();
        aCase3.setConditions(Lists.newArrayList(generalCondition3));
        ActivityAction activityAction3 = ActionsBuilder.buildTagMemberAction("TAG_3");
        aCase3.setActions(Lists.newArrayList(activityAction3));
        //create activity with 3 cases
        Activity activity = ruleBuilder.buildActivityWithCases(getTrigger().toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(aCase,aCase2,aCase3));

        LoyaltyService.createActivityInBox(activity);
        log4j.info("rule name:" +rule.getActivityName());
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();

        log4j.info("trigger submit with condition case 3 result true - check only tag_3 received (last case)");
        String transactionId = UUID.randomUUID().toString();
        //perform submit purchase so case 3 will perform
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, new ArrayList<>(),BRANCH_ID_3);

        checkDataStoreWithCorrectTag(TAG_OPERATION,transactionId,BRANCH_ID_3,"TAG_3");
        checkNegativeDataStoreTag(TAG_OPERATION,transactionId,BRANCH_ID_2);
        checkNegativeDataStoreTag(TAG_OPERATION,transactionId,BRANCH_ID_1);

        log4j.info("trigger submit with condition case 2 result true - check only tag_2 received (second case)");
        transactionId = UUID.randomUUID().toString();
        //perform submit purchase so case 2 will perform
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, new ArrayList<>(),BRANCH_ID_2);

        checkDataStoreWithCorrectTag(TAG_OPERATION,transactionId,BRANCH_ID_2,"TAG_2");
        checkNegativeDataStoreTag(TAG_OPERATION,transactionId,BRANCH_ID_3);
        checkNegativeDataStoreTag(TAG_OPERATION,transactionId,BRANCH_ID_1);

        log4j.info("trigger submit with condition case 1 result true - check only tag_1 received (first case)");
        transactionId = UUID.randomUUID().toString();
        //perform submit purchase so case 1 will perform
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, new ArrayList<>(),BRANCH_ID_1);

        checkDataStoreWithCorrectTag(TAG_OPERATION,transactionId,BRANCH_ID_1,"TAG_1");
        checkNegativeDataStoreTag(TAG_OPERATION,transactionId,BRANCH_ID_3);
        checkNegativeDataStoreTag(TAG_OPERATION,transactionId,BRANCH_ID_2);

    }

    @ru.yandex.qatools.allure.annotations.Description("Rule with 'Makes a purchase' trigger, 3 cases " +
            "case1: condition branch = 1 + tag action tag_1," +
            "case2: condition branch = 2 + tag action tag_2," +
            "case3: condition branch = 3 + tag action tag_3," +
            "delete case 2 - perform purchase match case 2 - no action," +
            " perform purchase match case 3 - tag action tag_3 performed.")
    @Test
    public void testSplitCasesWith3Cases_DeleteCase2() throws Exception {

        final String BRANCH_ID_1 = "1";
        final String BRANCH_ID_2 = "2";
        final String BRANCH_ID_3 = "3";

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        /****case1****/
        GeneralCondition generalCondition = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID_1);
        RuleCase aCase = new RuleCase();
        aCase.setConditions(Lists.newArrayList(generalCondition));
        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction("TAG_1");
        aCase.setActions(Lists.newArrayList(activityAction1));


        /****case2****/
        GeneralCondition generalCondition2 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID_2);
        RuleCase aCase2 = new RuleCase();
        aCase2.setConditions(Lists.newArrayList(generalCondition2));
        ActivityAction activityAction2 = ActionsBuilder.buildTagMemberAction("TAG_2");
        aCase2.setActions(Lists.newArrayList(activityAction2));


        /****case3****/
        GeneralCondition generalCondition3 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID_3);
        RuleCase aCase3 = new RuleCase();
        aCase3.setConditions(Lists.newArrayList(generalCondition3));
        ActivityAction activityAction3 = ActionsBuilder.buildTagMemberAction("TAG_3");
        aCase3.setActions(Lists.newArrayList(activityAction3));
        //create activity with 3 cases
        Activity activity = ruleBuilder.buildActivityWithCases(getTrigger().toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(aCase,aCase2,aCase3));

        LoyaltyService.createActivityInBox(activity);
        log4j.info("rule name:" +rule.getActivityName());
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        aCase2 = new RuleCase();
        aCase2.deleteCase();
        Rule rule2 = new Rule( ActivityStatus.Active);
        rule2.clickPublishActivityBtn();
        Activity updateActivity = ruleBuilder.buildActivityWithCases(null, box, rule2, Lists.newArrayList(), Lists.newArrayList(new RuleCase(),aCase2,new RuleCase()),true);
        //update rule - delete case 2
        LoyaltyService.updateActivity(updateActivity);
        log4j.info("case 2 was deleted");

        //Create member
        NewMember newMember = createNewMember();

        log4j.info("trigger submit with codition case 2 result true - no action performed because this case was deleted");
        String transactionId = UUID.randomUUID().toString();
        //perform submit purchase so case 3 will perform
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, new ArrayList<>(),BRANCH_ID_2);
        checkNegativeDataStoreTag(TAG_OPERATION,transactionId,BRANCH_ID_3);
        checkNegativeDataStoreTag(TAG_OPERATION,transactionId,BRANCH_ID_2);
        checkNegativeDataStoreTag(TAG_OPERATION,transactionId,BRANCH_ID_1);

        log4j.info("trigger submit with codition case 3 result true - check only tag_3 receieved (last case)");

        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, new ArrayList<>(),BRANCH_ID_3);

        checkDataStoreWithCorrectTag(TAG_OPERATION,transactionId,BRANCH_ID_3,"TAG_3");
        checkNegativeDataStoreTag(TAG_OPERATION,transactionId,BRANCH_ID_2);
        checkNegativeDataStoreTag(TAG_OPERATION,transactionId,BRANCH_ID_1);

    }

    @ru.yandex.qatools.allure.annotations.Description("Rule with 'Makes a purchase' trigger, 3 cases " +
            "case1: condition branch = 1 + tag action tag_1," +
            "case2: condition branch = 2 + tag action tag_2," +
            "case3: condition branch = 3 + tag action tag_3," +
            "change order - case2 move to be last.")
    @Test
    public void testSplitCasesWith3Cases_ChangeOrderMiddleCase() throws Exception {

        final String BRANCH_ID_1 = "1";
        final String BRANCH_ID_2 = "2";

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        /****case1****/
        GeneralCondition generalCondition = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID_2);
        RuleCase aCase = new RuleCase();
        aCase.setDescription("case1");
        aCase.setConditions(Lists.newArrayList(generalCondition));
        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction("TAG_1");
        aCase.setActions(Lists.newArrayList(activityAction1));


        /****case2****/
        GeneralCondition generalCondition2 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID_1);
        RuleCase aCase2 = new RuleCase();
        aCase2.setDescription("case2");
        aCase2.setConditions(Lists.newArrayList(generalCondition2));
        ActivityAction activityAction2 = ActionsBuilder.buildTagMemberAction("TAG_2");
        aCase2.setActions(Lists.newArrayList(activityAction2));


        /****case3****/

        GeneralCondition generalCondition3 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID_1);
        RuleCase aCase3 = new RuleCase();
        aCase3.setDescription("case3");
        aCase3.setConditions(Lists.newArrayList(generalCondition3));
        ActivityAction activityAction3 = ActionsBuilder.buildTagMemberAction("TAG_3");
        aCase3.setActions(Lists.newArrayList(activityAction3));
        //create activity with 3 cases
        Activity activity = ruleBuilder.buildActivityWithCases(getTrigger().toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(aCase,aCase2,aCase3));

        LoyaltyService.createActivityInBox(activity);
        log4j.info("rule name:" +rule.getActivityName());
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();

        log4j.info("trigger submit with condition case 2 result true - check only tag_2 received (middle case)");
        String transactionId = UUID.randomUUID().toString();
        //perform submit purchase so case 3 will perform
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, new ArrayList<>(),BRANCH_ID_1);

        checkDataStoreWithCorrectTag(TAG_OPERATION,transactionId,BRANCH_ID_1,"TAG_2");
        checkataStoreNotContainsTag(TAG_OPERATION,transactionId,BRANCH_ID_1,"TAG_1");
        checkataStoreNotContainsTag(TAG_OPERATION,transactionId,BRANCH_ID_1,"TAG_3");

        LoyaltyService.getRuleID(rule.getActivityName());
        //update rule with case 2 change order to be case 3
        aCase2 = new RuleCase();
        aCase2.clickOrderDown();
        Rule rule2 = new Rule( ActivityStatus.Active);
        rule2.clickPublishActivityBtn();
        Activity updateActivity = ruleBuilder.buildActivityWithCases(null, box, rule2, Lists.newArrayList(), Lists.newArrayList(new RuleCase(),aCase2,new RuleCase()),true);
        //update rule - click on case 2 up button to make it first case
        LoyaltyService.updateActivity(updateActivity);
        log4j.info("update rule with case 2 change order to be case 3");


        log4j.info("trigger submit with condition case 3 result true - check only tag_3 received (middle case)");
        transactionId = UUID.randomUUID().toString();
        //perform submit purchase so case 3 will perform
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, new ArrayList<>(),BRANCH_ID_1);

        checkDataStoreWithCorrectTag(TAG_OPERATION,transactionId,BRANCH_ID_1,"TAG_3");
        checkataStoreNotContainsTag(TAG_OPERATION,transactionId,BRANCH_ID_1,"TAG_2");
        checkataStoreNotContainsTag(TAG_OPERATION,transactionId,BRANCH_ID_1,"TAG_1");


    }

    @ru.yandex.qatools.allure.annotations.Description("Rule with 'Makes a purchase' trigger," +
            "activity condition: TOTAL_SPEND IS_GREATER_THAN 10 " +
            "case1: condition branch = 1 + tag action tag_1," +
            "Result: activity condition false - no action, activity result true - check case condition")
    @Test
    public void testActivityWithActivityConditionFalseAndCaseConditionTrue_ActionNotPerformed() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("10");


        /****case1****/
        GeneralCondition generalCondition = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID);
        RuleCase aCase = new RuleCase();
        aCase.setDescription("case1");
        aCase.setConditions(Lists.newArrayList(generalCondition));
        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction(tag);
        aCase.setActions(Lists.newArrayList(activityAction1));


        //create activity with case
        Activity activity = ruleBuilder.buildActivityWithCases(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(aCase));

        LoyaltyService.createActivityInBox(activity);
        log4j.info("rule name:" +rule.getActivityName());
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();

        log4j.info("trigger submit with activity condition result false - No action performed");
        String transactionId = UUID.randomUUID().toString();
        //perform submit purchase so activity condition is false
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "200", new ArrayList<>(), transactionId, new ArrayList<>(),BRANCH_ID);

        checkDataStoreNegative(TAG_OPERATION,transactionId);

        transactionId = UUID.randomUUID().toString();
        log4j.info("trigger submit with activity condition result true + case condition false - No action");
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000", new ArrayList<>(), transactionId, new ArrayList<>(),"2");

        checkDataStoreNegative(TAG_OPERATION,transactionId);

        transactionId = UUID.randomUUID().toString();
        log4j.info("trigger submit with activity condition result true + case condition true - tag member performed");
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000", new ArrayList<>(), transactionId, new ArrayList<>(),BRANCH_ID);

        checkDataStoreUpdatedWithTag(transactionId);

    }

    @ru.yandex.qatools.allure.annotations.Description("Rule with 'Makes a purchase' trigger," +
            "activity condition: TOTAL_SPEND IS_GREATER_THAN 10 " +
            "case1: condition branch = 1 + condition pos = 1 + tag action tag_1," +
            "Result: activity condition true - check case condition")
    @Test
    public void testActivityWithActivityConditionAnd2CaseConditionTrue_ActionPerformed() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        final String PURCHASE_TAGS = "purchase1";

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("10");


        /****case1****/
        GeneralCondition generalCondition = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.BRANCH_ID, InputOperators.IS_ONE_OF, BRANCH_ID);
        GeneralCondition generalCondition1 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.PURCHASE_TAGS, InputOperators.IS_ONE_OF, PURCHASE_TAGS);
        RuleCase aCase = new RuleCase();
        aCase.setDescription("case1");
        aCase.setConditions(Lists.newArrayList(generalCondition,generalCondition1));
        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction(tag);
        aCase.setActions(Lists.newArrayList(activityAction1));


        //create activity with case
        Activity activity = ruleBuilder.buildActivityWithCases(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(aCase));

        LoyaltyService.createActivityInBox(activity);
        log4j.info("rule name:" +rule.getActivityName());
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();

        log4j.info("trigger submit with activity condition result true + case conditions true - tag member performed");
        String transactionId = UUID.randomUUID().toString();
        //perform submit purchase so activity condition is false
        ArrayList tags = new ArrayList();
        tags.add(PURCHASE_TAGS);
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000" ,tags, transactionId, new ArrayList<>(),BRANCH_ID);

        checkDataStoreUpdatedWithTag(transactionId);

    }


    @Test
    public void test2Cases_ActionPerOccurancesPerformedTwice() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        /*******case 1 per occurances**********/
        RuleCase case1 = new RuleCase();
        case1.setDescription("case1");
        timestamp = String.valueOf(System.currentTimeMillis());
        ActivityAction activityAction = ActionsBuilder.buildPointsAccumulationAction("10");

        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(ruleBuilder.selectItemsByItemCode("2233"));
        occurrencesCondition.setConditionKey(OccurrencesCondition.Condition.IN_THE_QUANTITY_OF);
        occurrencesCondition.setConditionValue("2");
        case1.setOccurrencesConditions(Lists.newArrayList(occurrencesCondition));
        case1.setActions(Lists.newArrayList(activityAction));

        /****case2****/
        CaseMadeAPurchaseCondition madeAPurchaseCondition = new CaseMadeAPurchaseCondition();
        madeAPurchaseCondition.clickDelete();
        GeneralCondition generalCondition = madeAPurchaseCondition;
        RuleCase case2 = new RuleCase();
        case2.setConditions(Lists.newArrayList(generalCondition));
        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction("TAG_1");
        case2.setActions(Lists.newArrayList(activityAction1));

        //create activity with case
        Activity activity = ruleBuilder.buildActivityWithCases(getTrigger().toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(case1,case2));

        LoyaltyService.createActivityInBox(activity);
        log4j.info("rule name:" +rule.getActivityName());
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();

        log4j.info("trigger submit with activity case 2 conditions true - send text performed");
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("2233","Coke","3", "Drinks", 10000);
        item.setQuantity(5);
        Item item2 = createItem("1133","Coke","3", "Drinks", 10000);
        item2.setQuantity(6);
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "20000", new ArrayList<>(), transactionId, Lists.newArrayList(item,item2));

        //DataStore check
        checkDataStoreUpdatedWithPointsByRuleID( "2000",AmountType.Points);

    }

    //Check that no user action was created for this action
    private void checkDataStoreWithCorrectTag(String actionName,String transactionId,String branchId,String tag) throws Exception {
        //give the data store time to update
        Thread.sleep(5000);
        log4j.info("check DataStore not updated with :" + actionName);
        Query q = new Query("UserAction");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, actionName),
                new Query.FilterPredicate("BranchID", Query.FilterOperator.EQUAL, branchId),
                new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, transactionId))));

        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the branchId: " + branchId,
                q , 1, dataStore);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info( "ExportData contains the following data : " + t.getValue());
        Assert.assertTrue("Data must contain the tag: " + tag, t.getValue().contains(tag));

    }

    //Check that no user action was created for this action
    private void checkataStoreNotContainsTag(String actionName,String transactionId,String branchId,String tag) throws Exception {
        //give the data store time to update
        Thread.sleep(5000);
        log4j.info("check DataStore not updated with :" + actionName);
        Query q = new Query("UserAction");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, actionName),
                new Query.FilterPredicate("BranchID", Query.FilterOperator.EQUAL, branchId),
                new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, transactionId))));

        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the branchId: " + branchId,
                q , 1, dataStore);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info( "ExportData contains the following data : " + t.getValue());
        Assert.assertTrue("Data must not contain the tag: " + tag, !t.getValue().contains(tag));

    }

    //Check that no user action was created for this action
    private void checkNegativeDataStoreTag(String actionName,String transactionId,String branchId) throws Exception {
        //give the data store time to update
        Thread.sleep(5000);
        log4j.info("check DataStore not updated with :" + actionName);
        Query q = new Query("UserAction");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, actionName),
                new Query.FilterPredicate("BranchID", Query.FilterOperator.EQUAL, branchId),
                new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, transactionId))));

        List<Entity> res = dataStore.findByQuery(q);
       Assert.assertEquals("Query should return empty results for branchId " + branchId,0,res.size());

    }

    @NotNull
    private String buildSubmitPurchaseRequest(String itemCode,NewMember newMember) throws Exception {
        String transactionId = UUID.randomUUID().toString();
        //perform submit purchase
        Item item = createItem(itemCode, "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, items);
        return transactionId;
    }




    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.MadeAPurchase;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.MadeAPurchase.toString() +"_";
    }
}
