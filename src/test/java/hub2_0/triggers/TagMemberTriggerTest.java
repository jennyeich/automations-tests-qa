package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AmountType;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.conditions.TagCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.limits.RollingPeriod;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Arrays;
import java.util.Calendar;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class TagMemberTriggerTest extends BaseRuleHub2Test {

    private static Box box;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = tag + timestamp.toString();
            createSmartGift();
        }
    };

    @After
    public void cleanData() throws Exception{
        deactivateAutomation();
    }


    @Test
    public void testWithConditionTagOperationTagNotEqualActionSendAsset() throws Exception {
        TagCondition tagCondition = new TagCondition();
        tagCondition.setOperatorKey(InputOperators.IS_NOT_ONE_OF)
                .setConditionValue(timestamp.toString());

        createActivityTriggerAndValidate(tagCondition);
    }

    @Ignore("'Is' operator is not supported any more for Tagged condition.")
    //@Test
    public void testWithConditionTagOperationTagEqualsActionSendAsset() throws Exception {
        TagCondition tagCondition = new TagCondition();
        tagCondition.setOperatorKey(InputOperators.IS_EXACTLY)
                .setConditionValue(tag);

        createActivityTriggerAndValidate(tagCondition);
    }

    @Test
    @Category({Hub2Regression.class})
    public void testTagTriggerWithLimitUpTo2TimesInTotalAndUpTo1TimePerHourAndActionAddPoints() throws Exception {

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("5").setUpToTimesByPeriod(true).setTimePerPeriodVal("2").setTimesByPeriod(RollingPeriod.Hour.toString());
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.Tagged.toString(), box, rule, Lists.newArrayList(limitsGlobalCondition) , Lists.newArrayList(ActivityActions.PointsAccumulation), "300");
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        log4j.info("Send the first tag to the member ");
        MembersService.performTagMemberSmartAction("tag1", "TagMember");
        log4j.info("verify that automation ran (first time) and member got points after tagging");
        checkDataStoreUpdatedWithPointsByRuleID("30000" , AmountType.Points);

        log4j.info("Send the second tag to the member ");
        MembersService.performTagMemberSmartAction("tag2", "TagMember");
        log4j.info("verify that automation ran again (second time)");
        checkDataStoreUpdatedWithPointsByRuleID(2);

        log4j.info("Send the third tag to the member ");
        MembersService.performTagMemberSmartAction("tag2", "TagMember");
        log4j.info("verify that automation didn't ran again  - Limit to 5 in total but only 2 per hour ");
        checkDataStoreUpdatedWithPointsByRuleID(2);
    }



    @Test
    public void testWithConditionTagOperationTagStringIsOneOfActionSendAsset() throws Exception {
        TagCondition tagCondition = new TagCondition();
        tagCondition.setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue(tag + " , " + timestamp);

        createActivityTriggerAndValidate(tagCondition);
    }


    @ru.yandex.qatools.allure.annotations.Description("Test trigger of tag member with global condition of specific member. The action is send asset")
    @Test
    public void testWithGlobalConditionSpecificMembersAndActionSendAsset() throws Exception {

        //Create member
        NewMember newMember = createNewMember();
        //Tag member
        performAnActionTagMember(newMember, "Gold");
        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(globalCondition), java.util.Arrays.asList(ActivityActions.SendBenefit),smartGift.getTitle());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        MembersService.tryFindAndGetMemberByPhoneNumber(newMember);
        sendTagAndCheckAutomation();
    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of tag member with global condition of between dates and not on the current day. The action is tag member. the test verify that the member didn't get the tag")
    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), Arrays.asList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        createNewMember();

        //perform tag member
        MembersService.performTagMemberSmartAction(tag, "TagMember");

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of tag member with global condition of limit up to 2 times in total per member. The action is tag member. the test verify that the member received the push only 2 times")
    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        createNewMember();

        MembersService.performTagMemberSmartAction(tag, "TagMember1");
        MembersService.performTagMemberSmartAction(tag, "TagMember2");
        MembersService.performTagMemberSmartAction(tag, "TagMember3");

        checkDataStoreUpdatedWithSendPushNotification(2);
    }



    private void createActivityTriggerAndValidate(TagCondition tagCondition) throws Exception{
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Arrays.asList(tagCondition), Arrays.asList(ActivityActions.SendBenefit), smartGift.getTitle());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member Female by default
        NewMember member = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        sendTagAndCheckAutomation();
    }

    private void sendTagAndCheckAutomation() throws Exception {
        //should activate the tag trigger
        MembersService.performTagMemberSmartAction(tag, "TagMember");

        Assert.assertTrue("The member did not get the tag sent, so the trigger did not started", MembersService.isTagExists(tag));
        log4j.info("The member got the tag , so the trigger started");
        checkAutomationRunAndSendAssetToMember();
    }


    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.Tagged;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.Tagged.toString() +"_";
    }

}
