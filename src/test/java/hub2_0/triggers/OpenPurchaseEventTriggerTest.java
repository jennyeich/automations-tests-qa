package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import common.BaseTest;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.conditions.operators.ShoppingCartOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import integration.base.BaseRuleHub2Test;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.IServerResponse;
import server.v2_8.SubmitPurchase;
import server.v2_8.common.Item;
import server.v4.SubmitPurchaseResponse;
import server.v4.common.enums.Status;
import server.v4.common.models.Customer;
import server.v4.common.models.Purchase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class OpenPurchaseEventTriggerTest extends BaseRuleHub2Test {

    private static Box box;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());

        }
    };

    /**
     * Test trigger make purchase (2.8) status "Open"  with condition POSID is one of 1, then tag member
     * Test also negative - send purchase with status 'final' - see automation not running
     */
    @Test
    @Category({Hub2Regression.class})
    public void testWithConditionPOSIDIsOneOfAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.POS_ID)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("1");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();

        //check negative - status final - not trigger the automation
        String transactionId = UUID.randomUUID().toString();
        log4j.info("Submit purchase with status <Final> for transaction: " + transactionId);
        performAnActionSubmitPurchase("Final",newMember, "20000", transactionId);
        //DataStore check
        checkDataStoreNegative(TAG_OPERATION,transactionId);

        //check positive - status open - not trigger the automation
        transactionId = UUID.randomUUID().toString();
        log4j.info("Submit purchase with status <Open> for transaction: " + transactionId);
        performAnActionSubmitPurchase("Open",newMember, "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase(2.8)  status "Open"  with condition shopping cart contains less than 2 of '2244' item, then send tag member
     * Test also negative - send purchase with status 'final' - see automation not running
     */
    @Test
    public void testWithConditionShoppingCartContainsLessThanAndTagMemberAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes("2244");
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_LESS_THAN)
                .setConditionValue("2")
                .setSelectItems(selectItems);

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        //check negative - status final - not trigger the automation
        String transactionId = UUID.randomUUID().toString();
        log4j.info("Submit purchase with status <Final> for transaction: " + transactionId);
        Item item = createItem("2244", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams("Final", Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, items);
        //DataStore check
        checkDataStoreNegative(TAG_OPERATION,transactionId);


        transactionId = UUID.randomUUID().toString();
        log4j.info("Submit purchase with status <Open> for transaction: " + transactionId);
        //perform submit purchase
        performAnActionSubmitPurchaseWithParams("Open", Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, items);

        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase(4.0)  status "Open" with condition value of purchase is less than 2000, then tag member
     */
    @Test
    public void testWithConditionValueOfPurchaseIsLessThanAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("200000");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();

        String transactionId = UUID.randomUUID().toString();
        log4j.info("Submit purchase with status <Final> for transaction: " + transactionId);
        performAnActionSubmitPurchaseV4(Status.FINAL,newMember, 10000, transactionId);
        checkDataStoreNegative(TAG_OPERATION,transactionId);


        transactionId = UUID.randomUUID().toString();
        log4j.info("Submit purchase with status <Open> for transaction: " + transactionId);
        performAnActionSubmitPurchaseV4(Status.OPEN,newMember, 10000, transactionId);
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    private void performAnActionSubmitPurchaseV4(Status status, NewMember newMember, int totalAmount, String transactionId) throws Exception{

        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        Purchase purchase = v4Builder.buildPurchase(transactionId,totalAmount, Lists.newArrayList()) ;
        purchase.setStatus(status);
        //run submit purchase and validate the response
        server.v4.SubmitPurchase submitPurchase = v4Builder.submitPurchaseRequest(Lists.newArrayList(customer),Lists.newArrayList() , Lists.newArrayList(), purchase);
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("status" , status.toString());
        submitPurchase.setQueryParameters(queryParams);
        IServerResponse response =  submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);
        if(!(((SubmitPurchaseResponse) response).getStatus().equals("ok")))
            Assert.fail(getError(response).get(MESSAGE).toString());
    }

    private JSONObject getError(IServerResponse response){
        return ((JSONObject)((JSONArray)(response.getRawObject().get(BaseTest.ERRORS))).get(0));
    }

    @After
    public void cleanData() throws Exception{
        deactivateAutomation();
    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.OpenPurchaseEvent;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.OpenPurchaseEvent.toString() +"_";
    }
}
