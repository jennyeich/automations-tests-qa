package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import common.BaseTest;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.operations.PointsCreditSettingsPage;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.conditions.CreditCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.*;
import server.utils.ServerHelper;
import server.utils.UserActionsConstants;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class UsesCreditTriggerTest extends BaseRuleHub2Test {

    private static Box box;


    @BeforeClass
    public static void createNewBox() throws Exception {
        BaseTest.sqlDB.setHub2Wallet(resolveLocationId(locationId), PointsCreditSettingsPage.BusinessWallet.Budget.dbValue());
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }


    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = tag + timestamp.toString();
            serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false));
            serverHelper.updateBusinessBackend(UpdateBusinessBackendBuilder.newPair(LocationBackendFields.PAY_WITH_BUDGET_TYPE.key(), PaymentType.BUDGET.type()));
        }
    };


    /**
     * Test trigger use credit with condition used credit is greater than 20 , Action Tag member.
     */
    @Test
    public void  testConditionUsedCreditIsGreaterThanAndActionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        //Add condition
        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.CREDIT_AMOUNT).
                setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("20");
        createRule(Lists.newArrayList(creditCondition), Lists.newArrayList(ActionsBuilder.buildTagMemberAction(tag)));

        //Create member
        NewMember member = createNewMember();
        String actionTag =  MembersService.performSmartActionAddCredit("1000",timestamp);

        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedCredit = "4000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedCredit);

        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + member.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger use credit with condition used credit is less than 60 , Action Tag member.
     */
    @Test
    public void  testConditionUsedCreditIsLessThanAndActionSendBenefit() throws Exception {

        smartGift = hubAssetsCreator.createSmartGift(timestamp);

        //Add condition
        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.CREDIT_AMOUNT).
                setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("60");
        createRule(Lists.newArrayList(creditCondition), Lists.newArrayList(ActionsBuilder.buildSendBenefitAction(smartGift.getTitle())));

        //Create member
        NewMember member = createNewMember();
        String actionTag =  MembersService.performSmartActionAddCredit("1000",timestamp);

        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedCredit = "4000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedCredit);

        checkDataStoreUpdatedWithAssetByRuleID();

        Assert.assertTrue("The member did not get the gift sent by the automation in the hub,but it appears in the dataStore.", MembersService.isGiftExists(smartGift.getTitle()));
        log4j.info("member received the gift");
        log4j.info(smartGift + " was added to the member ");
    }

    /**
     * Test trigger use credit with condition used credit is greater than or equal to 60, use 60 credit , Action Tag member.
     */
    @Test
    @Category({Hub2Regression.class})
    public void  testConditionUsedCreditIsGreaterThanOrEqualToAndActionSendBenefit() throws Exception {

        smartGift = hubAssetsCreator.createSmartGift(timestamp);

        //Add condition
        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.CREDIT_AMOUNT).
                setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("60");
        createRule(Lists.newArrayList(creditCondition), Lists.newArrayList(ActionsBuilder.buildSendBenefitAction(smartGift.getTitle())));

        //Create member
        NewMember member = createNewMember();
        String actionTag =  MembersService.performSmartActionAddCredit("1000",timestamp);

        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedCredit = "6000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedCredit);

        checkDataStoreUpdatedWithAssetByRuleID();

        Assert.assertTrue("The member did not get the gift sent by the automation in the hub,but it appears in the dataStore.", MembersService.isGiftExists(smartGift.getTitle()));
        log4j.info("member received the gift");
        log4j.info(smartGift + " was added to the member ");

    }

    /**
     * Test trigger use credit with condition used credit is less than or equal to 60, use 40 credit , Action Tag member.
     */
    @Test
    public void  testConditionUsedCreditIsLessThanOrEqualToAndActionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        //Add condition
        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.CREDIT_AMOUNT).
                setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("60");
        createRule(Lists.newArrayList(creditCondition), Lists.newArrayList(ActionsBuilder.buildTagMemberAction(tag)));

        //Create member
        NewMember member = createNewMember();
        String actionTag =  MembersService.performSmartActionAddCredit("1000",timestamp);

        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedCredit = "4000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedCredit);

        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + member.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger use credit with condition used credit is less than or equal to 80, use 80 credit , Action Tag member.
     */
    @Test
    public void  testConditionUsedCreditIsExactlyAndActionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        //Add condition
        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.CREDIT_AMOUNT).
                setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("80");
        createRule(Lists.newArrayList(creditCondition), Lists.newArrayList(ActionsBuilder.buildTagMemberAction(tag)));

        //Create member
        NewMember member = createNewMember();
        String actionTag =  MembersService.performSmartActionAddCredit("1000",timestamp);

        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedCredit = "8000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedCredit);

        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + member.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger use credit with condition total credit value is greater than 60, Action Tag member.
     */
    @Test
    public void testConditionTotalCreditValueIsGreaterThanAndActionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        //Add condition
        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.NEW_BALANCE).
                setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("60");
        createRule(Lists.newArrayList(creditCondition), Lists.newArrayList(ActionsBuilder.buildTagMemberAction(tag)));

        //Create member
        NewMember member = createNewMember();
        String actionTag =  MembersService.performSmartActionAddCredit("1000",timestamp);

        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedCredit = "8000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedCredit);

        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + member.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger use credit with condition total credit value is less than 500, Action Tag member.
     */
    @Test
    public void testConditionTotalCreditValueIsLessThanAndActionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        //Add condition
        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.NEW_BALANCE).
                setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("500");
        createRule(Lists.newArrayList(creditCondition), Lists.newArrayList(ActionsBuilder.buildTagMemberAction(tag)));

        //Create member
        NewMember member = createNewMember();
        String actionTag =  MembersService.performSmartActionAddCredit("500",timestamp);

        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedCredit = "8000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedCredit);

        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + member.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * The member will have more than 500 credit
     * Test trigger use credit with condition total credit value is less than 500, Action Tag member.
     */
    @Test
    public void testConditionTotalCreditValueIsLessThanAndActionTagMemberNegative() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        //Add condition
        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.NEW_BALANCE).
                setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("500");
        createRule(Lists.newArrayList(creditCondition), Lists.newArrayList(ActionsBuilder.buildTagMemberAction(tag)));

        //Create member
        NewMember member = createNewMember();
        String actionTag =  MembersService.performSmartActionAddCredit("1000",timestamp);

        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedCredit = "8000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedCredit);

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

        log4j.info("member didn't received the tag");

    }

    /**
     * Test trigger use credit with condition total credit value is greater than or equal to 500, Action send benefit
     */
    @Test
    public void testConditionTotalCreditValueIsGreaterThanOrEqualToAndActionSendBenefit() throws Exception {

        smartGift = hubAssetsCreator.createSmartGift(timestamp);

        //Add condition
        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.NEW_BALANCE).
                setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("500");
        createRule(Lists.newArrayList(creditCondition), Lists.newArrayList(ActionsBuilder.buildSendBenefitAction(smartGift.getTitle())));

        //Create member
        NewMember member = createNewMember();
        String actionTag =  MembersService.performSmartActionAddCredit("800",timestamp);

        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedCredit = "8000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedCredit);

        checkDataStoreUpdatedWithAssetByRuleID();

        Assert.assertTrue("The member did not get the gift sent by the automation in the hub,but it appears in the dataStore.", MembersService.isGiftExists(smartGift.getTitle()));
        log4j.info("member received the gift");
        log4j.info(smartGift + " was added to the member ");

    }

    /**
     * Test trigger use credit with condition total credit value is less than or equal to 500, Action send benefit.
     */
    @Test
    public void testConditionTotalCreditValueIsLessThanOrEqualToAndActionSendBenefit() throws Exception {

        smartGift = hubAssetsCreator.createSmartGift(timestamp);

        //Add condition
        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.NEW_BALANCE).
                setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("500");
        createRule(Lists.newArrayList(creditCondition), Lists.newArrayList(ActionsBuilder.buildSendBenefitAction(smartGift.getTitle())));

        //Create member
        NewMember member = createNewMember();
        String actionTag =  MembersService.performSmartActionAddCredit("550",timestamp);

        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedCredit = "5000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedCredit);

        checkDataStoreUpdatedWithAssetByRuleID();

        Assert.assertTrue("The member did not get the gift sent by the automation in the hub,but it appears in the dataStore.", MembersService.isGiftExists(smartGift.getTitle()));
        log4j.info("member received the gift");
        log4j.info(smartGift + " was added to the member ");

    }

    /**
     * Test trigger use credit with condition total credit value is exactly 1000, Action Tag member.
     */
    @Test
    public void testConditionTotalCreditValueIsExactlyAndActionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        //Add condition
        CreditCondition creditCondition = new CreditCondition();
        creditCondition.setConditionKey(CreditCondition.ConditionKey.NEW_BALANCE).
                setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("1000");
        createRule(Lists.newArrayList(creditCondition), Lists.newArrayList(ActionsBuilder.buildTagMemberAction(tag)));

        //Create member
        NewMember member = createNewMember();
        String actionTag =  MembersService.performSmartActionAddCredit("1080",timestamp);

        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String usedCredit = "8000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(member, usedCredit);

        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + member.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of uses credit with global condition of specific member. The action is tag member.")
    @Test
    public void testWithGlobalConditionSpecificMembersAndActionTagMember() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(globalCondition), Arrays.asList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        NewMember newMember = createNewMember();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //Tag member
        performAnActionTagMember(newMember, "Gold");

        String givenCredit = "500";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);
        TimeUnit.SECONDS.sleep(7);

        String usedCredit = "8000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(newMember, usedCredit);

        checkDataStoreUpdatedWithTagByRuleID(tag);
    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of uses credit with global condition of between dates and not on the current day. The action is tag member. the test verify that the member didn't get the tag")
    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), Arrays.asList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenCredit = "500";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);
        TimeUnit.SECONDS.sleep(10);

        String usedCredit = "8000";
        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(newMember, usedCredit);
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of uses credit with global condition of limit up to 2 times in total per member. The action is tag member. the test verify that the member received the push only 2 times")
    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenCredit = "500";
        //add credit to member
        MembersService.performSmartActionAddCredit(givenCredit,timestamp);
        TimeUnit.SECONDS.sleep(10);

        String usedCredit1 = "6000";
        String usedCredit2 = "7000";
        String usedCredit3 = "8000";

        // perform pay with budget to use members points
        ServerHelper.performAnActionUsePoints(newMember, usedCredit1);
        ServerHelper.performAnActionUsePoints(newMember, usedCredit2);
        ServerHelper.performAnActionUsePoints(newMember, usedCredit3);

        checkDataStoreUpdatedWithSendPushNotification(2);

    }







    private void createRule(List<ApplyCondition> creditConditions, List<ActivityAction> activityActions) throws Exception {
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, creditConditions, activityActions);
        log4j.info("rule name:" +rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
    }



    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.UsesCredit;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.UsesCredit.toString() +"_";
    }


}
