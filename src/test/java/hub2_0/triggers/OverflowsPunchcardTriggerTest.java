package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartPunchCard;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.conditions.OverflowsPunchCardCondition;
import hub.hub2_0.common.conditions.PunchAPunchCardCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.conditions.operators.ListOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.utils.ServerHelper;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import static hub2_0.utils.DateTimeUtility.resolveDay;

/**
 * Created by Goni on 2/5/2018.
 */

public class OverflowsPunchcardTriggerTest  extends BaseRuleHub2Test {

    private static Box box;
    private final static String NUM_OF_PUNCHES_EXPR = "@NumOfPunches";


    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = tag + timestamp.toString();
            //create punch card
            createSmartPunchCard("5");
        }
    };

    @After
    public void cleanData() throws Exception{
        deactivateAutomation();
    }

    @Test
    @Category({Hub2Regression.class})
    public void testPunchLastPunchYES_OverflowConditionBenefitEquals() throws Exception {

        //Create rule for punch/LastPunch=YES
        createPunchRuleLastPunchYes();


        //Add condition
        OverflowsPunchCardCondition overflowsPunchCardCondition = new OverflowsPunchCardCondition();
        overflowsPunchCardCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setBenefitName(smartPunchCard.getTitle());
        createRulePunchOverflow(smartPunchCard,overflowsPunchCardCondition);


        //Create member
        createMemberAndSendPunch();

        //Perform punchCard
        //punch 5 in the first punch and then 2 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,7);

        checkDataStoreUpdatedWithExceededPunch("2");

    }

    @Test
    @Category({Hub2Regression.class})
    public void testPunchLastPunchYES_OverflowConditionBenefitEquals_Overflow2cards() throws Exception {


        //Create rule for punch/LastPunch=YES
        createPunchRuleLastPunchYes();

        //Create rule for overflowPunch
        //Add condition
        OverflowsPunchCardCondition overflowsPunchCardCondition = new OverflowsPunchCardCondition();
        overflowsPunchCardCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setBenefitName(smartPunchCard.getTitle());
        createRulePunchOverflow(smartPunchCard,overflowsPunchCardCondition);


        //Create member
        createMemberAndSendPunch();

        //Perform punchCard
        //punch 5 in the first punch and then 5 in the new punch card that will be send from automation
        //and then 2 in the new punch card that will be send from automation - total 7 overflow
        performAnActionPunchAPunchCard(smartPunchCard,12);

        checkDataStoreUpdatedWithExceededPunch("7");

    }


    @Test
    public void testPunchLastPunchYES_OverflowConditionBenefitNotEquals() throws Exception {

        String tempPunchId = UUID.randomUUID().toString();
        SmartPunchCard punchCard1 = hubAssetsCreator.createSmartPunchCard(tempPunchId, "5");

        //Create srule for punch/LastPunch=Yes
        createPunchRuleLastPunchYes();

        //Create rule for overflowPunch
        //Add condition
        OverflowsPunchCardCondition overflowsPunchCardCondition = new OverflowsPunchCardCondition();
        overflowsPunchCardCondition.setOperatorKey(ListOperators.IS_NOT)
                .setBenefitName(punchCard1.getTitle());
        createRulePunchOverflow(smartPunchCard,overflowsPunchCardCondition);

        createMemberAndSendPunch();


        //Perform punchCard
        //punch 5 in the first punch and then 2 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,7);

        checkDataStoreUpdatedWithExceededPunch("2");

    }

    private void createMemberAndSendPunch() throws Exception {
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);
    }


    //Test the case that user has 2 same punch cards with 5 punches and we autopunch from the hub 7 punches
    //so the overflow is done from the hub directly to the second punchcard.
    // We should check that the automation of overflow will not run
    @Test
    @Category({Hub2Regression.class})
    public void testPunchLastYES_OverflowConditionBenefitEqual_Punch2BenefitsFromHUB_Negative() throws Exception {

        //Create rule for overflowPunch
        //Add condition
        OverflowsPunchCardCondition overflowsPunchCardCondition = new OverflowsPunchCardCondition();
        overflowsPunchCardCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setBenefitName(smartPunchCard.getTitle());
        createRulePunchOverflow(smartPunchCard,overflowsPunchCardCondition);


        //Create member
        createMemberAndSendPunch();
        Thread.sleep(2000);
        //Send another punch to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard
        //punch 5 in the first punch and then 2 in the new punch card
        performAnActionPunchAPunchCard(smartPunchCard,7);

        checkDataStoreNotUpdatedWithExceededPunch();
    }



    @Test
    public void testPunchTotalPunchesEqualsToMaxPunches_OverflowConditionBenefitEquals() throws Exception {

        //Create smart automation for punch/Total punches after operation = 5 - then send another punch card
        createPunchAutomationTotalPunches(IntOperators.IS_EXACTLY,"5");

        //Create rule for overflowPunch
        //Add condition
        OverflowsPunchCardCondition overflowsPunchCardCondition = new OverflowsPunchCardCondition();
        overflowsPunchCardCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setBenefitName(smartPunchCard.getTitle());
        createRulePunchOverflow(smartPunchCard,overflowsPunchCardCondition);

        //Create member
        createMemberAndSendPunch();

        //Perform punchCard
        //punch 5 in the first punch and then 3 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,7);

        checkDataStoreUpdatedWithExceededPunch("2");
    }


    @Test
    public void testPunchTotalPunchesEqualsBeforeLastPunch_OverflowConditionBenefitEquals() throws Exception {

        //Create smart automation for punch/Total punches after operation = 5 - then send another punch card
        createPunchAutomationTotalPunches(IntOperators.IS_EXACTLY,"4");
        //Create rule for overflowPunch
        //Add condition
        OverflowsPunchCardCondition overflowsPunchCardCondition = new OverflowsPunchCardCondition();
        overflowsPunchCardCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setBenefitName(smartPunchCard.getTitle());
        createRulePunchOverflow(smartPunchCard,overflowsPunchCardCondition);


        //Create member
        createMemberAndSendPunch();

        //Perform punchCard
        //punch 5 in the first punch and then 3 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,6);

        checkDataStoreUpdatedWithExceededPunch("1");
    }

    @Test
    @Category({Hub2Regression.class})
    public void testPunchTotalPunchesGreaterThan5_OverflowConditionBenefitEquals() throws Exception {


        //Create smart automation for punch/Total punches after operation > 5 - then send another punch card
        createPunchAutomationTotalPunches(IntOperators.IS_GREATER_THAN,"5");

        //Create rule for overflowPunch
        //Add condition
        OverflowsPunchCardCondition overflowsPunchCardCondition = new OverflowsPunchCardCondition();
        overflowsPunchCardCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setBenefitName(smartPunchCard.getTitle());
        createRulePunchOverflow(smartPunchCard,overflowsPunchCardCondition);
        //Create member
        createMemberAndSendPunch();

        //Perform punchCard
        //punch 5 in the first punch and then 3 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,6);

        checkDataStoreUpdatedWithExceededPunch("1");
    }

    @Test
    public void testPunchLastPunchYES_OverflowConditionBenefitEquals_OverflowContinues() throws Exception {

        //Create smart automation for punch/LastPunch=YES
        createPunchRuleLastPunchYes();

        //Create rule for overflowPunch
        //Add condition
        OverflowsPunchCardCondition overflowsPunchCardCondition = new OverflowsPunchCardCondition();
        overflowsPunchCardCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setBenefitName(smartPunchCard.getTitle());
        createRulePunchOverflow(smartPunchCard,overflowsPunchCardCondition);
        //Create member
        createMemberAndSendPunch();

        //Perform punchCard
        //punch 5 in the first punch and then 3 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,8);

        checkDataStoreUpdatedWithExceededPunch("3");//exceeded = 3

        performAnActionPunchAPunchCard(smartPunchCard,8);//exceeded = 6

        Query.FilterPredicate predicate = new Query.FilterPredicate("LocationID", Query.FilterOperator.EQUAL, locationId);
        List<Entity> res = ServerHelper.queryWithWait("Must find 2 UserAction with the action: " + EXCEEDED_PUNCH,
                buildQuery(EXCEEDED_PUNCH,predicate) , 2, dataStore);
        for (int i = 0; i < res.size(); i++) {
            Text t = ((Text)res.get(i).getProperty("Data"));
            Assert.assertTrue("Data must contain the NumOfPunches: 3 or 6 accourdinally",(t.getValue().contains("NumOfPunches=\"3\"") || t.getValue().contains("NumOfPunches=\"6\"") ));
        }

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of overflows punch card with global condition of specific member. The action is tag member.")
    @Test
    public void testWithGlobalConditionSpecificMembersAndActionTagMember() throws Exception {

        //Create rule for punch/LastPunch=YES
        createPunchRuleLastPunchYes();

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(globalCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        NewMember newMember = createNewMember();
        performAnActionTagMember(newMember, "Gold");
        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);
        //Perform punchCard
        //punch 5 in the first punch and then 2 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,7);

        checkDataStoreUpdatedWithExceededPunch("2");

        checkAutomationRunAndTagMember();

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of overflows punch card with global condition of between dates and not on the current day. The action is tag member. the test verify that the member didn't get the tag")
    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        //Create rule for punch/LastPunch=YES
        createPunchRuleLastPunchYes();

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        createNewMember();
        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);
        //Perform punchCard
        //punch 5 in the first punch and then 2 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,7);
        checkDataStoreUpdatedWithExceededPunch("2");

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of overflows punch card with global condition of limit up to 2 times in total per member. The action is tag member. the test verify that the member received the push only 2 times")
    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        //Create rule for punch/LastPunch=YES
        createPunchRuleLastPunchYes();

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        createNewMember();

        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);
        //Perform punchCard
        //punch 5 in the first punch and then 2 in the new punch card that will be send from automation
        performAnActionPunchAPunchCard(smartPunchCard,7);
        performAnActionPunchAPunchCard(smartPunchCard,7);
        performAnActionPunchAPunchCard(smartPunchCard,7);

        checkDataStoreUpdatedWithSendPushNotification(2);
    }

    private void createRulePunchOverflow(SmartPunchCard punchCard,OverflowsPunchCardCondition condition) throws Exception {
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, ActivityTrigger.OverflowsPunchCard.toString());

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.OverflowsPunchCard.toString(),box,rule, Lists.newArrayList(condition), Lists.newArrayList(ActivityActions.PunchExceeded),punchCard.getTitle());
        log4j.info("Created rule name:" +rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
    }

    private void createPunchRuleLastPunchYes() throws Exception {
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityStatus.Active, ActivityTrigger.PunchesAPunchCard.toString());

        //Add condition
        PunchAPunchCardCondition punchAPunchCardCondition = new PunchAPunchCardCondition();
        punchAPunchCardCondition.setConditionKey(PunchAPunchCardCondition.ConditionKey.IS_LAST_PUNCH).clickYes();
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.PunchesAPunchCard.toString(),box,rule,Lists.newArrayList(punchAPunchCardCondition), Lists.newArrayList(ActivityActions.SendBenefit),smartPunchCard.getTitle());

        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
    }

    private void createPunchAutomationTotalPunches(String operator,String numOfPunches) throws Exception {

        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule,ActivityStatus.Active, ActivityTrigger.PunchesAPunchCard.toString());

        //Add condition
        PunchAPunchCardCondition punchAPunchCardCondition = new PunchAPunchCardCondition();
        punchAPunchCardCondition.setConditionKey(PunchAPunchCardCondition.ConditionKey.NUMBER_OF_PUNCHES)
                .setOperatorKey(operator)
                .setConditionValue(numOfPunches);
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.PunchesAPunchCard.toString(),box,rule,Lists.newArrayList(punchAPunchCardCondition), Lists.newArrayList(ActivityActions.SendBenefit),smartPunchCard.getTitle());

        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());

    }


    @Override
    protected void performAnActionPunchAPunchCard(SmartPunchCard punchCard,int numOfPunches) throws Exception {

        MembersService.performSmartActionPunchAPunchCard(punchCard.getTitle(),numOfPunches);
        Thread.sleep(5000);//Wait after the punch card sent so the data store will be updated with exceededPunch action

    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.OverflowsPunchCard;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.OverflowsPunchCard.toString() +"_";
    }
}



