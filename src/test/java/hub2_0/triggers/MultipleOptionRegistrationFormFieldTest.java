package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.common.RegistrationField;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.services.OperationService;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.MemberRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.services.member.MembersService;
import hub.utils.HubMemberCreator;
import hub2_0.actions.BaseActivityActionTest;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Calendar;
import java.util.UUID;


public class MultipleOptionRegistrationFormFieldTest extends BaseActivityActionTest {

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = tag + timestamp.toString();

        }
    };

    @Test
    @Category(Hub2Regression.class)
    public void testMakesAPurchaseWithMembershipConditionOfCustomTextFieldWithDropDownListAndTagMember() throws Exception {

        OperationService.createRegistrationFormWithCustomField("Option 1", "Option 1", "Option 2", "Option 2", RegistrationField.CustomText2);
        log4j.info("new field was added" + RegistrationField.CustomText2);

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberRegistrationFormFields.GenericString2.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionSelectValue("Option 1", "Option 2");

        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        //Create new rule
        createRule(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList(globalCondition));

        performAnActionSubmitPurchase(newMember, "20000", transactionId);

        MembersService.tryFindAndGetMemberByPhoneNumber(newMember);
        checkAutomationRunAndTagMember();

    }

    @Test
    public void testMakesAPurchaseWithMembershipConditionOfCustomIntegerFieldWithDropDownListAndTagMember() throws Exception {

        OperationService.createRegistrationFormWithCustomField("3", "3", "2", "2", RegistrationField.GenericInteger2);
        log4j.info("new field was added" + RegistrationField.GenericInteger2);

        //Create member
        NewMember newMember = HubMemberCreator.buildMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        newMember.setGenericInteger2("2");
        MembersService.createNewMember(newMember);
        String transactionId = UUID.randomUUID().toString();

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberRegistrationFormFields.GenericInteger2.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionSelectValue("2");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        //Create new rule
        createRule(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList(globalCondition));

        performAnActionSubmitPurchase(newMember, "20000", transactionId);

        MembersService.tryFindAndGetMemberByPhoneNumber(newMember);
        checkAutomationRunAndTagMember();

    }

    @Test
    @Category(Hub2Regression.class)
    public void testMakesAPurchaseWithBirthdayMonthIsOneOfMembershipConditionAndTagMember() throws Exception {

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Calendar calendar = Calendar.getInstance();
        int currentMonth = calendar.get(Calendar.MONTH);
        currentMonth++;
        String newCurrentMonth = toString().valueOf(currentMonth);

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.BirthdayMonth.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionSelectValue(newCurrentMonth);
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        //Create new rule
        createRule(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList(globalCondition));

        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        MembersService.tryFindAndGetMemberByPhoneNumber(newMember);

        checkAutomationRunAndTagMember();
    }

    @Test
    public void testMakesAPurchaseWithBirthdayMonthIsOneOfMultipleMembershipConditionAndTagMember() throws Exception {

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Calendar calendar = Calendar.getInstance();
        int currentMonth = calendar.get(Calendar.MONTH);
        currentMonth++;
        String newCurrentMonth = toString().valueOf(currentMonth);

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.BirthdayMonth.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionSelectValue(newCurrentMonth, "2");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        //Create new rule
        createRule(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList(globalCondition));

        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        MembersService.tryFindAndGetMemberByPhoneNumber(newMember);

        checkAutomationRunAndTagMember();
    }

    @Test
    public void testMakesAPurchaseWithBirthdayMonthIsNotOneOfMembershipConditionAndTagMember() throws Exception {

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Calendar calendar = Calendar.getInstance();
        int currentMonth = calendar.get(Calendar.MONTH);
        String newCurrentMonth = toString().valueOf(currentMonth);

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.BirthdayMonth.toString())
                .setOperatorKey(InputOperators.IS_NOT_ONE_OF)
                .setConditionSelectValue(newCurrentMonth);
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        //Create new rule
        createRule(ActivityTrigger.MadeAPurchase.toString(), Lists.newArrayList(globalCondition));

        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        MembersService.tryFindAndGetMemberByPhoneNumber(newMember);

        checkAutomationRunAndTagMember();
    }


    @Test
    public void testJoinTheClubWithMembershipConditionOfGenderIsOneOfAndTagMember() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberRegistrationFormFields.Gender.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionSelectValue("female");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        //Create new rule
        createRule(ActivityTrigger.JoinTheClub.toString(), Lists.newArrayList(globalCondition));

        //Create member
        createNewMember();

        checkAutomationRunAndTagMember();
    }


    @Override
    protected ActivityAction getConcreteAction() throws Exception {
        timestamp = String.valueOf(System.currentTimeMillis());
        ActivityAction activityAction = ActionsBuilder.buildTagMemberAction(tag);
        return activityAction;
    }

    @Override
    public ITrigger getTrigger() {
        return null;
    }

    @Override
    public String getAutomationName() {
        return null;
    }

    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }
}
