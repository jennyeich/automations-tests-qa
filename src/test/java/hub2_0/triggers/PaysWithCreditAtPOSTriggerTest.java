package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import common.BaseTest;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.operations.PointsCreditSettingsPage;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.conditions.PaysWithCreditAtPOSCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.*;
import server.utils.UserActionsConstants;

import java.util.Calendar;
import java.util.UUID;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class PaysWithCreditAtPOSTriggerTest extends BaseRuleHub2Test {

    private static Box box;

    @BeforeClass
    public static void createNewBox() throws Exception {
        BaseTest.sqlDB.setHub2Wallet(resolveLocationId(locationId), PointsCreditSettingsPage.BusinessWallet.Budget.dbValue());
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }


    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            serverHelper.saveApiClient(ApiClientBuilder.newPair(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false));
            serverHelper.updateBusinessBackend(UpdateBusinessBackendBuilder.newPair(LocationBackendFields.PAY_WITH_BUDGET_TYPE.key(), PaymentType.BUDGET.type()));
        }
    };


    @ru.yandex.qatools.allure.annotations.Description("Rule with pays with credit at POS trigger with condition of greater than or equal to and tag action")
    @Test
    @Category({Hub2Regression.class})
    public void testConditionPaymentAmountIsGreaterThanOrEqualToAndTagMember() throws Exception {

        PaysWithCreditAtPOSCondition paysWithCreditAtPOSCondition = new PaysWithCreditAtPOSCondition();
        paysWithCreditAtPOSCondition.setConditionKey(PaysWithCreditAtPOSCondition.ConditionKey.PAYMENT_AMOUNT)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("200");
        createAutomationAndVerify(paysWithCreditAtPOSCondition, "30000");

    }


    @ru.yandex.qatools.allure.annotations.Description("Rule with pays with credit at POS trigger with condition of greater than and tag action")
    @Test
    public void testConditionPaymentAmountIsGreaterThanAndTagMember() throws Exception {

        PaysWithCreditAtPOSCondition paysWithCreditAtPOSCondition = new PaysWithCreditAtPOSCondition();
        paysWithCreditAtPOSCondition.setConditionKey(PaysWithCreditAtPOSCondition.ConditionKey.PAYMENT_AMOUNT)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("200");
        createAutomationAndVerify(paysWithCreditAtPOSCondition, "30000");

    }


    @ru.yandex.qatools.allure.annotations.Description("Rule with pays with credit at POS trigger with condition of less than or equal to and tag action")
    @Test
    public void testConditionPaymentAmountIsLessThanOrEqualToAndTagMember() throws Exception {

        PaysWithCreditAtPOSCondition paysWithCreditAtPOSCondition = new PaysWithCreditAtPOSCondition();
        paysWithCreditAtPOSCondition.setConditionKey(PaysWithCreditAtPOSCondition.ConditionKey.PAYMENT_AMOUNT)
                .setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("200");
        createAutomationAndVerify(paysWithCreditAtPOSCondition, "10000");

    }

    @Test
    public void testConditionPaymentAmountIsLessThanAndTagMember() throws Exception {

        PaysWithCreditAtPOSCondition paysWithCreditAtPOSCondition = new PaysWithCreditAtPOSCondition();
        paysWithCreditAtPOSCondition.setConditionKey(PaysWithCreditAtPOSCondition.ConditionKey.PAYMENT_AMOUNT)
                .setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("200");
        createAutomationAndVerify(paysWithCreditAtPOSCondition, "5000");

    }

    @Test
    public void testConditionPaymentAmountIsAndSendBenefit() throws Exception {

        PaysWithCreditAtPOSCondition paysWithCreditAtPOSCondition = new PaysWithCreditAtPOSCondition();
        paysWithCreditAtPOSCondition.setConditionKey(PaysWithCreditAtPOSCondition.ConditionKey.PAYMENT_AMOUNT)
                .setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("200");
        createAutomationWithSendGiftActionAndVerify(paysWithCreditAtPOSCondition, "20000");

    }

    @Test
    public void testWithGlobalConditionSpecificMembersAndActionTagMember() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.BudgetWeighted.toString())
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("100");

        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        createAutomationAndVerify(globalCondition, "2000");

    }

    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        String actionTag = MembersService.performSmartActionAddCredit("70000",timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(newMember.getPhoneNumber(), "2000", transactionId);

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        String actionTag = MembersService.performSmartActionAddCredit("70000",timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(newMember.getPhoneNumber(), "1000", transactionId);
        performPayWithBudget(newMember.getPhoneNumber(), "2000", transactionId);
        performPayWithBudget(newMember.getPhoneNumber(), "3000", transactionId);

        checkDataStoreUpdatedWithSendPushNotification(2);

    }




    private void createAutomationAndVerify(ApplyCondition condition, String spentCredit) throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule, Lists.newArrayList(condition), Lists.newArrayList(ActivityActions.Tag),tag);
        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        String actionTag = MembersService.performSmartActionAddCredit("70000",timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(newMember.getPhoneNumber(), spentCredit, transactionId);
        checkAutomationRunAndTagMember();
    }

    private void createAutomationWithSendGiftActionAndVerify(ApplyCondition condition, String spentCredit) throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        createSmartGift();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule, Lists.newArrayList(condition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());
        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        String actionTag = MembersService.performSmartActionAddCredit("70000",timestamp);
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        String transactionId = UUID.randomUUID().toString();
        log4j.info("perform pay with budget, transactionId : " + transactionId);

        performPayWithBudget(newMember.getPhoneNumber(), spentCredit, transactionId);

        checkDataStoreUpdatedWithAssetByRuleID();
        log4j.info("member received the asset");
        log4j.info(smartGift.getTitle() + " was send to the member ");
    }



    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.PaysWithCreditAtPOS;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.PaysWithPointsAtPOS.toString() +"_";
    }
}
