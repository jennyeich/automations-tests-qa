package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartPunchCard;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.PunchAPunchCardCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.conditions.operators.ListOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.utils.ServerHelper;

import java.util.Arrays;
import java.util.Calendar;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class PunchAPunchCardTriggerTest extends BaseRuleHub2Test {


    private static Box box;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
        timestamp = String.valueOf(System.currentTimeMillis());
        tag = tag + timestamp.toString();
        if(smartPunchCard == null) {
            createSmartPunchCard("5");
        }
        }
    };


    /**
     * Test trigger punch a punch card with condition Asset is Exactly the smart punch, then tag member
     */
  @Test
  @Category({Hub2Regression.class})
  public void  testConditionAssetIsExactlyTagAction() throws Exception{

      tag = tag + timestamp.toString();

      PunchAPunchCardCondition punchAPunchCardCondition1 = new PunchAPunchCardCondition();
      punchAPunchCardCondition1.setConditionKey(PunchAPunchCardCondition.ConditionKey.BENEFIT).
              setOperatorKey(ListOperators.IS_EXACTLY).setBenefitName(smartPunchCard.getTitle());

      Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
      Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(punchAPunchCardCondition1), Lists.newArrayList(ActivityActions.Tag),tag);


      LoyaltyService.createActivityInBox(activity);
      automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

      createMemberAndSendPunchCard();
      //Perform punchCard
      performAnActionPunchAPunchCard(smartPunchCard);
      //Verify the automation run and the tag exists
      checkAutomationRunAndTagMember();
  }

    /**
     * Test trigger punch a punch card with condition Benfit is Exactly the smart punch, then execute action of push Notification
     */
    @Test
    public void  testConditionBenfitIsExactlyAndConditionLastPunchYesActionSendAssetAndPushNotification() throws Exception{


        ActivityAction sendBenefitAction = ActionsBuilder.buildSendBenefitAction(smartPunchCard.getTitle());
        ActivityAction sendPushNotification = ActionsBuilder.buildSendPushAction("You got new gift");

        PunchAPunchCardCondition punchAPunchCardCondition1 = new PunchAPunchCardCondition();
        punchAPunchCardCondition1.setConditionKey(PunchAPunchCardCondition.ConditionKey.IS_LAST_PUNCH).clickYes();

        PunchAPunchCardCondition punchAPunchCardCondition2 = new PunchAPunchCardCondition();
        punchAPunchCardCondition2.setConditionKey(PunchAPunchCardCondition.ConditionKey.BENEFIT).
                setOperatorKey(ListOperators.IS_EXACTLY).setBenefitName(smartPunchCard.getTitle());


        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Arrays.asList(punchAPunchCardCondition1,punchAPunchCardCondition2) , Arrays.asList(sendBenefitAction, sendPushNotification));

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        createNewMember();
        //Send punch to member
        performAnActionSendPunchCard(smartPunchCard);
        //Perform punchCard
        performPunchAPunchCardNoValidation(smartPunchCard,5);
        //verify asset was sent to member and also push action performed
        checkDataStoreWithOperation(RECEIVEDASSET);
        checkDataStoreUpdatedWithSendPushNotification(1);
    }


    /**
     * Test trigger punch a punch card with condition Benfit is Not the smart punch, then verify that no tag appear.
     */
    @Test
    public void  testConditionBenfitIsNotConditionTagActioNegativeTest() throws Exception{

        tag = tag + timestamp.toString();

        PunchAPunchCardCondition punchAPunchCardCondition1 = new PunchAPunchCardCondition();
        punchAPunchCardCondition1.setConditionKey(PunchAPunchCardCondition.ConditionKey.BENEFIT)
                .setOperatorKey(ListOperators.IS_NOT)
                .setBenefitName(smartPunchCard.getTitle());


        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(punchAPunchCardCondition1), Lists.newArrayList(ActivityActions.Tag),tag);


        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        createMemberAndSendPunchCard();
        //Perform punchCard
        performAnActionPunchAPunchCard(smartPunchCard);
        //Get member userKey and turn into User_KEY
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }

    /**
     * Test trigger punch a punch card with condition of last punch of the smart punch, Action of Tag member.
     */
    @Test
    public void testConditionLastPunchActionTagMember() throws Exception{

        tag = tag + timestamp.toString();

        PunchAPunchCardCondition punchAPunchCardCondition1 = new PunchAPunchCardCondition();
        punchAPunchCardCondition1.setConditionKey(PunchAPunchCardCondition.ConditionKey.IS_LAST_PUNCH).clickYes();

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(punchAPunchCardCondition1), Lists.newArrayList(ActivityActions.Tag),tag);

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        createMemberAndSendPunchCard();
        //Perform punchCard
        performAnActionPunchLastPunchCard(smartPunchCard);
        //Verify the automation run and the tag exists
        checkAutomationRunAndTagMember();
    }


    /**
     * Test trigger punch a punch card with Total Num of punches  greater than condition and Send Benefit action.
     */
    @Test
    public void testConditionTotalNumOfPunchesIsGreaterThanAndSendBenefitAction() throws Exception{

        PunchAPunchCardCondition punchAPunchCardCondition = new PunchAPunchCardCondition();
        punchAPunchCardCondition.setConditionKey(PunchAPunchCardCondition.ConditionKey.NUMBER_OF_PUNCHES)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("1");

        createSmartGift();
        CreateRuleWithNumOfPunchesCondition(punchAPunchCardCondition, ActionsBuilder.buildSendBenefitAction(smartGift.getTitle()));
        createMemberAndSendPunchCard();

        //Perform punchCard
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        // Check negative test
        checkDataStoreNegativeCheckByRuleID(RECEIVEDASSET);
        //Verify asset was sent to member on the 3rd punch.
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());
    }

    @Test
    public void testConditionTotalNumOfPunchesIsLessThanAndSendBenefitAction() throws Exception{

        tag = tag + timestamp.toString();
        PunchAPunchCardCondition punchAPunchCardCondition = new PunchAPunchCardCondition();
        punchAPunchCardCondition.setConditionKey(PunchAPunchCardCondition.ConditionKey.NUMBER_OF_PUNCHES).
                setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("2");

        createSmartGift();
        CreateRuleWithNumOfPunchesCondition(punchAPunchCardCondition, ActionsBuilder.buildSendBenefitAction(smartGift.getTitle()));
        createMemberAndSendPunchCard();

        //Perform punchCard
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        // check action executed- num_of_punches < 2
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle(), 1);
        //negative test- 2 punches.
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle(), 1);
    }

    @Test
    public void testConditionTotalNumOfPunchesIsGreaterThanOrEqualToAndSendBenefitAction() throws Exception{

        PunchAPunchCardCondition punchAPunchCardCondition = new PunchAPunchCardCondition();
        punchAPunchCardCondition.setConditionKey(PunchAPunchCardCondition.ConditionKey.NUMBER_OF_PUNCHES)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("2");

        createSmartGift();
        CreateRuleWithNumOfPunchesCondition(punchAPunchCardCondition, ActionsBuilder.buildSendBenefitAction(smartGift.getTitle()));
        createMemberAndSendPunchCard();

        //Perform punchCard
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        checkDataStoreNegativeCheckByRuleID(RECEIVEDASSET);
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        // Check action executed when num_of_punches = 2
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle(), 1);
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        // Check action executed when num_of_punches > 2
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle(), 2);
    }

    @Test
    public void testConditionTotalNumOfPunchesIsLessThanOrEqualToAndSendBenefitAction() throws Exception{

        PunchAPunchCardCondition punchAPunchCardCondition = new PunchAPunchCardCondition();
        punchAPunchCardCondition.setConditionKey(PunchAPunchCardCondition.ConditionKey.NUMBER_OF_PUNCHES).
                setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("2");

        createSmartGift();
        CreateRuleWithNumOfPunchesCondition(punchAPunchCardCondition, ActionsBuilder.buildSendBenefitAction(smartGift.getTitle()));

        createMemberAndSendPunchCard();
        //Perform punchCard - check less then 2 punches
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle(), 1);
        //Perform punchCard - check equals then 2 punches
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle(), 2);
        //Perform punchCard - negative test- check more than 2 punches
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle(), 2);


    }

    @Test
    @Category({Hub2Regression.class})
    public void testConditionTotalNumOfPunchesIsExactlyAndSendBenefitAction() throws Exception{

        tag = tag + timestamp.toString();
        PunchAPunchCardCondition punchAPunchCardCondition = new PunchAPunchCardCondition();
        punchAPunchCardCondition.setConditionKey(PunchAPunchCardCondition.ConditionKey.NUMBER_OF_PUNCHES).
                setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("2");

        createSmartGift();
        CreateRuleWithNumOfPunchesCondition(punchAPunchCardCondition, ActionsBuilder.buildSendBenefitAction(smartGift.getTitle()));

        createMemberAndSendPunchCard();
        //Perform punchCard - negative test - check less then 2 punches
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        checkDataStoreNegativeCheckByRuleID(RECEIVEDASSET);
        //Perform punchCard - check equals then 2 punches
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle(), 1);
        //Perform punchCard - negative test- check more than 2 punches
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle(), 1);
    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of punch a punch card with global condition of Limit up to 2 times in total per member. The action is send benfit. the test verify that no more than 2 benfits sent to the member")
    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {


        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("1");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        createSmartGift();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Arrays.asList(ActivityActions.SendBenefit),smartGift.getTitle());

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member and Get member userKey
        createMemberAndSendPunchCard();

        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        log4j.info("punch the punch for the first time and expect to get benfit");
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());

        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        log4j.info("punch another 1 punch and verify that still there is only 1 record of the automation in the data store");
        // the data store should contain only 1 record from the previous automation that run before. if another automation run than the query will return 2 records and this step will fail (2!=1)
        checkDataStoreUpdatedWithAssetByRuleIDNoUserKey(1);
    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of punch a punch card with global condition of specific member. The action is tag member.")
    @Test
    public void testWithGlobalConditionSpecificMembersAndActionTagMember() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(globalCondition), Arrays.asList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //Tag member
        performAnActionTagMember(newMember, "Gold");
        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);

        //Perform punchCard - check less then 2 punches
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        //Check DataStore
        checkAutomationRunAndTagMember();

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of punch a punch card with global condition of between dates and not on the current day. The action is tag member. the test verify that the member didn't get the tag")
    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), Arrays.asList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member and Get member userKey
        createMemberAndSendPunchCard();
        //Perform punchCard - check less then 2 punches
        performPunchAPunchCardNoValidation(smartPunchCard, 1);
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }



    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }

    private void checkDataStoreWithOperation(String operation) throws InterruptedException {
        log4j.info("check Data Store Updated with asset");
        Query.FilterPredicate predicate = new Query.FilterPredicate("RuleID", Query.FilterOperator.EQUAL, automationRuleId);
        java.util.List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the automation: " + automationRuleId,
                buildQuery(operation, predicate), 1, dataStore);
        Assert.assertEquals("Must find one UserAction with the automation: " + automationRuleId, 1, res.size());
    }


    private void performAnActionPunchLastPunchCard(SmartPunchCard punchCard1) throws Exception {
        Thread.sleep(5000); //Wait before th punch card will be send
        MembersService.performSmartActionPunchAPunchCard(punchCard1.getTitle(),5);
    }

    private void CreateRuleWithNumOfPunchesCondition(PunchAPunchCardCondition punchAPunchCardCondition, ActivityAction action) throws Exception {
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(punchAPunchCardCondition), Lists.newArrayList(action));

        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
    }

    private void createMemberAndSendPunchCard() throws Exception {
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //Send gift to member
        performAnActionSendPunchCard(smartPunchCard);
    }


    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.PunchesAPunchCard;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.PunchesAPunchCard.toString() +"_";
    }

}
