package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.conditions.PointsCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import ru.yandex.qatools.allure.annotations.Issue;

import java.util.Arrays;
import java.util.Calendar;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class ReceivesPointsTriggerTest extends BaseRuleHub2Test {

    private static Box box;
    private static boolean firstTest = true;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = tag + timestamp.toString();
            if(firstTest ) {
                serverHelper.configureBackendForPointsPayment();
                firstTest = false;
            }
            //createSmartGift();
        }
    };

    /**
     * Trigger of Receive points. No condition. Action Tag member
     * @throws Exception
     */
    @Test
    public void testNoConditionAndActionTagMember() throws Exception {
        //tag = tag + timestamp.toString();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(ActivityActions.Tag),tag);
        //Activity activity = loyaltyBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenPoints = "200";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);
        checkAutomationRunAndTagMember();
    }

    /**
     * Trigger of receive point with condition of number of point is greater than. Action is Tag member
     * @throws Exception
     */
    @Test
    public void testConditionNumberOfPointsIsGreaterThanAndTagMemberAction() throws Exception {
        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("200");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenPoints = "400";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);
        checkAutomationRunAndTagMember();
    }

    /**
     * Trigger of receive point with condition of number of point is less than. Action is Tag member
     * @throws Exception
     */
    @Test
    public void testConditionNumberOfPointsIsLessThanAndTagMemberAction() throws Exception {
        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("200");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenPoints = "100";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);
        checkAutomationRunAndTagMember();
    }

    /**
     * Trigger of receive point with condition of number of point is exactly. Action is Tag member.
     * @throws Exception
     */
    // NOTE : Currently the test failed due to CNP-10723
   @Issue("CNP-10723")
   @Test
    public void testConditionNumberOfPointsIsExactlyAndTagMemberAction() throws Exception {
        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("900");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenPoints = "900";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);
        checkAutomationRunAndTagMember();
    }


    /**
     * Trigger of receive point with condition of total sum of points is greater than. Action is Tag member
     * @throws Exception
     */
    @Test
    @Category({Hub2Regression.class})
    public void testConditionTotalSumValueIsGreaterThanAndTagMemberAction() throws Exception {
        //Create member
        NewMember member= hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        MembersService.performSmartActionAddPoints("100",timestamp);

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NEW_BALANACE).
                setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("200");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String givenPoints = "250";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);
        checkAutomationRunAndTagMember();
    }

    /**
     * Trigger of receive point with condition of total sum of points is less than. Action is Tag member
     * @throws Exception
     */
    @Test
    public void testConditionTotalSumValueIsLessThanAndTagMemberAction() throws Exception {

        //Create member
        NewMember member= hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        MembersService.performSmartActionAddPoints("500",timestamp);

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NEW_BALANACE).
                setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("800");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String givenPoints = "100";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);
        checkAutomationRunAndTagMember();
    }

    /**
     * Trigger of receive point with condition of total sum of points is exactly. Action is Tag member
     * @throws Exception
     */
    @Test
    public void testConditionTotalSumValueIsExactlyAndTagMemberAction() throws Exception {

        //Create member
        NewMember member= hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        MembersService.performSmartActionAddPoints("500",timestamp);

        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NEW_BALANACE).
                setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("300");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        String givenPoints = "200";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);
        checkAutomationRunAndTagMember();
    }

    /**
     * Trigger of receive point with condition of number of points is Greater than or equal. Action is Send benefit.
     * @throws Exception
     */
    @Test
    public void testConditionNumberOfPointsIsGreaterThanOrEqualToAndSendBenefitAction() throws Exception {
        createSmartGift();
        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("300");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenPoints = "500";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);
        checkAutomationRunAndSendAssetToMember();
    }

    /**
     * Trigger of receive point with condition of number of points is Less than or Equal to. Action is Send benefit.
     * @throws Exception
     */
    @Test
    public void testConditionNumberOfPointsIsLessThanOrEqualToAndSendBenefitAction() throws Exception {
        createSmartGift();
        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("300");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenPoints = "100";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);
        checkAutomationRunAndSendAssetToMember();
    }

    /**
     * Trigger of receive point with condition of total som of points is Greater than or Equal to. Action is Send benefit.
     * @throws Exception
     */
    @Test
    public void testConditionTotalSumValueIsGreaterThanOrEqualToAndSendBenefitAction() throws Exception {
        createSmartGift();
        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS).
                setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("300");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenPoints = "400";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);
        checkAutomationRunAndSendAssetToMember();
    }

    /**
     * Trigger of receive point with condition of total som of points is Less than or Equal to. Action is Send benefit.
     * @throws Exception
     */
    @Test
    public void testConditionTotalSumValueIsLessThanOrEqualToAndSendBenefitAction() throws Exception {
        createSmartGift();
        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NEW_BALANACE).
                setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("300");
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Lists.newArrayList(pointsCondition), Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        //Create member
        hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        String givenPoints = "100";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);
        checkAutomationRunAndSendAssetToMember();
    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of receives points with global condition of specific member. The action is tag member.")
    @Test
    public void testWithGlobalConditionSpecificMembersAndActionTagMember() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(globalCondition), Arrays.asList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //Tag member
        performAnActionTagMember(newMember, "Gold");
        //add points to member
        String givenPoints = "200";
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);
        checkAutomationRunAndTagMember();

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of receives points with global condition of between dates and not on the current day. The action is tag member. the test verify that the member didn't get the tag")
    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), Arrays.asList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        createNewMember();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        //add points to member
        String givenPoints = "200";
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of receives points with global condition of limit up to 2 times in total per member. The action is tag member. the test verify that the member received the push only 2 times")
    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        createNewMember();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String givenPoints1 = "100";
        String givenPoints2 = "200";
        String givenPoints3 = "300";
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints1,timestamp);
        MembersService.performSmartActionAddPoints(givenPoints2,timestamp);
        MembersService.performSmartActionAddPoints(givenPoints3,timestamp);

        checkDataStoreUpdatedWithSendPushNotification(2);
    }


    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.ReceivesPoints;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.ReceivesPoints.toString() +"_";
    }

}