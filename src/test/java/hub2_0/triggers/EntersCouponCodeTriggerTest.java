package hub2_0.triggers;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.content.information.CatalogItemPage;
import hub.common.objects.content.information.CatalogPage;
import hub.common.objects.content.information.CategoryPage;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.enums.ActivityActions;
import hub.hub2_0.common.enums.ActivityStatus;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.ActivityType;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.catalog.CatalogService;
import hub.utils.HubCatalogCreator;
import integration.base.BaseRuleHub2Test;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.common.IServerResponse;
import server.common.Response;
import server.internal.DoCodeBasedAction;
import server.utils.ServerHelper;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class EntersCouponCodeTriggerTest extends BaseRuleHub2Test {

    private static Box box;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = tag + timestamp.toString();

        }
    };


    @Test
    @Category(Hub2Regression.class)
    public void testEnterCouponCodeActionTagMember() throws Exception {

        tag = timestamp.toString();
        ActivityAction action = ActionsBuilder.buildTagMemberAction(tag);
        //create automation and save its name
       String ruleName = createActivityTrigger(action);

       //create coupon code and save one code to use in request
        String randomCodes = generateRandomCodes();
        String codeToUse = timestamp.toString().substring(6,10);
        String codes = randomCodes + codeToUse;
        hubAssetsCreator.createCouponCodeBulkCodes("bulkName" + timestamp,ruleName,"tag" + timestamp,"description" + timestamp,codes);
        Thread.sleep(10000);

        //sign on to get user token
        String userToken = serverHelper.signOn();
        log4j.info("userToken: " + userToken);

        String phoneNumber = String.valueOf(System.currentTimeMillis());
        String userKey1 = automationsHelper.joinClubWithUserToken(phoneNumber, userToken);
        key = automationsHelper.getKey(userKey1);

        performAnActionDoCode(userToken,codeToUse);

        chackDataStoreWithTag();
    }

    private void chackDataStoreWithTag() throws InterruptedException {
        log4j.info("check Data Store Updated with action "+ TAG_OPERATION);
        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction :" +TAG_OPERATION ,
                buildQuery(TAG_OPERATION) , 1, dataStore);
        Assert.assertEquals("Must find one UserAction :" +TAG_OPERATION, 1, res.size());
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info( "ExportData contains the following data : " + t.getValue());
        Assert.assertTrue("Data must contain the tag: " + tag, t.getValue().contains(tag));
    }


    private void performAnActionDoCode(String userToken, String codeToUse) {
        DoCodeBasedAction doCodeBasedAction = new DoCodeBasedAction(userToken,locationId,codeToUse);
        IServerResponse response = doCodeBasedAction.sendRequest(Response.class);
        JSONObject jsonObject = response.getRawObject();
        Assert.assertEquals(jsonObject.get("Status") ,"Success");
        Assert.assertEquals(jsonObject.get("Result") ,"Y");
        Assert.assertEquals(jsonObject.get("Message") ,"performed "+automationRuleId+" ruleId (Smart).");

    }

    private String generateRandomCodes() {

        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < 5; i++) {
            String code = UUID.randomUUID().toString().substring(0,5);
            buffer.append(code + ",");
        }
        return buffer.toString();
    }

    private String createActivityTrigger(ActivityAction action) throws Exception{
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickAllMembersBtn();
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule,Arrays.asList(globalCondition),
                Arrays.asList(action));
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        return rule.getActivityName();
    }

    @After
    public void cleanData() throws Exception{
        deactivateAutomation();
    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.EnteredCouponCode;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.EnteredCouponCode.toString() +"_";
    }

}