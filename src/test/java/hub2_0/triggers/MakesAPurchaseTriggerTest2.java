package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import com.google.appengine.api.datastore.Key;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.services.Navigator;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.limits.RollingPeriod;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub.utils.HubCreatorBase;
import hub.utils.HubMemberCreator;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.SubmitPurchase;
import server.v2_8.common.Item;
import server.v4.SubmitPurchaseResponse;
import server.v4.common.models.Customer;
import server.v4.common.models.Purchase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class MakesAPurchaseTriggerTest2 extends BaseRuleHub2Test {

    private static Box box;


    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    /*private Days resolveDay(int CurrentDay) {
        Days day=null;
        switch (CurrentDay) {
            case 1:
                day = Days.SUNDAY;
                break;
            case 2:
                day = Days.MONDAY;
                break;
            case 3:
                day = Days.TUESDAY;
                break;
            case 4:
                day = Days.WEDNESDAY;
                break;
            case 5:
                day = Days.THURSDAY;
                break;
            case 6:
                day =Days.FRIDAY;
                break;
            case 7:
                day = Days.SATURDAY;
                break;
        }
        return day;
    }*/

    /**
     * Test trigger make purchase with condition TransactionSourceName is one of POS,APP then tag member
     */
    @Test
    @Category({Hub2Regression.class})
    public void testWithConditionTransactionSourceNameIsOneOfAndTagAction_v2_8() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.PURCHASE_SOURCE_NAME)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("POS,APP");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("10009", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);

        SubmitPurchase submitPurchase = buildSubmitPurchaseRequest(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, items, "4");
        submitPurchase.setTransactionSourceName("POS");
        log4j.info("send request submit purchase");
        // Send the request
        getSubmitPurchaseResponse(submitPurchase);

        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition TransactionSourceName is one of POS,APP then tag member
     */
    @Test
    public void testWithConditionTransactionSourceNameIsOneOfAndTagAction_v2_8Negative() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.PURCHASE_SOURCE_TYPE)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("POS,APP");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("10009", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);

        SubmitPurchase submitPurchase = buildSubmitPurchaseRequest(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, items, "4");  submitPurchase.setTransactionSourceName("POS11");
        submitPurchase.setTransactionSource("POS111");
        submitPurchase.setTransactionSourceName("POS");
        log4j.info("send request submit purchase");
        // Send the request
        getSubmitPurchaseResponse(submitPurchase);

        //DataStore check
        checkDataStoreNegative(TAG_OPERATION,transactionId);
    }

    /**
     * Test trigger make purchase with condition TransactionSource is one of POS,APP then tag member
     */
    @Test
    public void testWithConditionTransactionSourceIsOneOfAndTagAction_v2_8() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.PURCHASE_SOURCE_TYPE)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("POS,APP");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("10009", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);

        SubmitPurchase submitPurchase = buildSubmitPurchaseRequest(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, items, "4");
        submitPurchase.setTransactionSource("POS");
        log4j.info("send request submit purchase");
        // Send the request
        getSubmitPurchaseResponse(submitPurchase);

        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition TransactionSource is not one of APP then tag member
     */
    @Test
    public void testWithConditionTransactionSourceIsNotOneOfAndTagAction_v4_0() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.PURCHASE_SOURCE_TYPE)
                .setOperatorKey(InputOperators.IS_NOT_ONE_OF)
                .setConditionValue("APP");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        String transactionId = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(transactionId,10000, Lists.newArrayList()) ;

        server.v4.SubmitPurchase submitPurchase = v4Builder.submitPurchaseRequest(Lists.newArrayList(customer), Lists.newArrayList() ,Lists.newArrayList(), purchase);
        submitPurchase.setxSourceType("POS");
        submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);

        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition TransactionSourceName is not one of APP then tag member
     */
    @Test
    public void testWithConditionTransactionSourceNameIsOneOfAndTagAction_v4_0() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.PURCHASE_SOURCE_NAME)
                .setOperatorKey(InputOperators.IS_NOT_ONE_OF)
                .setConditionValue("APP");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        Customer customer = new Customer();
        customer.setPhoneNumber(newMember.getPhoneNumber());
        String transactionId = UUID.randomUUID().toString();
        Purchase purchase = v4Builder.buildPurchase(transactionId,10000, Lists.newArrayList()) ;

        server.v4.SubmitPurchase submitPurchase = v4Builder.submitPurchaseRequest(Lists.newArrayList(customer), Lists.newArrayList() ,Lists.newArrayList(), purchase);
        submitPurchase.setxSourceName("POS");
        submitPurchase.SendRequestByApiKeyAndValidateResponse(apiKey,SubmitPurchaseResponse.class);

        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }
    /**
     * Test trigger make purchase, then tag member
     */
    @Test
    public void testNoConditionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "1000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    @Test
    public void testNoConditionPunchAPunchCard() throws Exception {

        smartPunchCard = hubAssetsCreator.createSmartPunchCard(timestamp, "5");

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(ActivityActions.PunchThePunchCard), smartPunchCard.getTitle());
        log4j.info("rule1 name:" + rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        HubMemberCreator hubMemberCreator = new HubMemberCreator();
        NewMember newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        key = hubMemberCreator.getMemberKeyToUserKey();
        performAnActionSendPunchCard(smartPunchCard);
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithPunch(transactionId);

    }

    /**
     * Test trigger make purchase with condition POSID is one of 1,4,6, then tag member
     */
    @Test
    public void testWithConditionPOSIDIsOneOfAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.POS_ID)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("1,4,6");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());


        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition POSID is not one of 5,7,9, then tag member
     */
    @Test
    @Category({Hub2Regression.class})
    public void testWithConditionPOSIDIsNotOneOfAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.POS_ID)
                .setOperatorKey(InputOperators.IS_NOT_ONE_OF)
                .setConditionValue("5,7,9");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());


        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        //perform submit purchase
        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition BranchID is exactly 1, then tag member
     */
    @Ignore("'Is exactly' Operator is not supported for Branch ID condition")
    //@Test
    public void testWithConditionBranchIDIsExactlyAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(InputOperators.IS_EXACTLY)
                .setConditionValue("1");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition BranchID is one of 1,4,6, then tag member
     */
    @Test
    public void testWithConditionBranchIDIsOneOfAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("1,4,6");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();

        Item item = createItem("10009", "itemA", "depA", "depAA", 10000);
        ArrayList<Item> items = new ArrayList<>();
        items.add(item);
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(newMember), "10000", new ArrayList<>(), transactionId, items, "4");

        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition BranchID is not one of 5,7,9, then tag member
     */
    @Test
    @Category({Hub2Regression.class})
    public void testWithConditionBranchIDIsNotOneOfAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(InputOperators.IS_NOT_ONE_OF)
                .setConditionValue("5,7,9");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }


    /**
     * Test trigger make purchase with condition POSID is exactly 1, then tag member
     */
    @Ignore("'Is exactly' Operator is not supported for POS ID condition")
    //@Test
    public void testWithConditionPOSIDIsExactlyAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.POS_ID)
                .setOperatorKey(InputOperators.IS_EXACTLY)
                .setConditionValue("1");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());


        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }



    @ru.yandex.qatools.allure.annotations.Description("Test trigger make purchase with global condition of all members (register+unregister). Action is tag member. In this case we register and unregister the member and then make a purchase with the  unregistered member")
    @Test
    public void testWithGlobalConditionAllMembersAndActionTagMemberPerformPurchaseWithNonRegisteredMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickAllMembersBtn();

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(globalCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        NewMember member = createNewMember();
        automationsHelper.unregisterMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", transactionId);
        checkAutomationRunAndTagMember();
    }


    @ru.yandex.qatools.allure.annotations.Description("Test trigger make purchase with global condition of between dates from now and forever and Not On The CurrentDay. Action is Tag Member. the check is Negative - which mean no tag should apper.")
    @Test
    @Category({Hub2Regression.class})
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        buildRuleAndActivity(Lists.newArrayList(dateTimeGlobalCondition),Lists.newArrayList(ActivityActions.Tag),tag);

        NewMember member = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", transactionId);
        checkDataStoreNegative(tag, transactionId);
    }


    @ru.yandex.qatools.allure.annotations.Description("Test trigger make purchase with global condition of Limits Up to 2 times in total. Action is Send benfit. The test Perform the trigger 3 times and verify that only 2 assets received .")
    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        smartGift = hubAssetsCreator.createSmartGift(timestamp);

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        buildRuleAndActivity(Lists.newArrayList(limitsGlobalCondition),Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle() );

        NewMember member = createNewMember();
        String transactionId1 = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", transactionId1);
        Thread.sleep(10000);
        String transactionId2 = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "3000", transactionId2);
        Thread.sleep(10000);
        String transactionId3 = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "4000", transactionId3);
        Thread.sleep(10000);
        checkDataStoreUpdatedWithAsset(transactionId1);
        checkDataStoreUpdatedWithAsset(transactionId2);
        //check that automation did not run
        log4j.info("going to verify that the automation didn't run for the 3rd purchase due to limit global condition of up to 2 times in total");
        checkDataStoreNegative(RECEIVEDASSET, transactionId3);
        Navigator.refresh();
    }




    @ru.yandex.qatools.allure.annotations.Description("Test trigger make purchase with global condition of Limits Up to 1 times per hour. Action is Send benfit. The test Perform the trigger 2 times and verify that only 1 assets received and than we change the creation time of the autoamtion and trigger again and verify that action occur.")
    @Test
    @Category({Hub2Regression.class})
    public void testWithLimitUpTo1TimePerHourAndActionSendBenefit() throws Exception {

        HubCreatorBase hubCreatorBase = new HubMemberCreator();
        tag = ActionsBuilder.TAG + timestamp.toString();
        smartGift = hubAssetsCreator.createSmartGift(timestamp);

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesByPeriod(true).setTimesByPeriod("1").setTimesByPeriod(RollingPeriod.Hour.toString());
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.SendBenefit), smartGift.getTitle());
        log4j.info("rule name:" + rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        NewMember member = createNewMember();

        String transactionId1 = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", transactionId1);
        log4j.info("first purchase should trigger the automation -> user should get an asset");
        checkDataStoreEventBasedAction(automationRuleId,1);

        String transactionId2 = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "3000", transactionId2);
        log4j.info("second purchase should not trigger the automation since we are limit to 1 per hour.");
        updateDateEventBaseAction(automationRuleId,1, hubCreatorBase.dateMinutesBack(70));

        log4j.info("moving the automation creation time back by more than an hour , make a purchase and verify that now automation is running");
        String transactionId3 = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "4000", transactionId3);
        // now the automation should run
        checkDataStoreEventBasedAction(automationRuleId,2);
        Navigator.refresh();
    }



    @ru.yandex.qatools.allure.annotations.Description("Test trigger make purchase with global condition of Limits Up to 1 times per hour and rolling period of 24 hours. Action is Send benfit. The test Perform the trigger 2 times and verify that only 1 assets received and than we change the creation time of the autoamtion back to more that 24 hours ,trigger again and verify that action occur.")
    @Test
    public void testWithLimitUpTo1TimePerRollingPeriod24HourAndActionSendBenefit() throws Exception {

        HubCreatorBase hubCreatorBase = new HubMemberCreator();
        tag = ActionsBuilder.TAG + timestamp.toString();
        smartGift = hubAssetsCreator.createSmartGift(timestamp);

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesByPeriod(true).setTimePerPeriodVal("1").setTimesByPeriod(RollingPeriod.RollingPeriod.toString()).setPeriodMultiplier("24");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.SendBenefit), smartGift.getTitle());
        log4j.info("rule name:" + rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        NewMember member = createNewMember();

        String transactionId1 = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "2000", transactionId1);
        log4j.info("first purchase should trigger the automation -> user should get an asset");
        checkDataStoreEventBasedAction(automationRuleId,1);

        String transactionId2 = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "3000", transactionId2);
        log4j.info("second purchase should not trigger the automation since we are limit to 1 per 24 hours.");
        updateDateEventBaseAction(automationRuleId,1, hubCreatorBase.dateHoursBack(20));
        log4j.info("verify that third purchase not trigger the automation even though we move to the next day but. Automation should run only after 24 hours");

        String transactionId3 = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "4000", transactionId3);
        // move totaly the purchase time by 25 hours
        updateDateEventBaseAction(automationRuleId,1, hubCreatorBase.dateHoursBack(25));

        log4j.info("verify that autoamtion run after update purchase time back by more than 24 hours , make a purchase and verify that now automation is running");
        String transactionId4 = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(member, "5000", transactionId4);
        // now the automation should run
        checkDataStoreEventBasedAction(automationRuleId,2);

        Navigator.refresh();
    }


    /**
     * Test trigger make purchase with condition number of members is greater than 1, then tag member
     */
    @Test
    @Category({Hub2Regression.class})
    public void testWithConditionNumOfMembersIsGreaterThanAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.NUMBER_OF_MEMBERS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("1");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member1
        CharSequence timestampTemp = String.valueOf(System.currentTimeMillis());
        NewMember newMember1 = hubMemberCreator.createMember(timestampTemp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //Create member2
        NewMember newMember2 = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        Key key1 = hubMemberCreator.getMemberKeyToUserKey();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(newMember1, newMember2), "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagForOneOfMembers(transactionId, key1);

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(newMember1.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        Assert.assertTrue("The tag was not found for member " + newMember1.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition number of members is less than 2, then tag member
     */
    @Test
    public void testWithConditionNumOfMembersIsLessThanAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.NUMBER_OF_MEMBERS)
                .setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("2");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(newMember, "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Negative test - purchase number of members is 2
     * Test trigger make purchase with condition number of members is less than 2, then tag member
     */
    @Test
    public void testWithConditionNumOfMembersIsLessThanAndTagActionNegative() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.NUMBER_OF_MEMBERS)
                .setOperatorKey(IntOperators.IS_LESS_THAN)
                .setConditionValue("2");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member1
        CharSequence timestampTemp = String.valueOf(System.currentTimeMillis());
        NewMember newMember1 = hubMemberCreator.createMember(timestampTemp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //Create member2
        NewMember newMember2 = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        Key key1 = hubMemberCreator.getMemberKeyToUserKey();
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(newMember1, newMember2), "20000", transactionId);

        //check that automation did not run
        checkDataStoreNegative(TAG_OPERATION, transactionId);

        log4j.info("member didn't received the tag");

    }

    /**
     * Test trigger make purchase with condition number of members is greater than or equal to 2, then tag member
     */
    @Test
    public void testWithConditionNumOfMembersIsGreaterThanOrEqualToAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.NUMBER_OF_MEMBERS)
                .setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("2");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member1
        CharSequence timestampTemp = String.valueOf(System.currentTimeMillis());
        NewMember newMember1 = hubMemberCreator.createMember(timestampTemp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //Create member2
        NewMember newMember2 = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        Key key1 = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(newMember1, newMember2), "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagForOneOfMembers(transactionId, key1);

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(newMember1.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        Assert.assertTrue("The tag was not found for member " + newMember1.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition number of members is less than or equal to 2, then tag member
     */
    @Test
    public void testWithConditionNumOfMembersIsLessThanOrEqualToAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.NUMBER_OF_MEMBERS)
                .setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("2");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember1 = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        //perform submit purchase
        performAnActionSubmitPurchase(newMember1, "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagByRuleID(tag);

        Assert.assertTrue("The tag was not found for member " + newMember1.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    /**
     * Test trigger make purchase with condition number of members is exactly 2, then tag member
     */
    @Test
    public void testWithConditionNumOfMembersIsExactlyAndTagAction() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        //Create new rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        //Add condition
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.NUMBER_OF_MEMBERS)
                .setOperatorKey(IntOperators.IS_EXACTLY)
                .setConditionValue("2");
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(ActivityActions.Tag), tag);
        log4j.info("rule name:" + rule.getActivityName());

        LoyaltyService.createActivityInBox(activity);
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member1
        CharSequence timestampTemp = String.valueOf(System.currentTimeMillis());
        NewMember newMember1 = hubMemberCreator.createMember(timestampTemp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //Create member2
        NewMember newMember2 = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        //Get member userKey and turn into User_KEY
        Key key1 = hubMemberCreator.getMemberKeyToUserKey();

        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchase(Lists.newArrayList(newMember1, newMember2), "20000", transactionId);
        //DataStore check
        checkDataStoreUpdatedWithTagForOneOfMembers(transactionId, key1);

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(newMember1.getPhoneNumber());
        MembersService.findAndGetMember(findMember);

        Assert.assertTrue("The tag was not found for member " + newMember1.getFirstName(), MembersService.isTagExists(tag));//check that user received the correct tag
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    private void buildRuleAndActivity(List<ApplyCondition> conditions, List<ActivityActions> activityActions, String freeText ) throws Exception {
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(conditions), Lists.newArrayList(activityActions), freeText);
        log4j.info("rule name:" + rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
    }



    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }


    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.MadeAPurchase;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.MadeAPurchase.toString() +"_";
    }
}
