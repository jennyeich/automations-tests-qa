package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.FindMember;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.conditions.PurchaseAnAssetCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.ListOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import ru.yandex.qatools.allure.annotations.Issue;
import server.common.IServerResponse;
import server.common.PaymentType;
import server.internal.GetLocationState;
import server.internal.GetLocationStateResponse;
import server.utils.UserActionsConstants;
import server.v2_8.GetResource;
import server.v2_8.GetResourceResponse;
import server.v2_8.PurchaseAssetResponse;
import server.v2_8.base.ServicesErrorResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.GiftShopItem;

import java.util.*;

import static hub2_0.utils.DateTimeUtility.resolveDay;

public class PurchaseAnAssetTriggerTest extends BaseRuleHub2Test {

    private static Box box;
    private static String userToken;//the user token that we receive after sign on
    private static String assetTemplateKey; //the asset template key as returns from GetResource request
    private static String itemID; //the itemId as returns from GetResource request
    private static String phoneNumber; //the phone number that we use to find the user for all tests
    private static String userKey; //the userKey that returns from JoinClub request



    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
        deactivateAutomation();
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = tag + timestamp.toString();
            if(smartGift == null) {
                //create assets
                try {
                    phoneNumber = String.valueOf(System.currentTimeMillis());
                    smartGift = hubAssetsCreator.createSmartGift(phoneNumber);
                } catch (Exception e) {
                    log4j.error("error while creating asset",e);
                    Assert.fail(e.getMessage());
                }

                try {
                    String version = hubAssetsCreator.addItemToPointShopAndGetVersionAfterPublish(smartGift.getTitle(),locationId);
                    getDataForPurchaseAssetRequest(version);

                } catch (Exception e) {
                    log4j.error("error while adding item to gift shop",e);
                    Assert.fail(e.getMessage());
                }
                try {
                    createUserAndUpdateForTests();

                } catch (Exception e) {
                    log4j.error("error while creating user and update details for test",e);
                    Assert.fail(e.getMessage());
                }
            }
            //each test should initialize the user key for the data store
            key = automationsHelper.getKey(userKey);
        }
    };


    private void getDataForPurchaseAssetRequest(String version) {

        GetResource getResource = new GetResource("location_" + locationId,version);
        IServerResponse response =  getResource.sendRequest(GetResourceResponse.class);
        if (response instanceof ServicesErrorResponse) {
            Assert.fail(((ServicesErrorResponse) response).getErrorMessage());
        }
        Assert.assertNotNull("GetResourceResponse should not return empty response", response);
        List<GiftShopItem> giftShopItems = ((GetResourceResponse)response).getGiftsShopItem();
        GiftShopItem item = giftShopItems.get(0);
        assetTemplateKey = item.getAssetKey();
        itemID = item.getItemID();
    }

    //Create the user as a mobile client to simulate the purchase an assset trigger from point shop
    private void createUserAndUpdateForTests() throws Exception {
        //signOn and get the user token
        phoneNumber = String.valueOf(System.currentTimeMillis());
        userToken = serverHelper.signOn();
        log4j.info("userToken: " + userToken);
        //join the club with the user token
        userKey = automationsHelper.joinClubWithUserToken(phoneNumber, userToken);

        log4j.info("userKey: " + userKey);
        FindMember findMember = new FindMember();
        findMember.setUserKey(userKey);
        MembersService.findAndGetMember(findMember);
        MembersService.performSmartActionAddPoints("300000",timestamp);
        log4j.info("Find member according to user key and give user 300000 points for using in all tests");
        automationsHelper.performUpdateMemberDetails(phoneNumber);
        log4j.info("Update user with relevant details(phone,name,birthday)");
        key = automationsHelper.getKey(userKey);
    }



    @Test
    @Category({Hub2Regression.class})
    public void testPurchaseAssetNoPointsAndAllowNegativeIsNO_ActionNotPerformed() throws Exception {

        serverHelper.setConfigurationForPayment("false", PaymentType.POINTS.type(),"1","false");
        tag = "negative_not_allowed_" + timestamp;
        //Create rule
        createActivityTrigger(Arrays.asList(), ActivityActions.Tag, tag);

        //make sure member has not enough points to purchase
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        timestamp = String.valueOf(System.currentTimeMillis());
        String actionTag = MembersService.performSmartActionAddPoints("-300000",timestamp);
        //Verify the points were reduced
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);

        //purchase asset from gift shop when not enough points
        IServerResponse response = automationsHelper.getPerformPurchaseAssetRequest(assetTemplateKey, itemID, userToken);
        if (!(response instanceof ZappServerErrorResponse)) {
            Assert.fail(((ZappServerErrorResponse) response).getErrorMessage());
        }

        ZappServerErrorResponse errResponse  = (ZappServerErrorResponse )response;
        Assert.assertEquals(errResponse.getErrorMessage(),"Not enough points.");
        Assert.assertEquals(errResponse.getErrorCode(),"5002");
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

        //return the points to member
        timestamp = String.valueOf(System.currentTimeMillis());
        actionTag = MembersService.performSmartActionAddPoints("200000",timestamp);
        //Verify the points were added
        checkUserAction(UserActionsConstants.POINTSTRASACTION,actionTag);
        serverHelper.setConfigurationForPayment("false",PaymentType.POINTS.type(),"1","true");
    }

    @Test
    public void testConditionBenefitIsNotAndSendBenefitAction() throws Exception {

        SmartGift newSmartGift = hubAssetsCreator.createSmartGift("is_not");
        PurchaseAnAssetCondition purchaseAnAssetCondition = new PurchaseAnAssetCondition();
        purchaseAnAssetCondition.setConditionKey(PurchaseAnAssetCondition.ConditionKey.BENEFIT)
                .setOperatorKey(ListOperators.IS_NOT)
                .selectBenefit(newSmartGift.getTitle());
        //Create smart automation
        createActivityTrigger(Arrays.asList(purchaseAnAssetCondition), ActivityActions.SendBenefit, newSmartGift.getTitle());

        //purchase asset from gift shop
        automationsHelper.performPurchaseAsset(assetTemplateKey, itemID, userToken);

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        checkAutomationRunAndSendAssetToMember(newSmartGift.getTitle());
    }


    /**
     * test trigger of Purchase an asset (Point shop) with condition of IS NOT .
     * smart gift created in the test and added to the point shop. than the new member buy the gift from the point shop.
     * the action is not fullfiled so the action not executed.
     * @throws Exception
     */
    @Test
    public void testConditionBenfitNOTEqualsConditionTagActionNegative() throws Exception {
        tag = tag + timestamp.toString();
        PurchaseAnAssetCondition purchaseAnAssetCondition = new PurchaseAnAssetCondition();

        purchaseAnAssetCondition.setConditionKey(PurchaseAnAssetCondition.ConditionKey.BENEFIT)
                .setOperatorKey(ListOperators.IS_NOT)
                .selectBenefit(smartGift.getTitle());

        //Create smart automation
        createActivityTrigger(Arrays.asList(purchaseAnAssetCondition), ActivityActions.Tag, tag);
        //purchase asset from gift shop
        automationsHelper.performPurchaseAsset(assetTemplateKey, itemID, userToken);
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }


    /**
     * test trigger of Purchase an asset (Point shop) with condition of IS_EXACTLY .
     * smart gift created in the test and added to the point shop. than the new member buy the gift from the point shop.
     * the action fullfiled so the action executed.
     * @throws Exception
     */
    @Test
    @Category({Hub2Regression.class})
    public void testConditionBenefitEqualsConditionTagAction() throws Exception {
        tag = tag + timestamp.toString();
        PurchaseAnAssetCondition purchaseAnAssetCondition = new PurchaseAnAssetCondition();

        purchaseAnAssetCondition.setConditionKey(PurchaseAnAssetCondition.ConditionKey.BENEFIT)
                .setOperatorKey(ListOperators.IS_EXACTLY)
                .selectBenefit(smartGift.getTitle());

        //Create smart automation
        createActivityTrigger(Arrays.asList(purchaseAnAssetCondition), ActivityActions.Tag, tag);
        //purchase asset from gift shop
        automationsHelper.performPurchaseAsset(assetTemplateKey, itemID, userToken);

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);
        checkAutomationRunAndTagMember();
    }


    @ru.yandex.qatools.allure.annotations.Description("Test trigger of purchase an asset with global condition of specific member. The action is tag member")
    @Test
    public void testWithGlobalConditionSpecificMembersAndActionTagMember() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(globalCondition), Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);

        performAnActionTagMember("Gold");
        //purchase asset from gift shop
        automationsHelper.performPurchaseAsset(assetTemplateKey, itemID, userToken);

        checkAutomationRunAndTagMember();
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, ActivityTrigger.RedeemsAnAsset.toString());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, com.beust.jcommander.internal.Lists.newArrayList(dateTimeGlobalCondition), com.beust.jcommander.internal.Lists.newArrayList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);

        //purchase asset from gift shop
        automationsHelper.performPurchaseAsset(assetTemplateKey, itemID, userToken);

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, com.beust.jcommander.internal.Lists.newArrayList(limitsGlobalCondition), com.beust.jcommander.internal.Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(phoneNumber);
        MembersService.findAndGetMember(findMember);

        //purchase asset from gift shop
        automationsHelper.performPurchaseAsset(assetTemplateKey, itemID, userToken);
        //purchase asset from gift shop
        automationsHelper.performPurchaseAsset(assetTemplateKey, itemID, userToken);
        //purchase asset from gift shop
        automationsHelper.performPurchaseAsset(assetTemplateKey, itemID, userToken);


        checkDataStoreUpdatedWithSendPushNotification(2);

    }

    private void createActivityTrigger(List<ApplyCondition> purchaseAnAssetCondition, ActivityActions activityActions, String text) throws Exception{
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());

        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, purchaseAnAssetCondition,
                Arrays.asList(activityActions), text);
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
    }

    @After
    public void cleanData() throws Exception{
        deactivateAutomation();
    }



    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.PurchaseAnAssetPointShop;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.PurchaseAnAssetPointShop.toString() +"_";
    }

}
