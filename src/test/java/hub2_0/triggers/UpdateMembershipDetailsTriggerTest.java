package hub2_0.triggers;

import com.beust.jcommander.internal.Lists;
import com.google.appengine.api.datastore.Key;
import edu.emory.mathcs.backport.java.util.Arrays;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.MemberDetails;
import hub.common.objects.member.NewMember;
import hub.common.objects.operations.JoiningCode;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.cases.RuleCase;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.builders.CasesConditionsBuilder;
import hub.hub2_0.common.builders.ConditionsBuilder;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.conditions.UpdateMembershipCondition;
import hub.hub2_0.common.conditions.cases.GeneralCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.DatePicker;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;
import hub.hub2_0.common.conditions.global.dateTime.TimeSelector;
import hub.hub2_0.common.conditions.global.limits.LimitedNumber;
import hub.hub2_0.common.conditions.global.limits.LimitsGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.common.items.SelectValue;
import hub.hub2_0.common.items.SpecificValue;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.assets.AssetService;
import hub.services.member.MembersService;
import hub.utils.HubAssetsCreator;
import hub2_0.utils.DateTimeUtility;
import integration.base.BaseRuleHub2Test;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.JoinClubResponse;
import server.v2_8.UpdatedFields;

import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import static hub2_0.utils.DateTimeUtility.resolveDay;

/**
 * Created by Goni on 2/5/2018.
 */

public class UpdateMembershipDetailsTriggerTest extends BaseRuleHub2Test {

    private static Box box;
    private static final String BRANCH1_JOINING_CODE_TAG = "Branch11";
    private static List<String> BRANCH1_JOINING_CODES;
    private static final String BRANCH2_JOINING_CODE_TAG = "Branch22";
    private static List<String> BRANCH2_JOINING_CODES;


    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
        //create 2 arrays with different joining codes for 3 tests
        BRANCH1_JOINING_CODES = Arrays.asList(createJoiningCodesBulk(BRANCH1_JOINING_CODE_TAG));
        BRANCH2_JOINING_CODES =  Arrays.asList(createJoiningCodesBulk(BRANCH2_JOINING_CODE_TAG));

    }

    private static String[] createJoiningCodesBulk(String bulkTag) throws Exception {
        String randomCodes = generateRandomCodes();
        String timestamp = String.valueOf(System.currentTimeMillis());
        String codeToUse = timestamp.substring(6,10);
        String codes = randomCodes + codeToUse;
        JoiningCode joiningCode = HubAssetsCreator.generateJoiningCodes(bulkTag,bulkTag,bulkTag,codes);
        AssetService.createJoiningCodes(joiningCode);
        return codes.split(",");
    }

    private static String generateRandomCodes() {

        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < 5; i++) {
            String code = UUID.randomUUID().toString().substring(0,5);
            buffer.append(code + ",");
        }
        return buffer.toString();
    }


    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = tag + timestamp.toString();
            createSmartGift();
        }
    };

    @After
    public void cleanData() throws Exception{
        deactivateAutomation();
    }

    @ru.yandex.qatools.allure.annotations.Description("C12286: trigger:Updates Membership Details with Split Cases")
    @Test
    @Category({Hub2Regression.class})
    public void testUpdateWithSplitCasesConditionJoiningCodeIsTagMember() throws Exception {

        /****case1 --- Should pass****/
        tag = BRANCH1_JOINING_CODE_TAG + "_"+ timestamp.toString();
        GeneralCondition generalCondition = CasesConditionsBuilder.buildGeneralConditionUpdateMember(UpdateMembershipCondition.ConditionKey.JOINING_CODE_BULK_TAG,InputOperators.IS_ONE_OF,BRANCH1_JOINING_CODE_TAG);
        RuleCase case1 = new RuleCase();
        case1.setDescription("case " + BRANCH1_JOINING_CODE_TAG);
        case1.setConditions(Lists.newArrayList(generalCondition));
        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction(tag);
        case1.setActions(Lists.newArrayList(activityAction1));


        /****case2 --- Should pass****/
        GeneralCondition generalCondition2 = CasesConditionsBuilder.buildGeneralConditionUpdateMember(UpdateMembershipCondition.ConditionKey.JOINING_CODE_BULK_TAG,InputOperators.IS_ONE_OF,BRANCH2_JOINING_CODE_TAG);
        RuleCase case2 = new RuleCase();
        case2.setDescription("case " + BRANCH2_JOINING_CODE_TAG);
        case2.setConditions(Lists.newArrayList(generalCondition2));
        ActivityAction activityAction2 = ActionsBuilder.buildSendPushAction("Hello, you are registered with" + BRANCH2_JOINING_CODE_TAG);
        case2.setActions(Lists.newArrayList(activityAction2));

        //create activity with 2 cases
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivityWithCases(getTrigger().toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(case1,case2));

        LoyaltyService.createActivityInBox(activity);
        log4j.info("rule name:" +rule.getActivityName());
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //register member and check if get the tag
        timestamp = String.valueOf(System.currentTimeMillis());
        String code2 = BRANCH2_JOINING_CODES.get(1);
        JoinClubResponse joinClubResponse = serverHelper.createMember(timestamp.toString(),code2);
        key = serverHelper.getDSKey(joinClubResponse.getUserKey());

        checkDataStoreUpdatedWithSendPushNotification(1);
        log4j.info("member received the push --> case 2 passed");

        //join with different code - see that not getting the tag
        timestamp = String.valueOf(System.currentTimeMillis());
        String code1 = BRANCH1_JOINING_CODES.get(1);
        joinClubResponse = serverHelper.createMember(timestamp.toString(),code1);
        key = serverHelper.getDSKey(joinClubResponse.getUserKey());

        checkDataStoreUpdatedWithTagByRuleID();
        log4j.info("member received the tag - case 1 passed");

    }

    @ru.yandex.qatools.allure.annotations.Description("C12305: trigger:Updates Membership Details, condition: updated field (numeric field)")
    @Test
    public void testUpdateNumericFieldTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        NewMember newMember = hubMemberCreator.createMemberWithGenericInteger(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX ,111);
        key = hubMemberCreator.getMemberKeyToUserKey();

        String numberToSet = String.valueOf(222);
        SpecificValue fromSpecificValue = ConditionsBuilder.buildSpecificValue(IntOperators.IS_LESS_THAN,numberToSet);
        SpecificValue toSpecificValue = ConditionsBuilder.buildSpecificValue(IntOperators.IS_EXACTLY,numberToSet);
        UpdateMembershipCondition updateMembershipCondition =
                ConditionsBuilder.buildUpdateMemberCondition(UpdateMembershipCondition.ConditionKey.GENERIC_INTEGER,
                        SelectValue.SELECT_VALUE_SPECIFIC_VALUE(fromSpecificValue),
                        SelectValue.SELECT_VALUE_SPECIFIC_VALUE(toSpecificValue));

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        performUpdateMemberDetails(newMember,Hub2Constants.GENERIC_INTEGER,numberToSet);

        checkAutomationRunAndTagMember();
        log4j.info("member received the tag -> as expected");

    }


    @ru.yandex.qatools.allure.annotations.Description("C12311: Changed from Empty value to any value - make sure the automation is being executed successfully")
    @Test
    public void testConditionChangeEmptyToAnyTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

       UpdateMembershipCondition updateMembershipCondition = ConditionsBuilder.buildUpdateMemberCondition(UpdateMembershipCondition.ConditionKey.GENERIC_STRING,
               SelectValue.SELECT_VALUE_EMPTY_VALUE(),
               SelectValue.SELECT_VALUE_ANY_VALUE());

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        performUpdateMemberDetails(newMember,Hub2Constants.GENERIC_STRING,"my-owsom-car");

        checkAutomationRunAndTagMember();
        log4j.info("member received the tag as expected");

    }

    @ru.yandex.qatools.allure.annotations.Description("C12311: Changed from Empty value to any value - Negative test-  make sure the automation not executed - condition not fulfill")
    @Test
    @Category({Hub2Regression.class})
    public void testConditionChangeEmptyToAnyTagMemberNegative() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        UpdateMembershipCondition updateMembershipCondition = ConditionsBuilder.buildUpdateMemberCondition(UpdateMembershipCondition.ConditionKey.FIRST_NAME,
                SelectValue.SELECT_VALUE_EMPTY_VALUE(),
                SelectValue.SELECT_VALUE_ANY_VALUE());


        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        performUpdateMemberDetails(newMember,Hub2Constants.FIRST_NAME,newMember.getFirstName() + "_update");

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
        log4j.info("member did not received the tag -> as expected");

    }

    @ru.yandex.qatools.allure.annotations.Description("C12313: Changed from Empty value to empty value - make sure the automation is being executed successfully")
    @Test
    public void testConditionChangeEmptyToEmptyTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        UpdateMembershipCondition updateMembershipCondition = ConditionsBuilder.buildUpdateMemberCondition(UpdateMembershipCondition.ConditionKey.GENERIC_STRING,
                SelectValue.SELECT_VALUE_EMPTY_VALUE(),
                SelectValue.SELECT_VALUE_EMPTY_VALUE());


        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //update but condition not fulfilled
        log4j.info("Update member generic string to www - condition result: false");
        performUpdateMemberDetails(newMember,Hub2Constants.GENERIC_STRING,"wwww");
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
        log4j.info("member did not received the tag as expected");

        log4j.info("Update member generic string to empty but from www - condition result: false");
        performUpdateMemberDetails(newMember,Hub2Constants.GENERIC_STRING,"");
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
        log4j.info("member did not received the tag -->as expected");

        //update with condition fulfilled
        log4j.info("Update member generic string to empty from empty - condition result: true");
        performUpdateMemberDetails(newMember,Hub2Constants.GENERIC_STRING,"");
        checkAutomationRunAndTagMember();
        log4j.info("member received the tag -->as expected  ");

    }

    @ru.yandex.qatools.allure.annotations.Description("C12312: Changed from Empty value to specific value - make sure the automation is being executed successfully")
    @Test
    public void testConditionChangeEmptyToSpecificValueTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        SpecificValue specificValue = ConditionsBuilder.buildSpecificValue(InputOperators.IS_ONE_OF,"my-owsom-car");
        UpdateMembershipCondition updateMembershipCondition =  ConditionsBuilder.buildUpdateMemberCondition(UpdateMembershipCondition.ConditionKey.GENERIC_STRING,
                SelectValue.SELECT_VALUE_EMPTY_VALUE(),
                SelectValue.SELECT_VALUE_SPECIFIC_VALUE(specificValue));


        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //update but condition not fulfilled
        log4j.info("Update member generic string to www - condition result: false");
        performUpdateMemberDetails(newMember,Hub2Constants.GENERIC_STRING,"wwww");
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
        log4j.info("member did not received the tag as expected");

        log4j.info("Update member generic string to empty but from www - condition result: false");
        performUpdateMemberDetails(newMember,Hub2Constants.GENERIC_STRING,"");
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
        log4j.info("member did not received the tag -->as expected");

        //update with condition fulfilled
        log4j.info("Update member generic string to 'my-owsom-car' from empty - condition result: true");
        performUpdateMemberDetails(newMember,Hub2Constants.GENERIC_STRING,"my-owsom-car");
        checkAutomationRunAndTagMember();
        log4j.info("member received the tag -->as expected  ");

    }

    @ru.yandex.qatools.allure.annotations.Description("C12302: Changed from any value to any value - make sure the automation is being executed successfully")
    @Test
    public void testConditionChangeAnyToAnyTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();

        updateMembershipCondition.setConditionKey(UpdateMembershipCondition.ConditionKey.FIRST_NAME)
                .setSelectValueFrom(SelectValue.SELECT_VALUE_ANY_VALUE())//any value is default
                .setSelectValueTo(SelectValue.SELECT_VALUE_ANY_VALUE());

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        performUpdateMemberDetails(newMember,Hub2Constants.FIRST_NAME,"stam");

        checkAutomationRunAndTagMember();
        log4j.info("member received the tag as expected");

    }

    @ru.yandex.qatools.allure.annotations.Description("C12310: Changed from any value to empty value - make sure the automation is being executed successfully")
    @Test
    public void testConditionChangeAnyToEmptyTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();

        updateMembershipCondition.setConditionKey(UpdateMembershipCondition.ConditionKey.FIRST_NAME)
                .setSelectValueFrom(SelectValue.SELECT_VALUE_ANY_VALUE())//any value is default
                .setSelectValueTo(SelectValue.SELECT_VALUE_EMPTY_VALUE());

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //update but condition not fulfilled
        performUpdateMemberDetails(newMember,Hub2Constants.FIRST_NAME,"ssss");
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
        log4j.info("member did not received the tag as expected");

        //update with condition fulfilled
        performUpdateMemberDetails(newMember,Hub2Constants.FIRST_NAME,"");
        checkAutomationRunAndTagMember();
        log4j.info("member received the tag as expected");

    }

    @ru.yandex.qatools.allure.annotations.Description("C12309: Changed from any value to specific value - make sure the automation is being executed successfully")
    @Test
    @Category({Hub2Regression.class})
    public void testConditionChangeAnyToSpecificValueTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        SpecificValue specificValue = ConditionsBuilder.buildSpecificValue(InputOperators.IS_EXACTLY,newMember.getLastName() +"_Updated");
        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();
        updateMembershipCondition.setConditionKey(UpdateMembershipCondition.ConditionKey.LAST_NAME)
                .setSelectValueFrom(SelectValue.SELECT_VALUE_ANY_VALUE())//any value is default
                .setSelectValueTo(SelectValue.SELECT_VALUE_SPECIFIC_VALUE(specificValue));

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //update but condition not fulfilled
        performUpdateMemberDetails(newMember,Hub2Constants.LAST_NAME,"ssss");
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
        log4j.info("member did not received the tag as expected");

        //update with condition fulfilled
        performUpdateMemberDetails(newMember,Hub2Constants.LAST_NAME,newMember.getLastName() +"_Updated");
        checkAutomationRunAndTagMember();
        log4j.info("member received the tag as expected");

    }

    @ru.yandex.qatools.allure.annotations.Description("C12315: Changed from specific value to specific value - make sure the automation is being executed successfully")
    @Test
    @Category({Hub2Regression.class})
    public void testConditionChangeSpecificValueToSpecificValueTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        String newEmail = newMember.getEmail().replace("@","update@");
        SpecificValue fromSpecificValue = ConditionsBuilder.buildSpecificValue(InputOperators.IS_EXACTLY,newMember.getEmail());
        SpecificValue toSpecificValue = ConditionsBuilder.buildSpecificValue(InputOperators.IS_EXACTLY,newEmail);
        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();
        updateMembershipCondition.setConditionKey(UpdateMembershipCondition.ConditionKey.EMAIL)
                .setSelectValueFrom(SelectValue.SELECT_VALUE_SPECIFIC_VALUE(fromSpecificValue))
                .setSelectValueTo(SelectValue.SELECT_VALUE_SPECIFIC_VALUE(toSpecificValue));

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //update with condition fulfilled
        performUpdateMemberDetails(newMember,Hub2Constants.EMAIL,newEmail);
        checkAutomationRunAndTagMember();
        log4j.info("member received the tag as expected");

    }

    @ru.yandex.qatools.allure.annotations.Description("C12314: Changed from specific value to any value - make sure the automation is being executed successfully")
    @Test
    public void testConditionChangeSpecificValueToAnyTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        String membershipKey = MembersService.getMembershipKey();
        key = hubMemberCreator.getMemberKeyToUserKey();

        SpecificValue fromSpecificValue = ConditionsBuilder.buildBooleanSpecificValue(ActivityCondition.BOOLEAN_YES);
        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();
        updateMembershipCondition.setConditionKey(UpdateMembershipCondition.ConditionKey.ALLOW_SMS)
                .setSelectValueFrom(SelectValue.SELECT_VALUE_SPECIFIC_VALUE(fromSpecificValue))
                .setSelectValueTo(SelectValue.SELECT_VALUE_ANY_VALUE());//any is default

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //update with condition fulfilled
        UpdatedFields updatedFields = new UpdatedFields();
        updatedFields.setAllowSMS(Boolean.FALSE);
        serverHelper.updateMembership(membershipKey,updatedFields);
        checkDataStoreUpdatedWithTagByRuleID();
        log4j.info("member received the tag as expected");

    }

    @ru.yandex.qatools.allure.annotations.Description("C123164: Changed from specific value to empty value - make sure the automation is being executed successfully")
    @Test
    public void testConditionChangeSpecificValueToEmptyTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        SpecificValue fromSpecificValue = ConditionsBuilder.buildSpecificValue(InputOperators.IS_NOT,"DDD");
        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();
        updateMembershipCondition.setConditionKey(UpdateMembershipCondition.ConditionKey.LAST_NAME)
                .setSelectValueFrom(SelectValue.SELECT_VALUE_SPECIFIC_VALUE(fromSpecificValue))
                .setSelectValueTo(SelectValue.SELECT_VALUE_EMPTY_VALUE());

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());


        //update but condition not fulfilled
        log4j.info("Update member last name to DDD - condition result: false");
        performUpdateMemberDetails(newMember,Hub2Constants.LAST_NAME,"DDD");
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
        log4j.info("member did not received the tag -> as expected");

        //update with condition not fulfilled
        log4j.info("Update member with last name DDD to empty  - condition result: false");
        performUpdateMemberDetails(newMember,Hub2Constants.LAST_NAME,"");
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
        log4j.info("member did not received the tag -> as expected");

        //update with condition fulfilled
        log4j.info("Update member with last name which is not DDD to empty  - condition result: true");
        performUpdateMemberDetails(newMember,Hub2Constants.LAST_NAME,"");
        checkAutomationRunAndTagMember();
        log4j.info("member received the tag -> as expected");

    }


    @ru.yandex.qatools.allure.annotations.Description("C12284: trigger:Updates Membership Details, condition: Updated during registration? = YES")
    @Test
    @Category({Hub2Regression.class})
    public void testConditionUpdateDuringRegistrationIsYESTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        NewMember memberBeforeAutomation = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        Key keyMemberBeforeAutomation = hubMemberCreator.getMemberKeyToUserKey();

        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();
        updateMembershipCondition.setConditionKey(UpdateMembershipCondition.ConditionKey.UPDATE_DURING_REGISTRATION).clickBooleanValue(YesNo.Yes);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        timestamp = String.valueOf(System.currentTimeMillis());
        //register member and check if get the tag
        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndTagMember();
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

        //update exist member and see if not getting the tag
        key = keyMemberBeforeAutomation;
        performUpdateMemberDetails(memberBeforeAutomation);

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
        log4j.info("member did not received the tag as expected");

    }

    @ru.yandex.qatools.allure.annotations.Description("C12284: trigger:Updates Membership Details, condition: Updated during registration? = NO")
    @Test
    public void testConditionUpdateDuringRegistrationIsNOTagMember() throws Exception {


        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();
        updateMembershipCondition.setConditionKey(UpdateMembershipCondition.ConditionKey.UPDATE_DURING_REGISTRATION);//default value is NO

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        ActivityAction sendPush = ActionsBuilder.buildSendPushAction("welcome " + timestamp.toString());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(sendPush));

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        timestamp = String.valueOf(System.currentTimeMillis());

        //register member and check if get the push
        NewMember member = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        checkAutomationRunAndSendPushNotification(1);
        log4j.info("member received the push");

        //update exist member and see if getting the push again
        performUpdateMemberDetails(member);

        checkAutomationRunAndSendPushNotification(2);
        log4j.info("member received the push again after update as expected");

    }

    @ru.yandex.qatools.allure.annotations.Description("C12282: trigger:Updates Membership Details, condition: Joining code bulk tag is")
    @Test
    @Category({Hub2Regression.class})
    public void testConditionJoiningCodeIsTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();

        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();
        updateMembershipCondition.setConditionKey(UpdateMembershipCondition.ConditionKey.JOINING_CODE_BULK_TAG)
                .setOperatorKey(InputOperators.IS_ONE_OF).setConditionValue(BRANCH1_JOINING_CODE_TAG);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());


        //register member and check if get the tag
        timestamp = String.valueOf(System.currentTimeMillis());
        String code1 = BRANCH1_JOINING_CODES.get(2);
        JoinClubResponse joinClubResponse = serverHelper.createMember(timestamp.toString(),code1);
        key = serverHelper.getDSKey(joinClubResponse.getUserKey());

        checkDataStoreUpdatedWithTagByRuleID();
        log4j.info("member received the tag");

        //join with different code - see that not getting the tag
        timestamp = String.valueOf(System.currentTimeMillis());
        String code2 = BRANCH2_JOINING_CODES.get(2);
        joinClubResponse = serverHelper.createMember(timestamp.toString(),code2);
        key = serverHelper.getDSKey(joinClubResponse.getUserKey());

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
        log4j.info("member did not received the tag as expected");

    }

    @ru.yandex.qatools.allure.annotations.Description("C12283: trigger:Updates Membership Details, condition: Joining code bulk tag is not")
    @Test
    public void testConditionJoiningCodeIsNotOneOfTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();


        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();
        updateMembershipCondition.setConditionKey(UpdateMembershipCondition.ConditionKey.JOINING_CODE_BULK_TAG)
                .setOperatorKey(InputOperators.IS_NOT_ONE_OF).setConditionValue(BRANCH2_JOINING_CODE_TAG);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(updateMembershipCondition), Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());


        //register member and check if get the tag
        timestamp = String.valueOf(System.currentTimeMillis());
        String code1 = BRANCH1_JOINING_CODES.get(0);

        JoinClubResponse joinClubResponse = serverHelper.createMember(timestamp.toString(),code1);
        key = serverHelper.getDSKey(joinClubResponse.getUserKey());

        checkDataStoreUpdatedWithTagByRuleID();
        log4j.info("member received the tag");

        //join with different code - see that not getting the tag
        timestamp = String.valueOf(System.currentTimeMillis());
        String code2 = BRANCH2_JOINING_CODES.get(0);
        joinClubResponse = serverHelper.createMember(timestamp.toString(),code2);
        key = serverHelper.getDSKey(joinClubResponse.getUserKey());

        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
        log4j.info("member did not received the tag as expected");

    }


    @Test
    public void testNoConditionTagMember() throws Exception {

        tag = ActionsBuilder.TAG + timestamp.toString();
        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule, Lists.newArrayList(ActivityActions.Tag),tag);

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        performUpdateMemberDetails(newMember);

        checkAutomationRunAndTagMember();
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }


    @Test
    public void testNoConditionSendBenefit() throws Exception {


        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule, Lists.newArrayList(ActivityActions.SendBenefit),smartGift.getTitle());

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");

        performUpdateMemberDetails(newMember);
        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());
        log4j.info("member received the benefit");
        log4j.info(smartGift.getTitle() + " was added to the member ");

    }


    @Test
    public void testNoConditionSendPush() throws Exception {

        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule, Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        performUpdateMemberDetails(newMember);
        checkAutomationRunAndSendPushNotification();
        log4j.info("member received the push");

    }

    @Test
    public void testNoConditionSendBenefitAndSendPushNotification() throws Exception {


        NewMember newMember = hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        ActivityAction activityAction1 = ActionsBuilder.buildAction( ActivityActions.SendBenefit.toString(), smartGift.getTitle());
        ActivityAction activityAction2 = ActionsBuilder.buildAction( ActivityActions.SendAPushNotification.toString(), "push " + timestamp);
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(),box,rule,Lists.newArrayList(),Lists.newArrayList(activityAction1,activityAction2));

        log4j.info("rule name:" +rule.getActivityName());
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        performUpdateMemberDetails(newMember);

        checkAutomationRunAndSendAssetToMember(smartGift.getTitle());
        log4j.info("member received the benefit");
        log4j.info(smartGift.getTitle() + " was added to the member ");

        checkAutomationRunAndSendPushNotification();
        log4j.info("member received the push");

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of update membership details with global condition of specific member. The action is tag member.")
    @Test
    public void testWithGlobalConditionSpecificMembersAndActionTagMember() throws Exception {

        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(MemberNonRegistrationFormFields.Tags.toString())
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue("Gold");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(globalCondition), java.util.Arrays.asList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        //Create member
        NewMember newMember = createNewMember();
        //Tag member
        performAnActionTagMember(newMember, "Gold");
        performUpdateMemberDetails(newMember);

        checkAutomationRunAndTagMember();
        log4j.info("member received the tag");
        log4j.info(tag + " was added to the member ");

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of update membership details with global condition of between dates and not on the current day. The action is tag member. the test verify that the member didn't get the tag")
    @Test
    public void testWithGlobalConditionBetweenDatesFromNowAndForeverAndNotOnTheCurrentDayAndActionTagMemberNegative() throws Exception {

        //Create member
        NewMember newMember = createNewMember();

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        Days currentDay= resolveDay(day);
        DateTimeGlobalCondition dateTimeGlobalCondition = new DateTimeGlobalCondition();
        dateTimeGlobalCondition.clickSpecificDateTimeChk();
        DatePicker datePickerStart = DatePicker.newDatePicker().setSelectStartDay(DateTimeUtility.getTodayAsNumber());

        DaysCondition daysCondition = DaysCondition.newDayCondition().unSelectDays(currentDay).setTimeSelector(TimeSelector.newTimeSelector().setAllDay());
        SpecificDateTime specificDateTime = SpecificDateTime.newSpecificDateTime()
                .setDaysConditions(daysCondition).setDatePickerStart(datePickerStart);
        dateTimeGlobalCondition.setSpecificDateTime(specificDateTime);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(dateTimeGlobalCondition), java.util.Arrays.asList(ActivityActions.Tag),tag);
        LoyaltyService.createActivityInBox(activity);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());

        performUpdateMemberDetails(newMember);
        checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);

    }

    @ru.yandex.qatools.allure.annotations.Description("Test trigger of update membership details with global condition of limit up to 2 times in total per member. The action is tag member. the test verify that the member received the push only 2 times")
    @Test
    public void testWithGlobalConditionLimitUpTo2TimesInTotalPerMemberAndActionSendBenefitPerformTheTrigger3Times() throws Exception {

        //Create member
        NewMember newMember = createNewMember();

        LimitsGlobalCondition limitsGlobalCondition = new LimitsGlobalCondition();
        limitsGlobalCondition.clickLimitedNumber();
        LimitedNumber limitedNumber = limitsGlobalCondition.newLimitedNumber().setUpToTimesInTotal(true).setTimesInTotalVal("2");
        limitsGlobalCondition.setLimitedNumberOfPurchases(limitedNumber);

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, getAutomationName());
        Activity activity = ruleBuilder.buildActivity(getTrigger().toString(), box, rule, Lists.newArrayList(limitsGlobalCondition), Lists.newArrayList(ActivityActions.SendAPushNotification),timestamp.toString());
        LoyaltyService.createActivityInBox(activity);
        log4j.info("Created rule name:" + rule.getActivityName());
        System.out.println("Create campaign " + box.getBoxName() + " successfully!");
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());


        performUpdateMemberDetails(newMember,Hub2Constants.FIRST_NAME,"New_1");
        performUpdateMemberDetails(newMember,Hub2Constants.FIRST_NAME,"New_2");
        performUpdateMemberDetails(newMember,Hub2Constants.FIRST_NAME,"New_3");

        checkDataStoreUpdatedWithSendPushNotification(2);

    }

    private void performUpdateMemberDetails(NewMember member,String updateField,String newValue) throws Exception {
        MemberDetails memberDetails = new MemberDetails();
        memberDetails.setPhoneNumber(member.getPhoneNumber());
        switch(updateField){
            case Hub2Constants.FIRST_NAME:
                memberDetails.setFirstName(newValue);
                break;
            case Hub2Constants.LAST_NAME:
                memberDetails.setLastname(newValue);
                break;
            case Hub2Constants.EMAIL:
                memberDetails.setEmail(newValue);
                break;
            case Hub2Constants.GENERIC_STRING:
                memberDetails.setGenericString(newValue);
                break;
            case Hub2Constants.GENERIC_INTEGER:
                memberDetails.setGenericInteger(newValue);
                break;
            default:
                log4j.info("update field is not supported - no update done!");
                break;
        }

        MembersService.updateMemberDetails(memberDetails);
    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.UpdatedMembershipDetails;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.UpdatedMembershipDetails.toString() +"_";
    }
}


