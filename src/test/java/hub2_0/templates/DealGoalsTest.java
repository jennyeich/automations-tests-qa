package hub2_0.templates;

import com.google.common.collect.Lists;
import common.BaseTest;
import common.LocalMongoDB;
import edu.emory.mathcs.backport.java.util.Collections;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.common.PermissionLevel;
import hub.common.objects.common.Tags;
import hub.common.objects.member.NewMember;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Deal;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.cases.DealCase;
import hub.hub2_0.basePages.loyalty.templates.SaveProductTemplate;
import hub.hub2_0.basePages.loyalty.templates.TemplateCategories;
import hub.hub2_0.basePages.loyalty.templates.TemplateVerticals;
import hub.hub2_0.basePages.loyalty.templates.TemplatesLibrary;
import hub.hub2_0.common.actions.dealActions.MakeItemsFreeAction;
import hub.hub2_0.common.actions.dealActions.SendCodeToPOSAction;
import hub.hub2_0.common.builders.CasesConditionsBuilder;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.cases.GeneralCondition;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.ItemOperation;
import hub.hub2_0.common.enums.ActivityType;
import hub.hub2_0.common.items.SelectBundle;
import hub.hub2_0.services.loyalty.ActivityService;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.AcpService;
import hub2_0.utils.DateTimeUtility;
import hub2_0.utils.MongoUtils;
import hub2_0.utils.ProductTemplateUtility;
import integration.base.BaseHub2DealTest;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DealGoalsTest extends BaseHub2DealTest {

    private static final String ENTIRE_TICKET_DEAL = "EntireTicketDeal_";
    private static final String ADVANCED_DEAL = "AdvancedDeal";
    private static final String MULTIPLE_CASES_DEAL = "MultipleCasesDeal";


    private static String resolveLocationId;
    private static Box box;
    private static String[] productTemplateNamesArr = new String[3];
    private static String goal;
    private static final String GOAL_PREFIX = "Goal_";
    private static String goalName;
    private static NewMember newMember;
    private static String activityName;


    @BeforeClass
    public static void initGoals() throws Exception {
        box = dealBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
        resolveLocationId = resolveLocationId(locationId);
        timestamp = String.valueOf(System.currentTimeMillis());
        newMember = hubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);

        BaseTest.sqlDB.setHub2BusinessVertical(resolveLocationId, TemplateVerticals.Services.getDBValue());
        AcpService.setPermissionTag(Tags.HUB2_TEMPLATE_PRODUCT.toString(), PermissionLevel.ALLOW.toString());

        Set<String> templateNames = initProductTemplates();
        goalName = GOAL_PREFIX + DateTimeUtility.randomInt(1, 10);
        MongoUtils.createGoal(BaseTest.mongoDB, resolveLocationId, goalName, Collections.emptySet(), templateNames);
        List<String> list = BaseTest.mongoDB.queryCollectionByProperty(LocalMongoDB.GOALS, goalName, "name");
        if (list.size() > 0)
            goal = list.get(0);
    }

    @After
    public void deactivateDeals() throws Exception {
        try {
            ActivityService.executeOperation(activityName, ItemOperation.STOP);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @AfterClass
    public static void deleteTemplates() throws Exception {
        MongoUtils.archiveDealProductTemplates(mongoDB);
        MongoUtils.deleteGoal(BaseTest.mongoDB, goal);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };

    @Test
    public void testGoalWithEntireTicketDeal() throws Exception {

        if (goal.startsWith(GOAL_PREFIX)) {

            String productTemplateName = productTemplateNamesArr[0];
            Deal deal = new Deal();
            activityName = ProductTemplateUtility.createActivityFromGoal(goalName, productTemplateName, deal);

            Item item1 = createItem("1111", "Prod1", "123", "Dep1", 5000, 1);
            Item item2 = createItem("2222", "Prod1", "123", "Dep1", 10000, 1);
            getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1, item2), "15000", "-1000");
        }
    }

    @Test
    @Category({Hub2Regression.class})
    public void testGoalWithMultipleCases() throws Exception {

        if (goal.startsWith(GOAL_PREFIX)) {

            String productTemplateName = productTemplateNamesArr[1];
            Deal deal = new Deal();
            activityName = ProductTemplateUtility.createActivityFromGoal(goalName, productTemplateName, deal);

            Item item1 = createItem("1111", "Prod1", "123", "Dep1", 5000, 5);
            Item item2 = createItem("2222", "Prod1", "123", "Dep1", 10000, 1);
            getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1, item2), "15000", "-5000");
        }
    }

    @Test
    public void testGoalEditProductTemplateAndActivate() throws Exception {

        if (goal.startsWith(GOAL_PREFIX)) {

            String productTemplateName = productTemplateNamesArr[2];
            OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
            occurrencesCondition.setSelectItemsDialog(selectItemsByItemCode("1111"));

            MakeItemsFreeAction makeItemsFreeAction = new MakeItemsFreeAction();
            SelectBundle selectBundle = new SelectBundle();
            selectBundle.clickSpecificItemsFromBundleButton();
            selectBundle.clickApplyButton();
            makeItemsFreeAction.setSelectItems(selectBundle);
            makeItemsFreeAction.setLimitDiscountTimesValue("2");

            Deal deal = new Deal();
            deal.setAdvancedDiscountEditMode();
            deal.setOccurrencesConditions(Lists.newArrayList(occurrencesCondition));
            deal.setActions(Lists.newArrayList(makeItemsFreeAction));
            activityName = ProductTemplateUtility.createActivityFromGoal(goalName, productTemplateName, deal);

            Item item1 = createItem("1111", "Prod1", "123", "Dep1", 5000, 5);
            Item item2 = createItem("2222", "Prod1", "123", "Dep1", 10000, 1);
            getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1, item2), "15000", "-1000");
        }
    }

    private static Set initProductTemplates() throws Exception {
        List<Activity> productTemplates = new ArrayList<>();
        Set<String> templateNames = new HashSet<>();
        TemplateVerticals vertical = TemplateVerticals.Services;

        // template1
        timestamp = String.valueOf(System.currentTimeMillis());
        templateNames.add(TemplatesLibrary.PRODUCT_TEMPLATE_PREFIX + timestamp);
        productTemplateNamesArr[0] = TemplatesLibrary.PRODUCT_TEMPLATE_PREFIX + timestamp;
        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ENTIRE_TICKET_DEAL + timestamp, "product deal template " + vertical.name());
        Activity activity1 = dealBuilder.buildEntireTicketDiscountActivity(box, deal, Lists.newArrayList(), Lists.newArrayList(buildAmountOffDiscountOnEntireTicket("10"),
                buildSendCodeToPOS(SendCodeToPOSAction.CodeType.ITEM_CODE, timestamp.toString())));
        activity1.clickSaveAsProductTemplate(saveProductTemplate(TemplatesLibrary.PRODUCT_TEMPLATE_PREFIX + timestamp));
        productTemplates.add(activity1);

        //template2
        timestamp = String.valueOf(System.currentTimeMillis());
        templateNames.add(TemplatesLibrary.PRODUCT_TEMPLATE_PREFIX + timestamp);
        productTemplateNamesArr[1] = TemplatesLibrary.PRODUCT_TEMPLATE_PREFIX + timestamp;

        GeneralCondition generalCondition1 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND, IntOperators.IS_GREATER_THAN, "150");

        DealCase case1 = new DealCase();
        case1.setConditions(com.google.common.collect.Lists.newArrayList(generalCondition1));
        case1.clickAdvancedDiscount();

        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(selectItemsByItemCode("1111"));
        occurrencesCondition.setConditionValue("2");

        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickSpecificItemsFromBundleButton();
        selectBundle.clickApplyButton();

        case1.setOccurrencesConditions(Lists.newArrayList(occurrencesCondition));
        case1.setActions(Lists.newArrayList(buildMakeItemFreeAction(selectBundle)));

        DealCase case2 = new DealCase();
        case2.clickSpecificItemsDiscount();
        GeneralCondition generalCondition2 = CasesConditionsBuilder.buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND, IntOperators.IS_LESS_THAN_OR_EQUAL_TO, "150");
        case2.setConditions(Lists.newArrayList(generalCondition2));
        case2.setActions(Lists.newArrayList(buildAmountOffDiscount("10", selectItemsByItemCode("1111"))));

        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, MULTIPLE_CASES_DEAL + timestamp, "product deal template " + vertical.name());
        Activity activity2 = dealBuilder.buildMultipleCasesDeal(box, deal, Lists.newArrayList(), Lists.newArrayList(case1, case2));
        activity2.clickSaveAsProductTemplate(saveProductTemplate(TemplatesLibrary.PRODUCT_TEMPLATE_PREFIX + timestamp));
        productTemplates.add(activity2);

        //template3
        timestamp = String.valueOf(System.currentTimeMillis());
        templateNames.add(TemplatesLibrary.PRODUCT_TEMPLATE_PREFIX + timestamp);
        productTemplateNamesArr[2] = TemplatesLibrary.PRODUCT_TEMPLATE_PREFIX + timestamp;

        OccurrencesCondition occurrencesCondition1 = new OccurrencesCondition();
        occurrencesCondition1.setConditionValue("3");
        MakeItemsFreeAction makeItemsFreeAction = new MakeItemsFreeAction();
        makeItemsFreeAction.clickMakeItemsFree();
        makeItemsFreeAction.clickLimitDiscountExpand();
        makeItemsFreeAction.clickLimitDiscountTimesCheckbox();

        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ADVANCED_DEAL + timestamp, "product deal template " + vertical.name());
        Activity activity3 = dealBuilder.buildAdvancedDiscountDeal(box, deal, Lists.newArrayList(), Lists.newArrayList(occurrencesCondition1), Lists.newArrayList(makeItemsFreeAction));
        activity3.clickSaveAsProductTemplate(saveProductTemplate(TemplatesLibrary.PRODUCT_TEMPLATE_PREFIX + timestamp));
        productTemplates.add(activity3);

        LoyaltyService.initProductTemplates(productTemplates);
        return templateNames;
    }

    private static SaveProductTemplate saveProductTemplate(String name) throws Exception {
        SaveProductTemplate saveProductTemplate = new SaveProductTemplate();
        saveProductTemplate.setName(name);
        saveProductTemplate.setDescription("description");
        saveProductTemplate.setCategory(TemplateCategories.Summer.toString());
        return saveProductTemplate;
    }
}