package hub2_0.templates;

import com.google.common.collect.Lists;
import hub.base.BaseService;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.member.NewMember;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Deal;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.templates.TemplatesLibrary;
import hub.hub2_0.common.actions.dealActions.*;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.enums.ItemOperation;
import hub.hub2_0.common.enums.ActivityStatus;
import hub.hub2_0.common.enums.ActivityType;
import hub.hub2_0.common.items.SelectBundle;
import hub.hub2_0.services.loyalty.ActivityService;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.utils.HubMemberCreator;
import integration.base.BaseHub2DealTest;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.v2_8.common.Item;

public class DealTemplatesTest extends BaseHub2DealTest {

    private static Box box;
    private static String templateName;
    private static NewMember newMember;

    @BeforeClass
    public static void createNewBox() throws Exception {
        timestamp = String.valueOf(System.currentTimeMillis());
        newMember = HubMemberCreator.createMember(timestamp, AUTO_USER_PREFIX, AUTO_LAST_PREFIX);
        box = dealBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            templateName = "DealTemplate_" + timestamp;
        }
    };

    @Test
    public void testTemplateStartFromScratchAndSaveToTemplate() throws Exception {

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN);

        createEntireTicketDiscountDealTemplate(box, Lists.newArrayList(condition), Lists.newArrayList(buildAmountOffDiscountOnEntireTicket(null)), templateName);

        MadeAPurchaseCondition condition2 = new MadeAPurchaseCondition();
        condition2.setConditionValue("50");
        AmountOffDiscountEntireTicketAction amountOffDiscountEntireTicketAction = new AmountOffDiscountEntireTicketAction();
        amountOffDiscountEntireTicketAction.setDiscountAmount("10");
        createEntireTicketDealFromTemplate(box, "deal" + timestamp, templateName, Lists.newArrayList(condition2), Lists.newArrayList(amountOffDiscountEntireTicketAction));

        Item item1 = createItem("1111", "Prod1", "123", "Dep1", 7000, 2);
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1), "7000", "-1000");
    }

    @Test
    @Category({Hub2Regression.class})
    public void testTemplateEditTemplateNameAndDescription() throws Exception {

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(InputOperators.IS_ONE_OF);

        PercentOffDiscountAction percentOffDiscountAction = new PercentOffDiscountAction();
        percentOffDiscountAction.clickPercentOffDiscount();
        percentOffDiscountAction.setDiscountPercentage("20");
        percentOffDiscountAction.clickLimitDiscountExpand();
        percentOffDiscountAction.clickLimitDiscountAmountCheckbox();

        MakeItemsFreeAction makeItemsFreeAction = buildMakeItemFreeAction(dealBuilder.selectItemsByItemCode("4455"));

        createSpecificItemsDiscountDealTemplate(box, Lists.newArrayList(condition), Lists.newArrayList(percentOffDiscountAction, makeItemsFreeAction), templateName);

        // Back in Loyalty tab.
        LoyaltyService.clickAddActivity(new Deal());
        String updateTemplateName = templateName + "_update";
        TemplatesLibrary.editTemplateName(templateName, updateTemplateName, "This template name and description were updated.");
        TemplatesLibrary.clickBack();

        MadeAPurchaseCondition condition1 = new MadeAPurchaseCondition();
        condition1.setConditionValue("1214");

        PercentOffDiscountAction percentOffDiscountAction1 = new PercentOffDiscountAction();
        percentOffDiscountAction1.setSelectItems(dealBuilder.selectItemsByItemCode("1111", "4455"));
        percentOffDiscountAction1.setLimitDiscountAmountValue("20");

        createSpecificItemsDealFromTemplate(box, "deal_" + timestamp, updateTemplateName, Lists.newArrayList(condition1), Lists.newArrayList(percentOffDiscountAction1));

        Item item1 = createItem("1111", "item1", "123", "Dep1", 8000, 2);
        Item item2 = createItem("4455", "item2", "123", "Dep1", 5000, 1);
        getMemberBenefitsWithPodIdAndBranch(newMember.getPhoneNumber(), "13000", Lists.newArrayList(item1, item2), "10", "1214", "-7000");
    }

    @Test
    public void testTemplateLoadAnother() throws Exception {

        String templateName1 = templateName + "_1";
        String templateName2 = templateName + "_2";
        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_EXACTLY);

        createSpecificItemsDiscountDealTemplate(box, Lists.newArrayList(condition), Lists.newArrayList(buildSetSpecialPriceAction("10", dealBuilder.selectItemsByItemCode("1111"))), templateName1);

        MadeAPurchaseCondition condition2 = new MadeAPurchaseCondition();
        condition2.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO);


        AmountOffDiscountAction amountOffDiscountAction = new AmountOffDiscountAction();
        amountOffDiscountAction.clickAmountOffDiscount();
        amountOffDiscountAction.setDiscountAmount("20");
        amountOffDiscountAction.clickLimitDiscountExpand();
        amountOffDiscountAction.clickLimitDiscountTimesCheckbox();

        createAdvancedDiscountDealTemplate(box, Lists.newArrayList(condition2), Lists.newArrayList(), Lists.newArrayList(amountOffDiscountAction), templateName2);


        String activityName = "dealTemplate" + timestamp;
        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, activityName, "Test for re-load deal template.");
        Activity activity = dealBuilder.buildSpecificItemsDiscountDealEditMode(box, deal, Lists.newArrayList(), Lists.newArrayList());
        activity.clickLoadAnother();
        //execute the selenium job - load from template and click load another - back to Templates library.
        LoyaltyService.createDealFromTemplateInBox(activity, templateName1);

        MadeAPurchaseCondition condition3 = new MadeAPurchaseCondition();
        condition3.setConditionValue("400");


        AmountOffDiscountAction amountOffDiscountAction2 = new AmountOffDiscountAction();
        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickEntireRecurringBundleButton();
        selectBundle.clickApplyButton();
        amountOffDiscountAction2.setSelectItems(selectBundle);
        amountOffDiscountAction2.setLimitDiscountTimesValue("2");

        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("1111"));
        occurrencesCondition.setConditionValue("2");


        Deal deal2 = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ActivityStatus.Active, activityName);
        Activity activity2 = dealBuilder.buildAdvancedDiscountDealEDitMode(box, deal2, Lists.newArrayList(condition3), Lists.newArrayList(occurrencesCondition), Lists.newArrayList(amountOffDiscountAction2));

        LoyaltyService.selectTemplateAndCreateActivity(activity2, templateName2);

        Item item1 = createItem("1111", "item1", "123", "Dep1", 35000, 7);
        Item item2 = createItem("4455", "item2", "123", "Dep1", 5000, 1);
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1, item2), "40000", "-4000");

        //Update activity name for @After method that stops the activity.
        deal.setActivityName(deal2.getActivityName());
    }

    @Test
    public void testSaveTemplateFromRule() throws Exception {

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.VALUE_OF_SPECIFIC_ITEMS)
                .setOperatorKey(IntOperators.IS_LESS_THAN_OR_EQUAL_TO)
                .setConditionValue("100")
                .setSelectItems(dealBuilder.selectItemsByDepartmentCode("5555"));

        createSpecificItemsDiscountDeal(box, Lists.newArrayList(condition), Lists.newArrayList(buildSetSpecialPriceAction("30", dealBuilder.selectItemsByDepartmentCode("5555"))));

        // edit deal and save to template.
        LoyaltyService.clickOnActivity(deal.getActivityName());
        Deal deal2 = new Deal();
        deal2.clickSaveAsTemplateAndSave(templateName, "This template created from deal " + deal.getActivityName());
        BaseService.fillSeleniumObject(deal2);
        deal2.clickCancel();
        timestamp = String.valueOf(System.currentTimeMillis());

        ActivityService.executeOperation(deal.getActivityName(), ItemOperation.STOP);

        MadeAPurchaseCondition condition1 = new MadeAPurchaseCondition();
        condition1.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("200");

        createSpecificItemsDealFromTemplate(box, "", templateName, Lists.newArrayList(new MadeAPurchaseCondition(), condition1), Lists.newArrayList());
        Item item1 = createItem("1111", "item1", "5555", "Dep1", 9000, 2);
        Item item2 = createItem("2222", "item2", "8888", "Dep2", 5000, 1);
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1), "9000", null);
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1, item2), "24000", "-3000");
    }

    @Test
    public void testSaveNewTemplateFromExistTemplate() throws Exception {

        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction = buildPercentOffDiscountOnEntireTicket(null);
        percentOffDiscountEntireTicketAction.clickExcludeItemsExpand();
        percentOffDiscountEntireTicketAction.clickExcludeItemsCheckbox();

        createEntireTicketDiscountDealTemplate(box, Lists.newArrayList(), Lists.newArrayList(percentOffDiscountEntireTicketAction), templateName);

        String newTemplateName = templateName + "updated";
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(InputOperators.IS_NOT_ONE_OF)
                .setConditionValue("1214");

        Deal deal_temp = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, "deal_" + timestamp, "Test for re-load deal template.");
        Activity activity_temp = dealBuilder.buildEntireTicketDiscountDealEditMode(box, deal_temp, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList());
        activity_temp.clickSaveAsTemplateAndSaveAsNew(newTemplateName, "this template was saved from template.");
        LoyaltyService.createDealFromTemplateInBox(activity_temp, templateName);
        activity_temp.clickCancel();

        PercentOffDiscountEntireTicketAction percentOffDiscountEntireTicketAction2 = new PercentOffDiscountEntireTicketAction();
        percentOffDiscountEntireTicketAction2.setDiscountPercentage("20");
        percentOffDiscountEntireTicketAction2.setSelectItemsToExclude(dealBuilder.selectItemsByItemCode("2222"));

        deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, ActivityStatus.Active, "newDeal_");
        Activity activity = dealBuilder.buildEntireTicketDiscountDealEditMode(box, deal, Lists.newArrayList(), Lists.newArrayList(percentOffDiscountEntireTicketAction2));
        LoyaltyService.createDealFromTemplateInBox(activity, templateName);

        Item item1 = createItem("1111", "item1", "5555", "Dep1", 18000, 2);
        Item item2 = createItem("2222", "item2", "8888", "Dep2", 10000, 1);
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1, item2), "28000", "-3600");
    }

    @Test
    public void testSaveAndReplaceTemplate() throws Exception {

        OccurrencesCondition occurrencesCondition = new OccurrencesCondition();
        occurrencesCondition.setSelectItemsDialog(dealBuilder.selectItemsByItemCode("1111"));

        createAdvancedDiscountDealTemplate(box, Lists.newArrayList(), Lists.newArrayList(occurrencesCondition), Lists.newArrayList(buildMakeItemFreeAction(null)), templateName);

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("100");
        // Save and replace template.
        Deal deal_temp = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, "deal_" + timestamp, "Test for re-load deal template.");
        Activity activity = dealBuilder.buildAdvancedDiscountDealEDitMode(box, deal_temp, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(), Lists.newArrayList());
        activity.clickSaveAsTemplateAndReplace(null, "This template was updated");
        LoyaltyService.createDealFromTemplateInBox(activity, templateName);
        activity.clickCancel();

        OccurrencesCondition occurrencesCondition1 = new OccurrencesCondition();
        occurrencesCondition1.setConditionValue("2");

        MakeItemsFreeAction makeItemsFreeAction = new MakeItemsFreeAction();
        SelectBundle selectBundle = new SelectBundle();
        selectBundle.clickSpecificItemsFromBundleButton();
        selectBundle.clickApplyButton();
        makeItemsFreeAction.setSelectItems(selectBundle);

        createAdvancedDealFromTemplate(box, "deal_" + timestamp, templateName, Lists.newArrayList(), Lists.newArrayList(occurrencesCondition1), Lists.newArrayList(makeItemsFreeAction));

        Item item1 = createItem("1111", "item1", "Dep1", "Dep1", 25000, 5);
        Item item2 = createItem("2222", "item2", "Dep2", "Dep2", 10000, 2);
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1, item2), "35000", "-10000");
    }


//    @Test
    public void testSaveDraftFromTemplate() throws Exception {
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.TOTAL_SPEND)
                .setOperatorKey(IntOperators.IS_GREATER_THAN)
                .setConditionValue("100");

        createSpecificItemsDiscountDeal(box, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(buildMakeItemFreeAction(dealBuilder.selectItemsByItemCode("1111"))));

        LoyaltyService.clickOnActivity(deal.getActivityName());
        Deal templateDeal = new Deal();
        templateDeal.clickSaveAsTemplateAndSave(templateName, "some description");
        BaseService.fillSeleniumObject(templateDeal);
        templateDeal.clickCancel();
        ActivityService.executeOperation(deal.getActivityName(), ItemOperation.STOP);

        Deal saveToDraftDeal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, "draftDeal_" + timestamp, "deal description");
        Activity activity = dealBuilder.buildSpecificItemsDiscountDeal(box, saveToDraftDeal, Lists.newArrayList(), Lists.newArrayList());
        activity.clickSaveActivityAsDraftBtn();
        LoyaltyService.createDealFromTemplateInBox(activity, templateName);
        LoyaltyService.clickOnActivity(saveToDraftDeal.getActivityName());
        Deal dealFromDraft = new Deal();
        dealFromDraft.clickPublishActivityBtn();
        BaseService.fillSeleniumObject(dealFromDraft);

        Item item1 = createItem("1111", "item1", "Dep1", "Dep1", 3000, 1);
        Item item2 = createItem("2222", "item2", "Dep2", "Dep2", 8000, 2);
        getMemberBenefitsAndVerifyDiscount(newMember, Lists.newArrayList(item1, item2), "11000", "-3000");
    }

}
