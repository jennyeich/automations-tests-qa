package hub2_0.templates;

import common.BaseTest;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.common.PermissionLevel;
import hub.common.objects.common.Tags;
import hub.common.objects.operations.UserPermissions;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.services.Navigator;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.templates.TemplateVerticals;
import hub.hub2_0.basePages.loyalty.templates.TemplatesLibrary;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.AcpService;
import hub2_0.utils.MongoUtils;
import hub2_0.utils.ProductTemplateUtility;
import integration.base.BaseRuleHub2Test;
import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;


public class ProductTemplatePerVerticalTest extends BaseRuleHub2Test {

    private static String resloveLocationId;
    private static Box box;
    private static String currentBusinessVertical;

    public static final Logger log4j = Logger.getLogger(BaseTest.class);

    @BeforeClass
    public static void createNewBox() throws Exception {
        resloveLocationId = resolveLocationId(locationId);
        currentBusinessVertical = ProductTemplateUtility.setBusinessVerticalToService(resloveLocationId);
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);

        LoyaltyService.initProductTemplates(box, ActivityTrigger.MadeAPurchase);
    }

    @AfterClass
    public static void deleteTemplates() throws Exception {
        MongoUtils.archiveRuleProductTemplates(mongoDB);
    }


    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            tag = "tag_" + timestamp;
        }
    };


    @Test
    public void testPermissionTagForTemplateProductCreation() throws Exception {
        //delete the tag if exist
        AcpService.checkUserPermissionTags(UserPermissions.HUB2_TEMPLATE_PRODUCT);
        Navigator.refresh();

        //check if the  'Save as product Template' is not shown
        LoyaltyService.navigateToNewRule();
        Assert.assertFalse("Element should not exist " + Rule.SAVE_AS_PRODUCT_TEMPLATE.toString(), LoyaltyService.isElementExist(Rule.SAVE_AS_PRODUCT_TEMPLATE.toString()));


        AcpService.setPermissionTag(Tags.HUB2_TEMPLATE_PRODUCT.toString(), PermissionLevel.ALLOW.toString());
        Navigator.refresh();
        LoyaltyService.navigateToNewRule();
        Assert.assertTrue("Element should exist " + Rule.SAVE_AS_PRODUCT_TEMPLATE.toString(), LoyaltyService.isElementExist(Rule.SAVE_AS_PRODUCT_TEMPLATE.toString()));
    }

    /**
     * Define template for each vertical + 1 template that contains all verticals.
     * Check the vertical for the business in App Settings.
     * set permission tag for <Hub2_product_template> to disallowed.
     * Change vertical for the business - repeat this step for each vertical--> User can only see the template with related vertical or the template that contains all verticals.
     *
     * @throws Exception
     */
    @ru.yandex.qatools.allure.annotations.Description("Test Rail C10742: User without permission tag for template product")
    @Test
    @Category({Hub2Regression.class})
    public void testPermissionTagForTemplateProductDisallowShowOnlyBusinessVertical() throws Exception {

        AcpService.setPermissionTag(Tags.HUB2_TEMPLATE_PRODUCT.toString(), PermissionLevel.DISALLOW.toString());


        int verticals = TemplateVerticals.values().length;
        for (int i = 0; i < verticals; i++) {
            TemplateVerticals vertical = TemplateVerticals.values()[i];
            log4j.info("Checking vertical : " + vertical);
            //set the business to the next vertical and check if templates for this vertical appears
            BaseTest.sqlDB.setHub2BusinessVertical(resloveLocationId, vertical.getDBValue());
            LoyaltyService.openRuleTemplateLibrary();
            boolean templateExist = TemplatesLibrary.findTemplate(vertical.name());
            Assert.assertTrue("Business should see the template regarding its vertical: " + vertical.name(), templateExist);
            templateExist = TemplatesLibrary.findTemplate(TemplatesLibrary.PRODUCT_TEMPLATE_ALL_VERTICALS);
            Assert.assertTrue("Business should see the template regarding all verticals: " + TemplatesLibrary.PRODUCT_TEMPLATE_ALL_VERTICALS, templateExist);
            String differentVertical = ProductTemplateUtility.getDifferentVertical(vertical.name());
            templateExist = TemplatesLibrary.findTemplate(differentVertical);
            Assert.assertFalse("Business should not see  template which is regarding  vertical: " + differentVertical, templateExist);
            Navigator.acpTab();
        }
    }


    @ru.yandex.qatools.allure.annotations.Description("Test Rail C10741:When permission tag <Hub2_product_template> is set to allowed : Check that can edit template from template manager (3 dots) and from rule." +
            "When permission tag <Hub2_product_template> is set to disallowed : Check the user cannot edit template (not from templates manager(3 dots))")
    @Test
    public void testPermissionTagForEditDeleteTemplateProduct() throws Exception {
        //delete the tag if exist
        AcpService.checkUserPermissionTags(UserPermissions.HUB2_TEMPLATE_PRODUCT);
        Navigator.refresh();
        AcpService.setPermissionTag(Tags.HUB2_TEMPLATE_PRODUCT.toString(), PermissionLevel.DISALLOW.toString());

        currentBusinessVertical = TemplateVerticals.getNameOfVertical(currentBusinessVertical);
        LoyaltyService.openRuleTemplateLibrary();
        boolean templateExist = TemplatesLibrary.findTemplate(currentBusinessVertical);
        Assert.assertTrue("Template containing the name " + currentBusinessVertical + " should exist.", templateExist);

        Assert.assertFalse("Edit/Delete option should not be allowed with the current permission tag", TemplatesLibrary.isEditOrDeleteAllowed(currentBusinessVertical));

        AcpService.checkUserPermissionTags(UserPermissions.HUB2_TEMPLATE_PRODUCT);
        Navigator.refresh();
        AcpService.setPermissionTag(Tags.HUB2_TEMPLATE_PRODUCT.toString(), PermissionLevel.ALLOW.toString());

        LoyaltyService.openRuleTemplateLibrary();
        templateExist = TemplatesLibrary.findTemplate(currentBusinessVertical);
        Assert.assertTrue("Template containing the name " + currentBusinessVertical + " should exist.", templateExist);

        Assert.assertTrue("Edit/Delete option should be allowed with the current permission tag", TemplatesLibrary.isEditOrDeleteAllowed(currentBusinessVertical));
    }


    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.MadeAPurchase;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.MadeAPurchase.toString();
    }
}
