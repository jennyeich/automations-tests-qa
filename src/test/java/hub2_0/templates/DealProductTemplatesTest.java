package hub2_0.templates;

import com.beust.jcommander.internal.Lists;
import common.BaseTest;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.common.PermissionLevel;
import hub.common.objects.common.Tags;
import hub.common.objects.operations.UserPermissions;
import hub.hub1_0.services.Navigator;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Deal;
import hub.hub2_0.basePages.loyalty.templates.SaveProductTemplate;
import hub.hub2_0.basePages.loyalty.templates.TemplateCategories;
import hub.hub2_0.basePages.loyalty.templates.TemplateVerticals;
import hub.hub2_0.basePages.loyalty.templates.TemplatesLibrary;
import hub.hub2_0.common.actions.dealActions.SendCodeToPOSAction;
import hub.hub2_0.common.enums.ActivityType;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.AcpService;
import hub2_0.utils.MongoUtils;
import hub2_0.utils.ProductTemplateUtility;
import integration.base.BaseHub2DealTest;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.ArrayList;
import java.util.List;

public class DealProductTemplatesTest extends BaseHub2DealTest {

    private static String resolveLocationId;
    private static String currentBusinessVertical;

    @BeforeClass
    public static void createNewBox() throws Exception {
        resolveLocationId = resolveLocationId(locationId);
        currentBusinessVertical = ProductTemplateUtility.setBusinessVerticalToService(resolveLocationId);
        Box box = dealBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);

        String timestamp;
        List<Activity> productTemplates = new ArrayList<>();
        int verticalSize = TemplateVerticals.values().length;
        TemplateVerticals vertical = null;
        for (int i = 0; i <= verticalSize; i++) {
            if (i < verticalSize) {
                vertical = TemplateVerticals.values()[i];
            }

            timestamp = String.valueOf(System.currentTimeMillis());
            deal = (Deal) dealBuilder.getConcreteActivity(box, ActivityType.Deal, "EntireTicket_" + timestamp, "product deal template " + vertical.name());
            Activity activity = dealBuilder.buildEntireTicketDiscountActivity(box, deal, Lists.newArrayList(), Lists.newArrayList(buildAmountOffDiscountOnEntireTicket("10"),
                    buildSendCodeToPOS(SendCodeToPOSAction.CodeType.ITEM_CODE, timestamp)));
            SaveProductTemplate productTemplate = new SaveProductTemplate();
            if (i < verticalSize) {
                productTemplate.setName(TemplatesLibrary.PRODUCT_TEMPLATE_PREFIX + vertical.name());
                productTemplate.setDescription("description_" + vertical.name());
                productTemplate.setVerticals(vertical.getValue());
            } else {
                productTemplate.setName(TemplatesLibrary.PRODUCT_TEMPLATE_ALL_VERTICALS);
                productTemplate.setDescription("description_All verticals");
                for (int j = 0; j < verticalSize; j++) {
                    productTemplate.addVertical(TemplateVerticals.values()[j].getValue());
                }
            }

            if ((i % 2) == 0)
                productTemplate.setCategory(TemplateCategories.Summer.toString());
            else
                productTemplate.setCategory(TemplateCategories.Winter.toString());

            activity.clickSaveAsProductTemplate(productTemplate);
            productTemplates.add(activity);
        }
        LoyaltyService.initProductTemplates(productTemplates);
    }

    @AfterClass
    public static void deleteTemplates() throws Exception {
        MongoUtils.archiveDealProductTemplates(mongoDB);
    }

    @Test
    public void testPermissionTagForTemplateProductCreation() throws Exception {
        //delete the tag if exist
        AcpService.checkUserPermissionTags(UserPermissions.HUB2_TEMPLATE_PRODUCT);
        Navigator.refresh();

        //check if the  'Save as product Template' is not shown in deal.
        LoyaltyService.navigateToNewDeal();
        Assert.assertFalse("Element 'Save as product template' should not exist in deal ", LoyaltyService.isElementExist(Deal.SAVE_AS_PRODUCT_TEMPLATE.toString()));

        AcpService.setPermissionTag(Tags.HUB2_TEMPLATE_PRODUCT.toString(), PermissionLevel.ALLOW.toString());
        Navigator.refresh();

        LoyaltyService.navigateToNewDeal();
        Assert.assertTrue("Element 'Save as product template' should exist in deal ", LoyaltyService.isElementExist(Deal.SAVE_AS_PRODUCT_TEMPLATE.toString()));
    }

    @Test
    @Category({Hub2Regression.class})
    public void testPermissionTagForTemplateProductDisallowShowOnlyBusinessVertical() throws Exception {

        AcpService.setPermissionTag(Tags.HUB2_TEMPLATE_PRODUCT.toString(), PermissionLevel.DISALLOW.toString());

        int verticals = TemplateVerticals.values().length;
        for (int i = 0; i < verticals; i++) {
            TemplateVerticals vertical = TemplateVerticals.values()[i];
            log4j.info("Checking vertical : " + vertical);
            //set the business to the next vertical and check if templates for this vertical appears
            BaseTest.sqlDB.setHub2BusinessVertical(resolveLocationId, vertical.getDBValue());
            LoyaltyService.openDealTemplateLibrary();
            boolean templateExist = TemplatesLibrary.findTemplate(vertical.name());
            Assert.assertTrue("Business should see the template regarding its vertical: " + vertical.name(), templateExist);
            templateExist = TemplatesLibrary.findTemplate(TemplatesLibrary.PRODUCT_TEMPLATE_ALL_VERTICALS);
            Assert.assertTrue("Business should see the template regarding all verticals: " + TemplatesLibrary.PRODUCT_TEMPLATE_ALL_VERTICALS, templateExist);
            String differentVertical = ProductTemplateUtility.getDifferentVertical(vertical.name());
            templateExist = TemplatesLibrary.findTemplate(differentVertical);
            Assert.assertFalse("Business should not see  template which is regarding  vertical: " + differentVertical, templateExist);
            Navigator.acpTab();
        }
    }

    @Test
    public void testPermissionTagForEditDeleteTemplateProduct() throws Exception {
        //delete the tag if exist
        AcpService.checkUserPermissionTags(UserPermissions.HUB2_TEMPLATE_PRODUCT);
        Navigator.refresh();
        AcpService.setPermissionTag(Tags.HUB2_TEMPLATE_PRODUCT.toString(), PermissionLevel.DISALLOW.toString());

        currentBusinessVertical = TemplateVerticals.getNameOfVertical(currentBusinessVertical);
        LoyaltyService.openDealTemplateLibrary();
        boolean templateExist = TemplatesLibrary.findTemplate(currentBusinessVertical);
        Assert.assertTrue("Template containing the name " + currentBusinessVertical + " should exist.", templateExist);

        Assert.assertFalse("Edit/Delete option should not be allowed with the current permission tag", TemplatesLibrary.isEditOrDeleteAllowed(currentBusinessVertical));

        AcpService.checkUserPermissionTags(UserPermissions.HUB2_TEMPLATE_PRODUCT);
        Navigator.refresh();
        AcpService.setPermissionTag(Tags.HUB2_TEMPLATE_PRODUCT.toString(), PermissionLevel.ALLOW.toString());

        LoyaltyService.openDealTemplateLibrary();
        templateExist = TemplatesLibrary.findTemplate(currentBusinessVertical);
        Assert.assertTrue("Template containing the name " + currentBusinessVertical + " should exist.", templateExist);

        Assert.assertTrue("Edit/Delete option should be allowed with the current permission tag", TemplatesLibrary.isEditOrDeleteAllowed(currentBusinessVertical));
    }

}