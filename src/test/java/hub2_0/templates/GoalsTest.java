package hub2_0.templates;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;
import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import common.BaseTest;
import common.LocalMongoDB;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.common.objects.common.PermissionLevel;
import hub.common.objects.common.Tags;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AmountType;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.services.Navigator;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.GoalWizard;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.cases.RuleCase;
import hub.hub2_0.basePages.loyalty.templates.SaveProductTemplate;
import hub.hub2_0.basePages.loyalty.templates.TemplateVerticals;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.builders.CasesConditionsBuilder;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.conditions.AssetCondition;
import hub.hub2_0.common.conditions.PointsCondition;
import hub.hub2_0.common.conditions.cases.GeneralCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.ListOperators;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.AcpService;
import hub.services.member.MembersService;
import hub.utils.HubAssetsCreator;
import hub2_0.utils.DateTimeUtility;
import hub2_0.utils.MongoUtils;
import integration.base.BaseRuleHub2Test;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import server.utils.ServerHelper;

import java.util.*;

/**
 * Hub 2.0 - Goal Wizard (CNP-11627)
 */
public class GoalsTest extends BaseRuleHub2Test {

    private static String resloveLocationId;
    private static Box box;
    private static Map<String,ProductTemplateDO> productTemplateDOMap;
    private static Object[]  productTemplateNamesArr;
    private static String goal;
    private static final String GOAL_PREFIX = "Goal_";
    private static final String NUM_OF_POINTS_CASE_1 = "200";
    private static final String NUM_OF_POINTS_CASE_2 = "300";
    private static final String TAG_CASE2 = "TAG_2";

    private static HubAssetsCreator hubAssetsCreator = new HubAssetsCreator();

    @BeforeClass
    public static void initGoals() throws Exception {

        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
        resloveLocationId = resolveLocationId(locationId);

        BaseTest.sqlDB.setHub2BusinessVertical(resloveLocationId, TemplateVerticals.Services.getDBValue());
        AcpService.setPermissionTag(Tags.HUB2_TEMPLATE_PRODUCT.toString(), PermissionLevel.ALLOW.toString());
        if(smartGift == null) {
            //create assets
            try {
                smartGift = hubAssetsCreator.createSmartGift(String.valueOf(System.currentTimeMillis()));

            } catch (Exception e) {
                log4j.error(e);
                Assert.fail("error while creating asset");
            }
        }
        initProductTemplates(box);
        String goalName = GOAL_PREFIX + DateTimeUtility.randomInt(1,10);
        MongoUtils.createGoal( BaseTest.mongoDB,resloveLocationId,goalName,productTemplateDOMap.keySet(), Collections.emptySet());
        List<String> list = BaseTest.mongoDB.queryCollectionByProperty(LocalMongoDB.GOALS,goalName,"name");
        if(list.size() > 0)//get the first goal
            goal = list.get(0);
    }

    @AfterClass
    public static void deleteTemplates() throws Exception {
        MongoUtils.archiveRuleProductTemplates(mongoDB);
        MongoUtils.deleteGoal( mongoDB,goal);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher(){

        @Override
        protected void starting(Description description){
            timestamp = String.valueOf(System.currentTimeMillis());
        }
    };



    @Test
    @ru.yandex.qatools.allure.annotations.Description("C11031 : Create a rule from a goal and execute it." +
            "Create Join club trigger with tag member action.")
    @Category({Hub2Regression.class})
    public void testGoalWithCampaignJoinClub() throws Exception {

        GoalWizard goalWizard = new GoalWizard();
        String productTemplateName = (String)productTemplateNamesArr[0];
        Rule rule = new Rule();
        ProductTemplateDO pt = createActivityFromGoal(goalWizard, productTemplateName,rule);

        hubMemberCreator.createMember(timestamp,AUTO_USER_PREFIX ,AUTO_LAST_PREFIX );
        key = hubMemberCreator.getMemberKeyToUserKey();

        tag = pt.getTag();
        checkAutomationRunAndTagMember();


    }



    //@Test
    @ru.yandex.qatools.allure.annotations.Description("C11030 : Create a rule from a goal and Edit it" +
            "Create Make a purchase trigger with send benefit action empty.Save as product rule.When creating the rule from goal, edit it and select the benefits." +
            "Run submit purchase to trigger rule.")
    public void testGoalWithCampaignMakeAPurchase() throws Exception {

        GoalWizard goalWizard = new GoalWizard();

        String productTemplateName = (String)productTemplateNamesArr[4];
        Rule rule = new Rule();
        rule.setActions(Lists.newArrayList(ActionsBuilder.buildSendBenefitAction(smartGift.getTitle())));
        ProductTemplateDO pt = createActivityFromGoal(goalWizard, productTemplateName,rule);

        //Create member
        NewMember newMember = createNewMember();
        String transactionId = UUID.randomUUID().toString();
        //perform submit purchase
        performAnActionSubmitPurchase(newMember, "2000", Lists.newArrayList("testGoal"), transactionId);
        //DataStore check
        checkDataStoreUpdatedWithAsset(transactionId);

        Navigator.refresh();
        Assert.assertTrue("The member did not get the gift sent by the automation in the hub,but it appears in the dataStore.", MembersService.isGiftExists(smartGift.getTitle()));
        log4j.info("member received the gift");
        log4j.info(smartGift.getTitle() + " was added to the member ");

    }

    @Test
    @ru.yandex.qatools.allure.annotations.Description("C11031 : Create a rule from a goal and execute it." +
            "Create Redeem trigger with add points action.")
    public void testGoalWithCampaignRedeem() throws Exception {

        GoalWizard goalWizard = new GoalWizard();

        String productTemplateName = (String)productTemplateNamesArr[1];
        Rule rule = new Rule();
        ProductTemplateDO pt = createActivityFromGoal(goalWizard, productTemplateName,rule);

        createMemberAssignBenefitAndRedeem();

        checkDataStoreUpdatedWithPointsByRuleID(pt.getNumOfPointsToAccumulate(), AmountType.Points);

    }

    @Test
    @ru.yandex.qatools.allure.annotations.Description("C11032 : Create a rule  with global conditions from a goal and execute it." +
            "Create Redeem trigger with add points action.")
    public void testGoalWithCampaignUpdateMembershipWithGlobalCondition() throws Exception {

        GoalWizard goalWizard = new GoalWizard();

        NewMember newMember = createNewMember();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();

        String productTemplateName = (String)productTemplateNamesArr[2];
        Rule rule = new Rule();
        ProductTemplateDO pt = createActivityFromGoal(goalWizard, productTemplateName,rule);

        performUpdateMemberDetails(newMember);

        checkDataStoreUpdatedWithPointsByRuleID(pt.getNumOfPointsToAccumulate(), AmountType.Points);

    }

    @Test
    @ru.yandex.qatools.allure.annotations.Description("C11033 : Create a rule from a goal and execute it." +
            "Create Receives Points trigger with 2 cases  - condition and tag member action." +
            "Run add points to trigger rule that matches case2.")
    public void testGoalWithCampaignReceivesPointsWithSplitCases() throws Exception{
        GoalWizard goalWizard = new GoalWizard();

        String productTemplateName = (String)productTemplateNamesArr[3];
        Rule rule = new Rule();
        ProductTemplateDO pt = createActivityFromGoal(goalWizard, productTemplateName,rule);

        //Create member
        createNewMember();
        String givenPoints = NUM_OF_POINTS_CASE_2;
        tag = pt.getTag();
        //add points to member
        MembersService.performSmartActionAddPoints(givenPoints,timestamp);
        checkAutomationRunAndTagMember();

    }

    private ProductTemplateDO createActivityFromGoal(GoalWizard goalWizard, String productTemplateName,Rule rule) throws Exception {
        goalWizard.clickOnGoal(goal, productTemplateName);
        LoyaltyService.createActivityFromGoal(goalWizard);
        ProductTemplateDO pt = productTemplateDOMap.get(productTemplateName);
        String activity = LoyaltyService.openDraftActivity(pt.getActivityName());
        log4j.info("Activity created: " + activity);
        rule.clickPublishActivityBtn();
        LoyaltyService.publishDaftActivity(rule);
        automationRuleId = LoyaltyService.getRuleID(activity);
        return pt;
    }

    //Check that no user action was created for this action
    private void checkDataStoreWithCorrectTag(String actionName,String transactionId,String branchId,String tag) throws Exception {
        //give the data store time to update
        Thread.sleep(5000);
        log4j.info("check DataStore not updated with :" + actionName);
        Query q = new Query("UserAction");
        q.setFilter(new Query.CompositeFilter(Query.CompositeFilterOperator.AND, Arrays.asList(
                new Query.FilterPredicate("Action", Query.FilterOperator.EQUAL, actionName),
                new Query.FilterPredicate("BranchID", Query.FilterOperator.EQUAL, branchId),
                new Query.FilterPredicate("TransactionID", Query.FilterOperator.EQUAL, transactionId))));

        List<Entity> res = ServerHelper.queryWithWait("Must find one UserAction with the branchId: " + branchId,
                q , 1, dataStore);
        Text t = ((Text) res.get(0).getProperty("Data"));
        log4j.info( "ExportData contains the following data : " + t.getValue());
        Assert.assertTrue("Data must contain the tag: " + tag, t.getValue().contains(tag));

    }

    private void createMemberAssignBenefitAndRedeem() throws Exception {
        //Create member
        NewMember member = createNewMember();
        String membershipKey = MembersService.getMembershipKey();
        String userKey = MembersService.getUserKey();
        //Get member userKey and turn into User_KEY
        key = hubMemberCreator.getMemberKeyToUserKey();
        //Send gift to member
        MembersService.performSmartActionSendAnAsset(smartGift.getTitle(),timestamp);
        Assert.assertTrue("Member did not get the gift",MembersService.isGiftExists(smartGift.getTitle()));

        String assetKey = getAssetKey(member.getPhoneNumber());
        //redeem asset
        performAnActionRedeemAsset(membershipKey,userKey,assetKey);

    }


    private static void initProductTemplates(Box box) throws Exception {
        productTemplateDOMap = new LinkedHashMap<>();

        //create template join club + tag member action
        String timestamp = String.valueOf(System.currentTimeMillis());
        String tag = "tag_" + timestamp.toString();
        ActivityAction activityAction = ActionsBuilder.buildTagMemberAction(tag);
        SaveProductTemplate productTemplate = LoyaltyService.createProductTemplateWithAllVerticals(box, ActivityTrigger.JoinTheClub, activityAction, ActivityTrigger.JoinTheClub.toString());
        ProductTemplateDO productTemplateDO = (new ProductTemplateDO()).setActivityName(ActivityTrigger.JoinTheClub.toString()).setTag(tag).setTrigger(ActivityTrigger.JoinTheClub.toString());
        productTemplateDOMap.put(productTemplate.getName(),productTemplateDO);

        //create template Redeem + add points action (asset name will be empty and later we will edit the activity
        ActivityCondition assetCondition = new AssetCondition();
        assetCondition.setOperatorKey(ListOperators.IS_EXACTLY)
                .setConditionValue(smartGift.getTitle());
        String pointsToAccumulate = "300";
        activityAction = ActionsBuilder.buildPointsAccumulationAction(pointsToAccumulate);
        productTemplate = LoyaltyService.createProductTemplateWithAllVerticals(box, ActivityTrigger.RedeemsAnAsset,assetCondition, activityAction);
        productTemplateDO = (new ProductTemplateDO()).setActivityName(ActivityTrigger.RedeemsAnAsset.toString()).setNumOfPointsToAccumulate(pointsToAccumulate).setTrigger(ActivityTrigger.RedeemsAnAsset.toString());
        productTemplateDOMap.put(productTemplate.getName(),productTemplateDO);


        //create template Update membership + global condition + add points action (asset name will be empty and later we will edit the activity
        pointsToAccumulate = "500";
        activityAction = ActionsBuilder.buildPointsAccumulationAction(pointsToAccumulate);
        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition().
                setFieldsSelect(MemberNonRegistrationFormFields.AllowEmail.toString()).toggleBooleanValue(ActivityCondition.BOOLEAN_YES);
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers().setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);
        productTemplate = LoyaltyService.createProductTemplateWithAllVerticals(box, ActivityTrigger.UpdatedMembershipDetails,globalCondition, activityAction);
        productTemplateDO = (new ProductTemplateDO()).setActivityName(ActivityTrigger.UpdatedMembershipDetails.toString()).setNumOfPointsToAccumulate(pointsToAccumulate).setTrigger(ActivityTrigger.UpdatedMembershipDetails.toString());
        productTemplateDOMap.put(productTemplate.getName(),productTemplateDO);


        createSplitCases();

//        //create template Make purchase + send asset action (asset name will be empty and later we will edit the activity
//        ActivityAction activityAction = ActionsBuilder.buildSendBenefitAction("");
//        SaveProductTemplate productTemplate = LoyaltyService.createProductTemplateWithAllVerticals(box, ActivityTrigger.MadeAPurchase, activityAction, ActivityTrigger.MadeAPurchase.toString());
//        ProductTemplateDO productTemplateDO = (new ProductTemplateDO()).setActivityName(ActivityTrigger.MadeAPurchase.toString()).setAssetName("").setTrigger(ActivityTrigger.MadeAPurchase.toString());
//        productTemplateDOMap.put(productTemplate.getName(),productTemplateDO);

        productTemplateNamesArr = productTemplateDOMap.keySet().toArray();
    }


    public static void createSplitCases() throws Exception {

        /****case1****/
        GeneralCondition generalCondition = CasesConditionsBuilder.buildGeneralConditionPoints(PointsCondition.ConditionKey.NUMBER_OF_POINTS, InputOperators.IS_EXACTLY, NUM_OF_POINTS_CASE_1);
        RuleCase aCase = new RuleCase();
        aCase.setDescription("case1");
        aCase.setConditions(Lists.newArrayList(generalCondition));
        ActivityAction activityAction1 = ActionsBuilder.buildTagMemberAction("TAG_1");
        aCase.setActions(com.beust.jcommander.internal.Lists.newArrayList(activityAction1));


        /****case2****/
        GeneralCondition generalCondition2 = CasesConditionsBuilder.buildGeneralConditionPoints(PointsCondition.ConditionKey.NUMBER_OF_POINTS, InputOperators.IS_EXACTLY, NUM_OF_POINTS_CASE_2);
        RuleCase aCase2 = new RuleCase();
        aCase2.setDescription("case2");
        aCase2.setConditions(Lists.newArrayList(generalCondition2));
        ActivityAction activityAction2 = ActionsBuilder.buildTagMemberAction(TAG_CASE2);
        aCase2.setActions(com.beust.jcommander.internal.Lists.newArrayList(activityAction2));

        //create activity with 2 cases
        SaveProductTemplate productTemplate = LoyaltyService.createProductTemplateWithAllVerticals(box, ActivityTrigger.ReceivesPoints,Lists.newArrayList(aCase,aCase2));
        ProductTemplateDO productTemplateDO = (new ProductTemplateDO()).setActivityName(ActivityTrigger.ReceivesPoints.toString()).setTag(TAG_CASE2).setTrigger(ActivityTrigger.ReceivesPoints.toString());
        productTemplateDOMap.put(productTemplate.getName(),productTemplateDO);

    }

    private static class ProductTemplateDO{
        private String activityName;
        private String trigger;
        private String tag;
        private String assetName;
        private String numOfPointsToAccumulate;

        public String getActivityName() {
            return activityName;
        }

        public ProductTemplateDO setActivityName(String activityName) {
            this.activityName = activityName;
            return this;
        }

        public String getNumOfPointsToAccumulate() {
            return String.valueOf( Integer.valueOf(numOfPointsToAccumulate)*100 );
        }

        public ProductTemplateDO setNumOfPointsToAccumulate(String numOfPointsToAccumulate) {
            this.numOfPointsToAccumulate = numOfPointsToAccumulate;
            return this;
        }

        public String getTrigger() {
            return trigger;
        }

        public ProductTemplateDO setTrigger(String trigger) {
          this.trigger = trigger;
          return this;
        }


        public String getTag() {
            return tag;
        }

        public ProductTemplateDO setTag(String tag) {
            this.tag = tag;
            return this;
        }

        public String getAssetName() {
            return assetName;
        }

        public ProductTemplateDO setAssetName(String assetName) {
            this.assetName = assetName;
            return this;
        }
    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.MadeAPurchase;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.MadeAPurchase.toString() +"_";
    }
}
