package hub2_0.templates;

import com.google.common.collect.Lists;
import edu.emory.mathcs.backport.java.util.Collections;
import hub.base.BaseService;
import hub.base.categories.hub2Categories.Hub2Regression;
import hub.base.categories.hub2Categories.hub2Sanity;
import hub.base.categories.hub2Categories.hub2Templates;
import hub.common.objects.common.Gender;
import hub.common.objects.common.RegistrationField;
import hub.common.objects.member.NewMember;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AmountType;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.templates.TemplatesLibrary;
import hub.hub2_0.common.actions.*;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.PointsCondition;
import hub.hub2_0.common.conditions.TagCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;
import hub.hub2_0.common.conditions.operators.InputOperators;
import hub.hub2_0.common.conditions.operators.IntOperators;
import hub.hub2_0.common.conditions.operators.ShoppingCartOperators;
import hub.hub2_0.common.enums.*;
import hub.hub2_0.common.items.CodesGroup;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import hub.services.member.MembersService;
import hub.utils.HubMemberCreator;
import integration.base.BaseRuleHub2Test;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import ru.yandex.qatools.allure.annotations.Issue;
import server.v2_8.common.Item;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class TemplatesManagerTest extends BaseRuleHub2Test {

    private static Box box;
    private static String templateName;

    @BeforeClass
    public static void createNewBox() throws Exception {
        box = ruleBuilder.buildBox("BOX " + String.valueOf(System.currentTimeMillis()));
        LoyaltyService.createBox(box);
    }

    @org.junit.Rule
    public TestWatcher methodsWatcher = new TestWatcher() {

        @Override
        protected void starting(Description description) {
            timestamp = String.valueOf(System.currentTimeMillis());
            templateName = "template_" + timestamp;
        }
    };


    @Test
    @Category({hub2Sanity.class, hub2Templates.class})
    public void testTemplateStartFromScratchAndSaveToTemplate() throws Exception {
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, getAutomationName(), "template" + timestamp);

        MadeAPurchaseCondition condition = new MadeAPurchaseCondition();
        condition.setConditionKey(MadeAPurchaseCondition.ConditionKey.BRANCH_ID)
                .setOperatorKey(InputOperators.IS_ONE_OF);

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.MadeAPurchase.toString(), box, rule, Arrays.asList(condition), Lists.newArrayList(ActivityActions.Tag), null);
        activity.clickSaveAsTemplateAndSave(templateName, "This is Make a purchase template with BranchID condition and tag action.");
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();

        MadeAPurchaseCondition condition2 = new MadeAPurchaseCondition();
        condition2.setConditionValue("1214");

        tag = ActionsBuilder.TAG + timestamp.toString();
        TagUntagMemberAction tagUntagMemberAction = new TagUntagMemberAction();
        tagUntagMemberAction.setTagText(tag);

        createActivityFromTemplate(ActivityTrigger.MadeAPurchase.toString(), templateName, Lists.newArrayList(condition2), Lists.newArrayList(tagUntagMemberAction));

        NewMember member = createNewMember();
        Item itemA = createItem("898", "itemA", "depA", "depAA", 30);
        submitPurchaseAndValidate(member, Lists.newArrayList(itemA), "1", false);
        submitPurchaseAndValidate(member, Lists.newArrayList(itemA), "1214", true);
    }

    @Test
    public void testTemplateEditTemplateNameAndDescription() throws Exception {

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.JoinTheClub.toString(), "some description");
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.JoinTheClub.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(ActivityActions.Tag), timestamp.toString());
        activity.clickSaveAsTemplateAndSave(templateName, String.format("This is Join club template with timestamp %s.", timestamp));
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel();

        // Back in Loyalty tab.
        LoyaltyService.clickAddActivity(activity);
        String updateTemplateName = templateName + "_update";
        TemplatesLibrary.editTemplateName(templateName, updateTemplateName, "This template name and description were updated.");
        TemplatesLibrary.clickBack();
        tag = ActionsBuilder.TAG + timestamp.toString();

        TagUntagMemberAction tagUntagMemberAction = new TagUntagMemberAction();
        tagUntagMemberAction.setTagText(tag);
        createActivityFromTemplate(ActivityTrigger.JoinTheClub.toString(), updateTemplateName, Lists.newArrayList(), Lists.newArrayList(tagUntagMemberAction));
        createNewMember();
        checkDataStoreUpdatedWithTagByRuleID();
    }

    @Test
    public void testDeleteTemplateAndTriggerRule() throws Exception {

        //Create female member
        timestamp = String.valueOf(System.currentTimeMillis());
        NewMember female = createNewMember();

        createSmartGift();
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.UpdatedMembershipDetails.toString(), "some description");
        MembershipGlobalCondition globalCondition = new MembershipGlobalCondition();
        globalCondition.clickSpecificMembersBtn();
        MemberFieldCondition memberFieldCondition = MemberFieldCondition.newCondition()
                .setFieldsSelect(RegistrationField.Gender.toString())
                .setConditionSelectValue("female");
        SpecificMembers specificMembers = SpecificMembers.newSpecificMembers()
                .setConditions(memberFieldCondition);
        globalCondition.setSpecificMembers(specificMembers);

        SendBenefitAction sendBenefitAction = new SendBenefitAction();
        sendBenefitAction.clickSendBenefitBtn();

        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.UpdatedMembershipDetails.toString(), box, rule, Arrays.asList(globalCondition), Lists.newArrayList(sendBenefitAction));
        activity.clickSaveAsTemplateAndSave(templateName, "This is UpdateMembership template with send benefit action.");

        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel(); // dont save the rule.

        SendBenefitAction sendBenefitAction2 = new SendBenefitAction();
        sendBenefitAction2.setSelectBenefit(smartGift.getTitle());
        createActivityFromTemplate(ActivityTrigger.UpdatedMembershipDetails.toString(), templateName, Lists.newArrayList(), Lists.newArrayList(sendBenefitAction2));
        activity.clickCancel();

        LoyaltyService.clickAddActivity(activity);
        TemplatesLibrary.deleteTemplate(templateName);
        TemplatesLibrary.clickBack();

        NewMember maleMember = HubMemberCreator.buildMember(timestamp, "maleUser_", "maleUser_");
        maleMember.setGender(Gender.MALE.toString());
        MembersService.createNewMember(maleMember);

        checkDataStoreNegativeCheckByRuleID(automationRuleId);

        NewMember updateMember = new NewMember();
        updateMember.setPhoneNumber(female.getPhoneNumber());
        updateMember.setLastname(female.getLastName() + "_Updated");
        performUpdateMemberDetails(updateMember);
        checkDataStoreUpdatedWithAssetByRuleID();
    }

    @Test
    @Category({Hub2Regression.class})
    public void testTemplateLoadAnother() throws Exception {

        createSmartGift();
        PointsCondition pointsCondition = new PointsCondition();
        pointsCondition.setConditionKey(PointsCondition.ConditionKey.NUMBER_OF_POINTS);

        SendTextMessageAction sendTextMessageAction = new SendTextMessageAction();
        sendTextMessageAction.clickSendTextMessageBtn();

        String templateName1 = templateName + "_1";
        String templateName2 = templateName + "_2";
        createNewTemplate(templateName1, ActivityTrigger.JoinTheClub, Lists.newArrayList(), Lists.newArrayList(ActionsBuilder.buildTagMemberAction(tag + timestamp)));
        createNewTemplate(templateName2, ActivityTrigger.ReceivesPoints, Lists.newArrayList(pointsCondition), Lists.newArrayList(sendTextMessageAction));

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.ReceivesPoints.toString(), "some description");
        Activity activity = ruleBuilder.buildActivity(null, box, rule, Collections.emptyList(), Collections.emptyList());
        activity.clickLoadAnother();
        //execute the selenium job - load from template and click load another - back to Templates library.
        LoyaltyService.createRuleFromTemplateInBox(activity, templateName1);

        PointsCondition pointsCondition2 = new PointsCondition();
        pointsCondition2.setOperatorKey(IntOperators.IS_GREATER_THAN_OR_EQUAL_TO)
                .setConditionValue("20");

        SendTextMessageAction sendTextMessageAction2 = new SendTextMessageAction();
        sendTextMessageAction2.setMessage("Hi! u received new points!");

        Rule rule2 = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, "_");
        Activity activity2 = ruleBuilder.buildActivity(null, box, rule2, Lists.newArrayList(pointsCondition2), Lists.newArrayList(sendTextMessageAction2));
        LoyaltyService.selectTemplateAndCreateActivity(activity2, templateName2);
        automationRuleId = LoyaltyService.getRuleID(rule2.getActivityName());


        createNewMember();
        MembersService.performSmartActionAddPoints("3000", timestamp);
        checkAutomationRunAndSendMemberSMS();
    }


    @Test
    @Category({Hub2Regression.class})
    public void testSaveTemplateFromRule() throws Exception {

        createSmartGift();
        tag = ActionsBuilder.TAG + timestamp.toString();
        TagCondition tagCondition = new TagCondition();
        tagCondition.setConditionKey(TagCondition.ConditionKey.TAG)
                .setOperatorKey(InputOperators.IS_ONE_OF)
                .setConditionValue(tag);

        // create exist rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, ActivityTrigger.Untagged.toString());
        Activity activity = ruleBuilder.buildActivity(ActivityTrigger.Untagged.toString(), box, rule, Lists.newArrayList(tagCondition), Lists.newArrayList(ActivityActions.SendTextMessage), "some text");
        LoyaltyService.createActivityInBox(activity);

        // edit rule and save to template.
        LoyaltyService.clickOnActivity(activity.getActivityName());
        Rule rule2 = new Rule();
        rule2.clickSaveAsTemplateAndSave(templateName, "this template created from exist rule : " + rule.getActivityName());
        BaseService.fillSeleniumObject(rule2);
        rule2.clickCancel();
        // create new rule from template
        timestamp = String.valueOf(System.currentTimeMillis());
        createActivityFromTemplate(ActivityTrigger.Untagged.toString(), templateName, Lists.newArrayList(), Lists.newArrayList(ActionsBuilder.buildSendBenefitAction(smartGift.getTitle())));

        createNewMember();
        MembersService.performTagMemberSmartAction(tag, "actionTag" + timestamp);
        MembersService.performUntagMemberSmartAction(tag, "actionUnTag" + timestamp);

        checkDataStoreUpdatedWithAssetByRuleID();
    }

    @Issue("CNP-11397")
    @Test
    @Category({Hub2Regression.class})
    public void testSaveNewTemplateFromExistTemplate() throws Exception {

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_AT_LEAST);

        PointsAccumulationAction pointsAccumulationAction = new PointsAccumulationAction();
        pointsAccumulationAction.clickPointsAccumulationBtn();
        pointsAccumulationAction.setAccumulationType(PointsAccumulationAction.AccumulationType.ACCUMULATE_BY_RATE);
        pointsAccumulationAction.setNumOfPoints("10");
        pointsAccumulationAction.setPointsRate("100");

        createNewTemplate(templateName, ActivityTrigger.MadeAPurchase, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(pointsAccumulationAction));

        tag = ActionsBuilder.TAG + timestamp.toString();
        String newTemplateName = templateName + "updated";

        // create exist rule
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.MadeAPurchase.toString(), "some description");
        Activity activity = ruleBuilder.buildActivity(null, box, rule, Lists.newArrayList(), Lists.newArrayList(new PointsAccumulationAction(), ActionsBuilder.buildTagMemberAction(tag)));

        activity.clickSaveAsTemplateAndSaveAsNew(newTemplateName, "this template was saved from template.");
        LoyaltyService.createRuleFromTemplateInBox(activity, templateName);
        activity.clickCancel();

        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        CodesGroup itemsCodesGroup = new CodesGroup();
        itemsCodesGroup.setCodes(CodesGroup.ITEM_CODES_GROUP, "111", "222");
        selectItems.setItemCodes(itemsCodesGroup);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        SelectItems selectItemsToAccumulate = new SelectItems();
        selectItemsToAccumulate.clickAnyItemRadioButton();
        selectItemsToAccumulate.clickButton(SelectedItemsDialogButtons.Apply.toString());
        SelectItems excludeItems = new SelectItems();
        excludeItems.clickItemCodesRadioButton();
        excludeItems.setItemCodes("333");
        excludeItems.clickApply();

        MadeAPurchaseCondition madeAPurchaseCondition2 = new MadeAPurchaseCondition();
        madeAPurchaseCondition2.setConditionValue("1")
                .setSelectItems(selectItems);
        PointsAccumulationAction pointsAccumulationAction2 = new PointsAccumulationAction();
        pointsAccumulationAction2.setNumOfPoints("20");
        pointsAccumulationAction2.clickSelectItemsButton();
        pointsAccumulationAction2.setSelectItems(selectItemsToAccumulate);
        pointsAccumulationAction2.setSelectItemsToExclude(excludeItems);

        createActivityFromTemplate(ActivityTrigger.MadeAPurchase.toString(), newTemplateName, Lists.newArrayList(madeAPurchaseCondition2), Lists.newArrayList(pointsAccumulationAction2));

        NewMember member = createNewMember();
        Item itemA = createItem("111", "itemA", "depA", "depAA", 15000);
        Item itemB = createItem("222", "itemB", "depA", "depAA", 29000);
        Item itemC = createItem("333", "itemC", "depA", "depAA", 90000);
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member), "134000", com.google.common.collect.Lists.newArrayList("DEP-1"), transactionId, Lists.newArrayList(itemA, itemB, itemC), "1");
        Thread.sleep(5000);
        checkDataStoreUpdatedWithPointsByRuleID("8000", AmountType.Points);
        checkDataStoreUpdatedWithTagByRuleID();
    }


    @Test
    public void testSaveAndReplaceTemplate() throws Exception {

        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        CodesGroup itemsCodesGroup = new CodesGroup();
        itemsCodesGroup.setCodes(CodesGroup.ITEM_CODES_GROUP, "111");
        selectItems.setItemCodes(itemsCodesGroup);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(MadeAPurchaseCondition.ConditionKey.SHOPPING_CART)
                .setOperatorKey(ShoppingCartOperators.CONTAINS_AT_LEAST)
                .setConditionValue("2")
                .setSelectItems(selectItems);

        PointsAccumulationAction pointsAccumulationAction = new PointsAccumulationAction();
        pointsAccumulationAction.clickPointsAccumulationBtn();
        pointsAccumulationAction.setNumOfPoints("10");

        createNewTemplate(templateName, ActivityTrigger.MadeAPurchase, Lists.newArrayList(madeAPurchaseCondition), Lists.newArrayList(pointsAccumulationAction));

        tag = ActionsBuilder.TAG + timestamp.toString();
        // Update template
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityTrigger.MadeAPurchase.toString(), "some description");
        Activity activity = ruleBuilder.buildActivity(null, box, rule, Lists.newArrayList(), Lists.newArrayList(new PointsAccumulationAction(), ActionsBuilder.buildTagMemberAction(tag)));

        activity.clickSaveAsTemplateAndReplace(null, "this template was updated");
        LoyaltyService.createRuleFromTemplateInBox(activity, templateName);
        activity.clickCancel();

        SelectItems selectItemsToAccumulate = new SelectItems();
        selectItemsToAccumulate.clickAnyItemRadioButton();
        selectItemsToAccumulate.clickButton(SelectedItemsDialogButtons.Apply.toString());
        SelectItems excludeItems = new SelectItems();
        excludeItems.clickItemCodesRadioButton();
        excludeItems.setItemCodes("333");
        excludeItems.clickButton(SelectedItemsDialogButtons.Apply.toString());

        PointsAccumulationAction pointsAccumulationAction2 = new PointsAccumulationAction();
        pointsAccumulationAction2.clickSelectItemsButton();
        pointsAccumulationAction2.setSelectItems(selectItemsToAccumulate);
        pointsAccumulationAction2.setSelectItemsToExclude(excludeItems);

        createActivityFromTemplate(ActivityTrigger.MadeAPurchase.toString(), templateName, Lists.newArrayList(), Lists.newArrayList(pointsAccumulationAction2));

        NewMember member = createNewMember();
        Item itemA = createItem("111", "itemA", "depA", "depAA", 10000);
        Item itemB = createItem("111", "itemB", "depA", "depAA", 5000);
        Item itemC = createItem("333", "itemC", "depA", "depAA", 70000);
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchaseWithParams(Lists.newArrayList(member), "85000", Lists.newArrayList("DEP-1"), transactionId, Lists.newArrayList(itemA, itemB, itemC), "1");
        Thread.sleep(5000);
        checkDataStoreUpdatedWithPointsByRuleID("1500", AmountType.Points);
        checkDataStoreUpdatedWithTagByRuleID();
    }


    private void createNewTemplate(String templateName, ActivityTrigger trigger, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, trigger.toString(), "some description");
        Activity activity = ruleBuilder.buildActivity(trigger.toString(), box, rule, conditions, activityActionList);
        activity.clickSaveAsTemplateAndSave(templateName, "template description");
        //execute the selenium job
        LoyaltyService.createActivityInBox(activity);
        activity.clickCancel(); // dont save the rule.
    }


    private void createActivityFromTemplate(String activityName, String templateName, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, ActivityStatus.Active, activityName);
        Activity activity = ruleBuilder.buildActivity(null, box, rule, conditions, activityActionList);
        //execute the selenium job - create rule from template
        LoyaltyService.createRuleFromTemplateInBox(activity, templateName);
        automationRuleId = LoyaltyService.getRuleID(rule.getActivityName());
    }


    private void submitPurchaseAndValidate(NewMember newMember, ArrayList<Item> items, String branchID, boolean isRuleTriggered) throws Exception {
        String transactionId = UUID.randomUUID().toString();
        performAnActionSubmitPurchaseWithParams(com.google.common.collect.Lists.newArrayList(newMember), "30", com.google.common.collect.Lists.newArrayList("DEP-1"), transactionId, items, branchID);
        Thread.sleep(5000);
        if (isRuleTriggered)
            checkDataStoreUpdatedWithTagByRuleID();
        else
            checkDataStoreNegativeCheckByRuleID(TAG_OPERATION);
    }

    @After
    public void cleanData() {
        log4j.info("deactivate rule from APIs");
        deactivateAutomation();
        log4j.info("Rule deactivated");
    }

    @Override
    public ITrigger getTrigger() {
        return ActivityTrigger.MadeAPurchase;
    }

    @Override
    public String getAutomationName() {
        return ActivityTrigger.MadeAPurchase.toString();
    }
}