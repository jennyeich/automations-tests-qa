package server.v2_8;

import server.common.BaseServicesAPI;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

/**
 * Created by Jenny on 06/19/2016.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)

public class GenerateToken extends BaseServicesAPI {

    public static final String TIME_TO_LIVE = "timeToLive";
    private static final String baseUrl = "memberships/generateToken";
    private String timeToLive;

    @JsonGetter(TIME_TO_LIVE)
    public String getTimeToLive() {
        return timeToLive;
    }
    public void setTimeToLive(String timeToLive) {
        this.timeToLive = timeToLive;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

}
