package server.v2_8;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseZappServerAPI;

/**
 * Created by Jenny on 11/30/2016.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)

public class GetUserAtLocation extends BaseZappServerAPI {

   // public static final String USER_KEY = "UserKey";
    public static final String LOCATION_ID = "LocationID";

    private static final String baseUrl = "alpha/api/GetUserAtLocation";

   // private String userKey;
    private String locationID;

   // @JsonGetter(USER_KEY)
  //  public String getUserKey() {
  //      return userKey;
 //   }
 //   public void setUserKey(String expandAssets) {
  //      this.userKey = userKey;
 //   }

    @JsonGetter(LOCATION_ID)
    public String getLocationID() { return locationID; }
    public void setLocationID(String locationID) { this.locationID = locationID; }


    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

}
