package server.v2_8;

import server.common.Response;
import server.v2_8.common.Message;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by Goni on 6/27/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetMemberMessagesResponse extends Response {

    public static final String RESULT = "Result";
    public static final String MESSAGES = "Messages";

    private String result;
    private ArrayList<Message> messages;

    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @JsonGetter(MESSAGES)
    public ArrayList<Message> getMessages() {
        if (messages==null){
            messages=new ArrayList<>();
        }
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

}