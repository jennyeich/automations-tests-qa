package server.v2_8;

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by Goni on 3/30/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SaveEventBasedActionRuleResponse  extends Response {

    public static final String RESULT = "Result";
    public static final String STATUS= "Status";

    private String result;
    private String status;

    public SaveEventBasedActionRuleResponse() {
    }

    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @JsonGetter(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
