package server.v2_8;

import com.fasterxml.jackson.annotation.JsonInclude;
import server.common.Response;
import server.v2_8.common.OptionalBenefit;
import server.common.asset.ActionResponse;
import server.common.asset.actions.Discount;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by Lior on 11/30/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetMemberBenefitsResponse extends Response {

    public static final String RESULT = "Result";
    public static final String ITEM_CODES = "ItemCodes";
    public static final String DEAL_CODES = "DealCodes";
    public static final String DISCOUNTS = "Discounts";
    public static final String OPTIONAL_BENEFITS = "OptionalBenefits";
    public static final String TOTAL_SUM_DISCOUNT = "TotalDiscountsSum";

    private String result;
    private ArrayList<ActionResponse> itemCodes;
    private ArrayList<ActionResponse> dealCodes;
    private ArrayList<Discount> discounts;
    private ArrayList<OptionalBenefit> optionalBenefits;
    private String totalSumDiscount;


    public void setTotalSumDiscount(String totalSumDiscount) {this.totalSumDiscount = totalSumDiscount;}

    @JsonGetter(TOTAL_SUM_DISCOUNT)
    public String getTotalSumDiscount() {
        return totalSumDiscount;
    }

    public GetMemberBenefitsResponse() {
    }

    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @JsonGetter(ITEM_CODES)
    public ArrayList<ActionResponse> getItemCodes() {
        if(itemCodes == null)
            itemCodes = new ArrayList<>();
        return itemCodes;
    }

    public void setItemCodes(ArrayList<ActionResponse> itemCodes) {
        this.itemCodes = itemCodes;
    }

    @JsonGetter(DEAL_CODES)
    public ArrayList<ActionResponse> getDealCodes() {
        if(dealCodes == null)
            dealCodes = new ArrayList<>();
        return dealCodes;
    }

    public void setDealCodes(ArrayList<ActionResponse> dealCodes) {
        this.dealCodes = dealCodes;
    }

    @JsonGetter(DISCOUNTS)
    public ArrayList<Discount> getDiscounts() {
        if(discounts == null)
            discounts = new ArrayList<>();
        return discounts;
    }

    public void setDiscounts(ArrayList<Discount> discounts) {
        this.discounts = discounts;
    }

    @JsonGetter(OPTIONAL_BENEFITS)
    public ArrayList<OptionalBenefit> getOptionalBenefits() {
        if(optionalBenefits == null)
            optionalBenefits = new ArrayList<>();
        return optionalBenefits;
    }

    public void setOptionalBenefits(ArrayList<OptionalBenefit> optionalBenefits) {
        this.optionalBenefits = optionalBenefits;
    }

    @JsonIgnore
    public ArrayList<String> getSimplefiedDealCodes(){
        ArrayList<String> res = new ArrayList<>();
        for (ActionResponse response : dealCodes){
            res.add(response.getCode());
        }
        return res;
    }

    @JsonIgnore
    public ArrayList<String> getSimplefiedItemCodes(){
        ArrayList<String> res = new ArrayList<>();
        for (ActionResponse response : itemCodes){
            res.add(response.getCode());
        }
        return res;
    }

    public ArrayList<String> getRelatedItems (String code){
        for (ActionResponse response : itemCodes){
            if(response.getCode().equals(code)){
                return response.getRequiredItems();
            }
        }
        for (ActionResponse response : dealCodes){
            if(response.getCode().equals(code)){
                return response.getRequiredItems();
            }
        }
        return null;
    }
}
