package server.v2_8;

import server.v2_8.common.RedeemItem;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

import java.util.ArrayList;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class  Redeem extends PurchaseAPI  {

	public static final String REDEEM_ITEMS = "RedeemItems";
	public static final String MARK_AS_USED = "MarkAsUsed";
	public static final String RETURN_EXTENDED_ITEMS = "ReturnExtendedItems";
	public static final String DETAILED_REDEEM_RESPONSE = "DetailedRedeemResponse";

	private static final String baseUrl = "alpha/api/Redeem";

	private ArrayList<RedeemItem> redeemItems;
	private Boolean markAsUsed;
	private Boolean returnExtendedItems;
	private Boolean detailedRedeemResponse;

	@JsonGetter(REDEEM_ITEMS)
	public ArrayList<RedeemItem> getRedeemItems() {
		if(redeemItems == null)
			redeemItems = new ArrayList<>();
		return redeemItems;
	}


	public void setRedeemItems(ArrayList<RedeemItem> redeemItems) {
		this.redeemItems = redeemItems;
	}

	@JsonGetter(DETAILED_REDEEM_RESPONSE)
	public Boolean getDetailedRedeemResponse() {return detailedRedeemResponse;}
	public void setDetailedRedeemResponse(boolean detailedRedeemResponse) {this.detailedRedeemResponse = detailedRedeemResponse;}

	@JsonGetter(MARK_AS_USED)
	public Boolean getMarkAsUsed() {
		return markAsUsed;
	}

	public void setMarkAsUsed(Boolean markAsUsed) {
		this.markAsUsed = markAsUsed;
	}

	@JsonGetter(RETURN_EXTENDED_ITEMS)
	public Boolean getReturnExtendedItems() {
		return returnExtendedItems;
	}

	public void setReturnExtendedItems(Boolean returnExtendedItems) {
		this.returnExtendedItems = returnExtendedItems;
	}

	@Override
	@JsonIgnore
	public String getRequestURL() {
		return baseUrl;
	}

	@Override
	@JsonIgnore
	public String getHttpMethod() {
		return HttpMethod.POST.toString();
	}
}