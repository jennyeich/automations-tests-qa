package server.v2_8;

//Created by Jenny on 12/13/2016.


 import server.common.BaseZappServerAPI;
 import com.fasterxml.jackson.annotation.JsonIgnore;
 import com.fasterxml.jackson.annotation.JsonInclude;
 import com.gargoylesoftware.htmlunit.HttpMethod;
 import com.google.appengine.api.datastore.Key;
 import com.google.appengine.api.datastore.KeyFactory;


@JsonInclude(JsonInclude.Include.NON_NULL)
 public class DataStoreKeyUtility extends BaseZappServerAPI {



    // private  String userKey;
     private  final String baseUrl = "http://tamar-is-awesome.appspot.com/utils/decode?code=";


    @Override
    @JsonIgnore
    public String getRequestURL() { return baseUrl;}

    @Override
    @JsonIgnore
    public String getHttpMethod() { return HttpMethod.GET.toString();}

   //  public  String getUserKey() {return userKey;}
   //  public  void setUserKey(String userKey) {this.userKey = userKey;}

   public Key userKey (String tamarDecodedKey){

       String kind = tamarDecodedKey.substring(tamarDecodedKey.indexOf(":")+1,tamarDecodedKey.indexOf("_"));
       Long id = Long.parseLong(tamarDecodedKey.substring(tamarDecodedKey.indexOf("_") + 1));
       return KeyFactory.createKey(kind,id);
   }

 }



