package server.v2_8;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import server.common.Response;

/**
 * Created by amit.levi on 27/04/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegisterResponse extends Response {

    public static final String MEMBERSHIP_STATUS = "MembershipStatus";
    public static final String USER_KEY = "UserKey";
    public static final String MEMBERSHIP_KEY = "Key";
    public static final String ALLOW_SMS = "AllowSMS";
    public static final String ALLOW_EMAIL = "AllowEmail";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String REGISTRATION_SOURCE = "RegistrationSource";
    public static final String CONSENT = "Consent";
    public static final String CONSENT_FOR_HUB = "ConsentForHub";

    private String membershipStatus;
    private String userKey;
    private String membershipKey;
    private Boolean allowSMS;
    private Boolean allowEmail;
    private String phoneNumber;
    private String registrationSource;
    private String consent;
    private String consentForHub;

    @JsonProperty(MEMBERSHIP_STATUS)
    public String getMembershipStatus() {
        return membershipStatus;
    }

    public void setMembershipStatus(String membershipStatus) {
        this.membershipStatus = membershipStatus;
    }

    @JsonProperty(USER_KEY)
    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    @JsonProperty(MEMBERSHIP_KEY)
    public String getMembershipKey() {
        return membershipKey;
    }

    public void setMembershipKey(String membershipKey) {
        this.membershipKey = membershipKey;
    }

    @JsonProperty(ALLOW_SMS)
    public Boolean getAllowSMS() { return allowSMS; }

    public void setAllowSMS(Boolean allowSMS) { this.allowSMS = allowSMS; }

    @JsonProperty(ALLOW_EMAIL)
    public Boolean getAllowEmail() { return allowEmail; }

    public void setAllowEmail(Boolean allowEmail) { this.allowEmail = allowEmail; }

    @JsonProperty(PHONE_NUMBER)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @JsonProperty(REGISTRATION_SOURCE)
    public String getRegistrationSource() {
        return registrationSource;
    }

    public void setRegistrationSource(String registrationSource) {
        this.registrationSource = registrationSource;
    }
    @JsonProperty(CONSENT)
    public String getConsent() {
        return consent;
    }

    public void setConsent(String consent) {
        this.consent = consent;
    }

    @JsonProperty(CONSENT_FOR_HUB)
    public String getConsentForHub() {
        return consentForHub;
    }

    public void setConsentForHub(String consentForHub) {
        this.consentForHub = consentForHub;
    }
}
