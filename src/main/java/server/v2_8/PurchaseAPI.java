package server.v2_8;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import server.common.BaseZappServerAPI;
import server.v2_8.common.Customer;
import server.v2_8.common.Item;
import server.v2_8.common.Payment;
import server.v2_8.common.RedeemItem;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;


@JsonInclude(JsonInclude.Include.NON_EMPTY)
public abstract class PurchaseAPI extends BaseZappServerAPI {

	private static final String baseUrl = "alpha/api/SubmitPurchase";

	private List<Customer> customers;
	//add redeem items
	private List<Item> items;
	private String posId;
	private String branchId;
	private String transactionId;
	private Long timeStamp;
	private String totalSum;
	private long totalGeneralDiscount;
	private List<String> tags;
	private String cashier;
	private String chainId;
	private List<Payment> payments;
	private String orderType;
	private String transactionSource;
	private String transactionSourceName;
	private ArrayList<RedeemItem> redeemItems;
	private String status;



	@JsonGetter("Customers")
	public List<Customer> getCustomers() {
		if (customers == null)
			customers = new ArrayList<>();
		return customers;
	}

	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}

	@JsonGetter("Items")
	public List<Item> getItems() {
		if(items == null){
			items = new ArrayList<>();
		}
		return items;
	}

	public void setItems(List<Item>  items) {
		this.items = items;
	}

	@JsonGetter("PosID")
	public String getPosId() {
		return posId;
	}

	public void setPosId(String posId) {
		this.posId = posId;
	}

	@JsonGetter("BranchID")
	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	@JsonGetter("TransactionID")
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@JsonGetter("TimeStamp")
	public Long getTimeStamp() {
		if (timeStamp == null) {
			timeStamp = Instant.now().getEpochSecond();
		}
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	@JsonGetter("TotalGeneralDiscount")
	public long getTotalGeneralDiscount() {
		return totalGeneralDiscount;
	}

	public void setTotalGeneralDiscount(long totalGeneralDiscount) {
		this.totalGeneralDiscount = totalGeneralDiscount;
	}

	@JsonGetter("TotalSum")
	public String getTotalSum() {
		return totalSum;
	}

	public void setTotalSum(String totalSum) {
		this.totalSum = totalSum;
	}


	@JsonGetter("Tags")
	public List<String> getTags() {
		if(tags == null)
			tags = new ArrayList<>();
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	@JsonGetter("Cashier")
	public String getCashier() {
		return cashier;
	}

	public void setCashier(String cashier) {
		this.cashier = cashier;
	}


	@JsonGetter("ChainID")
	public String getChainId() {
		return chainId;
	}

	public void setChainId(String chainId) {
		this.chainId = chainId;
	}

	@JsonGetter("Payment")
	public List<Payment> getPayments() {
		if (payments == null){
			payments = new ArrayList<>();
		}
		return payments;
	}

	public void setPayments(List<Payment> payments) {
		this.payments = payments;
	}

	@JsonGetter("OrderType")
	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	@JsonGetter("TransactionSource")
	public String getTransactionSource() {
		return transactionSource;
	}

	public void setTransactionSource(String transactionSource) {
		this.transactionSource = transactionSource;
	}

	@JsonGetter("TransactionSourceName")
	public String getTransactionSourceName() {
		return transactionSourceName;
	}

	public void setTransactionSourceName(String transactionSourceName) {
		this.transactionSourceName = transactionSourceName;
	}

	@JsonGetter("RedeemItems")
	public ArrayList<RedeemItem> getRedeemItems() {
		if(redeemItems == null){
			redeemItems = new ArrayList<>();
		}
		return redeemItems;
	}

	public void setRedeemItems(ArrayList<RedeemItem> redeemItems) {
		this.redeemItems = redeemItems;
	}

	@JsonGetter("Status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}




