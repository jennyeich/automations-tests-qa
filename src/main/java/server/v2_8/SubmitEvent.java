package server.v2_8;

import server.common.BaseZappServerAPI;
import server.v2_8.common.Customer;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

import java.util.ArrayList;

/**
 * Created by Goni on 5/15/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubmitEvent extends BaseZappServerAPI {

    public static final String EVENT_TIME = "EventTime";
    public static final String CUSTOMER = "Customers";
    public static final String TYPE = "Type";
    public static final String SUB_TYPE = "SubType";
    public static final String TAGS = "Tags";

    private static final String baseUrl = "/alpha/api/SubmitEvent";

    private ArrayList<Customer> customers;
    private String eventTime;
    private String type;
    private String subType;
    private ArrayList<String> tags;


    @JsonGetter(CUSTOMER)
    public ArrayList<Customer> getCustomers() {
        if(customers == null)
            customers = new ArrayList<>();
        return customers;
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    @JsonGetter(TAGS)
    public ArrayList<String> getTags() {
        if(tags == null)
            tags = new ArrayList<>();
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    @JsonGetter(EVENT_TIME)
    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    @JsonGetter(TYPE)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    @JsonGetter(SUB_TYPE)
    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }


    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }
}
