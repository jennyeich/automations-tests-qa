package server.v2_8;

import server.common.BaseServicesAPI;
import server.common.IHasRequestParams;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Goni on 5/17/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetResource  extends BaseServicesAPI implements IHasRequestParams {

    private  final String baseUrl = "GetResource";

    public static final String NAMESPACE = "namespace";
    public static final String VERSION = "version";
    public static final String RESOURCE_ID = "resourceId";
    public static final String REQUESTED_XML = "RequestValidXml";


    private String namespace;
    private String version;
    private Map requestParams;

    public GetResource(String namespace, String version) {
        this.namespace = namespace;
        this.version = version;
        requestParams = new HashMap<>();
        requestParams.put(NAMESPACE, this.namespace);
        requestParams.put(VERSION, this.version);
        requestParams.put(RESOURCE_ID, "Menu");
        requestParams.put(REQUESTED_XML,"Y");

    }

    @JsonGetter(NAMESPACE)
    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    @JsonGetter(VERSION)
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() { return baseUrl;}

    @Override
    @JsonIgnore
    public String getHttpMethod() { return HttpMethod.GET.toString();}

    @Override
    @JsonIgnore
    public Map getRequestParams() {
        return requestParams;
    }
}



