package server.v2_8;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseServicesAPI;

/**
 * Created by Jenny on 4/24/2018.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Consent extends BaseServicesAPI  {

    private  final String baseUrl = "locations/%s/consent";
    private String locationId;
    private boolean registerMembersByConsent;
    public static final String REGISTER_MEMBERS_BY_CONSENT = "registerMembersByConsent";

    public Consent(String locationId) {

        this.locationId =  locationId;

    }

    @JsonGetter(REGISTER_MEMBERS_BY_CONSENT)
    public boolean getConsentIsOn() {
        return registerMembersByConsent;
    }
    public void setConsentIsOn(boolean consentIsOn) {
        this.registerMembersByConsent = consentIsOn;
    }


    @Override
    @JsonIgnore
    public String getRequestURL() { return String.format(baseUrl, locationId);}


    @Override
    @JsonIgnore
    public String getHttpMethod() { return HttpMethod.PUT.toString();}



}






