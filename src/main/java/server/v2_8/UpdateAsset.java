package server.v2_8;

import server.common.BaseZappServerAPI;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateAsset extends BaseZappServerAPI {

	public static final String ACTIVE = "Active";
	public static final String TEMPLATE_KEY = "TemplateKey";
	public static final String TOKEN = "Token";

	private static final String baseUrl = "alpha/api/UpdateAsset";

	private String Active;
	private String TemplateKey;
	private String Token;


	@JsonGetter(ACTIVE)
	public String getActive() {return Active;}
	public void setActive(String Active) {this.Active = Active;}

	@JsonGetter(TEMPLATE_KEY)
	public String getTemplateKey() {
		return TemplateKey;
	}

	public void setTemplateKey(String TemplateKey) {
		this.TemplateKey = TemplateKey;
	}

	@JsonGetter(TOKEN)
	public String getToken() {
		return Token;
	}

	public void setToken(String Token) {
		this.Token = Token;
	}

	@Override
	@JsonIgnore
	public String getRequestURL() {
		return baseUrl;
	}

	@Override
	@JsonIgnore
	public String getHttpMethod() {
		return HttpMethod.POST.toString();
	}
}