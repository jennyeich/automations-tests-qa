package server.v2_8;

import server.common.BaseZappServerAPI;
import server.v2_8.common.Customer;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;
import java.util.ArrayList;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class PayWithBudget extends BaseZappServerAPI {

	public static final String CUSTOMERS = "Customers";
	public static final String ALLOW_PARTIAL_PAYMENT = "AllowPartialPayment";
	public static final String TOTAL_SUM = "TotalSum";
	public static final String TIMESTAMP = "TimeStamp";
	public static final String TRANSACTION_ID = "TransactionID";
	public static final String BRANCH_ID = "BranchID";
	public static final String POS_ID = "PosID";
	public static final String VERIFICATION_CODE = "VerificationCode";
	public static final String CASHIER = "Cashier";

	private static final String baseUrl = "alpha/api/PayWithBudget";

	private ArrayList<Customer> customers;
	private boolean allowPartialPayment;
	private String totalSum;
	private String timeStamp;
	private String transactionId;
	private String branchId;
	private String posId;
	private String verificationCode;
	private String cashier;

	@JsonGetter(CUSTOMERS)
	public ArrayList<Customer> getCustomers() {
		if (customers == null)
			customers = new ArrayList<>();
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	@JsonGetter(ALLOW_PARTIAL_PAYMENT)
	public boolean getAllowPartialPayment() {
		return allowPartialPayment;
	}

	public void setAllowPartialPayment(boolean allowPartialPayment) {
		this.allowPartialPayment = allowPartialPayment;
	}

	@JsonGetter(TOTAL_SUM)
	public String getTotalSum() {
		return totalSum;
	}

	public void setTotalSum(String totalSum) {
		this.totalSum = totalSum;
	}

	@JsonGetter(TIMESTAMP)
	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}


	@JsonGetter(TRANSACTION_ID)
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@JsonGetter(BRANCH_ID)
	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	@JsonGetter(POS_ID)
	public String getPosId() {
		return posId;
	}

	public void setPosId(String posId) {
		this.posId = posId;
	}

	@JsonGetter(VERIFICATION_CODE)
	public String getverificationCode() {
		return verificationCode;
	}

	public void setverificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	@JsonGetter(CASHIER)
	public String getCashier() {
		return cashier;
	}

	public void setCashier(String cashier) {
		this.cashier = cashier;
	}

	@Override
	@JsonIgnore
	public String getRequestURL() {
		return baseUrl;
	}

	@Override
	@JsonIgnore
	public String getHttpMethod() {
		return HttpMethod.POST.toString();
	}
}



