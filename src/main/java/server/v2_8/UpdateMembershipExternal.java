package server.v2_8;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseServicesAPI;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UpdateMembershipExternal extends BaseServicesAPI{

    public static final String FIRST_NAME = "FirstName";
    public static final String LAST_NAME = "LastName";
    public static final String EMAIL = "Email";
    public static final String ALLOW_SMS = "AllowSMS";
    public static final String ALLOW_EMAIL = "AllowEmail";

    private static final String baseUrl = "external/memberships/%s";

    private String firstName;
    private String lastName;
    private String email;
    private Boolean allowSMS;
    private Boolean allowEmail;
    private String phoneNumber;

    public UpdateMembershipExternal(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }


    @JsonProperty(FIRST_NAME)
    public String getFirstName() {return firstName;}
    public void setFirstName(String firstName) {this.firstName = firstName;}

    @JsonProperty(LAST_NAME)
    public String getLastName() {return lastName;}
    public void setLastName(String lastName) {this.lastName = lastName;}

    @JsonProperty(EMAIL)
    public String getEmail() {return email;}
    public void setEmail(String email) {this.email = email;}

    @JsonProperty(ALLOW_SMS)
    public Boolean getAllowSMS() { return allowSMS; }

    public void setAllowSMS(Boolean allowSMS) { this.allowSMS = allowSMS; }

    @JsonProperty(ALLOW_EMAIL)
    public Boolean getAllowEmail() { return allowEmail; }

    public void setAllowEmail(Boolean allowEmail) { this.allowEmail = allowEmail; }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return String.format(baseUrl, phoneNumber);
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.PUT.toString();
    }

}
