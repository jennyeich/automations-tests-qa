package server.v2_8;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import server.common.Response;
import server.v2_8.common.Membership;

/**
 * Created by Jenny on 6/13/2017.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateMembershipResponse extends Response {

    public static final String RESULT = "Result";
    public static final String MEMBERSHIP = "Membership";


    private String result;
    private Membership membership;


    @JsonProperty(RESULT)
    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    @JsonProperty(MEMBERSHIP)
    public Membership getMembership() {
        return membership;
    }
    public void setMembership(Membership membership) {
        this.membership = membership;
    }

}
