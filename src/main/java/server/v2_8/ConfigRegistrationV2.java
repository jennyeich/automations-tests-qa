package server.v2_8;

import com.google.appengine.repackaged.org.codehaus.jackson.annotate.JsonProperty;
import server.common.BaseServicesAPI;
import server.v2_8.common.FieldConfig;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.v2_8.common.Validations;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amit.levi on 27/04/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConfigRegistrationV2 extends BaseServicesAPI {

    private static final String baseUrl = "v2/registration/configuration";

    public static final String LOCATION_ID = "locationId";
    public static final String FIELD_CONFIG = "fieldConfig";
    public static final String VALIDATIONS = "validations";

    private String locationId;
    private List<FieldConfig> fieldConfigList;
    private Validations validations;

    @JsonGetter(LOCATION_ID)
    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    @JsonGetter(FIELD_CONFIG)
    public List<FieldConfig> getFieldConfigList() {
        return fieldConfigList;
    }

    public void addFieldName(FieldConfig fieldConfig) {
        if(this.fieldConfigList == null){
            this.fieldConfigList = new ArrayList<>();
        }
        this.fieldConfigList.add(fieldConfig);
    }

    public void setFieldConfigList(List<FieldConfig> fieldConfigList) {
        this.fieldConfigList = fieldConfigList;
    }

    @JsonProperty(VALIDATIONS)
    public Validations getValidations() {
        return validations;
    }

    public void setValidations(Validations validations) {
        this.validations = validations;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }
}
