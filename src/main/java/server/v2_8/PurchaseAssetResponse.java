package server.v2_8;

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by Goni on 5/16/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PurchaseAssetResponse extends Response {

    public static final String RESULT = "Result";
    public static final String CODE = "Code";

    private String result;
    private String code;

    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @JsonGetter(CODE)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

}