package server.v2_8;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubmitPurchase extends PurchaseAPI {

	private static final String baseUrl = "alpha/api/SubmitPurchase";

	@Override
	@JsonIgnore
	public String getRequestURL() {
		return baseUrl;
	}

	@Override
	@JsonIgnore
	public String getHttpMethod() {
		return HttpMethod.POST.toString();
	}

}

	


