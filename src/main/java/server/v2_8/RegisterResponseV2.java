package server.v2_8;

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by amit.levi on 27/04/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@Deprecated
public class RegisterResponseV2 extends Response {

    public static final String MEMBERSHIP_STATUS = "MembershipStatus";
    public static final String USER_KEY = "UserKey";
    public static final String MEMBERSHIP_KEY = "Key";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String BIRTHDAY = "Birthday";
    public static final String ANNIVERSARY = "Anniversary";

    private String membershipStatus;
    private String userKey;
    private String membershipKey;
    private String phoneNumber;
    private String birthday;
    private String anniversary;


    @JsonGetter(MEMBERSHIP_STATUS)
    public String getMembershipStatus() {
        return membershipStatus;
    }

    public void setMembershipStatus(String membershipStatus) {
        this.membershipStatus = membershipStatus;
    }

    @JsonGetter(USER_KEY)
    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    @JsonGetter(MEMBERSHIP_KEY)
    public String getMembershipKey() {
        return membershipKey;
    }

    public void setMembershipKey(String membershipKey) {
        this.membershipKey = membershipKey;
    }

    @JsonGetter(PHONE_NUMBER)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAnniversary() {
        return anniversary;
    }

    public void setAnniversary(String anniversary) {
        this.anniversary = anniversary;
    }
}
