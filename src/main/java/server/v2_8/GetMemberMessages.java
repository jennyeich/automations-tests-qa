package server.v2_8;

import server.common.IHasRequestParams;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Goni on 6/27/2017.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class GetMemberMessages extends PurchaseAPI implements IHasRequestParams {

    public static final String LIMIT = "Limit";
    public static final String OFFSET = "Offset";
    public static final String AUTO_MARKED_AS_READ = "AutoMarkAsRead";
    public static final String MEMBERSHIP_KEY = "MembershipKey";


    private static final String baseUrl = "alpha/api/GetMemberMessages";

    private Map requestParams;
    private String membershipKey;

    public GetMemberMessages(String membershipKey) {
        requestParams = new HashMap<>();
        this.membershipKey = membershipKey;
        requestParams.put(LIMIT, "1");
        requestParams.put(OFFSET, "0");
        requestParams.put(AUTO_MARKED_AS_READ, "Y");
        requestParams.put(MEMBERSHIP_KEY,membershipKey);

    }

    @JsonGetter
    public String getMembershipKey() {
        return membershipKey;
    }

    public void setMembershipKey(String membershipKey) {
        this.membershipKey = membershipKey;
    }
    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

    @Override
    public Map getRequestParams() {
        return requestParams;
    }
}

