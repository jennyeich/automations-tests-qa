package server.v2_8;

import server.common.Response;
import server.common.commonResponse.LocationCode;
import server.common.commonResponse.RedeemAssetResult;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by amit.levi on 30/04/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientRedeemResponse extends Response {

    public static final String RESULT = "Result";
    public static final String LOCATION_CODE = "LocationCode";
    public static final String REDEEM_ASSET_RESULT = "RedeemAssetResult";

    private String result;
    private LocationCode locationCode;
    private RedeemAssetResult redeemAssetResult;

    public ClientRedeemResponse() {
    }

    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @JsonGetter(LOCATION_CODE)
    public LocationCode getLocationCode() {
        return locationCode;
    }

    public void setLocationCode(LocationCode locationCode) {
        this.locationCode = locationCode;
    }

    @JsonGetter(REDEEM_ASSET_RESULT)
    public RedeemAssetResult getRedeemAssetResult() {
        return redeemAssetResult;
    }

    public void setRedeemAssetResult(RedeemAssetResult redeemAssetResult) {
        this.redeemAssetResult = redeemAssetResult;
    }
}
