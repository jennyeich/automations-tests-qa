package server.v2_8;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import server.common.Response;

/**
 * Created by Jenny on 7/31/2018.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetUserAtLocationResponse extends Response {

    public static final String STATUS = "Status";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String FIRST_NAME = "FirstName";
    public static final String LAST_NAME = "LastName";
    public static final String EMAIL = "Email";
    public static final String MEMBERSHIP_STATUS = "MembershipStatus";
    public static final String MEMBERSHIP_KEY = "Key";
    public static final String USER_KEY = "UserKey";
    public static final String REGISTRATION_SOURCE = "RegistrationSource";
    public static final String CONSENT = "Consent";
    public static final String CONSENT_FOR_HUB = "ConsentForHub";
    public static final String REFERRAL_CODE = "ReferralCode";

    private String status;
    private String phoneNumber;
    private String firstName;
    private String lastName;
    private String email;
    private String membershipStatus;
    private String membershipKey;
    private String userKey;
    private String registrationSource;
    private String consent;
    private String consentForHub;
    private String referralCode;




    @JsonGetter(REFERRAL_CODE)
    public String getReferralCode() { return referralCode; }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    @JsonGetter(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty(PHONE_NUMBER)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @JsonProperty(FIRST_NAME)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty(LAST_NAME)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty(EMAIL)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @JsonProperty(MEMBERSHIP_STATUS)
    public String getMembershipStatus() {
        return membershipStatus;
    }

    public void setMembershipStatus(String membershipStatus) {
        this.membershipStatus = membershipStatus;
    }

    @JsonProperty(MEMBERSHIP_KEY)
    public String getMembershipKey() {
        return membershipKey;
    }

    public void setMembershipKey(String key) {
        this.membershipKey = key;
    }

    @JsonProperty(USER_KEY)
    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    @JsonProperty(REGISTRATION_SOURCE)
    public String getRegistrationSource() {
        return registrationSource;
    }

    public void setRegistrationSource(String registrationSource) {
        this.registrationSource = registrationSource;
    }

    @JsonProperty(CONSENT)
    public String getConsent() {
        return consent;
    }

    public void setConsent(String consent) {
        this.consent = consent;
    }

    @JsonProperty(CONSENT_FOR_HUB)
    public String getConsentForHub() {
        return consentForHub;
    }

    public void setConsentForHub(String consentForHub) {
        this.consentForHub = consentForHub;
    }

}
