package server.v2_8;

import server.common.BaseServicesAPI;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by amit.levi on 27/04/2017.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ConfigRegistration extends BaseServicesAPI {

    private static final String baseUrl = "registration/configuration";
    public static final String LOCATION_ID = "locationId";
    public static final String IDENTIFICATION_FIELD = "identificationField";
    public static final String FIELD_NAME_MAP = "fieldNameMap";

    private String locationId;
    private String identificationField;
    private Map fieldNameMap;

    @JsonGetter(FIELD_NAME_MAP)
    public Map getFieldNameMap() {
        return fieldNameMap;
    }

    public void addFieldNameMap(String key, String value) {
        if(this.fieldNameMap == null){
            this.fieldNameMap = new HashMap();
        }
        this.fieldNameMap.put(key, value);
    }

    public void setFieldNameMap(Map fieldNameMap) {
        this.fieldNameMap = fieldNameMap;
    }

    @JsonGetter(LOCATION_ID)
    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    @JsonGetter(IDENTIFICATION_FIELD)
    public String getIdentificationField() {
        return identificationField;
    }

    public void setIdentificationField(String identificationField) {
        this.identificationField = identificationField;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }
}
