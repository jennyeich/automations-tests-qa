package server.v2_8;

import server.common.BaseZappServerAPI;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

/**
 * Created by amit.levi on 30/04/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

public  class ClientRedeem extends BaseZappServerAPI {

    public static final String REDEEM_CODE = "redeemCode";
    public static final String LOCATION_ID = "locationId";


    private static final String baseUrl = "alpha/api/ClientRedeem";

    private String redeemCode;
    private String locationId;

    @JsonGetter(REDEEM_CODE)
    public String getRedeemCode() {
        return redeemCode;
    }

    public void setRedeemCode(String redeemCode) {
        this.redeemCode = redeemCode;
    }

    @JsonGetter(LOCATION_ID)
    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }
}