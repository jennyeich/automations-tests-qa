package server.v2_8.common;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;

/**
 * Created by Lior on 12/1/2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class OptionalBenefit {
    public static final String MULTI_SELECT_MAX = "MultiSelectMax";
    public static final String OPTIONS = "Options";

    private Integer multiSelectMax;
    private ArrayList<String> options;

    public OptionalBenefit() {
    }

    @JsonGetter(MULTI_SELECT_MAX)
    public Integer getMultiSelectMax() {
        return multiSelectMax;
    }

    public void setMultiSelectMax(Integer multiSelectMax) {
        this.multiSelectMax = multiSelectMax;
    }

    @JsonGetter(OPTIONS)
    public ArrayList<String> getOptions() {
        if (options==null){
            options=new ArrayList<>();
        }
        return options;
    }

    public void setOptions(ArrayList<String> options) {
        this.options = options;
    }
}
