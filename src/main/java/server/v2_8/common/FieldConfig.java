package server.v2_8.common;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by amit.levi on 30/04/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FieldConfig {

    public static final String TYPE_NAME = "typeName";
    public static final String EXTERNAL_NAME = "externalName";
    public static final String DISPLAY_NAME = "displayName";
    public static final String ENABLE_UPDATE = "enableUpdate";


    private String typeName;
    private String externalName;
    private String displayName;
    private Boolean enableUpdate;

    public FieldConfig(String typeName, Boolean enableUpdate) {
        this.typeName = typeName;
        this.externalName = typeName;
        this.displayName = typeName;
        this.enableUpdate = enableUpdate;
    }

    @JsonGetter(TYPE_NAME)
    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @JsonGetter(EXTERNAL_NAME)
    public String getExternalName() {
        return externalName;
    }

    public void setExternalName(String externalName) {
        this.externalName = externalName;
    }

    @JsonGetter(DISPLAY_NAME)
    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @JsonGetter(ENABLE_UPDATE)
    public Boolean getEnableUpdate() {
        return enableUpdate;
    }

    public void setEnableUpdate(Boolean enableUpdate) {
        this.enableUpdate = enableUpdate;
    }
}
