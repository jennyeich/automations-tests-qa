package server.v2_8.common
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * .
 * Created by eliran on 22/06/2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
class Customer  {
	@JsonProperty("PhoneNumber")
	var phoneNumber: String?=null
	@JsonProperty("AppClientID")
	var appClientID: String?=null
	@JsonProperty("GovID")
	var govId: String?=null
	@JsonProperty("MemberID")
	var memberId: String?=null
	@JsonProperty("TemporaryToken")
	var temporaryToken: String?=null
}

