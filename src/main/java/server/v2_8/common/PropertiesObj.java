package server.v2_8.common;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Goni on 10/15/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PropertiesObj {

    private Map<String ,Object> props = new HashMap<>();

    public Map<String, Object> getProps() {
        return props;
    }

    public void setProps(Map<String, Object> props) {
        this.props = props;
    }

    public void addProp(String key, Map<String,Object> map) {
        this.props.put(key,map);
    }
}

