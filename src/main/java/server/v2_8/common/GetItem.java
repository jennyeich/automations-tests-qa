package server.v2_8.common;

import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseZappServerAPI;
import server.common.IHasRequestParams;
import utils.PropsReader;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lior.shory on 01/02/2017.
 */
public class GetItem extends BaseZappServerAPI implements IHasRequestParams {

    public static final String KEY = "Key";

    HashMap<String, String> requestParams = new HashMap<>();

    public GetItem() throws IOException {
        requestParams.put("Token", PropsReader.getPropValuesForEnv("serverToken"));
    }
    public void setKey(String key){
        requestParams.put(KEY, key);
    }

    public String getKey(){
        return requestParams.get(KEY);
    }

    @Override
    public String getRequestURL() {
        return "alpha/common/GetItem";
    }

    @Override
    public String getHttpMethod() {
        return HttpMethod.GET.toString();
    }

    @Override
    public Map getRequestParams() {
        return requestParams;
    }
}
