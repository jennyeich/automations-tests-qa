package server.v2_8.common;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;

/**
 * Created by Goni on 6/27/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Message {

    public static final String TAGS = "Tags";
    public static final String CONTENT = "Content";
    public static final String SENDER_NAME = "SenderName";
    public static final String TITLE = "Title";

    private ArrayList<String> tags;
    public String content;
    public String senderName;
    public String title;

    @JsonGetter(TAGS)
    public ArrayList<String> getTags() {
        if (tags==null){
            tags=new ArrayList<>();
        }
        return tags;
    }

    public void setTags(ArrayList<String> tag) {
        this.tags = tag;
    }

    @JsonGetter(CONTENT)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @JsonGetter(SENDER_NAME)
    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    @JsonGetter(TITLE)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
