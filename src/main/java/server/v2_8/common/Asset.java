package server.v2_8.common;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import server.v2_8.base.ICleanable;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

/**
 * Created by Lior on 11/30/2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Asset implements ICleanable {
    public static final String NAME = "Name";
    public static final String STATUS = "Status";
    public static final String DESCRIPTION = "Description";
    public static final String KEY = "Key";
    public static final String VALID_UNTIL = "ValidUntil";
    public static final String TYPE = "Type";
    public static final String REDEEMABLE = "Redeemable";
    public static final String CANNOT_REDEEM_REASONS = "CannotRedeemReasons";
    public static final String ARCHIVED = "Archived";
    public static final String IMAGE = "image";

    private String name;
    private String status;
    private String description;
    private String key;
    private Date validUntil;
    private String type;
    private String redeemable;
    private String cannotRedeemReasons;
    private Boolean archived;
    private String image;

    public Asset() {
    }

    public Asset(String name, String status, String description, String key, Date validUntil, String type, String redeemable, String cannotRedeemReasons, Boolean archived){
        this.name = name;
        this.status = status;
        this.description = description;
        this.key = key;
        this.validUntil = validUntil;
        this.type = type;
        this.redeemable = redeemable;
        this.cannotRedeemReasons = cannotRedeemReasons;
        this.archived = archived;
    }

    @JsonGetter(IMAGE)
    public String getImage() {return image;}
    public void setImage(String image) {this.image = image;}

    @JsonGetter(NAME)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonGetter(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonGetter(DESCRIPTION)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonGetter(KEY)
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @JsonGetter(VALID_UNTIL)
    @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    @JsonGetter(TYPE)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonGetter(REDEEMABLE)
    public String getRedeemable() {
        return redeemable;
    }

    public void setRedeemable(String redeemable) {
        this.redeemable = redeemable;
    }

    @JsonGetter(CANNOT_REDEEM_REASONS)
    public String getCannotRedeemReasons() {
        return cannotRedeemReasons;
    }

    public void setCannotRedeemReasons(String cannotRedeemReasons) {
        this.cannotRedeemReasons = cannotRedeemReasons;
    }

    @JsonGetter(ARCHIVED)
    public Boolean getArchived() {
        return archived;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    @Override
    public void clean(String apiKey) throws Exception {
        DeactivateAsset.deactivate(apiKey, this.getKey());
    }
}
