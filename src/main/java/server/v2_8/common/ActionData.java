package server.v2_8.common;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;

/**
 * Created by Goni on 5/24/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ActionData {

    public static final String ACTION = "Action";

    private ArrayList<Action> actions;

    @JsonGetter(ACTION)
    public ArrayList<Action> getActions() {
        if(actions == null)
            actions = new ArrayList<>();
        return actions;
    }

    public void setActions(ArrayList<Action> actions) {
        this.actions = actions;
    }


}
