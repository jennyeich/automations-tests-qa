package server.v2_8.common;

import server.common.UpdateAsset;
import server.common.Response;
import server.v2_8.base.ZappServerErrorResponse;
import utils.JsonUtils;

import java.util.HashMap;

/**
 * Created by gili on 2/1/17.
 */
public class DeactivateAsset {


    public static void deactivate(String apiKey, String assetKey) throws Exception {
        GetItem getItem = new GetItem();
        getItem.setKey(assetKey);
        Response response = (Response) getItem.SendRequestByApiKey(apiKey, Response.class);
        if(response instanceof ZappServerErrorResponse){
            throw new Exception(((ZappServerErrorResponse)response).getErrorMessage());
        }

        // Set Active to N
        response.getRawObject().put("Active", "false");
        // replace 'Key' with 'assetKey'
        response.getRawObject().put("assetKey", response.getRawObject().get("Key"));


        UpdateAsset updateAsset = new UpdateAsset();
        updateAsset.setAsset((HashMap) JsonUtils.jsonToMap(response.getRawObject()));
        updateAsset.SendRequestByApiKey(apiKey, Response.class);
        //wait for update data store
        Thread.sleep(7000);
    }
}
