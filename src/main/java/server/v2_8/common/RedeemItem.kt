package server.v2_8.common

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * .
 * Created by eliran on 22/06/2016.
 */
@JsonInclude(Include.NON_NULL)
open class RedeemItem {
   @JsonProperty("RedeemCode")
   var redeemCode: String? = null
    @JsonProperty("AssetKey")
    var assetKey: String? = null

}