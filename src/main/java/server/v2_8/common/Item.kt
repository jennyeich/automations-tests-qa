package server.v2_8.common
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

/**
 * .
 * Created by eliran on 22/06/2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
open class Item {
	@JsonProperty("ItemCode")
	var itemCode: String?= null
	@JsonProperty("ItemName")
	var itemName: String?=null
	@JsonProperty("DepartmentCode")
	var departmentCode: String?=null
	@JsonProperty("DepartmentName")
	var departmentName: String?=null
	@JsonProperty("Quantity")
	var quantity: Int? = null
	@JsonProperty("Amount")
	var amount: Double? = null
	@JsonProperty("Price")
	var price: Int? = null
	@JsonProperty("Tags")
	var tags: ArrayList<String>?= ArrayList()
	@JsonProperty("LineID")
	var lineID: String?= null
}
