package server.v2_8.common;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

/**
 * Created by Goni on 10/15/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Validations {

    public static final String TYPE = "type";
    public static final String PROPERTIES = "properties";

    private String type = "object";
    private PropertiesObj properties = new PropertiesObj();

    @JsonProperty(TYPE)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty(PROPERTIES)
    public PropertiesObj getProperties() {
        return properties;
    }

    public void setProperties(PropertiesObj properties) {
        this.properties = properties;
    }

    public void addProperties(String key,  Map<String,Object> map) {
        this.properties.addProp(key,map);
    }
}

