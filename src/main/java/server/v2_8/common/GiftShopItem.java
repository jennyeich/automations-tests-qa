package server.v2_8.common;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by Goni on 5/17/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GiftShopItem {

    public static final String ITEM_ID = "ItemID";
    public static final String ASSET_KEY = "assetKey";
    public static final String PRICE = "price";


    private String itemID;
    private String assetKey;
    private String price;

    @JsonGetter(ITEM_ID)
    public String getItemID() {
        return itemID;
    }

    public void setItemID(String itemID) {
        this.itemID = itemID;
    }
    @JsonGetter(ASSET_KEY)
    public String getAssetKey() {
        return assetKey;
    }

    public void setAssetKey(String assetKey) {
        this.assetKey = assetKey;
    }
    @JsonGetter(PRICE)
    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
