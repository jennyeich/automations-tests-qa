package server.v2_8.common;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Created by Lior on 11/30/2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Membership {

    public static final String FIRST_NAME = "FirstName";
    public static final String LAST_NAME = "LastName";
    public static final String PUSH_NOTIFICATION_ENABLED = "PushNotificationEnabled";
    public static final String LOCATION_ENABLED = "LocationEnabled";
    public static final String MOBILE_APP_USED = "MobileAppUsed";
    public static final String MOBILE_APP_USED_LAST_DATE = "MobileAppUsedLastDate";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String ALLOW_SMS = "AllowSMS";
    public static final String ALLOW_EMAIL = "AllowEmail";
    public static final String GOV_ID = "GovID";
    public static final String EMAIL = "Email";
    public static final String STATUS = "Status";
    public static final String MEMBER_ID = "MemberID";
    public static final String EXTRA_DATA = "ExtraData";
    public static final String LOCATION_ID = "LocationID";
    public static final String POINTS = "Points";
    public static final String ACCUMULATED_POINTS = "AccumulatedPoints";
    public static final String UNWEIGHTED_BUDGET_POINTS = "UnweightedBudgetPoints";
    public static final String WEIGHTED_POINTS = "WeightedPoints";
    public static final String BIRTHDAY = "Birthday";
    public static final String ANNIVERSARY = "Anniversary";
    public static final String GENDER = "Gender";
    public static final String GENERIC_STRING1 = "GenericString1";
    public static final String GENERIC_INTEGER1 = "GenericInteger1";
    public static final String GENERIC_CHECK_BOX1 = "GenericCheckBox1";
    public static final String GENERIC_DATE1 = "GenericDate1";
    public static final String TAG = "Tag";
    public static final String ASSETS = "Assets";
    public static final String MEMBERSHIP_STATUS = "MembershipStatus";
    public static final String USER_KEY = "UserKey";
    public static final String KEY = "Key";
    public static final String REGISTRATION_SOURCE = "RegistrationSource";
    public static final String CONSENT = "Consent";
    public static final String CONSENT_FOR_HUB = "ConsentForHub";

    private String firstName;
    private String lastName;
    private Boolean pushNotificationEnabled;
    private Boolean locationEnabled;
    private Boolean mobileAppUsed;
    private Date mobileAppUsedLastDate;
    private String phoneNumber;
    private Boolean allowSMS;
    private Boolean allowEmail;
    private String govID;
    private String email;
    private String status;
    private String memberID;
    private Map extraData;
    private String locationID;
    private Integer points;
    private Integer accumulatedPoints;
    private Integer unweightedBudgetPoints;
    private Integer weightedPoints;
    private String birthday;
    private String anniversary;
    private String gender;
    private String genericString1;
    private Integer genericInteger1;
    private Boolean genericCheckBox1;
    private Date genericDate1;
    private ArrayList<String> tags;
    private ArrayList<Asset> assets;
    private String membershipStatus;
    private String userKey;
    private String key;
    private String registrationSource;
    private String consent;
    private String consentForHub;


    @JsonProperty(KEY)
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }



    public Membership() {
    }

    public Asset findAssetByName(String name){
        for (Asset asset: assets){
            if (asset.getName().equals(name)) {
                return asset;
            }
        }
        return null;
    }

    public Asset findAssetByKey(String key){
        for (Asset asset: assets){
            if (asset.getKey().equals(key)) {
                return asset;
            }
        }
        return null;
    }

    @JsonProperty(FIRST_NAME)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty(LAST_NAME)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty(PUSH_NOTIFICATION_ENABLED)
    public Boolean getPushNotificationEnabled() {
        return pushNotificationEnabled;
    }

    public void setPushNotificationEnabled(Boolean pushNotificationEnabled) {
        this.pushNotificationEnabled = pushNotificationEnabled;
    }

    @JsonProperty(LOCATION_ENABLED)
    public Boolean getLocationEnabled() {
        return locationEnabled;
    }

    public void setLocationEnabled(Boolean locationEnabled) {
        this.locationEnabled = locationEnabled;
    }

    @JsonProperty(MOBILE_APP_USED)
    public Boolean getMobileAppUsed() {
        return mobileAppUsed;
    }

    public void setMobileAppUsed(Boolean mobileAppUsed) {
        this.mobileAppUsed = mobileAppUsed;
    }

    @JsonProperty(MOBILE_APP_USED_LAST_DATE)
    @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    public Date getMobileAppUsedLastDate() {
        return mobileAppUsedLastDate;
    }

    public void setMobileAppUsedLastDate(Date mobileAppUsedLastDate) {
        this.mobileAppUsedLastDate = mobileAppUsedLastDate;
    }

    @JsonProperty(PHONE_NUMBER)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @JsonProperty(ALLOW_SMS)
    public Boolean getAllowSMS() {
        return allowSMS;
    }

    public void setAllowSMS(Boolean allowSMS) {
        this.allowSMS = allowSMS;
    }

    @JsonProperty(ALLOW_EMAIL)
    public Boolean getAllowEmail() {
        return allowEmail;
    }

    public void setAllowEmail(Boolean allowEmail) {
        this.allowEmail = allowEmail;
    }

    @JsonProperty(GOV_ID)
    public String getGovID() {
        return govID;
    }

    public void setGovID(String govID) {
        this.govID = govID;
    }

    @JsonProperty(EMAIL)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @JsonProperty(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty(MEMBER_ID)
    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    @JsonProperty(EXTRA_DATA)
    public Map getExtraData() {
        return extraData;
    }

    public void setExtraData(Map extraData) {
        this.extraData = extraData;
    }

    @JsonProperty(LOCATION_ID)
    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    @JsonProperty(POINTS)
    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }

    @JsonProperty(ACCUMULATED_POINTS)
    public Integer getAccumulatedPoints() {
        return accumulatedPoints;
    }

    public void setAccumulatedPoints(Integer accumulatedPoints) {
        this.accumulatedPoints = accumulatedPoints;
    }

    @JsonProperty(UNWEIGHTED_BUDGET_POINTS)
    public Integer getUnweightedBudgetPoints() {
        return unweightedBudgetPoints;
    }

    public void setUnweightedBudgetPoints(Integer unweightedBudgetPoints) {
        this.unweightedBudgetPoints = unweightedBudgetPoints;
    }

    @JsonProperty(WEIGHTED_POINTS)
    public Integer getWeightedPoints() {
        return weightedPoints;
    }

    public void setWeightedPoints(Integer weightedPoints) {
        this.weightedPoints = weightedPoints;
    }

    @JsonProperty(BIRTHDAY)
   // @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @JsonProperty(ANNIVERSARY)
   // @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    public String getAnniversary() {
        return anniversary;
    }

    public void setAnniversary(String anniversary) {
        this.anniversary = anniversary;
    }

    @JsonProperty(GENDER)
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @JsonProperty(GENERIC_STRING1)
    public String getGenericString1() {
        return genericString1;
    }

    public void setGenericString1(String genericString1) {
        this.genericString1 = genericString1;
    }

    @JsonProperty(GENERIC_INTEGER1)
    public Integer getGenericInteger1() {
        return genericInteger1;
    }

    public void setGenericInteger1(Integer genericInteger1) {
        this.genericInteger1 = genericInteger1;
    }

    @JsonProperty(GENERIC_CHECK_BOX1)
    public Boolean getGenericCheckBox1() {
        return genericCheckBox1;
    }

    public void setGenericCheckBox1(Boolean genericCheckBox1) {
        this.genericCheckBox1 = genericCheckBox1;
    }

    @JsonProperty(GENERIC_DATE1)
    @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    public Date getGenericDate1() {
        return genericDate1;
    }

    public void setGenericDate1(Date genericDate1) {
        this.genericDate1 = genericDate1;
    }

    @JsonProperty(TAG)
    public ArrayList<String> getTags() {
        if (tags==null){
            tags=new ArrayList<>();
        }
        return tags;
    }

    public void setTags(ArrayList<String> tag) {
        this.tags = tag;
    }

    @JsonProperty(ASSETS)
    public ArrayList<Asset> getAssets() {
        if (assets==null){
            assets=new ArrayList<>();
        }
        return assets;
    }

    public void setAssets(ArrayList<Asset> assets) {
        this.assets = assets;
    }

    @JsonProperty(USER_KEY)
    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }
    @JsonProperty(MEMBERSHIP_STATUS)
    public String getMembershipStatus() {
        return membershipStatus;
    }

    public void setMembershipStatus(String membershipStatus) {
        this.membershipStatus = membershipStatus;
    }



    @JsonProperty(REGISTRATION_SOURCE)
    public String getRegistrationSource() {
        return registrationSource;
    }

    public void setRegistrationSource(String registrationSource) {
        this.registrationSource = registrationSource;
    }
    @JsonProperty(CONSENT)
    public String getConsent() {
        return consent;
    }

    public void setConsent(String consent) {
        this.consent = consent;
    }

    @JsonProperty(CONSENT_FOR_HUB)
    public String getConsentForHub() {
        return consentForHub;
    }

    public void setConsentForHub(String consentForHub) {
        this.consentForHub = consentForHub;
    }
}
