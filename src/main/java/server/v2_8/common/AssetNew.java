package server.v2_8.common;

import server.v2_8.base.ICleanable;
import com.fasterxml.jackson.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

/**
 * Created by Ariel on 6/13/2016.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssetNew implements ICleanable {
    public static final String KEY = "Key";
    public static final String CREATED_ON = "CreatedOn";
    public static final String LAST_UPDATE = "LastUpdate";
    public static final String KIND = "Kind";
    public static final String ENTITY_ID = "EntityId";
    public static final String DESCRIPTION = "Description";
    public static final String NAME = "Name";
    public static final String TYPE = "Type";
    public static final String COST = "Cost";
    public static final String VALUE = "Value";
    public static final String STATUS = "Status";
    public static final String RANDOM = "Random";
    public static final String ARCHIVED = "Archived";
    public static final String ACTIVE = "Active";
    public static final String TEMPLATE = "Template";
    public static final String TEMPLATE_KEY = "TemplateKey";
    public static final String STOCK_TRACKED = "StockTracked";
    public static final String DATA = "Data";
    public static final String TAG = "Tag";
    public static final String VALID_AT_LOCATION_IDS = "ValidAtLocationIDs";
    public static final String OWNING_USER = "OwningUser";
    public static final String OWNING_MEMBERS = "OwningMembers";
    public static final String REDEEM_CODE_KEY = "RedeemCodeKey";
    public static final String EXTERNAL_BENEFITS = "ExternalBenefits";
    public static final String ASSET_SOURCE = "AssetSource";
    public static final String LAST_ASSIGN_DATE = "LastAssignDate";
    public static final String REDEEM_ACTION = "RedeemAction";
    public static final String IMAGE = "image";
    public static final String REDEEM_LOCK_EXPIRATION = "RedeemLockExpiration";
    public static final String NOTIFY_ACTION = "NotifyAction";
    public static final String REDEEM_LOCK = "RedeemLock";
    public static final String REDEEM_STATE = "RedeemState";
    public static final String REDEEM_CODE = "redeemCode";

    private String key;
    public Date createOn;
    private Date lastUpdate;
    private String kind;
    private String entityId;
    private String description;
    private String name;
    private String type;
    private String cost;
    private String value;
    private String status;
    private String random;
    private Boolean archived;
    private String active;
    private String template;
    private String templateKey;
    private String stockTracked;
    private Map data;
    private ArrayList tag;
    private ArrayList validAtLocationIds;
    private ArrayList owningUser;
    private ArrayList owningMembers;
    private String redeemCodeKey;
    private Map externalBenefits;
    private Map assetSource;
    private Date lastAssignDate;
    private Map redeemAction;
    private String image;
    private Date redeemLockExpiration;
    private Map notifyAction;
    private String redeemLock;
    private String redeemState;
    private String redeemCode;


    @JsonGetter(KEY)
    public String getKey() { return key; }
    public void setKey(String key) { this.key = key; }

    @JsonGetter(CREATED_ON)
    @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    public Date getCreateOn() { return createOn; }
    public void setCreatedOn(Date createOn) { this.createOn = createOn; }

    @JsonGetter(LAST_UPDATE)
    @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    public Date getLastUpdate() { return lastUpdate; }
    public void setLastUpdate(Date lastUpdate) { this.lastUpdate = lastUpdate; }

    @JsonGetter(KIND)
    public String getKind() { return kind; }
    public void setKind(String kind) { this.kind = kind; }

    @JsonGetter(ENTITY_ID)
    public String getEntityId() { return entityId; }
    public void setEntityId(String entityId) { this.entityId = entityId; }

    @JsonGetter(DESCRIPTION)
    public String getDescription() { return description; }
    public void setDescription(String description) { this.description = description; }

    @JsonGetter(NAME)
    public String getName() { return name; }
    public void setName(String name) { this.name = name; }

    @JsonGetter(TYPE)
    public String getType() { return type; }
    public void setType(String type) { this.type = type; }

    @JsonGetter(COST)
    public String getCost() { return cost; }
    public void setCost(String cost) { this.cost = cost; }

    @JsonGetter(VALUE)
    public String getValue() { return value; }
    public void setValue(String value) { this.value = value; }

    @JsonGetter(STATUS)
    public String getStatus() { return status; }
    public void setStatus(String status) { this.status = status; }

    @JsonGetter(RANDOM)
    public String getRandom() { return random; }
    public void setRandom(String random) { this.random = random; }

    @JsonGetter(ARCHIVED)
    public Boolean getArchived() { return archived; }
    public void setArchived(Boolean archived) { this.archived = archived; }

    @JsonGetter(ACTIVE)
    public String getActive() { return active; }
    public void setActive(String active) { this.active = active; }

    @JsonGetter(TEMPLATE)
    public String getTemplate() { return template; }
    public void setTemplate(String template) { this.template = template; }

    @JsonGetter(TEMPLATE_KEY)
    public String getTemplateKey() { return templateKey; }
    public void setTemplateKey(String templateKey) { this.templateKey = templateKey; }

    @JsonGetter(STOCK_TRACKED)
    public String getStockTracked() { return stockTracked; }
    public void setStockTracked(String stockTracked) { this.stockTracked = stockTracked; }

    @JsonGetter(DATA)
    public Map getData() { return data; }
    public void setData(Map data) { this.data = data; }

    @JsonGetter(TAG)
    public ArrayList getTag() { return tag; }
    public void setTag(ArrayList tag) { this.tag = tag; }

    @JsonGetter(VALID_AT_LOCATION_IDS)
    public ArrayList getValidAtLocationIds() { return validAtLocationIds; }
    public void setValidAtLocationIds(ArrayList validAtLocationIds) { this.validAtLocationIds = validAtLocationIds; }

    @JsonGetter(OWNING_USER)
    public ArrayList getOwningUser() { return owningUser; }
    public void setOwningUser(ArrayList owningUser) { this.owningUser = owningUser; }

    @JsonGetter(OWNING_MEMBERS)
    public ArrayList getOwningMembers() { return owningMembers; }
    public void setOwningMembers(ArrayList owningMembers) { this.owningMembers = owningMembers; }

    @JsonGetter(REDEEM_CODE_KEY)
    public String getRedeemCodeKey() { return redeemCodeKey; }
    public void setRedeemCodeKey(String redeemCodeKey) { this.redeemCodeKey = redeemCodeKey; }

    @JsonGetter(EXTERNAL_BENEFITS)
    public Map getExternalBenefits() { return externalBenefits; }
    public void setExternalBenefits(Map externalBenefits) { this.externalBenefits = externalBenefits; }

    @JsonGetter(ASSET_SOURCE)
    public Map getAssetSource() { return assetSource; }
    public void setAssetSource(Map assetSource) { this.assetSource = assetSource; }

    @JsonGetter(LAST_ASSIGN_DATE)
    @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    public Date getLastAssignDate() { return lastAssignDate; }
    public void setLastAssignDate(Date lastAssignDate) { this.lastAssignDate = lastAssignDate; }

    @JsonGetter(REDEEM_ACTION)
    public Map getRedeemAction() { return redeemAction; }
    public void setRedeemAction(Map redeemAction) { this.redeemAction = redeemAction; }

    @JsonGetter(IMAGE)
    public String getImage() { return image; }
    public void setImage(String image) { this.image = image; }

    @JsonGetter(REDEEM_LOCK_EXPIRATION)
    @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    public Date getRedeemLockExpiration() { return redeemLockExpiration; }
    public void setRedeemLockExpiration(Date redeemLockExpiration) { this.redeemLockExpiration = redeemLockExpiration; }

    @JsonGetter(NOTIFY_ACTION)
    public Map getNotifyAction() { return notifyAction; }
    public void setNotifyAction(Map notifyAction) { this.notifyAction = notifyAction; }

    @JsonGetter(REDEEM_LOCK)
    public String getRedeemLock() { return redeemLock; }
    public void setRedeemLock(String redeemLock) { this.redeemLock = redeemLock; }

    @JsonGetter(REDEEM_STATE)
    public String getRedeemState() { return redeemState; }
    public void setRedeemState(String redeemState) { this.redeemState = redeemState; }

    @JsonGetter(REDEEM_CODE)
    public String getRedeemCode() { return redeemCode; }
    public void setRedeemCode(String redeemCode) { this.redeemCode = redeemCode; }

    @Override
    public void clean(String apiKey) throws Exception {
        DeactivateAsset.deactivate(apiKey, this.getKey());
    }

    @JsonAnySetter
    public void setProperty(String key,Object value){

    }

}
