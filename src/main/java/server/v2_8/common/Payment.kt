package server.v2_8.common
import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * .
 * Created by eliran on 22/06/2016.
 */
open class Payment @JsonCreator constructor(
	@JsonProperty("Type")
	var type: String?=null,
	@JsonProperty("Details")
	var details: String?=null,
	@JsonProperty("Sum")
	var sum: String="0") {

}

