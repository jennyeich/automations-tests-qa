package server.v2_8;

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/*
 * Created by Jenny on 19/06/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GenerateTokenResponse extends Response {

    public static final String EXPIRATION_DATE = "ExpirationDate";
    public static final String TOKEN = "TemporaryToken";

    private String expirationDate;
    private String token;

    public GenerateTokenResponse() {}
    @JsonGetter(EXPIRATION_DATE)
    public String getExpirationDate() {
        return expirationDate;
    }
    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    @JsonGetter(TOKEN)
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
}
