package server.v2_8;


/**
 * Created by amit.levi on 25/04/2017.
 */
import server.common.BaseServicesAPI;
import server.common.IHasRequestParams;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

import java.util.HashMap;
import java.util.Map;


@JsonInclude(JsonInclude.Include.NON_NULL)
 public class GetPurchase extends BaseServicesAPI implements IHasRequestParams {

    private  final String baseUrl = "purchases";

    public static final String LOCATION_ID = "locationId";
    public static final String STATUS = "status";


    private String locationId;
    private String status;
    private Map requestParams;

    public GetPurchase(String locationId, String status) {
        this.locationId = locationId;
        this.status = status;
        requestParams = new HashMap<>();
        requestParams.put(LOCATION_ID, this.locationId);
        requestParams.put(STATUS, this.status);

    }

    @JsonGetter(LOCATION_ID)
    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    @JsonGetter(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() { return baseUrl;}

    @Override
    @JsonIgnore
    public String getHttpMethod() { return HttpMethod.GET.toString();}

    @Override
    @JsonIgnore
    public Map getRequestParams() {
        return requestParams;
    }
 }



