package server.v2_8;

import server.common.Response;
import server.v2_8.common.GiftShopItem;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by Goni on 5/17/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetResourceResponse extends Response {

    public static final String GIFTS_SHOP_ITEM = "gifts_shop_item";

    private List<GiftShopItem> giftsShopItem;

    @JsonGetter(GIFTS_SHOP_ITEM)
    public List getGiftsShopItem() {
        return giftsShopItem;
    }

    public void setGiftsShopItem(List<GiftShopItem> giftsShopItem) {
        this.giftsShopItem = giftsShopItem;
    }
}
