package server.v2_8.base;

/**
 * Created by amit.levi on 30/04/2017.
 */
public interface IServerErrorResponse {


    String getResult();
    String getErrorMessage();
}
