package server.v2_8.base;


/**
 * Created by amit.levi on 24/04/2017.
 */
public interface ICleanable {

    void clean(String apiKey) throws Exception;

}
