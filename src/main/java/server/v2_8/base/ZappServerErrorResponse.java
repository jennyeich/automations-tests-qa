package server.v2_8.base;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import server.common.Response;

/**
 * Created by gili on 11/2/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ZappServerErrorResponse extends Response implements IServerErrorResponse{

    public static final String RESULT = "Result";
    public static final String ERROR_CODE = "ErrorCode";
    public static final String ERROR_MESSAGE = "ErrorMessage";
    public static final String ERRORDISPLAYSTRING = "ErrorDisplayString";


    private String result;
    private String errorCode;
    private String errorMessage;
    private String errorDisplayString;

    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @JsonGetter(ERROR_CODE)
    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @JsonGetter(ERROR_MESSAGE)
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
    @JsonGetter(ERRORDISPLAYSTRING)
    public String getErrorDisplayString() {
        return errorDisplayString;
    }

    public void setErrorDisplayString(String errorDisplayString) {
        this.errorDisplayString = errorDisplayString;
    }
}
