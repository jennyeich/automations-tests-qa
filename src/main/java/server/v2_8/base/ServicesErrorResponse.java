package server.v2_8.base;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import server.common.Response;

/**
 * Created by amit.levi on 27/04/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServicesErrorResponse extends Response implements IServerErrorResponse{

    public static final String RESULT = "Result";
    public static final String ERROR_TYPE = "errorType";
    public static final String ERROR_MESSAGE = "errorMessage";


    private String result;
    private String errorType;
    private String errorMessage;

    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @JsonGetter(ERROR_TYPE)
    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    @JsonGetter(ERROR_MESSAGE)
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

}
