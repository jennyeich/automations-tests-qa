package server.v2_8;

import server.common.BaseZappServerAPI;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

/**
 * Created by israel on 26/01/2017.
 *
 * RedeemAsset API - simulates redeem form the client
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public  class RedeemAsset extends BaseZappServerAPI {

    public static final String TOKEN = "Token";
    public static final String ASSETSKEY = "AssetKey";
    public static final String MEMBERSHIPKEY = "MembershipKey";
    public static final String LOCATIONID = "LocationID";


    private static final String baseUrl = "alpha/api/RedeemAsset";

    private String Token;
    private String AssetKey;
    private String MembershipKey;
    private String LocationID;


    @JsonGetter(TOKEN)
    public String getToken() {
        return Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }

    @JsonGetter(ASSETSKEY)
    public String getAssetKey() {
        return AssetKey;
    }

    public void setAssetKey(String AssetKey) {
        this.AssetKey = AssetKey;
    }

    @JsonGetter(MEMBERSHIPKEY)
    public String getMembershipKey() {
        return MembershipKey;
    }

    public void setMembershipKey(String MembershipKey) {
        this.MembershipKey = MembershipKey;
    }

    @JsonGetter(LOCATIONID)
    public String getLocationID() {
        return LocationID;
    }

    public void setLocationID(String LocationID) {
        this.LocationID = LocationID;
    }


    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }
}