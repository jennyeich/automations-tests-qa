package server.v2_8;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

/**
 * Created by Lior on 11/30/2016.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)

public class GetMemberDetails extends PurchaseAPI {

    public static final String EXPAND_ASSETS = "ExpandAssets";
    public static final String TEST_REDEEM_CONDITIONS = "TestRedeemConditions";
    public static final String INCLUDE_ARCHIVED_ASSET = "IncludeArchivedAssets";


    private static final String baseUrl = "alpha/api/GetMemberDetails";

    private Boolean expandAssets;
    private Boolean testRedeemConditions;
    private Boolean includeArchivedAssets;

    @JsonGetter(EXPAND_ASSETS)
    public Boolean getExpandAssets() {
        return expandAssets;
    }

    public void setExpandAssets(Boolean expandAssets) {
        this.expandAssets = expandAssets;
    }

    @JsonGetter(TEST_REDEEM_CONDITIONS)
    public Boolean getTestRedeemConditions() {
        return testRedeemConditions;
    }

    public void setTestRedeemConditions(Boolean testRedeemConditions) {
        this.testRedeemConditions = testRedeemConditions;
    }

    @JsonGetter(INCLUDE_ARCHIVED_ASSET)
    public Boolean getIncludeArchivedAssets() {
        return includeArchivedAssets;
    }

    public void setIncludeArchivedAssets(Boolean includeArchivedAssets) {
        this.includeArchivedAssets = includeArchivedAssets;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

}
