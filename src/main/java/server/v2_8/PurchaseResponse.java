package server.v2_8;

/**
 * Created by amit.levi on 25/04/2017.
 */
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PurchaseResponse {

    public static final String STATUS = "Status";
    public static final String TRANSACTION_ID = "TransactionID";

    private String status;
    private String transactionId;

    @JsonGetter(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonGetter(TRANSACTION_ID)
    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }
}