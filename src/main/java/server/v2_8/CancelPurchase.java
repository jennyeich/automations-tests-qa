package server.v2_8;

import server.common.BaseZappServerAPI;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CancelPurchase extends BaseZappServerAPI {

	public static final String CONFIRMATION = "Confirmation";
	public static final String TRANSACTION_ID = "TransactionID";
	public static final String BRANCH_ID = "BranchID";
	public static final String POS_ID = "PosID ";

	private static final String baseUrl = "alpha/api/CancelPurchase";


	private String confirmation;
	private String transactionId;
	private String branchId;
	private String posId;


	@JsonGetter(CONFIRMATION)
	public String getConfirmation() {return confirmation;}
	public void setConfirmation(String confirmation) {this.confirmation = confirmation;}

	@JsonGetter(TRANSACTION_ID)
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@JsonGetter(BRANCH_ID)
	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	@JsonGetter(POS_ID)
	public String getPosId() {
		return posId;
	}

	public void setPosId(String posId) {
		this.posId = posId;
	}


	@Override
	@JsonIgnore
	public String getRequestURL() {
		return baseUrl;
	}

	@Override
	@JsonIgnore
	public String getHttpMethod() {
		return HttpMethod.POST.toString();
	}
}

	


