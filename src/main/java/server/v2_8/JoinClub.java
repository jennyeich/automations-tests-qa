package server.v2_8;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseZappServerAPI;

/**
 * Created by Lior on 11/30/2016.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class JoinClub extends BaseZappServerAPI {

    public static final String LOCATION_ID = "LocationID";
    public static final String TOKEN = "Token";
    public static final String ORIGIN = "Origin";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String CODE = "Code";//joining code
    public static final String UPDATEDFIELDS = "UpdatedFields";
    public static final String REFERRAL_CODE = "ReferralCode";


    private static final String baseUrl = "alpha/api/JoinClub";

    private String Token;
    private String LocationID;
    private String phoneNumber;
    private String origin;
    private String code;
    private UpdatedFields updatedFields;


    private String referralCode;

    @JsonGetter(UPDATEDFIELDS)
    public UpdatedFields getUpdatedFields() {return updatedFields;}

    public void setUpdatedFields(UpdatedFields updatedFields) {this.updatedFields = updatedFields;}

    @JsonGetter(REFERRAL_CODE)
    public String getReferralCode() { return referralCode; }
    public void setReferralCode(String referralCode) { this.referralCode = referralCode; }

    @JsonGetter(ORIGIN)
    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    @JsonGetter(TOKEN)
    public String getToken() {
        return Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }

    @JsonGetter(LOCATION_ID)
    public String getLocationID() {
        return LocationID;
    }

    public void setLocationID(String LocationID) {
        this.LocationID = LocationID;
    }

    @JsonProperty(PHONE_NUMBER)
    public String getPhoneNumber() {return phoneNumber;}
    public void setPhoneNumber(String phoneNumber) {this.phoneNumber = phoneNumber;}

    @JsonProperty(CODE)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

}
