package server.v2_8;

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by israel on 26/01/2017.
 *
 * RedeemAsset API it's api that simulate redeem form the client, in this class we build the request.
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)

public  class SignOnResponse extends Response {

    public static final String TOKEN = "Token";

    private String Token;


    @JsonGetter(TOKEN)
    public String getToken() {
        return Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }
}