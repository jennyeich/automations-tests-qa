package server.v2_8;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseZappServerAPI;
import server.v2_8.common.Customer;
import server.v2_8.common.Item;
import server.v2_8.common.Payment;
import server.v2_8.common.RedeemItem;

import java.util.ArrayList;

/**
 * Created by Lior on 11/30/2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)

public class GetMemberBenefits extends BaseZappServerAPI {

    public static final String CUSTOMER = "Customers";
    public static final String REDEEM_ITEMS = "RedeemItems";
    public static final String ITEMS = "Items";
    public static final String POS_ID = "PosID";
    public static final String BRANCH_ID = "BranchID";
    public static final String TRANSACTION_ID = "TransactionID";
    public static final String TIMESTAMP = "TimeStamp";
    public static final String TOTAL_SUM = "TotalSum";
    public static final String TAGS = "Tags";
    public static final String CASHIER = "Cashier";
    public static final String CHAIN_ID = "ChainID";
    public static final String TOTAL_GENERAL_DISCOUNT = "TotalGeneralDiscount";
    public static final String PAYMENTS = "Payment";
    public static final String ORDER_TYPE = "OrderType";
    public static final String TRANSACTION_SOURCE = "TransactionSource";
    public static final String RETURN_EXTENDED_ITEMS = "ReturnExtendedItems";

    private static final String baseUrl = "alpha/api/GetMemberBenefits";

    private ArrayList<Customer> customers;
    private ArrayList<RedeemItem> redeemItems;
    private ArrayList<Item> items;
    private String posId = "2";
    private String branchId = "4";
    private String transactionId;
    private Integer timeStamp;
    private String totalSum;
    private ArrayList<String> tags;
    private String cashier;
    private String chainId;
    private Integer totalGeneralDiscount;
    private ArrayList <Payment> payments;
    private String orderType;
    private String transactionSource;
    private Boolean returnExtendedItems;



    @JsonGetter(CUSTOMER)
    public ArrayList<Customer> getCustomers() {
        if(customers == null)
            customers = new ArrayList<>();
        return customers;
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    @JsonGetter(REDEEM_ITEMS)
    public ArrayList<RedeemItem> getRedeemItems() {
        if(redeemItems == null)
            redeemItems = new ArrayList<>();
        return redeemItems;
    }

    public void setRedeemItems(ArrayList<RedeemItem> redeemItems) {
        this.redeemItems = redeemItems;
    }

    @JsonGetter(ITEMS)
    public ArrayList<Item> getItems() {
        if(items == null)
            items = new ArrayList<>();
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }

    @JsonGetter(POS_ID)
    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    @JsonGetter(BRANCH_ID)
    public String getBranchId() {
        return branchId;
    }

    public void setBranchId(String branchId) {
        this.branchId = branchId;
    }

    @JsonGetter(TRANSACTION_ID)
    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @JsonGetter(TIMESTAMP)
    public Integer getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Integer timeStamp) {
        this.timeStamp = timeStamp;
    }

    @JsonGetter(TOTAL_SUM)
    public String getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(String totalSum) {
        this.totalSum = totalSum;
    }

    @JsonGetter(TAGS)
    public ArrayList<String> getTags() {
        if(tags == null)
            tags = new ArrayList<>();
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    @JsonGetter(CASHIER)
    public String getCashier() {
        return cashier;
    }

    public void setCashier(String cashier) {
        this.cashier = cashier;
    }

    @JsonGetter(CHAIN_ID)
    public String getChainId() {
        return chainId;
    }

    public void setChainId(String chainId) {
        this.chainId = chainId;
    }

    @JsonGetter(TOTAL_GENERAL_DISCOUNT)
    public Integer getTotalGeneralDiscount() {
        return totalGeneralDiscount;
    }

    public void setTotalGeneralDiscount(Integer totalGeneralDiscount) {
        this.totalGeneralDiscount = totalGeneralDiscount;
    }

    @JsonGetter(PAYMENTS)
    public ArrayList<Payment> getPayments() {
        if(payments == null)
            payments = new ArrayList<>();
        return payments;
    }

    public void setPayments(ArrayList<Payment> payments) {
        this.payments = payments;
    }

    @JsonGetter(ORDER_TYPE)
    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    @JsonGetter(TRANSACTION_SOURCE)
    public String getTransactionSource() {
        return transactionSource;
    }

    public void setTransactionSource(String transactionSource) {
        this.transactionSource = transactionSource;
    }

    @JsonGetter(RETURN_EXTENDED_ITEMS)
    public Boolean getReturnExtendedItems() {
        return returnExtendedItems;
    }

    public void setReturnExtendedItems(Boolean returnExtendedItems) {
        this.returnExtendedItems = returnExtendedItems;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }
}
