package server.v2_8;

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by israel on 26/01/2017.
 *
 * RedeemAsset API it's api that simulate redeem form the client, in this class we build the request.
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)

public  class GetClientVerificationCodeResponse extends Response {

    public static final String RESULT = "Result";
    public static final String VERIFICATION_CODE = "VerificationCode";

    private String result;
    private String VerificationCode;

    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @JsonGetter(VERIFICATION_CODE)
    public String getVerificationCode() {
        return VerificationCode;
    }

    public void setVerificationCode(String VerificationCode) {
        this.VerificationCode = VerificationCode;
    }
}