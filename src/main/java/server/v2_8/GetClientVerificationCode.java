package server.v2_8;

import server.common.BaseZappServerAPI;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

/**
 * Created by Lior on 11/30/2016.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)

public class GetClientVerificationCode extends BaseZappServerAPI {

    public static final String LOCATION_ID = "LocationID";
    public static final String TOKEN = "Token";

    private static final String baseUrl = "alpha/api/GetClientVerificationCode";

    private String Token;
    private String LocationID;

    @JsonGetter(TOKEN)
    public String getToken() {
        return Token;
    }

    public void setToken(String Token) {
        this.Token = Token;
    }

    @JsonGetter(LOCATION_ID)
    public String getLocationID() {
        return LocationID;
    }

    public void setLocationID(String LocationID) {
        this.LocationID = LocationID;
    }


    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

}
