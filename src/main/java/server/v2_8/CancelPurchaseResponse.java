package server.v2_8;

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * Created by Jenny on 12/11/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class CancelPurchaseResponse extends Response {


    public static final String RESULT = "Result";

    private String result;

    public CancelPurchaseResponse() {}

    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

}
