package server.v2_8;


/**
 * Created by amit.levi on 25/04/2017.
 */
import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;


@JsonInclude(JsonInclude.Include.NON_NULL)
 public class GetPurchaseResponse extends Response {

    public static final String DATA = "data";

    private List<PurchaseResponse> data;

    @JsonGetter(DATA)
    public List getData() {
        return data;
    }

    public void setData(List<PurchaseResponse> data) {
        this.data = data;
    }
}