package server.v2_8;

import com.fasterxml.jackson.annotation.JsonInclude;
import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;

/**
 * Created by israel on 02/03/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CancelBudgetPaymentResponse extends Response {

    public static final String RESULT = "Result";

    private String result;


    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }
    public void setResult(String result) {
        this.result = result;
    }

 }

