package server.v2_8;

import com.fasterxml.jackson.annotation.JsonProperty;
import server.common.BaseZappServerAPI;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;
import utils.PropsReader;

/**
 * Created by Goni on 3/30/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SaveEventBasedActionRule extends BaseZappServerAPI {

    public static final String STATUS = "Status";
    public static final String RULE_ID = "RuleID";
    public static final String LOCATION_ID = "LocationID";
    public static final String TRIGGER = "Trigger";
    public static final String SMART_ACTION = "SmartAction";
    public static final String ACTION_DATA = "ActionData";

    private static final String baseUrl = "alpha/api/SaveEventBasedActionRule";

    private String status;
    private String locationId;
    private String ruleId;
    private String trigger;
    private String smartAction;
    private String actionData;

    public SaveEventBasedActionRule(){}
    public SaveEventBasedActionRule(String locationId, String ruleId, String trigger) {
        this.locationId = locationId;
        this.ruleId = ruleId;
        this.trigger = trigger;
        this.status =  "active";
        this.smartAction = PropsReader.readActionFromFile();
        this.actionData = "\"ActionData\":{\"MaxInvokesOfRulesPerTag\":10000,\"Tag\":\"" + ruleId + "\"}";
    }

    @JsonProperty(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty(LOCATION_ID)
    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    @JsonProperty(RULE_ID)
    public String getRuleId() {
        return ruleId;
    }

    @JsonProperty(TRIGGER)
    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    @JsonProperty(SMART_ACTION)
    public String getSmartAction() {
        return smartAction;
    }

    public void setSmartAction(String smartAction) {
        this.smartAction = smartAction;
    }

    @JsonProperty(ACTION_DATA)
    public String getActionData() {
        return actionData;
    }

    public void setActionData(String actionData) {
        this.actionData = actionData;
    }

    public void setRuleId(String ruleId) {
        this.ruleId = ruleId;
    }


    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

    @Override
    protected String getServerUrl() {
        return PropsReader.getPropValuesForEnv("panel.url");
    }
}

