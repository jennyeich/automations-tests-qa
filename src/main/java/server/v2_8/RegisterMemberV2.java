package server.v2_8;

import server.common.BaseServicesAPI;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by amit.levi on 27/04/2017.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@Deprecated
public class RegisterMemberV2 extends BaseServicesAPI {

    private static final String baseUrl = "api/v2/memberships/register";

    private Map fieldNameMap;

    @JsonAnyGetter()
    public Map getFieldNameMap() {
        return fieldNameMap;
    }

    public void addFieldNameMap(String key, String value) {
        if(this.fieldNameMap == null){
            this.fieldNameMap = new HashMap();
        }
        this.fieldNameMap.put(key, value);
    }

    public void setFieldNameMap(Map fieldNameMap) {
        this.fieldNameMap = fieldNameMap;
    }


    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }
}
