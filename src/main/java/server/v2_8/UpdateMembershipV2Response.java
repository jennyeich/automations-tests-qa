package server.v2_8;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import server.common.Response;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateMembershipV2Response extends Response {

    public static final String MEMBERSHIP_STATUS = "MembershipStatus";
    public static final String USER_KEY = "UserKey";
    public static final String ALLOW_SMS = "AllowSMS";
    public static final String ALLOW_EMAIL = "AllowEmail";

    private String membershipStatus;
    private String userKey;

    private Boolean allowSMS;
    private Boolean allowEmail;

    @JsonProperty(MEMBERSHIP_STATUS)
    public String getMembershipStatus() {
        return membershipStatus;
    }
    public void setMembershipStatus(String membershipStatus) {
        this.membershipStatus = membershipStatus;
    }

    @JsonProperty(USER_KEY)
    public String getUserKey() {
        return userKey;
    }
    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    @JsonProperty(ALLOW_SMS)
    public Boolean getAllowSMS() { return allowSMS; }

    public void setAllowSMS(Boolean allowSMS) {
        this.allowSMS = allowSMS;
    }

    @JsonProperty(ALLOW_EMAIL)
    public Boolean getAllowEmail() { return allowEmail; }

    public void setAllowEmail(Boolean allowEmail) {
        this.allowEmail = allowEmail;
    }

}
