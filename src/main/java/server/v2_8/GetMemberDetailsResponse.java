package server.v2_8;

import server.common.Response;
import server.v2_8.common.Membership;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;

/**
 * Created by Lior on 11/30/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class GetMemberDetailsResponse extends Response {

    public static final String RESULT = "Result";
    public static final String MEMBERSHIP = "Membership";

    private String result;
    private ArrayList<Membership> memberships;


    public GetMemberDetailsResponse() {
    }

    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @JsonGetter(MEMBERSHIP)
    public ArrayList<Membership> getMemberships() {
        return memberships;
    }

    public void setMembership(ArrayList<Membership> membership) {
        this.memberships = membership;
    }
}
