package server.v2_8;

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by israel on 26/01/2017.
 *
 * RedeemAsset API it's api that simulate redeem form the client (app), in this class we get the response.
 *
 */

@JsonInclude(JsonInclude.Include.NON_NULL)

public  class RedeemAssetResponse extends Response {

    public static final String RESULT = "Result";
    public static final String CODE = "Code";

    private String result;
    private String code;

    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @JsonGetter(CODE)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    }


