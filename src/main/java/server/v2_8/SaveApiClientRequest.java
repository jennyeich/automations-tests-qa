package server.v2_8;

import server.common.BaseZappServerAPI;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;


@JsonInclude(JsonInclude.Include.NON_NULL)
public class SaveApiClientRequest extends BaseZappServerAPI {

	private static final String baseUrl = "alpha/api/SaveApiClient";

	@Override
	@JsonIgnore
	public String getRequestURL() {
		return baseUrl;
	}

	@Override
	@JsonIgnore
	public String getHttpMethod() {
		return HttpMethod.POST.toString();
	}

}
