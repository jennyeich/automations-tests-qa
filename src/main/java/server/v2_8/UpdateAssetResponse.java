package server.v2_8;

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)

public class UpdateAssetResponse extends Response {

    public static final String ACTIVE = "Active";

    private String Active;

    @JsonGetter(ACTIVE)
    public String getActive() {return Active;}

    public void setActive(String Active) {
        this.Active = Active;
    }

}
