package server.v2_8;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import server.common.Response;

/*
 * Created by Jenny on 24/04/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsentResponse extends Response {

    public static final String CONSENT_IS_ON = "REGISTER_MEMBERS_BY_CONSENT";
    private String consentIsOn;


    public ConsentResponse() {}
    @JsonGetter(CONSENT_IS_ON)
    public String getConsentIsOn() {
        return consentIsOn;
    }
    public void setConsentIsOn(String consentIsOn) {
        this.consentIsOn = consentIsOn;
    }


}
