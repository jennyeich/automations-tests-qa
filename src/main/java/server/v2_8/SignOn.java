package server.v2_8;

import server.common.BaseZappServerAPI;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

/**
 * Created by Lior on 11/30/2016.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)

public class SignOn extends BaseZappServerAPI {

    public static final String DEVICE_ID = "DeviceID";
    public static final String INSTALLATION_ID = "InstallationID";

    private static final String baseUrl = "alpha/api/SignOn";

    private String DeviceID;
    private String InstallationID;

    @JsonGetter(DEVICE_ID)
    public String getDeviceID() {
        return DeviceID;
    }

    public void setDeviceID(String DeviceID) {
        this.DeviceID = DeviceID;
    }

    @JsonGetter(INSTALLATION_ID)
    public String getInstallationID() {
        return InstallationID;
    }

    public void setInstallationID(String InstallationID) {
        this.InstallationID = InstallationID;
    }


    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

}
