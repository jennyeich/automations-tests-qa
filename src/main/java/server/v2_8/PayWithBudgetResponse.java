package server.v2_8;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import server.common.Response;

/**
 * Created by israel on 30/11/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)

public class PayWithBudgetResponse extends Response {

    public static final String RESULT = "Result";
    public static final String ACTUAL_CHARGE= "ActualCharge";
    public static final String PAYMENT_UID = "PaymentUID";

    private String result;
    private String actualCharge;
    private String paymentUID;
    private Integer statusCode;

    public PayWithBudgetResponse() {
    }

    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @JsonGetter(ACTUAL_CHARGE)
    public String getActualCharge() {
        return actualCharge;
    }

    public void setActualCharge(String actualCharge) {
        this.actualCharge = actualCharge;
    }


    @JsonGetter(PAYMENT_UID)
    public String getPaymentUID() {
        return paymentUID;
    }

    public void setPaymentUID(String paymentUID) {
        this.paymentUID = paymentUID;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }
}


