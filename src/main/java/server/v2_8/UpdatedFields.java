package server.v2_8;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Jenny on 6/13/2017.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdatedFields  {


    public static final String FIRST_NAME = "FirstName";
    public static final String LAST_NAME = "LastName";
    public static final String BIRTHDAY = "Birthday";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String ALLOW_EMAIL = "AllowEmail";
    public static final String ALLOW_SMS = "AllowSMS";
    public static final String EXPIRATION_DATE= "ExpirationDate";
    public static final String EMAIL = "Email";


    private String expirationDate;
    private String email;
    private String firstName;
    private String lastName;
    private String birthDay;
    private String phoneNumber;
    private Boolean allowEmail;
    private Boolean allowSMS;


    @JsonProperty(FIRST_NAME)
    public String getFirstName() {return firstName;}
    public void setFirstName(String firstName) {this.firstName = firstName;}

    @JsonProperty(LAST_NAME)
    public String getLastName() {return lastName;}
    public void setLastName(String lastName) {this.lastName = lastName;}

    @JsonProperty(BIRTHDAY)
    public String getBirthDay() {return birthDay;}
    public void setBirthDay(String birthDay) {this.birthDay = birthDay;}

    @JsonProperty(PHONE_NUMBER)
    public String getPhoneNumber() {return phoneNumber;}
    public void setPhoneNumber(String phoneNumber) {this.phoneNumber = phoneNumber;}

    @JsonProperty(ALLOW_EMAIL)
    public Boolean getAllowEmail() { return allowEmail; }
    public void setAllowEmail(Boolean allowEmail) {
        this.allowEmail = allowEmail;
    }

    @JsonProperty(ALLOW_SMS)
    public Boolean getAllowSMS() { return allowSMS; }
    public void setAllowSMS(Boolean allowSMS) {
        this.allowSMS = allowSMS;
    }

    @JsonGetter(EXPIRATION_DATE)
    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }
    @JsonProperty(EMAIL)
    public String getEmail() {return email;}
    public void setEmail(String email) {this.email = email;}

}
