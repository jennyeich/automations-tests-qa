package server.v2_8;

import server.common.Response;
import server.common.asset.ActionResponse;
import server.common.asset.actions.Discount;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;

/**
 * Created by Jenny on 11/23/2016.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RedeemResponse extends Response {

    public static final String DISCOUNTS = "Discounts";
    public static final String RESULT = "Result";
    public static final String ITEMCODES = "ItemCodes";
    public static final String DEALCODES = "DealCodes";
    public static final String TOTALDISCOUNTSSUM = "TotalDiscountsSum";

    private String result;
    private ArrayList<Discount> discounts;
    private ArrayList<ActionResponse> itemCodes;
    private ArrayList<ActionResponse> dealCodes;
    private String totalDiscountsSum;

    @JsonGetter(TOTALDISCOUNTSSUM)
    public String getTotalDiscountsSum() {return totalDiscountsSum;}

    public void setTotalDiscountsSum(String totalDiscountsSum) {
        this.totalDiscountsSum = totalDiscountsSum;
    }

    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @JsonGetter(ITEMCODES)
    public ArrayList<ActionResponse> getItemCodes() {
        if(itemCodes == null)
            itemCodes = new ArrayList<>();
        return itemCodes;
    }

    public void setItemCodes(ArrayList<ActionResponse> itemCodes) {
        this.itemCodes = itemCodes;
    }

    @JsonGetter(DEALCODES)
    public ArrayList<ActionResponse> getDealCodes() {
        if(dealCodes == null)
            dealCodes = new ArrayList<>();
        return dealCodes;
    }

    public void setDealCodes(ArrayList<ActionResponse> dealCodes) {
        this.dealCodes = dealCodes;
    }

    @JsonGetter(DISCOUNTS)
    public ArrayList<server.common.asset.actions.Discount> getDiscounts() {
        if(discounts == null)
            discounts = new ArrayList<>();
        return discounts;
    }

    public void setDiscounts(ArrayList<server.common.asset.actions.Discount> discounts) {
        this.discounts = discounts;
    }

    @JsonIgnore
    public ArrayList<String> getSimplefiedDealCodes(){
        ArrayList<String> res = new ArrayList<>();
        for (ActionResponse response : dealCodes){
            res.add(response.getCode());
        }
        return res;
    }

    @JsonIgnore
    public ArrayList<String> getSimplefiedItemCodes(){
        ArrayList<String> res = new ArrayList<>();
        for (ActionResponse response : itemCodes){
            res.add(response.getCode());
        }
        return res;
    }

    public ArrayList<String> getRelatedItems (String code){
        for (ActionResponse response : itemCodes){
            if(response.getCode().equals(code)){
                return response.getRequiredItems();
            }
        }
        for (ActionResponse response : dealCodes){
            if(response.getCode().equals(code)){
                return response.getRequiredItems();
            }
        }
        return null;
    }
}
