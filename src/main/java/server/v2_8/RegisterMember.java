package server.v2_8;

import com.fasterxml.jackson.annotation.JsonProperty;
import server.common.BaseServicesAPI;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by amit.levi on 27/04/2017.
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RegisterMember extends BaseServicesAPI {

    private static final String baseUrl = "external/memberships/register";

    private Map fieldNameMap;

    @JsonAnyGetter()
    public Map getFieldNameMap() {
        return fieldNameMap;
    }

    public void addFieldNameMap(String key, Object value) {
        if(this.fieldNameMap == null){
            this.fieldNameMap = new HashMap();
        }
        this.fieldNameMap.put(key, value);
    }

    public void setFieldNameMap(Map fieldNameMap) {
        this.fieldNameMap = fieldNameMap;
    }


    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }
}
