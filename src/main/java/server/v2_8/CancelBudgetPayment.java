package server.v2_8;

import com.fasterxml.jackson.annotation.JsonInclude;
import server.common.BaseZappServerAPI;
import server.v2_8.common.Customer;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gargoylesoftware.htmlunit.HttpMethod;

import java.util.ArrayList;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CancelBudgetPayment extends BaseZappServerAPI {

	public static final String PAYMENTUID = "PaymentUID";
	public static final String TRANSACTION_ID = "TransactionID";
	public static final String BRANCH_ID = "BranchID";
	public static final String POS_ID = "PosID";
	public static final String CUSTOMERS = "Customers";
	public static final String VERIFICATION_CODE = "VerificationCode";

	private static final String baseUrl = "alpha/api/CancelBudgetPayment";


	private String PaymentUID;
	private String transactionId;
	private String branchId;
	private String posId;
	private String VerificationCode;
	private ArrayList<Customer> customers;

	@JsonGetter("Customers")
	public ArrayList<Customer> getCustomers() {
		if (customers == null)
			customers = new ArrayList<>();
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	@JsonGetter(PAYMENTUID)
	public String getPaymentuidPaymentUID() {return PaymentUID;}
	public void setPaymentUID(String PaymentUID) {this.PaymentUID = PaymentUID;}

	@JsonGetter(TRANSACTION_ID)
	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	@JsonGetter(BRANCH_ID)
	public String getBranchId() {
		return branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	@JsonGetter(POS_ID)
	public String getPosId() {
		return posId;
	}

	public void setPosId(String posId) {
		this.posId = posId;
	}

	@JsonGetter(VERIFICATION_CODE)
	public String getVerificationCode() {
		return VerificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.VerificationCode = verificationCode;
	}


	@Override
	@JsonIgnore
	public String getRequestURL() {
		return baseUrl;
	}

	@Override
	@JsonIgnore
	public String getHttpMethod() {
		return HttpMethod.POST.toString();
	}
}

	


