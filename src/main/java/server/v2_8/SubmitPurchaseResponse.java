package server.v2_8;

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;

/**
 * Created by gili on 11/1/16.
 */
public class SubmitPurchaseResponse extends Response {

    private String result;
    private String status;
    private String confirmation;
    private String expectedTotalSum;
    private String totalItems;

    public SubmitPurchaseResponse() {
    }

    @JsonGetter("Result")
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @JsonGetter("Confirmation")
    public String getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(String confirmation) {
        this.confirmation = confirmation;
    }

    @JsonGetter("ExpectedTotalSum")
    public String getExpectedTotalSum() {
        return expectedTotalSum;
    }

    public void setExpectedTotalSum(String expectedTotalSum) {
        this.expectedTotalSum = expectedTotalSum;
    }

    @JsonGetter("TotalItems")
    public String getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(String totalItems) {
        this.totalItems = totalItems;
    }

    @JsonGetter("Status")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
