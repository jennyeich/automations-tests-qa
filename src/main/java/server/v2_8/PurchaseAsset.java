package server.v2_8;

import server.common.BaseZappServerAPI;
import server.v2_8.common.GiftShopItem;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

/**
 * Created by Goni on 5/16/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PurchaseAsset extends BaseZappServerAPI {

    public static final String ITEM = "Item";

    private static final String baseUrl = "alpha/api/PurchaseAsset";

    private GiftShopItem item;


    @JsonGetter(ITEM)
    public GiftShopItem getItem() {
        return item;
    }

    public void setItem(GiftShopItem item) {
        this.item = item;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }
}