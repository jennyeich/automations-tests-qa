package server.v2_8;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseZappServerAPI;

/**
 * Created by Jenny on 6/13/2017.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateMembership extends BaseZappServerAPI {

    public static final String MEMBERSHIPKEY = "MembershipKey";
    public static final String TOKEN = "Token";
    public static final String LOCATIONID = "LocationID";
    public static final String UPDATEDFIELDS = "UpdatedFields";
    public static final String ORIGIN = "Origin";


    private static final String baseUrl = "alpha/api/UpdateMembership";


    private String MembershipKey;
    private String Token;
    private String LocationID;
    private String origin;
    private UpdatedFields updatedFields;

    @JsonProperty(UPDATEDFIELDS)
    public UpdatedFields getUpdatedFields() {return updatedFields;}
    public void setUpdatedFields(UpdatedFields updatedFields) {this.updatedFields = updatedFields;}

    @JsonProperty(MEMBERSHIPKEY)
    public String getMembershipKey() {return MembershipKey;}
    public void setMembershipKey(String membershipKey) {MembershipKey = membershipKey;}

    @JsonProperty(LOCATIONID)
    public String getLocationID() {
        return LocationID;
    }
    public void setLocationID(String LocationID) {
        this.LocationID = LocationID;
    }

    @JsonProperty(TOKEN)
    public String getToken() {
        return Token;
    }
    public void setToken(String Token) {
        this.Token = Token;
    }

    @JsonProperty(ORIGIN)
    public String getOrigin() {
        return origin;
    }
    public void setOrigin(String origin) {
        this.origin = origin;
    }


    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }



}
