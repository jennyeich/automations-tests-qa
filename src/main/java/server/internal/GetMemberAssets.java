package server.internal;

/**
 * Created by ariel on 13/06/17.
 */

import server.common.BaseServicesAPI;
import server.common.IHasRequestParams;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
 public class GetMemberAssets extends BaseServicesAPI implements IHasRequestParams {

    private  final String baseUrl = "memberAssets";

    public static final String MEMBERSHIP_KEY = "membershipKey";
    public static final String LOCATION_ID = "locationId";
    public static final String TOKEN = "token";

    private Map requestParams;

    public GetMemberAssets(String membershipKey, String locationId, String token) {
        requestParams = new HashMap<>();
        requestParams.put(MEMBERSHIP_KEY, membershipKey);
        requestParams.put(LOCATION_ID, locationId);
        requestParams.put(TOKEN, token);
    }

    @Override
    @JsonIgnore
    public String getRequestURL() { return baseUrl;}

    @Override
    @JsonIgnore
    public String getHttpMethod() { return HttpMethod.GET.toString();}

    @Override
    @JsonIgnore
    public Map getRequestParams() {
        return requestParams;
    }

 }
