package server.internal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MainResource {
    public static final String RESOURCE = "Resource";

    private ArrayList<Map<String,String>> resource;


    @JsonProperty(RESOURCE)
    public ArrayList<Map<String, String>> getResource() {
        return resource;
    }

    public void setResource(ArrayList<Map<String, String>> resource) {
        this.resource = resource;
    }
}
