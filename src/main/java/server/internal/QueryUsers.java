package server.internal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseZappServerAPI;
import server.common.IHasRequestParams;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Goni on 9/18/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QueryUsers extends BaseZappServerAPI implements IHasRequestParams {

    private  final String baseUrl = "alpha/api/QueryUsers";

    public static final String LOCATION_ID = "LocationID";
    public static final String TOKEN = "Token";

    private Map requestParams;

    public QueryUsers(String token,String locationId) {
        requestParams = new HashMap<>();
        requestParams.put(TOKEN, token);
        requestParams.put(LOCATION_ID, locationId);
    }

    @Override
    @JsonIgnore
    public String getRequestURL() { return baseUrl;}

    @Override
    @JsonIgnore
    public String getHttpMethod() { return HttpMethod.GET.toString();}

    @Override
    @JsonIgnore
    public Map getRequestParams() {
        return requestParams;
    }
}

