package server.internal;

/**
 * Created by ariel on 6/15/17.
 */

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SignOnResponse extends Response {

    public static final String TOKEN = "Token";
    public static final String USER_DATA = "UserData";
    public static final String SUCCESS = "Success";

    private String token;
    private Map userData;
    private Boolean success;

    @JsonGetter(TOKEN)
    public String getToken() { return token; }
    public void setToken(String token) { this.token = token; }

    @JsonGetter(USER_DATA)
    public Map getUserData() { return userData; }
    public void setUserData(Map userData) { this.userData = userData; }

    @JsonGetter(SUCCESS)
    public Boolean getSuccess() { return success; }
    public void setSuccess(Boolean success) { this.success = success; }
}
