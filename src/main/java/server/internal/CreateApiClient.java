package server.internal;

import com.fasterxml.jackson.annotation.*;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseServicesAPI;
import server.common.IHasRequestParams;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Goni on 10/15/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CreateApiClient extends BaseServicesAPI{

    private  final String baseUrl = "api_client";


    public static final String BUSINESS_ID = "BusinessID";
    public static final String NAME = "Name";

    private String businessId;
    private String name;

    private Map requestParams;


    @JsonProperty(BUSINESS_ID)
    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }
    @JsonProperty(NAME)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() { return baseUrl;}

    @Override
    @JsonIgnore
    public String getHttpMethod() { return HttpMethod.POST.toString();}


}



