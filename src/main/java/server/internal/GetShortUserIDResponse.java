package server.internal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import server.common.Response;

/**
 * Created by Goni on 7/16/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetShortUserIDResponse extends Response {


    public static final String KEY = "Key";
    public static final String STATUS = "status";
    public static final String TOKEN = "Token";
    public static final String KIND = "Kind";
    public static final String ID = "ID";
    public static final String VALID = "Valid";
    public static final String MEMBERSHIP_KEY = "MembershipKey";

    private String token;
    private String status;
    private String key;
    private String kind;
    private String id;
    private String valid;
    private String membershipKey;

    @JsonProperty(TOKEN)
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    @JsonProperty(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    @JsonProperty(KEY)
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
    @JsonProperty(KIND)
    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }
    @JsonProperty(ID)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    @JsonProperty(VALID)
    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }
    @JsonProperty(MEMBERSHIP_KEY)
    public String getMembershipKey() {
        return membershipKey;
    }

    public void setMembershipKey(String membershipKey) {
        this.membershipKey = membershipKey;
    }
}

