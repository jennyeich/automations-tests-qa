package server.internal;

/**
 * Created by ariel on 11/06/17.
 */

import server.common.BaseServicesAPI;
import server.common.IHasRequestParams;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
 public class GetLocationState extends BaseServicesAPI implements IHasRequestParams {

    private  final String baseUrl = "GetLocationState";

    public static final String LOCATION_ID = "locationId";

    private Map requestParams;

    public GetLocationState(String locationId) {
        requestParams = new HashMap<>();
        requestParams.put(LOCATION_ID, locationId);
    }

    @Override
    @JsonIgnore
    public String getRequestURL() { return baseUrl;}

    @Override
    @JsonIgnore
    public String getHttpMethod() { return HttpMethod.GET.toString();}

    @Override
    @JsonIgnore
    public Map getRequestParams() {
        return requestParams;
    }

 }
