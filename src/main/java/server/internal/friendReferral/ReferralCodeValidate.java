package server.internal.friendReferral;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseServicesAPI;

/**
 * Created by Jenny on 7/18/2018.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReferralCodeValidate extends BaseServicesAPI {

    private  final String baseUrl = "referralCode/validate";

    public static final String LOCATION_ID = "LocationID";
    public static final String TOKEN = "Token";
    public static final String CODE = "code";


    @Override
    @JsonIgnore
    public String getRequestURL() { return baseUrl;}

    @Override
    @JsonIgnore
    public String getHttpMethod() { return HttpMethod.GET.toString();}



}

