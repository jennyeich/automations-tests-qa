package server.internal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseZappServerAPI;
import server.common.IHasRequestParams;
import utils.PropsReader;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class DoCodeBasedAction extends BaseZappServerAPI implements IHasRequestParams {

    private  final String baseUrl = "alpha/api/DoCodeBasedAction";

    public static final String TOKEN = "Token";
    public static final String LOCATION_ID = "LocationID";
    public static final String CODE = "Code";

    private Map requestParams;

    public DoCodeBasedAction(String token,String locationId, String code) {

        requestParams = new HashMap<>();
        requestParams.put(TOKEN, token);
        requestParams.put(LOCATION_ID, locationId);
        requestParams.put(CODE, code);

    }

    @Override
    protected String getServerUrl(){
        return PropsReader.getPropValuesForEnv("app.url");
    }

    @Override
    @JsonIgnore
    public Map getRequestParams() {
        return requestParams;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.GET.toString();
    }
}
