package server.internal;

import com.fasterxml.jackson.annotation.*;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseServicesAPI;
import server.common.IHasRequestParams;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Goni on 10/22/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetApiClient extends BaseServicesAPI implements IHasRequestParams {

    private  final String baseUrl = "api_client";

    public static final String TOKEN = "token";

    private Map<String,Object> props = new HashMap<>();

    private Map requestParams;

    public GetApiClient(String token) {
        requestParams = new HashMap<>();
        requestParams.put(TOKEN, token);
    }

    @JsonAnyGetter
    public Map<String, Object> getProps() {
        return props;
    }

    @JsonAnySetter
    public void setProps(Map<String, Object> props) {
        this.props = props;
    }

    public void addProp(String key, Object value) {
        this.props.put(key,value);
    }

    @Override
    @JsonIgnore
    public String getRequestURL() { return baseUrl;}

    @Override
    @JsonIgnore
    public String getHttpMethod() { return HttpMethod.GET.toString();}

    @Override
    @JsonIgnore
    public Map getRequestParams() {
        return requestParams;
    }

}

