package server.internal;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseZappServerAPI;
import server.common.IHasRequestParams;
import utils.PropsReader;

import javax.swing.text.html.HTML;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Goni on 10/16/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenerateOneTimeCodes extends BaseZappServerAPI implements IHasRequestParams {

    public static final String TOKEN = "Token";
    public static final String BUSINESS_ID = "BusinessID";
    public static final String TEMPLATE_ID = "TemplateID";
    public static final String BULK_ID = "BulkID";
    public static final String TAG = "Tag";
    public static final String UNITS = "Units";
    public static final String LENGTH = "Length";

    private  final String baseUrl = "/alpha/api/GenerateOneTimeCodes";

    private Map requestParams;

    public GenerateOneTimeCodes(String templateId,String token, String locationId,String bulkId, String units,String length) {
        requestParams = new HashMap<>();
        requestParams.put(TOKEN, token);
        requestParams.put(TEMPLATE_ID, templateId);
        requestParams.put(BUSINESS_ID, locationId);
        requestParams.put(BULK_ID, bulkId);
        requestParams.put(TAG, templateId);
        requestParams.put(UNITS, units);
        requestParams.put(LENGTH, length);
    }


    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.GET.toString();
    }

    @Override
    public Map getRequestParams() {
        return requestParams;
    }

    @Override
    protected String getServerUrl() {
        return PropsReader.getPropValuesForEnv("panel.url");
    }
}

