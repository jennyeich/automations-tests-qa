package server.internal;

/**
 * Created by ariel on 11/06/17.
 */

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetLocationStateResponse extends Response {

    public static final String KEY = "Key";
    public static final String KIND = "Kind";
    public static final String NAME = "Name";
    public static final String SECRET_CODE = "SecretCode";
    public static final String STATUS = "Status";
    public static final String LOCATION_ID = "LocationID";
    public static final String MAIN_RESOURCES = "MainResources";
    public static final String CREATED_ON = "CreatedOn";
    public static final String ZAPP_TYPE = "ZappType";
    public static final String LIVE_ZAPP_VERSION = "LiveZappVersion";
    public static final String TIME_ZONE = "TimeZone";
    public static final String PUBLISH_VERSION = "PublishVersion";

    private String key;
    private String kind;
    private String name;
    private String secretCode;
    private String status;
    private String locationId;
    private MainResource mainResources;
    private String createdOn;
    private String zappType;
    private String liveZappVersion;
    private String timeZone;
    private String publishVersion;

    @JsonGetter(KEY)
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @JsonGetter(KIND)
    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    @JsonGetter(NAME)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonGetter(SECRET_CODE)
    public String getSecretCode() {
        return secretCode;
    }

    public void setSecretCode(String secretCode) {
        this.secretCode = secretCode;
    }

    @JsonGetter(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonGetter(MAIN_RESOURCES)
    public MainResource getMainResources() {
        return mainResources;
    }

    public void setMainResources(MainResource mainResources) {
        this.mainResources = mainResources;
    }

    @JsonGetter(LOCATION_ID)
    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    @JsonGetter(CREATED_ON)
    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    @JsonGetter(ZAPP_TYPE)
    public String getZappType() {
        return zappType;
    }

    public void setZappType(String zappType) {
        this.zappType = zappType;
    }

    @JsonGetter(LIVE_ZAPP_VERSION)
    public String getLiveZappVersion() {
        return liveZappVersion;
    }

    public void setLiveZappVersion(String liveZappVersion) {
        this.liveZappVersion = liveZappVersion;
    }

    @JsonGetter(TIME_ZONE)
    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @JsonGetter(PUBLISH_VERSION)
    public String getPublishVersion() {
        return publishVersion;
    }

    public void setPublishVersion(String publishVersion) {
        this.publishVersion = publishVersion;
    }
}
