package server.internal;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseZappServerAPI;
import server.common.IHasRequestParams;
import utils.PropsReader;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Goni on 10/16/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SaveOneTimeCodeTemplate extends BaseZappServerAPI implements IHasRequestParams {

    public static final String TOKEN = "Token";
    public static final String IDENTIFIER = "Identifier";
    public static final String BUSINESS_ID = "BusinessID";
    public static final String TEMPLATE_ID = "TemplateID";
    public static final String TYPE = "Type";
    public static final String ASSET_TEMPLATE_KEY = "AssetTemplateKey";

    private  final String baseUrl = "/alpha/api/SaveOneTimeCodeTemplate";

    private String identifier;

    private Map requestParams;

    public SaveOneTimeCodeTemplate(String token, String locationId,String templateId,String identifier){
        this.identifier = identifier;
        requestParams = new HashMap<>();
        requestParams.put(TOKEN, token);
        requestParams.put(BUSINESS_ID, locationId);
        requestParams.put(TEMPLATE_ID, templateId);
        requestParams.put(TYPE, "sku");
    }

    public SaveOneTimeCodeTemplate(String token, String locationId,String templateId,String identifier,String assetTemplateKey){
        this.identifier = identifier;
        requestParams = new HashMap<>();
        requestParams.put(TOKEN, token);
        requestParams.put(BUSINESS_ID, locationId);
        requestParams.put(TEMPLATE_ID, templateId);
        requestParams.put(TYPE, "sku");
        requestParams.put(ASSET_TEMPLATE_KEY, assetTemplateKey);
    }

    @JsonGetter(IDENTIFIER)
    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public Map getRequestParams() {
        return requestParams;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

    @Override
    protected String getServerUrl() {
        return PropsReader.getPropValuesForEnv("panel.url");
    }

}

