package server.internal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.xpath.operations.Bool;
import server.common.Response;

/**
 * Created by Goni on 10/24/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenerateApiKeyResponse extends Response{

    public static final String SUCCESS = "Success";
    public static final String API_KEY = "ApiKey";

    private Boolean success;
    private String apiKey;

    @JsonProperty(SUCCESS)
    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
    @JsonProperty(API_KEY)
    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}

