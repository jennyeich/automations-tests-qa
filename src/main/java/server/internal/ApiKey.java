package server.internal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni on 10/10/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiKey {

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String API_KEY = "apiKey";
    public static final String STATUS = "status";
    public static final String LOCATION_ID = "locationId";
    public static final String IS_DEFAULT = "default";

    private String id;
    private String name;
    private String apiKey;
    private String status;
    private String locationId;
    private Boolean isDefault;


    public ApiKey(){

    }
    public ApiKey(String name,String status,String apikey){
        this.name = name;
        this.status = status;
        this.apiKey = apikey;
    }
    @JsonProperty(ID)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    @JsonProperty(NAME)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @JsonProperty(API_KEY)
    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
    @JsonProperty(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    @JsonProperty(LOCATION_ID)
    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }
    @JsonProperty(IS_DEFAULT)
    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }
}

