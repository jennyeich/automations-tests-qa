package server.internal;

/**
 * Created by ariel on 6/12/17.
 */

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetZappResponse extends Response {

    public static final String KEY = "Key";
    public static final String CREATED_ON = "CreatedOn";
    public static final String LAST_UPDATE = "LastUpdate";
    public static final String KIND = "Kind";
    public static final String ENTITY_ID = "EntityId";
    public static final String TYPE = "Type";
    public static final String VARIATION = "Variation";
    public static final String VERSION = "Version";
    public static final String VALID_XML = "ValidXml";
    public static final String NOTES = "Notes";
    public static final String COMPRESSED = "Compressed";
    public static final String APP_FILE = "AppFile";

    private String key;
    private String createdOn;
    private String lastUpdate;
    private String kind;
    private String entityId;
    private String type;
    private String variation;
    private String version;
    private String validXml;
    private String notes;
    private String compressed;
    private Map appFile;

    @JsonGetter(KEY)
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @JsonGetter(CREATED_ON)
    public String getCreatedOn() { return createdOn; }

    public void setCreatedOn(String createdOn) { this.createdOn = createdOn; }

    @JsonGetter(LAST_UPDATE)
    public String getLastUpdate() { return lastUpdate; }

    public void setLastUpdate(String lastUpdate) { this.lastUpdate = lastUpdate; }

    @JsonGetter(KIND)
    public String getKind() {
        return kind;
    }

    public void setKind(String kind) { this.kind = kind; }

    @JsonGetter(ENTITY_ID)
    public String getEntityId() { return entityId; }

    public void setEntityId(String entityId) { this.entityId = entityId; }

    @JsonGetter(TYPE)
    public String getType() { return type; }

    public void setType(String type) { this.type = type; }

    @JsonGetter(VARIATION)
    public String getVariation() { return variation; }

    public void setVariation(String variation) { this.variation = variation; }

    @JsonGetter(VALID_XML)
    public String getValidXml() { return validXml; }

    public void setValidXml(String validXml) { this.validXml = validXml; }

    @JsonGetter(NOTES)
    public String getNotes() { return notes; }

    public void setNotes(String notes) { this.notes = notes; }

    @JsonGetter(COMPRESSED)
    public String getCompressed() { return compressed; }

    public void setCompressed(String compressed) { this.compressed = compressed; }

    @JsonGetter(VERSION)
    public String getVersion() { return version; }

    public void setVersion(String version) {
        this.version = version;
    }

    @JsonGetter(APP_FILE)
    public Map getAppFile() { return appFile; }

    public void setAppFile(Map appFile) {
        this.appFile = appFile;
    }
}
