package server.internal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseServicesAPI;
import server.common.BaseZappServerAPI;
import server.common.IHasRequestParams;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Goni on 7/16/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetShortUserID extends BaseZappServerAPI implements IHasRequestParams {

    private  final String baseUrl = "/alpha/api/GetShortUserID";

    public static final String TOKEN = "Token";
    public static final String NAMESPACE ="Namespace";

    private Map requestParams;

    public GetShortUserID(String token,String locationId) {
        requestParams = new HashMap<>();
        requestParams.put(TOKEN, token);
        requestParams.put(NAMESPACE,locationId);
    }

    @Override
    @JsonIgnore
    public String getRequestURL() { return baseUrl;}

    @Override
    @JsonIgnore
    public String getHttpMethod() { return HttpMethod.GET.toString();}

    @Override
    @JsonIgnore
    public Map getRequestParams() {
        return requestParams;
    }

}