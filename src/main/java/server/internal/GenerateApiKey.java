package server.internal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseServicesAPI;
import server.common.IHasRequestParams;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Goni on 10/24/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenerateApiKey extends BaseServicesAPI implements IHasRequestParams {

    private  final String baseUrl = "api_client/generate_api_key";

    public static final String TOKEN = "token";

    private Map requestParams;

    public GenerateApiKey(String token) {
        requestParams = new HashMap<>();
        requestParams.put(TOKEN, token);
    }
    @Override
    @JsonIgnore
    public String getRequestURL() { return baseUrl;}

    @Override
    @JsonIgnore
    public String getHttpMethod() { return HttpMethod.GET.toString();}

    @Override
    @JsonIgnore
    public Map getRequestParams() {
        return requestParams;
    }
}

