package server.internal;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.common.BaseZappServerAPI;

/**
 * Created by Goni on 10/15/2017.
 */

public class NewLocation extends BaseZappServerAPI {

    private static final String baseUrl = "alpha/api/NewLocation";

    private static final String TOKEN = "Token";
    private static final String LOCATION_NAME = "Name";
    private static final String LOCATION_ID = "LocationID";
    private static final String TIME_ZONE = "TimeZone";
    private static final String SECRET_CODE = "SecretCode";
    private static final String ZAPP_TYPE = "ZappType";
    private static final String LIVE_ZAPP_VERSION = "LiveZappVersion";
    private static final String MINE_TYPE = "MimeType";
    private static final String COUNTRY_CODE = "CountryCode_APP_SETTINGS";


    private String token;
    private String name;
    private String locationID;
    private String timeZone;
    private String SecretCode;
    private String zappType;
    private Integer liveZappVersion;
    private String mimeType;
    private String countryCode;

    @JsonProperty(TOKEN)
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
    @JsonProperty(LOCATION_NAME)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty(LOCATION_ID)
    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }
    @JsonProperty(TIME_ZONE)
    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }
    @JsonProperty(SECRET_CODE)
    public String getSecretCode() {
        return SecretCode;
    }

    public void setSecretCode(String secretCode) {
        SecretCode = secretCode;
    }
    @JsonProperty(ZAPP_TYPE)
    public String getZappType() {
        return zappType;
    }

    public void setZappType(String zappType) {
        this.zappType = zappType;
    }
    @JsonProperty(LIVE_ZAPP_VERSION)
    public Integer getLiveZappVersion() {
        return liveZappVersion;
    }

    public void setLiveZappVersion(Integer liveZappVersion) {
        this.liveZappVersion = liveZappVersion;
    }
    @JsonProperty(MINE_TYPE)
    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
    @JsonProperty(COUNTRY_CODE)
    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }
}

