package server.internal;

/**
 * Created by ariel on 6/15/17.
 */

import server.common.BaseServicesAPI;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

@JsonInclude(JsonInclude.Include.NON_NULL)
 public class SignOn extends BaseServicesAPI {

    public static final String DEVICE_ID = "deviceId";
    public static final String INSTALLATION_ID = "installationId";

    private static final String baseUrl = "SignOn";

    private String deviceId;
    private String installationId;

    @JsonGetter(DEVICE_ID)
    public String getDeviceId() { return deviceId; }
    public void setDeviceId(String deviceId) { this.deviceId = deviceId; }

    @JsonGetter(INSTALLATION_ID)
    public String getInstallationId() { return installationId; }
    public void setInstallationId(String installationId) { this.installationId = installationId; }

    @Override
    @JsonIgnore
    public String getRequestURL() { return baseUrl; }

    @Override
    @JsonIgnore
    public String getHttpMethod() { return HttpMethod.POST.toString(); }
}
