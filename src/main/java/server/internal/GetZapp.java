package server.internal;

/**
 * Created by ariel on 6/12/17.
 */

import server.common.BaseServicesAPI;
import server.common.IHasRequestParams;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gargoylesoftware.htmlunit.HttpMethod;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
 public class GetZapp extends BaseServicesAPI implements IHasRequestParams {

    private  final String baseUrl = "GetZapp";

    public static final String VERSION = "version";
    public static final String TYPE = "type";
    public static final String VARIATION = "variation";

    private Map requestParams;

    public GetZapp(String version, String type, String variation) {
        requestParams = new HashMap<>();
        requestParams.put(VERSION, version);
        requestParams.put(TYPE, type);
        requestParams.put(VARIATION, variation);
    }

    @Override
    @JsonIgnore
    public String getRequestURL() { return baseUrl;}

    @Override
    @JsonIgnore
    public String getHttpMethod() { return HttpMethod.GET.toString();}

    @Override
    @JsonIgnore
    public Map getRequestParams() {
        return requestParams;
    }

 }
