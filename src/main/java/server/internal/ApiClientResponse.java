package server.internal;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import server.common.Response;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 10/10/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ApiClientResponse extends Response {

    public static final String KEY = "Key";
    public static final String KIND = "Kind";
    public static final String NAME = "Name";
    public static final String ENTITY_ID = "EntityId";
    public static final String API_KEY = "ApiKey";
    public static final String API_KEYS = "ApiKeys";
    public static final String BUSINESS_ID = "BusinessID";
    public static final String SAVE_ANONYMOUS_PURCHASE_ON = "SaveAnonymousPurchaseOn";
    public static final String AUTO_MARK_REDEEM_CODES_AS_USED = "AutoMarkRedeemCodesAsUsed";
    public static final String AUTOMATION_DELAY_MS = "AutomationDelayMS";
    public static final String ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS = "AllowMultiplePurchasesForPOSIdentifiers";
    public static final String ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT ="AllowCancelMemberBudgetPayment";
    public static final String ALLOW_CANCEL_BUDGET_PAYMENT_BY_POS_IDENTIFIERS = "AllowCancelBudgetPaymentByPosIdentifiers";
    public static final String PAYMENT_REQUIRES_VERIFICATION_CODE = "PaymentRequiresVerificationCode";
    public static final String ALLOW_ANALYZED_PURCHASE_CANCELLATION ="AllowAnalyzedPurchaseCancellation";
    public static final String DETAILED_REDEEM_RESPONSE = "DetailedRedeemResponse";


    private String key;
    private String kind;
    private String apiKey;
    private String name;
    private Long entityId;
    private String businessID;


    private String saveAnonymousPurchaseOn;
    private Boolean autoMarkRedeemCodesAsUsed;
    private Integer automationDelayMS;
    private Boolean allowMultiplePurchasesForPOSIdentifiers;
    private Boolean allowCancelMemberBudgetPayment;
    private Boolean allowCancelBudgetPaymentByPosIdentifiers;
    private Boolean paymentRequiresVerificationCode;
    private Boolean allowAnalyzedPurchaseCancellation;
    private Boolean detailedRedeemResponse;

    private List<ApiKey> ApiKeys = new ArrayList<>();

    @JsonProperty(SAVE_ANONYMOUS_PURCHASE_ON)
    public String getSaveAnonymousPurchaseOn() {
        return saveAnonymousPurchaseOn;
    }

    public void setSaveAnonymousPurchaseOn(String saveAnonymousPurchaseOn) {
        this.saveAnonymousPurchaseOn = saveAnonymousPurchaseOn;
    }
    @JsonProperty(AUTO_MARK_REDEEM_CODES_AS_USED)
    public Boolean getAutoMarkRedeemCodesAsUsed() {
        return autoMarkRedeemCodesAsUsed;
    }

    public void setAutoMarkRedeemCodesAsUsed(Boolean autoMarkRedeemCodesAsUsed) {
        this.autoMarkRedeemCodesAsUsed = autoMarkRedeemCodesAsUsed;
    }
    @JsonProperty(AUTOMATION_DELAY_MS)
    public Integer getAutomationDelayMS() {
        return automationDelayMS;
    }

    public void setAutomationDelayMS(Integer automationDelayMS) {
        this.automationDelayMS = automationDelayMS;
    }
    @JsonProperty(ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS)
    public Boolean getAllowMultiplePurchasesForPOSIdentifiers() {
        return allowMultiplePurchasesForPOSIdentifiers;
    }

    public void setAllowMultiplePurchasesForPOSIdentifiers(Boolean allowMultiplePurchasesForPOSIdentifiers) {
        this.allowMultiplePurchasesForPOSIdentifiers = allowMultiplePurchasesForPOSIdentifiers;
    }
    @JsonProperty(ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT)
    public Boolean getAllowCancelMemberBudgetPayment() {
        return allowCancelMemberBudgetPayment;
    }

    public void setAllowCancelMemberBudgetPayment(Boolean allowCancelMemberBudgetPayment) {
        this.allowCancelMemberBudgetPayment = allowCancelMemberBudgetPayment;
    }
    @JsonProperty(ALLOW_CANCEL_BUDGET_PAYMENT_BY_POS_IDENTIFIERS)
    public Boolean getAllowCancelBudgetPaymentByPosIdentifiers() {
        return allowCancelBudgetPaymentByPosIdentifiers;
    }

    public void setAllowCancelBudgetPaymentByPosIdentifiers(Boolean allowCancelBudgetPaymentByPosIdentifiers) {
        this.allowCancelBudgetPaymentByPosIdentifiers = allowCancelBudgetPaymentByPosIdentifiers;
    }
    @JsonProperty(PAYMENT_REQUIRES_VERIFICATION_CODE)
    public Boolean getPaymentRequiresVerificationCode() {
        return paymentRequiresVerificationCode;
    }

    public void setPaymentRequiresVerificationCode(Boolean paymentRequiresVerificationCode) {
        this.paymentRequiresVerificationCode = paymentRequiresVerificationCode;
    }
    @JsonProperty(ALLOW_ANALYZED_PURCHASE_CANCELLATION)
    public Boolean getAllowAnalyzedPurchaseCancellation() {
        return allowAnalyzedPurchaseCancellation;
    }

    public void setAllowAnalyzedPurchaseCancellation(Boolean allowAnalyzedPurchaseCancellation) {
        this.allowAnalyzedPurchaseCancellation = allowAnalyzedPurchaseCancellation;
    }
    @JsonProperty(DETAILED_REDEEM_RESPONSE)
    public Boolean getDetailedRedeemResponse() {
        return detailedRedeemResponse;
    }

    public void setDetailedRedeemResponse(Boolean detailedRedeemResponse) {
        this.detailedRedeemResponse = detailedRedeemResponse;
    }

    @JsonProperty(KEY)
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty(KIND)
    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    @JsonProperty(NAME)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty(API_KEY)
    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
    @JsonProperty(ENTITY_ID)
    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    @JsonProperty(BUSINESS_ID)
    public String getBusinessID() {
        return businessID;
    }

    public void setBusinessID(String businessID) {
        this.businessID = businessID;
    }
    @JsonProperty(API_KEYS)
    public List<ApiKey> getApiKeys() {
        return ApiKeys;
    }

    public void setApiKeys(List<ApiKey> apiKeys) {
        ApiKeys = apiKeys;
    }
    public void addApiKey(ApiKey apiKey) {
        ApiKeys.add(apiKey);
    }
}

