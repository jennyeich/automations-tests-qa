package server.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gargoylesoftware.htmlunit.HttpMethod;
import utils.PropsReader;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lior.shory on 01/02/2017.
 */
public class UpdateAsset extends BaseZappServerAPI implements IHasRequestParams {

    private HashMap asset;

    public UpdateAsset() throws IOException {
        requestParams.put("Token", PropsReader.getPropValuesForEnv("serverToken"));
    }

    @Override
    public String getRequestURL() {
        return "/alpha/api/UpdateAsset";
    }

    @Override
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

    @Override
    @JsonIgnore
    public Object getRequestBody(){
        return asset;
    }

    @JsonIgnore
    public HashMap getAsset() {
        return asset;
    }

    public void setAsset(HashMap asset) {

        requestParams.put("AssetKey", (String)asset.get("Key"));
        this.asset = asset;

    }

    HashMap<String, String> requestParams = new HashMap<>();
    @Override
    @JsonIgnore
    public Map getRequestParams() {
        return requestParams;
    }
}
