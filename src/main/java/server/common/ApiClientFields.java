package server.common;

/**
 * Created by Goni on 10/19/2017.
 */

public enum ApiClientFields {
    MEMBER_CUSTOM_IDENTIFIER("MemberCustomIdentifier"),
    API_KEYS("ApiKeys"),
    SAVE_ANONYMOUS_PURCHASE_ON("SaveAnonymousPurchaseOn"),
    AUTO_MARK_REDEEM_CODES_AS_USED("AutoMarkRedeemCodesAsUsed"),
    AUTOMATION_DELAY_MS("AutomationDelayMS"),
    ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS("AllowMultiplePurchasesForPOSIdentifiers"),
    ALLOW_CANCEL_MEMBER_BUDGET_PAYMENT("AllowCancelMemberBudgetPayment"),
    ALLOW_CANCEL_BUDGET_PAYMENT_BY_POS_IDENTIFIERS("AllowCancelBudgetPaymentByPosIdentifiers"),
    PAYMENT_REQUIRES_VERIFICATION_CODE("PaymentRequiresVerificationCode"),
    ALLOW_ANALYZED_PURCHASE_CANCELLATION("AllowAnalyzedPurchaseCancellation"),
    ACCUMULATION_VERSION("AccumulationVersion"),
    DETAILED_REDEEM_RESPONSE("DetailedRedeemResponse");

    private String key;

    ApiClientFields(String key) {
        this.key = key;
    }

    public String key() {
        return key;
    }
}
