package server.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.HttpRequestWithBody;
import org.apache.log4j.Logger;
import org.junit.Assert;
import server.v2_8.base.ZappServerErrorResponse;
import utils.PropsReader;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseZappServerAPI implements IServerRequest {

	protected final static Logger log4j = Logger.getLogger(BaseZappServerAPI.class);

	private final String serverUrl = PropsReader.getPropValuesForEnv("server.api.url");

	static int MAX_RETRY_COUNT = 5;

	protected String getServerUrl(){
		return serverUrl;
	}

	@JsonIgnore
	public Object getRequestBody(){
		return this;
	}

	public IServerResponse SendRequestByApiKey(String apiKey, Class<? extends  IServerResponse> responseType){

		String url = String.format("%s%s?InputType=Json&MimeType=application/json&api-key=%s",getServerUrl(),getRequestURL(),apiKey);

		return this.SendRequest(url, responseType);
	}

	public  IServerResponse SendRequestByApiKeyAndValidateResponse(String apiKey, Class<? extends  IServerResponse> responseType){

		IServerResponse response = SendRequestByApiKey(apiKey, responseType);

		// make sure the response is not Error response
		if(response instanceof ZappServerErrorResponse){
			Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
		}
		return response;
	}

	public Object sendRequestByClass(Class responseType){
		String formattedUrl = String.format("%s%s?InputType=json&MimeType=application/json",getServerUrl(),getRequestURL());

		return this.sendServicesRequestByClass(formattedUrl, responseType);
	}

	private Object sendServicesRequestByClass(String url, Class responseType){

		ObjectMapper mapper = new ObjectMapper();
		try {
			String body = mapper.writeValueAsString(getRequestBody());

			HttpResponse response;
			if(getHttpMethod() == HttpMethod.GET.toString()){
				response = addRequestParams(Unirest.get(url)).asString();
			}
			else {
				response = ((HttpRequestWithBody)addRequestParams(Unirest.post(url))).header("content-Type", "application/json")
						.body(body).asJson();
			}

			List<Object> result = new ArrayList<>();
			if(response.getBody() != null){
				if(response.getBody() instanceof JsonNode){
					JsonNode jsonResponse = ((JsonNode) response.getBody());
					if(jsonResponse.getObject().has(ZappServerErrorResponse.ERROR_CODE)){
						return mapper.readValue(jsonResponse.getObject().toString(), ZappServerErrorResponse.class);
					}
					JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, responseType);
					result = mapper.readValue(jsonResponse.getObject().toString(), type);
				}
				else {
					if(response.getBody().toString().contains(ZappServerErrorResponse.ERROR_CODE)){
						IServerResponse errorResult = mapper.readValue(response.getBody().toString(), ZappServerErrorResponse.class);
						errorResult.setStatusCode(response.getStatus());
						return errorResult;
					}

					JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, responseType);
					result = mapper.readValue(response.getBody().toString(), type);
				}
			}
			return result;

		} catch (Exception e) {
			throw new RuntimeException("Failed to send request!", e);
		}
	}


	public  IServerResponse SendRequestByToken( String token, Class<? extends  IServerResponse> responseType){

		String url = String.format("%s%s?InputType=Json&MimeType=application/json&Token=%s",getServerUrl(),getRequestURL(),token);

		return this.SendRequest(url, responseType);

	}


	public  IServerResponse SendRequestByTokenAndValidateResponse( String token, Class<? extends  IServerResponse> responseType){

		IServerResponse response = SendRequestByToken(token, responseType);

		// make sure the response is not Error response
		if(response instanceof ZappServerErrorResponse){
			Assert.fail(((ZappServerErrorResponse)response).getErrorMessage());
		}
		return response;

	}

	public  IServerResponse sendRequestByTokenAndLocation(String token, String locationID, Class<? extends  IServerResponse> responseType){

		String formattedUrl = String.format("%s%s?InputType=json&MimeType=application/json&Token=%s&LocationID=%s",getServerUrl(),getRequestURL(),token, locationID);

		return this.SendRequest(formattedUrl, responseType);

	}

	public  IServerResponse sendRequest(Class<? extends  IServerResponse> responseType){

		String formattedUrl = String.format("%s%s?InputType=json&MimeType=application/json",getServerUrl(),getRequestURL());

		return this.SendRequest(formattedUrl, responseType);

	}

	protected IServerResponse SendRequest(String url, Class<? extends  IServerResponse> responseType){

		ObjectMapper mapper = new ObjectMapper();
		try {
			String body = mapper.writeValueAsString(getRequestBody());


			long time = System.currentTimeMillis();
			HttpResponse<JsonNode> jsonResponse = null;
			if(getHttpMethod() == HttpMethod.POST.toString()) {
				int retryCounter = 0;
				boolean success = false;
				while (!success && retryCounter < MAX_RETRY_COUNT)
				try {
					jsonResponse = ((HttpRequestWithBody) addRequestParams(Unirest.post(url))).header("content-Type", "application/json")
							.body(body).asJson();
					success = true;
				}catch (com.mashape.unirest.http.exceptions.UnirestException e){
					retryCounter++;
					if(retryCounter == MAX_RETRY_COUNT){
						throw e;
					}
				}
			}
			else if (getHttpMethod() == HttpMethod.GET.toString()){
				jsonResponse = addRequestParams(Unirest.get(url)).asJson();
			}

			time = System.currentTimeMillis() - time;

			if(jsonResponse.getBody().getObject().has(ZappServerErrorResponse.ERROR_CODE)){
				responseType = ZappServerErrorResponse.class;
			}

			IServerResponse result = mapper.readValue(jsonResponse.getBody().getObject().toString(), responseType);
			result.setRawObject(jsonResponse.getBody().getObject());
			result.setStatusCode(jsonResponse.getStatus());
			result.setTime((int)time);
			return result;

		} catch (Exception e) {
			throw new RuntimeException("Failed to send request!", e);
		}
	}
	public  HttpResponse SendRequest(String userKey){

		//ObjectMapper mapper = new ObjectMapper();
		try {

			HttpResponse httpResponse = null;
			if (getHttpMethod() == HttpMethod.GET.toString()){
				httpResponse = addRequestParams(Unirest.get(getRequestURL()+userKey)).asString();
			}

			//if(httpResponse.getBody()){
			//	responseType = ZappServerErrorResponse.class;
//
			//	IServerResponse result = mapper.readValue(jsonResponse.getBody().getObject().toString(), responseType);
			return httpResponse;

		} catch (Exception e) {
			throw new RuntimeException("Failed to send request!", e);
		}
	}

	public  IServerResponse sendRequestByUserTokenAndNamespace(String userToken, String locationId, Class<? extends  IServerResponse> responseType){

		String formattedUrl = String.format("%s%s?InputType=json&MimeType=application/json&Token=%s&Namespace=%s",getServerUrl(),getRequestURL(),userToken, locationId);
		return this.SendRequest(formattedUrl, responseType);
	}

	public  IServerResponse sendRequestByUserToken(String token, Class<? extends  IServerResponse> responseType){

		String formattedUrl = String.format("%s%s?InputType=json&MimeType=application/json&Token=%s",getServerUrl(),getRequestURL(),token);

		return this.SendRequest(formattedUrl, responseType);

	}

	public  IServerResponse sendRequestByTokenAndAssetKey(String token, String assetKey, Class<? extends  IServerResponse> responseType){

		String formattedUrl = String.format("%s%s?InputType=json&MimeType=application/json&Token=%s&AssetKey=%s",getServerUrl(),getRequestURL(),token, assetKey);
		return this.SendRequest(formattedUrl, responseType);
	}


	public IServerResponse sendRequestWithGivenBody(String url,String token,String body,Class<? extends  IServerResponse> responseType){

		String formattedUrl = String.format(url,getServerUrl(),getRequestURL(),token);
		ObjectMapper mapper = new ObjectMapper();
		try {

			long time = System.currentTimeMillis();
			HttpResponse<JsonNode> jsonResponse = null;
			if(getHttpMethod() == HttpMethod.POST.toString()) {
				int retryCounter = 0;
				boolean success = false;
				while (!success && retryCounter < MAX_RETRY_COUNT)
					try {
						jsonResponse = ((HttpRequestWithBody) addRequestParams(Unirest.post(formattedUrl))).header("content-Type", "application/json")
								.body(body).asJson();
						success = true;
					}catch (com.mashape.unirest.http.exceptions.UnirestException e){
						retryCounter++;
						if(retryCounter == MAX_RETRY_COUNT){
							throw e;
						}
					}
			}

			time = System.currentTimeMillis() - time;

			if(jsonResponse.getBody().getObject().has(ZappServerErrorResponse.ERROR_CODE)){
				responseType = ZappServerErrorResponse.class;
			}

			IServerResponse result = mapper.readValue(jsonResponse.getBody().getObject().toString(), responseType);
			result.setRawObject(jsonResponse.getBody().getObject());
			result.setStatusCode(jsonResponse.getStatus());
			result.setTime((int)time);
			return result;

		} catch (Exception e) {
			throw new RuntimeException("Failed to send request!", e);
		}
	}
}
