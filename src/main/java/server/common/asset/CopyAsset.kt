package server.common.asset

import server.common.BaseZappServerAPI
import com.fasterxml.jackson.annotation.JsonProperty
import com.gargoylesoftware.htmlunit.HttpMethod

/**
 * Created by israel on 27/02/2017.
 */
class CopyAsset : BaseZappServerAPI() {
    @JsonProperty("NewVersion")
    var newVersion: String? = null
    @JsonProperty("Key")
    var key: String? = null
    @JsonProperty("Copies")
    var copies: Int? = null

    override fun getHttpMethod(): String {
        return HttpMethod.POST.toString();
    }

    override fun getRequestURL(): String {
        return "alpha/api/CopyAsset"
    }

}