package server.common.asset.conditions;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * Created by amit.levi on 08/05/2017.
 * TODO: not implementing  ICondition interface. Just a temp solution for date condition until Condition objects will be added
 */
public class AssetDateCondition {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm:ss");

    String from;
    String to;
    public AssetDateCondition(Date from, Date to){
        this.from = DATE_TIME_FORMATTER.format(LocalDateTime.ofInstant(from.toInstant(), ZoneId.systemDefault()));
        this.to = DATE_TIME_FORMATTER.format(LocalDateTime.ofInstant(to.toInstant(), ZoneId.systemDefault()));
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }
}
