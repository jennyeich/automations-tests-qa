package server.common.asset;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dorit on 12/28/2016.
 */
public class SmartActionMapBuilder {

    public static Map<String, Object> buildComputeFunctionMap(String function, Map params){
        Map<String, Object> map= new HashMap<>();
        map.put("function",function);
        map.put("params",params);
        map.put("Type","Compute");
        return map;
    }

    public static Map<String, Object> buildActionFlowMap(String flowType, ArrayList<Map> smartActions){
        Map<String, Object> map= new HashMap<>();
        map.put("flowType",flowType);
        map.put("smartActions",smartActions);
        map.put("type","actionFlow");
        return map;
    }

    public static Map<String, Object> buildSmartActionMap(ArrayList<Map> calcs,Map condition, Map action,Map elseAction){
        Map<String,Object> map= new HashMap();
        map.put("action",action);
        if(calcs!=null)
            map.put("calcs",calcs);
        if(condition!=null)
            map.put("condition",condition);
        if(elseAction!=null)
            map.put("elseAction",elseAction);
        return map;

    }



}
