package server.common.asset

import server.common.BaseZappServerAPI
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import com.gargoylesoftware.htmlunit.HttpMethod
import java.util.*

/**
 * Created by Dorit on 12/20/2016.
 */
class NewAsset : BaseZappServerAPI() {
    @get:JsonProperty("Data")
    var data : Map<String,Object>? = null
    @get:JsonProperty("UsableConditions")
    var usableConditions : Array<Map<String,Object>>? = null
    @get:JsonProperty("DataOverride")
    var dataOverride: Map<String,Object>? = null
    @get:JsonProperty("ValidAtLocationIDs")
    var validAtLocationIDs: ArrayList<String>? = null
    @get:JsonProperty("Template")
    var template: String? = null
    @get:JsonProperty("Type")
    var type: String? = null
    @get:JsonProperty("Name")
    var name: String? = null
    @get:JsonProperty("Description")
    var description: String? = null
    @get:JsonProperty("Value")
    var value: String? = null
    @get:JsonProperty("Cost")
    var cost: String? = null
    @get:JsonProperty("StockTracked")
    var stockTracked: String? = null
    @get:JsonProperty("Active")
    var active: String? = null
    @get:JsonProperty("NotifyAction")
    var notifyAction:Map<String,Object>? = null


    @get:JsonProperty("RedeemAction")
    var redeemAction: Map<*,*>? = null

    fun setRedeemAction(redeemAction: RedeemAction) {
        this.redeemAction = redeemAction.toMap()
    }

    fun setRedeemAction(redeemActionJson:String) {
        this.redeemAction = ObjectMapper().readValue(redeemActionJson, Map::class.java)
    }


    override fun getHttpMethod(): String {
        return HttpMethod.POST.toString();
    }

    override fun getRequestURL(): String {
        return "alpha/api/NewAsset"
    }

}
