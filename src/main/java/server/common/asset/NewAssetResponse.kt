package server.common.asset

import server.common.IServerResponse
import server.common.Response
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

/**
 * Created by Dorit on 12/21/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
class NewAssetResponse : Response() {
    @JsonIgnore
    var data : String? = null
    @JsonIgnore
    var usableConditions : String? = null
    @JsonIgnore
    var redeemAction: String? = null
    @JsonIgnore
    var dataOverride: String? = null
    @JsonIgnore
    var validAtLocationIDs: ArrayList<String>? = null
    @JsonProperty("Template")
    var template: String? = null
    @JsonIgnore
    var type: String? = null
    @JsonIgnore
    var name: String? = null
    @JsonIgnore
    var description: String? = null
    @JsonIgnore
    var value: String? = null
    @JsonIgnore
    var cost: String? = null
    @JsonIgnore
    var stockTracked: String? = null
    @JsonIgnore
    var active: String? = null
    @JsonIgnore
    var notifyAction:String? = null
    @JsonProperty("Key")
    var key: String? = null
    @JsonIgnore
    var lastUpdate: String? = null
    @JsonIgnore
    var createdOn: String? = null
    @JsonIgnore
    var externalBenefits: String? = null

    @JsonAnySetter
    fun setProperty(key: String, value: Any?) {

    }
}
