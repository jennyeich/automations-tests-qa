package server.common.asset.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by gili on 1/26/17.
 */
public class AmountOff implements IDiscountType{

    int value;

    public AmountOff(){}

    public AmountOff(int value) {
        this.value = value;
    }

    @Override
    public Map toMap() {
        Map<String, Object> amountOff = new HashMap<>();
        amountOff.put("applyOn", new ArrayList<>());
        amountOff.put("value", value);
        amountOff.put("type", "amountOff");
        return amountOff;
    }

    public int getValue() {
        return value;
    }
}
