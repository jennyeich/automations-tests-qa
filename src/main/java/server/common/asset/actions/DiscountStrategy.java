package server.common.asset.actions;

/**
 * Created by gili on 1/26/17.
 */
public enum DiscountStrategy {
    BEST_FOR_CUSTOMER("LeastExpensiveForCustomer"),
    BEST_FOR_BUISNESS("LeastExpensiveForBusiness");

    private String strategy;

    DiscountStrategy(String strategy) {
        this.strategy = strategy;
    }

    public String strategy() {
        return strategy;
    }
}
