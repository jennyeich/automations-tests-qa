package server.common.asset.actions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by gili on 1/26/17.
 */
public class PercentOff implements IDiscountType {

    int value;

    public  PercentOff(){}
    public PercentOff(int value) {
        this.value = value;
    }

    @Override
    public Map toMap() {
        Map<String, Object> percentOff = new HashMap<>();
        percentOff.put("applyOn", new ArrayList<>());
        percentOff.put("value", value);
        percentOff.put("type", "percentOff");
        return percentOff;
    }

    public int getValue() {
        return value;
    }
}
