package server.common.asset.actions;

import com.fasterxml.jackson.annotation.JsonProperty;
import server.common.asset.SmartActionMapBuilder;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dorit on 12/28/2016.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Discount implements IAction {

    public static final String DESCRIPTION = "Description";
    public static final String SUM = "Sum";

    private String description;
    private IDiscountType type;
    private String sum;


    public void addDiscount(String description, IDiscountType type){
        this.description = description;
        this.type = type;
    }


    @Override
    public Map toMap() {
        Map<String, Object> actionMap= new HashMap<>();
        actionMap.put("type","addDiscount");
        actionMap.put("description", description);
        actionMap.put("sum", SmartActionMapBuilder.buildComputeFunctionMap("calcDiscount",buildDiscountParams()));

        return actionMap;
    }

    private Map<String,Object> buildDiscountParams(){
        Map<String,Object> discountParams= new HashMap<>();
        ArrayList<Map<String,Object>> conditionsArray= buildJsonConditionsArray();
        discountParams.put("conditions",conditionsArray);
        discountParams.put("limits",buildJsonLimits());
        discountParams.put("rewards",buildJsonRewardsArray());

        return discountParams;
    }

    private  ArrayList<Map<String, Object>> buildJsonConditionsArray(){
        return new ArrayList<>();
    }

    private  Map<String, Object> buildJsonLimits(){
        Map<String, Object> limits = new HashMap<>();
        limits.put("offerLimit", "-1");
        limits.put("amountLimit", "-1");
        return limits;
    }



    private ArrayList<Map<String,Object>> buildJsonRewardsArray() {
        ArrayList<Map<String,Object>> rewardsArray= new ArrayList<>();
        rewardsArray.add(type.toMap());
        return rewardsArray;
    }
    @JsonProperty(DESCRIPTION)
    public String getDescription() {
        return description;
    }
    @JsonProperty(SUM)
    public String getSum() {return sum;}
    public IDiscountType getType() {
        return type;
    }
}
