package server.common.asset.actions;

import server.common.asset.SmartActionMapBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dorit on 12/28/2016.
 */
public class ItemCode implements IAction {
    ArrayList<String> itemCodes= new ArrayList<>();
    HashMap<String, ArrayList> relatedItems= new HashMap<String, ArrayList>();
    public ItemCode addItemCodes(String itemCode){
        itemCodes.add(itemCode);
        return this;
    }

    public ItemCode addRelatedItem(String itemCode, ArrayList<String> items){
        relatedItems.put(itemCode,items);
        return this;

    }

    @Override
    public Map toMap() {
        Map<String, Object> actionMap= new HashMap<>();
        actionMap.put("type","addItemCodes");
        actionMap.put("itemCodes", SmartActionMapBuilder.buildComputeFunctionMap("addItemCodes",buildItemCodePrams()));
        return actionMap;
    }

    private Map<String,Object> buildItemCodePrams(){
        Map<String,Object> dealCodePrams= new HashMap<>();
        ArrayList<Map<String,Object>> dealCodesArray= buildJsonItemCodesArray();
        dealCodePrams.put("itemCodes",dealCodesArray);
        return dealCodePrams;
    }

    private ArrayList<Map<String,Object>> buildJsonItemCodesArray() {
        ArrayList<Map<String,Object>> itemCodesArray= new ArrayList<>();
        for (String itemCode : itemCodes) {
            Map<String,Object> codeObj=new HashMap<>();
            codeObj.put("Code",itemCode);
            itemCodesArray.add(codeObj);
        }
        for (Map.Entry<String, ArrayList> relatedItem : relatedItems.entrySet()) {
            Map<String,Object> codeObj=new HashMap<>();
            codeObj.put("Code",relatedItem.getKey());
            codeObj.put("RequiredItems",relatedItem.getValue());
            itemCodesArray.add(codeObj);
        }
        return itemCodesArray;
    }

    public ArrayList<String> getItemCodes() {
        return itemCodes;
    }

    public HashMap<String, ArrayList> getRelatedItems() {
        return relatedItems;
    }
}
