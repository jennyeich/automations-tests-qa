package server.common.asset.actions;

import server.common.asset.SmartActionMapBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dorit on 12/28/2016.
 */
public class DealCode implements IAction {
    ArrayList<String> dealCodes= new ArrayList<>();
    HashMap<String, ArrayList> relatedItems= new HashMap<String, ArrayList>();

    public DealCode addDealCode(String dealCode){
        dealCodes.add(dealCode);
        return this;
    }


    public DealCode addDealCode(String dealCode,ArrayList<String> items){
        relatedItems.put(dealCode,items);
        return this;
    }

    @Override
    public Map toMap() {
        Map<String, Object> actionMap= new HashMap<>();
        actionMap.put("type","addDealCodes");
        actionMap.put("dealCodes", SmartActionMapBuilder.buildComputeFunctionMap("addDealCodes",buildDealCodePrams()));
        return actionMap;
    }

    private Map<String,Object> buildDealCodePrams(){
        Map<String,Object> dealCodePrams= new HashMap<>();
        ArrayList<Map<String,Object>> dealCodesArray= buildJsonDealCodesArray();
        dealCodePrams.put("dealCodes",dealCodesArray);
        return dealCodePrams;
    }

    private ArrayList<Map<String,Object>> buildJsonDealCodesArray() {
        ArrayList<Map<String,Object>> dealCodesArray= new ArrayList<>();
        for (String dealCode : dealCodes) {
            Map<String,Object> codeObj=new HashMap<>();
            codeObj.put("Code",dealCode);
            dealCodesArray.add(codeObj);
        }
        for (Map.Entry<String, ArrayList> relatedItem : relatedItems.entrySet()) {
            Map<String,Object> codeObj=new HashMap<>();
            codeObj.put("Code",relatedItem.getKey());
            codeObj.put("RequiredItems",relatedItem.getValue());
            dealCodesArray.add(codeObj);
        }
        return dealCodesArray;
    }

    public ArrayList<String> getDealCodes() {
        return dealCodes;
    }

    public HashMap<String, ArrayList> getRelatedItems() {
        return relatedItems;
    }
}
