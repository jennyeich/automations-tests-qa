package server.common.asset;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Map;

/**
 * Created by israel on 02/03/2017.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)

public class DataOverride  {

    public static final String VALIDFROM = "ValidFrom";
    public static final String VALIDUNTIL = "ValidUntil";
    public static final String PUNCHCARDTYPE = "PunchCardType";
    public static final String ALLOWMULTIPUNCH = "AllowMultiPunch";
    public static final String CODETEXT = "CodeText";
    public static final String ALLOWEMPLOYEEPUNCH = "AllowEmployeePunch";
    public static final String PUNCHCARDPRIZE = "PunchCardPrize ";
    public static final String PUSHMESSAGE = "PushMessage";
    public static final String BACKGROUNDIMAGE = "BackgroundImage";
    public static final String PREPAID = "Prepaid";
    public static final String ALLOWMANUALPUNCH = "allowManualPunch";
    public static final String PAYMENTMIN = "payment_min";
    public static final String PAYMENTMAX = "payment_max";
    public static final String ALLOWTOPUP = "allow_top_up";
    public static final String BRANCHID = "BranchID";
    public static final String USERTAG = "UserTag";
    public static final String VALIDTIME = "ValidTime";
    public static final String SHORTCODESDATA = "ShortCodeData";
    public static final String ENUMERATORDATA = "EnumeratorData";

    private String ValidFrom;
    private String ValidUntil;
    private String PunchCardType;
    private String AllowMultiPunch;
    private String CodeText;
    private String AllowEmployeePunch;
    private String PunchCardPrize;
    private String PushMessage;
    private String BackgroundImage;
    private String Prepaid;
    private String allowManualPunch;
    private String payment_min;
    private String payment_max;
    private String allow_top_up;
    private String BranchID;
    private String UserTag;
    private String ValidTime;
    private Map ShortCodeData;
    private Map EnumeratorData;



    @JsonGetter(VALIDFROM)
    public String getValidFrom() {
        return ValidFrom;
    }

    public void setValidFrom(String ValidFrom) {
        this.ValidFrom = ValidFrom;
    }

    @JsonGetter(VALIDUNTIL)
    public String getValidUntil() {
        return ValidUntil;
    }

    public void setValidUntil(String ValidUntil) {
        this.ValidUntil = ValidUntil;
    }

    @JsonGetter(PUNCHCARDTYPE)
    public String getPunchCardType() {
        return PunchCardType;
    }

    public void setPunchCardType(String PunchCardType) {
        this.PunchCardType = PunchCardType;
    }

    @JsonGetter(ALLOWMULTIPUNCH)
    public String getAllowMultiPunch() {
        return AllowMultiPunch;
    }

    public void setAllowMultiPunch(String AllowMultiPunch) {
        this.AllowMultiPunch = AllowMultiPunch;
    }

    @JsonGetter(CODETEXT)
    public String getCodeText() {
        return CodeText;
    }

    public void setCodeText(String CodeText) {
        this.CodeText = CodeText;
    }
    @JsonGetter(ALLOWEMPLOYEEPUNCH)
    public String getAllowEmployeePunch() {
        return AllowEmployeePunch;
    }

    public void setAllowEmployeePunch(String AllowEmployeePunch) {
        this.AllowEmployeePunch = AllowEmployeePunch;
    }

    @JsonGetter(PUNCHCARDPRIZE)
    public String getPunchCardPrize() {
        return PunchCardPrize;
    }

    public void setPunchCardPrize(String PunchCardPrize) {
        this.PunchCardPrize = PunchCardPrize;
    }

    @JsonGetter(PUSHMESSAGE)
    public String getPushMessage() {
        return PushMessage;
    }

    public void setPushMessage(String PushMessage) {
        this.PushMessage = PushMessage;
    }

    @JsonGetter(BACKGROUNDIMAGE)
    public String getBackgroundImage() {
        return BackgroundImage;
    }

    public void setBackgroundImage(String BackgroundImage) {
        this.BackgroundImage = BackgroundImage;
    }

    @JsonGetter(PREPAID)
    public String getPrepaid() {
        return Prepaid;
    }

    public void setPrepaid(String Prepaid) {
        this.Prepaid = Prepaid;
    }

    @JsonGetter(ALLOWMANUALPUNCH)
    public String getallowManualPunch() {
        return allowManualPunch;
    }

    public void setallowManualPunch(String allowManualPunch) {
        this.allowManualPunch = allowManualPunch;
    }

    @JsonGetter(PAYMENTMIN)
    public String getpayment_min() {
        return payment_min;
    }

    public void setpayment_min(String payment_min) {
        this.payment_min = payment_min;
    }

    @JsonGetter(PAYMENTMAX)
    public String getpayment_max() {
        return payment_max;
    }

    public void setpayment_max(String payment_max) {
        this.payment_max = payment_max;
    }

    @JsonGetter(ALLOWTOPUP)
    public String getallow_top_up() {
        return allow_top_up;
    }

    public void setallow_top_up(String allow_top_up) {
        this.allow_top_up = allow_top_up;
    }

    @JsonGetter(BRANCHID)
    public String getBranchID() {
        return BranchID;
    }

    public void setBranchID(String BranchID) {
        this.BranchID = BranchID;
    }

    @JsonGetter(USERTAG)
    public String getUserTag() {
        return UserTag;
    }

    public void setUserTag(String UserTag) {
        this.UserTag = UserTag;
    }

    @JsonGetter(VALIDTIME)
    public String getValidTime() {
        return ValidTime;
    }

    public void setValidTime(String ValidTime) {
        this.ValidTime = ValidTime;
    }

    @JsonGetter(SHORTCODESDATA)
    public Map getShortCodeData() {
        return ShortCodeData;
    }

    public void setShortCodeData(Map ShortCodeData) {
        this.ShortCodeData = ShortCodeData;
    }
    @JsonGetter(ENUMERATORDATA)
    public Map getEnumeratorData() {
        return EnumeratorData;
    }

    public void setEnumeratorData(Map EnumeratorData) {
        this.EnumeratorData = EnumeratorData;
    }

}
