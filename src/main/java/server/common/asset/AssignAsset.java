package server.common.asset;

import org.junit.Assert;
import server.common.IServerResponse;
import server.common.InvokeEventBasedActionRule;
import server.common.InvokeEventBasedActionRuleBuilder;
import server.common.Response;
import server.common.asset.conditions.AssetDateCondition;
import server.v2_8.GetMemberDetails;
import server.v2_8.GetMemberDetailsResponse;
import server.v2_8.base.ZappServerErrorResponse;
import server.v2_8.common.Asset;
import server.v2_8.common.Customer;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;


/**
 * Created by israel on 29/01/2017.
 *
 * Create and Assign asset to user via InvokeEventBasedActionRule (return AssetKey)
 */

public class  AssignAsset {

    public static String DEFAULT_DEAL_CODE="11223344";
    public static String EXPIRATION_SECONDS="6048000";

    public static Asset assignAssetToMember(String assetType,String codeType,String membershipKey, String apiKey, String locationId , String token, String phoneNumber) throws IOException, InterruptedException {

        return assignAssetToMember(assetType, codeType, membershipKey, apiKey, locationId, token, phoneNumber, null);

    }

    public static Asset assignAssetToMember(String assetType, String codeType, String membershipKey, String apiKey, String locationId , String token, String phoneNumber, AssetDateCondition dateCondition) throws IOException, InterruptedException {

        NewAsset newAsset = null;
        AssetBuilder assetBuilder = new AssetBuilder();
        newAsset = getNewAsset(assetType, codeType, locationId, dateCondition, newAsset, assetBuilder);

        NewAssetResponse newAssetResponse = (NewAssetResponse) newAsset.SendRequestByToken(token, NewAssetResponse.class);
        String templateResponse = newAssetResponse.getTemplate();
        String assetName = newAsset.getName();

        //Build GetMemberDetails request in order to get UserKey
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.getCustomers().add(customer);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(false);

        //Send GetMemberDetails request
        IServerResponse response = getMemberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response).getErrorMessage(), false);
        }

        // Cast to getMemberDetailsResponse object to fetch all response data
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) response;

        String userKey = (getMemberDetailsResponse).getMemberships().get(0).getUserKey().toString();


        //Build assign asset via InvokeEventBasedActionRule
        InvokeEventBasedActionRuleBuilder assignRequest = new InvokeEventBasedActionRuleBuilder();
        InvokeEventBasedActionRule request = assignRequest.buildAssignAssetRequest(membershipKey, userKey, templateResponse, locationId);

        //Send assign asset via InvokeEventBasedActionRule
        IServerResponse basedActionResponse = request.SendRequestByToken(token, Response.class);

        // make sure the response is not Error response
        if (basedActionResponse instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) basedActionResponse).getErrorMessage(), false);
        }
        TimeUnit.MINUTES.sleep(1);//wait 1 min due to cache of assets
        getMemberDetailsResponse = waitForAsset(getMemberDetails, apiKey, assetName, 500);
        Asset asset = getMemberDetailsResponse.getMemberships().get(0).findAssetByName(assetName);
        Assert.assertNotNull("Asset should not be null", asset);

        //Get Asset Name from getMemberDetailsResponse
        String assetNameMemberDetails = asset.getName();


        Assert.assertEquals("User get the asset", assetNameMemberDetails, assetName);

        return asset;

    }

    public static void assignAssetToMember(NewAssetResponse newAssetResponse,String membershipKey, String apiKey, String locationId , String token, String phoneNumber) throws Exception {

        //Build GetMemberDetails request in order to get UserKey
        Customer customer = new Customer();
        customer.setPhoneNumber(phoneNumber);
        GetMemberDetails getMemberDetails = new GetMemberDetails();
        getMemberDetails.getCustomers().add(customer);
        getMemberDetails.setExpandAssets(true);
        getMemberDetails.setIncludeArchivedAssets(false);

        //Send GetMemberDetails request
        IServerResponse response = getMemberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);

        // make sure the response is not Error response
        if (response instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) response).getErrorMessage(), false);
        }

        // Cast to getMemberDetailsResponse object to fetch all response data
        GetMemberDetailsResponse getMemberDetailsResponse = (GetMemberDetailsResponse) response;

        String userKey = (getMemberDetailsResponse).getMemberships().get(0).getUserKey().toString();


        //Build assign asset via InvokeEventBasedActionRule
        InvokeEventBasedActionRuleBuilder assignRequest = new InvokeEventBasedActionRuleBuilder();
        InvokeEventBasedActionRule request = assignRequest.buildAssignAssetRequest(membershipKey, userKey, newAssetResponse.getTemplate(), locationId);

        //Send assign asset via InvokeEventBasedActionRule
        IServerResponse basedActionResponse = request.SendRequestByToken(token, Response.class);

        // make sure the response is not Error response
        if (basedActionResponse instanceof ZappServerErrorResponse) {
            Assert.assertTrue(((ZappServerErrorResponse) basedActionResponse).getErrorMessage(), false);
        }
        TimeUnit.MINUTES.sleep(1);//wait 1 min due to cache of assets

    }

    public static String createAssetNoAssign(String assetType, String codeType,String locationId , String token, AssetDateCondition dateCondition) throws IOException, InterruptedException {

        //Build new Asset type = gift
        NewAsset newAsset = null;
        AssetBuilder assetBuilder = new AssetBuilder();
        newAsset = getNewAsset(assetType, codeType, locationId, dateCondition, newAsset, assetBuilder);

        NewAssetResponse newAssetResponse = (NewAssetResponse) newAsset.SendRequestByToken(token, NewAssetResponse.class);
        String templateResponse = newAssetResponse.getTemplate();

        return templateResponse;
    }


    public static NewAsset createAssetNoAssignReturnNewAsset(String assetType, String codeType, Integer numOfCode, String locationId , String token, AssetDateCondition dateCondition) throws IOException, InterruptedException {

        //Build new Asset type = gift
        NewAsset newAsset = null;
        AssetBuilder assetBuilder = new AssetBuilder();
        newAsset = getNewAsset(assetType, codeType, locationId, dateCondition, newAsset, assetBuilder);

        NewAssetResponse newAssetResponse = (NewAssetResponse) newAsset.SendRequestByToken(token, NewAssetResponse.class);

        return newAsset;
    }


    private static NewAsset getNewAsset(String assetType, String codeType, String locationId, AssetDateCondition dateCondition, NewAsset newAsset, AssetBuilder assetBuilder) {
        switch (codeType) {
            case "None":
                newAsset = assetBuilder.buildDefaultAsset(locationId, assetType, dateCondition);
                break;

            case "Bulk":
                assetBuilder.buildDefaultAsset(locationId, assetType, dateCondition);
                HashMap<String, Object> objBulk = new HashMap();
                objBulk.put("CodeID", DEFAULT_DEAL_CODE);
                assetBuilder.setEnumeratorData(objBulk);
                newAsset = assetBuilder.getAsset();
                //newAsset.addScenario().addDealCodeAction().addDealCode("112233344");
                //newAsset = assetBuilder.buildCustom(locationId,"gift","31/10/2016 22:00:00","31/10/2022 22:00:00","v2","N","apple","","Y","","image_medium_127856","N","","","","","","","","","11223344","Bulk");
                break;

            case "Auto":
                newAsset = assetBuilder.buildDefaultAsset(locationId, assetType, dateCondition);
                HashMap<String, Object> objAuto = new HashMap();
                objAuto.put("ExpirationSeconds", EXPIRATION_SECONDS);
                assetBuilder.setShortCodeData(objAuto);
//            newAsset=assetBuilder.getAsset();
                //newAsset.addScenario().addDealCodeAction().addDealCode("112233344");
                //newAsset = assetBuilder.buildCustom(locationId,"gift","31/10/2016 22:00:00","31/10/2022 22:00:00","v2","N","apple","","Y","","image_medium_127856","N","","","","","","","","","6048000","Auto");
                break;
        }
        return newAsset;
    }


    public static GetMemberDetailsResponse waitForAsset(GetMemberDetails memberDetails, String apiKey, String assetName, int maxSecWait) throws InterruptedException {

        int maxTimeout = (maxSecWait*1000);
        GetMemberDetailsResponse getMemberDetailsResponse;
        //Get Asset Name from getMemberDetailsResponse
        while (maxTimeout > 0) {
            getMemberDetailsResponse=(GetMemberDetailsResponse)memberDetails.SendRequestByApiKey(apiKey, GetMemberDetailsResponse.class);
            Asset asset=null;
            try {
                asset = getMemberDetailsResponse.getMemberships().get(0).findAssetByName(assetName);
            }catch (Exception e){

            }
            if(asset!=null)
                return getMemberDetailsResponse;
            Thread.sleep(1000);
            maxTimeout -= 1000;
        }
        Assert.fail("Asset "+ assetName +" didn't found in GetMemberDetailsResponse");
        return null;
    }
}
