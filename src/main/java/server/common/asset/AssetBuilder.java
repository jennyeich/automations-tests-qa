package server.common.asset;

import server.common.asset.conditions.AssetDateCondition;
import utils.JsonUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Dorit on 12/21/2016.
 */
public class AssetBuilder {

    private static final String DATE_CONDITION_PLACE_HOLDER = "---DATE-CONDITION---";
    private static final String FROM_DATE_PLACE_HOLDER = "---FROM_DATE---";
    private static final String TO_DATE_PLACE_HOLDER = "---TO_DATE---";

    //Temporary solution until asset condition will be supported
    private String dateCondition = "\"condition\": { \"arguments\": [{\"arguments\": [{\"arguments\": [{\"computed\": \"false\",\"property\": {\"name\": \"TimeStamp\",\"type\": \"Identifier\" }, \"object\": {\"name\": \"context\",\"type\": \"Identifier\" }, \"type\": \"MemberExpression\" }, { \"computed\": \"false\", \"property\": {\"name\": \"TimeZone\",\"type\": \"Identifier\" }, \"object\": {\"name\": \"context\",\"type\": \"Identifier\" }, \"type\": \"MemberExpression\" }], \"callee\": { \"computed\": \"false\", \"property\": {\"name\": \"tzDateTimeFromUTC\",\"type\": \"Identifier\" }, \"object\": {\"name\": \"led\",\"type\": \"Identifier\" }, \"type\": \"MemberExpression\" }, \"type\": \"CallExpression\" }, { \"arguments\": [{ \"raw\": \"'" + FROM_DATE_PLACE_HOLDER + "'\", \"value\": \"" +FROM_DATE_PLACE_HOLDER + "\", \"type\": \"Literal\" }, { \"computed\": \"false\", \"property\": {\"name\": \"TimeZone\",\"type\": \"Identifier\" }, \"object\": {\"name\": \"context\",\"type\": \"Identifier\" }, \"type\": \"MemberExpression\" }], \"callee\": { \"computed\": \"false\", \"property\": {\"name\": \"tzDateTimeFromUTC\",\"type\": \"Identifier\" }, \"object\": {\"name\": \"led\",\"type\": \"Identifier\" }, \"type\": \"MemberExpression\" }, \"type\": \"CallExpression\" }, { \"arguments\": [{ \"raw\": \"'"+ TO_DATE_PLACE_HOLDER+ "'\", \"value\": \"" + TO_DATE_PLACE_HOLDER+ "\", \"type\": \"Literal\" }, { \"computed\": \"false\", \"property\": {\"name\": \"TimeZone\",\"type\": \"Identifier\" }, \"object\": {\"name\": \"context\",\"type\": \"Identifier\" }, \"type\": \"MemberExpression\" }], \"callee\": { \"computed\": \"false\", \"property\": {\"name\": \"tzDateTimeFromUTC\",\"type\": \"Identifier\" }, \"object\": {\"name\": \"led\",\"type\": \"Identifier\" }, \"type\": \"MemberExpression\" }, \"type\": \"CallExpression\" }], \"callee\": { \"computed\": \"false\", \"property\": { \"name\": \"dateTimeIsBetween\", \"type\": \"Identifier\" }, \"object\": { \"name\": \"led\", \"type\": \"Identifier\" }, \"type\": \"MemberExpression\" }, \"type\": \"CallExpression\" }], \"callee\": { \"name\": \"led.and\", \"type\": \"Identifier\" }, \"type\": \"CallExpression\"  },";

    String namePrefix = "test";
    String templatePrefix = "smartAssetTest";
    String data = "{\"ValidTime\":[],\"RequiredPunches\":\"0\",\"RequiresSecretCode\":\"N\"}";
    //String redeemAction = "{\"elseAction\":{\"flowType\":\"all\",\"type\":\"actionFlow\",\"smartActions\":[{\"action\":{\"_skip\":\"true\",\"data\":{\"Type\":\"Compute\",\"params\":{\"expression\":{\"computed\":\"false\",\"property\":{\"name\":\"testAssetConditions\",\"type\":\"Identifier\"},\"object\":{\"name\":\"context\",\"type\":\"Identifier\"},\"type\":\"MemberExpression\"}},\"function\":\"evalExpression\"},\"type\":\"addToResponse\"}}]},\"calcs\":[{\"id\":\"dueDateCondition\",\"calc\":{\"Type\":\"Compute\",\"params\":{},\"function\":\"dueDateCondition\"}},{\"id\":\"startDateCondition\",\"calc\":{\"Type\":\"Compute\",\"params\":{},\"function\":\"startDateCondition\"}},{\"id\":\"Employee\",\"calc\":{\"queryType\":\"Employee\",\"Type\":\"SpecialQuery\",\"params\":{}}},{\"id\":\"testAssetConditions\",\"calc\":{\"Type\":\"Compute\",\"params\":{},\"function\":\"testAssetConditions\"}},{\"id\":\"TimeZone\",\"calc\":{\"Type\":\"Compute\",\"params\":{\"expression\":{\"raw\":\"'Asia/Jerusalem'\",\"value\":\"Asia/Jerusalem\",\"type\":\"Literal\"}},\"function\":\"evalExpression\"}}],\"condition\":{\"arguments\":[{\"computed\":\"false\",\"property\":{\"name\":\"Result\",\"type\":\"Identifier\"},\"object\":{\"computed\":\"false\",\"property\":{\"name\":\"testAssetConditions\",\"type\":\"Identifier\"},\"object\":{\"name\":\"context\",\"type\":\"Identifier\"},\"type\":\"MemberExpression\"},\"type\":\"MemberExpression\"}],\"callee\":{\"name\":\"led.and\",\"type\":\"Identifier\"},\"type\":\"CallExpression\"},\"action\":{\"flowType\":\"onlyFirst\",\"type\":\"actionFlow\",\"smartActions\":[{\"elseAction\":{\"flowType\":\"all\",\"type\":\"actionFlow\",\"smartActions\":[{\"action\":{\"_skip\":\"true\",\"data\":{\"Type\":\"Compute\",\"params\":{\"expression\":{\"computed\":\"false\",\"property\":{\"name\":\"testAssetConditions\",\"type\":\"Identifier\"},\"object\":{\"name\":\"context\",\"type\":\"Identifier\"},\"type\":\"MemberExpression\"}},\"function\":\"evalExpression\"},\"type\":\"addToResponse\"}}]},\"condition\":{\"arguments\":[{\"left\":{\"computed\":\"false\",\"property\":{\"name\":\"RedeemType\",\"type\":\"Identifier\"},\"object\":{\"name\":\"context\",\"type\":\"Identifier\"},\"type\":\"MemberExpression\"},\"type\":\"BinaryExpression\",\"right\":{\"raw\":\"'RedeemGetBenefits'\",\"value\":\"RedeemGetBenefits\",\"type\":\"Literal\"},\"operator\":\"==\"}],\"callee\":{\"name\":\"led.and\",\"type\":\"Identifier\"},\"type\":\"CallExpression\"},\"action\":{\"flowType\":\"onlyFirst\",\"type\":\"actionFlow\",\"smartActions\":[{\"action\":{\"flowType\":\"all\",\"type\":\"actionFlow\",\"smartActions\":[{\"action\":{\"dealCodes\":{\"Type\":\"Compute\",\"params\":{\"dealCodes\":[{\"Code\":\"11223344\"}]},\"function\":\"addDealCodes\"},\"type\":\"addDealCodes\"}}]}}]}}]}}";
    String dataOverride = "{\"ValidFrom\":\"31/10/2016 22:00:00\",\"ValidUntil\":\"30/12/2018 09:59:59\",\"PunchCardType\":\"v2\",\"AllowMultiPunch\":\"N\",\"CodeText\":\"apple\",\"AllowEmployeePunch\":\"Y\",\"PunchCardPrize\":\"\",\"PushMessage\":\"\",\"BackgroundImage\":\"image_medium_127856\",\"Prepaid\":\"N\",\"allowManualPunch\":\"\",\"payment_min\":\"\",\"payment_max\":\"\",\"allow_top_up\":\"\",\"BranchID\":[],\"UserTag\":[],\"ValidTime\":[]}";
    String usableCondition1 = "{\"Name\":\"dueDateCondition\",\"FailStringAppText\":\"assetTemplate.condition.DueDate\",\"FailString\":\"We are sorry, but this is not valid anymore.\"}";
    String usableCondition2 = "{\"Name\":\"startDateCondition\",\"FailStringAppText\":\"assetTemplate.condition.StartDate\",\"FailString\":\"Its not valid yet!\"}";
    NewAsset newAsset;

    public NewAsset buildDefaultAsset(String locationId, String type, AssetDateCondition dateLimit) {
        String redeemAction = getRedeemAction(dateLimit);
        newAsset = new NewAsset();
        newAsset.setData(JsonUtils.convertJsonStringToMap(data));
        Map<String, Object>[] usableCondition = new Map[2];
        usableCondition[0] = JsonUtils.convertJsonStringToMap(usableCondition1);
        usableCondition[1] = JsonUtils.convertJsonStringToMap(usableCondition2);
        newAsset.setUsableConditions(usableCondition);
        newAsset.setDataOverride(JsonUtils.convertJsonStringToMap(dataOverride));
        newAsset.setRedeemAction(redeemAction);
        ArrayList<String> locationIds = new ArrayList<String>();
        locationIds.add(locationId);
        newAsset.setValidAtLocationIDs(locationIds);
        newAsset.setTemplate(templatePrefix + UUID.randomUUID());
        newAsset.setType(type);
        newAsset.setName(namePrefix + UUID.randomUUID());
        newAsset.setDescription("Description");
        newAsset.setValue("0");
        newAsset.setCost("0");
        newAsset.setStockTracked("N");
        newAsset.setActive("Y");
        newAsset.setNotifyAction(new HashMap<String, Object>());

        return newAsset;
    }

    /**
     * Get redeem action according to date condition
     * @param dateLimit
     * @return
     */
    private String getRedeemAction(AssetDateCondition dateLimit) {
        String redeemAction = "{\"elseAction\":{\"flowType\":\"all\",\"type\":\"actionFlow\",\"smartActions\":[{\"action\":{\"_skip\":\"true\",\"data\":{\"Type\":\"Compute\",\"params\":{\"expression\":{\"computed\":\"false\",\"property\":{\"name\":\"testAssetConditions\",\"type\":\"Identifier\"},\"object\":{\"name\":\"context\",\"type\":\"Identifier\"},\"type\":\"MemberExpression\"}},\"function\":\"evalExpression\"},\"type\":\"addToResponse\"}}]},\"calcs\":[{\"id\":\"dueDateCondition\",\"calc\":{\"Type\":\"Compute\",\"params\":{},\"function\":\"dueDateCondition\"}},{\"id\":\"startDateCondition\",\"calc\":{\"Type\":\"Compute\",\"params\":{},\"function\":\"startDateCondition\"}},{\"id\":\"Employee\",\"calc\":{\"queryType\":\"Employee\",\"Type\":\"SpecialQuery\",\"params\":{}}},{\"id\":\"testAssetConditions\",\"calc\":{\"Type\":\"Compute\",\"params\":{},\"function\":\"testAssetConditions\"}},{\"id\":\"TimeZone\",\"calc\":{\"Type\":\"Compute\",\"params\":{\"expression\":{\"raw\":\"'Asia/Jerusalem'\",\"value\":\"Asia/Jerusalem\",\"type\":\"Literal\"}},\"function\":\"evalExpression\"}}],\"condition\":{\"arguments\":[{\"computed\":\"false\",\"property\":{\"name\":\"Result\",\"type\":\"Identifier\"},\"object\":{\"computed\":\"false\",\"property\":{\"name\":\"testAssetConditions\",\"type\":\"Identifier\"},\"object\":{\"name\":\"context\",\"type\":\"Identifier\"},\"type\":\"MemberExpression\"},\"type\":\"MemberExpression\"}],\"callee\":{\"name\":\"led.and\",\"type\":\"Identifier\"},\"type\":\"CallExpression\"},\"action\":{\"flowType\":\"onlyFirst\",\"type\":\"actionFlow\",\"smartActions\":[{\"elseAction\":{\"flowType\":\"all\",\"type\":\"actionFlow\",\"smartActions\":[{\"action\":{\"_skip\":\"true\",\"data\":{\"Type\":\"Compute\",\"params\":{\"expression\":{\"computed\":\"false\",\"property\":{\"name\":\"testAssetConditions\",\"type\":\"Identifier\"},\"object\":{\"name\":\"context\",\"type\":\"Identifier\"},\"type\":\"MemberExpression\"}},\"function\":\"evalExpression\"},\"type\":\"addToResponse\"}}]},\"condition\":{\"arguments\":[{\"left\":{\"computed\":\"false\",\"property\":{\"name\":\"RedeemType\",\"type\":\"Identifier\"},\"object\":{\"name\":\"context\",\"type\":\"Identifier\"},\"type\":\"MemberExpression\"},\"type\":\"BinaryExpression\",\"right\":{\"raw\":\"'RedeemGetBenefits'\",\"value\":\"RedeemGetBenefits\",\"type\":\"Literal\"},\"operator\":\"==\"}],\"callee\":{\"name\":\"led.and\",\"type\":\"Identifier\"},\"type\":\"CallExpression\"},\"action\":{\"flowType\":\"onlyFirst\",\"type\":\"actionFlow\",\"smartActions\":[{"
                + DATE_CONDITION_PLACE_HOLDER
                + "\"action\":{\"flowType\":\"all\",\"type\":\"actionFlow\",\"smartActions\":[{\"action\":{\"dealCodes\":{\"Type\":\"Compute\",\"params\":{\"dealCodes\":[{\"Code\":\"11223344\"}]},\"function\":\"addDealCodes\"},\"type\":\"addDealCodes\"}}]}}]}}]}}";

        if(dateLimit != null){
            dateCondition = dateCondition.replaceAll(FROM_DATE_PLACE_HOLDER, dateLimit.getFrom()).replaceAll(TO_DATE_PLACE_HOLDER, dateLimit.getTo());
            redeemAction = redeemAction.replaceAll(DATE_CONDITION_PLACE_HOLDER, dateCondition);
        }
        else{
            redeemAction = redeemAction.replace(DATE_CONDITION_PLACE_HOLDER, "");
        }
        return redeemAction;
    }

    public NewAsset buildDefaultClubDeals(String locationId) {
        newAsset = new NewAsset();
        newAsset.setData(JsonUtils.convertJsonStringToMap(data));
        Map<String, Object>[] usableCondition = new Map[2];
        usableCondition[0] = JsonUtils.convertJsonStringToMap(usableCondition1);
        usableCondition[1] = JsonUtils.convertJsonStringToMap(usableCondition2);
        newAsset.setUsableConditions(usableCondition);
        newAsset.setDataOverride(JsonUtils.convertJsonStringToMap(dataOverride));
        ArrayList<String> locationIds = new ArrayList<String>();
        locationIds.add(locationId);
        newAsset.setValidAtLocationIDs(locationIds);
        newAsset.setTemplate(templatePrefix + UUID.randomUUID());
        newAsset.setType("deal");
        newAsset.setName(namePrefix + UUID.randomUUID());
        newAsset.setDescription("Description");
        newAsset.setValue("0");
        newAsset.setCost("0");
        newAsset.setStockTracked("N");
        newAsset.setActive("Y");
        newAsset.setNotifyAction(new HashMap<String, Object>());

        return newAsset;
    }


    public void setValidFrom(String validFrom) {
        newAsset.getDataOverride().put("ValidFrom", validFrom);
    }

    public NewAsset getAsset() {
        return newAsset;
    }

    public void setEnumeratorData(HashMap<String, Object> enumeratorData) {
        newAsset.getDataOverride().put("EnumeratorData", enumeratorData);
    }

    public NewAsset getEnumeratorData() {
        return newAsset;
    }
    public void setShortCodeData(HashMap<String, Object> shortCodeData) {
        newAsset.getDataOverride().put("ShortCodeData", shortCodeData);
    }

    public NewAsset getShortCodeData() {
        return newAsset;
    }

}
