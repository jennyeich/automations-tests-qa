package server.common.asset;

import utils.JsonUtils;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Dorit on 12/28/2016.
 */
public class RedeemAction implements IMappable {
    ArrayList<Scenario> scenarios= new ArrayList<>();
    public Scenario addScenario(){
        Scenario scenario= new Scenario();
        scenarios.add(scenario);
        return scenario;
    }

    String elseActionString="{\"flowType\":\"all\",\"type\":\"actionFlow\",\"smartActions\":[{\"action\":{\"_skip\":\"true\",\"data\":{\"Type\":\"Compute\",\"params\":{\"expression\":{\"computed\":\"false\",\"property\":{\"name\":\"testAssetConditions\",\"type\":\"Identifier\"},\"object\":{\"name\":\"context\",\"type\":\"Identifier\"},\"type\":\"MemberExpression\"}},\"function\":\"evalExpression\"},\"type\":\"addToResponse\"}}]}";
    String calcsString="[{\"id\":\"dueDateCondition\",\"calc\":{\"Type\":\"Compute\",\"params\":{},\"function\":\"dueDateCondition\"}},{\"id\":\"startDateCondition\",\"calc\":{\"Type\":\"Compute\",\"params\":{},\"function\":\"startDateCondition\"}},{\"id\":\"Employee\",\"calc\":{\"queryType\":\"Employee\",\"Type\":\"SpecialQuery\",\"params\":{}}},{\"id\":\"testAssetConditions\",\"calc\":{\"Type\":\"Compute\",\"params\":{},\"function\":\"testAssetConditions\"}},{\"id\":\"TimeZone\",\"calc\":{\"Type\":\"Compute\",\"params\":{\"expression\":{\"raw\":\"'Asia/Jerusalem'\",\"value\":\"Asia/Jerusalem\",\"type\":\"Literal\"}},\"function\":\"evalExpression\"}}]";
    String conditionString="{\"arguments\":[{\"computed\":\"false\",\"property\":{\"name\":\"Result\",\"type\":\"Identifier\"},\"object\":{\"computed\":\"false\",\"property\":{\"name\":\"testAssetConditions\",\"type\":\"Identifier\"},\"object\":{\"name\":\"context\",\"type\":\"Identifier\"},\"type\":\"MemberExpression\"},\"type\":\"MemberExpression\"}],\"callee\":{\"name\":\"led.and\",\"type\":\"Identifier\"},\"type\":\"CallExpression\"}";


    @Override
    public Map toMap() {
        Map elseAction= JsonUtils.convertJsonStringToMap(elseActionString);
        ArrayList<Map> calcs=JsonUtils.convertJsonStringToArrayOfMaps(calcsString);
        Map condition=JsonUtils.convertJsonStringToMap(conditionString);
        Map action= SmartActionMapBuilder.buildActionFlowMap("onlyFirst",buildScenarioMap());

        return SmartActionMapBuilder.buildSmartActionMap(calcs,condition,action,elseAction);
    }

    ArrayList<Map> buildScenarioMap(){
        ArrayList<Map> scenariosMap= new ArrayList<>();
        for(Scenario scenario: scenarios){
            scenariosMap.add(scenario.toMap());
        }
        return scenariosMap;
    }
}
