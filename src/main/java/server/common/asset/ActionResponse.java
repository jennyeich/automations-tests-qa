package server.common.asset;

import com.fasterxml.jackson.annotation.JsonGetter;

import java.util.ArrayList;

/**
 * Created by lior.shory on 30/01/2017.
 */
public class ActionResponse {
    public static final String CODE = "Code";
    public static final String REQUIRED_ITEMS = "RequiredItems";

    private String code;
    private ArrayList<String> requiredItems;

    public ActionResponse() {
    }

    public ActionResponse(String dealCode) {
        this.code = dealCode;
    }

    @JsonGetter(CODE)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @JsonGetter(REQUIRED_ITEMS)
    public ArrayList<String> getRequiredItems() {
        if(requiredItems == null){
            requiredItems = new ArrayList<>();
        }
        return requiredItems;
    }

    public void setRequiredItems(ArrayList<String> requiredItems) {
        this.requiredItems = requiredItems;
    }
}
