package server.common.asset;

import server.common.asset.actions.DealCode;
import server.common.asset.actions.Discount;
import server.common.asset.actions.IAction;
import server.common.asset.actions.ItemCode;
import server.common.asset.conditions.ICondition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Dorit on 12/28/2016.
 */
public class Scenario implements IMappable {

    ArrayList<IAction> actions=new ArrayList<>();
    ArrayList<ICondition> conditions=new ArrayList<>();

//    discount addDiscount(){
//        discount discount= new discount();
//        actions.add(discount);
//        return discount;
//    }
    public DealCode addDealCodeAction(){
        DealCode dealCode= new DealCode();
        actions.add(dealCode);
        return dealCode;
    }


    public ItemCode addItemCodeAction(){
        ItemCode itemCode= new ItemCode();
        actions.add(itemCode);
        return itemCode;
    }

    public Discount addDiscountAction(){
        Discount discount= new Discount();
        actions.add(discount);
        return discount;
    }

    public ArrayList<IAction> getActions() {
        return actions;
    }

    @Override
    public Map toMap() {
        Map action= SmartActionMapBuilder.buildActionFlowMap("all",buildActionsMap());
        return SmartActionMapBuilder.buildSmartActionMap(null,null,action,null);
    }

    ArrayList<Map> buildActionsMap(){
        ArrayList<Map> actionsMaps=new ArrayList<>();
        for (IAction action :
                actions) {
            Map<String, Object> actionMap = new HashMap();
            actionMap.put("action",action.toMap());
            actionsMaps.add(actionMap);
        }
        return actionsMaps;
    }
}
