package server.common.asset;

import java.util.Map;

/**
 * Created by Dorit on 12/29/2016.
 */
public interface IMappable {
    Map toMap();
}
