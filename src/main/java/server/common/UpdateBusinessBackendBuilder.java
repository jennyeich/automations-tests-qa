package server.common;

/**
 * Created by Dorit on 12/27/2016.
 */
public class UpdateBusinessBackendBuilder {
    public UpdateBusinessBackend build(String locationId){
        UpdateBusinessBackend updateBusinessBackend= new UpdateBusinessBackend();
        updateBusinessBackend.setGatewayFileName("server/app-engine/xmls/code/GatewayApp.xml");
        updateBusinessBackend.setPath("UpdateBusinessBackend");
        updateBusinessBackend.setServerNamespace("alpha");
        updateBusinessBackend.setLocationID(locationId);
        updateBusinessBackend.setPathPrefix("server/app-engine/xmls/code/api/");
        return updateBusinessBackend;
    }

    public static ApiPair newPair(String key,Object value){
        return new ApiPair(key,value);
    }
}
