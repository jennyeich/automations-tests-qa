package server.common

import server.common.BaseZappServerAPI
import com.fasterxml.jackson.annotation.JsonAnyGetter
import com.fasterxml.jackson.annotation.JsonAnySetter
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import com.gargoylesoftware.htmlunit.HttpMethod
import utils.PropsReader

/**
 * Created by Dorit on 12/26/2016.
 */
class UpdateBusinessBackend : BaseZappServerAPI(){
    @JsonIgnore
    val prop :MutableMap<String,Any?> = mutableMapOf()

    @get:JsonProperty("PathPrefix")
    var pathPrefix  :String?= null

    @get:JsonProperty("ServerNamespace")
    var serverNamespace :String?= null

    @get:JsonProperty("GatewayFileName")
    var gatewayFileName :String?= null

    @get:JsonProperty("Path")
    var path :String?= null

    @get:JsonProperty("LocationID")
    var locationID :String?= null

    @JsonAnySetter
    fun add(key: String, value: String) {
        prop.put(key, value)
    }

    @JsonAnyGetter
    fun getMap(): Map<String, Any?> {
        return prop
    }

    @JsonAnySetter
    fun setMap (props : Map<String, Any?>){
        prop.putAll(props)
    }

    override fun getHttpMethod(): String {
        return HttpMethod.POST.toString();
    }

    override fun getRequestURL(): String {
        return "alpha/api/UpdateBusinessBackend"
    }

    override fun getServerUrl(): String{
        return PropsReader.getPropValuesForEnv("panel.url");
    }

}