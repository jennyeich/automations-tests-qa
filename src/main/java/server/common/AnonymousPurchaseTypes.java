package server.common;

/**
 * Created by Goni on 10/19/2017.
 */

public enum AnonymousPurchaseTypes {

    ALWAYS("Always"),
    NEVER( "Never"),
    NO_IDENTIFIER("NoIdentifier");

    private String type;

    AnonymousPurchaseTypes(String type) {
        this.type = type;
    }

    public String type() {
        return type;
    }
}
