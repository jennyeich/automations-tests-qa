package server.common;

/**
 * Created by Goni on 11/5/2017.
 */

public enum LocationBackendFields {

    DECIMAL_MODE("DecimalMode"),
    PAY_WITH_BUDGET_RATIO("PayWithBudgetRatio"),
    ALLOW_NEGATIVE_POINT_BALANCE("AllowNegativePointBalance"),
    PAY_WITH_BUDGET_TYPE("PayWithBudgetType"),
    PAY_WITH_BUDGET_MIN_SUM("PayWithBudgetMinSum"),
    SUBMIT_PURCHASE_BUDGET_TYPE("SubmitPurchaseBudgetType"),
    PURCHASE_ASSET_BUDGET_TYPE("PurchaseAssetBudgetType"),
    PAY_WITH_BUDGET_MAX_SUM("PayWithBudgetMaxSum"),
    ALLOW_PAY_WITH_BUDGET_CHARGE("AllowPayWithBudgetCharge"),
    CLIENT_VERIFICATION_CODE_EXPIRATION_SECONDS("ClientVerificationCodeExpirationSeconds");

    private String key;

    LocationBackendFields(String key) {
        this.key = key;
    }

    public String key() {
        return key;
    }
}
