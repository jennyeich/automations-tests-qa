package server.common;

/**
 * Created by Goni on 11/1/2017.
 */

public enum PaymentType {

    BUDGET("Budget"),
    POINTS("Points");

    private String type;

    PaymentType(String type) {
        this.type = type;
    }

    public String type() {
        return type;
    }
}
