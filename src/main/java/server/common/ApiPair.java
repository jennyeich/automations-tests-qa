package server.common;

/**
 * Created by Goni on 10/19/2017.
 */

public class ApiPair {

    private String key;
    private Object value;

    public ApiPair(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }

}

