package server.common;

import java.util.Map;

/**
 * Created by amit.levi on 22/05/2017.
 */
public interface IHasRequestHeaderParams {

    Map getRequestHeaderParams();
}
