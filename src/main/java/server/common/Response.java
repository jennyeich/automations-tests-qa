package server.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.json.JSONObject;

/**
 * Created by Dorit on 12/26/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Response implements IServerResponse {

    Integer status=0;
    Integer time = 0;
    JSONObject object;
    @Override
    public Integer getStatusCode() {
        return status;
    }

    @Override
    public void setStatusCode(Integer status) {
        this.status=status;
    }

    @Override
    public Integer getTime() {
        return time;
    }

    @Override
    public void setTime(Integer time) {
        this.time = time;
    }

    @Override
    @JsonIgnore
    public JSONObject getRawObject() {
        return object;
    }

    @Override
    public void setRawObject(JSONObject object) {
        this.object = object;
    }
}
