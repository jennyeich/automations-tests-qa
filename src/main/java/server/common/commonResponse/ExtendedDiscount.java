package server.common.commonResponse;

import com.fasterxml.jackson.annotation.JsonGetter;

import java.util.ArrayList;

/**
 * Created by Jenny on 11/23/2016.
 */
public class ExtendedDiscount {


    public static final String DISCOUNTSPLIT = "DiscountSplit";

    public static final String REWARDEDQUANTITY = "RewardedQuantity";
    public static final String ITEMCODE = "ItemCode";
    public static final String QUANTITY = "Quantity";
    public static final String PRICE = "Price";
    public static final String LINEID = "LineID";
    public static final String LINEDISCOUNT = "LineDiscount";


    private String ItemCode;
    private String Quantity;
    private String Price;
    private String LineID;
    private ArrayList<DiscountSplit> discountSplit;
    private String RewardedQuantity;
    private String LineDiscount;


    @JsonGetter(LINEDISCOUNT)
    public String getLineDiscount() {
        return LineDiscount;
    }

    public void setLineDiscount(String lineDiscount) {
        LineDiscount = lineDiscount;
    }


    @JsonGetter(REWARDEDQUANTITY)
    public String getRewardedQuantity() {
        return RewardedQuantity;
    }

    public void setRewardedQuantity(String rewardedQuantity) {
        RewardedQuantity = rewardedQuantity;
    }


    @JsonGetter(ITEMCODE)
    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }

    @JsonGetter(QUANTITY)
    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    @JsonGetter(PRICE)
    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    @JsonGetter(LINEID)
    public String getLineID() {
        return LineID;
    }

    public void setLineID(String lineID) {
        LineID = lineID;
    }


    @JsonGetter(DISCOUNTSPLIT)
    public ArrayList<DiscountSplit> getDiscountSplit() {
        return discountSplit;
    }

    public void setDiscountSplit(ArrayList<DiscountSplit> discountSplit) {
        this.discountSplit = discountSplit;
    }



}
