package server.common.commonResponse;

import com.fasterxml.jackson.annotation.JsonGetter;

/**
 * Created by Jenny on 11/23/2016.
 */
public class DiscountSplit {


    public static final String QUANTITY = "Quantity";
    public static final String UNITDISCOUNT = "UnitDiscount";

    private String Quantity;
    private String UnitDiscount;


    @JsonGetter(QUANTITY)
    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    @JsonGetter(UNITDISCOUNT)
    public String getUnitDiscount() {
        return UnitDiscount;
    }

    public void setUnitDiscount(String unitDiscount) {
        UnitDiscount = unitDiscount;
    }









}
