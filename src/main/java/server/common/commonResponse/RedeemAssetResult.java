package server.common.commonResponse;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by amit.levi on 04/05/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RedeemAssetResult {

    public static final String REDEEM_RESPONSE_VERSION = "redeemResponseVersion";
    public static final String RESULT = "Result";

    private String redeemResponseVersion;
    private String result;
    //TODO: add deal-codes

    @JsonGetter(REDEEM_RESPONSE_VERSION)
    public String getRedeemResponseVersion() {
        return redeemResponseVersion;
    }

    public void setRedeemResponseVersion(String redeemResponseVersion) {
        this.redeemResponseVersion = redeemResponseVersion;
    }

    @JsonGetter(RESULT)
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
