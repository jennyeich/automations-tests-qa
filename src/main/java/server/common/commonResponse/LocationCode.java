package server.common.commonResponse;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by amit.levi on 04/05/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class LocationCode {

    public static final String STATUS = "Status";
    public static final String CODE = "Code";
    public static final String ASSET_KEY = "AssetKey";
    public static final String BULK_ID = "BulkID";

    private String status;
    private String code;
    private String assetKey;
    private String bulkId;

    @JsonGetter(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonGetter(CODE)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @JsonGetter(ASSET_KEY)
    public String getAssetKey() {
        return assetKey;
    }

    public void setAssetKey(String assetKey) {
        this.assetKey = assetKey;
    }

    @JsonGetter(BULK_ID)
    public String getBulkId() {
        return bulkId;
    }

    public void setBulkId(String bulkId) {
        this.bulkId = bulkId;
    }
}
