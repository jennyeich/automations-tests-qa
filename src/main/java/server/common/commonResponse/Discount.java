package server.common.commonResponse;

import com.fasterxml.jackson.annotation.JsonGetter;

import java.util.ArrayList;

/**
 * Created by Jenny on 11/23/2016.
 */
public class Discount {

    public static final String SUM = "Sum";
    public static final String DESCRIPTION = "Description";
    public static final String EXTENDEDDISCOUNT = "ExtendedDiscount";


    private ArrayList<ExtendedDiscount> extendedDiscount;
    private String sum;
    private String description;

    @JsonGetter(DESCRIPTION)
    public String getDescription() {return description;}

    public void setDescription(String description) {this.description = description;}

    @JsonGetter(EXTENDEDDISCOUNT)
    public ArrayList<ExtendedDiscount> getExtendedDiscount() {
        return extendedDiscount;
    }
    public void setExtendedDiscount(ArrayList<ExtendedDiscount> extendedDiscount) {
        this.extendedDiscount = extendedDiscount;
    }

    @JsonGetter(SUM)
    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }


}
