package server.common

import server.common.BaseZappServerAPI
import com.fasterxml.jackson.annotation.JsonProperty
import com.gargoylesoftware.htmlunit.HttpMethod
import java.util.*

/**
 * Created by Dorit on 12/26/2016.
 */
class InvokeEventBasedActionRule : BaseZappServerAPI() {
    @get:JsonProperty("UserKeys")
    var userKeys : ArrayList<String>? = null
    @get:JsonProperty("MembershipKeys")
    var membershipKeys : ArrayList<String>? = null
    @get:JsonProperty("ActionData")
    var actionData : Map<String,Object>? = null
    @get:JsonProperty("PathPrefix")
    var pathPrefix : String?=null
    @get:JsonProperty("Path")
    var path : String?=null
    @get:JsonProperty("ServerNamespace")
    var serverNamespace : String?=null
    @get:JsonProperty("LocationID")
    var locationID : String?=null
    @get:JsonProperty("Tag")
    var tag : String?=null
    @get:JsonProperty("GatewayFileName")
    var gatewayFileName : String?=null
    @get:JsonProperty("RuleID")
    var ruleID : String?=null


    override fun getHttpMethod(): String {
        return HttpMethod.POST.toString();
    }

    override fun getRequestURL(): String {
        return "alpha/api/InvokeEventBasedActionRule"
    }
}

