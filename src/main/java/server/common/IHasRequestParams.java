package server.common;

import java.util.Map;

/**
 * Created by gili on 2/1/17.
 */
public interface IHasRequestParams {

    Map getRequestParams();
}
