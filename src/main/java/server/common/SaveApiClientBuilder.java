package server.common;

/**
 * Created by Dorit on 12/27/2016.
 */
public class SaveApiClientBuilder {
    public SaveApiClient build(String locationId, String apiKey){
            SaveApiClient saveApiClient= new SaveApiClient();
            saveApiClient.setGatewayFileName("server/app-engine/xmls/code/GatewayApp.xml");
            saveApiClient.setPath("SaveApiClient");
            saveApiClient.setServerNamespace("alpha");
            saveApiClient.setLocationID(locationId);
            saveApiClient.setApiKey(apiKey);
            saveApiClient.setPathPrefix("server/app-engine/xmls/code/api/");
        return saveApiClient;
    }
}
