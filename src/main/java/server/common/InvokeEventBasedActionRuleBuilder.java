package server.common;

import org.jetbrains.annotations.NotNull;
import utils.PropsReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
* Created by Dorit on 12/26/2016.
*/
public class InvokeEventBasedActionRuleBuilder {

    public InvokeEventBasedActionRule buildAssignAssetRequest(String membershipKey, String userKey, String template, String locationId) {
        InvokeEventBasedActionRule assignAssetRequest = new InvokeEventBasedActionRule();
        assignAssetRequest = setDefaultValues(assignAssetRequest);
        assignAssetRequest.setActionData(buildGiveAssetActionData(template));

        return getRequest(membershipKey, userKey, locationId, assignAssetRequest);
    }

    private InvokeEventBasedActionRule setDefaultValues(InvokeEventBasedActionRule request){
        request.setPathPrefix("server/app-engine/xmls/code/api/");
        request.setPath("InvokeEventBasedActionRule");
        request.setServerNamespace(PropsReader.getPropValuesForEnv("namespace"));
        request.setTag("None");
        request.setGatewayFileName("server/app-engine/xmls/code/GatewayApp.xml");
        return request;
    }

    private Map<String, Object> buildGiveAssetActionData(String template) {
        Map<String, Object> action = new HashMap<String, Object>();
        action.put("Template", template);
        action.put("ActionType", "GiveAsset");
        action.put("NotifyUsers", "N");
        Map<String, Object> actionData = new HashMap<String, Object>();
        ArrayList<Map<String, Object>> actions = new ArrayList<>();
        actions.add(action);
        actionData.put("Action", actions);
        return actionData;
    }

    public InvokeEventBasedActionRule buildGivePointsRequest(String membershipKey, String userKey, String locationId, String amount) {
        InvokeEventBasedActionRule givePointsRequest = new InvokeEventBasedActionRule();
        givePointsRequest = setDefaultValues(givePointsRequest);
        givePointsRequest.setActionData(buildGivePointsActionData(amount));

        return getRequest(membershipKey, userKey, locationId, givePointsRequest);

    }

    public InvokeEventBasedActionRule buildGiveCreditRequest(String membershipKey, String userKey, String locationId, String amount) {
        InvokeEventBasedActionRule giveCreditRequest = new InvokeEventBasedActionRule();
        giveCreditRequest = setDefaultValues(giveCreditRequest);
        giveCreditRequest.setActionData(buildGiveCreditActionData(amount));

        return getRequest(membershipKey, userKey, locationId, giveCreditRequest);

    }

    public InvokeEventBasedActionRule buildTagMemberRequest(String membershipKey, String userKey, String locationId, String tag) {
        InvokeEventBasedActionRule tagRequest = new InvokeEventBasedActionRule();
        tagRequest = setDefaultValues(tagRequest);
        tagRequest.setActionData(buildTagMemberActionData(tag));

        return getRequest(membershipKey, userKey, locationId, tagRequest);

    }

    private Map<String,? extends Object> buildTagMemberActionData(String tag) {
        Map<String, Object> action = new HashMap<String, Object>();
        action.put("ActionType", "tagMembership");
        action.put("Operation", "Tag");
        action.put("Tag", tag);
        Map<String, Object> actionData = new HashMap<String, Object>();
        ArrayList<Map<String, Object>> actions = new ArrayList<>();
        actions.add(action);
        actionData.put("Action", actions);
        return actionData;
    }


    public Map<String, Object> buildGiveCreditActionData (String amount){
        Map<String, Object> action = new HashMap<String, Object>();
        action.put("ActionType", "AddPoints");
        action.put("BudgetType", PaymentType.BUDGET.type());
        action.put("Amount", amount);
        Map<String, Object> actionData = new HashMap<String, Object>();
        ArrayList<Map<String, Object>> actions = new ArrayList<>();
        actions.add(action);
        actionData.put("Action", actions);
        return actionData;
    }


    public Map<String, Object> buildGivePointsActionData (String amount){
        Map<String, Object> action = new HashMap<String, Object>();
        action.put("ActionType", "AddPoints");
        action.put("BudgetType", PaymentType.POINTS.type());
        action.put("Amount", amount);
        Map<String, Object> actionData = new HashMap<String, Object>();
        ArrayList<Map<String, Object>> actions = new ArrayList<>();
        actions.add(action);
        actionData.put("Action", actions);
        return actionData;
    }

    public InvokeEventBasedActionRule buildRedeemAssetRequest(String membershipKey, String userKey, String locationId,String assetKey ) {
        InvokeEventBasedActionRule assignAssetRequest = new InvokeEventBasedActionRule();
        assignAssetRequest = setDefaultValues(assignAssetRequest);
        assignAssetRequest.setActionData(buildRedeemAssetActionData(assetKey));

        return getRequest(membershipKey, userKey, locationId, assignAssetRequest);
    }

    private Map<String, Object> buildRedeemAssetActionData (String assetKey){
        Map<String, Object> action = new HashMap<String, Object>();
        action.put("ActionType", "RedeemAsset");
        action.put("AssetKey",assetKey);
        Map<String, Object> actionData = new HashMap<String, Object>();
        ArrayList<Map<String, Object>> actions = new ArrayList<>();
        actions.add(action);
        actionData.put("Action", actions);
        return actionData;
    }

    public InvokeEventBasedActionRule buildUnregisterRequest(String membershipKey, String userKey, String locationId) {
        InvokeEventBasedActionRule assignAssetRequest = new InvokeEventBasedActionRule();
        assignAssetRequest = setDefaultValues(assignAssetRequest);
        assignAssetRequest.setActionData(buildUnregisterActionData());

        return getRequest(membershipKey, userKey, locationId, assignAssetRequest);
    }

    public InvokeEventBasedActionRule buildDeleteRequest(String membershipKey, String userKey, String locationId) {
        InvokeEventBasedActionRule deleteRequest = new InvokeEventBasedActionRule();
        deleteRequest = setDefaultValues(deleteRequest);
        deleteRequest.setActionData(buildDeleteActionData());

        return getRequest(membershipKey, userKey, locationId, deleteRequest);
    }

    private InvokeEventBasedActionRule getRequest(String membershipKey, String userKey, String locationId, InvokeEventBasedActionRule request) {
        ArrayList<String> userKeys = new ArrayList<String>();
        userKeys.add(userKey);
        request.setUserKeys(userKeys);

        ArrayList<String> membershipKeys = new ArrayList<String>();
        membershipKeys.add(membershipKey);
        request.setMembershipKeys(membershipKeys);

        request.setLocationID(locationId);
        request.setRuleID("");

        return request;
    }


    private Map<String, Object> buildUnregisterActionData (){
        Map<String, Object> action = new HashMap<String, Object>();
        action.put("ActionType", "UnregisterMember");
        Map<String, Object> actionData = new HashMap<String, Object>();
        ArrayList<Map<String, Object>> actions = new ArrayList<>();
        actions.add(action);
        actionData.put("Action", actions);
        return actionData;
    }

    private Map<String, Object> buildDeleteActionData (){
        Map<String, Object> action = new HashMap<String, Object>();
        action.put("ActionType", "DeleteMembership");
        Map<String, Object> actionData = new HashMap<String, Object>();
        ArrayList<Map<String, Object>> actions = new ArrayList<>();
        actions.add(action);
        actionData.put("Action", actions);
        return actionData;
    }


}
