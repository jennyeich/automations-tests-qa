package server.common;

import org.json.JSONObject;

/**
 * Created by gili on 11/1/16.
 */
public interface IServerResponse {
    Integer getStatusCode();
    void setStatusCode(Integer status);

    Integer getTime();
    void setTime(Integer time);

    JSONObject getRawObject();
    void setRawObject(JSONObject object);


}
