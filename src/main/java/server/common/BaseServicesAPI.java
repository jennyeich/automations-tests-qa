package server.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gargoylesoftware.htmlunit.HttpMethod;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.HttpRequestWithBody;
import org.apache.log4j.Logger;
import org.junit.Assert;
import server.v2_8.base.ServicesErrorResponse;
import utils.PropsReader;

import java.util.ArrayList;
import java.util.List;


public abstract class BaseServicesAPI implements IServerRequest {
	
	private final String serverUrl = PropsReader.getPropValuesForEnv("server.services.url");
	public static final Logger log4j = Logger.getLogger(BaseServicesAPI.class);

	protected String getServerUrl(){
		return serverUrl;
	}


	@JsonIgnore
	public Object getRequestBody(){
		return this;
	}

	public IServerResponse sendRequestByApiKey(String apiKey, Class<? extends  IServerResponse> responseType){

		String url = String.format("%s%s?api-key=%s",getServerUrl(), getRequestURL(),apiKey);

		return this.sendServicesRequest(url, responseType);
	}

	public  IServerResponse sendRequestByApiKeyAndValidateResponse(String apiKey, Class<? extends  IServerResponse> responseType){

		IServerResponse response = sendRequestByApiKey(apiKey, responseType);

		// make sure the response is not Error response
		if(response instanceof ServicesErrorResponse){
			Assert.fail(((ServicesErrorResponse)response).getErrorMessage());
		}
		return response;
	}


	public  IServerResponse sendRequestByToken(String token, Class<? extends  IServerResponse> responseType){

		String formattedUrl = String.format("%s%s?token=%s",getServerUrl(),getRequestURL(),token);

		return this.sendServicesRequest(formattedUrl, responseType);

	}

	public  IServerResponse sendRequestByTokenAndLocation(String token, String locationID, Class<? extends  IServerResponse> responseType){

		String formattedUrl = String.format("%s%s?InputType=json&MimeType=application/json&Token=%s&LocationID=%s",getServerUrl(),getRequestURL(),token, locationID);

		return this.sendServicesRequest(formattedUrl, responseType);

	}

	public  IServerResponse sendRequestByLocation(String locationID, Class<? extends  IServerResponse> responseType){

		String formattedUrl = String.format("%s%s/%s?InputType=json&MimeType=application/json",getServerUrl(),getRequestURL(),locationID);

		return this.sendServicesRequest(formattedUrl, responseType);

	}


	public Object sendRequestByClass(Class responseType){
		String formattedUrl = String.format("%s%s?InputType=json&MimeType=application/json",getServerUrl(),getRequestURL());

		return this.sendServicesRequestByClass(formattedUrl, responseType);
	}

	public  IServerResponse sendRequest(Class<? extends  IServerResponse> responseType){

		String formattedUrl = String.format("%s%s?InputType=json&MimeType=application/json",getServerUrl(),getRequestURL());

		return this.sendServicesRequest(formattedUrl, responseType);

	}


	public  IServerResponse sendRequestByTokenAndValidateResponse(String token, Class<? extends  IServerResponse> responseType){

		IServerResponse response = sendRequestByToken(token, responseType);

		// make sure the response is not Error response
		if(response instanceof ServicesErrorResponse){
			Assert.fail(((ServicesErrorResponse)response).getErrorMessage());
		}
		return response;

	}


	private  IServerResponse sendServicesRequest(String url, Class<? extends  IServerResponse> responseType){

		ObjectMapper mapper = new ObjectMapper();
		try {
			log4j.info("Prepare Request:" + url);
			String body = mapper.writeValueAsString(getRequestBody());

			HttpResponse response;
			if(getHttpMethod().equals(HttpMethod.GET.toString())){
				response = addRequestParams(Unirest.get(url)).asString();

			}else if(getHttpMethod().equals(HttpMethod.PUT.toString())){
				response = ((HttpRequestWithBody)addRequestParams(Unirest.put(url))).header("content-Type", "application/json")
						.body(body).asJson();
			}
			else {
				response = ((HttpRequestWithBody)addRequestParams(Unirest.post(url))).header("content-Type", "application/json")
						.body(body).asJson();
			}

			IServerResponse result = new Response();
			if(response.getBody() != null){
				if(response.getBody() instanceof JsonNode){
					JsonNode jsonResponse = ((JsonNode) response.getBody());
					if(jsonResponse.getObject().has(ServicesErrorResponse.ERROR_TYPE)){
						responseType = ServicesErrorResponse.class;
					}
					result = mapper.readValue(jsonResponse.getObject().toString(), responseType);
				}
				else{
					if(response.getBody().toString().contains("\"" + ServicesErrorResponse.ERROR_TYPE + "\":")){
						responseType = ServicesErrorResponse.class;
					}
					result = mapper.readValue(response.getBody().toString(), responseType);
				}

			}
			result.setStatusCode(response.getStatus());
			return result;

		} catch (Exception e) {
			throw new RuntimeException("Failed to send request!", e);
		}
	}


	private  String sendServicesRequest(String url)throws Exception{

		ObjectMapper mapper = new ObjectMapper();
		try {
			log4j.info("Prepare Request:" + url);
			String body = mapper.writeValueAsString(getRequestBody());

			HttpResponse response;

			if (getHttpMethod().equals(HttpMethod.POST.toString())) {
				response = addRequestParams(Unirest.post(url)).asString();
				return response.getBody().toString();
			}
			if(getHttpMethod().equals(HttpMethod.GET.toString())) {
				response = addRequestParams(Unirest.get(url)).asString();
				if (response.getStatusText().equals("No Content") && response.getStatus()==204)
					return "success";
				else
					return "NOT_FOUND";
			}
		} catch (Exception e)
			{
				throw new Exception("Failed to return the response");
			}
			return "";
		}



			private  Object sendServicesRequestByClass(String url, Class responseType){

		ObjectMapper mapper = new ObjectMapper();
		try {
			String body = mapper.writeValueAsString(getRequestBody());

			HttpResponse response;
			if(getHttpMethod().equals(HttpMethod.GET.toString())){
				response = addRequestParams(Unirest.get(url)).asString();
			}else if(getHttpMethod().equals(HttpMethod.PUT.toString())){
				response = ((HttpRequestWithBody)addRequestParams(Unirest.put(url))).header("content-Type", "application/json")
						.body(body).asJson();
			}
			else {
				response = ((HttpRequestWithBody)addRequestParams(Unirest.post(url))).header("content-Type", "application/json")
						.body(body).asJson();
			}

			List <Object> result = new ArrayList<>();
			if(response.getBody() != null){
				if(response.getBody() instanceof JsonNode){
					JsonNode jsonResponse = ((JsonNode) response.getBody());
					if(jsonResponse.getObject().has(ServicesErrorResponse.ERROR_TYPE)){
						return mapper.readValue(jsonResponse.getObject().toString(), ServicesErrorResponse.class);
					}
					JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, responseType);
					result = mapper.readValue(jsonResponse.getObject().toString(), type);
				}
				else {
					if(response.getBody().toString().contains(ServicesErrorResponse.ERROR_TYPE)){
						IServerResponse errorResult = mapper.readValue(response.getBody().toString(), ServicesErrorResponse.class);
						errorResult.setStatusCode(response.getStatus());
						return errorResult;
					}

					JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, responseType);
					result = mapper.readValue(response.getBody().toString(), type);
				}
			}
			return result;

		} catch (Exception e) {
			throw new RuntimeException("Failed to send request!", e);
		}
	}


	public  IServerResponse sendRequestByUserTokenAndLocationID(String token, String locationID, Class<? extends  IServerResponse> responseType){

		String formattedUrl = String.format("%s%s?InputType=json&MimeType=application/json&token=%s&locationId=%s",getServerUrl(),getRequestURL(),token,locationID);

		return this.sendServicesRequest(formattedUrl, responseType);

	}


	public  IServerResponse sendRequestByUserToken(String token, Class<? extends  IServerResponse> responseType){

		String formattedUrl = String.format("%s$s?InputType=json&MimeType=application/json&token=%s",getServerUrl(),getRequestURL(),token);

		return this.sendServicesRequest(formattedUrl, responseType);

	}

	public  String sendRequestByUserTokenAndLocationID(String token, String locationID)throws Exception{

		String formattedUrl = String.format("%s%s?InputType=json&MimeType=application/json&token=%s&locationId=%s",getServerUrl(),getRequestURL(),token,locationID);

		return this.sendServicesRequest(formattedUrl);

	}

	public  String sendRequestByUserTokenAndLocationIDAndCode(String token, String locationID,String code)throws Exception{

		String formattedUrl = String.format("%s%s?InputType=json&MimeType=application/json&token=%s&locationId=%s&code=%s",getServerUrl(),getRequestURL(),token,locationID,code);

		return this.sendServicesRequest(formattedUrl);

	}

}
