package server.common;

import com.mashape.unirest.request.HttpRequest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by gili on 11/1/16.
 */
public interface IServerRequest {
    String getRequestURL();

    String getHttpMethod();

    default HttpRequest addRequestParams(HttpRequest request){
        if(this instanceof IHasRequestParams){
            Iterator entries = ((IHasRequestParams)this).getRequestParams().entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry thisEntry = (Map.Entry) entries.next();
                String key = (String) thisEntry.getKey();
                if(thisEntry.getValue() instanceof  String) {
                    String value = (String) thisEntry.getValue();
                    request.queryString(key, value);
                }else if(thisEntry.getValue() instanceof ArrayList){
                    ArrayList<String> values = (ArrayList<String>) thisEntry.getValue();
                    StringBuffer buffer = new StringBuffer();
                    for (String val: values) {
                        buffer.append(val).append(",");
                    }
                    buffer = buffer.delete(buffer.length()-1,buffer.length());
                    request.queryString(key, buffer.toString());
                }


            }
        }

        if(this instanceof IHasRequestHeaderParams){
            Iterator entries = ((IHasRequestHeaderParams)this).getRequestHeaderParams().entrySet().iterator();
            while (entries.hasNext()) {
                Map.Entry thisEntry = (Map.Entry) entries.next();
                String key = (String) thisEntry.getKey();
                String value = (String) thisEntry.getValue();

                request.header(key, value);
            }
        }
        return request;
    }
}
