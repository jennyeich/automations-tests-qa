package server.v4;


import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.v4.base.BaseZappServerAPI;
import server.v4.common.models.Customer;
import server.v4.common.models.Event;
import server.v4.common.models.Purchase;
import server.v4.common.models.RedeemAsset;

import java.util.ArrayList;

/**
 * Created by Jenny on 30/07/2017.
 */
public class SubmitEvent extends BaseZappServerAPI {

    private String baseUrl = "advanced/submitEvent";
    public static final String CUSTOMERS = "customers";
    public static final String TAGS = "tags";


    private ArrayList<Customer> customers;
    private ArrayList<String> tags;
    private Event event;

    public SubmitEvent(String apiKey) {
        this.setApiKey(apiKey);
        setDefauldHeaderParams();
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }


    @JsonProperty(CUSTOMERS)
    public ArrayList<Customer> getCustomers() {
        if(customers == null)
            customers = new ArrayList<>();
        return customers;
    }

    public void setCustomers(ArrayList<Customer> customers) {
        this.customers = customers;
    }

    @JsonGetter(TAGS)
    public ArrayList<String> getTags() {
        if(tags == null)
            tags = new ArrayList<>();
        return tags;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }
    public Event getEvent() {return event;}
    public void setEvent(Event event) {this.event = event;}



}
