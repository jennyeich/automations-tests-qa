package server.v4;

import server.v4.base.BaseZappServerAPI;
import server.common.asset.NewAsset;
import server.v4.common.models.Customer;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.v4.common.models.Deal;
import server.v4.common.models.Purchase;
import server.v4.common.models.RedeemAsset;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 7/3/2017.
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
//@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SubmitPurchase extends BaseZappServerAPI {

    private String baseUrl = "submitPurchase";

    public static final String CUSTOMERS = "customers";
    public static final String PURCHASE = "purchase";
    public static final String REDEEM_ASSETS = "redeemAssets";
    public static final String DEALS = "deals";


    private List<Customer> customers;
    private Purchase purchase;
    private List<RedeemAsset> redeemAssets;



    private List<Deal> deals;

    @JsonProperty(CUSTOMERS)
    public List<Customer> getCustomers() {
        if(customers == null){
            customers = new ArrayList<>();
        }
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }

    @JsonProperty(PURCHASE)
    public Purchase getPurchase() {
        return purchase;

    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }


    @JsonProperty(REDEEM_ASSETS)
    public List<RedeemAsset> getRedeemAssets() {
        if(redeemAssets == null){
            redeemAssets = new ArrayList<>();
        }
        return redeemAssets;
    }

    public void setRedeemAssets(List<RedeemAsset> redeemAssets) {
        this.redeemAssets = redeemAssets;
    }

    @JsonProperty(DEALS)
    public List<Deal> getDeals() {
        if(deals == null){
            deals = new ArrayList<>();
        }
        return deals;
    }

    public void setDeals(List<Deal> deals) {
        this.deals = deals;
    }


    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }
}
