package server.v4;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.v4.base.BaseZappServerAPI;
import server.v4.base.IHeaderParams;
import server.v4.common.models.RedeemAsset;
import server.v4.common.models.Customer;
import server.v4.common.models.Purchase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anastasiya on 02/07/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetBenefits extends BaseZappServerAPI {

    public static final String CUSTOMERS = "customers";
    private static final String PURCHASE = "purchase";
    private static final String REDEEM_ASSETS = "redeemAssets";


    private List<Customer> customers;
    private Purchase purchase;
    private List<RedeemAsset> redeemAssets;

    public GetBenefits(String apiKey) {
        this.setApiKey(apiKey);
        setDefauldHeaderParams();
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return "getBenefits";
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }


    @JsonProperty(CUSTOMERS)
    public List<Customer> getCustomers() {
        if(customers == null)
            customers = new ArrayList<>();
        return customers;
    }

    public void setCustomers(List<Customer> customers) {
        this.customers = customers;
    }


    @JsonProperty(PURCHASE)
    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    @JsonProperty(REDEEM_ASSETS)
    public List<RedeemAsset> getRedeemAssets() {
        if(redeemAssets == null)
            redeemAssets = new ArrayList<>();
        return redeemAssets;
    }

    public void setRedeemAssets(ArrayList<RedeemAsset> redeemAssets) {
        this.redeemAssets = redeemAssets;
    }

}
