package server.v4.base;

import java.util.Map;

/**
 * Created by Goni on 7/2/2017.
 */
public interface IHeaderParams {
    public final String SOURCE_TYPE = "1234";
    public final String SOURCE_NAME = "2222";
    public final String SOURCE_VERSION = "1122";
    public final String BRANCH_ID = "1";
    public final String POS_ID = "121212";
    public final String CHAIN_ID = "1";
    public Map<String,String> getParams();
}
