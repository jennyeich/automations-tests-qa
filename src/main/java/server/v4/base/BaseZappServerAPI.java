package server.v4.base;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.junit.Assert;
import server.common.IHasRequestHeaderParams;
import server.common.IHasRequestParams;
import server.common.IServerResponse;
import utils.PropsReader;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by amit.levi on 22/05/2017.
 */
public abstract class BaseZappServerAPI extends server.common.BaseZappServerAPI implements IHasRequestHeaderParams, IHasRequestParams{

	public static final String serverUrl = PropsReader.getPropValuesForEnv("server.v4.url") + "api/v4/";
	public static final String API_KEY_HEADER_PARAM = "X-Api-Key";
	public static final String BRANCH_ID_HEADER_PARAM = "X-Branch-Id";
	public static final String POS_ID_HEADER_PARAM = "X-Pos-Id";
	public static final String CHAIN_ID_HEADER_PARAM = "X-Chain-Id";
	public static final String X_SOURCE_TYPE = "X-Source-Type";
	public static final String X_SOURCE_NAME = "X-Source-Name";
	public static final String X_SOURCE_VERSION = "X-Source-Version";

	private Map<String, String> headerParams = new HashMap<>();

	private Map<String, String> queryParams = new HashMap<>();

	protected String getServerUrl(){
		return serverUrl;
	}

	@JsonIgnore
	public String getApiKey() {
		return headerParams.get(API_KEY_HEADER_PARAM);
	}

	public void setApiKey(String apiKey) {
		headerParams.put(API_KEY_HEADER_PARAM, apiKey);
	}

	@JsonIgnore
	public String getBranchId() {
		return headerParams.get(BRANCH_ID_HEADER_PARAM);
	}

	public void setBranchId(String branchId) {
		headerParams.put(BRANCH_ID_HEADER_PARAM, branchId);
	}

	public void setDefauldHeaderParams(){
		setBranchId(IHeaderParams.BRANCH_ID);
		setPosId(IHeaderParams.POS_ID);
		setChainId(IHeaderParams.CHAIN_ID);
		setxSourceName(IHeaderParams.SOURCE_NAME);
		setxSourceType(IHeaderParams.SOURCE_TYPE);
		setxSourceVersion(IHeaderParams.SOURCE_VERSION);

	}
	@JsonIgnore
	public String getXSourceType() {
		return headerParams.get(X_SOURCE_TYPE);
	}

	public void setxSourceType(String xSourceType) {
		headerParams.put(X_SOURCE_TYPE, xSourceType);
	}

	@JsonIgnore
	public String getXSourceName() {
		return headerParams.get(X_SOURCE_NAME);
	}

	public void setxSourceName(String xSourceName) {
		headerParams.put(X_SOURCE_NAME, xSourceName);
	}

	@JsonIgnore
	public String getxSourceVersion() {
		return headerParams.get(X_SOURCE_VERSION);
	}

	public void setxSourceVersion(String xSourceVersion) {
		headerParams.put(X_SOURCE_VERSION, xSourceVersion);
	}



	public String getPosId() {
		return headerParams.get(POS_ID_HEADER_PARAM);
	}

	public void setPosId(String posId) {
		headerParams.put(POS_ID_HEADER_PARAM, posId);
	}

	public String getChainId() {
		return headerParams.get(CHAIN_ID_HEADER_PARAM);
	}

	public void setChainId(String chainId) {
		headerParams.put(CHAIN_ID_HEADER_PARAM, chainId);
	}

	public  IServerResponse sendRequestAndValidateResponse(Class<? extends  IServerResponse> responseType){
		String url = getServerUrl().concat(getRequestURL());
		IServerResponse response = super.SendRequest(url, responseType);
		// make sure the response is not Error response
		if(response instanceof ZappServerErrorResponse){
			Assert.assertTrue(((ZappServerErrorResponse) response).getErrorMessage(), false);
		}
		return response;
	}

	public  IServerResponse sendRequest(Class<? extends  IServerResponse> responseType){
		String url = getServerUrl().concat(getRequestURL());
		return super.SendRequest(url, responseType);
	}

	@JsonIgnore
	public void addQueryParameter(String key, String value){
		queryParams.put(key, value);
	}

	@JsonIgnore
	public void setQueryParameters(Map<String, String> queryParams){
		this.queryParams.putAll(queryParams);
	}

	@JsonIgnore
	public void addHeaderParameter(String key, String value){
		headerParams.put(key, value);
	}

	@Override
	@JsonIgnore
	public Map getRequestParams() {
		return queryParams;
	}

	@Override
	@JsonIgnore
	public Map getRequestHeaderParams() {
		return headerParams;
	}

}
