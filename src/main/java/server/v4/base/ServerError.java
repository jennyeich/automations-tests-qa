package server.v4.base;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by amit.levi on 22/05/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServerError {

    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
