package server.v4.base;


import server.common.Response;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by amit.levi on 22/05/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ZappServerErrorResponse extends Response{

    private String status;
    private List<ServerError> errors;


    public String getResult() {
        return status;
    }


    public String getErrorMessage() {
        return errors.toString();
    }
}
