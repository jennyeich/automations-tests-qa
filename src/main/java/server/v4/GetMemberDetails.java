package server.v4;

import server.v4.base.BaseZappServerAPI;
import server.v4.common.models.Customer;
import server.v4.common.models.Purchase;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gargoylesoftware.htmlunit.HttpMethod;

/**
 * Created by Goni on 7/3/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetMemberDetails extends BaseZappServerAPI {

    private String baseUrl = "getMemberDetails";

    public static final String CUSTOMER = "customer";
    public static final String PURCHASE = "purchase";


    private Customer customer;
    private Purchase purchase;


    @JsonProperty(CUSTOMER)
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @JsonProperty(PURCHASE)
    public Purchase getPurchase() {
        return purchase;

    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

}
