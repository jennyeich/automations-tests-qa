package server.v4;

import server.common.Response;
import server.v4.common.models.MemberNote;
import server.v4.common.models.Membership;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

/**
 * Created by Goni on 7/3/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GetMemberDetailsResponse extends Response {

    public static final String STATUS = "status";
    public static final String MEMBERSHIP = "membership";
    public static final String MEMBER_NOTES = "memberNotes";

    private String status;
    private Membership membership;
    private ArrayList<MemberNote> memberNotes;

    public void setMemberNotes(ArrayList<MemberNote> memberNotes) {
        this.memberNotes = memberNotes;
    }
    @JsonProperty(MEMBER_NOTES)
    public ArrayList<MemberNote> getMemberNotes() {
        if (memberNotes==null){
            memberNotes=new ArrayList<>();
        }
        return memberNotes;
    }
    @JsonProperty(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    @JsonProperty(MEMBERSHIP)
    public Membership getMembership() {
        return membership;
    }

    public void setMembership(Membership membership) {
        this.membership = membership;
    }



}
