package server.v4;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.v4.base.BaseZappServerAPI;
import server.v4.common.models.Purchase;

/**
 * Created by Jenny on 7/8/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VoidPurchase extends BaseZappServerAPI{

    private String baseUrl = "voidPurchase";
    public static final String PURCHASE = "purchase";

    private Purchase purchase;

    public VoidPurchase (String apiKey){
        this.setApiKey(apiKey);
        setDefauldHeaderParams();
    }
    @JsonProperty(PURCHASE)
    public Purchase getPurchase() {
        return purchase;

    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

}
