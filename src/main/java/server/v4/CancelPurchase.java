package server.v4;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.v4.base.BaseZappServerAPI;
import server.v4.common.models.Purchase;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class CancelPurchase extends BaseZappServerAPI {

    public static final String CONFIRMATION = "confirmation";
    public static final String PURCHASE = "purchase";

    private String baseUrl = "cancelPurchase";

    private String confirmation;
    private Purchase purchase;


    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

    @JsonProperty(PURCHASE)
    public Purchase getPurchase() {
        return purchase;

    }

    @JsonGetter(CONFIRMATION)
    public String getConfirmation() {return confirmation;}
    public void setConfirmation(String confirmation) {this.confirmation = confirmation;}
}
