package server.v4;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import server.common.Response;
import server.v4.common.models.UpdatedBalance;

/**
 * Created by jenny on 7/8/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VoidPurchaseResponse extends Response {

    public static final String STATUS = "status";

    private String status;

    @JsonProperty(STATUS)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }


}
