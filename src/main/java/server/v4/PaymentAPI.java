package server.v4;

import server.v4.base.BaseZappServerAPI;
import server.v4.common.models.Customer;
import server.v4.common.models.Purchase;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gargoylesoftware.htmlunit.HttpMethod;

/**
 * Created by Goni 02/07/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentAPI extends BaseZappServerAPI{

    private String baseUrl = "payment";

	public static final String AMOUNT = "amount";
	public static final String VARIFICATION_CODE = "verificationCode";
	public static final String CUSTOMER = "customer";
	public static final String PURCHASE = "purchase";


	private int amount ;
	private String verificationCode;
	private Customer customer;
	private Purchase purchase;

	@JsonProperty(AMOUNT)
	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@JsonProperty(VARIFICATION_CODE)
	public String getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(String verificationCode) {
		this.verificationCode = verificationCode;
	}

	@JsonProperty(CUSTOMER)
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@JsonProperty(PURCHASE)
	public Purchase getPurchase() {
		return purchase;

	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}

	@Override
    @JsonIgnore
	public String getRequestURL() {
		return baseUrl;
	}

	@Override
    @JsonIgnore
	public String getHttpMethod() {
		return HttpMethod.POST.toString();
	}
}