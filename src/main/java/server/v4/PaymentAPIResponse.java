package server.v4;

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import server.v4.common.models.UpdatedBalance;
import server.v4.common.models.PaymentReturned;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni 02/07/2017
*/
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentAPIResponse extends Response{

    public static final String STATUS = "status";
    public static final String PAYMENTS = "payments";
    public static final String CONFIRMATION = "confirmation";
    public static final String TYPE = "type";
    public static final String UPDATED_BALANCE = "updatedBalance";

    private String status;
    private List<PaymentReturned> payments;
    private String confirmation;
    private String type;
    private UpdatedBalance updatedBalance;

    @JsonProperty(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty(PAYMENTS)
    public List<PaymentReturned> getPayments() {
        if(payments == null)
            payments = new ArrayList<>();
        return payments;
    }

    public void setPayments(List<PaymentReturned> payments) {
        this.payments = payments;
    }

    @JsonProperty(CONFIRMATION)
    public String getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(String confirmation) {
        this.confirmation = confirmation;
    }

    @JsonProperty(TYPE)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty(UPDATED_BALANCE)
    public UpdatedBalance getUpdatedBalance() {
        return updatedBalance;
    }

    public void setUpdatedBalance(UpdatedBalance updatedBalance) {
        this.updatedBalance = updatedBalance;
    }


}