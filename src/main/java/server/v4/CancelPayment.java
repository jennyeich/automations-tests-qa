package server.v4;

import server.v4.base.BaseZappServerAPI;
import server.v4.common.models.Purchase;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gargoylesoftware.htmlunit.HttpMethod;

/**
 * Created by Goni on 7/3/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CancelPayment extends BaseZappServerAPI{

    private String baseUrl = "cancelPayment";

    public static final String CONFIRMATION = "confirmation";
    public static final String PURCHASE = "purchase";


    private String confirmation ;
    private Purchase purchase;

    @JsonProperty(CONFIRMATION)
    public String getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(String confirmation) {
        this.confirmation = confirmation;
    }

    @JsonProperty(PURCHASE)
    public Purchase getPurchase() {
        return purchase;

    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

}
