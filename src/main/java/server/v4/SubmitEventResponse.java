package server.v4;

import com.fasterxml.jackson.annotation.JsonProperty;
import server.common.Response;
import server.v4.common.models.Deal;

import java.util.List;

/**
 * Created by jenny on 30/07/2017.
 */
public class SubmitEventResponse extends Response {

    public static final String STATUS = "status";
    private String status;


    @JsonProperty(STATUS)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

}
