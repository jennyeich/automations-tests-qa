package server.v4;

import server.common.Response;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import server.v4.common.models.UpdatedBalance;

/**
 * Created by Goni on 7/3/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CancelPaymentResponse extends Response {

    public static final String STATUS = "status";
    public static final String TYPE = "type";
    public static final String UPDATE_BALANCE = "updatedBalance";

    private String status;
    private String type;
    private UpdatedBalance updatedBalance;

    @JsonProperty(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty(TYPE)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty(UPDATE_BALANCE)
    public UpdatedBalance getUpdatedBalance() {
        return updatedBalance;
    }

    public void setUpdatedBalance(UpdatedBalance updatedBalance) {
        this.updatedBalance = updatedBalance;
    }

}
