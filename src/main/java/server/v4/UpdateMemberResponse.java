package server.v4;

import com.fasterxml.jackson.annotation.JsonProperty;
import server.common.Response;

/**
 * Created by jenny on 31/07/2017.
 */
public class UpdateMemberResponse extends Response {

    public static final String STATUS = "status";
    private String status;


    @JsonProperty(STATUS)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

}
