package server.v4;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.v4.base.BaseZappServerAPI;
import server.v4.common.models.RegistrationData;

public class RegisterMemberV4 extends BaseZappServerAPI {

    private String baseUrl = "advanced/registerMember";

    public static final String REGISTRATION_DATA = "registrationData";

    RegistrationData registrationData;


    @JsonProperty(REGISTRATION_DATA)
    public RegistrationData getRegistrationData() {return registrationData;}
    public void setRegistrationData(RegistrationData registrationData) {this.registrationData = registrationData;}

    public RegisterMemberV4(String apiKey) {
        this.setApiKey(apiKey);
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }

}
