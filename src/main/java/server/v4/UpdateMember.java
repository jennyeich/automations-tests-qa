package server.v4;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gargoylesoftware.htmlunit.HttpMethod;
import server.v4.base.BaseZappServerAPI;
import server.v4.common.models.Customer;
import server.v4.common.models.RegistrationData;

/**
 * Created by Jenny on 30/07/2017.
 */
public class UpdateMember extends BaseZappServerAPI {

    private String baseUrl = "advanced/updateMember";
    public static final String CUSTOMER = "customer";

    private Customer customer;
    RegistrationData registrationData;


    @JsonProperty(CUSTOMER)
    public Customer getCustomer() {return customer;}
    public void setCustomer(Customer customer) {this.customer = customer;}

    public RegistrationData getRegistrationData() {return registrationData;}
    public void setRegistrationData(RegistrationData registrationData) {this.registrationData = registrationData;}

    public UpdateMember(String apiKey) {
        this.setApiKey(apiKey);
    }

    @Override
    @JsonIgnore
    public String getRequestURL() {
        return baseUrl;
    }

    @Override
    @JsonIgnore
    public String getHttpMethod() {
        return HttpMethod.POST.toString();
    }







}
