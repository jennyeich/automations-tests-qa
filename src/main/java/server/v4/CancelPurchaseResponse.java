package server.v4;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import server.common.Response;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CancelPurchaseResponse extends Response {

    public static final String STATUS = "status";

    private String status;

    public CancelPurchaseResponse() {}

    @JsonGetter(STATUS)
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }

}
