package server.v4;

import com.fasterxml.jackson.annotation.JsonProperty;
import server.common.Response;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import server.v4.common.models.MemberNote;
import server.v4.common.models.Membership;

import java.util.ArrayList;

/**
 * Created by Goni on 7/3/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubmitPurchaseResponse extends Response {

    public static final String CONFIRMATION = "confirmation";
    public static final String MEMBER_NOTES = "memberNotes";
    public static final String STATUS = "status";

    private String confirmation;
    private ArrayList<MemberNote> memberNotes;
    private String status;

    @JsonProperty(STATUS)
    public String getStatus() {return status;}
    public void setStatus(String status) {this.status = status;}



    public void setMemberNotes(ArrayList<MemberNote> memberNotes) {
        this.memberNotes = memberNotes;
    }
    @JsonProperty(MEMBER_NOTES)
    public ArrayList<MemberNote> getMemberNotes() {
        if (memberNotes==null){
            memberNotes=new ArrayList<>();
        }
        return memberNotes;
    }

    public String getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(String confirmation) {
        this.confirmation = confirmation;
    }
}
