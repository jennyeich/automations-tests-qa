package server.v4.common.models;

/**
 * Created by Goni on 7/26/2017.
 */

public class Balance {

    private UpdatedBalance balance;
    private boolean usedByPayment;

    public UpdatedBalance getBalance() {
        return balance;
    }

    public void setBalance(UpdatedBalance balance) {
        this.balance = balance;
    }

    public boolean isUsedByPayment() {
        return usedByPayment;
    }

    public void setUsedByPayment(boolean usedByPayment) {
        this.usedByPayment = usedByPayment;
    }
}

