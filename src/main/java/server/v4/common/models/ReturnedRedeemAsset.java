package server.v4.common.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 8/20/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReturnedRedeemAsset extends RedeemAsset{

    public static final String NAME = "name";
    public static final String REDEEMABLE = "redeemable";
    public static final String BENEFITS = "benefits";
    public static final String APPLIED_AMOUNT = "appliedAmount";
    public static final String NON_REDEEMABLE_CAUSE = "nonRedeemableCause";


    private List<Benefit> benefits;
    private boolean redeemable;
    private String appliedAmount;
    private NonRedeemableCause nonRedeemableCause;


    @JsonProperty(BENEFITS)
    public List<Benefit>  getBenefits() {
        if(benefits == null)
            benefits = new ArrayList<>();
        return benefits;
    }

    public void setBenefits(List<Benefit>  benefits) {
        this.benefits = benefits;
    }

    @JsonProperty(REDEEMABLE)
    public boolean isRedeemable() {
        return redeemable;
    }

    public void setRedeemable(boolean redeemable) {
        this.redeemable = redeemable;
    }

    @JsonProperty(APPLIED_AMOUNT)
    public String getAppliedAmount() {return appliedAmount;}
    public void setAppliedAmount(String appliedAmount) {this.appliedAmount = appliedAmount;}

    @JsonProperty(NON_REDEEMABLE_CAUSE)
    public NonRedeemableCause getNonRedeemableCause() {return this.nonRedeemableCause;}

    public void setNonRedeemableCause(NonRedeemableCause nonRedeemableCause) {
        this.nonRedeemableCause = nonRedeemableCause;
    }
}

