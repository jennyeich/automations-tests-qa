package server.v4.common.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni 02/07/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Customer {

    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String APP_CLIENT_ID = "appClientId";
    public static final String GOV_ID = "govId";
    public static final String MEMBER_ID = "memberId";
    public static final String CUSTOMER_DENTIFIER = "customIdentifier";

    private String appClientId;
    private String govId;
    private String memberId;
    private String phoneNumber;
    private String customIdentifier;


    public Customer(){}
    public Customer(String phone){
        this.phoneNumber = phone;
    }

    @JsonProperty(CUSTOMER_DENTIFIER)
    public String getCustomIdentifier() {return customIdentifier;}
    public void setCustomIdentifier(String customIdentifier) {this.customIdentifier = customIdentifier;}


    @JsonProperty(PHONE_NUMBER)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @JsonProperty(APP_CLIENT_ID)
    public String getAppClientId() {
        return appClientId;
    }

    public void setAppClientId(String appClientId) {
        this.appClientId = appClientId;
    }
    @JsonProperty(GOV_ID)
    public String getGovId() {
        return govId;
    }

    public void setGovId(String govId) {
        this.govId = govId;
    }
    @JsonProperty(MEMBER_ID)
    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}