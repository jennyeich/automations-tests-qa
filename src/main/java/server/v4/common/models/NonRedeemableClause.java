package server.v4.common.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

/**
 * Created by Jenny on 07/24/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NonRedeemableClause {
    public static final String CODE = "code";
    public static final String MESSAGE = "message";

    private String code;
    private String message;

    @JsonGetter(CODE)
    public String getCode() {return code;}
    public void setCode(String code) {this.code = code;}

    @JsonGetter(MESSAGE)
    public String getMessage() {return message;}
    public void setMessage(String message) {this.message = message;}



}
