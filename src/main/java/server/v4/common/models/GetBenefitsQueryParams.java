package server.v4.common.models;


import server.v4.base.IHeaderParams;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by anastasiya on 02/07/2017.
 */
public class GetBenefitsQueryParams implements IHeaderParams {

    private List<String> expand;
    private boolean markAssetsAsUsed;
    private String returnBenefits;

    public List<String> getExpand() {
        return expand;
    }

    public void setExpand(List<String> expand) {
        this.expand = expand;
    }

    public boolean isMarkAssetsAsUsed() {
        return markAssetsAsUsed;
    }

    public void setMarkAssetsAsUsed(boolean markAssetsAsUsed) {
        this.markAssetsAsUsed = markAssetsAsUsed;
    }

    public String getReturnBenefits() {
        return returnBenefits;
    }

    public void setReturnBenefits(String returnBenefits) {
        this.returnBenefits = returnBenefits;
    }

    private HashMap paramsMap;

    public GetBenefitsQueryParams(){
    }

    public GetBenefitsQueryParams(List<String> expand, boolean markAssetsAsUsed, String returnBenefits){
        this.expand=expand;
        this.markAssetsAsUsed=markAssetsAsUsed;
        this.returnBenefits=returnBenefits;
        initQueryParams();
    }

    @Override
    public Map<String, String> getParams() {
        if(paramsMap==null) {
            initQueryParams();
        }

        return  paramsMap;
    }

    private void initQueryParams() {
        paramsMap = new HashMap();
        paramsMap.put("expand",expand);
        paramsMap.put("markAssetsAsUsed",markAssetsAsUsed);
        paramsMap.put("returnBenefits",returnBenefits);
    }
}
