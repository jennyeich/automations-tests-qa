package server.v4.common.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import server.v4.common.enums.OrderType;
import server.v4.common.enums.Status;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni 02/07/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Purchase {

    public static final String OPEN_TIME = "openTime";
    public static final String TRANSACTION_ID = "transactionId";
    public static final String TOTAL_AMOUNT = "totalAmount";
    public static final String TOTAL_TAX_AMOUNT = "totalTaxAmount";
    public static final String ITEMS = "items";
    public static final String RECEIPT_URL = "receiptUrl";
    public static final String STATUS = "status";
    public static final String EMPLOYEE = "employee";
    public static final String MEAN_OF_PAYMENTS = "meansOfPayment";
    public static final String RELATED_PURCHASE_KEY = "relatedPurchaseKey";
    public static final String ORDER_TYPE = "orderType";
    public static final String RELATED_TRANSACTION_ID = "relatedTransactionId";
    public static final String TAGS = "tags";
    public static final String TOTAL_GENERAL_DISCOUNT = "totalGeneralDiscount";
    public static final String CANCELLATION = "cancellation";
    public static final String CONFIRMATION = "confirmation";

    private Long openTime;
    private String transactionId;
    private String confirmation;
    private Integer totalAmount;
    private Integer totalTaxAmount;
    private List<PurchaseItem> items;
    private List<String> receiptUrl;
    private List<String> tags;
    private Status status;
    private String employee;
    private List<Payment> meansOfPayment;
    private String relatedPurchaseKey;
    private OrderType orderType;
    private String relatedTransactionId;
    private Integer totalGeneralDiscount;
    private boolean cancellation;

    @JsonProperty(TOTAL_TAX_AMOUNT)
    public Integer getTotalTaxAmount() { return totalTaxAmount; }
    public void setTotalTaxAmount(Integer totalTaxAmount) { this.totalTaxAmount = totalTaxAmount; }

    @JsonProperty(OPEN_TIME)
    public Long getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Long openTime) {
        this.openTime = openTime;
    }

    @JsonProperty(TRANSACTION_ID)
    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    @JsonProperty(TOTAL_AMOUNT)
    public Integer getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }
    @JsonProperty(CONFIRMATION)
    public String getConfirmation() {
        return confirmation;
    }

    public void setConfirmation(String confirmation) {
        this.confirmation = confirmation;
    }

    @JsonProperty(ITEMS)
    public List<PurchaseItem> getItems() {
        if(items == null)
            items = new ArrayList<>();
        return items;
    }

    public void setItems(List<PurchaseItem> items) {
        this.items = items;
    }
    @JsonProperty(RECEIPT_URL)
    public List<String> getReceiptUrl() {
        if(receiptUrl == null)
            receiptUrl = new ArrayList<>();
        return receiptUrl;
    }

    public void setReceiptUrl(List<String> receiptUrl) {
        this.receiptUrl = receiptUrl;
    }
    @JsonProperty(TAGS)
    public List<String> getTags() {
        if(tags == null)
            tags = new ArrayList<>();
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    @JsonProperty(STATUS)
    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
    @JsonProperty(EMPLOYEE)
    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }
    @JsonProperty(MEAN_OF_PAYMENTS)
    public List<Payment> getMeansOfPayment() {
        if(meansOfPayment == null)
            meansOfPayment = new ArrayList<>();
        return meansOfPayment;
    }

    public void setMeansOfPayment(List<Payment> meansOfPayment) {
        this.meansOfPayment = meansOfPayment;
    }
    @JsonProperty(RELATED_PURCHASE_KEY)
    public String getRelatedPurchaseKey() {
        return relatedPurchaseKey;
    }

    public void setRelatedPurchaseKey(String relatedPurchaseKey) {
        this.relatedPurchaseKey = relatedPurchaseKey;
    }
    @JsonProperty(ORDER_TYPE)
    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }
    @JsonProperty(RELATED_TRANSACTION_ID)
    public String getRelatedTransactionId() {
        return relatedTransactionId;
    }

    public void setRelatedTransactionId(String relatedTransactionId) {
        this.relatedTransactionId = relatedTransactionId;
    }

    @JsonProperty(TOTAL_GENERAL_DISCOUNT)
    public Integer getTotalGeneralDiscount() {
        return totalGeneralDiscount;
    }

    public void setTotalGeneralDiscount(Integer totalGeneralDiscount) {
        this.totalGeneralDiscount = totalGeneralDiscount;
    }
    @JsonProperty(CANCELLATION)
    public boolean getCancellation() {
        return cancellation;
    }

    public void setCancellation(boolean cancellation) {
        this.cancellation = cancellation;
    }





}