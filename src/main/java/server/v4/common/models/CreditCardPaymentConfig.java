package server.v4.common.models;

/**
 * Created by Goni on 7/25/2017.
 */

/**
 * This class extends the payment config to set the default value for allowPartialPayment= false for credit card payment(Zooz)
 * It overrides the setter for the allowPartialPayment flag
 */
public class CreditCardPaymentConfig extends PaymentConfig {

    public CreditCardPaymentConfig(String paymentMethod, boolean allowPartialPayment) {
        super(paymentMethod, false);
    }

    @Override
    public void setAllowPartialPayment(boolean allowPartialPayment) {
        this.allowPartialPayment = false;
    }

    public boolean isAllowPartialPayment() {
        return false;
    }
}

