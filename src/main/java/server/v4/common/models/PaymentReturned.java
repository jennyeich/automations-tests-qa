package server.v4.common.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import server.v4.common.enums.PaymentMethod;

/**
 * Created by Goni 02/07/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentReturned {

    public static final String PAYMENT_METHOD = "paymentMethod";
    public static final String AMOUNT = "amount";

    private PaymentMethod paymentMethod;
    private int amount;

    @JsonProperty(PAYMENT_METHOD)
    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @JsonProperty(AMOUNT)
    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

}