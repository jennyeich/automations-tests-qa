package server.v4.common.models;

/**
 * Created by tatiana on 8/16/17.
 */
public class NonRedeemableCause {
    public static final String CODE = "code";
    public static final String MESSAGE = "message";
    private String code;
    private String message;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
