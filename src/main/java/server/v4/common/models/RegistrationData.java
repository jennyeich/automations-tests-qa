package server.v4.common.models;


import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jenny 31/07/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RegistrationData {

    private Map fieldNameMap;

    @JsonAnyGetter()
    public Map getFieldNameMap() {
        return fieldNameMap;
    }

    public void addFieldNameMap(String key, String value) {
        if(this.fieldNameMap == null){
            this.fieldNameMap = new HashMap();
        }
        this.fieldNameMap.put(key, value);
    }

    public void setFieldNameMap(Map fieldNameMap) {
        this.fieldNameMap = fieldNameMap;
    }

}