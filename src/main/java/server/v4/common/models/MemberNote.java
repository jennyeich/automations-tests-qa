package server.v4.common.models;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Jenny 20/07/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MemberNote {

    public static final String CONTENT = "content";
    //public static final String NOTE_TARGET_TYPE = "NoteTargetType";
    public static final String NOTE_TYPE = "type";

    private String content;
    //private String noteTargetType;
    private String noteType;


    @JsonProperty(CONTENT)
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

   // @JsonProperty(NOTE_TARGET_TYPE)
   // public String getNoteTargetType() {return noteTargetType;}
   // public void setNoteTargetType(String noteTargetType) {this.noteTargetType = noteTargetType;}

    @JsonProperty(NOTE_TYPE)
    public String getNoteType() {return noteType;}
    public void setNoteType(String noteType) {this.noteType = noteType;}




}