package server.v4.common.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.appengine.repackaged.org.codehaus.jackson.map.annotate.JsonSerialize;
import utils.CustomDateOnlyDeserializer;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Goni on 7/3/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Membership {

    /** Not the final list of member fields **/
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String ALLOW_SMS = "allowSMS";
    public static final String GOV_ID = "govID";
    public static final String EMAIL = "email";
    public static final String EXPIRATION_DATE = "expirationDate";
    public static final String STATUS = "status";
    public static final String MEMBER_ID = "memberID";
    public static final String COMMON_EXTERNAL_ID = "commonExtId";
    public static final String LOCATION_ID = "LocationID";
    public static final String POINTS_BALANCE = "pointsBalance";
    public static final String CREDIT_BALANCE = "creditBalance";
    public static final String BIRTHDAY = "birthday";
    public static final String CREATED_ON = "createdOn";
    public static final String ANNIVERSARY = "anniversary";
    public static final String GENDER = "gender";
    public static final String TAGS = "tags";
    public static final String ASSETS = "assets";


    private String firstName;
    private String lastName;
    private String phoneNumber;
    private Boolean allowSMS;
    private String govID;
    private String email;
    private String memberID;
    private Date birthday;
    private Date expirationDate;
    private Date createdOn;
    private Date anniversary;
    private String commonExtId;
    private String gender;
    private ArrayList<String> tags;
    private ArrayList<Asset> assets;
    private String status;
    private Balance pointsBalance;
    private Balance creditBalance;

    @JsonProperty(POINTS_BALANCE)
    public Balance getPointsBalance() {
        return pointsBalance;
    }

    public void setPointsBalance(Balance pointsBalance) {
        this.pointsBalance = pointsBalance;
    }
    @JsonProperty(CREDIT_BALANCE)
    public Balance getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(Balance creditBalance) {
        this.creditBalance = creditBalance;
    }

    @JsonProperty(FIRST_NAME)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty(LAST_NAME)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    @JsonProperty(PHONE_NUMBER)
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @JsonProperty(ALLOW_SMS)
    public Boolean getAllowSMS() {
        return allowSMS;
    }

    public void setAllowSMS(Boolean allowSMS) {
        this.allowSMS = allowSMS;
    }

    @JsonProperty(GOV_ID)
    public String getGovID() {
        return govID;
    }

    public void setGovID(String govID) {
        this.govID = govID;
    }

    @JsonProperty(EMAIL)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @JsonProperty(MEMBER_ID)
    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }



    @JsonProperty(BIRTHDAY)
    public Date getBirthday() {
        return birthday;
    }

    @JsonDeserialize(using = CustomDateOnlyDeserializer.class)
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @JsonProperty(ANNIVERSARY)
    public Date getAnniversary() {
        return anniversary;
    }

    @JsonDeserialize(using = CustomDateOnlyDeserializer.class)
    public void setAnniversary(Date anniversary) {
        this.anniversary = anniversary;
    }

    @JsonProperty(EXPIRATION_DATE)
    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    @JsonProperty(CREATED_ON)
    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    @JsonProperty(COMMON_EXTERNAL_ID)
    public String getCommonExtId() {
        return commonExtId;
    }

    public void setCommonExtId(String commonExtId) {
        this.commonExtId = commonExtId;
    }

    @JsonProperty(GENDER)
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    @JsonProperty(TAGS)
    public ArrayList<String> getTags() {
        if (tags==null){
            tags=new ArrayList<>();
        }
        return tags;
    }

    public void setTags(ArrayList<String> tag) {
        this.tags = tag;
    }

    @JsonProperty(ASSETS)
    public ArrayList<Asset> getAssets() {
        if (assets==null){
            assets=new ArrayList<>();
        }
        return assets;
    }

    public void setAssets(ArrayList<Asset> assets) {
        this.assets = assets;
    }


    @JsonProperty(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Asset findAssetByName(String name){
        for (Asset asset: assets){
            if (asset.getName().equals(name)) {
                return asset;
            }
        }
        return null;
    }

    public Asset findAssetByKey(String key){
        for (Asset asset: assets){
            if (asset.getKey().equals(key)) {
                return asset;
            }
        }
        return null;
    }
}
