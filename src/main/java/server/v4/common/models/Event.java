package server.v4.common.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by Jenny on 7/30/2017.
 */

public class Event {

    public static final String TYPE = "type";
    public static final String SUB_TYPE = "subType";
    public static final String TIME = "time";

    private Date time;
    private String subType;
    private String type;
    private Data data;

    @JsonProperty(SUB_TYPE)
    public String getSubType() {return subType;}
    public void setSubType(String subType) {this.subType = subType;}

    @JsonProperty(TYPE)
    public String getType() {return type;}
    public void setType(String type) {this.type = type;}

    @JsonProperty(TIME)
    //@JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
    public Data getData() {return data;}
    public void setData(Data data) {this.data = data;}


}
