package server.v4.common.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;



/**
 * Created by anastasiya on 02/07/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Benefit {
    public static final String TYPE = "type";
    public static final String CODE = "code";
    public static final String SUM = "sum";

    private int sum;
    private String code;
    private String type;


    public Benefit() {
    }

    public Benefit(int sum, String code,String type){
        this.sum = sum;
        this.code = code;
        this.type = type;

    }

    @JsonProperty(SUM)
    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    @JsonProperty(CODE)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty(TYPE)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
