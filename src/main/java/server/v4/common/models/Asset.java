package server.v4.common.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Date;

/**
 * Created by Jenny on 07/24/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Asset {
    public static final String NAME = "name";
    public static final String STATUS = "status";
    public static final String DESCRIPTION = "description";
    public static final String KEY = "key";
    public static final String VALID_UNTIL = "validUntil";
    public static final String VALID_FROM = "validFrom";
    public static final String REDEEMABLE = "redeemable";
    // public static final String ARCHIVED = "Archived";
    public static final String IMAGE = "image";
    public static final String NON_REDEEMABLE_CAUSE = "nonRedeemableCause";

    private String name;
    private String status;
    private String description;
    private String key;
    private Date validUntil;
    private Date validFrom;
    private String redeemable;

    NonRedeemableClause nonRedeemableClause;
    // private Boolean archived;
    private String image;


    public Asset() {
    }

    public Asset(String name, String status, String description, String key, Date validUntil, Date validFrom) {
        this.name = name;
        this.status = status;
        this.description = description;
        this.key = key;
        this.validUntil = validUntil;
        this.validFrom = validFrom;
        // this.type = type;
        // this.redeemable = redeemable;
        ///  this.cannotRedeemReasons = cannotRedeemReasons;
        // this.archived = archived;
    }

    @JsonGetter(REDEEMABLE)
    public String getRedeemable() {
        return redeemable;
    }

    public void setRedeemable(String redeemable) {
        this.redeemable = redeemable;
    }

    @JsonGetter(IMAGE)
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @JsonGetter(NAME)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonGetter(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonGetter(DESCRIPTION)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonGetter(KEY)
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @JsonGetter(VALID_UNTIL)
    // @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    @JsonGetter(VALID_FROM)
    // @JsonFormat(pattern = "dd.MM.yyyy HH:mm:ss")
    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    @JsonGetter(NON_REDEEMABLE_CAUSE)
    public NonRedeemableClause getNonRedeemableClause() {return nonRedeemableClause;}
    public void setNonRedeemableClause(NonRedeemableClause nonRedeemableClause) {this.nonRedeemableClause = nonRedeemableClause;}

}
