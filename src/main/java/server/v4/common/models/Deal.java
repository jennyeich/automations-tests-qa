package server.v4.common.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anastasiya on 02/07/2017.
 */
@JsonIgnoreProperties (ignoreUnknown = true)
public class Deal {
    public static final String NAME = "name";
    public static final String BENEFITS = "benefits";
    public static final String CODE = "code";
    public static final String KEY = "key";
    public static final String APPLIED_AMOUNT = "appliedAmount";

    private String code;
    private String key;
    private String appliedAmount;
    private String name;
    private List<Benefit> benefits;


    @JsonProperty(KEY)
    public String getKey() {
        return key;
    }
    public void setKey(String key) {
        this.key = key;
    }

    @JsonProperty(CODE)
    public String getCode() {
        return code;
    }
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty(APPLIED_AMOUNT)
    public String getAppliedAmount() {return appliedAmount;}
    public void setAppliedAmount(String appliedAmount) {this.appliedAmount = appliedAmount;}


    public Deal() {
    }

    public Deal(String name, List<Benefit> benefits){
        this.name = name;
        this.benefits = benefits;
    }

    @JsonProperty(NAME)
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty(BENEFITS)
    public List<Benefit>  getBenefits() {
        if(benefits == null)
            benefits = new ArrayList<>();
        return benefits;
    }
    public void setBenefits(List<Benefit>  benefits) {
        this.benefits = benefits;
    }
}
