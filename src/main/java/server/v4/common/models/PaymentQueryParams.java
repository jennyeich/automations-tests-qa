package server.v4.common.models;

import com.google.appengine.repackaged.org.apache.commons.collections.map.HashedMap;
import server.v4.base.IHeaderParams;
import server.v4.common.enums.Mode;
import server.v4.common.enums.PaymentMethod;

import java.util.Map;

/**
 * Created by Goni 02/07/2017
 */
public class PaymentQueryParams implements IHeaderParams {

	public static final String MODE = "mode";
	public static final String ALLOW_PAYMENT_METHOD = "allowedPaymentMethod";


	private Mode mode = Mode.PAY;
	private PaymentMethod allowedPaymentMethod = PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT;


	private Map<String,String> queryParams = new HashedMap();
	public Mode getMode() {
		return mode;
	}

	public void setMode(Mode mode) {
		this.mode = mode;
		queryParams.put(MODE,mode.name());
	}

	public PaymentMethod getAllowedPaymentMethod() {
		return allowedPaymentMethod;
	}

	public void setAllowedPaymentMethod(PaymentMethod allowedPaymentMethod) {
		this.allowedPaymentMethod = allowedPaymentMethod;
		queryParams.put(ALLOW_PAYMENT_METHOD,this.allowedPaymentMethod.toString());
	}


	@Override
	public Map<String, String> getParams() {
		if(queryParams.isEmpty()){
			queryParams.put("mode",Mode.PAY.toString());
			queryParams.put("allowedPaymentMethod",PaymentMethod.MEAN_OF_PAYMENT_OR_DISCOUNT.toString());
			queryParams.put("allowPartialPayment","false");
		}
		return queryParams;
	}
}