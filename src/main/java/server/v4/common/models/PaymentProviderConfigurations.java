package server.v4.common.models;

/**
 * Created by Goni on 7/6/2017.
 */
public class PaymentProviderConfigurations {

    private PaymentConfig paymentWallet;
    private PaymentConfig giftCard;
    private CreditCardPaymentConfig creditCard;


    public PaymentConfig getPaymentWallet() {
        return paymentWallet;
    }

    public void setPaymentWallet(PaymentConfig paymentWallet) {
        this.paymentWallet = paymentWallet;
    }

    public PaymentConfig getGiftCard() {
        return giftCard;
    }

    public void setGiftCard(PaymentConfig giftCard) {
        this.giftCard = giftCard;
    }

    public CreditCardPaymentConfig getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(CreditCardPaymentConfig creditCard) {
        this.creditCard = creditCard;
    }
}
