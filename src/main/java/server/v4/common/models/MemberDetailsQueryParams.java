package server.v4.common.models;

import com.google.appengine.repackaged.org.apache.commons.collections.map.HashedMap;
import server.v4.base.IHeaderParams;

import java.util.List;
import java.util.Map;

/**
 * Created by Goni on 7/4/2017.
 */
public class MemberDetailsQueryParams implements IHeaderParams {
    public static final String EXPAND = "expand";
    public static final String RETURN_ASSETS = "returnAssets";

    private List<String> expand;
    private String returnAssets;
    private Map<String,String> queryParams = new HashedMap();

    public void setQueryParams(Map<String, String> queryParams) {this.queryParams = queryParams;}
    public List<String> getExpand() {
        return expand;
    }
    public void setExpand(List<String> expand) {
        this.expand = expand;
    }
    public String getReturnAssets() {
        return returnAssets;
    }
    public void setReturnAssets(String returnAssets) {
        this.returnAssets = returnAssets;
    }

    @Override
    public Map<String, String> getParams() {

        if(queryParams.isEmpty()){
            queryParams.put(RETURN_ASSETS,"all");
        }
        return queryParams;
    }
}
