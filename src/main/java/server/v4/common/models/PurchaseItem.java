package server.v4.common.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import server.v4.common.enums.Action;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni 02/07/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PurchaseItem {

    public static final String CODE = "code";
    public static final String NAME = "name";
    public static final String ACTION = "action";
    public static final String QUANTITY = "quantity";
    public static final String WEIGHT = "weight";
    public static final String GROSS_AMOUNT = "grossAmount";
    public static final String NET_AMOUNT = "netAmount";
    public static final String LINE_ID = "lineId";
    public static final String DEPARTMENT_CODE = "departmentCode";
    public static final String DEPARTMENT_NAME = "departmentName";
    public static final String TAGS = "tags";

    private String code;
    private String name;
    private Action action;
    private Integer quantity;
    private Double weight;
    private Integer netAmount;
    private Integer grossAmount;
    private String lineId;
    private String departmentCode;
    private String departmentName;
    private List<String> tags;

    @JsonProperty(CODE)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    @JsonProperty(NAME)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    @JsonProperty(ACTION)
    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    @JsonProperty(QUANTITY)
    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
    @JsonProperty(WEIGHT)
    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    @JsonProperty(NET_AMOUNT)
    public Integer getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(Integer netAmount) {
        this.netAmount = netAmount;
    }
    @JsonProperty(GROSS_AMOUNT)
    public Integer getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(Integer grossAmount) {
        this.grossAmount = grossAmount;
    }

    @JsonProperty(LINE_ID)
    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }
    @JsonProperty(DEPARTMENT_CODE)
    public String getDepartmentCode() {
        return departmentCode;
    }

    public void setDepartmentCode(String departmentCode) {
        this.departmentCode = departmentCode;
    }
    @JsonProperty(DEPARTMENT_NAME)
    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    @JsonProperty(TAGS)
    public List<String> getTags() {
        if(tags == null)
            tags = new ArrayList<String>();
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }



}