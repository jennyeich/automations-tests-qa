package server.v4.common.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni on 8/20/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class RedeemAssetOfSubmitPurchase extends RedeemAsset {

    public static final String APPLIED_AMOUNT = "appliedAmount";

    private String appliedAmount;

    @JsonProperty(APPLIED_AMOUNT)
    public String getAppliedAmount() {return appliedAmount;}

    public void setAppliedAmount(String appliedAmount) {this.appliedAmount = appliedAmount;}
}

