package server.v4.common.models;

import com.fasterxml.jackson.annotation.JsonGetter;

/**
 * Created by Goni 02/07/2017
 */
public class UpdatedBalance {

	public static final String MONETARY = "monetary";
	public static final String NON_MONETARY = "nonMonetary";

	private int monetary;
	private int nonMonetary;

	@JsonGetter(MONETARY)
	public int getMonetary() {
		return monetary;
	}

	public void setMonetary(int monetary) {
		this.monetary = monetary;
	}
	@JsonGetter(NON_MONETARY)
	public int getNonMonetary() {
		return nonMonetary;
	}

	public void setNonMonetary(int nonMonetary) {
		this.nonMonetary = nonMonetary;
	}

}