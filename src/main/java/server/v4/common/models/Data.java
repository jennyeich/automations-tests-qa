package server.v4.common.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import server.v4.common.enums.Type;

/**
 * Created by Jenny 30/07/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {

    public static final String CONTENT = "content";
    public static final String SOUND = "sound";
    public static final String OVER_LINE = "overLine";
    public static final String SENT_TIME_IN_MINUTES = "sentTimeInMinutes";
    public static final String PROTOCOL = "protocol";

    private String content;
    private String sound;
    private String overLine;
    private int sentTimeInMinutes;
    private String protocol;

    @JsonProperty(CONTENT)
    public String getContent() {return content;}
    public void setContent(String content) {this.content = content;}

    @JsonProperty(SOUND)
    public String getSound() {return sound;}
    public void setSound(String sound) {this.sound = sound;}

    @JsonProperty(OVER_LINE)
    public String getOverLine() {return overLine;}
    public void setOverLine(String overLine) {this.overLine = overLine;}

    @JsonProperty(SENT_TIME_IN_MINUTES)
    public int getSentTimeInMinutes() {return sentTimeInMinutes;}
    public void setSentTimeInMinutes(int sentTimeInMinutes) {this.sentTimeInMinutes = sentTimeInMinutes;}

    @JsonProperty(PROTOCOL)
    public String getProtocol() {return protocol;}
    public void setProtocol(String protocol) {this.protocol = protocol;}






}