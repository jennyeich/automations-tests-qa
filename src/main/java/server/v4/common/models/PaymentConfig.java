package server.v4.common.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni on 7/25/2017.
 */

public class PaymentConfig {

    protected boolean allowPartialPayment;
    protected String paymentMethod ;

    public PaymentConfig(String paymentMethod,boolean allowPartialPayment){
        this.paymentMethod = paymentMethod;
        this.allowPartialPayment = allowPartialPayment;
    }

    public boolean isAllowPartialPayment() {
        return allowPartialPayment;
    }
    public void setAllowPartialPayment(boolean allowPartialPayment) {
        this.allowPartialPayment = allowPartialPayment;
    }
    public String getPaymentMethod() {
        return paymentMethod;
    }
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}

