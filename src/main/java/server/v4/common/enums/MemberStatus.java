package server.v4.common.enums;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni on 7/3/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public enum MemberStatus {

    @JsonProperty(V4ConstantsLabels.ACTIVE)
    ACTIVE(V4ConstantsLabels.ACTIVE),
    @JsonProperty(V4ConstantsLabels.INACTIVE)
    INACTIVE(V4ConstantsLabels.INACTIVE),
    @JsonProperty(V4ConstantsLabels.EXPIRED)
    EXPIRED(V4ConstantsLabels.EXPIRED),
    @JsonProperty(V4ConstantsLabels.PENDING)
    PENDING(V4ConstantsLabels.PENDING);

    // the value which is used for matching
    // the json node value with this enum
    private final String value;

    MemberStatus(final String type) {
        value = type;
    }

    @Override
    public String toString() {
        return value;
    }


}
