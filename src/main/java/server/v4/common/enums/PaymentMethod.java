package server.v4.common.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni 02/07/2017
 */
public enum PaymentMethod {

	@JsonProperty(V4ConstantsLabels.DISCOUNT)
	DISCOUNT(V4ConstantsLabels.DISCOUNT),
	@JsonProperty(V4ConstantsLabels.MEAN_OF_PAYMENT)
	MEAN_OF_PAYMENT(V4ConstantsLabels.MEAN_OF_PAYMENT),
	@JsonProperty(V4ConstantsLabels.MEAN_OF_PAYMENT_OR_DISCOUNT)
	MEAN_OF_PAYMENT_OR_DISCOUNT(V4ConstantsLabels.MEAN_OF_PAYMENT_OR_DISCOUNT);

	// the value which is used for matching
	// the json node value with this enum
	private final String value;

	PaymentMethod(final String type) {
		value = type;
	}

	@Override
	public String toString() {
		return value;
	}

}