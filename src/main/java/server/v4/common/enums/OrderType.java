package server.v4.common.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni on 7/2/2017.
 */
public enum OrderType {
    @JsonProperty(V4ConstantsLabels.TAKE_AWAY)
    TAKE_AWAY(V4ConstantsLabels.TAKE_AWAY),
    @JsonProperty(V4ConstantsLabels.DINE_IN)
    DINE_IN(V4ConstantsLabels.DINE_IN),
    @JsonProperty(V4ConstantsLabels.DELIVERY)
    DELIVERY(V4ConstantsLabels.DELIVERY);

    // the value which is used for matching
    // the json node value with this enum
    private final String value;

    OrderType(final String type) {
        value = type;
    }

    @Override
    public String toString() {
        return value;
    }
}
