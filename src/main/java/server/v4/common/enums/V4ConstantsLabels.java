package server.v4.common.enums;

public class V4ConstantsLabels {

    //payment method
    public static final String DISCOUNT = "discount";
    public static final String MEAN_OF_PAYMENT = "meanOfPayment";
    public static final String MEAN_OF_PAYMENT_OR_DISCOUNT = "meanOfPaymentOrDiscount";

    //payment provider
    public static final String CREDIT_CARD_KEY="creditCard";
    public static final String PAYMENT_WALLET_KEY="paymentWallet";
    public static final String GIFT_CARD_KEY="giftCard";

    //Action
    public static final String SALE = "sale";
    public static final String REFUND = "refund";
    public static final String VOID = "void";

    //Returned benefits types
    public static final String ALL = "all";
    public static final String DEALS = "deals";
    public static final String REDEEM_ASSETS = "redeemAssets";

    //Mode
    public static final String PAY = "pay";
    public static final String QUERY = "query";


    //Expand types
    public static final String DISCOUNT_BY_DISCOUNT = "discountByDiscount";

    //Order type
    public static final String TAKE_AWAY = "takeAway";
    public static final String DINE_IN = "dineIn";
    public static final String DELIVERY = "delivery";

    //Wallet type
    public static final String CREDIT_CARD = "creditCard";
    public static final String MEMBER_POINTS = "memberPoints";
    public static final String MEMBER_CREDIT = "memberCredit";
    public static final String GIFT_CARD = "giftCard";

    //Status
    public static final String OPEN = "open";
    public static final String FINAL = "final";

    //type
    public static final String CASH = "cash";
    public static final String DEBIT_CARD = "debitCard";
    public static final String CLUB_BUDGET = "clubBudget";
    public static final String VOUCHER = "voucher";
    public static final String COUPON = "coupon";
    public static final String CHEQUE = "cheque";
    public static final String PRE_PAID = "prePaid";
    public static final String DEBT = "debt";


    //member status
    public static final String ACTIVE = "Active";
    public static final String INACTIVE = "Inactive";
    public static final String PENDING = "Pending";
    public static final String EXPIRED = "Expired ";

    // Asset Status
    public static final String ASSET_STATUS_ACTIVE = "active";
    public static final String ASSET_STATUS_INACTIVE = "inactive";
    public static final String ASSET_STATUS_ALL = "all";

    // Asset Expand Types
    public static final String ASSETS_REDEEMABLE = "assets.redeemable";

}
