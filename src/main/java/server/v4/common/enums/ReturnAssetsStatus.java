package server.v4.common.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by lior on 8/6/17.
 */
public enum ReturnAssetsStatus {

    @JsonProperty(V4ConstantsLabels.ASSET_STATUS_ACTIVE)
    ASSET_STATUS_ACTIVE(V4ConstantsLabels.ASSET_STATUS_ACTIVE),
    @JsonProperty(V4ConstantsLabels.ASSET_STATUS_INACTIVE)
    ASSET_STATUS_INACTIVE(V4ConstantsLabels.ASSET_STATUS_INACTIVE),
    @JsonProperty(V4ConstantsLabels.ASSET_STATUS_ALL)
    ASSET_STATUS_ALL(V4ConstantsLabels.ASSET_STATUS_ALL);

    // the value which is used for matching
    // the json node value with this enum
    private final String value;

    ReturnAssetsStatus(final String type) {
        value = type;
    }

    @Override
    public String toString() {
        return value;
    }

}
