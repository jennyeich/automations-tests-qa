package server.v4.common.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni on 7/2/2017.
 */
public enum Action {
    @JsonProperty(V4ConstantsLabels.SALE)
    SALE(V4ConstantsLabels.SALE),
    @JsonProperty(V4ConstantsLabels.REFUND)
    REFUND(V4ConstantsLabels.REFUND),
    @JsonProperty(V4ConstantsLabels.VOID)
    VOID(V4ConstantsLabels.VOID);

    // the value which is used for matching
    // the json node value with this enum
    private final String value;

    Action(final String type) {
        value = type;
    }

    @Override
    public String toString() {
        return value;
    }
}
