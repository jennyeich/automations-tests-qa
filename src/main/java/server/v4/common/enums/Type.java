package server.v4.common.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni on 7/2/2017.
 */
public enum Type {
    @JsonProperty(V4ConstantsLabels.CASH)
    CASH(V4ConstantsLabels.CASH),
    @JsonProperty(V4ConstantsLabels.CREDIT_CARD)
    CREDIT_CARD(V4ConstantsLabels.CREDIT_CARD),
    @JsonProperty(V4ConstantsLabels.DEBIT_CARD)
    DEBIT_CARD(V4ConstantsLabels.DEBIT_CARD),
    @JsonProperty(V4ConstantsLabels.CLUB_BUDGET)
    CLUB_BUDGET(V4ConstantsLabels.CLUB_BUDGET),
    @JsonProperty(V4ConstantsLabels.VOUCHER)
    VOUCHER(V4ConstantsLabels.VOUCHER),
    @JsonProperty(V4ConstantsLabels.COUPON)
    COUPON(V4ConstantsLabels.COUPON),
    @JsonProperty(V4ConstantsLabels.CHEQUE)
    CHEQUE(V4ConstantsLabels.CHEQUE),
    @JsonProperty(V4ConstantsLabels.PRE_PAID)
    PRE_PAID(V4ConstantsLabels.PRE_PAID),
    @JsonProperty(V4ConstantsLabels.DEBT)
    DEBT(V4ConstantsLabels.DEBT);

    // the value which is used for matching
    // the json node value with this enum
    private final String value;

    Type(final String type) {
        value = type;
    }

    @Override
    public String toString() {
        return value;
    }
}
