package server.v4.common.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni on 7/20/2017.
 */

public enum ReturnedBenefits {
    @JsonProperty(V4ConstantsLabels.ALL)
    ALL(V4ConstantsLabels.ALL),
    @JsonProperty(V4ConstantsLabels.DEALS)
    DEALS(V4ConstantsLabels.DEALS),
    @JsonProperty(V4ConstantsLabels.REDEEM_ASSETS)
    REDEEM_ASSETS(V4ConstantsLabels.REDEEM_ASSETS);

    // the value which is used for matching
    // the json node value with this enum
    private final String value;

    ReturnedBenefits(final String type) {
        value = type;
    }

    @Override
    public String toString() {
        return value;
    }
}
