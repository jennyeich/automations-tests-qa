package server.v4.common.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni on 7/2/2017.
 */
public enum Mode {
    @JsonProperty(V4ConstantsLabels.PAY)
    PAY(V4ConstantsLabels.PAY),
    @JsonProperty(V4ConstantsLabels.QUERY)
    QUERY(V4ConstantsLabels.QUERY);

    // the value which is used for matching
    // the json node value with this enum
    private final String value;

    Mode(final String type) {
        value = type;
    }

    @Override
    public String toString() {
        return value;
    }
}
