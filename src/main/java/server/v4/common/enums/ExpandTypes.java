package server.v4.common.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni on 7/20/2017.
 */

public enum ExpandTypes {

    @JsonProperty(V4ConstantsLabels.DISCOUNT_BY_DISCOUNT)
    DISCOUNT_BY_DISCOUNT(V4ConstantsLabels.DISCOUNT_BY_DISCOUNT);


    // the value which is used for matching
    // the json node value with this enum
    private final String value;

    ExpandTypes(final String type) {
        value = type;
    }

    @Override
    public String toString() {
        return value;
    }

}

