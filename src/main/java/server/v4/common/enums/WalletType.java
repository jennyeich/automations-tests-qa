package server.v4.common.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni 02/07/2017
 */
public enum WalletType {
	@JsonProperty(V4ConstantsLabels.CREDIT_CARD)
	CREDIT_CARD(V4ConstantsLabels.CREDIT_CARD),
	@JsonProperty(V4ConstantsLabels.MEMBER_POINTS)
	MEMBER_POINTS(V4ConstantsLabels.MEMBER_POINTS),
	@JsonProperty(V4ConstantsLabels.MEMBER_CREDIT)
	MEMBER_CREDIT(V4ConstantsLabels.MEMBER_CREDIT),
	@JsonProperty(V4ConstantsLabels.GIFT_CARD)
	GIFT_CARD(V4ConstantsLabels.GIFT_CARD);;


	// the value which is used for matching
	// the json node value with this enum
	private final String value;

	WalletType(final String type) {
		value = type;
	}

	@Override
	public String toString() {
		return value;
	}

}

