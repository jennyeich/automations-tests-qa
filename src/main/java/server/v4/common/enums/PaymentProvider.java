package server.v4.common.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni on 7/25/2017.
 */

public enum PaymentProvider {

    @JsonProperty(V4ConstantsLabels.CREDIT_CARD_KEY)
    CREDIT_CARD_KEY(V4ConstantsLabels.CREDIT_CARD_KEY),
    @JsonProperty(V4ConstantsLabels.PAYMENT_WALLET_KEY)
    PAYMENT_WALLET_KEY(V4ConstantsLabels.PAYMENT_WALLET_KEY),
    @JsonProperty(V4ConstantsLabels.GIFT_CARD_KEY)
    GIFT_CARD_KEY(V4ConstantsLabels.GIFT_CARD_KEY);

    // the value which is used for matching
    // the json node value with this enum
    private final String value;

    PaymentProvider(final String type) {
        value = type;
    }

    @Override
    public String toString() {
        return value;
    }
}
