package server.v4.common.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Goni on 7/2/2017.
 */
public enum Status {
    @JsonProperty(V4ConstantsLabels.OPEN)
    OPEN(V4ConstantsLabels.OPEN)  ,
    @JsonProperty(V4ConstantsLabels.FINAL)
    FINAL(V4ConstantsLabels.FINAL) ;
    // the value which is used for matching
    // the json node value with this enum
    private final String value;

    Status(final String type) {
        value = type;
    }

    @Override
    public String toString() {
        return value;
    }

}
