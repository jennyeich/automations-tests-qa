package server.v4;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import server.common.Response;
import server.v4.common.models.Deal;
import server.v4.common.models.RedeemAsset;
import server.v4.common.models.ReturnedRedeemAsset;

import java.util.List;

/**
 * Created by anastasiya on 02/07/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GetBenefitsResponse extends Response {

    public static final String STATUS = "status";
    public static final String DEALS = "deals";
    public static final String REDEEM_ASSETS = "redeemAssets";
    public static final String TOTAL_DISCOUNTS_SUM = "totalDiscountsSum";

    private String status;
    private List<Deal> deals;
    private List<ReturnedRedeemAsset> redeemAssets;
    private Integer totalDiscountsSum;

    @JsonProperty(STATUS)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty(DEALS)
    public List<Deal> getDeals() {
        return deals;
    }
    public void setDeals(List<Deal> deals) {
        this.deals = deals;
    }
    @JsonProperty(REDEEM_ASSETS)
    public List<ReturnedRedeemAsset> getRedeemAssets() {
        return redeemAssets;
    }

    public void setRedeemAssets(List<ReturnedRedeemAsset> redeemAssets) {
        this.redeemAssets = redeemAssets;
    }

    @JsonProperty(TOTAL_DISCOUNTS_SUM)
    public Integer getTotalDiscountSum() {
        return totalDiscountsSum;
    }
    public void setTotalDiscountSum(Integer totalDiscountsSum) {
        this.totalDiscountsSum = totalDiscountsSum;
    }
}
