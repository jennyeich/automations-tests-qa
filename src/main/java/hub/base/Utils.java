package hub.base;

/**
 * Created by Jenny on 11/21/2016.
 */
public class Utils {

    public static <T extends Enum<T>> T getEnumFromString(
            Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.toString().equals(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format(
                "There is no value with name '%s' in Enum %s",
                name, enumeration.getName()
        ));
    }

}
