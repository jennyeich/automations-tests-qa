package hub.base;

import hub.common.objects.model.ExportItemModel;
import hub.hub1_0.base.basePages.BaseTabsPage;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.basePages.loyalty.gifts.GiftDisplay;
import hub.hub2_0.services.loyalty.BaseHub2Service;
import hub.services.LoginLogoutService;
import hub.services.UIDebugLogger;
import hub.services.member.MembersService;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.AutomationException;
import utils.JsonUtils;
import utils.PropsReader;

import javax.management.ReflectionException;
import java.beans.IntrospectionException;
import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * Created by Jenny on 11/8/2016.
 */
public class BaseService {

    public static final long STANDART_PAGE_LOAD_WAIT_TIME_SEC = 200;
    public static final long TIMEOUT_FOR_PAGE_LOAD_WAIT_SEC = 200;
    public static final long WAIT_AFTER_CLICK_MILLSEC = 3000;
    public static final String HUB1_AUTOMATION_STR =  "automationid";
    public static final String HUB2_AUTOMATION_STR =  "data-automation-id";
    public static final String AUTOMATION_ID_PATTERN = "//*[@"+HUB1_AUTOMATION_STR+"=\"{0}\"]";
    public static final String HUB2_AUTOMATION_ID_PATTERN = "//*[@"+HUB2_AUTOMATION_STR+"=\"{0}\"]";

    public static final String ERROR_MESSAGE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"Error\"]";
    public static final String ERROR_MESSAGE_CLOSE_BUTTON = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"Error.CloseButton\"]";

    protected static WebDriver driver;
    protected static WebDriverWait driverWait;

    public static final Logger log4j = Logger.getLogger(BaseService.class);

    public static final String resourcesDir = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" + File.separator;

    public static final boolean isUiDebugLogEnable = Boolean.valueOf(PropsReader.getPropValuesForEnv("UidDebugLog")).booleanValue();

    protected static boolean hub2mode = false; // this flag define if we are working on hub2 tab in order to use the correct AUTOMATION_ID_PATTERN

    protected static Field currentField;


    public static boolean isHub2mode() {
        return hub2mode;
    }


    public static void setHub2mode(boolean hub2mode) {
        BaseService.hub2mode = hub2mode;
    }


    public static WebDriver getDriver() {
        return driver;
    }

    public static WebDriverWait getDriverWait() {
        return driverWait;
    }

    public static void setDriver(WebDriver driver1) {
        driver = driver1;
        driverWait = new WebDriverWait(driver, STANDART_PAGE_LOAD_WAIT_TIME_SEC);
    }

    public static WebElement findElementByAutomationId(String id) {

        String xpath = MessageFormat.format(hub2mode ? HUB2_AUTOMATION_ID_PATTERN : AUTOMATION_ID_PATTERN, id);
        return driver.findElement(By.xpath(xpath));
    }

    public static WebElement findElementByAutomationId(WebElement element, String id) {

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String xpath = MessageFormat.format(hub2mode ? HUB2_AUTOMATION_ID_PATTERN : AUTOMATION_ID_PATTERN, id);
        return driver.findElement(By.xpath(xpath));
    }

    public static List<WebElement> findElementsByAutomationId(String id) {
        String xpath = MessageFormat.format(hub2mode ? HUB2_AUTOMATION_ID_PATTERN : AUTOMATION_ID_PATTERN, id);
        return getDriver().findElements(By.xpath(xpath));
    }


    public static WebElement findElementByDiffrentAttribute(String attr, String val) {
        String xpath = MessageFormat.format("//*[@{0}=\"{1}\"]", attr, val);
        return driver.findElement(By.xpath(xpath));
    }

    public static List<WebElement> findElementsByDiffrentAttribute(WebElement element, String attr, String val) {
        String xpath = MessageFormat.format("//*[@{0}=\"{1}\"]", attr, val);
        return element.findElements(By.xpath(xpath));
    }

    protected static void selectImageFromImagesLibery() {
        WebElement imagesWindow = BaseService.findElementByDiffrentAttribute("ngf-drop", "onFileSelect($files)");
        List<WebElement> images = imagesWindow.findElements(By.tagName("li"));
        if (images.size() > 1) {
            images.get(1).click();
            UIDebugLogger.selectImageFromImagesLiberyAddActionToLog(UIDebugLogger.INFO, images.get(1));
        } else {
            // TODO: 09/05/2017  implement add new image if images window empty
        }
    }

    public static  WebElement getElementIfPresent(By by){
        try{
            WebElement element = driver.findElement(by);
            return element;
        }
        catch(NoSuchElementException e){
            return null;
        }
    }

    public static void  fillSeleniumObject(BaseSeleniumObject object) throws AutomationException {
        Field[] fields = object.getClass().getDeclaredFields();
        String symbol = "\u2295";
        try {
            for (Field field : fields) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(SeleniumPath.class)) {
                    SeleniumPath annotation = field.getAnnotation(SeleniumPath.class);
                    currentField = field;
                    switch (annotation.elementInputType()) {
                        case INPUT:
                            if (field.get(object) != null)
                                fillElement(annotation.type(), field, object, annotation);
                            break;
                        case INPUT_FOCUS:
                            if (field.get(object) != null)
                                fillElementForNonFocus(annotation.type(), field, object, annotation);
                            break;
                        case INPUT_KEY_TRIGGER:
                            if (field.get(object) != null)
                                fillElementWithKeyTrigger(annotation.type(), field, object, annotation);
                            break;
                        case TEXT_AREA:
                            if (field.get(object) != null)
                                fillElementForTextArea(annotation.type(), field, object, annotation);
                            break;
                        case INPUT_TEXT:
                            if (field.get(object) != null)
                                fillElementForTextWithInput(annotation.type(), field, object, annotation);
                            break;
                        case FILE:
                            if (field.get(object) != null)
                                fillFileInput(annotation.type(), field, object, annotation);
                            break;
                        case SELECT:
                            if (field.get(object) != null)
                                selectElementfromList(annotation.type(), field, object, annotation);
                            break;
                        case SELECT_THROUGH_XPATH:
                            if (field.get(object) != null)
                                selectElementfromListThroughXPATH(annotation.type(), field, object, annotation);
                            break;
                        case SELECT_THROUGH_LABEL:
                            if (field.get(object) != null)
                                selectLabelElementfromList(annotation.type(), field, object, annotation);
                            break;
                        case SELECT2_THROUGH_LABEL:
                            if (field.get(object) != null)
                                selectElementfromSelect2List(annotation.type(), field, object, annotation);
                            break;
                        case SELECT_WITH_WAIT:
                            if (field.get(object) != null)
                                selectLabelElementfromListWithWait(annotation.type(), field, object, annotation);
                            break;
                        case SELECT_FROM_OPEN_FILED:
                            if (field.get(object) != null)
                                selectElementfromListThroughXPATH(annotation.type(), field, object, annotation);
                            break;
                        case CLICK:
                            if (field.get(object) != null) {
                                clickElement(annotation.type(), field, object, annotation);
                                checkForErrorDialog();
                            }
                            break;
                        case CLICK_WAIT: //click with wait
                            if (field.get(object) != null) {
                                waitClickElement(annotation.type(), field, object, annotation);
                                checkForErrorDialog();
                            }
                            break;
                        case CLICK_FORCE: //click without filling the fields (for images and save button and etc)
                                waitClickElement(annotation.type(), field, object, annotation);
                                checkForErrorDialog();
                            break;
                        case CLICK_SCROLL: //click and scroll up to move to scenarios in the smart gift
                            clickScrollkElement(annotation.type(), field, annotation, object);
                            checkForErrorDialog();
                            break;
                        case CLICK_FORCE_WAIT: //click and scroll up to move to scenarios in the smart gift
                            clickForceWaitElement(annotation.type(), field, annotation, object);
                            waitForLoad();
                            break;
                        case CLICK_FORCE_ALERT:
                            clickForceAlertElement(annotation.type(), field, annotation, object);
                            break;
                        case CLICK_AND_INPUT:
                            if (field.get(object) != null)
                                clickAndInputElement(annotation.type(), field, annotation, object);
                            break;
                        case CHECKBOX:
                            if (field.get(object) != null)
                                clickElement(annotation.type(), field, object, annotation);
                            break;
                        case TAGS:
                            if (field.get(object) != null)
                                fillTags(annotation.type(), field, object, annotation);
                            break;
                        case SCROLL_DOWN:
                            scrollDown();
                            break;
                        case DATE:
                            if (field.get(object) != null)
                                fillDateElement(annotation.type(), field, object, annotation);
                            break;
                        case TIME:
                            if (field.get(object) != null)
                                fillTimeElement(annotation.type(), field, object, annotation);
                            break;
                        case INPUT_AND_ENTER_HUB2:
                        case INPUT_TIME:
                        case INPUT_HUB2:
                        case INPUT_CAPSULA_HUB2:
                        case CLICK_HUB2:
                        case CLICK_HUB2_1PARAM:
                        case SELECT_HUB2:
                        case SELECT_BY_TEXT_HUB2:
                        case MULTI_SELECT_HUB2:
                        case ADVANCED_SELECT_HUB2:
                        case CLICK_INNER_ELEMENT_HUB2:
                        case SCROLL_AND_CLICK:
                        case CHECKBOX_HUB2:
                        case TEXT_AREA_HUB2:
                        case CLICK_ESC:
                        case CLICK_REAPEATED:
                        case CLICK_IF_ENABLED:
                            BaseHub2Service.fillHub2SeleniumObject(object, field, annotation);
                            break;
                        case OBJECT_ARRAY:
                            if (field.get(object) != null) {
                                ArrayList arr = (ArrayList) field.get(object);
                                for (int i = 0; i < arr.size(); i++) {
                                    fillSeleniumObject((BaseSeleniumObject) arr.get(i));
                                }
                            }
                            break;
                        case CHOOSE_IMAGE:
                            waitClickElement(annotation.type(), field, object, annotation);
                            selectImageFromImagesLibery();
                            break;
                        case CLICK_AND_APPROVE:
                            if (field.get(object) != null) {
                                clickElement(annotation.type(), field, object, annotation);
                                checkForAlerts();
                                waitForLoad();
                            }
                            break;
                    }

                } else if (field.get(object) != null) {
                    // Note: Don't remove the first condition in order to keep java Performance better!!!!
                    if (BaseSeleniumObject.class.isAssignableFrom(field.getType()) || BaseSeleniumObject.class.isAssignableFrom(field.get(object).getClass()))
                        fillSeleniumObject((BaseSeleniumObject) field.get(object));
                }
            }
            dismissNoticeMsg();
            waitForLoad();
        } catch (IllegalAccessException e) {
            throw new AutomationException("IllegalAccessException is thrown: ",e);
        } catch (IntrospectionException e) {
            throw new AutomationException("IntrospectionException is thrown: ",e);
        } catch (InterruptedException e) {
            throw new AutomationException("InterruptedException is thrown: ",e);
        }catch (ElementNotVisibleException e){
            throw new AutomationException("ElementNotVisibleException is thrown: ",e);
        } catch (NoSuchElementException e) {
            handleNoSuchElementException(e);
        }
    }

    protected static void handleNoSuchElementException(NoSuchElementException e) throws AutomationException {
        log4j.info(e.getMessage());
        if(e.getClass() != NoSuchElementException.class)
            throw e;

        if( (e.getMessage().indexOf('{') != -1) && (e.getMessage().indexOf('}') != -1) ) {
            String jsonMessage = e.getMessage().substring(e.getMessage().indexOf('{'), e.getMessage().indexOf('}') + 1);
            jsonMessage = jsonMessage.replace("=\"", "='");//we need to replace special character " for json to '
            jsonMessage = jsonMessage.replace("\"]", "']");//we need to replace special character " for json to '
            Map<String, Object> data = JsonUtils.convertJsonStringToMap(jsonMessage);
            String msg = "Failed to set field << " + currentField.getName() + " >>\n with type << " + currentField.getAnnotation(SeleniumPath.class).elementInputType() + " >>\n";
            throw new AutomationException(msg + " << Unable to locate element with: " + data.get("method") + " = " + data.get("selector") + " >>", e);
        }else
            throw e;
    }
    /**
     * For updating scenario name - it is combined click for open the element for editing + edit
     */
    protected static void clickAndInputElement(SeleniumPath.Type type, Field field, SeleniumPath annotation, BaseSeleniumObject object) throws IllegalAccessException, InterruptedException {

        waitForLoad();
        if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {

            field.setAccessible(true);
            String dynamicPath = annotation.value() + "[" + object.getNext() + "]";
            Actions actions = new Actions(driver);
            actions.moveToElement(driver.findElement(By.xpath(annotation.value())));
            actions.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

            //script for updating scenario name
            String script = "var element = document.evaluate('" + dynamicPath + "' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;" +
                    "angular.element(element).scope().title ='" + field.get(object).toString() + "'; ";

            executeJavascript(script);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            actions.sendKeys(Keys.TAB);
            actions.build().perform();
        }
        UIDebugLogger.fillElementAddActionToLog(UIDebugLogger.INFO, field, object);
    }

    protected static void fillFileInput(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IllegalAccessException {

        WebElement element = null;
        if (type == SeleniumPath.Type.BY_ID) {

            field.setAccessible(true);
            element = driver.findElement(By.id(annotation.value()));

        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            element = driver.findElement(By.xpath(annotation.value()));

        }
        if (driver instanceof RemoteWebDriver) {
            LocalFileDetector detector = new LocalFileDetector();
            File localFile = detector.getLocalFile((String) field.get(object));
            if (element instanceof RemoteWebElement)
                ((RemoteWebElement) element).setFileDetector(detector);
            String absolutePath = localFile.getAbsolutePath();
            element.sendKeys(absolutePath);
        } else {
            driver.findElement(By.id(annotation.value())).sendKeys(field.get(object).toString());
        }

    }

    public static void scrollDown() {
        // TODO: 15/05/2017 Jenny: write comment about what this line do...
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[@ng-click=\"SACtrl.addAction()\"][1]")));

    }

    public static void scrollUp() {
        WebElement loyaltyTab = driver.findElement(BaseTabsPage.TAB_LOYALTY_CENTER);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", loyaltyTab);

    }



    public static void fillElement(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException {

        String dynamicPath;
        waitForLoad();
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(annotation.value());
            element.clear();
            element.sendKeys(field.get(object).toString());
        } else if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            driver.findElement(By.id(annotation.value())).clear();
            driver.findElement(By.id(annotation.value())).sendKeys(field.get(object).toString());
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            driver.findElement(By.xpath(annotation.value())).clear();
            driver.findElement(By.xpath(annotation.value())).sendKeys(field.get(object).toString());
        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            TimeUnit.MILLISECONDS.sleep(500);
            dynamicPath= buildDynamicPath(hub2mode,object,annotation);
            driver.findElement(By.xpath(dynamicPath)).clear();
            driver.findElement(By.xpath(dynamicPath)).sendKeys(field.get(object).toString());
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }else if (type == SeleniumPath.Type.BY_DYNAMIC_XPATH) {
            field.setAccessible(true);
            TimeUnit.MILLISECONDS.sleep(500);
            dynamicPath = MessageFormat.format(annotation.value(), object.getNext());
            driver.findElement(By.xpath(dynamicPath)).clear();
            driver.findElement(By.xpath(dynamicPath)).sendKeys(field.get(object).toString());

            UIDebugLogger.fillElementAddActionToLog(UIDebugLogger.INFO, field, object);
        }
    }


    public static void fillElementForTextArea(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException {
        String dynamicPath;

        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(annotation.value());
            element.sendKeys(Keys.TAB);
            element.clear();
            element.sendKeys(field.get(object).toString());
        } else if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.id(annotation.value()));
            element.sendKeys(Keys.TAB);
            element.clear();
            element.sendKeys(field.get(object).toString());
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.xpath(annotation.value()));
            element.sendKeys(Keys.TAB);
            element.clear();
            element.sendKeys(field.get(object).toString());
        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            TimeUnit.MILLISECONDS.sleep(500);
            dynamicPath = buildDynamicPath(hub2mode, object, annotation);
            WebElement element = driver.findElement(By.xpath(dynamicPath));
            element.sendKeys(Keys.TAB);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            element.sendKeys(field.get(object).toString());
            element.sendKeys(Keys.TAB);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }
        UIDebugLogger.fillElementAddActionToLog(UIDebugLogger.INFO, field, object);
    }


    public static void fillElementForTextWithInput(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException {
        String dynamicPath;

        if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.id(annotation.value()));
            element.sendKeys(Keys.TAB);
            element.clear();
            element.sendKeys(field.get(object).toString());
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.xpath(annotation.value()));
            element.sendKeys(Keys.TAB);
            element.clear();
            element.sendKeys(field.get(object).toString());
        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            TimeUnit.MILLISECONDS.sleep(500);
            dynamicPath = buildDynamicPath(hub2mode, object, annotation);
            WebElement element = driver.findElement(By.xpath(dynamicPath));
            element.sendKeys(Keys.TAB);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

            if (getOS().contains(Platform.MAC.name().toLowerCase())) {
//
                element.clear();
                element.sendKeys(field.get(object).toString());

            } else {
                element.sendKeys(Keys.CONTROL + "a");
                element.sendKeys(Keys.DELETE);
                element.sendKeys(field.get(object).toString());
                element.sendKeys(Keys.TAB);

            }
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }
        UIDebugLogger.fillElementAddActionToLog(UIDebugLogger.INFO, field, object);
    }


    public static String getOS() {
        return LoginLogoutService.getOS();
    }

    //here we handle date in a special way - workaround using java script executer to update the date.
    public static void fillDateElement(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException {
        String dynamicPath = "";
        waitForLoad();
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(annotation.value());
            String xpath = "//*[@automationid=\"" + annotation.value() + "\"]";
            Actions actions = new Actions(driver);
            actions.moveToElement(element);
            actions.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            String script;
            //script for date fields of new member
            script = "var element = document.evaluate('" + xpath + "' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;" +
                    "angular.element(element).scope().InputDateCtrl.inlineModel ='" + field.get(object).toString() + "'; ";
            log4j.info("SCRIPT for date: " + script);
            executeJavascript(script);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            actions.sendKeys(Keys.TAB);
            actions.build().perform();

        } else if (type == SeleniumPath.Type.BY_ID) {

            field.setAccessible(true);
            String id = annotation.value();
            Actions actions = new Actions(driver);
            actions.moveToElement(driver.findElement(By.id(annotation.value())));
            actions.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            executeJavascript("document.getElementById('" + id + "').removeAttribute('readonly',0);"); // Enables date box
            String script = "angular.element('#" + id + "').scope().InputDateCtrl.ngModel = '" + field.get(object).toString() + "'; ";
            log4j.info("SCRIPT for date: " + script);
            executeJavascript(script);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            actions.sendKeys(Keys.TAB);
            actions.build().perform();


        } else if (type == SeleniumPath.Type.BY_XPATH) {

            field.setAccessible(true);
            String xpath = annotation.value();
            Actions actions = new Actions(driver);
            actions.moveToElement(driver.findElement(By.xpath(annotation.value())));
            actions.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            String script;
            //script for date fields of new member
            if (xpath.contains("From") || xpath.contains("Until")) {
                //script for date in date&time condition
                script = "var element = document.evaluate('" + xpath + "' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;" +
                        "angular.element(element).scope().InputDatetimeCtrl.inlineModel.date ='" + field.get(object).toString() + "'; ";
            } else if ((xpath.contains("from") || xpath.contains("to") || xpath.contains("datepicker")) && !xpath.contains("automationid")) {
                //script for date in date&time condition
                script = "var element = document.evaluate('" + xpath + "' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;" +
                        "angular.element(element).scope().InputDatetimeCtrl.inlineModel.date ='" + field.get(object).toString() + "'; ";
            } else {
                script = "var element = document.evaluate('" + xpath + "' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;" +
                        "angular.element(element).scope().InputDateCtrl.inlineModel ='" + field.get(object).toString() + "'; ";
            }
            log4j.info("SCRIPT for date: " + script);
            executeJavascript(script);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            actions.sendKeys(Keys.TAB);
            actions.build().perform();

        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {

            field.setAccessible(true);
            dynamicPath = buildDynamicPath(hub2mode,object,annotation);
            Actions actions = new Actions(driver);
            actions.moveToElement(driver.findElement(By.xpath(annotation.value())));
            actions.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            String script;
            if (dynamicPath.contains("fromDateTime") || dynamicPath.contains("toDateTime")) {
                //script for date in date&time condition
                script = "var element = document.evaluate('" + dynamicPath + "' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;" +
                        "angular.element(element).scope().InputDatetimeCtrl.inlineModel.date ='" + field.get(object).toString() + "'; ";
            } else {
                //script for date in membership date fields
                script = "var element = document.evaluate('" + dynamicPath + "' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;" +
                        "angular.element(element).scope().InputDateCtrl.inlineModel ='" + field.get(object).toString() + "'; ";
            }
            log4j.info("SCRIPT for date: " + script);
            executeJavascript(script);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            actions.sendKeys(Keys.TAB);
            actions.build().perform();
        }
        UIDebugLogger.fillDateElementAddActionToLog(UIDebugLogger.INFO, field, object);

    }

    //here we handle date in a special way - workaround using java script executer to update the time.
    public static void fillTimeElement(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException {

        waitForLoad();
        if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            String id = annotation.value();
            Actions actions = new Actions(driver);
            actions.moveToElement(driver.findElement(By.id(annotation.value())));
            actions.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            executeJavascript("document.getElementById('" + id + "').removeAttribute('readonly',0);"); // Enables date box
            String script = "angular.element('#" + id + "').scope().timeInput.inlineModel.time = '" + field.get(object).toString() + "'; ";
            executeJavascript(script);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            actions.sendKeys(Keys.TAB);
            actions.build().perform();


        } else if (type == SeleniumPath.Type.BY_XPATH) {

            field.setAccessible(true);
            String xpath = annotation.value();
            Actions actions = new Actions(driver);
            actions.moveToElement(driver.findElement(By.xpath(annotation.value())));
            actions.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

            String script = "var element = document.evaluate('" + xpath + "' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;" +
                    "angular.element(element).scope().timeInput.inlineModel.time ='" + field.get(object).toString() + "'; ";
            executeJavascript(script);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            actions.sendKeys(Keys.TAB);
            actions.build().perform();

        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {

            field.setAccessible(true);
            String  dynamicPath = buildDynamicPath(hub2mode,object,annotation);
            Actions actions = new Actions(driver);
            actions.moveToElement(driver.findElement(By.xpath(annotation.value())));
            actions.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

            String script = "var element = document.evaluate('" + dynamicPath + "' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;" +
                    "angular.element(element).scope().timeInput.inlineModel.time ='" + field.get(object).toString() + "'; ";
            executeJavascript(script);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            actions.sendKeys(Keys.TAB);
            actions.build().perform();

        }

    }

    public static void fillTags(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException {

        String dynamicPath;
        ArrayList arr = (ArrayList) field.get(object);
        waitForLoad();
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(annotation.value());
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            for (int i = 0; i < arr.size(); i++) {
                element.sendKeys(arr.get(i).toString());
                element.sendKeys(Keys.ENTER);
                UIDebugLogger.fillTagsAddToActionLog(UIDebugLogger.INFO, arr.get(i).toString(), field, object);
            }
        } else if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            driver.findElement(By.id(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            for (int i = 0; i < arr.size(); i++) {
                driver.findElement(By.id(annotation.value())).sendKeys(arr.get(i).toString());
                driver.findElement(By.id(annotation.value())).sendKeys(Keys.ENTER);
                UIDebugLogger.fillTagsAddToActionLog(UIDebugLogger.INFO, arr.get(i).toString(), field, object);
            }
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            driver.findElement(By.xpath(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            for (int i = 0; i < arr.size(); i++) {
                driver.findElement(By.xpath(annotation.value())).sendKeys(arr.get(i).toString());
                driver.findElement(By.xpath(annotation.value())).sendKeys(Keys.ENTER);
                UIDebugLogger.fillTagsAddToActionLog(UIDebugLogger.INFO, arr.get(i).toString(), field, object);
            }
        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            dynamicPath = annotation.value() + "[" + object.getNextTag() + "]";
            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            for (int i = 0; i < arr.size(); i++) {
                driver.findElement(By.xpath(dynamicPath)).sendKeys(arr.get(i).toString());
                driver.findElement(By.xpath(dynamicPath)).sendKeys(Keys.ENTER);
                UIDebugLogger.fillTagsAddToActionLog(UIDebugLogger.INFO, arr.get(i).toString(), field, object);
            }
        }
    }


    public static void clickElement(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException {

        String dynamicPath;
        waitForLoad();
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(annotation.value());
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        } else if (type == SeleniumPath.Type.BY_NAME) {
            field.setAccessible(true);
            driver.findElement(By.name(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        } else if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            driver.findElement(By.id(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            driver.findElement(By.xpath(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }else if (type == SeleniumPath.Type.BY_XPATH2) {
            field.setAccessible(true);
            driver.findElement(By.xpath(field.get(object).toString())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            dynamicPath = buildDynamicPath(isHub2mode(),object,annotation);
            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH_IMAGE_SELECT){
            field.setAccessible(true);
            dynamicPath = buildDynamicPath(isHub2mode(),object,annotation);
            WebElement imageUpload = getDriver().findElement(By.xpath(dynamicPath));
            Actions action = new Actions(BaseService.getDriver());
            action.moveToElement(imageUpload);
            action.click(imageUpload).perform();
        }
        else if (type == SeleniumPath.Type.BY_GENERATE_NEXT) {
            field.setAccessible(true);
            dynamicPath = annotation.value() + "[" + object.getCreateActionCounter() + "]";
            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }
        UIDebugLogger.clickElementAddActionToLog(UIDebugLogger.INFO, field, object);
    }


    public static void dismissNoticeMsg() {
        try {
            while (driver.findElements(By.xpath("//*[@class='alert_close']")).size() > 0) {
                WebElement element = driver.findElement(By.xpath("//*[@class='alert_close']"));
                //element.click();
                JavascriptExecutor executor = (JavascriptExecutor) driver;
                executor.executeScript("arguments[0].click();", element);
                //Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            }
        } catch (Exception e) {
            //do nothing
        }

    }

    public static void scrollToElement(WebElement element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
        actions.perform();
    }

    public static void clickScrollkElement(SeleniumPath.Type type, Field field, SeleniumPath annotation, BaseSeleniumObject object) throws IntrospectionException, IllegalAccessException, InterruptedException {

        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", driver.findElement(By.xpath("//*[@translate=\"save_and_close\"]")));
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(annotation.value());
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_NAME) {
            field.setAccessible(true);
            driver.findElement(By.name(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            driver.findElement(By.id(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            driver.findElement(By.xpath(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            String dynamicPath = annotation.value() + "[" + object.getNext() + "]";
            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }
    }


    public static void selectElementfromList(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException {

        Select select;
        String dynamicPath;
        waitForLoad();
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(annotation.value());
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            select = new Select(element);
            try {
                // try find option by value
                select.selectByValue(field.get(object).toString());
                driver.findElement(By.id(annotation.value())).click();
            } catch (NoSuchElementException e) {
                //if could find by value try find by visible text
                select.selectByVisibleText(field.get(object).toString());
            }
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_NAME) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.name(annotation.value()));
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            select = new Select(element);
            try {
                // try find option by value
                select.selectByValue(field.get(object).toString());
                driver.findElement(By.id(annotation.value())).click();
            } catch (NoSuchElementException e) {
                //if could find by value try find by visible text
                select.selectByVisibleText(field.get(object).toString());
            }
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        } else if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.id(annotation.value()));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            select = new Select(element);
            select.selectByValue(field.get(object).toString());
            driver.findElement(By.id(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            driver.findElement(By.xpath(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            select = new Select(driver.findElement(By.xpath(annotation.value())));
            select.selectByValue(field.get(object).toString());
            driver.findElement(By.xpath(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            TimeUnit.MILLISECONDS.sleep(500);
            dynamicPath = annotation.value() + "[" + object.getNext() + "]";
            WebElement element = driver.findElement(By.xpath(dynamicPath));
            Actions actions = new Actions(driver);
            actions.moveToElement(element);
            actions.perform();
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            select = new Select(element);
            select.selectByIndex(Integer.parseInt(field.get(object).toString()));
            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }
        UIDebugLogger.selectElementfromListAddActionToLog(UIDebugLogger.INFO, field, object);

    }

    public static void selectElementfromSelect2List(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException {


        waitForLoad();
        if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            driver.findElement(By.xpath(field.get(object).toString())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }

    }



    public static void fillElementForNonFocus(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException {
        String dynamicPath="";
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(annotation.value());
            Actions actions = new Actions(driver);
            actions.moveToElement(element);
            actions.doubleClick().build().perform(); //double click to select field text before refill
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            element.clear();
            element.sendKeys(field.get(object).toString());

        } else if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            Actions actions = new Actions(driver);
            actions.moveToElement(driver.findElement(By.id(annotation.value())));
            actions.doubleClick(); //double click to select field text before refill
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            actions.sendKeys(field.get(object).toString());
            actions.build().perform();

        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            Thread.sleep(2000);
            driver.findElement(By.xpath(annotation.value())).clear();
            Thread.sleep(2000);
            driver.findElement(By.xpath(annotation.value())).sendKeys(field.get(object).toString());

        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            dynamicPath = buildDynamicPath(isHub2mode(), object, annotation);
            WebElement element = driver.findElement(By.xpath(dynamicPath));
            element.sendKeys(field.get(object).toString());

        }
        UIDebugLogger.fillElementAddActionToLog(UIDebugLogger.INFO, field, object);
    }

    public static void fillElementWithKeyTrigger(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IllegalAccessException {

        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(annotation.value());
            Actions builder = new Actions(driver);
            Action seriesOfActions = builder.click(element).sendKeys(field.get(object).toString()).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build();
            seriesOfActions.perform();

        } else if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.id(annotation.value()));
            Actions builder = new Actions(driver);
            Action seriesOfActions = builder.click(element).sendKeys(field.get(object).toString()).pause(Duration.ofMillis(500)).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build();
            seriesOfActions.perform();

        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.xpath(annotation.value()));
            Actions builder = new Actions(driver);
            Action seriesOfActions = builder.click(element).sendKeys(field.get(object).toString()).sendKeys(Keys.ARROW_DOWN).sendKeys(Keys.ENTER).build();
            seriesOfActions.perform();
        }
        UIDebugLogger.fillElementAddActionToLog(UIDebugLogger.INFO, field, object);
    }

    public static void selectLabelElementfromList(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException {

        //TimeUnit.SECONDS.sleep(2);
        Select select;
        String dynamicPath;
        waitForLoad();

        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(annotation.value());
            Actions actions = new Actions(driver);
            actions.moveToElement(element);
            actions.perform();
            element.click();
            Thread.sleep(1000);
            actions.build().perform();
            select = new Select(element);
            select.selectByVisibleText(field.get(object).toString());
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        } else if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            select = new Select(driver.findElement(By.id(annotation.value())));
            select.selectByVisibleText(field.get(object).toString());
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.xpath(annotation.value()));
            Actions actions = new Actions(driver);
            actions.moveToElement(element);
            actions.perform();
            element.click();
            Thread.sleep(3000);
            actions.build().perform();
            select = new Select(driver.findElement(By.xpath(annotation.value())));
            select.selectByVisibleText(field.get(object).toString());
            Thread.sleep(5000);
            driver.findElement(By.xpath(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            dynamicPath = annotation.value() + "[" + object.getNext() + "]";
            WebElement element = driver.findElement(By.xpath(dynamicPath));
            Actions actions = new Actions(driver);
            actions.moveToElement(element);
            actions.perform();
            element.click();
            Thread.sleep(2000);
            actions.build().perform();
            select = new Select(driver.findElement(By.xpath(dynamicPath)));
            select.selectByVisibleText(field.get(object).toString());
            Thread.sleep(10000);
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }
        UIDebugLogger.selectElementfromListAddActionToLog(UIDebugLogger.INFO, field,object);
        waitForLoad();
    }


    public static void selectLabelElementfromListWithWait(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException {

        Select select;
        waitForLoad();

        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(annotation.value());
            Actions actions = new Actions(driver);
            actions.moveToElement(element);
            actions.perform();
            element.click();
            Thread.sleep(1000);
            select = new Select(element);
            select.selectByVisibleText(field.get(object).toString());
            waitForLoad();
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.xpath(annotation.value()));
            Actions actions = new Actions(driver);
            actions.moveToElement(element)
                    .perform();
            element.click();
            Thread.sleep(3000);
            select = new Select(driver.findElement(By.xpath(annotation.value())));
            select.selectByVisibleText(field.get(object).toString());
            waitForLoad();
            driver.findElement(By.xpath(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }
        UIDebugLogger.selectElementfromListAddActionToLog(UIDebugLogger.INFO, field,object);
        waitForLoad();
    }


    public static void selectElementfromListThroughXPATH(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException {

        String dynamicPath, dynamicValue;
        waitForLoad();
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(annotation.value());
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            driver.findElement(By.xpath(field.get(object).toString())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            driver.findElement(By.id(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            driver.findElement(By.xpath(field.get(object).toString())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            driver.findElement(By.xpath(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            driver.findElement(By.xpath(field.get(object).toString())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            dynamicPath = annotation.value() + "[" + object.getNext() + "]";
            dynamicValue = field.get(object).toString() + "[" + object.getNext() + "]";
            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            driver.findElement(By.xpath(dynamicValue)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        } else if (type == SeleniumPath.Type.BY_OPERATOR_GENERATE_XPATH) {
            field.setAccessible(true);
            dynamicPath = annotation.value() + "[" + object.getNext() + "]";
            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            driver.findElement(By.xpath(field.get(object).toString())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_XPATH2) {
            field.setAccessible(true);
            driver.findElement(By.xpath(field.get(object).toString())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }
        UIDebugLogger.selectElementfromListAddActionToLog(UIDebugLogger.INFO, field,object);
    }


    public static void waitClickElement(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException {
        List<WebElement> elements;
        String dynamicPath;
        waitForLoad(); // smart needs more time out than gift ...to modify

        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(annotation.value());
            element.click(); //scroll to the element
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_NAME) {
            field.setAccessible(true);
            driver.findElement(By.name(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            driver.findElement(By.id(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.xpath(annotation.value())); //scroll to the element
            Actions actions = new Actions(driver);
            actions.moveToElement(element);
            actions.perform();
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            dynamicPath = buildDynamicPath(isHub2mode(),object,annotation);
            WebElement element = driver.findElement(By.xpath(dynamicPath));
            Actions actions = new Actions(driver);
            actions.moveToElement(element);
            actions.perform();
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH_IMAGE_SELECT) {
            scrollUp();
            field.setAccessible(true);
            dynamicPath = buildDynamicPath(isHub2mode(), object, annotation);
            WebElement element = driver.findElement(By.xpath(dynamicPath));
            Actions actions = new Actions(driver);
            actions.moveToElement(element);
            actions.perform();
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_GENERATE_NEXT) {
            field.setAccessible(true);
            dynamicPath = annotation.value() + "[" + object.getCreateActionCounter() + "]";
            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_DYNAMIC_XPATH) {
            field.setAccessible(true);
            dynamicPath = MessageFormat.format(annotation.value() + "{0}\"" + ")]", object.getNextTagsPath(0));
            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }

        UIDebugLogger.waitClickElementAddActionToLog(UIDebugLogger.INFO, field);
        waitForLoad();
    }

    public static void clickForceWaitElement(SeleniumPath.Type type, Field field, SeleniumPath annotation, BaseSeleniumObject object) throws IntrospectionException, IllegalAccessException, InterruptedException {

        waitForLoad();
        dismissNoticeMsg();
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(annotation.value());
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            driver.findElement(By.id(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            driver.findElement(By.xpath(annotation.value())).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            String dynamicPath = annotation.value() + "[" + object.getNext() + "]";
            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }
        UIDebugLogger.waitClickElementAddActionToLog(UIDebugLogger.INFO, field);
    }

    public static void clickForceAlertElement(SeleniumPath.Type type, Field field, SeleniumPath annotation, BaseSeleniumObject object) throws IntrospectionException, IllegalAccessException, InterruptedException {
        waitForLoad();
        clickForceWaitElement(type, field, annotation, object);
       checkForAlerts();
    }


    public static void readSeleniumObject(BaseSeleniumObject object) throws Exception {


        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(SeleniumPath.class)) {
                SeleniumPath annotation = field.getAnnotation(SeleniumPath.class);
                if (!annotation.readValue().equals("")) {
                    switchInputTypes(object, field, annotation);
                } else if (BaseSeleniumObject.class.isAssignableFrom(field.getType())) {
                    BaseSeleniumObject object1 = (BaseSeleniumObject) Class.forName(field.getType().getCanonicalName()).getConstructor().newInstance();
                    readSeleniumObject(object1);
                    waitForLoad();
                    if (checkIfNullSeleniumObject(object1))
                        field.set(object, object1);
                }

            }//end for fields
        }//end function
    }

    protected static void switchInputTypes(BaseSeleniumObject object, Field field, SeleniumPath annotation) throws Exception {
        switch (annotation.elementInputType()) {
            case INPUT:
            case INPUT_FOCUS:
                readInputElement(annotation.type(), field, object, annotation);
                break;
            case LABEL:
                readLableElement(annotation.type(), field, object, annotation);
                break;
            case SELECT:
                readSelectElement(annotation.type(), field, object, annotation);
                break;
            case TAGS:
                readTagsElement(annotation.type(), field, object, annotation);
                break;
            case DATE:
                //TODO: add date reader here
                break;
            case OBJECT_ARRAY:
                //readObjectArray(annotation.type(), field,object, annotation); break; //TODO after the paths from the HUB will be ready
                break;
            case EXPORT_REPORTS:
                readExportItems(annotation.type(), field, object, annotation);
                break;
            case CHECKBOX:
                readCheckboxElement(annotation.type(), field, object, annotation);
                break;
            case INPUT_AND_ENTER_HUB2:
            case INPUT_TIME:
            case INPUT_HUB2:
            case CLICK_HUB2:
            case CLICK_HUB2_1PARAM:
            case SELECT_HUB2:
            case SELECT_BY_TEXT_HUB2:
            case MULTI_SELECT_HUB2:
            case ADVANCED_SELECT_HUB2:
            case CLICK_INNER_ELEMENT_HUB2:
            case SCROLL_AND_CLICK:
            case CHECKBOX_HUB2:
            case TEXT_AREA_HUB2:
            case CLICK_ESC:
            case CLICK_REAPEATED:
                BaseHub2Service.readHub2SeleniumObject((BaseSeleniumObjectHub2) object, field, annotation);
                break;
        }
    }

    public static void readField(BaseSeleniumObject object, String fieldName) throws Exception {

        Field field = object.getClass().getDeclaredField(fieldName);
        field.setAccessible(true);
        SeleniumPath annotation = field.getAnnotation(SeleniumPath.class);
        if (annotation.readValue().equals(""))
            return;
        switchInputTypes(object, field, annotation);
    }

    public static void readExportItems(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws Exception {
        List<WebElement> elements;
        ArrayList<ExportItemModel> arr = new ArrayList<>();
        String stringMemberCount = "";
        String stringDate = "";
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            Thread.sleep(4000); // help to avoid "element is not attached to the page document" exception
            elements = findElementsByAutomationId(annotation.readValue());
            if (elements.size() > 0) {
                for (WebElement element : elements) {
                    String status = MembersService.waitForExportDone(element);
                    if (status.contentEquals("Download")) {
                        ExportItemModel exportItem = new ExportItemModel();
                        readSeleniumObject(exportItem);
                        arr.add(exportItem);
                    }

                }
            }
            field.set(object, arr);
        }
    }


    //reads input field
    public static void readInputElement(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException {

        List<WebElement> elements;
        if (type == SeleniumPath.Type.BY_ID) {
            elements = driver.findElements(By.id(annotation.readValue()));
            if (elements.size() > 0 && elements.get(0).getAttribute("value") != null)
                field.set(object, elements.get(0).getAttribute("value"));
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            elements = driver.findElements(By.xpath(annotation.readValue()));
            if (elements.size() > 0 && elements.get(0).getAttribute("value") != null)
                field.set(object, elements.get(0).getAttribute("value"));
        } else if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            WebElement element = findElementByAutomationId(annotation.readValue());
            if (element.getAttribute("value") != null)
                field.set(object, element.getAttribute("value"));
            else if (element.getText() != null)
                field.set(object, element.getText());
        }

    }

    public static void readLableElement(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException {

        List<WebElement> elements;
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            WebElement element = findElementByAutomationId(annotation.readValue());
            if (element.getText() != null) {
                field.set(object, element.getText());
            }
        }

    }

    public static void readSelectElement(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException, InstantiationException, ReflectionException, InvocationTargetException, NoSuchFieldException {

        Select select;
        List<WebElement> elements;

        if (type == SeleniumPath.Type.BY_ID) {
            elements = driver.findElements(By.id(annotation.readValue()));
            if (elements.size() > 0 && elements.get(0).getAttribute("value") != null) {
                select = new Select(elements.get(0));
                field.set(object, Utils.getEnumFromString((Class<Enum>) field.getType(), select.getFirstSelectedOption().getAttribute("value")));
            }
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            elements = driver.findElements(By.xpath(annotation.readValue()));
            if (elements.size() > 0 && elements.get(0).getAttribute("value") != null) {
                select = new Select(elements.get(0));

                field.set(object, Utils.getEnumFromString((Class<Enum>) field.getType(), select.getFirstSelectedOption().getAttribute("value")));

            }
        }
    }

    public static void readTagsElement(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException {

        ArrayList arr = new ArrayList();
        List<WebElement> tag;
        int i = 1;

        if (type == SeleniumPath.Type.BY_ID) {
            tag = driver.findElements(By.id(object.getNextTagsPath(i)));
            while (tag.size() > 0) {
                arr.add(tag.get(0).getText());
                i++;
                tag = driver.findElements(By.id(object.getNextTagsPath(i)));
            }
            field.set(object, arr);
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            tag = driver.findElements(By.xpath(object.getNextTagsPath(i)));
            while (tag.size() > 0) {
                String s = new String(tag.get(0).getText());
                s = s.replace("-", " ");
                arr.add(s);
                i++;
                tag = driver.findElements(By.xpath(object.getNextTagsPath(i)));
            }
            field.set(object, arr);
        }
    }


    public static void readClickElement(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException {
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            WebElement element = findElementByAutomationId(annotation.readValue());
            field.set(object, element.isEnabled());
        }
        //TODO: fix this
        if (type == SeleniumPath.Type.BY_ID) {
            field.set(object, driver.findElement(By.id(annotation.readValue())).getText());
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            driver.findElement(By.xpath(annotation.readValue())).getText();
            field.set(object, driver.findElement(By.xpath(annotation.readValue())).getText());

        }
    }

    public static void readCheckboxElement(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IllegalAccessException {
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            WebElement element = findElementByAutomationId(annotation.readValue());
            field.set(object, element.isSelected());
        }
    }


    public static boolean checkIfNullSeleniumObject(BaseSeleniumObject object) throws IntrospectionException, IllegalAccessException, InterruptedException {

        boolean ans = false;
        Field[] fields = object.getClass().getDeclaredFields();

        for (Field field : fields) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(SeleniumPath.class)) {
                if (field.get(object) != null)
                    ans = true;
            } else if (field.get(object) != null)
                if (BaseSeleniumObject.class.isAssignableFrom(field.getType()))
                    checkIfNullSeleniumObject((BaseSeleniumObject) field.get(object));
        }
        return ans;
    }

    public static Object executeJavascript(String script) throws InterruptedException {

        JavascriptExecutor je = (JavascriptExecutor) driver;

        TimeUnit.SECONDS.sleep(2);

        Object retVal = je.executeScript(script);
        return retVal;
    }

    public static Object executeJavascript(String script, WebElement elemToExecute) throws InterruptedException {

        JavascriptExecutor je = (JavascriptExecutor) driver;

        TimeUnit.SECONDS.sleep(2);

        Object retVal = je.executeScript(script, elemToExecute);
        return retVal;
    }




    public static String checkForAlerts() {

        try {
            getDriverWait().withTimeout(5, TimeUnit.SECONDS);
            getDriverWait().until(ExpectedConditions.alertIsPresent());
            if (driver.switchTo().alert() != null) {
                Alert alert = driver.switchTo().alert();
                String msg = alert.getText();
                alert.accept();
                getDriverWait().withTimeout(STANDART_PAGE_LOAD_WAIT_TIME_SEC, TimeUnit.SECONDS);
                return msg;
            }
        } catch (Exception e) {
            //do nothing
        }
        getDriverWait().withTimeout(STANDART_PAGE_LOAD_WAIT_TIME_SEC, TimeUnit.SECONDS);
        return "";
    }

    private static void checkForErrorDialog(){

        List<WebElement> notes = getDriver().findElements(By.xpath(ERROR_MESSAGE));
        if (notes.size() > 0){
            getDriver().findElement(By.xpath(ERROR_MESSAGE_CLOSE_BUTTON)).click();
            log4j.info("Error dialog is shown");
        }
    }

    public static void waitForLoad() {

        waitForLoad(By.id("loading-bar"));
    }

    public static void waitForLoad(By by) {

        boolean loading = true;
        long timeoutCounter = 0;

        while (loading && (timeoutCounter <= TIMEOUT_FOR_PAGE_LOAD_WAIT_SEC)) {
            try {
                TimeUnit.SECONDS.sleep(1);
                getDriver().findElement(by);
                timeoutCounter++;
            } catch (NoSuchElementException e) {
                getDriverWait().until((ExpectedCondition) wd ->
                        ((JavascriptExecutor) getDriver()).executeScript("return document.readyState").equals("complete"));
                return;
            } catch (InterruptedException e) {
                e.printStackTrace();
                timeoutCounter++;
            }
        }

    }

    public static String buildDynamicPath(boolean hub2Mode,BaseSeleniumObject object, SeleniumPath annotation) {
        return buildDynamicPath(hub2Mode,object,annotation.value());
    }

    public static String buildDynamicPath(boolean hub2Mode,BaseSeleniumObject object, String xpath) {
        String dynamicPath="";
        if(hub2Mode)
            dynamicPath = MessageFormat.format(xpath,object.getNext());
        else
            dynamicPath = xpath+"["+object.getNext()+"]";
        return dynamicPath;
    }

    public static void waiForPageToBeReady()
    {
        WebDriverWait wait = new WebDriverWait(driver,TIMEOUT_FOR_PAGE_LOAD_WAIT_SEC);
        wait.until((ExpectedCondition<Boolean>) arg0 -> {
            String state = (String) ((JavascriptExecutor)arg0).executeScript("return document.readyState;");
            return state.equalsIgnoreCase("complete");
        });
    }
    public static By getByAutomationId(String id) {
        String xpath = "//*[@automationId=\'" + id + "\']";
        return By.xpath(xpath);
    }



}



