package hub.base;

public abstract class BaseSeleniumObjectHub2 extends  BaseSeleniumObject{

    public String objectPrefix = "";
    public String idSubString = "";

    public boolean isInCase = false;

    public void setParentIndex (int i){}
    public abstract int getParentIndex ();

    public String getPrefix (){return this.objectPrefix;}

    public void setPrefix (String prefix){this.objectPrefix = prefix;}

    public boolean isInCase(){
        return isInCase;
    };
    public void setIsInCase(boolean inCase){
        isInCase = inCase;
    }

    public void setIdSubString(String idSubString) { this.idSubString = idSubString; }
    public String getIdSubString() { return idSubString;}
}
