package hub.base;

/**
 * Created by Jenny on 11/8/2016.
 */
public abstract class BaseSeleniumObject {

   public abstract String getNextTagsPath(int i);
   public abstract   void setNext (int i);
   public abstract int getNext ();
   public abstract int getNextTag ();
   public abstract int getCreateActionCounter();

   public String readField(String field,String fieldName) {

      if (field==null){
         try {
            BaseService.readField(this,fieldName);
         } catch (Exception e) {
            return "NOT_FOUND";
         }
      }
      return field;
   }


}
