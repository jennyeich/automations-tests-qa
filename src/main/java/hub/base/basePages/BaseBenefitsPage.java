package hub.base.basePages;

import org.openqa.selenium.By;

public class BaseBenefitsPage {

    //Gifts cc
    public static final By GiftMainTitle = By.xpath("//*[@id=\"main\"]/div/div[1]/h2");
    public static final By GiftCardsMainTitle = By.xpath("//*[@id=\"main\"]/div/div[1]/h2");
    public static final By Gifts_main_menu = By.xpath("//*[@id=\"sidebar\"]/ul/ul/li[1]/div/span");
    public static final By Add_Smart_gift = By.xpath("//span[contains(text(),\"Add Gift\")]");
    public static final By Add_Gift_card = By.xpath("//span[contains(text(),\"Add Gift Card\")]");
    public static final By Smart_Gifts = By.id("nav-item-smart_gifts");
    public static final By gift_cards = By.id("nav-item-gift cards");



    //PointShopPage
    public static final By PointShop_main_menu = By.xpath("//*[@automationid=\"gifts_shop_items\"]");

    //loterry rewards page
    public static final By LotteryRewards_main = By.id("nav-item-randomRewards");
    public static final By Add_Lottery_Reward = By.xpath("//*[contains(text(),\"Add Lottery Reward\")]");


    //codes Page
    public static final By Codes = By.xpath("//*[@automationid=\"nav.codes\"]");
    public static final By POSRedeemCodes = By.id("nav-item-sku_codes");
    public static final By AddPOSRedeemCodes = By.xpath("//span[contains(text(), \"Add POS Redeem Codes\")]");

    //POS Redeem codes Page
    public static final By AddPOSRedeemCodes_Name = By.id("couponName");
    public static final By AddPOSRedeemCodes_RewardSKU = By.id("code_id");
    public static final By AddPOSRedeemCodes_SaveButton = By.xpath("//button[@ng-click = 'save();']");
    public static final By AddPOSRedeemCodes_CreateBulkArrow = By.xpath("//i[@ng-click=\"toggle()\"]");
    public static final By AddPOSRedeemCodes_BulkName = By.id("name");
    public static final By AddPOSRedeemCodes_BulkTag = By.id("tag");
    public static final By AddPOSRedeemCodes_BulkDescription = By.id("description");
    public static final By AddPOSRedeemCodes_NumberOfCodes = By.id("units");
    public static final By AddPOSRedeemCodes_CodeLength = By.id("codes_length");
    public static final By AddPOSRedeemCodes_GenerateNewCodes = By.xpath("//button[@translate='sku_codes.generate_codes']");

    //Punch codes
    public static final By SideMenuPunchCards = By.xpath("//span[@automationid=\"punch_cards\"]");
    public static final By Add_punch_card = By.xpath("//span[contains(text(),\"Add Punch Card\")]");
    public static final By SideMenu_SmartPunchCards = By.xpath("//a[@automationid=\"smartPunchCards\"]");

    //Smart Club Deals
    public static final By ClubDealsPage = By.xpath("//span[contains(text(),\"Club Deals\")]");
    public static final By SmartDealPage = By.xpath("//*[@id=\"nav-item-smart_deals\"]");
    public static final By Add_Club_Deal = By.xpath("//span[contains(text(),\"Add Club Deal\")]");

    //old club deal
    public static final By OldClubDealPage = By.xpath("//*[@id=\"nav-item-assetDeals\"]");
    public static final By Add_Old_Club_Deal = By.xpath("//span[contains(text(),\"Add Club Deal\")]");

    //3d party codes

    public static final By THIRD_PARTY_Codes = By.xpath("//*[@automationid=\"third_party\"]");

    public static final By COUPON_Codes = By.xpath("//*[@automationid=\"coupon_codes\"]");

}
