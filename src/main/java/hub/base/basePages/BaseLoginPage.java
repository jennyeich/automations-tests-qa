package hub.base.basePages;

import org.openqa.selenium.By;

/**
 * Created by Jenny on 9/20/2016.
 */
public class BaseLoginPage {

    // fields and elements
    public static final By EMAIL = By.xpath("//*[@name=\"email\"]");
    public static final By PASSWORD = By.xpath("//*[@name=\"password\"]");
    public static final By TERMS = By.id("loginTerms-checkbox");
    public static final By LOGIN = By.id("loginButton");

}
