package hub.base.basePages;

import org.openqa.selenium.By;

/**
 * Created by Goni on 3/28/2017.
 */
public class BaseContentPage {

    //sub menus
    public static final By UpdateApp = By.xpath("//span[contains(text(),'Update App')]");
    public static final By PublishApp = By.id("nav-item-publish_app");

    public static final By Layout = By.xpath("//span[@automationid=\"nav.app_layout\"]");
    public static final By Layout_HomeScreen = By.xpath("//a[@automationid=\"tcalts_title_MainLocationTC\"]");
    public static final By Catalogs = By.id("nav-item-nav.catalogs");

    public static final By Welcome_messages = By.xpath("//*[@automationid=\"welcome_screens\"]");
    public static final By Splash_screen_ads = By.xpath("//*[@automationid=\"commercialBanners\"][1]");
    public static final By Photo_gallery = By.xpath("//*[@automationid=\"gallery\"]");
    public static final By Badges = By.xpath("//*[@automationid=\"badges\"]");
    public static final By Custom_screen = By.xpath("//*[@automationid=\"generic_items\"]");
    public static final By Web_views = By.xpath("//*[@automationid=\"web_views\"]");
    public static final By Files = By.xpath("//*[@automationid=\"files\"]");


    //general forms
    public static final By Forms = By.xpath("//*[@automationid=\"forms\"]");
    public static final By GeneralForms =By.xpath("//*[@automationid=\"forms.generalForm\"]");
    public static final By FormDescription = By.xpath("//*[@data-ng-bind=\"ctrlGeneralForm.info.form.description\"]");
    public static final By FormSubmitButton = By.xpath("//button[contains(text(), \"Submit\")]");
    //survey
    public static final By Survey = By.xpath("//*[@automationid=\"forms.basicQuiz\"]");
    public static final By SurveyTitle = By.xpath("//*[@ng-bind=\"info.form.title\"]");
    public static final By SurveyStartButton = By.xpath("//button[contains(text(), \"Start the survey\")]");
    public static final By NextQuestionButton = By.xpath("//button[contains(text(), \"Next Question\")]");

    //information group
    public static final By Information_menu = By.xpath("//span[contains(text(),\"Information\")]");
    public static final By Information_Locations = By.xpath("//*[@automationid=\"branchesGroups\"]");

    public static final By Branding_menu = By.xpath("//*[@automationid=\"nav.branding\"]");
    public static final By Branding_Design = By.xpath("//*[@automationid=\"app_design\"]");

    //Email templates manager
    public static final By EmailTemplatesManager = By.xpath("//*[@automationid=\"email_templates\"]");
    public static final By EmailTemplatesTitle = By.xpath("//h2[contains(text(), \"Email Templates\")]");
    public static final By Add_Template = By.xpath("//a[contains(text(),\"Add New\")]");
}
