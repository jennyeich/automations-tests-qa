package hub.base.basePages;

import org.openqa.selenium.By;


public class BaseDataAndBIPage {

    //Find Members
    public static final By Find_MembersPage = By.id("nav-item-find_users");
    public static final By Filter_MembersPage = By.id("nav-item-filter_users");

    public static final By Membership_Status_Registered = By.id("MembershipStatus_registered");
    public static final By Membership_Status_Non_Registered = By.id("MembershipStatus_not_registered");
    public static final By First_Name = By.id("FirstName");
    public static final By Last_Name = By.id("LastName");
    public static final By Find_MemberBy_Birthday = By.xpath("//*[@id=\"main\"]/div/form/div/div[3]/div[2]/div/div/div/div/input");
    public static final By Phone_Number = By.id("PhoneNumber");
    public static final By Find_MemberBy_User_Key = By.xpath("//*[@ng-value=\"userinfo.UserKey\"]");
    public static final By Find_MemberBy_Membership_Key = By.xpath("//*[@ng-value=\"userinfo.Key\"]");
    public static final By REFRESH = By.xpath("//*[@automationid=\"refreshMember\"]");

    public  static final By FILTER_ERROR = By.xpath("//*[@automationid=\"alertMessage\"]");


    //FindMember fields
    public static final By Member_points = By.xpath("//form//div[contains(.,'Points')]/div[2]");
    public static final String Member_tags_xpath = "//div/ol/li[contains(text(),\"{0}\")]";
    public static final By Member_tags = By.xpath(Member_tags_xpath);
    public static final By Member_gifts = By.xpath("//*[@automationid=\"userAsset\"]/div[2]");
    public static final By Member_credit = By.xpath("//form/div[contains(.,'Credit')]/div[2]");
    public static final By Member_last_recieved_punchcard_punches_description = By.xpath("(//*[@automationid=\"userAsset\"]/div[contains(text(),\"punched\")])[1]");

    public static final By Member_status = By.xpath("//input[@ng-value=\"userinfo.MembershipStatus|translate\"]");
    //Action monitor
    public static final String ACTION_MONITOR = "actionsMonitorNav";


    public static final By Import_User_Keys_Menu = By.xpath("//a[@automationid=\"customUsers\"]");
}
