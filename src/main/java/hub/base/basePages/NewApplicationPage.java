package hub.base.basePages;

import org.openqa.selenium.By;

/**
 * Created by Goni on 3/23/2017.
 */
public class NewApplicationPage {

    // fields and elements
    public static final By APP_NAME = By.xpath("//input[@automationid=\"createAppAppName\"]");
    public static final By EMAIL = By.xpath("//input[@automationid=\"createAppEmail\"]");
    public static final By PHONE = By.xpath("//input[@automationid=\"createAppPhone\"]");
    public static final By PHONE_PREFIX = By.xpath("//*[@id=\"short_name\"]/select");
    public static final By SERVER = By.xpath("//select[@automationid=\"createAppServer\"]");
    public static final By START = By.id("startButton");


}
