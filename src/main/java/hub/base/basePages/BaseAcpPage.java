package hub.base.basePages;

import org.openqa.selenium.By;

/**
 * Created by lior on 7/4/17.
 */
public class BaseAcpPage {

    //Manage como hub user
    public static final By Manage_Como_Hub_Users = By.xpath("//span[@automationid='usersManagement']");
    public static final By Presets = By.xpath("//span[@automationid=\"presets\"]");
    public static final By Create_New_User = By.xpath("//a[@automationid='usersManagementCreate']");
    public static final By Update_User = By.xpath("//a[@automationid='usersManagementSearch']");
    public static final By Update_User_Me = By.xpath("//button[@automationid=\"current_user\"]");

    public static final By Import_Users_Improved = By.xpath("//a[@automationid=\"users_import_csv\"]");

    public static final By Presets_Filter_members = By.xpath("//a[@id=\"nav-item-filterUsersRulesPresets\"]");

    public static final By User_Plans = By.id("nav-item-plans");
    public static final By Business_Plans = By.id("nav-item-businessPlans");

}
