package hub.hub2_0.basePages.loyalty;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.BaseService;
import hub.base.SeleniumPath;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Box extends BaseSeleniumObjectHub2 {

    private static final Logger log4j = Logger.getLogger(Box.class);
    private static final String ADD_BOX = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"business-center.add-campaign\"]";
    public static final String BOX_NAME = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\"business-center.campaign.{0}.title\"]/input" ;
    private static final String ADD_ACTIVITY = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\"business-center.campaign.{0}.add-activity\"]" ;
    private static final String RULE_ACTIVITY =  "//*[@"+ BaseService.HUB2_AUTOMATION_STR +"=\"business-center.campaign.{0}.add-activity.rule\"]" ;
    private static final String DEAL_ACTIVITY =  "//*[@"+ BaseService.HUB2_AUTOMATION_STR +"=\"business-center.campaign.{0}.add-activity.deal\"]" ;

    @SeleniumPath(value = ADD_BOX, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addBox;
    @SeleniumPath(value = BOX_NAME, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = BOX_NAME)
    private String boxName;
    @SeleniumPath(value = ADD_ACTIVITY, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addActivity;
    @SeleniumPath(value = RULE_ACTIVITY, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String ruleActivity;
    @SeleniumPath(value = DEAL_ACTIVITY, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String dealActivity;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    List<Activity> activities;

    private int currentBoxIdx =0; // hold the current box to update

    public void setActivities(ArrayList<Activity> activities){
        this.activities = activities;
    }

    public List<Activity> getActivities() {
        if(activities == null)
            activities = new ArrayList<>();
        return activities;
    }

    public Activity getActivity(int index){
        return getActivities().get(index);
    }

    public void addActivity(Activity activity){
        getActivities().add(activity);
        //TODO: add support in case there is exists activity which already have rules/deals)/
        log4j.info("Activity added. current activities in the box:" +  getActivities().size());
    }

    public void clickRuleActivity() {
        this.ruleActivity = "Rule Activity";
        this.dealActivity = null;
    }

    public void clickDealActivity() {
        this.dealActivity = "Deal Activity";
        this.ruleActivity = null;
    }

    public String getBoxName() {
        return boxName;
    }

    public void setBoxName(String boxName) {
        this.addBox = "Adding new box with name " + boxName;
        this.boxName = boxName;
    }

    public void clickAddActivity() {
        this.addActivity = "Add activity Rule";
    }



    @Override
    public int getNext() { return this.currentBoxIdx; }
    @Override
    public void setNext(int idx) {this.currentBoxIdx =idx;}
    @Override
    public int getCreateActionCounter() { return 0; }      // unused in HUB2
    public String getNextTagsPath (int i){
        return  "";
    }  //unused in HUB2
    public int getNextTag () {
        return 0;
    }                 // unused in HUB2

    @Override
    public int getParentIndex() {
        return 0;
    }
}
