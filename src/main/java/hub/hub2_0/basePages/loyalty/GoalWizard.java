package hub.hub2_0.basePages.loyalty;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

import java.util.List;

public class GoalWizard extends BaseSeleniumObjectHub2 {

    private static final String OPEN_WIZARD = "business-center.goal-wizard.button";
    private static final String CLOSE_WIZARD = "link.close";
    private static final String LINK_TO_GOAL ="//*[@" + Hub2Constants.HUB2_AUTOMATION_STR  + "=\"goals.link.to.goal\"]//p[contains(text(),'%s')]";


    @SeleniumPath(value = OPEN_WIZARD, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String openGoalWizard;
    @SeleniumPath(value = LINK_TO_GOAL, type = SeleniumPath.Type.BY_DYNAMIC_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String linkToGoal;

    private Goal goal;

    @SeleniumPath(value = CLOSE_WIZARD, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String closeWizard;


    public String getLinkToGoal() {
        return this.linkToGoal;
    }

    public void clickOnGoal(String goalName,String  campaignName) {
        this.linkToGoal = String.format(LINK_TO_GOAL,goalName);
        this.goal = new Goal();
        goal.setSelectedCampaign(campaignName);
    }

    public void clickOnGoal(String goalName,int goalIndex) {
        this.linkToGoal = String.format(LINK_TO_GOAL,goalName);
        this.goal = new Goal();
        goal.setSelectedCampaign(goalIndex);
    }
    public void closeWizard() {
        this.closeWizard = "click on close wizard";
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
