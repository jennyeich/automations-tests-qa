package hub.hub2_0.basePages.loyalty;

import hub.base.BaseSeleniumObject;
import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.conditions.global.DateTimeGlobalCondition;
import hub.hub2_0.common.conditions.global.MembershipGlobalCondition;

public abstract class GlobalCondition extends BaseSeleniumObjectHub2 implements ApplyCondition{

    public abstract void clickGlobalCondition();
    public abstract void clickCancel();
    public abstract void clickSave();

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

}
