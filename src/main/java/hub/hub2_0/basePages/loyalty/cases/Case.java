package hub.hub2_0.basePages.loyalty.cases;

import hub.base.BaseSeleniumObjectHub2;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.conditions.cases.CaseCondition;

import java.text.MessageFormat;
import java.util.List;

public abstract class Case extends BaseSeleniumObjectHub2 {

    protected static final String ADD_CASE_BUTTON = "activity.cases.add-case";
    protected static final String DESCRIPTION = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}description\"]/input";
    protected static final String SIMPLE_MODE = "{0}actions.button.simpleMode";
    public static final String PER_OCCURRENCE_MODE = "{0}actions.button.bundleMode";
    public static final String ORDER_UP = "{0}order.up";
    protected static final String ORDER_DOWN = "{0}order.down";
    protected static final String DELETE_CASE = "{0}delete-case.delete-case";
    protected static final String APPROVE_DALETE = "{0}delete-case.delete-case.delete";


    protected int conditionsCounter = 0;
    protected int actionsCounter = 0;
    protected int occurrencesConditionsCounter = 0;
    protected int caseIndex = 0;

    public Case() {
        setIsInCase(true);
        this.objectPrefix = MessageFormat.format("activity.cases.{0}.", caseIndex);
    }

    public abstract void setConditions(List<CaseCondition> caseConditions);

    public abstract void setActions(List<ActivityAction> activityActions);

    public abstract List<ActivityAction> getActions();

    public abstract ActivityAction replaceAction(ActivityAction currentAction, ActivityAction newAction);

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }


    @Override
    public int getNext() {
        return caseIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public int getParentIndex() {
        return caseIndex;
    }
}
