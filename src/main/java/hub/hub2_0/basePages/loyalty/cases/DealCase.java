package hub.hub2_0.basePages.loyalty.cases;

import hub.base.SeleniumPath;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountAction;
import hub.hub2_0.common.conditions.cases.CaseCondition;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class DealCase extends Case {

    private static final String ENTIRE_TICKET_DISCOUNT = "{0}actions.button.simpleEntireTicketDiscount";
    private static final String SPECIFIC_ITEMS_DISCOUNT = "{0}actions.button.simpleSpecificItemsDiscount";
    private static final String ADVANCED_DISCOUNT = "{0}actions.button.advancedDiscount";

    @SeleniumPath(value = ADD_CASE_BUTTON, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addCaseBtn;
    @SeleniumPath(value = DESCRIPTION, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = DESCRIPTION)
    private String description;
    @SeleniumPath(value = "", type = SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<CaseCondition> conditions;
    @SeleniumPath(value = ENTIRE_TICKET_DISCOUNT, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String entireTicketDiscount;
    @SeleniumPath(value = SPECIFIC_ITEMS_DISCOUNT, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String specificItemsDiscount;
    @SeleniumPath(value = ADVANCED_DISCOUNT, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String advancedDiscount;

    @SeleniumPath(value = "", type = SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<OccurrencesCondition> occurrencesConditions; //array of automation actions action
    @SeleniumPath(value = "", type = SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<ActivityAction> actions; //array of automation actions action
    @SeleniumPath(value = ORDER_UP, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.SCROLL_AND_CLICK)
    private String orderUp;
    @SeleniumPath(value = ORDER_DOWN, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.SCROLL_AND_CLICK)
    private String orderDown;
    @SeleniumPath(value = DELETE_CASE, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String deleteCase;
    @SeleniumPath(value = APPROVE_DALETE, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String approveDelete;

    private enum DiscountType {
        ENTIRE_TICKET("OnEntireTicket"),
        SPECIFIC_ITEMS("OnSpecificItems"),
        SPECIFIC_ITEMS_ADVANCED("OnSpecificItemsAdvanced"),
        ADVANCED("Advanced"),
        EMPTY_SUFFIX("");

        private final String discountType;

        DiscountType(final String discountType) {
            this.discountType = discountType;
        }

        @Override
        public String toString() {
            return discountType;
        }
    }

    private DiscountType discountType = DiscountType.ENTIRE_TICKET;

    public DealCase() {
        super();
    }

    public void clickAddCase() {
        this.addCaseBtn = "add case";
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public void deleteCase() {
        this.deleteCase = "delete";
        this.approveDelete = "approve delete";
    }

    /***************************************************/
    public void setConditions(List<CaseCondition> caseConditions) {
        for (CaseCondition condition : caseConditions) {
            if (conditionsCounter > 0)
                condition.clickAdd();
            addCondition(condition);
        }
    }

    public List<CaseCondition> getConditions() {
        if (conditions == null)
            conditions = new ArrayList<>();
        return conditions;
    }


    public void addCondition(CaseCondition condition) {
        condition.setIsInCase(true);
        condition.setNext(conditionsCounter);
        getConditions().add(condition);
        conditionsCounter++;
    }

    public void clickEntireTicketDiscount() {
        this.entireTicketDiscount = "Clicked 'Give a discount on entire ticket'.";
        this.discountType = DiscountType.ENTIRE_TICKET;
    }

    public void clickSpecificItemsDiscount() {
        this.specificItemsDiscount = "Clicked 'Give discount on specific items or make them free'";
        this.discountType = DiscountType.SPECIFIC_ITEMS;

    }

    public void clickAdvancedDiscount() {
        this.advancedDiscount = "Clicked 'Advanced deal: 1+1, buy X get Y, free item for each $100, and more'";
        this.discountType = DiscountType.SPECIFIC_ITEMS_ADVANCED;
    }


    public void setOccurrencesConditions(List<OccurrencesCondition> occurrencesConditions) {
        clickAdvancedDiscount();
        for (OccurrencesCondition condition : occurrencesConditions) {
            addOccurrencesCondition(condition);
        }
    }

    public void addOccurrencesCondition(OccurrencesCondition occurrencesCondition) {
        occurrencesCondition.setIsInCase(true);
        occurrencesCondition.setNext(occurrencesConditionsCounter);
        getOccurrencesConditions().add(occurrencesCondition);
        occurrencesConditionsCounter++;
    }

    public List<OccurrencesCondition> getOccurrencesConditions() {
        if (occurrencesConditions == null)
            occurrencesConditions = new ArrayList<>();
        return occurrencesConditions;
    }

    public void setActions(List<ActivityAction> activityActions) {
        for (ActivityAction action : activityActions) {
            addAction(action);
        }
    }

    public ActivityAction replaceAction(ActivityAction currentAction, ActivityAction newAction) {
        newAction.setNext(currentAction.getNext());
        getActions().add(currentAction.getNext(), newAction);
        return newAction;
    }

    public void addAction(ActivityAction action) {
        action.setIsInCase(true);
        action.setNext(actionsCounter);
        action = completeActionID(action);
        getActions().add(action);
        actionsCounter++;
    }


    public void removeAction(ActivityAction action) {
        getActions().remove(action);
        actionsCounter--;
    }


    public ActivityAction getAction(int actionIndex) {
        return getActions().get(actionIndex);
    }

    public List<ActivityAction> getActions() {
        if (actions == null)
            actions = new ArrayList<>();
        return actions;
    }

    public void clickOrderUp() {
        this.orderUp = "click order up";
    }

    public void clickOrderDown() {
        this.orderDown = "click order down";
    }

    private ActivityAction completeActionID(ActivityAction action) {
        switch (this.discountType) {
            case ENTIRE_TICKET:
                action.setIdSubString(DiscountType.ENTIRE_TICKET.toString());
                break;
            case SPECIFIC_ITEMS:
                if (action instanceof PercentOffDiscountAction || action instanceof AmountOffDiscountAction) {
                    action.setIdSubString(DiscountType.SPECIFIC_ITEMS.toString());
                } else {
                    action.setIdSubString(DiscountType.EMPTY_SUFFIX.toString());
                }
                break;
            case SPECIFIC_ITEMS_ADVANCED:
                if (action instanceof PercentOffDiscountAction || action instanceof AmountOffDiscountAction) {
                    action.setIdSubString(DiscountType.SPECIFIC_ITEMS_ADVANCED.toString());
                } else {
                    action.setIdSubString(DiscountType.ADVANCED.toString());
                }
                break;
        }
        return action;

    }

    @Override
    public void setNext(int i) {
        this.caseIndex = i;
        this.objectPrefix = MessageFormat.format("activity.cases.{0}.", caseIndex);
        if (actions != null) {
            for (ActivityAction action : actions) {
                action.setPrefix(this.objectPrefix);
            }
        }
        if (occurrencesConditions != null) {
            for (OccurrencesCondition occurrencesCond : occurrencesConditions) {
                occurrencesCond.setPrefix(this.objectPrefix);
            }
        }
        if (conditions != null) {
            for (CaseCondition condition : conditions) {
                condition.setPrefix(this.objectPrefix);
            }
        }
    }

}
