package hub.hub2_0.basePages.loyalty;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.items.SelectItems;

public class OccurrencesCondition extends BaseSeleniumObjectHub2 {

    private static final String SELECT_ITEMS_BUTTON = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.bundle.{1}.itemsPopulation.trigger\"]";
    private static final String CONDITION_KEY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.bundle.{1}.itemsPopulation.conditionKey.{2}\"]";
    private static final String CONDITION_VALUE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.bundle.{1}.itemsPopulation.conditionValue\"]/input";
    private static final String ADD_OCCURRENCES_CONDITION = "activity.actions.itemsPopulation.addLine"; // Deals only

    @SeleniumPath(value = ADD_OCCURRENCES_CONDITION, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addOccurrencesCondition;
    @SeleniumPath(value = SELECT_ITEMS_BUTTON, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String selectItemsButton;
    private SelectItems selectItemsDialog;
    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = CONDITION_KEY)
    private String conditionKey;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = CONDITION_VALUE)
    private String conditionValue;


    private int conditionIndex = 0;

    public OccurrencesCondition() {
        if (!isInCase())
            this.objectPrefix = ActivityAction.ACTIVITY_PREFIX;

    }

    @Override
    public int getParentIndex() {
        return 0;
    }

    public enum Condition {
        IN_THE_QUANTITY_OF(Hub2Constants.IN_THE_QUANTITY_OF),
        IN_THE_TOTAL_SPEND_OF(Hub2Constants.IN_THE_TOTAL_SPEND_OF);

        private final String conditionName;

        Condition(final String name) {
            this.conditionName = name;
        }

        @Override
        public String toString() {
            return conditionName;
        }
    }

    public SelectItems getSelectItemsDialog() {
        return selectItemsDialog;
    }

    public void setSelectItemsDialog(SelectItems selectItemsDialog) {
        this.selectItemsButton = "clicked Select items button- per occurrences";
        this.selectItemsDialog = selectItemsDialog;
    }

    public String getConditionKey() {
        return conditionKey;
    }

    public void setConditionKey(Condition condition) {
        this.conditionKey = condition.toString();
    }

    public String getConditionValue() {
        return conditionValue;
    }

    public void setConditionValue(String conditionValue) {
        this.conditionValue = conditionValue;
    }

    public void clickAddOccurrencesCondition() {
        this.addOccurrencesCondition = "Clicked 'Add occurrences line'";
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        this.conditionIndex = i;
    }

    @Override
    public int getNext() {
        return conditionIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
