package hub.hub2_0.basePages.loyalty;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import org.openqa.selenium.By;

public class LoyaltyCenterPage extends BaseSeleniumObject {

    public static final By CAMPAIGN_CENTER = By.xpath("//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"navigation.campaign.center\"]");
    public static final By GIFTS = By.xpath("//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"navigation.gifts.text\"]");


    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
