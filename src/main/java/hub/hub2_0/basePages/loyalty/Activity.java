package hub.hub2_0.basePages.loyalty;


import hub.hub2_0.Hub2Constants;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.basePages.loyalty.gifts.Gift;
import hub.hub2_0.basePages.loyalty.gifts.GiftDisplay;
import hub.hub2_0.basePages.loyalty.templates.SaveProductTemplate;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.conditions.ActivityCondition;

import java.util.List;

public interface Activity {

    public static final String ACTIVITY_NAME = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.name\"]";
    public static final String ACTIVITY_DESCRIPTION = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.description\"]";
    public static final String CANCEL_ACTIVITY_BUTTON = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.cancel\"]";
    public static final String BACK_ACTIVITY_BUTTON = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.back\"]";
    public static final String PUBLISH_ACTIVITY_BUTTON = "activity.save";
    public static final String LOAD_ANOTHER = "activity.template.load-another";
    public static final String SAVE_AS_TEMPLATE = "activity.save-as-template.business.trigger";
    public static final String SAVE_AS_PRODUCT_TEMPLATE = "activity.save-as-template.product.trigger";
    public static final String SAVE_ACTIVITY_AS_DRAFT_BUTTON = "activity.save-as-draft";
    public static final String SPLIT_CASES_ICON = "activity.actions.split-to-cases";
    public static final String CANCEL_SPLIT_ICON = "activity.actions.cancel-split";

    void setActivityName(String name);

    void setActivityDescription(String description);

    String getActivityName();

    String getActivityDescription();

    void addCase(Case caze);

    void setCases(List<Case> cases);

    void clickPublishActivityBtn();

    void clickBack() throws InterruptedException;

    void clickCancel() throws InterruptedException;

    void clickLoadAnother();

    void clickSaveAsTemplateAndSave(String templateName, String description);

    void clickSaveAsTemplateAndReplace(String newTemplateName, String newDescription);

    void clickSaveAsTemplateAndSaveAsNew(String newTemplateName, String newDescription);

    void clickSaveAsProductTemplate(String category, String vertical, String templateName, String description);

    void clickSaveAsProductTemplate(SaveProductTemplate saveProductTemplate);

    SaveProductTemplate getSaveProductTemplate();

    void clickSaveActivityAsDraftBtn();

    List<Case> getCases();

    void setGlobalConditions(List<GlobalCondition> globalConditions);

    List<GlobalCondition> getGlobalConditions();

    void addGlobalCondition(GlobalCondition condition);

    void setConditions(List<ActivityCondition> activityConditions);

    List<ActivityCondition> getConditions();

    void addCondition(ActivityCondition condition);

    ActivityCondition replaceCondition(ActivityCondition currentCondition, ActivityCondition newCondition);

    void removeCondition(ActivityCondition condition);

    void clickSplitCases();

    void clickCancelSplit();

    void setActions(List<ActivityAction> activityActions);

    void setGiftDisplay(GiftDisplay giftDisplay);

    ActivityAction replaceAction(ActivityAction currentAction, ActivityAction newAction);

    void addAction(ActivityAction action);

    void removeAction(ActivityAction action);

    ActivityAction getAction(int actionIndex);

    List<ActivityAction> getActions();

    void setOccurrencesConditions(List<OccurrencesCondition> occurrencesConditions);

    Case defaultCase();

}
