package hub.hub2_0.basePages.loyalty;


import hub.base.BaseSeleniumObjectHub2;
import hub.base.BaseService;
import hub.base.SeleniumPath;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.basePages.loyalty.cases.RuleCase;
import hub.hub2_0.basePages.loyalty.gifts.GiftDisplay;
import hub.hub2_0.basePages.loyalty.templates.ReplaceOrSaveTemplate;
import hub.hub2_0.basePages.loyalty.templates.SaveProductTemplate;
import hub.hub2_0.basePages.loyalty.templates.SaveTemplate;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.enums.ActivityStatus;
import hub.hub2_0.common.triggers.When;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

import static hub.base.BaseService.WAIT_AFTER_CLICK_MILLSEC;

public class Rule extends BaseSeleniumObjectHub2 implements Activity {

    private static final String SIMPLE_MODE = "activity.actions.button.simpleMode";
    private static final String PER_OCCURRENCE_MODE = "activity.actions.button.bundleMode";

    @SeleniumPath(value = ACTIVITY_NAME, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS)
    private String activityName;
    @SeleniumPath(value = ACTIVITY_DESCRIPTION, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS,readValue =ACTIVITY_DESCRIPTION)
    private String activityDescription;

    private When when;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<GlobalCondition> globalConditions;

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<ActivityCondition> conditions;

    @SeleniumPath(value = SPLIT_CASES_ICON, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_WAIT)
    private String splitCases;

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    private List<Case> cases;

    @SeleniumPath(value = SIMPLE_MODE, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String simpleMode;
    @SeleniumPath(value = PER_OCCURRENCE_MODE, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String perOccurrencesMode;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<OccurrencesCondition> occurrencesConditions; //array of automation actions action

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<ActivityAction> actions; //array of automation actions action

    @SeleniumPath(value = CANCEL_SPLIT_ICON, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String cancelSplit;


    @SeleniumPath(value = SAVE_AS_TEMPLATE, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.SCROLL_AND_CLICK)
    private String saveAsTemplate;
    private ReplaceOrSaveTemplate replaceOrSaveTemplate;
    private SaveTemplate saveTemplate;
    @SeleniumPath(value = SAVE_AS_PRODUCT_TEMPLATE, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.SCROLL_AND_CLICK)
    private String saveAsProductTemplate;
    private SaveProductTemplate saveProductTemplate;
    @SeleniumPath(value = PUBLISH_ACTIVITY_BUTTON, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String publishActivityBtn;
    @SeleniumPath(value = SAVE_ACTIVITY_AS_DRAFT_BUTTON, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String saveActivityAsDraftBtn;
    @SeleniumPath(value = CANCEL_ACTIVITY_BUTTON, type=SeleniumPath.Type.BY_AUTOMATION_ID ,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String cancelActivityBtn;
    @SeleniumPath(value = BACK_ACTIVITY_BUTTON, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String backActivityButton;
    @SeleniumPath(value = LOAD_ANOTHER, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String loadAnother;


    private int currentActivityIdx =0; // hold the current Activity related to Box
    private int globalConditionsCounter = 0;
    private int conditionsCounter = 0;
    private int actionsCounter = 0;
    private int occurrencesConditionsCounter = 0;
    private int caseCounter = 0;

    public Rule() {
        if(splitCases == null)
            this.objectPrefix = "";
    }

    public Rule(ActivityStatus activityStatus) throws Exception {
        if(splitCases == null)
            this.objectPrefix = "";
        if (activityStatus.equals(ActivityStatus.Draft))
            clickSaveActivityAsDraftBtn();
        else if (activityStatus.equals(ActivityStatus.Active))
            clickPublishActivityBtn();
    }

    public void clickSplitCases() {
        this.splitCases = "Split cases on";
    }


    public void clickCancelSplit() {
        this.cancelSplit = "Cancel split";
        this.caseCounter = 0;
    }


    public int getCaseCounter() {
        return this.caseCounter;
    }

    @Override
    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    @Override
    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }

    @Override
    public String getActivityName() {
        return activityName;
    }

    @Override
    public String getActivityDescription() {
        return activityDescription;
    }

    public void setWhen(When when) {
        this.when = when;
        if(when != null) {
            this.when.clickWhen();
        }
    }

    @Override
    public void addCase(Case caze) {
        if(cases == null) {
            cases = new ArrayList<>();
        }
        caze.setNext(caseCounter);
        this.cases.add(caze);
        caseCounter++;

    }

    @Override
    public void setCases(List<Case> cases) {
        for(Case aCase : cases) {
            if(caseCounter > 0) {
                ((RuleCase)aCase).clickAddCase();
            }
            addCase(aCase);
        }
    }

    @Override
    public void clickPublishActivityBtn() {
        this.saveActivityAsDraftBtn = null;
        this.publishActivityBtn = "Publish activity button";
        this.loadAnother = null;
        this.cancelActivityBtn = null;
        this.backActivityButton = null;
    }
    @Override
    public void clickLoadAnother(){
        this.loadAnother = "Click Load another button";
        this.saveActivityAsDraftBtn = null;
        this.publishActivityBtn=null;
        this.cancelActivityBtn = null;
        this.backActivityButton = null;
    }

    @Override
    public void clickSaveAsTemplateAndSave(String newTemplateName, String newDescription){
        this.saveAsTemplate = "Click Save as template";
        this.replaceOrSaveTemplate = null;
        saveTemplate(newTemplateName, newDescription);
    }

    @Override
    public void clickSaveAsTemplateAndReplace(String newTemplateName, String newDescription){
        this.saveAsTemplate = "Click Save as template";
        this.replaceOrSaveTemplate = new ReplaceOrSaveTemplate();
        this.replaceOrSaveTemplate.clickReplace();
        saveTemplate(newTemplateName, newDescription);
    }

    @Override
    public void clickSaveAsTemplateAndSaveAsNew(String newTemplateName, String newDescription){
        this.saveAsTemplate = "Click Save as template";
        this.replaceOrSaveTemplate = new ReplaceOrSaveTemplate();
        this.replaceOrSaveTemplate.clickSaveAsNewTemplate();
        saveTemplate(newTemplateName, newDescription);
    }

    @Override
    public void clickSaveAsProductTemplate(String category,String vertical, String templateName, String description){
        this.saveAsProductTemplate = "Click Save as product template";
        this.saveProductTemplate = new SaveProductTemplate();
        this.saveProductTemplate.setCategory(category);
        this.saveProductTemplate.setVerticals(vertical);
        this.saveProductTemplate.setName(templateName);
        this.saveProductTemplate.setDescription(description);
        this.saveProductTemplate.clickSave();
    }

    @Override
    public void clickSaveAsProductTemplate(SaveProductTemplate productTemplate){
        this.saveAsProductTemplate = "Click Save as product template";
        this.saveProductTemplate = productTemplate;
        this.saveProductTemplate.clickSave();
    }

    private void saveTemplate(String newTemplateName, String newDescription) {
        SaveTemplate saveTemplate = new SaveTemplate();
        saveTemplate.clickSave();
        if(newTemplateName != null) {
            saveTemplate.setName(newTemplateName);
        }
        if(newDescription != null) {
            saveTemplate.setDescription(newDescription);
        }
        this.saveTemplate = saveTemplate;
    }


    @Override
    public void clickSaveActivityAsDraftBtn() {
        this.saveActivityAsDraftBtn = "Save as draft button";
        this.publishActivityBtn=null;
        this.loadAnother = null;
        this.cancelActivityBtn = null;
        this.backActivityButton = null;
    }

    public When getWhen() {
        return when;
    }

    /** Global Conditions**/
    public void setGlobalConditions(List<GlobalCondition> globalConditions) {
        for(GlobalCondition condition : globalConditions) {
            condition.clickGlobalCondition();
            addGlobalCondition(condition);
        }

    }
    public List<GlobalCondition> getGlobalConditions() {
        if(globalConditions == null)
            globalConditions = new ArrayList<>();
        return globalConditions;
    }


    public void addGlobalCondition(GlobalCondition condition) {
        condition.setNext(globalConditionsCounter);
        getGlobalConditions().add(condition);
        globalConditionsCounter++;
        condition.clickSave();
    }

    /***************************************************/
    public void setConditions(List<ActivityCondition> activityConditions) {
        for(ActivityCondition condition : activityConditions) {
            condition.clickAdd();
            addCondition(condition);
        }
    }

    public List<ActivityCondition> getConditions() {
        if(conditions == null)
            conditions = new ArrayList<>();
        return conditions;
    }


    public void addCondition(ActivityCondition condition) {
        condition.setNext(conditionsCounter);
        getConditions().add(condition);
        conditionsCounter++;
    }

    public ActivityCondition replaceCondition(ActivityCondition currentCondition ,ActivityCondition newCondition) {
        newCondition.setNext(currentCondition.getNext());
        getConditions().add(currentCondition.getNext(),newCondition);
        return newCondition;
    }

    public void removeCondition(ActivityCondition condition ) {
        getConditions().remove(condition);
        conditionsCounter--;
    }

    public List<Case> getCases() {
        if(cases == null) {
            cases = new ArrayList<>();
        }
        return cases;
    }

    //*****************************Activity Actions******************************/
    public void clickPerOccurrencesMode() {
        this.perOccurrencesMode = "clicked per occurrences mode";
    }

    public void setOccurrencesConditions(List<OccurrencesCondition> occurrencesConditions) {
        for(OccurrencesCondition condition : occurrencesConditions) {
            addOccurrencesCondition(condition);
        }
    }

    public void addOccurrencesCondition(OccurrencesCondition occurrencesCondition) {
        occurrencesCondition.setNext(occurrencesConditionsCounter);
        getOccurrencesConditions().add(occurrencesCondition);
        occurrencesConditionsCounter++;
    }

    public List<OccurrencesCondition> getOccurrencesConditions() {
        if(occurrencesConditions == null)
            occurrencesConditions = new ArrayList<>();
        return occurrencesConditions;
    }

    public void setActions(List<ActivityAction> activityActions) {
        for(ActivityAction action : activityActions) {
            addAction(action);
        }
    }

    @Override
    public void setGiftDisplay(GiftDisplay giftDisplay) {

    }

    public ActivityAction replaceAction(ActivityAction currentAction ,ActivityAction newAction) {
        newAction.setNext(currentAction.getNext());
        getActions().add(currentAction.getNext(),newAction);
        return newAction;
    }

    public void addAction(ActivityAction action ) {
        action.setNext(actionsCounter);
        getActions().add(action);
        actionsCounter++;
    }


    public void removeAction(ActivityAction action ) {
        getActions().remove(action);
        actionsCounter--;
    }


    public ActivityAction getAction(int actionIndex ) {
        return getActions().get(actionIndex);
    }

    public List<ActivityAction> getActions() {
        if(actions == null)
            actions = new ArrayList<>();
        return actions;
    }
    @Override
    public Case defaultCase() {
        return getCases().get(0);
    }

    @Override
    public void clickBack() throws InterruptedException {
        BaseService.getDriver().findElement(By.xpath(BACK_ACTIVITY_BUTTON)).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
    }

    @Override
    public void clickCancel() throws InterruptedException {
        BaseService.getDriver().findElement(By.xpath(CANCEL_ACTIVITY_BUTTON)).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
    }


    public SaveProductTemplate getSaveProductTemplate() {
        return saveProductTemplate;
    }

    public void setSaveProductTemplate(SaveProductTemplate saveProductTemplate) {
        this.saveProductTemplate = saveProductTemplate;
    }

    @Override
    public int getNext() {
        return this.currentActivityIdx;
    }
    @Override
    public int getNextTag () {
        return 0;
    }
    @Override
    public void setNext(int i) { this.currentActivityIdx=i; }
    @Override
    public int getCreateActionCounter() { return 0; }
    @Override
    public String getNextTagsPath (int i){
        return  "";
    }


    @Override
    public void setParentIndex(int i) {
    }

    @Override
    public int getParentIndex() {
        return 0;
    }
}