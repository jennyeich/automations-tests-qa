package hub.hub2_0.basePages.loyalty.templates;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

import java.util.ArrayList;
import java.util.List;

public class SaveProductTemplate extends BaseSeleniumObjectHub2 {


    private static final String SELECT_CATEGORIES = "save-as-template.dropdown";
    private static final String CATEGORIES = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"save-as-template.dropdown.{0}\"]";
    private static final String NAME = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"save-as-template.name\"]/input";
    private static final String DESCRIPTION = "save-as-template.description";
    private static final String VERTICALS_SELECT ="save-as-template.verticals.dropdown";
    private static final String VERTICALS = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"save-as-template.verticals.dropdown.{0}\"]";
    private static final String CANCEL = "save-as-template.btn.cancel";
    private static final String SAVE = "save-as-template.btn.save";


    @SeleniumPath(value = NAME, type=SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT)
    private String name;
    @SeleniumPath(value = SELECT_CATEGORIES, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String categories_open;
    @SeleniumPath(value = CATEGORIES, type=SeleniumPath.Type.BY_DYNAMIC_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2)
    private String categories;
    @SeleniumPath(value = DESCRIPTION, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.TEXT_AREA)
    private String description;
    @SeleniumPath(value = VERTICALS_SELECT, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String verticals_open;
    @SeleniumPath(value = VERTICALS, type=SeleniumPath.Type.BY_DYNAMIC_XPATH, elementInputType = SeleniumPath.ElementInputType.MULTI_SELECT_HUB2)
    private StringBuffer verticals;
    @SeleniumPath(value = CANCEL, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String cancel;
    @SeleniumPath(value = SAVE, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String save;

    public void setCategory(String category) {
        this.categories_open = "select category";
        this.categories = category;
    }


    public void setVerticals( String... verticals) {
        this.verticals_open = "select vertical";
        this.verticals = new StringBuffer();
        for (String vertical:verticals) {
            this.verticals.append(vertical).append(",");
        }
        this.verticals.toString();
    }

    public void addVertical(String vertical) {
        this.verticals_open = "select vertical";
        if(this.verticals == null){
            this.verticals = new StringBuffer();
            this.verticals.append(vertical.toString()).append(",");
        }else {
            this.verticals.append(vertical.toString()).append(",");
        }
    }

    public String getVerticals() {
        return this.verticals.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void clickCancel() {
        this.cancel = "click Cancel button";
    }

    public void clickSave() {
        this.save = "click Save button";
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
