package hub.hub2_0.basePages.loyalty.templates;

import hub.base.BaseService;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.services.loyalty.BaseHub2Service;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TemplatesLibrary {

    public static final Logger log4j = Logger.getLogger(TemplatesLibrary.class);

    public static final String PRODUCT_TEMPLATE_PREFIX = "PT_";
    public static final String PRODUCT_TEMPLATE_ALL_VERTICALS = "PT_All_Verticals";
    private static String TEMPLATE_NAME = "//div[contains(text(),\"%s\")]";

    private static final String START_FROM_SCRATCH = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"template-library.start-from-scratch\"]";

    private static String TEMPLATE_CARD = TEMPLATE_NAME + "//ancestor::div[2]";
    private static final String TEMPLATE_ACTIONS_MENU = TEMPLATE_CARD + "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity-templates.template-card.dropdown\"]";
    private static final String ACTION_MENU_EDIT = TEMPLATE_CARD + "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity-templates.template-card.menu.edit.trigger\"]";
    private static final String ACTION_MENU_DELETE = TEMPLATE_CARD + "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity-templates.template-card.menu.delete.trigger\"]";
    private static final String DELETE_BUTTON = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"undefined.delete-confirmation.btn.delete\"]";
    public static final String BACK_BUTTON = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"link.back\"]";


    public static void clickStartFromScratch() throws Exception {
        BaseService.getDriver().findElement(By.xpath(START_FROM_SCRATCH)).click();
        TimeUnit.SECONDS.sleep(2);
        log4j.info("Clicked start from scratch");
    }

    public static void clickStartFromTemplate(String templateName) {
        String templateXpath = String.format(TEMPLATE_CARD, templateName);
        BaseService.getDriver().findElement(By.xpath(templateXpath)).click();
        log4j.info("Clicked start from template");
    }

    public static boolean findTemplate(String templateName) {

        String templateXpath = String.format(TEMPLATE_NAME, templateName);
        List<WebElement> templates = BaseService.getDriver().findElements(By.xpath(templateXpath));
        return ( templates.size()>0 );

    }

    public static WebElement findTemplateIfExist(String templateName) {

        String templateXpath = String.format(TEMPLATE_NAME, templateName);
        List<WebElement> templates = BaseService.getDriver().findElements(By.xpath(templateXpath));
        if ( templates.size()>0 )
            return templates.get(0);

        return null;
    }

    public static List<WebElement> getTemplatesIfExist(String templateName) {

        String templateXpath = String.format(TEMPLATE_NAME, templateName);
        List<WebElement> templates = BaseService.getDriver().findElements(By.xpath(templateXpath));
        if ( templates.size()>0 )
            return templates;

        return new ArrayList<>();
    }

    public static void editTemplateName(String templateName, String newTemplateName, String description) throws Exception {
        hoverAndClick(templateName);
        String editOption = String.format(ACTION_MENU_EDIT, templateName);
        BaseService.getDriver().findElement(By.xpath(editOption)).click();
        Thread.sleep(BaseService.WAIT_AFTER_CLICK_MILLSEC);
        SaveTemplate saveTemplate = new SaveTemplate();
        saveTemplate.clickSave();
        if(newTemplateName != null) {
            saveTemplate.setName(newTemplateName);
        }
        if(description != null) {
            saveTemplate.setDescription(description);
        }
        BaseService.fillSeleniumObject(saveTemplate);
    }

    public static void deleteTemplate(String templateName) throws Exception {
        hoverAndClick(templateName);
        String deleteOption = String.format(ACTION_MENU_DELETE, templateName);
        BaseService.getDriver().findElement(By.xpath(deleteOption)).click();
        Thread.sleep(BaseService.WAIT_AFTER_CLICK_MILLSEC);
        BaseService.getDriver().findElement(By.xpath(DELETE_BUTTON)).click();
        Thread.sleep(BaseService.WAIT_AFTER_CLICK_MILLSEC);
        BaseHub2Service.waitForLoad();
    }

    public static void clickBack() {
        BaseService.getDriver().findElement(By.xpath(BACK_BUTTON)).click();
    }


    private static void hoverAndClick( String templateName) throws Exception{
        String templateXpath = String.format(TEMPLATE_ACTIONS_MENU, templateName);
        WebElement templateElement = BaseService.getDriver().findElement(By.xpath(templateXpath));
        Actions action = new Actions(BaseService.getDriver());
        action.moveToElement(templateElement);
        action.click(templateElement).perform();
        Thread.sleep(BaseService.WAIT_AFTER_CLICK_MILLSEC);
    }


    public static boolean isEditOrDeleteAllowed(String templateName) throws Exception {
        try {
            hoverAndClick(templateName);
            return true;
        }catch (NoSuchElementException e){
            return false;
        }
    }
}
