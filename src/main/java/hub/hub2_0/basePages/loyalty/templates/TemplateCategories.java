package hub.hub2_0.basePages.loyalty.templates;

public enum TemplateCategories {

    Summer("summer"),
    Winter("winter"),
    Autoum("autoum"),
    Spring("spring"),
    None("none");

    private final String name;
    TemplateCategories(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
