package hub.hub2_0.basePages.loyalty.templates;

public enum TemplateVerticals {

    Entertainment("Entertainment"),
    RetailForeignExchange("Retail Foreign Exchange"),
    Services("Services"),
    FoodAndDrink("Food & Drink"),
    General("General"),
    Sports("Sports"),
    RetailAndFashion("Retail & Fashion"),
    HealthAndBeauty("Health & Beauty"),
    Other("Other"),
    More("More");

    private final String name;
    TemplateVerticals(final String name) {
        this.name = name;
    }

    //return the data inside the () - the xpath suffix of this data-automation-id
    public String getValue() {
        return this.name;
    }

    /**
     # 0 Entertainment
     # 1 Retail Foreign Exchange
     # 2 Services
     # 3 Food & Drink
     # 4 General
     # 5 Sports
     # 6 Retail & Fashion
     # 7 Health & Beauty
     # 8 Other
     # 9 More
     */
    public int getDBValue(){
        if(name.equals(Entertainment.name)){
            return 0;
        }else if(name.equals(RetailForeignExchange.name)){
            return 1;
        }else if(name.equals(Services.name)){
            return 2;
        }else if(name.equals(FoodAndDrink.name)){
            return 3;
        }else if(name.equals(General.name)){
            return 4;
        }else if(name.equals(Sports.name)){
            return 5;
        }else if(name.equals(RetailAndFashion.name)){
            return 6;
        }else if(name.equals(HealthAndBeauty.name)){
            return 7;
        }else if(name.equals(Other.name)){
            return 8;
        }else if(name.equals(More.name)){
            return 9;
        }
        return -1;//Unknown
    }

    public static String getNameOfVertical(String currentVertical) {
        if(TemplateVerticals.Entertainment.getValue().equals(currentVertical))
            return TemplateVerticals.Entertainment.name();
        else  if(TemplateVerticals.RetailForeignExchange.getValue().equals(currentVertical))
            return TemplateVerticals.RetailForeignExchange.name();
        else  if(TemplateVerticals.Services.getValue().equals(currentVertical))
            return TemplateVerticals.Services.name();
        else  if(TemplateVerticals.FoodAndDrink.getValue().equals(currentVertical))
            return TemplateVerticals.FoodAndDrink.name();
        else  if(TemplateVerticals.General.getValue().equals(currentVertical))
            return TemplateVerticals.General.name();
        else  if(TemplateVerticals.Sports.getValue().equals(currentVertical))
            return TemplateVerticals.Sports.name();
        else  if(TemplateVerticals.RetailAndFashion.getValue().equals(currentVertical))
            return TemplateVerticals.RetailAndFashion.name();
        else  if(TemplateVerticals.HealthAndBeauty.getValue().equals(currentVertical))
            return TemplateVerticals.HealthAndBeauty.name();
        else  if(TemplateVerticals.Other.getValue().equals(currentVertical))
            return TemplateVerticals.Other.name();
        else  if(TemplateVerticals.More.getValue().equals(currentVertical))
            return TemplateVerticals.More.name();

        return "None";//Unknown

    }

}
