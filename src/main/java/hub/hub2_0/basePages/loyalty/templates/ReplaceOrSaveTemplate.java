package hub.hub2_0.basePages.loyalty.templates;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class ReplaceOrSaveTemplate extends BaseSeleniumObjectHub2 {

    private static final String CANCEL = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ", \"replace-or-new-template.btn.cancel\")]";
    private static final String REPLACE = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ", \"replace-or-new-template.btn.replace\")]";
    private static final String SAVE_AS_NEW_TEMPLATE = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ", \"replace-or-new-template.btn.save\")]";

    @SeleniumPath(value = CANCEL, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String cancel;
    @SeleniumPath(value = REPLACE, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String replace;
    @SeleniumPath(value = SAVE_AS_NEW_TEMPLATE, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String saveAsNewTemplate;

    public void clickCancel() {
        this.cancel = "Replace or save template dialog- click cancel button";
        this.replace = null;
        this.saveAsNewTemplate = null;
    }

    public void clickReplace() {
        this.replace = "Replace or save template dialog- click replace button";
        this.cancel = null;
        this.saveAsNewTemplate = null;
    }

    public void clickSaveAsNewTemplate() {
        this.saveAsNewTemplate = "Replace or save template dialog- click Save as new template button";
        this.replace = null;
        this.cancel = null;
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
