package hub.hub2_0.basePages.loyalty.templates;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class SaveTemplate extends BaseSeleniumObjectHub2{

    private static final String NAME = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"save-as-template.name\"]/input";
    private static final String DESCRIPTION = "save-as-template.description";
    private static final String CANCEL = "save-as-template.btn.cancel";
    private static final String SAVE = "save-as-template.btn.save";


    @SeleniumPath(value = NAME, type=SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT)
    private String name;
    @SeleniumPath(value = DESCRIPTION, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT)
    private String description;
    @SeleniumPath(value = CANCEL, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String cancel;
    @SeleniumPath(value = SAVE, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String save;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void clickCancel() {
        this.cancel = "click Cancel button";
    }

    public void clickSave() {
        this.save = "click Save button";
    }


    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
