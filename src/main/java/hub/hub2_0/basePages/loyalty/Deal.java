package hub.hub2_0.basePages.loyalty;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.BaseService;
import hub.base.SeleniumPath;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.basePages.loyalty.cases.DealCase;
import hub.hub2_0.basePages.loyalty.gifts.GiftDisplay;
import hub.hub2_0.basePages.loyalty.templates.ReplaceOrSaveTemplate;
import hub.hub2_0.basePages.loyalty.templates.SaveProductTemplate;
import hub.hub2_0.basePages.loyalty.templates.SaveTemplate;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountAction;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.enums.ActivityStatus;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

import static hub.base.BaseService.WAIT_AFTER_CLICK_MILLSEC;

public class Deal extends BaseSeleniumObjectHub2 implements Activity {

    protected static final String ENTIRE_TICKET_DISCOUNT = "activity.actions.button.simpleEntireTicketDiscount";
    protected static final String SPECIFIC_ITEMS_DISCOUNT = "activity.actions.button.simpleSpecificItemsDiscount";
    protected static final String ADVANCED_DISCOUNT = "activity.actions.button.advancedDiscount";


    @SeleniumPath(value = ACTIVITY_NAME, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS)
    private String activityName;
    @SeleniumPath(value = ACTIVITY_DESCRIPTION, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = ACTIVITY_DESCRIPTION)
    private String activityDescription;
    @SeleniumPath(value = "", type = SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<GlobalCondition> globalConditions;
    @SeleniumPath(value = "", type = SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<ActivityCondition> conditions;

    //Split to cases
    @SeleniumPath(value = SPLIT_CASES_ICON, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_WAIT)
    private String splitCases;
    @SeleniumPath(value = "", type = SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    private List<Case> cases;
    @SeleniumPath(value = CANCEL_SPLIT_ICON, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String cancelSplit;

    @SeleniumPath(value = ENTIRE_TICKET_DISCOUNT, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String entireTicketDiscount;
    @SeleniumPath(value = SPECIFIC_ITEMS_DISCOUNT, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String specificItemsDiscount;
    @SeleniumPath(value = ADVANCED_DISCOUNT, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String advancedDiscount;

    @SeleniumPath(value = "", type = SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<OccurrencesCondition> occurrencesConditions;
    @SeleniumPath(value = "", type = SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<ActivityAction> actions; //array of automation actions action

    @SeleniumPath(value = SAVE_AS_TEMPLATE, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.SCROLL_AND_CLICK)
    private String saveAsTemplate;
    private ReplaceOrSaveTemplate replaceOrSaveTemplate;
    private SaveTemplate saveTemplate;
    @SeleniumPath(value = SAVE_AS_PRODUCT_TEMPLATE, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.SCROLL_AND_CLICK)
    private String saveAsProductTemplate;
    private SaveProductTemplate saveProductTemplate;

    @SeleniumPath(value = PUBLISH_ACTIVITY_BUTTON, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String publishActivityBtn;
    @SeleniumPath(value = SAVE_ACTIVITY_AS_DRAFT_BUTTON, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String saveActivityAsDraftBtn;
    @SeleniumPath(value = LOAD_ANOTHER, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String loadAnother;

    protected DiscountType discountType = DiscountType.ENTIRE_TICKET;

    protected int currentActivityIdx = 0; // hold the current Activity related to Box
    protected int globalConditionsCounter = 0;
    protected int conditionsCounter = 0;
    protected int actionsCounter = 0;
    protected int occurrencesConditionsCounter = 0;
    protected int caseCounter = 0;

    public Deal() {
        if (splitCases == null)
            this.objectPrefix = "";
    }

    public Deal(ActivityStatus activityStatus) throws Exception {
        if (splitCases == null)
            this.objectPrefix = "";
        if (activityStatus.equals(ActivityStatus.Draft))
            clickSaveActivityAsDraftBtn();
        else if (activityStatus.equals(ActivityStatus.Active))
            clickPublishActivityBtn();
    }

    public void setEntireTicketDiscountEditMode() {
        this.discountType = DiscountType.ENTIRE_TICKET;

    }

    public void setSpecificItemsDiscountEditMode() {
        this.discountType = DiscountType.SPECIFIC_ITEMS;
    }

    public void setAdvancedDiscountEditMode() {
        this.discountType = DiscountType.SPECIFIC_ITEMS_ADVANCED;
    }

    public enum DiscountType {
        ENTIRE_TICKET("OnEntireTicket"),
        SPECIFIC_ITEMS("OnSpecificItems"),
        SPECIFIC_ITEMS_ADVANCED("OnSpecificItemsAdvanced"),
        ADVANCED("Advanced"),
        EMPTY_SUFFIX("");

        private final String discountType;

        DiscountType(final String discountType) {
            this.discountType = discountType;
        }

        @Override
        public String toString() {
            return discountType;
        }
    }

    @Override
    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    @Override
    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }

    @Override
    public String getActivityName() {
        return activityName;
    }

    @Override
    public String getActivityDescription() {
        return activityDescription;
    }

    ////**** Global Conditions methods ****////

    @Override
    public void setGlobalConditions(List<GlobalCondition> globalConditions) {
        for (GlobalCondition condition : globalConditions) {
            condition.clickGlobalCondition();
            addGlobalCondition(condition);
        }
    }

    @Override
    public List<GlobalCondition> getGlobalConditions() {
        if (globalConditions == null)
            globalConditions = new ArrayList<>();
        return globalConditions;
    }

    @Override
    public void addGlobalCondition(GlobalCondition condition) {
        condition.setNext(globalConditionsCounter);
        getGlobalConditions().add(condition);
        globalConditionsCounter++;
        condition.clickSave();
    }

    @Override
    public void setConditions(List<ActivityCondition> activityConditions) {
        for (ActivityCondition condition : activityConditions) {
            condition.clickAdd();
            addCondition(condition);
        }
    }

    @Override
    public List<ActivityCondition> getConditions() {
        if (conditions == null)
            conditions = new ArrayList<>();
        return conditions;
    }

    @Override
    public void addCondition(ActivityCondition condition) {
        condition.setNext(conditionsCounter);
        getConditions().add(condition);
        conditionsCounter++;
    }

    @Override
    public ActivityCondition replaceCondition(ActivityCondition currentCondition, ActivityCondition newCondition) {
        newCondition.setNext(currentCondition.getNext());
        getConditions().add(currentCondition.getNext(), newCondition);
        return newCondition;
    }

    @Override
    public void removeCondition(ActivityCondition condition) {
        getConditions().remove(condition);
        conditionsCounter--;
    }

    ////**** Split to cases methods****////

    @Override
    public void clickSplitCases() {
        this.splitCases = "Split cases on";
    }

    @Override
    public void clickCancelSplit() {
        this.cancelSplit = "Cancel split";
        this.caseCounter = 0;
    }

    public int getCaseCounter() {
        return this.caseCounter;
    }

    @Override
    public void addCase(Case caze) {
        if (cases == null) {
            cases = new ArrayList<>();
        }
        caze.setNext(caseCounter);
        this.cases.add(caze);
        caseCounter++;
    }

    @Override
    public void setCases(List<Case> cases) {
        for (Case aCase : cases) {
            if (caseCounter > 0) {
                ((DealCase) aCase).clickAddCase();
            }
            addCase(aCase);
        }
    }

    @Override
    public List<Case> getCases() {
        if (cases == null) {
            cases = new ArrayList<>();
        }
        return cases;
    }

    @Override
    public Case defaultCase() {
        return getCases().get(0);
    }

    ////**** Activity Actions ****////

    public void clickEntireTicketDiscount() {
        this.entireTicketDiscount = "Clicked 'Give a discount on entire ticket'.";
        this.discountType = DiscountType.ENTIRE_TICKET;
    }

    public void clickSpecificItemsDiscount() {
        this.specificItemsDiscount = "Clicked 'Give discount on specific items or make them free'";
        this.discountType = DiscountType.SPECIFIC_ITEMS;

    }

    public void clickAdvancedDiscount() {
        this.advancedDiscount = "Clicked 'Advanced deal: 1+1, buy X get Y, free item for each $100, and more'";
        this.discountType = DiscountType.SPECIFIC_ITEMS_ADVANCED;
    }

    @Override
    public void setActions(List<ActivityAction> activityActions) {
        for (ActivityAction action : activityActions) {
            addAction(action);
        }
    }

    @Override
    public void setGiftDisplay(GiftDisplay giftDisplay) {

    }

    @Override
    public ActivityAction replaceAction(ActivityAction currentAction, ActivityAction newAction) {
        newAction.setNext(currentAction.getNext());
        getActions().add(currentAction.getNext(), newAction);
        return newAction;
    }

    @Override
    public void addAction(ActivityAction action) {
        action.setNext(actionsCounter);
        action = completeActionID(action);
        getActions().add(action);
        actionsCounter++;
    }

    private ActivityAction completeActionID(ActivityAction action) {
        switch (this.discountType) {
            case ENTIRE_TICKET:
                action.setIdSubString(DiscountType.ENTIRE_TICKET.toString());
                break;
            case SPECIFIC_ITEMS:
                if (action instanceof PercentOffDiscountAction || action instanceof AmountOffDiscountAction) {
                    action.setIdSubString(DiscountType.SPECIFIC_ITEMS.toString());
                } else {
                    action.setIdSubString(DiscountType.EMPTY_SUFFIX.toString());
                }
                break;
            case SPECIFIC_ITEMS_ADVANCED:
                if (action instanceof PercentOffDiscountAction || action instanceof AmountOffDiscountAction) {
                    action.setIdSubString(DiscountType.SPECIFIC_ITEMS_ADVANCED.toString());
                } else {
                    action.setIdSubString(DiscountType.ADVANCED.toString());
                }
                break;
        }
        return action;

    }

    @Override
    public void removeAction(ActivityAction action) {
        getActions().remove(action);
        actionsCounter--;
    }

    @Override
    public ActivityAction getAction(int actionIndex) {
        return getActions().get(actionIndex);
    }

    @Override
    public List<ActivityAction> getActions() {
        if (actions == null)
            actions = new ArrayList<>();
        return actions;
    }

    @Override
    public void setOccurrencesConditions(List<OccurrencesCondition> occurrencesConditions) {
        for (OccurrencesCondition condition : occurrencesConditions) {
            if (occurrencesConditionsCounter > 0) {
                condition.clickAddOccurrencesCondition();
            }
            addOccurrencesCondition(condition);
        }
    }

    private void addOccurrencesCondition(OccurrencesCondition occurrencesCondition) {
        occurrencesCondition.setNext(occurrencesConditionsCounter);
        getOccurrencesConditions().add(occurrencesCondition);
        occurrencesConditionsCounter++;
    }

    private List<OccurrencesCondition> getOccurrencesConditions() {
        if (occurrencesConditions == null)
            occurrencesConditions = new ArrayList<>();
        return occurrencesConditions;
    }

    ////**** Templates methods ****////

    @Override
    public void clickSaveAsTemplateAndSave(String newTemplateName, String newDescription) {
        this.saveAsTemplate = "Click Save as template";
        this.replaceOrSaveTemplate = null;
        saveTemplate(newTemplateName, newDescription);
    }

    @Override
    public void clickSaveAsTemplateAndReplace(String newTemplateName, String newDescription) {
        this.saveAsTemplate = "Click Save as template";
        this.replaceOrSaveTemplate = new ReplaceOrSaveTemplate();
        this.replaceOrSaveTemplate.clickReplace();
        saveTemplate(newTemplateName, newDescription);
    }

    @Override
    public void clickSaveAsTemplateAndSaveAsNew(String newTemplateName, String newDescription) {
        this.saveAsTemplate = "Click Save as template";
        this.replaceOrSaveTemplate = new ReplaceOrSaveTemplate();
        this.replaceOrSaveTemplate.clickSaveAsNewTemplate();
        saveTemplate(newTemplateName, newDescription);
    }

    @Override
    public void clickSaveAsProductTemplate(String category, String vertical, String templateName, String description) {
        this.saveAsProductTemplate = "Click Save as product template";
        this.saveProductTemplate = new SaveProductTemplate();
        this.saveProductTemplate.setCategory(category);
        this.saveProductTemplate.setVerticals(vertical);
        this.saveProductTemplate.setName(templateName);
        this.saveProductTemplate.setDescription(description);
        this.saveProductTemplate.clickSave();
    }

    @Override
    public void clickSaveAsProductTemplate(SaveProductTemplate productTemplate) {
        this.saveAsProductTemplate = "Click Save as product template";
        this.saveProductTemplate = productTemplate;
        this.saveProductTemplate.clickSave();
    }

    private void saveTemplate(String newTemplateName, String newDescription) {
        SaveTemplate saveTemplate = new SaveTemplate();
        saveTemplate.clickSave();
        if (newTemplateName != null) {
            saveTemplate.setName(newTemplateName);
        }
        if (newDescription != null) {
            saveTemplate.setDescription(newDescription);
        }
        this.saveTemplate = saveTemplate;
    }

    public SaveProductTemplate getSaveProductTemplate() {
        return saveProductTemplate;
    }

    public void setSaveProductTemplate(SaveProductTemplate saveProductTemplate) {
        this.saveProductTemplate = saveProductTemplate;
    }

    ////**** Activity navigation buttons ****////

    @Override
    public void clickPublishActivityBtn() {
        this.saveActivityAsDraftBtn = null;
        this.publishActivityBtn = "Publish activity button";
        this.loadAnother = null;
    }

    @Override
    public void clickLoadAnother() {
        this.loadAnother = "Click Load another button";
        this.saveActivityAsDraftBtn = null;
        this.publishActivityBtn = null;
    }

    @Override
    public void clickSaveActivityAsDraftBtn() {
        this.saveActivityAsDraftBtn = "Save as draft button";
        this.publishActivityBtn = null;
        this.loadAnother = null;
    }

    @Override
    public void clickBack() throws InterruptedException {
        BaseService.getDriver().findElement(By.xpath(BACK_ACTIVITY_BUTTON)).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
    }

    @Override
    public void clickCancel() throws InterruptedException {
        BaseService.getDriver().findElement(By.xpath(CANCEL_ACTIVITY_BUTTON)).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
    }

    @Override
    public int getNext() {
        return this.currentActivityIdx;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public void setNext(int i) {
        this.currentActivityIdx = i;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    @Override
    public void setParentIndex(int i) {
    }

    @Override
    public int getParentIndex() {
        return 0;
    }
}
