package hub.hub2_0.basePages.loyalty;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class Goal extends BaseSeleniumObjectHub2 {

    private static final String BACK = "link.back";
    private static final String SELECTED_CAMPAIGN = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR  + "=\"goal\"]/div/p[contains(text(),\"%s\")]";
    private static final String SELECTED_CAMPAIGN_BY_INDEX = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR  + "=\"goal\"][%d]";
    private static final String CREATE_CAMPAIGN_BTN ="goal.create.bundle";


    @SeleniumPath(value = SELECTED_CAMPAIGN, type = SeleniumPath.Type.BY_DYNAMIC_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String selectedCampaign ;

    @SeleniumPath(value = SELECTED_CAMPAIGN_BY_INDEX, type = SeleniumPath.Type.BY_DYNAMIC_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String selectedCampaignByIndex;
    @SeleniumPath(value = CREATE_CAMPAIGN_BTN, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String createCampaignBtn;


    public String getSelectedCampaign() {
        if(selectedCampaign != null)
            return selectedCampaign;
        else
            return selectedCampaignByIndex;
    }

    public void setSelectedCampaign(String campaignName) {
        this.selectedCampaign = String.format(SELECTED_CAMPAIGN ,campaignName);
        this.createCampaignBtn = "click create";
    }


    public void setSelectedCampaign(int campaignIndex) {
        this.selectedCampaignByIndex = String.format(SELECTED_CAMPAIGN_BY_INDEX,campaignIndex +1);
        this.createCampaignBtn = "click create";
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
