package hub.hub2_0.basePages.loyalty.gifts;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

import java.text.MessageFormat;

public class SelectDialog extends BaseSeleniumObjectHub2 {


    public static final String ON_TIME= "ontime";
    public static final String TIME = "time";
    public static final String CANCEL= "cancel";
    public static final String SAVE = "save";


    public static final String SELECT_RADIO ="//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"options.{0}\"]/div" ;
    public static final String INPUT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"time.value.input\"]/input" ;
    public static final String OPEN_UNITS_DROPDOWN ="//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"time.unit.dropdown\"]" ;
    public static final String UNITS ="//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"time.unit.dropdown.{0}\"]" ;
    private static final String SAVE_BTN = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"save.button\"]";
    private static final String CANCEL_BTN = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"cancel.button\"]";

    @SeleniumPath(value = SELECT_RADIO, type = SeleniumPath.Type.BY_DYNAMIC_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String selectRadio;
    @SeleniumPath(value = INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2)
    private String input;
    @SeleniumPath(value = OPEN_UNITS_DROPDOWN, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String openUnits;
    @SeleniumPath(value = UNITS, type = SeleniumPath.Type.BY_DYNAMIC_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2)
    private String units;

    @SeleniumPath(value = SAVE_BTN, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String saveBtn =SelectDialog.SAVE; //default value is save
    @SeleniumPath(value = CANCEL_BTN, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String cancelBtn;

    public String getSelectRadio() {
        return selectRadio;
    }

    public SelectDialog setSelectRadio(String selectRadio) {
        this.selectRadio = MessageFormat.format(SELECT_RADIO,selectRadio);
        return this;
    }

    public String getInput() {
        return input;
    }

    public SelectDialog setInput(String input) {
        this.input = input;
        return this;
    }

    public String getUnits() {
        return units;
    }

    public SelectDialog setUnits(String units) {
        this.openUnits = "click";
        this.units = units;
        return this;
    }

    public void clickSave() {
        this.saveBtn = SelectDialog.SAVE;
        this.cancelBtn =null;
    }

    public void clickCancel() {
        this.cancelBtn = SelectDialog.CANCEL;
        this.saveBtn = null;
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public static SelectDialog create() {
        return new SelectDialog();
    }
}
