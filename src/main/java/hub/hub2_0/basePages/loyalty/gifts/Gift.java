package hub.hub2_0.basePages.loyalty.gifts;


import hub.base.BaseService;
import hub.base.SeleniumPath;
import hub.hub2_0.basePages.loyalty.Deal;
import hub.hub2_0.basePages.loyalty.GlobalCondition;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.basePages.loyalty.cases.DealCase;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.dealActions.AmountOffDiscountAction;
import hub.hub2_0.common.actions.dealActions.PercentOffDiscountAction;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.services.loyalty.Gifts.GiftsGalleryService;

import java.util.ArrayList;
import java.util.List;

public class Gift extends Deal {

    private static final String ADD_GIFT = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"gifts-page.create-button\"]";
    private static final String DISPLAY_TAB = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"gift-tab.display\"]";

    @SeleniumPath(value = ADD_GIFT, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String addGift;

    @SeleniumPath(value = ACTIVITY_NAME, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS)
    private String activityName;
    @SeleniumPath(value = ACTIVITY_DESCRIPTION, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = ACTIVITY_DESCRIPTION)
    private String activityDescription;
    @SeleniumPath(value = "", type = SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<GlobalCondition> globalConditions;

    @SeleniumPath(value = DISPLAY_TAB, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String displayTab;

    ExpirationSelect expirationSelect;
    GiftDisplay giftDisplayl;



    @SeleniumPath(value = "", type = SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<ActivityCondition> conditions;

    //Split to cases
    @SeleniumPath(value = SPLIT_CASES_ICON, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_WAIT)
    private String splitCases;
    @SeleniumPath(value = "", type = SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    private List<Case> cases;
    @SeleniumPath(value = CANCEL_SPLIT_ICON, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String cancelSplit;

    @SeleniumPath(value = ENTIRE_TICKET_DISCOUNT, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String entireTicketDiscount;
    @SeleniumPath(value = SPECIFIC_ITEMS_DISCOUNT, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String specificItemsDiscount;
    @SeleniumPath(value = ADVANCED_DISCOUNT, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String advancedDiscount;

    @SeleniumPath(value = "", type = SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<OccurrencesCondition> occurrencesConditions;
    @SeleniumPath(value = "", type = SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<ActivityAction> actions; //array of automation actions action

    private GiftDisplay giftDisplay;

    @SeleniumPath(value = PUBLISH_ACTIVITY_BUTTON, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String publishActivityBtn;

    public String getDisplayTab() {
        return displayTab;
    }

    public void setDisplayTab(String displayTab) {
        this.displayTab = displayTab;
    }


    /*public GiftDisplay getGiftDisplay() {
        return giftDisplay;
    }*/

    public void setGiftDisplay(GiftDisplay giftDisplay) {
        this.giftDisplay = giftDisplay;
    }

    public Gift() {
        if (splitCases == null)
            this.objectPrefix = "";
    }

    @Override
    public void setActivityName(String activityName) {
        this.activityName = activityName;
    }

    @Override
    public void setActivityDescription(String activityDescription) {
        this.activityDescription = activityDescription;
    }

    @Override
    public String getActivityName() {
        return activityName;
    }

    @Override
    public String getActivityDescription() {
        return activityDescription;
    }

    ////**** Global Conditions methods ****////

    @Override
    public void setGlobalConditions(List<GlobalCondition> globalConditions) {
        for (GlobalCondition condition : globalConditions) {
            condition.clickGlobalCondition();
            addGlobalCondition(condition);
        }
    }

    @Override
    public List<GlobalCondition> getGlobalConditions() {
        if (globalConditions == null)
            globalConditions = new ArrayList<>();
        return globalConditions;
    }

    @Override
    public void addGlobalCondition(GlobalCondition condition) {
        condition.setNext(globalConditionsCounter);
        getGlobalConditions().add(condition);
        globalConditionsCounter++;
        condition.clickSave();
    }

    @Override
    public void setConditions(List<ActivityCondition> activityConditions) {
        for (ActivityCondition condition : activityConditions) {
            condition.clickAdd();
            addCondition(condition);
        }
    }

    @Override
    public List<ActivityCondition> getConditions() {
        if (conditions == null)
            conditions = new ArrayList<>();
        return conditions;
    }

    @Override
    public void addCondition(ActivityCondition condition) {
        condition.setNext(conditionsCounter);
        getConditions().add(condition);
        conditionsCounter++;
    }

    @Override
    public ActivityCondition replaceCondition(ActivityCondition currentCondition, ActivityCondition newCondition) {
        newCondition.setNext(currentCondition.getNext());
        getConditions().add(currentCondition.getNext(), newCondition);
        return newCondition;
    }

    @Override
    public void removeCondition(ActivityCondition condition) {
        getConditions().remove(condition);
        conditionsCounter--;
    }

    public ExpirationSelect getExpirationSelect() {
        return expirationSelect;
    }

    public void setExpirationSelect(ExpirationSelect expirationSelect) {
        this.expirationSelect = expirationSelect;
    }

    public GiftDisplay getGiftDisplay() {
        return giftDisplay;
    }



////**** Split to cases methods****////

    @Override
    public void clickSplitCases() {
        this.splitCases = "Split cases on";
    }

    @Override
    public void clickCancelSplit() {
        this.cancelSplit = "Cancel split";
        this.caseCounter = 0;
    }

    public int getCaseCounter() {
        return this.caseCounter;
    }

    @Override
    public void addCase(Case caze) {
        if (cases == null) {
            cases = new ArrayList<>();
        }
        caze.setNext(caseCounter);
        this.cases.add(caze);
        caseCounter++;
    }

    @Override
    public void setCases(List<Case> cases) {
        for (Case aCase : cases) {
            if (caseCounter > 0) {
                ((DealCase) aCase).clickAddCase();
            }
            addCase(aCase);
        }
    }

    @Override
    public List<Case> getCases() {
        if (cases == null) {
            cases = new ArrayList<>();
        }
        return cases;
    }

    @Override
    public Case defaultCase() {
        return getCases().get(0);
    }

    ////**** Activity Actions ****////

    public void clickEntireTicketDiscount() {
        this.entireTicketDiscount = "Clicked 'Give a discount on entire ticket'.";
        this.discountType = DiscountType.ENTIRE_TICKET;
    }

    public void clickSpecificItemsDiscount() {
        this.specificItemsDiscount = "Clicked 'Give discount on specific items or make them free'";
        this.discountType = DiscountType.SPECIFIC_ITEMS;

    }

    public void clickAdvancedDiscount() {
        this.advancedDiscount = "Clicked 'Advanced deal: 1+1, buy X get Y, free item for each $100, and more'";
        this.discountType = DiscountType.SPECIFIC_ITEMS_ADVANCED;
    }

    @Override
    public void setActions(List<ActivityAction> activityActions) {
        for (ActivityAction action : activityActions) {
            addAction(action);
        }
    }

    @Override
    public ActivityAction replaceAction(ActivityAction currentAction, ActivityAction newAction) {
        newAction.setNext(currentAction.getNext());
        getActions().add(currentAction.getNext(), newAction);
        return newAction;
    }

    @Override
    public void addAction(ActivityAction action) {
        action.setNext(actionsCounter);
        action = completeActionID(action);
        getActions().add(action);
        actionsCounter++;
    }

    private ActivityAction completeActionID(ActivityAction action) {
        switch (this.discountType) {
            case ENTIRE_TICKET:
                action.setIdSubString(Deal.DiscountType.ENTIRE_TICKET.toString());
                break;
            case SPECIFIC_ITEMS:
                if (action instanceof PercentOffDiscountAction || action instanceof AmountOffDiscountAction) {
                    action.setIdSubString(Deal.DiscountType.SPECIFIC_ITEMS.toString());
                } else {
                    action.setIdSubString(Deal.DiscountType.EMPTY_SUFFIX.toString());
                }
                break;
            case SPECIFIC_ITEMS_ADVANCED:
                if (action instanceof PercentOffDiscountAction || action instanceof AmountOffDiscountAction) {
                    action.setIdSubString(Deal.DiscountType.SPECIFIC_ITEMS_ADVANCED.toString());
                } else {
                    action.setIdSubString(Deal.DiscountType.ADVANCED.toString());
                }
                break;
        }
        return action;

    }

    @Override
    public void removeAction(ActivityAction action) {
        getActions().remove(action);
        actionsCounter--;
    }

    @Override
    public ActivityAction getAction(int actionIndex) {
        return getActions().get(actionIndex);
    }

    @Override
    public List<ActivityAction> getActions() {
        if (actions == null)
            actions = new ArrayList<>();
        return actions;
    }

    @Override
    public void setOccurrencesConditions(List<OccurrencesCondition> occurrencesConditions) {
        for (OccurrencesCondition condition : occurrencesConditions) {
            if (occurrencesConditionsCounter > 0) {
                condition.clickAddOccurrencesCondition();
            }
            addOccurrencesCondition(condition);
        }
    }

    private void addOccurrencesCondition(OccurrencesCondition occurrencesCondition) {
        occurrencesCondition.setNext(occurrencesConditionsCounter);
        getOccurrencesConditions().add(occurrencesCondition);
        occurrencesConditionsCounter++;
    }

    private List<OccurrencesCondition> getOccurrencesConditions() {
        if (occurrencesConditions == null)
            occurrencesConditions = new ArrayList<>();
        return occurrencesConditions;
    }


    ////**** Activity navigation buttons ****////

    @Override
    public void clickPublishActivityBtn() {
        this.publishActivityBtn = "Publish activity button";
    }


}