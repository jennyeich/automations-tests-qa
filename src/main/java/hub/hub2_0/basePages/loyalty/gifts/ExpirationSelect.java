package hub.hub2_0.basePages.loyalty.gifts;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class ExpirationSelect extends BaseSeleniumObjectHub2 {


    public static final String AFTER_TRIGGER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"globalConditions.giftActivationDeactivation.can.reedem.after.trigger\"]";
    public static final String UP_TO_TRIGGER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"globalConditions.giftActivationDeactivation.expiresAfter.trigger\"]";


    @SeleniumPath(value = AFTER_TRIGGER, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String afterTrigger;

    SelectDialog selectDialogAfter;

    @SeleniumPath(value = UP_TO_TRIGGER, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String upToTrigger;

    SelectDialog selectDialogUpTo;

    public ExpirationSelect setAfter(SelectDialog selectDialogAfter) {
        this.afterTrigger = "open after";
        this.selectDialogAfter = selectDialogAfter;
        return this;
    }

    public ExpirationSelect setUpTo(SelectDialog selectDialogUpTo) {
        this.upToTrigger =  "open up to";
        this.selectDialogUpTo = selectDialogUpTo;
        return this;
    }

    public String getAfterTrigger() {
        return afterTrigger;
    }

    public SelectDialog getSelectDialogAfter() {
        return selectDialogAfter;
    }


    public String getUpToTrigger() {
        return upToTrigger;
    }

    public SelectDialog getSelectDialogUpTo() {
        return selectDialogUpTo;
    }


    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public int getParentIndex() {
        return 0;
    }


}
