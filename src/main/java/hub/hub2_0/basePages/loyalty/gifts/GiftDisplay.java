package hub.hub2_0.basePages.loyalty.gifts;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.BaseService;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

import java.util.concurrent.TimeUnit;

public class GiftDisplay extends BaseSeleniumObjectHub2 {

    public static final String GIFT_NAME = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"gift.displayName\"]/input";
    public static final String GIFT_DESCRIPTION = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"gift.displayDescription\"]/input";
    private static final String DISPLAY_TAB = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"gift-tab.display\"]";

    private static String IMAGE_GALLERY =  "//input[@type='file']" ;
    public static final String IMAGE_UPLOAD = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"select.gift.image\"]";
    public static final String SELECT_EXIST_IMAGE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"imageGallery.image-0\"]";
    public static final String IMAGE_SAVE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"imageGallery.save\"]";
    public static final String CANCEL = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"imageGallery.cancel\"]";
    public static final String IMAGE_TO_SELECT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"imageGallery.image-{0}\"]";
    public static final String DELETE_IMAGE= "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"imageGallery.image-{0}.delete.button\"]";
    public static final String DELETE_CONFIRMATION_WINDOW= "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"imageGallery.image-{0}.delete.delete\"]";


    @SeleniumPath(value = DISPLAY_TAB, type = SeleniumPath.Type.BY_GENERATE_XPATH_IMAGE_SELECT, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String displayTab;

    @SeleniumPath(value = GIFT_NAME, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS)
    private String giftName;

    @SeleniumPath(value = GIFT_DESCRIPTION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS)
    private String giftDescription;

    @SeleniumPath(value = IMAGE_UPLOAD, type = SeleniumPath.Type.BY_GENERATE_XPATH_IMAGE_SELECT, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String imageUpload;

    @SeleniumPath(value = SELECT_EXIST_IMAGE, type = SeleniumPath.Type.BY_GENERATE_XPATH_IMAGE_SELECT, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String selectExistImage;

    @SeleniumPath(value = IMAGE_SAVE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String imageSave;

    @SeleniumPath(value = CANCEL, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String cancel;

    @SeleniumPath(value = IMAGE_TO_SELECT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String imageToSelect;

    @SeleniumPath(value = DELETE_IMAGE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String deleteImage;

    @SeleniumPath(value = DELETE_CONFIRMATION_WINDOW, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String deleteConfirmationWindow;


    public GiftDisplay setGiftName(String giftName) {
        this.giftName = giftName;
        return this;
    }
    public GiftDisplay setGiftDescription(String giftDescription) {
        this.giftDescription = giftDescription;
        return this;
    }

    public String getDisplayTab() {
        return displayTab;
    }

    public void setDisplayTab(String displayTab) {
        this.displayTab = displayTab;
    }

    public String getImageUpload(){
        return imageUpload;
    }

    public void setImageUpload(String imageUpload) throws InterruptedException {
        TimeUnit.SECONDS.sleep(5);
        this.imageUpload = imageUpload;
    }

    public String getSelectExistImage() {
        return selectExistImage;
    }

    public void setSelectExistImage(String selectExistImage) {
        this.selectExistImage = selectExistImage;
    }

    public String getImageSave() {
        return imageSave;
    }

    public void setImageSave(String imageSave) {
        this.imageSave = imageSave;
    }

    public String getCancel() {
        return cancel;
    }

    public void setCancel(String cancel) {
        this.cancel = cancel;
    }

    public String getImageToSelect() {
        return imageToSelect;
    }

    public void setImageToSelect(String imageToSelect) {

        this.imageToSelect = imageToSelect;
    }

    public String getDeleteImage() {
        return deleteImage;
    }

    public void setDeleteImage(String deleteImage) {
        this.deleteImage = deleteImage;
    }

    public String getDeleteConfirmationWindow() {
        return deleteConfirmationWindow;
    }

    public void setDeleteConfirmationWindow(String deleteConfirmationWindow) {
        this.deleteConfirmationWindow = deleteConfirmationWindow;
    }

    @Override
    public int getParentIndex() {
        return 0;
    }


    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public String getGiftName() {
        return giftName;
    }

    public String getGiftDescription() {
        return giftDescription;
    }


}
