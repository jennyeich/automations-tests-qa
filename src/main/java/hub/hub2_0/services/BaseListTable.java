package hub.hub2_0.services;

import hub.hub2_0.common.enums.ItemOperation;
import hub.hub2_0.common.enums.SortType;
import hub.hub2_0.services.loyalty.BaseHub2Service;
import hub.hub2_0.services.loyalty.ListTable;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.text.MessageFormat;



public class BaseListTable extends BaseHub2Service implements ListTable {

    private static  final String SORT_BTN = "//*[@" + HUB2_AUTOMATION_STR +"=\"" + "{0}sort-sets\"]" ;

    private static String itemXpath;
    private static String itemSideMenuXpath;
    private static String itemMenuStartStopXpath;
    private static String itemMenuDetailsXpath;
    private static String itemMenuAnalysis;
    private static String sortBtnXpath;


    public static void setItemXpath(String locator,String campaignIdx,String rowIdx ) {
        itemXpath =  MessageFormat.format(locator,campaignIdx,rowIdx);
    }

    public static void setItemXpath(String locator,String rowIdx ) {MessageFormat.format(locator,rowIdx);
        itemXpath =  MessageFormat.format(locator,rowIdx);
    }

    public static void setItemSideMenuXpath(String locator,String campaignIdx,String rowIdx ) {
        itemSideMenuXpath = MessageFormat.format(locator,campaignIdx,rowIdx);
    }

    public static void setItemSideMenuXpath(String locator,String rowIdx ) {
        itemSideMenuXpath = MessageFormat.format(locator,rowIdx);
    }

    public static void setItemMenuStartStopXpath(String locator,String campaignIdx,String rowIdx ) {
        itemMenuStartStopXpath =MessageFormat.format(locator,campaignIdx,rowIdx);
    }

    public static void setItemMenuStartStopXpath(String locator,String rowIdx ) {
        itemMenuStartStopXpath =MessageFormat.format(locator,rowIdx);
    }

    public static void setSortBtnXpath(String prefix ) {
        sortBtnXpath = MessageFormat.format(SORT_BTN,prefix);
    }

    public static void setItemSideMenuDetailsXpath(String locator,String rowIdx) {
        itemMenuDetailsXpath = MessageFormat.format(locator,rowIdx);
    }


    public static void setItemMenuDetailsXpath(String locator,String campaignIdx,String rowIdx) {
        itemMenuDetailsXpath = MessageFormat.format(locator,campaignIdx,rowIdx);
    }

    public static void setItemMenuAnalysis(String locator,String campaignIdx,String rowIdx) {
        itemMenuAnalysis = MessageFormat.format(locator,campaignIdx,rowIdx);
    }

    public static void sideMenuClick() throws Exception {
        Actions actions = new Actions(getDriver());
        //click on side menu
        try {
            WebElement activitySideMenuElement = getDriver().findElement(By.xpath(itemSideMenuXpath));
            actions.moveToElement(activitySideMenuElement);
            actions.click().build().perform();
        } catch  (Exception e) {
            log4j.info("Failed to click on side menu, Exit test!");
            throw new Exception("Failed to click on side menu, Exit test!\n",e);
        }
    }

    private static void clickOnStartStopAction(ItemOperation itemAction ) throws Exception {
        WebElement StartStopActionElement = getDriver().findElement(By.xpath(itemMenuStartStopXpath));
        String currentItemStatus =StartStopActionElement.findElement(By.cssSelector(".content")).getText();
        if (currentItemStatus.equals(itemAction.toString())) {
            StartStopActionElement.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            log4j.info("changed state to :<" + itemAction.toString()+ ">");
        } else {
            log4j.info("item menu expected to be:<" + (itemAction.equals(ItemOperation.ACTIVATE)? ItemOperation.ACTIVATE: ItemOperation.STOP) + "> But instead it:<" + currentItemStatus + ">");
            throw new Exception("item menu expected to be:<" + (itemAction.equals(ItemOperation.ACTIVATE)? ItemOperation.ACTIVATE: ItemOperation.STOP) + "> But instead it:<" + currentItemStatus + ">");
        }
    }

    public static void executeOperationOnListItem(ItemOperation itemAction) throws Exception {
        WebElement subMenuElement;
        sideMenuClick();
        switch(itemAction){
            case ACTIVATE:
            case STOP:
                clickOnStartStopAction(itemAction);
                break;
            case DETAILS:
                 subMenuElement = getDriver().findElement(By.xpath(itemMenuDetailsXpath));
                try {
                    subMenuElement.click();
                    Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
                    log4j.info("Click on side menu :<" + itemAction.toString() + ">");
                } catch (Exception e){
                    log4j.info("Failed perform action:" + itemAction.toString());
                    throw new Exception("Failed perform action:" + itemAction.toString());
                }
                break;
        }

    }


    public static void clickOnItem(String ItemNameTofind)
    {
        WebElement giftElem = getDriver().findElement(By.xpath(itemXpath));
        giftElem.click();
    }

    public  boolean isItemExist(String itemNameTofind)
    {
        boolean isExists =false;
        try {
            WebElement activityElement = findElementByDiffrentAttribute("title",itemNameTofind);
            isExists =true;
        } catch (Exception e){}// do nothing , just catch the exception and return back that activity wasn't found
        return isExists;
    }


    public  static void sortItems(SortType sortType ) throws InterruptedException {
        // Get the sort state
        String classes =getDriver().findElement(By.className("sort-icon")).getAttribute("class");
        if (sortType.equals(SortType.LAST_CREATE_UP)) {
            if (classes.contains("up")) {
                sortClick();
                log4j.info("Click on Sort button:" +  sortType.name() );
            }
        }
        else if (sortType.equals(SortType.LAST_CREATE_DOWN)) {
            if (classes.contains("down")) {
                sortClick();
                log4j.info("Click on Sort button:" + sortType.name());
            }
        }

    }

    private  static void sortClick() throws InterruptedException {
        WebElement sortElement = getDriver().findElement(By.xpath(sortBtnXpath));
        sortElement.click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
    }

}
