package hub.hub2_0.services.loyalty.Gifts;

import hub.base.BaseService;
import hub.hub2_0.Hub2Constants;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import utils.AutomationException;

import java.io.File;
import java.text.MessageFormat;
import java.util.List;

import static hub.base.BaseService.*;
import static hub.base.BaseService.getDriver;
import static hub.hub2_0.basePages.loyalty.Activity.BACK_ACTIVITY_BUTTON;

public class GiftsGalleryService {

    public static   final Logger log4j = Logger.getLogger(GiftsGalleryService.class);
    private static  String IMAGE_GALLERY =  "//input[@type='file']" ;
    public static final String DISPLAY_TAB = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"gift-tab.display\"]";
    public static final String IMAGE_UPLOAD = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"select.gift.image\"]";
    public static final String IMAGE_SAVE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"imageGallery.save\"]";
    public static final String CANCEL = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"imageGallery.cancel\"]";
    public static final String IMAGE_TO_SELECT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"imageGallery.image-{0}\"]";
    public static final String DELETE_IMAGE= "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"imageGallery.image-{0}.delete.button\"]";
    public static final String DELETE_CONFIRMATION_WINDOW= "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"imageGallery.image-{0}.delete.delete\"]";
    public static final String PUBLISH_ACTIVITY_BUTTON =  "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + " = \"activity.save\"]";


    private static int getFileIdx(String fileName)
    {
        int index=0;
        List<WebElement> ImageGallery = getDriver().findElements(By.className("single-image"));
        for ( WebElement image: ImageGallery) {
            String imageName = image.findElement(By.className("single-image-name")).getText();
            // String imageName = image.getText();
            if (imageName.equals(fileName) ) {
                return index;
            }
            index++;
        }
        return -1; // didn't found image name
    }


    public static void uploadFile(String giftName, String imageName) throws InterruptedException, AutomationException {
        getIntoImageGallery(giftName);
        WebElement element = getDriver().findElement(By.xpath(IMAGE_GALLERY));
        if (getDriver() instanceof RemoteWebDriver) {
            LocalFileDetector detector = new LocalFileDetector();
            File localFile = detector.getLocalFile(resourcesDir + imageName);
            if (element instanceof RemoteWebElement)
                ((RemoteWebElement) element).setFileDetector(detector);
            String absolutePath = localFile.getAbsolutePath();
            element.sendKeys(absolutePath);
        } else {
            element.sendKeys(resourcesDir + "cat.jpg");
        }
        Thread.sleep(3000);//wait 3 sec until file loaded
        getDriver().findElement(By.xpath(IMAGE_SAVE)).click();
        getDriver().findElement(By.xpath(PUBLISH_ACTIVITY_BUTTON)).click();
        Thread.sleep(2000);
    }

    public static void uploadFileInCreateGift(String imageName) throws Exception {
        WebElement element = getDriver().findElement(By.xpath(IMAGE_GALLERY));
        if (getDriver() instanceof RemoteWebDriver) {
            LocalFileDetector detector = new LocalFileDetector();
            File localFile = detector.getLocalFile(resourcesDir + imageName);
            if (element instanceof RemoteWebElement)
                ((RemoteWebElement) element).setFileDetector(detector);
            String absolutePath = localFile.getAbsolutePath();
            element.sendKeys(absolutePath);
        } else {
            element.sendKeys(resourcesDir + "cat.jpg");
        }
        Thread.sleep(3000);//wait 3 sec until file loaded
        getDriver().findElement(By.xpath(IMAGE_SAVE)).click();
        selectImageInCreateGift(imageName);
        Thread.sleep(2000);
    }

    public static void selectImageInCreateGift(String imageName) throws Exception {
        int fileIdx = getFileIdx(imageName);
        if (fileIdx<0)
            throw new Exception("Failed to find image name:<" + imageName +"> in the gallery");
        String imageXpath = MessageFormat.format(IMAGE_TO_SELECT,fileIdx);
        getDriver().findElement(By.xpath(imageXpath)).click();
        getDriver().findElement(By.xpath(IMAGE_SAVE)).click();
        getDriver().findElement(By.xpath(PUBLISH_ACTIVITY_BUTTON)).click();
        Thread.sleep(2000);
    }

    public static void selectImage(String giftName,String imageName) throws Exception {
        getIntoImageGallery(giftName);
        int fileIdx = getFileIdx(imageName);
        if (fileIdx<0)
            throw new Exception("Failed to find image name:<" + imageName +"> in the gallery");
        String imageXpath = MessageFormat.format(IMAGE_TO_SELECT,fileIdx);
        getDriver().findElement(By.xpath(imageXpath)).click();
        getDriver().findElement(By.xpath(IMAGE_SAVE)).click();
        getDriver().findElement(By.xpath(PUBLISH_ACTIVITY_BUTTON)).click();
        Thread.sleep(2000);
    }

    public static void deleteImage(String giftName,String imageToDelete) throws Exception {
        getIntoImageGallery(giftName);
        int fileIdx = findImageIdx(imageToDelete);
        String imageXpath = MessageFormat.format(DELETE_IMAGE,fileIdx);
        getDriver().findElement(By.xpath(imageXpath)).click();
        Thread.sleep(2000);
        String deleteConfirmationXpath = MessageFormat.format(DELETE_CONFIRMATION_WINDOW,fileIdx);
        getDriver().findElement(By.xpath(deleteConfirmationXpath)).click();
        Thread.sleep(2000);
        getDriver().findElement(By.xpath(IMAGE_SAVE)).click();
        Thread.sleep(2000);
        getDriver().findElement(By.xpath(PUBLISH_ACTIVITY_BUTTON)).click();
        Thread.sleep(2000);
    }

    private static int findImageIdx(String imageName ) throws Exception {
        int fileIdx = getFileIdx(imageName);
        if (fileIdx<0)
            throw new Exception("Failed to find image name:<" + imageName +"> in the gallery");
        return fileIdx;
    }

    public static boolean isImageExists(String giftName,String imageName) throws Exception {
        boolean isExists = true;
        getIntoImageGallery(giftName);
        try {
            int fileIdx = findImageIdx(imageName);
        }catch (Exception e){
            isExists = false;  // catch the exception and
        }
        getDriver().findElement(By.xpath(CANCEL)).click();
        BaseService.getDriver().findElement(By.xpath(BACK_ACTIVITY_BUTTON)).click();
        return isExists;
    }

    private static void imageUploadClick()
    {
        WebElement imageUpload = getDriver().findElement(By.xpath(IMAGE_UPLOAD));
        Actions action = new Actions(BaseService.getDriver());
        action.moveToElement(imageUpload);
        action.click(imageUpload).perform();
    }

    private static void  getIntoImageGallery(String giftName ) throws InterruptedException, AutomationException {
        GiftService.clickOnGift(giftName);
        Thread.sleep(2000);
        getDriver().findElement(By.xpath(DISPLAY_TAB)).click();
        imageUploadClick();
    }

}
