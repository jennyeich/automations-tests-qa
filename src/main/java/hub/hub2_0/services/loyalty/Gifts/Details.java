package hub.hub2_0.services.loyalty.Gifts;

public class Details {
    String created;
    String firstActivated;
    String lastModified;


    public void setCreated(String created) {
        this.created = created;
    }

    public void setFirstActivated(String firstActivated) {
        this.firstActivated = firstActivated;
    }

    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    public String getCreated() {
        return created;
    }

    public String getFirstActivated() {
        return firstActivated;
    }

    public String getLastModified() {
        return lastModified;
    }
}
