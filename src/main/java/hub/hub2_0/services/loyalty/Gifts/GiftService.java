package hub.hub2_0.services.loyalty.Gifts;

import hub.hub1_0.services.Navigator;
import hub.hub2_0.common.enums.ItemOperation;
import hub.hub2_0.common.enums.SortType;
import hub.hub2_0.services.BaseListTable;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import utils.AutomationException;

import static hub.hub2_0.services.loyalty.LoyaltyService.resolveIdx;

public class GiftService extends BaseListTable {
    public static   final Logger log4j = Logger.getLogger(GiftService.class);
    private static  final String GIFTS_PAGE_CAMPAIGN = "gifts-page.gift.";
    private static  String GIFT = "//*[@" + HUB2_AUTOMATION_STR +"=\"gifts-page.campaign.row.{0}\"]";
    private static  String GIFT_SIDE_MENU = "//*[@" + HUB2_AUTOMATION_STR + "=\""+ GIFTS_PAGE_CAMPAIGN + "{0}.gift-context-menu\"]";
    private static  String GIFT_SIDE_MENU_DETAILS =  "//*[@" + HUB2_AUTOMATION_STR + "=\"gifts-page.activity-row.{0}.contextual-menu.0\"]";
    private static  String GIFT_DETAILS_WINDOW = "//*[@" + HUB2_AUTOMATION_STR + "=\"modal.gift.details\"]" ;
    private static  String GIFT_SIDE_MENU_ANALYSIS =  "//*[@" + HUB2_AUTOMATION_STR + "=\"gifts-page.activity-row.{0}.contextual-menu.1\"]";
    private static  String NUMBER_OF_SENT_GIFTS = "//span[contains(text(), \"Number of gifts that were sent to members: {0} gift\")]";
    private static  String GIFT_SIDE_MENU_START_STOP =  "//*[@" + HUB2_AUTOMATION_STR + "=\"gifts-page.activity-row.{0}.contextual-menu.2\"]";
    private static  String SEARCH = "//*[@" + HUB2_AUTOMATION_STR + "=\"gifts-page.search-box\"]" ;
    private static  String GIFT_PAGE_PREFIX  = "gifts-page.";

    public static void executeOperation(String giftName, ItemOperation operation) throws Exception {
        Navigator.loyaltyTabPageAndClickOnGiftPage();
        try {
            initXpath(giftName);
            executeOperationOnListItem(operation);
        }catch (Exception e) {
            throw new Exception("Failed to click on gift name:<" + giftName + "> , exit test!!!\n" , e );
        }
    }

    public static void sortGift(SortType sortType) throws AutomationException, InterruptedException {
        Navigator.loyaltyTabPageAndClickOnGiftPage();
        setSortBtnXpath(GIFT_PAGE_PREFIX);
        sortItems(sortType);
    }

    public static void clickOnGift(String giftName) throws AutomationException, InterruptedException {
        Navigator.loyaltyTabPageAndClickOnGiftPage();
        initXpath(giftName);
        clickOnItem(giftName);
    }

    public static void clickOnGiftAnalysis(String giftName) throws AutomationException, InterruptedException {
        Navigator.loyaltyTabPageAndClickOnGiftPage();
        initXpath(giftName);
        getDriver().findElement(By.xpath(GIFT_SIDE_MENU_ANALYSIS)).click();
    }

    public static WebElement numOfSentGift(String numOfSentGift) throws Exception {
        String NumOfSentGifts = String.valueOf(getDriver().findElement(By.xpath(NUMBER_OF_SENT_GIFTS)));
        return driver.findElement(By.xpath(NumOfSentGifts.replaceAll("0", numOfSentGift)));

    }

    public static Details getGiftDetails(String giftName) throws Exception {
        Details details = new Details();
        Navigator.loyaltyTabPageAndClickOnGiftPage();
        initXpath(giftName);
        executeOperationOnListItem(ItemOperation.DETAILS);
        WebElement giftDetails = getDriver().findElement(By.xpath(GIFT_DETAILS_WINDOW));
        WebElement gotItButton = giftDetails.findElement(By.className("actions"));
        String content = giftDetails.findElement(By.className("content")).getText();
        String created = content.substring(( content.indexOf(hub.hub2_0.common.enums.Details.CREATED.toString()) + hub.hub2_0.common.enums.Details.CREATED.toString().length() ), content.indexOf(hub.hub2_0.common.enums.Details.FIRST_ACTIVATED.toString()));
        String firstActivated = content.substring(( content.indexOf(hub.hub2_0.common.enums.Details.FIRST_ACTIVATED.toString()) + hub.hub2_0.common.enums.Details.FIRST_ACTIVATED.toString().length() ), content.indexOf(hub.hub2_0.common.enums.Details.LAST_MODIFIED.toString()));
        String lastModified = content.substring(( content.indexOf(hub.hub2_0.common.enums.Details.LAST_MODIFIED.toString()) + hub.hub2_0.common.enums.Details.LAST_MODIFIED.toString().length() ), content.length());
        details.setCreated(created);
        details.setFirstActivated(firstActivated);
        details.setLastModified(lastModified);
        gotItButton.click();
        return details;
    }


    public static void search(String searchInput)
    {
        Actions actions = moveToSearch();
        actions.click();
        actions.sendKeys(searchInput);
        actions.build().perform();
    }

    public static void clearSearch()
    {
        Actions actions = moveToSearch();
        actions.click();
        actions.sendKeys(Keys.CONTROL,"a",Keys.DELETE);
        actions.build().perform();
    }

    private static Actions moveToSearch()
    {
        WebElement searchElement = getDriver().findElement(By.xpath(SEARCH));
        Actions actions = new Actions(driver);
        actions.moveToElement(searchElement);
        return actions;
    }

    private static void initXpath(String giftName)
    {
        WebElement activityElement = findElementByDiffrentAttribute("title",giftName);
        log4j.info("Find Gift:<" + giftName + ">");
        String row = getRow(activityElement);
        setItemXpath(GIFT,row);
        setItemSideMenuXpath(GIFT_SIDE_MENU,row);
        setItemMenuStartStopXpath(GIFT_SIDE_MENU_START_STOP,row);
        setItemSideMenuDetailsXpath(GIFT_SIDE_MENU_DETAILS,row);
    }

    private static String getRow(WebElement activityElement) {
        WebElement parent = activityElement.findElement(By.xpath(".."));
        String attributeVal = parent.getAttribute(HUB2_AUTOMATION_STR);
        return(resolveIdx(attributeVal));
    }
}
