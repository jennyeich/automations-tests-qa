package hub.hub2_0.services.loyalty;

import com.beust.jcommander.internal.Lists;
import hub.base.BaseSeleniumObject;
import hub.base.BaseSeleniumObjectHub2;
import hub.base.BaseService;
import hub.common.objects.benefits.IAsset;
import hub.common.objects.common.PermissionLevel;
import hub.common.objects.common.Tags;
import hub.hub1_0.services.Navigator;
import hub.hub2_0.basePages.loyalty.*;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.basePages.loyalty.gifts.Gift;
import hub.hub2_0.basePages.loyalty.templates.SaveProductTemplate;
import hub.hub2_0.basePages.loyalty.templates.TemplateCategories;
import hub.hub2_0.basePages.loyalty.templates.TemplateVerticals;
import hub.hub2_0.basePages.loyalty.templates.TemplatesLibrary;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.builders.ActionsBuilder;
import hub.hub2_0.common.builders.DealBuilder;
import hub.hub2_0.common.builders.GiftsBuilder;
import hub.hub2_0.common.builders.RuleBuilder;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.enums.ActivityTrigger;
import hub.hub2_0.common.enums.ActivityType;
import hub.services.AcpService;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.AutomationException;

import java.util.List;
import java.util.concurrent.TimeUnit;


public class LoyaltyService extends BaseHub2Service {

    public static final String AUTOMATION_RULE_PREFIX = "automation_";
    public static final String AUTOMATION_DEAL_PREFIX = "smartAsset_";
    private static final String PUBLISH_BUTTON = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"" + Rule.PUBLISH_ACTIVITY_BUTTON + "\"]";
    public static final Logger log4j = Logger.getLogger(LoyaltyService.class);

    private static RuleBuilder ruleBuilder = new RuleBuilder();
    private static DealBuilder dealBuilder = new DealBuilder();
    private static GiftsBuilder giftsBuilder = new GiftsBuilder();


    public static void createBox(Box box) throws Exception {
        WebElement backButtonElement = getElementIfPresent(By.xpath(Rule.BACK_ACTIVITY_BUTTON));
        if (backButtonElement != null) {
            refresh();
            backButtonElement = getElementIfPresent(By.xpath(Rule.BACK_ACTIVITY_BUTTON));
            backButtonElement.click();
        }
        Navigator.loyaltyTabPage();
        fillSeleniumObject(box);
        waitForLoad();
        log4j.info("New Box was created successfully :  " + box.getBoxName());
    }

    public static void clickAddActivity(Activity activity) throws Exception {
        Navigator.loyaltyTabPage();
        TimeUnit.SECONDS.sleep(2);
        Box box = null;
        if (activity instanceof Rule) {
            box = ruleBuilder.buildClickAddActivity(ActivityType.Rule);
        } else if (activity instanceof Deal) {
            box = ruleBuilder.buildClickAddActivity(ActivityType.Deal);
        }
        fillSeleniumObject(box);
    }



    public static void createGift(Gift gift) throws Exception {

        Navigator.loyaltyTabPageAndClickOnGiftPage();
        fillSeleniumObject(gift);
        TimeUnit.SECONDS.sleep(2);

    }

    public static void returnToCampaignCenterPage() throws Exception {

        Navigator.loyaltyTabPageAndClickOnCampaignCenterPage();
        TimeUnit.SECONDS.sleep(2);

    }


    public static void createActivityInBox(Activity activity) throws Exception {
        WebElement backButtonElement = getElementIfPresent(By.xpath(Rule.BACK_ACTIVITY_BUTTON));
        if (backButtonElement != null) {
            refresh();
            backButtonElement = getElementIfPresent(By.xpath(Rule.BACK_ACTIVITY_BUTTON));
            backButtonElement.click();
        } else {
            backButtonElement = getElementIfPresent(By.xpath(TemplatesLibrary.BACK_BUTTON));
            if (backButtonElement != null) {
                refresh();
            }
        }
        clickAddActivity(activity);
        TemplatesLibrary.clickStartFromScratch();
        fillSeleniumObject((BaseSeleniumObject) activity);
        waitForLoad();
        log4j.info("New Activity was created successfully :  " + activity.getActivityName());
    }

    public static void createRuleFromTemplateInBox(Activity activity, String templateName) throws Exception {
        Navigator.loyaltyTabPage();
        Box box = ruleBuilder.buildClickAddActivity(ActivityType.Rule);
        fillSeleniumObject(box);
        selectTemplateAndCreateActivity(activity, templateName);
    }

    public static void openRuleTemplateLibrary() throws Exception {
        Navigator.loyaltyTabPage();
        Box box = ruleBuilder.buildClickAddActivity(ActivityType.Rule);
        fillSeleniumObject(box);
    }

    public static void selectTemplateAndCreateActivity(Activity activity, String templateName) throws Exception {
        TemplatesLibrary.clickStartFromTemplate(templateName);
        fillSeleniumObject((BaseSeleniumObject) activity);
        waitForLoad();
        log4j.info("New Activity was created successfully :  " + activity.getActivityName());
    }

    public static void createDealFromTemplateInBox(Activity activity, String templateName) throws Exception {
        Navigator.loyaltyTabPage();
        Box box = dealBuilder.buildClickAddActivity(ActivityType.Deal);
        fillSeleniumObject(box);
        selectTemplateAndCreateActivity(activity, templateName);
    }

    public static void openDealTemplateLibrary() throws Exception {
        Navigator.loyaltyTabPage();
        Box box = dealBuilder.buildClickAddActivity(ActivityType.Deal);
        fillSeleniumObject(box);
    }

    /* this method return the total box exists in the loyalty page*/
    public static int getNumOfExistsBoxes() throws Exception {
        Navigator.loyaltyTabPage();
        waitForLoad();
        int boxesNumber = getDriver().findElements(By.className("add-activity")).size();
        return boxesNumber;
    }

    public static String getRuleID(String activityName) throws Exception {
        String ruleId = getActivityURL(activityName, AUTOMATION_RULE_PREFIX);
        log4j.info("RuleID : " + ruleId);
        return ruleId;
    }

    public static String getDealID(String activityName) throws Exception {
        String dealId = getActivityURL(activityName, AUTOMATION_DEAL_PREFIX);
        log4j.info("DealID : " + dealId);
        return dealId;
    }

    public static String getActivityURL(String activityName, String activityPrefix) throws Exception {

        Navigator.loyaltyTabPage();

        Thread.sleep(3000);
        String autoNumber = "Rule not found";
        try {
            WebElement activityElement = getFirstActivityElement(activityName);
            autoNumber = selectActivityAndGetActivityId(activityElement, activityPrefix);
        } catch (TimeoutException e) {
            //try again
            Thread.sleep(2000);
            WebElement activityElement = getFirstActivityElement(activityName);
            autoNumber = selectActivityAndGetActivityId(activityElement, activityPrefix);
        }
        return autoNumber;

    }

    private static WebElement getActivityElement(String activityName) {
        String xPath = "//*[contains(text(),'" + activityName + "')]";
        WebElement automation = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
        return automation;
    }

    private static WebElement getFirstActivityElement(String activityName) {
        String xPath = "//*[contains(@title,'" + activityName + "')]";
        List<WebElement> elements = getDriverWait().until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xPath)));
        return elements.get(0);
    }

    public static WebElement findBoxByName(String boxName) {
        WebElement boxElement = findElementByDiffrentAttribute("value", boxName);
        return boxElement;
    }

    public static boolean isBoxExists(String boxName) {
        boolean isExists = false;
        try {
            WebElement boxElement = findElementByDiffrentAttribute("value", boxName);
            isExists = true;
        } catch (Exception e) {
            // do nothing , just catch the exception and return back that box wasn't found
        }
        return isExists;
    }

    private static String selectActivityAndGetActivityId(WebElement activityElement, String activity_prefix) throws InterruptedException {
        activityElement.click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        String autoNumber = getDriver().findElement(By.xpath("//input[starts-with(@" + HUB2_AUTOMATION_STR + ",'" + activity_prefix + "')]")).getAttribute(HUB2_AUTOMATION_STR);
        return autoNumber;
    }

    public static void clickPublish() throws InterruptedException {
        WebElement publish = getDriver().findElement(By.xpath(PUBLISH_BUTTON));
        publish.click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
    }

    public static String findBoxIndex(String boxName) {
        WebElement boxElement = LoyaltyService.findBoxByName(boxName);
        WebElement parent = boxElement.findElement(By.xpath(".."));
        String attributeVal = parent.getAttribute(HUB2_AUTOMATION_STR);
        return (resolveIdx(attributeVal));

    }

    /* this method resolve the indexes from locator*/
    public static String resolveIdx(String locator) {
        return locator.replaceAll("[^?0-9]+", " ").trim();
    }

    public static void clickOnActivity(String activityNameTofind) throws Exception {
        String xpath = "//*[contains(text(),'" + activityNameTofind + "')]";
        try {
            WebElement activityElement = getDriver().findElement(By.xpath(xpath));
            activityElement.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } catch (Exception e) {
            throw new Exception("Can't find activity name: <" + activityNameTofind + "> , exit test!!!\n", e);
        }
    }


    public static void updateActivity(Activity updateActivity) throws Exception {
        fillSeleniumObject((BaseSeleniumObject) updateActivity);
        waitForLoad();
    }

    public static void navigateToNewRule() throws Exception {

        LoyaltyService.createActivityInBox(new Rule());
    }

    public static void navigateToNewDeal() throws Exception {
        LoyaltyService.createActivityInBox(new Deal());
    }

    public static boolean isElementExist(String automationId) {
        List<WebElement> elementList = LoyaltyService.findElementsByAutomationId(automationId);
        return elementList.size() > 0;
    }


    private static WebElement getTemplateElement(String templateName) {
        String xPath = "//a[contains(text(),'" + templateName + "')]";
        WebElement template = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
        return template;
    }


    public static String getTemplateURL(IAsset asset) throws Exception {

        Thread.sleep(3000);
        String templateNumber;
        try {
            WebElement template = getTemplateElement(asset.getTitle());
            templateNumber = selectTemplateAndGetTemplateId(template);
        } catch (TimeoutException e) {
            //try again
            Thread.sleep(2000);
            WebElement template = getTemplateElement(asset.getTitle());
            templateNumber = selectTemplateAndGetTemplateId(template);
        }
        return templateNumber;

    }


    private static String selectTemplateAndGetTemplateId(WebElement template) throws InterruptedException {
        template.click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        String templateNumber = concatNumber();
        return templateNumber;
    }

    private static String concatNumber() throws InterruptedException {
        String url = getDriver().getCurrentUrl();
        String autoNumber = url.substring(url.lastIndexOf("/") + 1, url.indexOf("?"));
        return autoNumber;
    }


    /**
     * Create product templates according to the verticals - template for each vertical and one for all verticals
     */
    public static void initProductTemplates(Box box, ActivityTrigger trigger) throws Exception {
        //give permission to create product templates

        AcpService.setPermissionTag(Tags.HUB2_TEMPLATE_PRODUCT.toString(), PermissionLevel.ALLOW.toString());
        Navigator.loyaltyTabPage();

        int verticals = TemplateVerticals.values().length;
        for (int i = 0; i < verticals; i++) {
            TemplateVerticals vertical = TemplateVerticals.values()[i];
            String timestamp = String.valueOf(System.currentTimeMillis());
            String tag = "tag_" + timestamp.toString();
            Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, trigger.toString(), "template for vertical " + vertical.name());
            ActivityAction activityAction = ActionsBuilder.buildTagMemberAction(tag);
            Activity activity = ruleBuilder.buildActivity(trigger.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(activityAction));

            SaveProductTemplate productTemplate = new SaveProductTemplate();

            productTemplate.setName(TemplatesLibrary.PRODUCT_TEMPLATE_PREFIX + vertical.name());
            if ((i % 2) == 0)
                productTemplate.setCategory(TemplateCategories.Summer.toString());
            else
                productTemplate.setCategory(TemplateCategories.Winter.toString());

            productTemplate.setDescription("description_" + vertical.name());
            productTemplate.setVerticals(vertical.getValue());
            activity.clickSaveAsProductTemplate(productTemplate);
            createActivityInBox(activity);
            activity.clickCancel(); // dont save the rule.
            log4j.info("New activity was saved as product template called: " + productTemplate.getName());
        }
        createProductTemplateWithAllVerticals(box, trigger, "");

    }

    public static void initProductTemplates(List<Activity> productTemplates) throws Exception {
        //give permission to create product templates

        AcpService.setPermissionTag(Tags.HUB2_TEMPLATE_PRODUCT.toString(), PermissionLevel.ALLOW.toString());
        Navigator.loyaltyTabPage();

        for (int i = 0; i < productTemplates.size(); i++) {
            createActivityInBox(productTemplates.get(i));
            productTemplates.get(i).clickCancel();
            log4j.info("New activity was saved as product template called: " + productTemplates.get(i).getSaveProductTemplate().getName());
        }
    }

    private static void createProductTemplateWithAllVerticals(Box box, ActivityTrigger trigger, String suffix) throws Exception {
        String timestamp = String.valueOf(System.currentTimeMillis());
        String tag = "tag_" + timestamp.toString();
        ActivityAction activityAction = ActionsBuilder.buildTagMemberAction(tag);
        createProductTemplateWithAllVerticals(box, trigger, activityAction, suffix);
    }

    public static SaveProductTemplate createProductTemplateWithAllVerticals(Box box, ActivityTrigger trigger, ActivityAction activityAction, String suffix) throws Exception {

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, trigger.toString(), "description of template for all verticals");

        Activity activity = ruleBuilder.buildActivity(trigger.toString(), box, rule, Lists.newArrayList(), Lists.newArrayList(activityAction));

        SaveProductTemplate productTemplate = addAllVerticals();

        productTemplate.setName(TemplatesLibrary.PRODUCT_TEMPLATE_ALL_VERTICALS + suffix);

        productTemplate.setCategory(TemplateCategories.Summer.toString());

        productTemplate.setDescription("description_All_Verticals");

        activity.clickSaveAsProductTemplate(productTemplate);
        createActivityInBox(activity);
        activity.clickCancel(); // dont save the rule.
        log4j.info("New activity was saved as product template called: " + productTemplate.getName());
        return productTemplate;
    }

    public static SaveProductTemplate createProductTemplateWithAllVerticals(Box box, ActivityTrigger trigger, List<Case> cases) throws Exception {

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, trigger.toString(), "description of template for all verticals");

        Activity activity = ruleBuilder.buildActivityWithCases(trigger.toString(), box, rule, Lists.newArrayList(), cases);

        SaveProductTemplate productTemplate = addAllVerticals();

        productTemplate.setName(TemplatesLibrary.PRODUCT_TEMPLATE_ALL_VERTICALS + trigger.toString());

        productTemplate.setCategory(TemplateCategories.Summer.toString());

        productTemplate.setDescription("description_All_Verticals");

        activity.clickSaveAsProductTemplate(productTemplate);
        createActivityInBox(activity);
        activity.clickCancel(); // dont save the rule.
        log4j.info("New activity was saved as product template called: " + productTemplate.getName());
        return productTemplate;
    }

    public static SaveProductTemplate createProductTemplateWithAllVerticals(Box box, ActivityTrigger trigger, ApplyCondition activityCondition, ActivityAction activityAction) throws Exception {

        Rule rule = (Rule) ruleBuilder.getConcreteActivity(box, ActivityType.Rule, trigger.toString(), "description of template for all verticals");

        Activity activity = ruleBuilder.buildActivity(trigger.toString(), box, rule, Lists.newArrayList(activityCondition), Lists.newArrayList(activityAction));

        SaveProductTemplate productTemplate = addAllVerticals();

        productTemplate.setName(TemplatesLibrary.PRODUCT_TEMPLATE_ALL_VERTICALS + trigger.toString());

        productTemplate.setCategory(TemplateCategories.Summer.toString());

        productTemplate.setDescription("description_All_Verticals");

        activity.clickSaveAsProductTemplate(productTemplate);
        createActivityInBox(activity);
        activity.clickCancel(); // dont save the rule.
        log4j.info("New activity was saved as product template called: " + productTemplate.getName());
        return productTemplate;

    }

    @NotNull
    private static SaveProductTemplate addAllVerticals() {
        SaveProductTemplate productTemplate = new SaveProductTemplate();
        int verticals = TemplateVerticals.values().length;
        for (int i = 0; i < verticals; i++) {
            productTemplate.addVertical(TemplateVerticals.values()[i].getValue());
        }
        return productTemplate;
    }


    public static void createActivityFromGoal(GoalWizard goalWizard) throws AutomationException, InterruptedException {
        Navigator.loyaltyTabPage();
        fillSeleniumObject(goalWizard);
        waitForLoad();
    }


    public static String openDraftActivity(String activityName) throws InterruptedException {
        WebElement activityElement = getFirstActivityElement(activityName);
        activityElement.click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
        return getDriver().findElement(By.xpath(Activity.ACTIVITY_NAME)).getAttribute("value");
    }

    public static String loadFirstActivity() throws InterruptedException {
        String xPath = "//*[@" + HUB2_AUTOMATION_STR + "= 'business-center.campaign.0.row.0']";
        List<WebElement> elements = getDriverWait().until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath(xPath)));

        elements.get(0).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
        return getDriver().findElement(By.xpath(Activity.ACTIVITY_NAME)).getAttribute("value");
    }

    public static void publishDaftActivity(Activity activity) throws AutomationException {
        fillSeleniumObject((BaseSeleniumObjectHub2) activity);
        waitForLoad();
    }


    public static String getText(By xpath) {
        WebElement element = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(xpath));
        return element.getText();
    }

    public static List<WebElement> findElements(By xpath) {
        return getDriver().findElements(xpath);
    }
}
