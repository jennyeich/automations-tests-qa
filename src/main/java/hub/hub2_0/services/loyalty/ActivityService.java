package hub.hub2_0.services.loyalty;

import hub.base.BaseService;
import hub.hub1_0.services.Navigator;
import hub.hub2_0.common.enums.ItemOperation;
import hub.hub2_0.common.enums.SelectedItemsDialogButtons;
import hub.hub2_0.services.BaseListTable;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.text.MessageFormat;

public class ActivityService extends BaseListTable {

    public static   final Logger log4j = Logger.getLogger(DeleteService.class);

    private static final String BUISNESS_CENTER_CAMPAIGN = "business-center.campaign.";
    private static  String ACTIVITY = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\""+ BUISNESS_CENTER_CAMPAIGN +"{0}.row.{1}\"]";
    private static  final String ACTIVITY_SIDE_MENU = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\""+ BUISNESS_CENTER_CAMPAIGN +"{0}.row.{1}.activity-context-menu\"]/i" ;
    private static  final String ACTIVITY_DETALIS_SUB_MENU = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\"" + BUISNESS_CENTER_CAMPAIGN +"{0}.activity-row.{1}.contextual-menu.0\"]";
    private static  final String MOVE_TO_SUB_MENU = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\"" + BUISNESS_CENTER_CAMPAIGN +"{0}.activity-row.{1}.contextual-menu.1\"]";
    private static  final String START_STOP_ACTIVITY_SUB_MENU = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\"" + BUISNESS_CENTER_CAMPAIGN +"{0}.activity-row.{1}.contextual-menu.2\"]";
    private static  final String MOVE_ACTIVITY_TO  = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\"activity-move-to\"]";
    private static  final String MOVE_BUTTON_SUB_MENU = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\""+ "activity-move-to.btn.move\"]";
    private static  final String CONFIRMATION_WINDOW = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\"" + "modal.confirmation\"]" ;
    private static  final String CONFIRMATION_WINDOW_OK_BTN = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\"" + "confirmation.btn.confirm\"]" ;
    private static  final String CONFIRMATION_WINDOW_CANCEL_BTN = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\"" + "confirmation.btn.cancel\"]" ;

    private static String campaignIdx;
    private static String activityIdx;
    private static String activityMoveToXpath ;

    public static void executeOperation(String activityName, ItemOperation operation) throws Exception {
        Navigator.loyaltyTabPage();
        try {
            initXpath(activityName);
            executeOperationOnListItem(operation);
        }catch (Exception e) {
            throw new Exception("Failed to execute operation < " +  operation.toString() + "> on activity name:<" + activityName + "> , exit test!!!\n" , e );
        }
    }

    public static boolean isActiviyExists(String activityName)
    {
        boolean isExists =false;
        try {
            WebElement activityElement = findElementByDiffrentAttribute("title",activityName);
            isExists =true;
        } catch (Exception e){}// do nothing , just catch the exception and return back that activity wasn't found
        return isExists;
    }


    public static void moveToActivity(String activityNameToMove , String newBox) throws Exception {
        Navigator.loyaltyTabPage();
        try {

            initXpath(activityNameToMove);
            clickOnMoveTo(newBox);
            Thread.sleep(2000); // sleep until rule moved
            log4j.info("Activity:<" + activityNameToMove + "> Moved to <" + newBox + ">");
        }catch (Exception e) {
            throw new Exception("Failed to execute operation: <Move to> on activity name:" + activityNameToMove + "> , exit test!!!\n" , e );
        }
    }

    private static void clickOnMoveTo(String newBox) throws Exception {
        sideMenuClick();
        try {
            // click on Move to sub menu
            getDriver().findElement(By.xpath(activityMoveToXpath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            //click on drop down and click on box to move to
            getDriver().findElement(By.xpath(MOVE_ACTIVITY_TO)).click();
            getDriver().findElement(By.xpath("//span[contains(text(),'" + newBox + "')]")).click();
            //click on Move button
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            getDriver().findElement(By.xpath(MOVE_BUTTON_SUB_MENU)).click();
        } catch(Exception e)
        {
            log4j.info("Failed to move activity to:<" + newBox + ">");
            throw new Exception("Failed to move activity to:<" + newBox + ">\n" ,e);
        }
    }

    private static void initXpath(String activityName)
    {
        WebElement activityElement = findElementByDiffrentAttribute("title",activityName);
        log4j.info("Find activity:<" + activityName + ">");
        WebElement parent = activityElement.findElement(By.xpath(".."));
        String attributeVal = parent.getAttribute(HUB2_AUTOMATION_STR);
        String campaignAndactivity =  (LoyaltyService.resolveIdx(attributeVal));
        String[] indexes = campaignAndactivity.split(" ");
        campaignIdx = indexes[0];
        activityIdx = indexes[1];

        setItemXpath(ACTIVITY,campaignIdx,activityIdx);
        setItemSideMenuXpath(ACTIVITY_SIDE_MENU,campaignIdx,activityIdx);
        setItemMenuStartStopXpath(START_STOP_ACTIVITY_SUB_MENU,campaignIdx,activityIdx);
        setItemMenuDetailsXpath(ACTIVITY_DETALIS_SUB_MENU,campaignIdx,activityIdx);
        activityMoveToXpath = getXpath(MOVE_TO_SUB_MENU);
    }


    private static String getXpath(String locator ) {
        return MessageFormat.format(locator,campaignIdx,activityIdx);
    }


    /*
 this method verify that confirmation window of "This campaign is archived, when an activity is activated the campaign will be automatically un-archived" appear
 */
    public static boolean isConfirmationWindowsAppearAndRespond(SelectedItemsDialogButtons respond) throws InterruptedException {
        Boolean appearance = false;
        WebElement confirmationMessage =  getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(CONFIRMATION_WINDOW)));
        if (confirmationMessage.isDisplayed()) {
            appearance = true;
            if (respond.toString().equals(SelectedItemsDialogButtons.Apply.toString()))
                confirmationMessage.findElement(By.xpath(CONFIRMATION_WINDOW_OK_BTN)).click();
            if (respond.toString().equals(SelectedItemsDialogButtons.Cancel.toString()))
                confirmationMessage.findElement(By.xpath(CONFIRMATION_WINDOW_CANCEL_BTN)).click();
            Thread.sleep(2000);
        }
        return appearance;
    }


    public static int getNumberOfRulesInBox(String boxName) throws Exception {
        try {
            String boxIdx = LoyaltyService.findBoxIndex(boxName);
            int rowSize = driver.findElement(By.className("businessCenter")).findElements(By.className("campaign")).get(Integer.parseInt(boxIdx)).findElements(By.tagName("tr")).size();
            int result = (rowSize > 0) ? (rowSize - 1) : rowSize;
            return result; //the first row is the table headers so we remove 1 line from the results.
        } catch (Exception e) {
            throw new Exception ("Failed to get number of rules in campaign:<" + boxName + ">\n",e );
        }
    }

    public static String getActivityStatus(String activityName) throws Exception {
        Navigator.loyaltyTabPage();
        String activityState;
        try {
            initXpath(activityName);
            activityState =getDriver().findElement(By.xpath(getXpath(ACTIVITY))).findElement(By.className("activities-status")).getText();
        } catch (Exception e) {
            throw new Exception("Failed to get state of activity name:<" + activityName + "> , exit test!!!\n", e);
        }
        return activityState;
    }

}
