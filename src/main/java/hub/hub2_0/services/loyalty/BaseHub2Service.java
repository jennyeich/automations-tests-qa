package hub.hub2_0.services.loyalty;

import hub.base.*;
import hub.hub1_0.base.basePages.BaseTabsPage;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.services.UIDebugLogger;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;

import javax.management.ReflectionException;
import java.beans.IntrospectionException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.text.MessageFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BaseHub2Service extends BaseService {

    public static final Logger log4j = Logger.getLogger(BaseHub2Service.class);


    public static final String CAPSULA_DELIMITER = ",";
    public static void fillHub2SeleniumObject(BaseSeleniumObject object, Field field, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, InterruptedException{

        BaseSeleniumObjectHub2 objectHub2 = (BaseSeleniumObjectHub2) object;
        String pathWithPrefix;

        if(!objectHub2.getPrefix().isEmpty() ){
            pathWithPrefix = updatePathWithParams(annotation.value(), objectHub2);
        }else{
            pathWithPrefix = annotation.value();
        }
        switch (annotation.elementInputType()) {
            case INPUT_AND_ENTER_HUB2:
                if (field.get(object) != null)
                    fillElementAndPressEnter(annotation.type(), field,  objectHub2, pathWithPrefix);
                return;
            case CLICK_HUB2:
                if (field.get(object) != null)
                    clickHub2Element(annotation.type(), field,objectHub2, pathWithPrefix);
                break;
            case CLICK_HUB2_1PARAM:
                if (field.get(object) != null)
                    clickElement1Param(annotation.type(), field,objectHub2, pathWithPrefix);
                break;
            case CLICK_IF_ENABLED:
                if (field.get(object) != null)
                    clickElementIfEnabled(annotation.type(), field,objectHub2, pathWithPrefix);
                break;
            case SELECT_HUB2:
                if (field.get(object) != null)
                    selectHub2Element(annotation.type(), field, objectHub2, pathWithPrefix);
                break;
            case INPUT_HUB2:
                if (field.get(object) != null)
                    fillInputElement(annotation.type(), field, objectHub2, pathWithPrefix);
                break;
            case INPUT_CAPSULA_HUB2:
                if (field.get(object) != null)
                    fillInputCapsula(annotation.type(), field, objectHub2, pathWithPrefix);
                break;
            case SELECT_BY_TEXT_HUB2:
                if (field.get(object) != null)
                    selectByTextHub2Element(annotation.type(), field, objectHub2, pathWithPrefix);
                break;
            case ADVANCED_SELECT_HUB2:
                if (field.get(object) != null)
                    advancedSearchHub2(annotation.type(), field, objectHub2, pathWithPrefix);
                break;
            case MULTI_SELECT_HUB2:
                if (field.get(object) != null)
                    multiSelectHub2Element(annotation.type(), field, objectHub2, pathWithPrefix);
                break;
            case CLICK_INNER_ELEMENT_HUB2:
                if (field.get(object) != null)
                    clickInnerElementHub2(annotation.type(), field, objectHub2, pathWithPrefix);
                break;
            case INPUT_TIME:
                if(field.get(object) != null){
                    fillTimeInput(annotation.type(), field, objectHub2, pathWithPrefix);
                } break;
            case SCROLL_AND_CLICK:
                if(field.get(object) != null)
                    scrollToViewAndClick(annotation.type(), field, objectHub2, pathWithPrefix);
                break;
            case CHECKBOX_HUB2:
                if (field.get(object) != null)
                    clickHub2Element(annotation.type(), field, objectHub2, pathWithPrefix);
                break;
            case TEXT_AREA_HUB2:
                if (field.get(object) != null)
                    fillElementForTextAreaHub2(annotation.type(), field, objectHub2, pathWithPrefix);
                break;
            case CLICK_ESC:
                if (field.get(object) != null)
                    closeByEsc();
                break;
            case CLICK_REAPEATED:
                if (objectHub2 instanceof Activity) {
                    if (field.get(object) != null) {
                        int numOfCases = ((Activity)objectHub2).getCases().size();
                        for(int i=0 ; i<(numOfCases-1) ;i++)
                            clickElement1Param(annotation.type(), field, objectHub2, pathWithPrefix);
                        scrollUp();
                    }
                }
                break;
        }
    }

    public static void fillInputCapsula(SeleniumPath.Type type, Field field, BaseSeleniumObjectHub2 objectHub2, String pathWithPrefix) throws IllegalAccessException, InterruptedException {

        waitForLoad();
        String dynamicPath = MessageFormat.format(pathWithPrefix,objectHub2.getNext());;
        String stringToSplit = (String) field.get(objectHub2);
        String[] capsulaTexts = stringToSplit.split(CAPSULA_DELIMITER);
        if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);

            driver.findElement(By.xpath(dynamicPath)).clear();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

            for (int i = 0; i < capsulaTexts.length; i++) {
                driver.findElement(By.xpath(dynamicPath)).sendKeys(capsulaTexts[i].trim());
                driver.findElement(By.xpath(dynamicPath)).sendKeys(Keys.ENTER);

            }
            UIDebugLogger.fillTagsAddToActionLog(UIDebugLogger.INFO, stringToSplit, field, objectHub2);
        }
    }

    private static void multiSelectHub2Element(SeleniumPath.Type type, Field field, BaseSeleniumObjectHub2 objectHub2, String pathWithPrefix) throws InterruptedException, IllegalAccessException {

        if (type == SeleniumPath.Type.BY_DYNAMIC_XPATH) {
            field.setAccessible(true);

            String valueAsString = field.get(objectHub2).toString();
            String[] arr =  valueAsString.split(",");
            for (int i=0; i< arr.length ; i++) {
                String optionXpath = MessageFormat.format(pathWithPrefix, arr[i]);

                WebElement optionElement = driver.findElement(By.xpath(optionXpath));
                optionElement.click();
                Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            }
            Actions builder = new Actions(driver);
            Action seriesOfActions = builder.sendKeys(Keys.TAB).build();
            seriesOfActions.perform();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }else if(type == SeleniumPath.Type.BY_GENERATE_XPATH){
            field.setAccessible(true);

            String valueAsString = field.get(objectHub2).toString();
            String[] arr =  valueAsString.split(",");
            for (int i=0; i< arr.length ; i++) {
                String optionXpath = MessageFormat.format(pathWithPrefix, objectHub2.getNext(),arr[i]);

                WebElement optionElement = driver.findElement(By.xpath(optionXpath));
                optionElement.click();
                Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            }
            Actions builder = new Actions(driver);
            Action seriesOfActions = builder.sendKeys(Keys.TAB).build();
            seriesOfActions.perform();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }
    }

    private static void closeByEsc() throws InterruptedException {
        Actions builder = new Actions(driver);
        Actions actions = builder.sendKeys(Keys.ESCAPE);
        actions.perform();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
    }

    private static String updatePathWithParams(String originPath, BaseSeleniumObjectHub2 objectHub2) {
        String pathWithPrefix;
        String path =  MessageFormat.format(originPath, objectHub2.getPrefix());
        String newPath = path;
        if (newPath.indexOf("{1}") != -1)
            newPath = newPath.replace("{1}", "{0}");
        if (newPath.indexOf("{2}") != -1)
            newPath = newPath.replace("{2}", "{1}");
        pathWithPrefix = newPath;
        return pathWithPrefix;
    }

    public static void fillElementForTextAreaHub2(SeleniumPath.Type type, Field field, BaseSeleniumObjectHub2 object, String pathWithPrefix) throws IntrospectionException, IllegalAccessException, InterruptedException {
        String dynamicPath;

        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            WebElement element = findElementByAutomationId(pathWithPrefix);
            element.sendKeys(Keys.TAB);
            element.clear();
            element.sendKeys(field.get(object).toString());
        } else if (type == SeleniumPath.Type.BY_ID) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.id(pathWithPrefix));
            element.sendKeys(Keys.TAB);
            element.clear();
            element.sendKeys(field.get(object).toString());
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.xpath(pathWithPrefix));
            element.sendKeys(Keys.TAB);
            element.clear();
            element.sendKeys(field.get(object).toString());
        } else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            TimeUnit.MILLISECONDS.sleep(500);
            dynamicPath = buildDynamicPath(hub2mode, object, pathWithPrefix);
            WebElement element = driver.findElement(By.xpath(dynamicPath));
            element.sendKeys(Keys.TAB);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            element.sendKeys(field.get(object).toString());
            element.sendKeys(Keys.TAB);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }
        UIDebugLogger.fillElementAddActionToLog(UIDebugLogger.INFO, field, object);
    }

    public static void fillInputElement(SeleniumPath.Type type, Field field, BaseSeleniumObjectHub2 object, String pathWithPrefix) throws IntrospectionException, IllegalAccessException, InterruptedException {

        String dynamicPath;
        waitForLoad();
        if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            TimeUnit.MILLISECONDS.sleep(500);

            dynamicPath = MessageFormat.format(pathWithPrefix,object.getNext());

            driver.findElement(By.xpath(dynamicPath)).clear();
            driver.findElement(By.xpath(dynamicPath)).sendKeys(field.get(object).toString());
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.xpath(pathWithPrefix));
            element.clear();
            element.sendKeys(field.get(object).toString());
        } else if(type == SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT) {
            field.setAccessible(true);
            TimeUnit.MILLISECONDS.sleep(500);

            dynamicPath = MessageFormat.format(pathWithPrefix, object.getIdSubString(), object.getNext());

            driver.findElement(By.xpath(dynamicPath)).clear();
            driver.findElement(By.xpath(dynamicPath)).sendKeys(field.get(object).toString());
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }
    }

    private static void clickElement1Param(SeleniumPath.Type type, Field field, BaseSeleniumObjectHub2 object, String pathWithPrefix) throws IntrospectionException, IllegalAccessException, InterruptedException {

        String dynamicPath;
        waitForLoad();
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);

            dynamicPath = pathWithPrefix;
            WebElement element = findElementByAutomationId(dynamicPath);
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }else if (type == SeleniumPath.Type.BY_DYNAMIC_XPATH) {
            field.setAccessible(true);

            dynamicPath = (String) field.get(object);
            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);

            dynamicPath = pathWithPrefix;

            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }else if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            dynamicPath = MessageFormat.format(pathWithPrefix,object.getNext());

            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }else if (type == SeleniumPath.Type.BY_GENERATE_PARENT_XPATH) {
            field.setAccessible(true);
            dynamicPath = MessageFormat.format(pathWithPrefix,object.getParentIndex());

            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        } else if(type == SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT) {
            field.setAccessible(true);
            dynamicPath = MessageFormat.format(pathWithPrefix, object.getIdSubString(), object.getNext());

            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }
        UIDebugLogger.clickElementAddActionToLog(UIDebugLogger.INFO, field, object);
    }

    private static void clickElementIfEnabled(SeleniumPath.Type type, Field field, BaseSeleniumObjectHub2 object, String pathWithPrefix) throws IntrospectionException, IllegalAccessException, InterruptedException {

        if (type == SeleniumPath.Type.BY_XPATH2) {
            field.setAccessible(true);
            boolean found;
            List<WebElement> elements = driver.findElements(By.xpath(field.get(object).toString()));
            for (WebElement element : elements) {
                found = true;
                String classes = element.getAttribute("class");
                for (String c : classes.split(" ")) {
                    if (c.contains("disabled")) {
                        found = false;
                    }
                }
                if (found) {
                    element.click();
                    break;
                }
            }
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }
        UIDebugLogger.clickElementAddActionToLog(UIDebugLogger.INFO, field, object);
    }



    public static void fillTimeInput(SeleniumPath.Type type, Field field, BaseSeleniumObject object, String pathWithPrefix)throws IntrospectionException, IllegalAccessException, InterruptedException {

        if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            String dynamicPath= buildDynamicPath(hub2mode,object,pathWithPrefix);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            WebElement element = driver.findElement(By.xpath(dynamicPath));
            Actions builder = new Actions(driver);
            Action seriesOfActions = builder.click(element).sendKeys(Keys.BACK_SPACE).sendKeys(Keys.BACK_SPACE).sendKeys(field.get(object).toString()).sendKeys(Keys.TAB).build();
            seriesOfActions.perform();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }
        UIDebugLogger.fillElementAddActionToLog(UIDebugLogger.INFO, field, object);
    }
    public static void clickHub2Element(SeleniumPath.Type type, Field field, BaseSeleniumObjectHub2 object, String pathWithPrefix) throws IllegalAccessException, InterruptedException {

        String dynamicPath;
        waitForLoad();
        if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);

            dynamicPath = MessageFormat.format(pathWithPrefix, object.getNext(), field.get(object));

            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }else if(type == SeleniumPath.Type.BY_AUTOMATION_ID){//for xpath that has dynamic id (e.g. trigger.{0})
            field.setAccessible(true);
            dynamicPath = MessageFormat.format(pathWithPrefix,field.get(object));
            WebElement element = driver.findElement(By.xpath(dynamicPath));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        } else if (type == SeleniumPath.Type.BY_GENERATE_PARENT_XPATH) {
            field.setAccessible(true);
            dynamicPath = MessageFormat.format(pathWithPrefix,object.getParentIndex());
            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }else if (type == SeleniumPath.Type.BY_XPATH) {
            field.setAccessible(true);

            driver.findElement(By.xpath(pathWithPrefix)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        } else if(type == SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT) {
            field.setAccessible(true);
            dynamicPath = MessageFormat.format(pathWithPrefix, object.getIdSubString(), object.getNext(), field.get(object));
            driver.findElement(By.xpath(dynamicPath)).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }
        UIDebugLogger.clickElementAddActionToLog(UIDebugLogger.INFO, field, object);
    }

    /**
     * This method is for using the select drop down in hub2.It relies on the data-automation-id that concatenate from the annotation.value(),object.getNext(),field.get(object)
     * and creates the xpath that matches the correct selection e.g. '[@data-automation-id='condition.0.conditionsOperator.==']
     * @param type
     * @param field
     * @param object
     * @param pathWithPrefix
     */
    public static void selectHub2Element(SeleniumPath.Type type, Field field, BaseSeleniumObjectHub2 object, String pathWithPrefix) throws InterruptedException, IllegalAccessException {
        waitForLoad();
        if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            //take the annotation xpath without the suffix which represents which condition to click on (e.g. condition.{0}.conditionsOperator.{1} - without '.{1}' )
            String xpath = pathWithPrefix;
            String suffix = xpath.substring(xpath.lastIndexOf('}')+1, xpath.length());
            String dynamicPath = MessageFormat.format(xpath.substring(0, xpath.lastIndexOf('.')) + suffix,object.getNext());
            WebElement element = driver.findElements(By.xpath(dynamicPath)).get(0);
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            //create the correct xpath for the selected option (e.g. condition.{0}.conditionsOperator.{1} --> condition.0.conditionsOperator.==
            String optionXpath = MessageFormat.format(pathWithPrefix,object.getNext(),field.get(object));

            WebElement optionElement = driver.findElement(By.xpath(optionXpath));
            optionElement.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }else if (type == SeleniumPath.Type.BY_GENERATE_XPATH_ONE_CLICK) {
            field.setAccessible(true);

            String optionXpath = MessageFormat.format(pathWithPrefix,object.getNext(),field.get(object));


            WebElement optionElement = driver.findElement(By.xpath(optionXpath));
            optionElement.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }else if (type == SeleniumPath.Type.BY_DYNAMIC_XPATH) {
            field.setAccessible(true);

            String optionXpath = MessageFormat.format(pathWithPrefix,field.get(object));


            WebElement optionElement = driver.findElement(By.xpath(optionXpath));
            optionElement.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }


    }

    public static void selectByTextHub2Element(SeleniumPath.Type type, Field field, BaseSeleniumObject object, String pathWithPrefix) throws InterruptedException, IllegalAccessException {
        waitForLoad();
        WebElement element = null;
        if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            element = driver.findElement(By.xpath(buildDynamicPath(hub2mode, object, pathWithPrefix)));
        }
        else if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            field.setAccessible(true);
            element = findElementByAutomationId(pathWithPrefix);
        }
        element.click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        //create the correct xpath for the selected option (e.g. condition.{0}.conditionsOperator.{1} --> condition.0.conditionsOperator.==
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        String optionXpath = "//span[contains(text(), '" + field.get(object).toString() + "')]";
        clickIfDisplayed(field, object, optionXpath);

        UIDebugLogger.selectElementfromListAddActionToLog(UIDebugLogger.INFO, field, object);
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
    }

    protected static void clickIfDisplayed(Field field, BaseSeleniumObject object, String optionXpath) throws IllegalAccessException, InterruptedException {

        List<WebElement> elements = driver.findElements(By.xpath(optionXpath));

        for(WebElement optionElement : elements) {
            if((optionElement.isDisplayed()) && (!optionElement.isSelected())) {
                optionElement.click();
                break;
            }
        }
        UIDebugLogger.selectElementfromListAddActionToLog(UIDebugLogger.INFO, field, object);
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
    }

    public static void advancedSearchHub2(SeleniumPath.Type type, Field field, BaseSeleniumObject object, String pathWithPrefix) throws InterruptedException, IllegalAccessException {
        waitForLoad();
        if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            WebElement element = driver.findElement(By.xpath(buildDynamicPath(hub2mode, object, pathWithPrefix)));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            String optionXpath = "//span[@title='" + field.get(object).toString() +"']";
            WebElement optionElement = driver.findElement(By.xpath(optionXpath));
            optionElement.click();

        }
        UIDebugLogger.selectElementfromListAddActionToLog(UIDebugLogger.INFO, field, object);
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
    }


    public static void fillElementAndPressEnter(SeleniumPath.Type type, Field field, BaseSeleniumObjectHub2 object, String pathWithPrefix) throws IllegalAccessException, InterruptedException {

        String dynamicPath;
        waitForLoad();
        if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            TimeUnit.MILLISECONDS.sleep(500);
            dynamicPath = MessageFormat.format(pathWithPrefix, object.getNext());


            WebElement element = driver.findElement(By.xpath(dynamicPath));
            element.clear();
            element.sendKeys(field.get(object).toString());
            element.sendKeys(Keys.ENTER);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            UIDebugLogger.fillElementAddActionToLog(UIDebugLogger.INFO, field, object);
        }
    }


    public static void clickInnerElementHub2(SeleniumPath.Type type, Field field, BaseSeleniumObjectHub2 object, String pathWithPrefix) throws IntrospectionException, IllegalAccessException, InterruptedException {
        String optionXpath;
        String dynamicPath;
        waitForLoad();
        if (type == SeleniumPath.Type.BY_GENERATE_XPATH) {
            field.setAccessible(true);
            //take the annotation xpath without the suffix which represents which condition to click on (e.g. condition.{0}.itemsPopulationcondition.{1}.conditionKey.{2} - without '.{2}' )
            String xpath = pathWithPrefix;
            dynamicPath = MessageFormat.format(xpath.substring(0, xpath.lastIndexOf('.')) + "\"]",object.getNext());
            dynamicPath = dynamicPath.replace("]",")]");
            WebElement element = driver.findElement(By.xpath(dynamicPath));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            //create the correct xpath for the selected option (e.g. condition.{0}.itemsPopulationcondition.{1}.conditionKey.{2} --> condition.0.itemsPopulationcondition.0.conditionKey.ItemCode
            if (object.getNext()>=0) //handle case that the element have incremental indexes (such as conditions).
               optionXpath = MessageFormat.format(pathWithPrefix,object.getNext(),field.get(object));
            else //handle case that the element appear 1 time
                optionXpath = MessageFormat.format(pathWithPrefix,field.get(object));
            WebElement optionElement = driver.findElement(By.xpath(optionXpath));
            optionElement.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }
    }

    public static void scrollToViewAndClick(SeleniumPath.Type type, Field field, BaseSeleniumObjectHub2 object, String pathWithPrefix) throws IllegalAccessException, InterruptedException {
        waitForLoad();

        if(type == SeleniumPath.Type.BY_AUTOMATION_ID){
            field.setAccessible(true);
            WebElement clickableElement = findElementByAutomationId(pathWithPrefix);
            scrollUp();
            Actions actions = new Actions(driver);
            actions.moveToElement(clickableElement);
            actions.perform();
            clickableElement.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }
        UIDebugLogger.clickElementAddActionToLog(UIDebugLogger.INFO, field, object);
    }

    public static void scrollUp() {
        WebElement loyaltyTab = driver.findElement(BaseTabsPage.TAB_LOYALTY_CENTER);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", loyaltyTab);

    }


    public static void readHub2SeleniumObject(BaseSeleniumObjectHub2 object, Field field, SeleniumPath annotation)  throws Exception{

        switch (annotation.elementInputType()) {
            case INPUT_HUB2:
            case TEXT_AREA_HUB2:
                readHub2InputElement(annotation.type(), field, object, annotation);
                break;
            case SELECT_HUB2:
                readSelectHub2Element(annotation.type(), field, object, annotation);
                break;
            case CHECKBOX_HUB2:
                readHub2CheckboxElement(annotation.type(), field, object, annotation);
                break;
        }
    }

    public static void readHub2CheckboxElement(SeleniumPath.Type type, Field field, BaseSeleniumObject object, SeleniumPath annotation) throws IllegalAccessException {
        if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            WebElement element = findElementByAutomationId(annotation.readValue());
            field.set(object, element.isSelected());
        }
    }



    //reads input field
    public static void readHub2InputElement(SeleniumPath.Type type, Field field, BaseSeleniumObjectHub2 object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException {

        List<WebElement> elements;
        if (type == SeleniumPath.Type.BY_ID) {
            elements = driver.findElements(By.id(annotation.readValue()));
            if (elements.size() > 0 && elements.get(0).getAttribute("value") != null)
                field.set(object, elements.get(0).getAttribute("value"));
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            elements = driver.findElements(By.xpath(annotation.readValue()));
            if (elements.size() > 0 && elements.get(0).getAttribute("value") != null)
                field.set(object, elements.get(0).getAttribute("value"));
        } else if (type == SeleniumPath.Type.BY_AUTOMATION_ID) {
            WebElement element = findElementByAutomationId(annotation.readValue());
            if (element.getAttribute("value") != null)
                field.set(object, element.getAttribute("value"));
            else if (element.getText() != null)
                field.set(object, element.getText());
        }

    }


    public static void readSelectHub2Element(SeleniumPath.Type type, Field field, BaseSeleniumObjectHub2 object, SeleniumPath annotation) throws IntrospectionException, IllegalAccessException, ClassNotFoundException, NoSuchMethodException, InstantiationException, ReflectionException, InvocationTargetException, NoSuchFieldException {

        Select select;
        List<WebElement> elements;

        if (type == SeleniumPath.Type.BY_ID) {
            elements = driver.findElements(By.id(annotation.readValue()));
            if (elements.size() > 0 && elements.get(0).getAttribute("value") != null) {
                select = new Select(elements.get(0));
                field.set(object, Utils.getEnumFromString((Class<Enum>) field.getType(), select.getFirstSelectedOption().getAttribute("value")));
            }
        } else if (type == SeleniumPath.Type.BY_XPATH) {
            elements = driver.findElements(By.xpath(annotation.readValue()));
            if (elements.size() > 0 && elements.get(0).getAttribute("value") != null) {
                select = new Select(elements.get(0));

                field.set(object, Utils.getEnumFromString((Class<Enum>) field.getType(), select.getFirstSelectedOption().getAttribute("value")));

            }
        }
    }

    public static void refresh() {
        getDriver().navigate().refresh();
        waitForLoad();
    }

    public static void waitForLoad() {

        boolean loading = true;
        long timeoutCounter = 0;
        while (loading && (timeoutCounter <= TIMEOUT_FOR_PAGE_LOAD_WAIT_SEC)) {
            try {
                TimeUnit.SECONDS.sleep(1);
                WebElement loader = getDriver().findElement(By.xpath("//*[@class=\"ui loader\"]"));
                if(!loader.isDisplayed())
                    loading = false;
                timeoutCounter++;
            } catch (NoSuchElementException e) {
                getDriverWait().until((ExpectedCondition) wd ->
                        ((JavascriptExecutor) getDriver()).executeScript("return document.readyState").equals("complete"));
                return;
            } catch (InterruptedException e) {
                e.printStackTrace();
                timeoutCounter++;
            }
        }

    }
}
