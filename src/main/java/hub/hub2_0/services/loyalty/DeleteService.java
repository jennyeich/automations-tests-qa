package hub.hub2_0.services.loyalty;

import hub.base.BaseService;
import hub.hub1_0.services.Navigator;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.text.MessageFormat;

/**
 * Created by Sharon on 1/30/2018.
 */

public class DeleteService extends BaseHub2Service
{
    public static final Logger log4j = Logger.getLogger(DeleteService.class);
    private static final String BUISNESS_CENTER_CAMPAIGN = "business-center.campaign.";
    private static  final String BOX_SIDE_MENU = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\""+ BUISNESS_CENTER_CAMPAIGN +"{0}.contextual-menu\"]" ;
    private static  final String ARCHIVE_SUB_MENU = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\"" + BUISNESS_CENTER_CAMPAIGN +"{0}.contextual-menu.0\"]" ;
    private static  final String REMOVE_CONDITION = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\"" + "rule.condition.{0}.delete\"]" ;
    private static  final String REMOVE_ACTION =    "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\"" + "activity.actions.{0}.delete.{0}\"]" ; //todo add supported for remove action - pending on BUG CNP-10207
    private static  final String INFORMATION_WINDOW = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\"" + "modal.information\"]" ;
    private static  final String INFORMATION_WINDOW_DISMISS_BUTTON = "//*[@" + BaseService.HUB2_AUTOMATION_STR +"=\"" + "information.btn.got-it\"]" ;


    public static final String UNARCHIVE_TEXT_SUB_MENU= "UnArchive";
    public static final String ARCHIVE_TEXT_SUB_MENU= "Archive";

    public static void archiveBox(String boxName) throws Exception {
        Navigator.loyaltyTabPage();
        try {
             String boxIndex = LoyaltyService.findBoxIndex(boxName);
             moveBoxToArchive(boxIndex);
             Thread.sleep(2000); // sleep until campaign will move to archive
             log4j.info("archive BOX: <" + boxName + ">" );
        } catch (Exception e) {
            throw new Exception("Failed to find Box: <" + boxName + "> in the Loyalty page, Exit test!!!!\n" , e);
        }
    }

    private static void moveBoxToArchive(String boxIndex) throws Exception {
        clickOnElement(BOX_SIDE_MENU,boxIndex);
        clickOnElement(ARCHIVE_SUB_MENU,boxIndex);
    }

    private static void  clickOnElement(String elementLocator, String index) throws Exception {
        String xpath = MessageFormat.format(elementLocator, index);
        try {
            WebElement elem = getDriver().findElement(By.xpath(xpath));
            elem.click();
        } catch(Exception e){
            throw new Exception("Failed to click on element:<" + elementLocator + "> at location:" + index +". Exit Test!!!!\n" , e );
        }
    }

   /* this method check if the side menu of activity show Archive/Unarchive*/
    public static String getArchiveState(String boxName) throws Exception {
        String currentArchiveStatus= null;
        String boxIndex = LoyaltyService.findBoxIndex(boxName);
        String boxSideMenuXpath = MessageFormat.format(BOX_SIDE_MENU, boxIndex);
        String archiveSubMenuXpath = MessageFormat.format(ARCHIVE_SUB_MENU, boxIndex);
        try {
            WebElement sideMenuElem = getDriver().findElement(By.xpath(boxSideMenuXpath));
            sideMenuElem.click();
            WebElement archiveSubMenuElem = getDriver().findElement(By.xpath(archiveSubMenuXpath));
            currentArchiveStatus = archiveSubMenuElem.findElement(By.cssSelector(".content")).getText();
            clickOnElement(BOX_SIDE_MENU,boxIndex); // dummy click , just to disappear the menu
        } catch(Exception e){
            throw new Exception("Failed to click on element:<" + BOX_SIDE_MENU + "> at location:" + boxIndex +". Exit Test!!!!\n" , e );
        }
        return currentArchiveStatus;
    }



    public static void deleteAction(String activityName ,int  actionNumberToDelete) throws Exception {
        Navigator.loyaltyTabPage();
        LoyaltyService.clickOnActivity(activityName);
        clickOnElement(REMOVE_ACTION,String.valueOf(actionNumberToDelete-1));
        LoyaltyService.clickPublish();
    }

    public static void deleteCondition(String activityName ,int  conditionNumberToDelete) throws Exception {
        Navigator.loyaltyTabPage();
        LoyaltyService.clickOnActivity(activityName);
        clickOnElement(REMOVE_CONDITION,String.valueOf(conditionNumberToDelete-1));
        LoyaltyService.clickPublish();
    }
/* this method check if the warning meesage appear while trying to archive campagin */
    public static boolean isArchiveWarningWindowsAppear()
    {
        Boolean appearance = false;
        WebElement warnningMessage =  getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(INFORMATION_WINDOW)));
        if (warnningMessage.isDisplayed()) {
            appearance = true;
            warnningMessage.findElement(By.xpath(INFORMATION_WINDOW_DISMISS_BUTTON)).click();
        }
        return appearance;
    }

}

