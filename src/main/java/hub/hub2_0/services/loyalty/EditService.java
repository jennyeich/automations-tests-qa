package hub.hub2_0.services.loyalty;

import hub.hub1_0.services.Navigator;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.conditions.ActivityCondition;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by Goni on 1/30/2018.
 */

public class EditService extends BaseHub2Service {

    /**
     * Edit the current box name according to new given one.
     * @param box - the current box to edit
     * @param name - the new name to set
     * @return the new name we set
     * @throws Exception
     */
    public static String editBoxName(Box box,String name) throws Exception{

        Navigator.loyaltyTabPage();
        clearAndSetValue(name,buildDynamicPath(isHub2mode(),box,Box.BOX_NAME));
        //get box element again
        WebElement element = getDriver().findElement(By.xpath(buildDynamicPath(isHub2mode(),box,Box.BOX_NAME)));
        return element.getAttribute("value");
    }

    /**
     * Edit the current activity name according to new given one.
     * @param currentActivity - the current activity to edit
     * @param name - the new activity name to edit
     * @return
     * @throws Exception
     */
    public static String editActivityName(Activity currentActivity,String name) throws Exception{

        Navigator.loyaltyTabPage();
        LoyaltyService.clickOnActivity(currentActivity.getActivityName());
        clearAndSetValue(name,Activity.ACTIVITY_NAME);
        //click publish
        LoyaltyService.clickPublish();

        LoyaltyService.clickOnActivity(name);
        WebElement element = getDriver().findElement(By.xpath(Activity.ACTIVITY_NAME));
        return element.getAttribute("value");

    }

    public static String editActivityDescription(Activity currentActivity,String description) throws Exception{

        Navigator.loyaltyTabPage();
        LoyaltyService.clickOnActivity(currentActivity.getActivityName());
        clearAndSetValue(description,Activity.ACTIVITY_DESCRIPTION);
        //click publish
        LoyaltyService.clickPublish();
        LoyaltyService.clickOnActivity(currentActivity.getActivityName());
        WebElement element = getDriver().findElement(By.xpath(Activity.ACTIVITY_DESCRIPTION));
        return element.getAttribute("value");

    }

    public static void editActivityAction(Activity activity, ActivityAction newAction,int indexOfAction) throws Exception{

        Navigator.loyaltyTabPage();
        LoyaltyService.clickOnActivity(activity.getActivityName());
        findActionAndReplace(activity,indexOfAction,newAction);
        //click publish
        LoyaltyService.clickPublish();

    }

    public static void editActivityCondition(Activity activity, ActivityCondition newCondition,int indexOfCondition) throws Exception{


        Navigator.loyaltyTabPage();
        LoyaltyService.clickOnActivity(activity.getActivityName());
        findConditionAndReplace(activity,indexOfCondition,newCondition);
        //click publish
        LoyaltyService.clickPublish();

    }

    private static Activity findActionAndReplace(Activity activity,int indexOfAction, ActivityAction newAction) throws Exception{

        List<ActivityAction> actions = activity.defaultCase().getActions();
        if(activity instanceof Rule) {
            ((Rule)activity).setWhen(null);
        }
        ActivityAction currentAction = actions.get(indexOfAction-1);
        if(!currentAction.getPerformTheAction().equals(newAction.getPerformTheAction())) {
            newAction = activity.defaultCase().replaceAction(currentAction,newAction);
            fillSeleniumObject(newAction);

        }else{
            currentAction.clearPerformTheAction();
            currentAction.editActivityAction(newAction);
            fillSeleniumObject(currentAction);
        }
        return activity;
    }
    private static Activity findConditionAndReplace(Activity activity,int indexOfCondition, ActivityCondition newCondition) throws Exception{

        List<ActivityCondition> conditions = activity.getConditions();
        if(activity instanceof Rule) {
            ((Rule)activity).setWhen(null);
        }
        ActivityCondition currentCondition = conditions.get(indexOfCondition-1);

        newCondition = activity.replaceCondition(currentCondition,newCondition);
        fillSeleniumObject( newCondition);
        return activity;
    }

    private static void clearAndSetValue(String name,String xpath) {
        WebElement element = getDriver().findElement(By.xpath(xpath));
        element.clear();
        element.sendKeys(name);
        element.sendKeys(Keys.TAB);
    }


}

