package hub.hub2_0.common.enums;

public enum ActivityStatus {

    Active("Active"),
    Draft("Draft"),
    Deactivated("Deactivated"),
    Expired("Expired")
    ;

    private final String name;
    ActivityStatus(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }


}



