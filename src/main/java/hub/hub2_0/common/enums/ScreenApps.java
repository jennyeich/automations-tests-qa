package hub.hub2_0.common.enums;

public enum ScreenApps {

    InfoUpdatesTC("General Info"),
    GiftsTC("Gift List")
    ;

    private final String title;
    ScreenApps(final String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
