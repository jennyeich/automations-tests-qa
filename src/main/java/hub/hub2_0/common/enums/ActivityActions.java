package hub.hub2_0.common.enums;

public enum ActivityActions {

    // Rule Actions
    Tag("tagMembership"),
    UnTag("untagMembership"),
    SendBenefit("assignAsset"),
    SendAPushNotification("sendPersonalPush"),
    SendTextMessage("sendMemberSms"),
    Register("registerMembership"),
    ExportEvent("exportEvent"),
    UpdateMembership("updateMembershipExpiration"),
    SendEmail("sendMemberEmail"),
    PunchThePunchCard("punch"),
    PunchExceeded("punchExceeded"),
    PointsAccumulation("addPoints"),

    // Deal Actions
    SendCodeToPOS("sendCodesToPos"),
    AmountOffDiscount("discountAmountOff"),
    PercentOffDiscount("discountPercentOff"),
    SetSpecialPrice("discountSpecialPrice"),
    MakeItemsFree("discountFreeItems");

    private final String name;
    ActivityActions(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }



}
