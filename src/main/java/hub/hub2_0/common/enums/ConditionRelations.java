package hub.hub2_0.common.enums;

public enum ConditionRelations {

    Any("or"),
    All("and"),
    ;
    private final String name;
    ConditionRelations(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
