package hub.hub2_0.common.enums;

public enum SelectedItemsDialogButtons {

    Apply("apply"),
    Cancel("cancel")
    ;
    private final String name;
    SelectedItemsDialogButtons(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
