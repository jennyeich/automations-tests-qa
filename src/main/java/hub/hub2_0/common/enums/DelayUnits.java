package hub.hub2_0.common.enums;

public enum DelayUnits {

    Seconds("second"),
    Minutes("minute"),
    Hour("hour"),
    Day("day"),
    Week("week")
    ;

    private final String name;
    DelayUnits(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
