package hub.hub2_0.common.enums;

public enum SortType {
    LAST_CREATE_UP("lastCreateUp"),
    LAST_CREATE_DOWN("lastCreateDown")
    ;
    private final String sortType;
    SortType(final String sortType) {
        this.sortType = sortType;
    }

    @Override
    public String toString() {
        return sortType;
    }
}