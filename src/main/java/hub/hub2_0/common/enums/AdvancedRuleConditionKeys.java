package hub.hub2_0.common.enums;

public enum AdvancedRuleConditionKeys {

    ItemCode("item.ItemCode"),
    ItemName("item.ItemName"),
    Tags("item.Tags"),
    Price("item.Price"),
    DepartmentCode("item.DepartmentCode"),
    DepartmentName("item.DepartmentName"),
    ;
    private final String name;
    AdvancedRuleConditionKeys(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
