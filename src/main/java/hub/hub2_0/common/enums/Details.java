package hub.hub2_0.common.enums;

public enum Details {

    CREATED("CREATED"),
    FIRST_ACTIVATED("FIRST ACTIVATED"),
    LAST_MODIFIED("LAST MODIFIED")
    ;

    private final String name;
    Details(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
