package hub.hub2_0.common.enums;

import hub.hub2_0.common.filters.IFilter;

public enum GiftFilterView implements IFilter {
    ACTIVE("Active"),
    EXPIRED("Expired"),
    STOPPED("Stopped"),
    FUTURE("Future"),
    ;

    private final String index;

    GiftFilterView(final String index) {
        this.index = index;
    }

    public String index() {
        return index;
    }
}
