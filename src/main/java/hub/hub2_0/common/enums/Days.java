package hub.hub2_0.common.enums;

public enum Days {

    SUNDAY("0"),//this reffer to the data-automation-id of sunday 'days_and_times.{0}.days.values.0.circle'
    MONDAY("1"),//this reffer to the data-automation-id of sunday 'days_and_times.{0}.days.values.1.circle'
    TUESDAY("2"),//this reffer to the data-automation-id of sunday 'days_and_times.{0}.days.values.2.circle'
    WEDNESDAY("3"),//this reffer to the data-automation-id of sunday 'days_and_times.{0}.days.values.3.circle'
    THURSDAY("4"),//this reffer to the data-automation-id of sunday 'days_and_times.{0}.days.values.4.circle'
    FRIDAY("5"),//this reffer to the data-automation-id of sunday 'days_and_times.{0}.days.values.5.circle'
    SATURDAY("6")//this reffer to the data-automation-id of sunday 'days_and_times.{0}.days.values.6.circle'
    ;

    private final String index;

    Days(final String index) {
        this.index = index;
    }

    public String index() {
        return index;
    }

}
