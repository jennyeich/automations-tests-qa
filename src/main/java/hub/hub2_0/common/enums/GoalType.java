package hub.hub2_0.common.enums;

public enum GoalType {

    EnlargeSpendPerVisit("Enlarge Spend Per Visit"),
    IncreaseVisitFrequency("Increase Visit Frequency"),
    ;

    private final String name;
    GoalType(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
