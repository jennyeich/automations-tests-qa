package hub.hub2_0.common.enums;

public enum ActivityType {

    Rule("Rule"),
    Deal("Deal"),
    ScheduledAction("Schedule")
    ;
    private final String name;
    ActivityType(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }



}








