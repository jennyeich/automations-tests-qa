package hub.hub2_0.common.enums;

public enum YesNo{
    Yes("yes"),
    No("no")
    ;

    private final String name;
    YesNo(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
