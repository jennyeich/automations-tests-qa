package hub.hub2_0.common.enums;


import hub.hub2_0.common.filters.IFilter;

public enum CampaignFilterView implements IFilter {
    RULE("Rule"),
    DEAL("Deal"),
    ONE_TIME_ACTION("One time action"),
    ACTIVE("Active"),
    COMPLETED("Completed"),
    STOPPED("Stopped"),
    FUTURE("Future"),
    DRAFT("Draft"),
    ARCHIVED_CAMPAIGNS("Archived campaigns")
    ;

    private final String index;

    CampaignFilterView(final String index) {
        this.index = index;
    }

    public String index() {
        return index;
    }
}
