package hub.hub2_0.common.enums;


import hub.common.objects.smarts.smarObjects.triggers.ITrigger;

/**
 * Created by Sharon on 14/12/2017.
 */
public enum ActivityTrigger implements ITrigger {



    //PURCHASE & PAYMENTS
    MadeAPurchase("purchaseAnalyzed"),
    PaysWithPointsAtPOS("paysWithPointsAtPos"),
    PaysWithCreditAtPOS("paysWithCreditAtPos"),
    PaysWithAppCreditCard("paysWithAppCreditCard"),
    PaysWithPayPal("paysWithPayPal"),
    AppCreditCardPaymentIsDeclined("appCreditCardPaymentIsDeclined"),
    PayPalPaymentIsDeclined("payPalPaymentIsDeclined"),
    OpenPurchaseEvent("startPurchase"),
    //MEMBERSHIP
    JoinTheClub("joinedClub"),
    UpdatedMembershipDetails("updateMembership"),
    Tagged("tagOperation"),
    Untagged("untagOperation"),
    //APP
    OpenedTheApp("openedClub"),
    LaunchedTheApp("joinedapp"),
    SharedToFacebook("fbPost"),
    FilledOutForm("formFilled"),
    TappedClaimButton("claim"),
    //BENEFITS
    ReceivesAnAsset("receivedAsset"),
    RedeemsAnAsset("redeem"),
    PurchaseAnAssetPointShop("purchasedAsset"),
    PunchesAPunchCard("punch"),
    OverflowsPunchCard("exceededPunch"),
    ReceivesPoints("receivedPoints"),
    UsePoints("usedPoints"),
    ReceivesCredit("receivedCredit"),
    UsesCredit("usedCredit"),
    //CODES
    EnteredCouponCode("oneTimeCode"),
    ScannedCode("codeGenerator"),
    //CONNECTIVITY
    ReceivedBeaconSignal("beaconSignal"),
    ExternalEventSubmitted("externalEvent"),
    UnsubscribedFromSMS("unsubscribe"),
    //FRIEND_REFERRAL
    JoinedTheClubUsingReferredCode("joinedClubWithReferralCode"),
    UsedMyReferredCode("usedMyReferralCode")

    ;

    private final String name;
    ActivityTrigger(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }



}