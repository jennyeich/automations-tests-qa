package hub.hub2_0.common.enums;

public enum Currency {

    AE("د.إ."),
    BR("R$"),
    CL("$"),
    CO("$"),
    DK("kr."),
    GB("£"),
    IE("€"),
    IL("₪"),
    IT("€"),
    KZ("₸"),
    MX("$"),
    PA("B/."),
    SE("kr"),
    TR("₺"),
    US("$"),
    ZA("R");

    private final String sign;

    Currency(final String sign) {
        this.sign = sign;
    }
    //return the data inside the () - the sign for the given isoCode
    public String getSign() {
        return sign;
    }
}
