package hub.hub2_0.common.enums;

public enum ItemOperation {

    ACTIVATE("Activate"),
    STOP("Stop"),
    DETAILS("details")
    ;

    private final String name;
    ItemOperation(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
