package hub.hub2_0.common.conditions.global.dateTime;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;

public class BetweenDates extends BaseSeleniumObjectHub2 {

    private static final String START_BTN = "globalConditions.dateTime.datesRange.start.input";
    private static final String END_BTN = "globalConditions.dateTime.datesRangeCondition.end.input";

    @SeleniumPath(value = START_BTN, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String startBtn;

    private DatePicker datePickerStart;

    @SeleniumPath(value = END_BTN, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String endBtn;

    private DatePicker datePickerEnd;


    public DatePicker getDatePickerStart() {
        return datePickerStart;
    }

    public BetweenDates setDatePickerStart(DatePicker datePickerStart) {
        this.startBtn = "open start";
        this.datePickerStart = datePickerStart;
        return this;
    }

    public DatePicker getDatePickerEnd() {
        return datePickerEnd;
    }

    public BetweenDates setDatePickerEnd(DatePicker datePickerEnd) {
        this.endBtn = "open end";
        this.datePickerEnd = datePickerEnd;
        return this;
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public static BetweenDates newBetweenDates() {
        return new BetweenDates();
    }

    public void setCloseBtn() {
        if(this.datePickerEnd != null)
            this.datePickerStart.setClose(null);
    }
}
