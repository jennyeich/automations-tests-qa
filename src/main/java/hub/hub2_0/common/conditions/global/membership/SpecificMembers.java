package hub.hub2_0.common.conditions.global.membership;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;

import java.util.ArrayList;
import java.util.List;

public class SpecificMembers extends BaseSeleniumObjectHub2 {


    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<MemberFieldCondition> memberFieldConditions;

    private MembershipStatus membershipStatus;
    private int conditionsCounter = 0;


    public MembershipStatus getMembershipStatus() {
        return membershipStatus;
    }

    public void setMembershipStatus(MembershipStatus membershipStatus) {
        this.membershipStatus = membershipStatus;
        this.membershipStatus.openAppliesTo();
    }

    public SpecificMembers setConditions(MemberFieldCondition... memberFieldConditions) {
        for(MemberFieldCondition condition : memberFieldConditions) {
            if(conditionsCounter > 0)
                condition.clickAdd();
            addCondition(condition);
        }
        return this;
    }


    private SpecificMembers addCondition(MemberFieldCondition condition) {
        condition.setNext(conditionsCounter);
        getMemberFieldConditions().add(condition);
        conditionsCounter++;
        return this;
    }

    public List<MemberFieldCondition> getMemberFieldConditions() {
        if(memberFieldConditions == null)
                memberFieldConditions = new ArrayList<>();
        return memberFieldConditions;
    }


    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public static SpecificMembers newSpecificMembers() {
        return new SpecificMembers();
    }

    @Override
    public int getParentIndex() {
        return 0;
    }
}
