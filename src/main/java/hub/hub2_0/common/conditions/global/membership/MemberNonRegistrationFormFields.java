package hub.hub2_0.common.conditions.global.membership;

public enum MemberNonRegistrationFormFields {
    Points("Points"),
    BudgetWeighted("BudgetWeighted"),
    BirthdayMonth("BirthdayMonth"),
    AnniversaryMonth("AnniversaryMonth"),
    Tags("Tag"),
    PushNotificationEnabled("PushNotificationEnabled"),
    LocationEnabled("LocationEnabled"),
    MobileAppUsed("MobileAppUsed"),
    AllowEmail("AllowEmail"),
    FavoriteBranchID("FavoriteBranchID"),
    AllowSMS("AllowSMS"),
    CodeTagNewValue("CodeTag[0].NewValue"),
    UpdatedDuringRegistration("Operation");

    private final String name;

    MemberNonRegistrationFormFields(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name.replace(" ","");
    }

}
