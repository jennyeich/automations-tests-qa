package hub.hub2_0.common.conditions.global.dateTime;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class DatePicker extends BaseSeleniumObjectHub2 {
    int counter = 0;
    private static final String START_NOW = "globalConditions.dateTime.datesRange.start.now";
    private static final String END_FOREVER = "globalConditions.dateTime.datesRangeCondition.end.forever";

    private static final String SELECT_DAY = "(//div[@aria-label=\"day-%s\"])";
    //dummy field to click outside frame of date picker for setting the selection - instead of save
    private static final String CLOSE = "globalConditions.dateTime.datesRangeCondition.end.input";
    private static final String AT_HOUR = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"hour_and_minute.0.hour.cycle_counter\")]";
    private static final String AT_MIN = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"hour_and_minute.0.minute.cycle_counter\")]";
    private static final String AT_AMPM = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"ampm.{0}\")]";

    @SeleniumPath(value = START_NOW, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String startNow;

    @SeleniumPath(value = SELECT_DAY, type = SeleniumPath.Type.BY_XPATH2, elementInputType = SeleniumPath.ElementInputType.CLICK_IF_ENABLED)
    private String selectStartDay;

    @SeleniumPath(value = AT_HOUR, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_TIME)
    private String startAtHour;

    @SeleniumPath(value = AT_MIN, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_TIME)
    private String startAtMin;

    @SeleniumPath(value = AT_AMPM, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_INNER_ELEMENT_HUB2)
    private TimeSelector.TimeMeridiem startAtAmPm;

    @SeleniumPath(value = SELECT_DAY, type = SeleniumPath.Type.BY_XPATH2, elementInputType = SeleniumPath.ElementInputType.CLICK_IF_ENABLED)
    private String selectEndDay;

    @SeleniumPath(value = AT_HOUR, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_TIME)
    private String endAtHour;

    @SeleniumPath(value = AT_MIN, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_TIME)
    private String endAtMin;

    @SeleniumPath(value = AT_AMPM, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_INNER_ELEMENT_HUB2)
    private TimeSelector.TimeMeridiem endAtAmPm;

    @SeleniumPath(value = END_FOREVER, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String endForever;

    @SeleniumPath(value = CLOSE, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String releaseDatePicker = "Y";

    @SeleniumPath(value = CLOSE, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_ESC)
    private String close = "Y";

    public String getClose() {
        return close;
    }

    public void setClose(String close) {
        this.close = close;
    }

    public String getStartNow() {
        return startNow;
    }

    public DatePicker clickStartNow() {
        this.startNow = "start now";
        return this;
    }

    public String getEndForever() {
        return endForever;
    }

    public DatePicker clickEndForever() {
        this.endForever = "endForever";
        return this;
    }

    public String getSelectStartDay() {
        return selectStartDay;
    }

    public DatePicker setSelectStartDay(String selectStartDay) {
        this.selectStartDay = String.format(SELECT_DAY, selectStartDay);
        return this;
    }

    public String getSelectEndDay() {
        return selectEndDay;
    }

    public DatePicker setSelectEndDay(String selectEndDay) {
        this.selectEndDay = String.format(SELECT_DAY, selectEndDay);
        return this;
    }


    public String getStartAtHour() {
        return startAtHour;
    }

    public String getStartAtMin() {
        return startAtMin;
    }

    public TimeSelector.TimeMeridiem getStartAtAmPm() {
        return startAtAmPm;
    }


    public DatePicker setStartAtHour(String startAtHour) {
        this.startAtHour = startAtHour;
        return this;
    }

    public DatePicker setStartAtMin(String startAtMin) {
        this.startAtMin = startAtMin;
        return this;
    }

    public DatePicker setStartAtAmPm(TimeSelector.TimeMeridiem startAtAmPm) {
        this.startAtAmPm = startAtAmPm;
        return this;
    }


    public String getEndAtHour() {
        return endAtHour;
    }

    public String getEndAtMin() {
        return endAtMin;
    }

    public TimeSelector.TimeMeridiem getEndAtAmPm() {
        return endAtAmPm;
    }

    public DatePicker setEndAtHour(String endAtHour) {
        this.endAtHour = endAtHour;
        return this;
    }

    public DatePicker setEndAtMin(String endAtMin) {
        this.endAtMin = endAtMin;
        return this;
    }

    public DatePicker setEndAtAmPm(TimeSelector.TimeMeridiem endAtAmPm) {
        this.endAtAmPm = endAtAmPm;
        return this;
    }

    public static DatePicker newDatePicker() {
        return new DatePicker();
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        this.counter = i;
    }

    @Override
    public int getNext() {
        return this.counter;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
