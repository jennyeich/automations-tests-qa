package hub.hub2_0.common.conditions.global;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.basePages.loyalty.GlobalCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;

public class DateTimeGlobalCondition extends GlobalCondition {

    private static final String GLC_DATE_TIME = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.dateTime\"]";
    private static final String ANY_DATE_TIME = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.dateTime.radioBtn.any\"]";
    private static final String SPECIFIC_DATE_TIME = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.dateTime.radioBtn.specificDateTime\"]";
    private static final String BTN_CANCEL = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.dateTime.btnCancel\"]";
    private static final String BTN_SAVE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.dateTime.btnSave\"]";

    @SeleniumPath(value = GLC_DATE_TIME, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addGLCDateTime;
    @SeleniumPath(value = ANY_DATE_TIME, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String anyDateTimeChk;
    @SeleniumPath(value = SPECIFIC_DATE_TIME, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String specificDateTimeChk;

    private SpecificDateTime specificDateTime;

    @SeleniumPath(value = BTN_CANCEL, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String cancelBtn;
    @SeleniumPath(value = BTN_SAVE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String saveBtn;

    @Override
    public void clickGlobalCondition() {
        this.addGLCDateTime = "Select global condition Date & Time";
    }

    public String getAnyDateTimeChk() {
        return anyDateTimeChk;
    }

    public void clickAny() {
        this.anyDateTimeChk = "Register Members checkbox is selected";
        this.specificDateTimeChk = null;
    }

    public String getSpecificDateTimeChk() {
        return specificDateTimeChk;
    }

    public void clickSpecificDateTimeChk() {
        this.specificDateTimeChk = "Specific Date and Time checkbox is selected";
        this.anyDateTimeChk = null;

    }

    public SpecificDateTime getSpecificDateTime() {
        return specificDateTime;
    }

    public void setSpecificDateTime(SpecificDateTime specificDateTime) {
        this.specificDateTime = specificDateTime;

    }

    public String getCancelBtn() {
        return cancelBtn;
    }

    @Override
    public void clickCancel() {
        this.cancelBtn = "Cancel button clicked";
        this.saveBtn = null;
    }

    public String getSaveBtn() {
        return saveBtn;
    }

    @Override
    public void clickSave() {
        this.saveBtn = "Save button clicked";
        this.cancelBtn = null;
    }
}
