package hub.hub2_0.common.conditions.global;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.basePages.loyalty.GlobalCondition;
import hub.hub2_0.common.conditions.global.membership.SpecificMembers;

public class MembershipGlobalCondition extends GlobalCondition {

    private static final String GLC_MEMBERS = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members\"]";
    private static final String ALL_MEMBERS = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.radioBtn.allMembers\"]";
    private static final String REGISTER_MEMBERS = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.radioBtn.activeMembers\"]";
    private static final String SPECIFIC_MEMBERS = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.radioBtn.specificMembers\"]";
    private static final String BTN_CANCEL = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.btnCancel\"]";
    private static final String BTN_SAVE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.btnSave\"]";

    @SeleniumPath(value = GLC_MEMBERS, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addGLCMembers;

    @SeleniumPath(value = ALL_MEMBERS, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String allMembersChk;
    @SeleniumPath(value = REGISTER_MEMBERS, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String registerMembersChk;
    @SeleniumPath(value = SPECIFIC_MEMBERS, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String specificMembersChk;

    private SpecificMembers specificMembers;

    @SeleniumPath(value = BTN_CANCEL, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String cancelBtn;
    @SeleniumPath(value = BTN_SAVE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String saveBtn;

    @Override
    public void clickGlobalCondition() {
        this.addGLCMembers = "Select global condition members";
    }

    public String getAllMembersChk() {
        return allMembersChk;
    }

    public void clickAllMembersBtn() {
        this.allMembersChk = "All Members checkbox is selected";
        this.registerMembersChk = null;
        this.specificMembersChk = null;
    }

    public String getActiveMembersBtn() {
        return registerMembersChk;
    }

    public void clickRegiterMembersBtn() {
        this.registerMembersChk = "Register Members checkbox is selected";
        this.specificMembersChk = null;
        this.allMembersChk = null;
    }

    public String getSpecificMembersChk() {
        return specificMembersChk;
    }

    public void clickSpecificMembersBtn() {
        this.specificMembersChk = "Specific Members checkbox is selected";
        this.allMembersChk = null;
        this.registerMembersChk = null;
    }

    public SpecificMembers getSpecificMembers() {
        return specificMembers;
    }

    public void setSpecificMembers(SpecificMembers specificMembers) {
        this.specificMembers = specificMembers;

    }

    public String getCancelBtn() {
        return cancelBtn;
    }

    @Override
    public void clickCancel() {
        this.cancelBtn = "Cancel button clicked";
        this.saveBtn = null;
    }

    public String getSaveBtn() {
        return saveBtn;
    }
    @Override
    public void clickSave() {
        this.saveBtn = "Save button clicked";
        this.cancelBtn = null;
    }
}
