package hub.hub2_0.common.conditions.global.dateTime;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class TimeSelector extends BaseSeleniumObjectHub2 {


    private static final String FROM_HOUR_INPUT = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}.hour_and_minute.0.hour.cycle_counter\")]/input";
    private static final String FROM_MINUTE_INPUT =  "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}.hour_and_minute.0.minute.cycle_counter\")]/input";
    private static final String FROM_AMPM =  "(//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}.ampm.{1}\")])[1]";
    private static final String TO_HOUR_INPUT = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}.hour_and_minute.1.hour.cycle_counter\")]/input";
    private static final String TO_MINUTE_INPUT =  "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}.hour_and_minute.1.minute.cycle_counter\")]/input";
    private static final String TO_AMPM =  "(//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}.ampm.{1}\")])[2]";
    private static final String OPEN_TIME_FROM_TO = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}.represent_time_from_to\")]";
    private static final String ALL_DAY = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}.all_day\")]";


    @SeleniumPath(value = OPEN_TIME_FROM_TO, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String timeFromTo = "open time select";//to click

    @SeleniumPath(value = ALL_DAY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String allDay;

    @SeleniumPath(value = FROM_HOUR_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_TIME)
    private String fromHourInput;
    @SeleniumPath(value = FROM_MINUTE_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_TIME)
    private String fromMinuteInput;
    @SeleniumPath(value = FROM_AMPM, type = SeleniumPath.Type.BY_GENERATE_XPATH_ONE_CLICK, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2)
    private TimeMeridiem fromAmPm;
    @SeleniumPath(value = TO_HOUR_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_TIME)
    private String toHourInput;
    @SeleniumPath(value = TO_MINUTE_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_TIME)
    private String toMinuteInput;
    @SeleniumPath(value = TO_AMPM, type = SeleniumPath.Type.BY_GENERATE_XPATH_ONE_CLICK, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2)
    private TimeMeridiem toAmPm;


    private int counter = 0;

    public String getFromHourInput() {
        return fromHourInput;
    }

    public TimeSelector setFromHourInput(String fromHourInput) {
        this.fromHourInput = fromHourInput;
        return this;
    }

    public String getFromMinuteInput() {
        return fromMinuteInput;
    }

    public TimeSelector setFromMinuteInput(String fromMinuteInput) {
        this.fromMinuteInput = fromMinuteInput;
        return this;
    }

    public String getToHourInput() {
        return toHourInput;
    }

    public TimeSelector setToHourInput(String toHourInput) {
        this.toHourInput = toHourInput;
        return this;
    }

    public String getToMinuteInput() {
        return toMinuteInput;
    }

    public TimeSelector setToMinuteInput(String toMinuteInput) {
        this.toMinuteInput = toMinuteInput;
        return this;
    }

    public TimeMeridiem getFromAmPm() {
        return fromAmPm;
    }

    public TimeSelector setFromAmPm(TimeMeridiem fromAmPm) {
        this.fromAmPm = fromAmPm;
        return this;
    }

    public TimeMeridiem getToAmPm() {
        return toAmPm;
    }

    public TimeSelector setToAmPm(TimeMeridiem toAmPm) {
        this.toAmPm = toAmPm;
        return this;
    }

    public String getAllDay() {
        return allDay;
    }

    public TimeSelector setAllDay() {
        this.allDay = "select all day";
        return this;
    }
    public static TimeSelector newTimeSelector() {

        return new TimeSelector();
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        this.counter = i;
    }

    @Override
    public int getNext() {
        return this.counter;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public static DaysCondition newDayCondition() {
        return new DaysCondition();
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

    public enum TimeMeridiem{
        AM("am"),
        PM("pm");
        private final String name;
        TimeMeridiem(final String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }


    }
}
