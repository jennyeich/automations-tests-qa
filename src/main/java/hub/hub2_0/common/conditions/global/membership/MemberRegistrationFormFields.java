package hub.hub2_0.common.conditions.global.membership;

public enum MemberRegistrationFormFields {
    PhoneNumber("PhoneNumber"),
    Email("Email"),
    FirstName("FirstName"),
    LastName("LastName"),
    Gender("Gender"),
    MemberID("MemberID"),
    GenericString1("GenericString1"),
    GenericString2("GenericString2"),
    GenericInteger2("GenericInteger2"),
    GenericDate2("GenericDate2")
    ;


    private final String name;

    MemberRegistrationFormFields(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name.replace(" ","");
    }

}
