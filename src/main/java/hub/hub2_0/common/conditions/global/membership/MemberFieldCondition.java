package hub.hub2_0.common.conditions.global.membership;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class MemberFieldCondition extends BaseSeleniumObjectHub2 {

    private static final String ADD_GLOBAL_CONDITION = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.add-global-condition\"]";
    private static final String IDENTIFY_BY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.condition.{0}.fields.select\"]";
    private static final String FIELDS_SELECT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.{0}.conditionKey.context.Membership.{1}\"]";
    public static final String OPERATOR_KEY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.condition.{0}.conditionsOperator.{1}\"]";
    public static final String CONDITION_VALUE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.condition.{0}.conditionValue\"]/input";
    private static final String CONDITION_VALUE_BOOLEAN = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.condition.{0}.conditionValue.{1}\"]";
    public static final String OPEN_VALUE_SELECT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.condition.{0}.conditionValue\"]";
    public static final String CONDITION_VALUE_SELECT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.condition.{0}.conditionValue.{1}\"]";

    @SeleniumPath(value = ADD_GLOBAL_CONDITION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addCondition;

    @SeleniumPath(value = IDENTIFY_BY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String identifyBy;
    @SeleniumPath(value = FIELDS_SELECT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String fieldsSelect;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY)
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_CAPSULA_HUB2, readValue = CONDITION_VALUE)
    private String conditionValue;
    @SeleniumPath(value = CONDITION_VALUE_BOOLEAN, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2 )
    private String booleanValue;
    @SeleniumPath(value = OPEN_VALUE_SELECT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM )
    private String openValueSelect;
    @SeleniumPath(value = CONDITION_VALUE_SELECT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.MULTI_SELECT_HUB2, readValue = CONDITION_VALUE_SELECT)
    private StringBuffer selectValue;

    private int counter = 0;

    public void clickAdd() {
        this.addCondition = "Click add member field condition";
    }

    public String getFieldsSelect() {
        return fieldsSelect;
    }

    public MemberFieldCondition setFieldsSelect(String field) {
        if(!isInCase())
            this.identifyBy = "Click to open fields list";
        this.fieldsSelect = field;
        return this;
    }

    public String getOperatorKey() {
        return operatorKey;
    }

    public MemberFieldCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey;
        return this;
    }

    public StringBuffer getConditionSelectValue() {
        return selectValue;
    }

    public MemberFieldCondition setConditionSelectValue(String... values) {
        this.openValueSelect = "select value";
        this.selectValue = new StringBuffer();
        for (String val:values) {
            this.selectValue.append(val).append(",");
        }
        this.selectValue.toString();
        return this;
    }

    public String getConditionValue() {
        return conditionValue;
    }

    public MemberFieldCondition setConditionValue(String conditionValue) {
        this.conditionValue = conditionValue;
        return this;
    }

    public MemberFieldCondition toggleBooleanValue(String selction) {
        this.booleanValue = selction;
        this.conditionValue = null;
        return this;
    }

    public String getBooleanValue() {
        return booleanValue;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        this.counter = i;
    }

    @Override
    public int getNext() {
        return this.counter;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public static MemberFieldCondition newCondition() {
        return new MemberFieldCondition();
    }

    @Override
    public int getParentIndex() {
        return 0;
    }
}
