package hub.hub2_0.common.conditions.global.limits;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.basePages.loyalty.GlobalCondition;

public class LimitsGlobalCondition extends GlobalCondition {

    private static final String GLC_INFINITY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.applySeveralTimes.trigger\"]";
    private static final String INFINITE_PURCHASE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.applySeveralTimes.infinite\"]";
    private static final String LIMITED_NUMBER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.applySeveralTimes.limitedPerMember\"]";
    private static final String BTN_CANCEL = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.applySeveralTimes.btnCancel\"]";
    private static final String BTN_SAVE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.applySeveralTimes.btnSave\"]";

    @SeleniumPath(value = GLC_INFINITY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addGLCInfinity;

    @SeleniumPath(value = LIMITED_NUMBER, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String limitedNumber;

    @SeleniumPath(value = INFINITE_PURCHASE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String infinitePurchase;

    @SeleniumPath(value = BTN_CANCEL, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String cancelBtn;

    private LimitedNumber limitedNumberOfPurchases;

    @SeleniumPath(value = BTN_SAVE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String saveBtn;

    @Override
    public void clickGlobalCondition() {
        this.addGLCInfinity = "Select global condition Limits";
    }

    public void clickCancel() {
        this.cancelBtn = "Cancel button clicked";
        this.saveBtn = null;
    }

    @Override
    public void clickSave() {
        this.saveBtn = "Save button clicked";
        this.cancelBtn = null;
    }

    public void clickInfinitePurchase(){
        this.infinitePurchase= "Select condition Infinite purchase per member";
    }

    public void clickLimitedNumber(){
        this.limitedNumber= "Select condition limited number per member";
    }

    public static LimitedNumber newLimitedNumber() {
        return new LimitedNumber();
    }


    public String getCancelBtn() {
        return cancelBtn;
    }
    public String getSaveBtn() {
        return saveBtn;
    }

    public LimitedNumber getLimitedNumberOfPurchases() {
        return limitedNumberOfPurchases;
    }

    public void setLimitedNumberOfPurchases(LimitedNumber limitedNumberOfPurchases) {
        this.limitedNumberOfPurchases = limitedNumberOfPurchases;
    }
}
