package hub.hub2_0.common.conditions.global.membership;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class MembershipStatus extends BaseSeleniumObjectHub2 {

    public final static Boolean CHECK = true;
    public final static Boolean UNCHECK = false;

    private static final String APPLIES_TO = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.memberStatus.fields.select\"]";
    private static final String REGISTER_CHK = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.memberStatus.fields.registered\"]";
    private static final String PENDING_CHK = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.memberStatus.fields.pending\"]";
    private static final String NON_REGISTER_CHK = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.memberStatus.fields.not_registered\"]";
    private static final String SAVE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.memberStatus.save\"]";
    private static final String CANCEL = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.members.memberStatus.cancel\"]";

    @SeleniumPath(value = APPLIES_TO, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String appliesTo;

    @SeleniumPath(value = REGISTER_CHK, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String registerChk = "select Register checkbox";//By default this is selected
    @SeleniumPath(value = PENDING_CHK, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String pendingChk;
    @SeleniumPath(value = NON_REGISTER_CHK, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String nonRegisterChk;

    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String saveBtn;
    @SeleniumPath(value = CANCEL, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String cancelBtn;

    public void openAppliesTo(){
        this.appliesTo = "Open Applies to dialog";
    }
    public String getRegisterChk() {
        return registerChk;
    }

    public MembershipStatus setRegister(boolean check) {
        if( (this.registerChk != null  && check) || (this.registerChk == null  && !check) )
            this.registerChk = null;
        else{
            this.registerChk = "select Register checkbox";
        }
        clickSaveBtn();
        return this;
    }

    public String getPendingChk() {
        return pendingChk;
    }

    public MembershipStatus setPending(boolean check) {
        if( (this.pendingChk != null  && check) || (this.pendingChk == null  && !check) )
            this.pendingChk = null;
        else{
            this.pendingChk = "select Pending checkbox";
        }
        clickSaveBtn();
        return this;
    }

    public String getNonRegisterChk() {
        return nonRegisterChk;
    }

    public MembershipStatus setNonRegister(boolean check) {
        if( (this.nonRegisterChk != null  && check) || (this.nonRegisterChk == null  && !check) )
            this.nonRegisterChk = null;
        else{
            this.nonRegisterChk = "select Non-Register checkbox";
        }
        clickSaveBtn();
        return this;
    }

    public static MembershipStatus newMembershipStatus(){
        return new MembershipStatus();
    }

    public String getSaveBtn() {
        return saveBtn;
    }

    public void clickSaveBtn() {
        this.saveBtn = "click save";
        this.cancelBtn = null;
    }

    public String getCancelBtn() {
        return cancelBtn;
    }

    public void clickCancelBtn() {
        this.cancelBtn = "click cancel";
        this.saveBtn = null;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }


    @Override
    public int getParentIndex() {
        return 0;
    }
}
