package hub.hub2_0.common.conditions.global.limits;

public enum RollingPeriod {

    Minute("minute"),
    Hour("hour"),
    Day("day"),
    Week("week"),
    Month("month"),
    Year("year"),
    RollingPeriod("rollingPeriod");

    private final String name;
    RollingPeriod(final String name) {
        this.name = name;
    }

    @Override
    public String toString(){
        return name;
    }
}


