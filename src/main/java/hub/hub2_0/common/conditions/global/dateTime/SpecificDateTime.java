package hub.hub2_0.common.conditions.global.dateTime;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;

import java.util.ArrayList;
import java.util.List;

public class SpecificDateTime extends BaseSeleniumObjectHub2 {


    private BetweenDates betweenDates;

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<DaysCondition> daysConditions;

    private int conditionsCounter = 0;

    public BetweenDates getBetweenDates() {
        if(betweenDates == null)
            betweenDates = new BetweenDates();
        return betweenDates;
    }

    public SpecificDateTime setDatePickerStart(DatePicker datePickerStart) {
        datePickerStart.setNext(-1); // set to -1 in case the date Picker appear only once in the page.
        getBetweenDates().setDatePickerStart(datePickerStart);
        return this;
    }

    public SpecificDateTime setDatePickerEnd(DatePicker datePickerEnd) {
        datePickerEnd.setNext(-1); // set to -1 in case the date Picker appear only once in the page.
        getBetweenDates().setDatePickerEnd(datePickerEnd);
        return this;
    }

    public DatePicker getDatePickerStart() {
        return getBetweenDates().getDatePickerStart();
    }

    public DatePicker getDatePickerEnd() {
        return getBetweenDates().getDatePickerEnd();
    }




    public SpecificDateTime setDaysConditions(DaysCondition... daysConditions) {
        for(DaysCondition condition : daysConditions) {
            if(conditionsCounter > 0)
                condition.clickAdd();
            addDaysCondition(condition);
        }
        return this;
    }


    private SpecificDateTime addDaysCondition(DaysCondition condition) {
        condition.setNext(conditionsCounter);
        if(condition.getTimeSelector() != null)
            condition.getTimeSelector().setNext(conditionsCounter);
        getDaysConditions().add(condition);
        conditionsCounter++;
        return this;
    }

    public List<DaysCondition> getDaysConditions() {
        if(daysConditions == null)
            daysConditions = new ArrayList<>();
        return daysConditions;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public static SpecificDateTime newSpecificDateTime() {
        return new SpecificDateTime();
    }

    @Override
    public int getParentIndex() {
        return 0;
    }
}
