package hub.hub2_0.common.conditions.global.dateTime;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.Days;

public class DaysCondition extends BaseSeleniumObjectHub2 {

    private static final String ADD_GLOBAL_CONDITION = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.dateTime.add-global-condition\"]";

    private static final String SUNDAY = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR +  ",\"{0}.days.0\")]";
    private static final String MONDAY = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR +  ",\"{0}.days.1\")]";
    private static final String TUESDAY = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}.days.2\")]";
    private static final String WEDNESDAY = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}.days.3\")]";
    private static final String THURSDAY = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}.days.4\")]";
    private static final String FRIDAY = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}.days.5\")]";
    private static final String SATURDAY = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}.days.6\")]";



    @SeleniumPath(value = ADD_GLOBAL_CONDITION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addCondition;

    //All days are
    @SeleniumPath(value = SUNDAY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String sunday;
    @SeleniumPath(value = MONDAY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String monday;
    @SeleniumPath(value = TUESDAY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String tuesday;
    @SeleniumPath(value = WEDNESDAY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String wednesday ;
    @SeleniumPath(value = THURSDAY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String thursday;
    @SeleniumPath(value = FRIDAY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String friday;
    @SeleniumPath(value = SATURDAY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String saturday;

    private TimeSelector timeSelector;

    private int counter = 0;

    public void clickAdd() {
        this.addCondition = "Click add days and times condition";
    }

    public DaysCondition unSelectDays(Days... days) {
        for (Days day:days){
            if(day == Days.SUNDAY) this.sunday = Days.SUNDAY.name();
            if(day == Days.MONDAY) this.monday = Days.MONDAY.name();
            if(day == Days.TUESDAY) this.tuesday =  Days.TUESDAY.name();
            if(day == Days.WEDNESDAY) this.wednesday =  Days.WEDNESDAY.name();
            if(day == Days.THURSDAY) this.thursday =  Days.THURSDAY.name();
            if(day == Days.FRIDAY) this.friday =  Days.FRIDAY.name();
            if(day == Days.SATURDAY) this.saturday =  Days.SATURDAY.name();
        }
        return this;
    }

    public String getSunday() {
        return sunday;
    }

    public String getMonday() {
        return monday;
    }

    public String getTuesday() {
        return tuesday;
    }

    public String getWednesday() {
        return wednesday;
    }

    public String getThursday() {
        return thursday;
    }

    public String getFriday() {
        return friday;
    }

    public String getSaturday() {
        return saturday;
    }

    public TimeSelector getTimeSelector() {
        return timeSelector;
    }

    public DaysCondition setTimeSelector(TimeSelector timeSelector) {
        this.timeSelector = timeSelector;
        return this;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        this.counter = i;
    }

    @Override
    public int getNext() {
        return this.counter;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public static DaysCondition newDayCondition() {
        return new DaysCondition();
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

}
