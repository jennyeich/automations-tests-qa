package hub.hub2_0.common.conditions.global.limits;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class LimitedNumber extends BaseSeleniumObjectHub2 {

    private static final String UP_TO_TIMES_IN_TOTAL_CHK = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.applySeveralTimes.limitedPerMember.totalLimit.enable\"]";
    private static final String UP_TO_TIMES_IN_TOTAL_VAL = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.applySeveralTimes.limitedPerMember.totalLimit.count\"]/input";
    private static final String UP_TO_TIMES_BY_PERIOD_CHK = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.applySeveralTimes.limitedPerMember.byPeriod.enable\"]";
    private static final String TIMES_PER_PERIOD_VAL ="//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.applySeveralTimes.limitedPerMember.byPeriod.count\"]/input";
    private static final String UP_TO_TIMES_BY_PERIOD = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.applySeveralTimes.limitedPerMember.byPeriod.timesPer\"]";
    private static final String PERIOD = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.applySeveralTimes.limitedPerMember.byPeriod.timesPer.{0}\"]";
    private static final String PERIOD_MULTIPLIER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"globalConditions.applySeveralTimes.limitedPerMember.byPeriod.multiplier\"]/input";

    @SeleniumPath(value = UP_TO_TIMES_IN_TOTAL_CHK, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String upToTimesInTotalChk;
    @SeleniumPath(value = UP_TO_TIMES_IN_TOTAL_VAL, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT)
    private String timesInTotalVal;
    @SeleniumPath(value = UP_TO_TIMES_BY_PERIOD_CHK, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String upToTimesByPeriodChk;
    @SeleniumPath(value = TIMES_PER_PERIOD_VAL, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT)
    private String timePerPeriodVal;
    @SeleniumPath(value = UP_TO_TIMES_BY_PERIOD, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = UP_TO_TIMES_BY_PERIOD)
    private String upToTimesByPeriod;
    @SeleniumPath(value = PERIOD, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2, readValue = PERIOD)
    private String period;
    @SeleniumPath(value = PERIOD_MULTIPLIER, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = PERIOD_MULTIPLIER)
    private String periodMultiplier;


    public LimitedNumber setUpToTimesInTotal(boolean check) {
        if( (this.upToTimesInTotalChk != null  && check) || (this.upToTimesInTotalChk == null  && !check) )
            this.upToTimesInTotalChk = null;
        else{
            this.upToTimesInTotalChk = "select Up to times in total checkbox";
        }
        return this;
    }

    public LimitedNumber setUpToTimesByPeriod(boolean check) {
        if( (this.upToTimesByPeriodChk != null  && check) || (this.upToTimesByPeriodChk == null  && !check) )
            this.upToTimesByPeriodChk = null;
        else{
            this.upToTimesByPeriodChk = "select Up to times by period checkbox";
        }
        return this;
    }

    public String getUpToTimesByPeriod() {
        return upToTimesByPeriodChk;
    }

    public LimitedNumber setTimesByPeriod(String period) {
        this.upToTimesByPeriod = "Click on Time period";
        this.period = period;
        return this;
    }


    public String getTimePerPeriodVal() {
        return timePerPeriodVal;
    }

    public LimitedNumber setTimePerPeriodVal(String timePerPeriodVal) {
        this.timePerPeriodVal = timePerPeriodVal;
        return this;
    }

    public String getTimesInTotalVal() {
        return timesInTotalVal;
    }

    public LimitedNumber setTimesInTotalVal(String timesInTotalVal) {
        this.timesInTotalVal = timesInTotalVal;
        return this;
    }


    public String getPeriodMultiplier() {
        return periodMultiplier;
    }

    public LimitedNumber setPeriodMultiplier(String periodMultiplier) {
        this.periodMultiplier = periodMultiplier;
        return this;
    }



    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public int getParentIndex() {
        return 0;
    }


}
