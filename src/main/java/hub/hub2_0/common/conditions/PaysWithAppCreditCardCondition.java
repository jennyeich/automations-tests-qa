package hub.hub2_0.common.conditions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class PaysWithAppCreditCardCondition extends ActivityCondition implements Monetary{

    public static final String PAYMENT_FLOW_VALUE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue.{1}\"]";

    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addCondition;
    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = CONDITION_KEY)
    private String conditionKey;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY)
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = CONDITION_VALUE)
    private String conditionValue;

    @SeleniumPath(value = PAYMENT_FLOW_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = PAYMENT_FLOW_VALUE)
    private String paymentFlow;


    public enum ConditionKey implements hub.hub2_0.common.conditions.ConditionKey {
        PAYMENT_AMOUNT(Hub2Constants.PAYMENT_AMOUNT),
        PAYMENT_FLOW(Hub2Constants.PAYMENT_FLOW),
        ITEM_CODE(Hub2Constants.ITEM_CODE);

        private final String conditionName;
        ConditionKey(final String name) {
            this.conditionName = name;
        }

        @Override
        public String toString() {
            return conditionName;
        }
    }

    public enum PaymentFlows {
        IN_APP(Hub2Constants.IN_APP),
        IN_STORE(Hub2Constants.IN_STORE);

        private final String paymentFlow;
        PaymentFlows(final String payment) {
            this.paymentFlow = payment;
        }

        @Override
        public String toString() {
            return paymentFlow;
        }
    }


    public void clickAdd(){
        this.addCondition = "Click add condition";
    }

    @Override
    public PaysWithAppCreditCardCondition setConditionKey(hub.hub2_0.common.conditions.ConditionKey conditionKey) {
        this.conditionKey = conditionKey.toString();
        return this;
    }

    @Override
    public PaysWithAppCreditCardCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey.toString();
        return this;
    }

    @Override
    public PaysWithAppCreditCardCondition setConditionValue(String value){
        this.conditionValue = value;
        return this;
    }

    public void selectPaymentFlow(PaymentFlows paymentFlow) {
        this.paymentFlow = paymentFlow.toString();
    }


    @Override
    public String getAddCondition() {
        return addCondition;
    }

    @Override
    public String getConditionKey() {
        return conditionKey;
    }

    @Override
    public String getOperatorKey() {
        return operatorKey;
    }

    @Override
    public String getConditionValue() {
        return conditionValue;
    }

    public String getPaymentFlow() {
        return paymentFlow;
    }

}
