package hub.hub2_0.common.conditions.cases;

import hub.base.SeleniumPath;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.conditions.global.dateTime.BetweenDates;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;
import hub.hub2_0.common.conditions.global.dateTime.SpecificDateTime;

import java.util.ArrayList;
import java.util.List;

public class DatesAndTimeCondition extends CaseCondition {

    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String addCondition;

    private ConditionKeyTabs conditionKeyTabs;

    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY)
    private String operatorKey;

    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String conditionValue;


    private BetweenDates betweenDates;

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<DaysCondition> daysConditions;

    private int daysConditionIndex = 0;

    public BetweenDates getBetweenDates() {
        return betweenDates;
    }

    public void setBetweenDates(BetweenDates betweenDates) {
        conditionKeyTabs = new ConditionKeyTabs();
        setConditionValue("Y");
        conditionKeyTabs.setBetweenDates();
        this.betweenDates = betweenDates;
        this.betweenDates.setCloseBtn();
    }

    public void setOnDays(DaysCondition... daysConditions) {
        conditionKeyTabs = new ConditionKeyTabs();
        setConditionValue("Y");
        conditionKeyTabs.setOnDays();
        for(DaysCondition condition : daysConditions) {
            if(daysConditionIndex > 0)
                condition.clickAdd();
            addDaysCondition(condition);
        }

    }


    private void addDaysCondition(DaysCondition condition) {
        condition.setNext(daysConditionIndex);
        if(condition.getTimeSelector() != null)
            condition.getTimeSelector().setNext(daysConditionIndex);
        getDaysConditions().add(condition);
        daysConditionIndex++;

    }

    public List<DaysCondition> getDaysConditions() {
        if(daysConditions == null)
            daysConditions = new ArrayList<>();
        return daysConditions;
    }


    @Override
    public CaseCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey;
        return this;
    }

    @Override
    public CaseCondition setConditionValue(String value) {
        this.conditionValue = value;
        return this;
    }

    @Override
    public void clickAdd() {
        addCondition = "add condition";
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        conditionIndex = i;
        conditionKeyTabs.setNext(i);
    }

    @Override
    public void setPrefix (String prefix){
        this.objectPrefix = prefix;
        conditionKeyTabs.setPrefix(prefix);
    }

    @Override
    public int getNext() {
        return conditionIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
