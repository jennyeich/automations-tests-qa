package hub.hub2_0.common.conditions.cases;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;

public class MembersCondition extends CaseCondition {


    public static final String CONDITION_KEY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionKeytabs.membership.context.Membership.{2}\"]";
    public static final String CONDITION_VALUE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionValue\"]/input";
    public static final String CONDITION_VALUE_CHECK = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionValue\"]/button";
    private static final String CONDITION_VALUE_BOOLEAN = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionValue.{2}\"]";

    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String addCondition;

    private ConditionKeyTabs conditionKeyTabs;


    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH_ONE_CLICK, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2)
    private String conditionKey;

    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY)
    private String operatorKey;

    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_CAPSULA_HUB2, readValue = CONDITION_VALUE)
    private String conditionValue;
    @SeleniumPath(value = CONDITION_VALUE_BOOLEAN, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2 )
    private String booleanValue;
    @SeleniumPath(value = CONDITION_DELETE_BUTTON, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String deleteButton;


    public CaseCondition setConditionKey(String conditionKey) {
        this.conditionKey = conditionKey;
        return this;
    }

    @Override
    public CaseCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey;
        return this;
    }

    @Override
    public CaseCondition setConditionValue(String value) {
        this.conditionValue = value;
        return this;
    }

    public void setMembersCondition(MemberFieldCondition cond) {
        conditionKeyTabs = new ConditionKeyTabs();
        conditionKeyTabs.setMembersCondition();
        setConditionKey(cond.getFieldsSelect());
        setOperatorKey(cond.getOperatorKey());
        if(cond.getConditionValue() != null) {
            setConditionValue(cond.getConditionValue());
        }else{
            toggleBooleanValue(cond.getBooleanValue());
        }
    }



    public void toggleBooleanValue(String selction) {
        this.booleanValue = selction;
        this.conditionValue = null;

    }

    public void clickDelete() {
        this.deleteButton = "Clicked delete button.";
    }

    @Override
    public void clickAdd() {
        addCondition = "add condition";
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        conditionIndex = i;
        if(conditionKeyTabs != null ) {
            conditionKeyTabs.setNext(i);
        }
    }

    @Override
    public void setPrefix (String prefix){
        this.objectPrefix = prefix;
        if(conditionKeyTabs != null ) {
            conditionKeyTabs.setPrefix(prefix);
        }
    }

    @Override
    public int getNext() {
        return conditionIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
