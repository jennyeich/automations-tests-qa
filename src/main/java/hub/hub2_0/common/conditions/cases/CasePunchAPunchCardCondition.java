package hub.hub2_0.common.conditions.cases;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.conditions.PunchAPunchCardCondition;

public class CasePunchAPunchCardCondition extends GeneralCondition{

    private static final String CONDITION_VALUE_ADVANCED = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionValue\"]";
    private static final String CONDITION_VALUE_YES = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionValue.yes\"]";
    private static final String CONDITION_VALUE_NO = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionValue.no\"]";
    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String addCondition;

    private ConditionKeyTabs conditionKeyTabs;

    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH_ONE_CLICK, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2)
    private String conditionKey;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY)
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = CONDITION_VALUE)
    private String conditionValue;

    @SeleniumPath(value = CONDITION_VALUE_ADVANCED, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.ADVANCED_SELECT_HUB2)
    private String benefitName;
    @SeleniumPath(value = CONDITION_VALUE_YES, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM )
    private String lastPunchYes;
    @SeleniumPath(value = CONDITION_VALUE_NO, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM )
    private String lastPunchNo;




    public void setGeneralCondition(ActivityCondition cond){
        conditionKeyTabs = new ConditionKeyTabs();
        conditionKeyTabs.setGeneralCondition();
        setCondition(cond);
    }



    public CaseCondition setConditionKey(String conditionKey) {
        this.conditionKey = conditionKey;
        return this;
    }

    @Override
    public CaseCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey;
        return this;
    }



    @Override
    public CaseCondition setConditionValue(String value){
        this.conditionValue = value;
        this.benefitName= null;
        this.lastPunchYes = null;
        this.lastPunchNo = null;
        return this;
    }

    public CaseCondition setBenefitName(String benefitName) {
        this.benefitName = benefitName;
        this.conditionValue = null;
        this.lastPunchYes = null;
        this.lastPunchNo = null;
        return this;
    }

    public CaseCondition clickYes() {
        this.lastPunchYes = ActivityCondition.BOOLEAN_YES;
        this.lastPunchNo = null;
        this.benefitName = null;
        this.conditionValue = null;
        return this;
    }

    public CaseCondition clickNo() {
        this.lastPunchNo =  ActivityCondition.BOOLEAN_NO;
        this.lastPunchYes = null;
        this.benefitName = null;
        this.conditionValue = null;
        return this;
    }


    public String getConditionValue() {
        return conditionValue;
    }

    public String getBenefitName() {
        return benefitName;
    }

    public String getLastPunchYes() {
        return lastPunchYes;
    }

    public String getLastPunchNo() {
        return lastPunchNo;
    }

    @Override
    public void clickAdd() {
        addCondition = "add condition";
    }


    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        conditionIndex = i;
        conditionKeyTabs.setNext(i);

    }

    @Override
    public void setPrefix (String prefix){
        this.objectPrefix = prefix;
        conditionKeyTabs.setPrefix(prefix);
    }

    @Override
    public int getNext() {
        return conditionIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
