package hub.hub2_0.common.conditions.cases;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.conditions.global.dateTime.BetweenDates;
import hub.hub2_0.common.conditions.global.dateTime.DaysCondition;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class ConditionKeyTabs extends BaseSeleniumObjectHub2{


    public static final String CONDITION_KEY_CLICK = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionKey\"]";
    public static final String GENERAL = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"conditionKeytabs\")]/div/a[contains(text(),\"General\")]";
    public static final String DATES_AND_TIMES = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"conditionKeytabs\")]/div/a[contains(text(),\"Dates and times\")]";
    public static final String MEMBERS = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"conditionKeytabs\")]/div/a[contains(text(),\"Members\")]";

    public static final String BETWEEN_DATES = "(//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"conditionKeytabs.datesAndTimes.context.TimeStamp\")])[1]";
    public static final String ON_DAYS = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"conditionKeytabs.datesAndTimes.context.TimeStampDays\")]";


    @SeleniumPath(value = CONDITION_KEY_CLICK, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String conditionKeyClick;
    @SeleniumPath(value = GENERAL, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String generalTab;

    @SeleniumPath(value = DATES_AND_TIMES, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String datesAndTimesTab;
    @SeleniumPath(value = BETWEEN_DATES, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String betweenDatesSelect;
    @SeleniumPath(value = ON_DAYS, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String onDays;

    @SeleniumPath(value = MEMBERS, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String membersTab;


    public int index = 0;


    public void setGeneralCondition() {
        this.conditionKeyClick = "click";
        this.generalTab = "select General";
    }

    public void setBetweenDates() {
        this.conditionKeyClick = "click";
        this.datesAndTimesTab = "select Dates and Times";
        this.betweenDatesSelect = "select Between Dates";
    }
    public void setOnDays() {
        this.conditionKeyClick = "click";
        this.datesAndTimesTab = "select Dates and Times";
        this.onDays = "select On Days";
    }

    public void setMembersCondition() {
        this.conditionKeyClick = "click";
        this.membersTab = "select Members";
    }



    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        index = i;
    }

    @Override
    public int getNext() {
        return index;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

}
