package hub.hub2_0.common.conditions.cases;

import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.ActivityCondition;

public abstract class GeneralCondition extends CaseCondition {


    public static final String CONDITION_KEY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionKeytabs.general.{2}\"]";
    public static final String CONDITION_VALUE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionValue\"]/input";
    public static final String CONDITION_VALUE_CHECK = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionValue\"]/button";

    public abstract void setGeneralCondition(ActivityCondition cond);
    public abstract CaseCondition setConditionKey(String conditionKey);

    protected void setCondition(ActivityCondition cond) {
        setConditionKey(cond.getConditionKey());
        setOperatorKey(cond.getOperatorKey());
        setConditionValue(cond.getConditionValue());
    }


}
