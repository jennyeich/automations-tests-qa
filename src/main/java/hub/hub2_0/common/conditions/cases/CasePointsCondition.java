package hub.hub2_0.common.conditions.cases;

import hub.base.SeleniumPath;
import hub.hub2_0.common.conditions.ActivityCondition;

public class CasePointsCondition extends GeneralCondition{


    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String addCondition;

    private ConditionKeyTabs conditionKeyTabs;

    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH_ONE_CLICK, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2)
    private String conditionKey;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY)
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = CONDITION_VALUE)
    private String conditionValue;




    public void setGeneralCondition(ActivityCondition cond){
        conditionKeyTabs = new ConditionKeyTabs();
        conditionKeyTabs.setGeneralCondition();
        setCondition(cond);
    }



    public CaseCondition setConditionKey(String conditionKey) {
        this.conditionKey = conditionKey;
        return this;
    }

    @Override
    public CaseCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey;
        return this;
    }

    @Override
    public CaseCondition setConditionValue(String value) {
        this.conditionValue= value;
        return this;
    }


    @Override
    public void clickAdd() {
        addCondition = "add condition";
    }


    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        conditionIndex = i;
        conditionKeyTabs.setNext(i);

    }

    @Override
    public void setPrefix (String prefix){
        this.objectPrefix = prefix;
        conditionKeyTabs.setPrefix(prefix);
    }

    @Override
    public int getNext() {
        return conditionIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
