package hub.hub2_0.common.conditions.cases;

import hub.base.BaseSeleniumObjectHub2;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.conditions.ConditionKey;
import hub.hub2_0.common.conditions.global.dateTime.BetweenDates;

public abstract class CaseCondition extends BaseSeleniumObjectHub2 implements ApplyCondition{

    public static final String ADD_CONDITION = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}addCondition\"]";
    public static final String CONDITION_KEY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionKey.{2}\"]";
    public static final String OPERATOR_KEY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionsOperator.{2}\"]";
    public static final String CONDITION_VALUE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionValue\"]";
    public static final String CONDITION_DELETE_BUTTON = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}condition.{1}.delete\")]";



    public abstract CaseCondition setOperatorKey(String operatorKey) ;

    public abstract CaseCondition setConditionValue(String value);

    public abstract void clickAdd();


    public int conditionIndex = 0;

}
