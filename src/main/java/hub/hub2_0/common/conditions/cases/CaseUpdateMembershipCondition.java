package hub.hub2_0.common.conditions.cases;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.enums.YesNo;
import hub.hub2_0.common.items.SelectValue;

public class CaseUpdateMembershipCondition extends GeneralCondition{


    public static final String CONDITION_VALUE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionValue\"]/input";
    private static final String CONDITION_VALUE_BOOLEAN = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.conditionValue.{2}\"]";
    private static final String CHANGE_FROM_SELECTOR = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}updateMembershipPopup.{1}.from\"]";
    private static final String CHANGE_TO_SELECTOR = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}updateMembershipPopup.{1}.to\"]";

    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String addCondition;

    private ConditionKeyTabs conditionKeyTabs;

    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH_ONE_CLICK, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2)
    private String conditionKey;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY)
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_CAPSULA_HUB2, readValue = CONDITION_VALUE)
    private String conditionValue;
    @SeleniumPath(value = CONDITION_VALUE_BOOLEAN, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2, readValue = CONDITION_VALUE_BOOLEAN)
    private String booleanValue;

    @SeleniumPath(value = CHANGE_FROM_SELECTOR, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String changeFrom;
    private SelectValue selectValueFrom;

    @SeleniumPath(value = CHANGE_TO_SELECTOR, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String changeTo;
    private SelectValue selectValueTo;



    public void setGeneralCondition(ActivityCondition cond){
        conditionKeyTabs = new ConditionKeyTabs();
        conditionKeyTabs.setGeneralCondition();
        setCondition(cond);
    }



    public CaseCondition setConditionKey(String conditionKey) {
        this.conditionKey = conditionKey;
        return this;
    }

    @Override
    public CaseCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey;
        return this;
    }

    @Override
    public CaseCondition setConditionValue(String value) {
        this.conditionValue= value;
        return this;
    }

    public CaseCondition clickBooleanValue(YesNo value) {
        this.booleanValue = value.toString();
        return this;
    }
    public SelectValue getSelectValueFrom() {
        return selectValueFrom;
    }
    public CaseCondition setSelectValueFrom(SelectValue selectValueFrom) {
        this.changeFrom = "open any item from dialog";
        this.selectValueFrom = selectValueFrom;
        return this;
    }

    public SelectValue getSelectValueTo() {
        return selectValueTo;
    }

    public CaseCondition setSelectValueTo(SelectValue selectValueTo) {
        this.changeTo = "open any item to dialog";;
        this.selectValueTo = selectValueTo;
        return this;
    }

    @Override
    public void clickAdd() {
        addCondition = "add condition";
    }


    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        conditionIndex = i;
        conditionKeyTabs.setNext(i);

    }

    @Override
    public void setPrefix (String prefix){
        this.objectPrefix = prefix;
        conditionKeyTabs.setPrefix(prefix);
    }

    @Override
    public int getNext() {
        return conditionIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

