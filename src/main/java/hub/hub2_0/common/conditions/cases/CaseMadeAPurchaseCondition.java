package hub.hub2_0.common.conditions.cases;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.Monetary;
import hub.hub2_0.common.items.SelectItems;

public class CaseMadeAPurchaseCondition extends GeneralCondition implements Monetary {

    private final String SELECT_ITEMS_BUTTON = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}condition.{1}.itemsPopulation.trigger\"]";

    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String addCondition;

    private ConditionKeyTabs conditionKeyTabs;

    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH_ONE_CLICK, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2)
    private String conditionKey;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY)
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_CAPSULA_HUB2, readValue = CONDITION_VALUE)
    private String conditionValue;
    @SeleniumPath(value = SELECT_ITEMS_BUTTON, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String selectItemsButton;
    @SeleniumPath(value = CONDITION_DELETE_BUTTON, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String deleteButton;


    private SelectItems selectItems;



    public void setGeneralCondition(ActivityCondition cond){
        conditionKeyTabs = new ConditionKeyTabs();
        conditionKeyTabs.setGeneralCondition();
        setCondition(cond);
        if(((MadeAPurchaseCondition)cond).getSelectItems() != null)
            setSelectItems(((MadeAPurchaseCondition)cond).getSelectItems());
    }

    public CaseCondition setSelectItems(SelectItems selectItems) {
        this.selectItemsButton = "Click select Items dialog";
        this.selectItems = selectItems;
        return this;
    }

    public CaseCondition setConditionKey(String conditionKey) {
        this.conditionKey = conditionKey;
        return this;
    }

    @Override
    public CaseCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey;
        return this;
    }

    @Override
    public CaseCondition setConditionValue(String value) {
        this.conditionValue= value;
        return this;
    }

    public void clickDelete() {
        this.deleteButton = "Clicked delete button.";
    }

    @Override
    public void clickAdd() {
        addCondition = "add condition";
    }


    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        conditionIndex = i;
        if(conditionKeyTabs != null) {
            conditionKeyTabs.setNext(i);
        }
        if(this.selectItems != null) {
            this.selectItems.setNext(i);
        }
    }

    @Override
    public void setPrefix (String prefix){
        this.objectPrefix = prefix;
        if(conditionKeyTabs != null) {
            conditionKeyTabs.setPrefix(prefix);
        }
    }


    @Override
    public int getNext() {
        return conditionIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
