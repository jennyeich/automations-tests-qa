package hub.hub2_0.common.conditions;


import hub.base.BaseSeleniumObject;
import hub.base.BaseSeleniumObjectHub2;
import hub.hub2_0.Hub2Constants;

public abstract class ActivityCondition extends BaseSeleniumObjectHub2 implements ApplyCondition{

    public static final String BOOLEAN_YES =  "yes";
    public static final String BOOLEAN_NO =  "no";

    public static final String ADD_CONDITION = "activity.addCondition";
    public static final String CONDITION_KEY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionKey.{1}\"]";
    public static final String OPERATOR_KEY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionsOperator.{1}\"]";
    public static final String CONDITION_VALUE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue\"]/input";
    public static final String CONDITION_DELETE_BUTTON = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"activity.condition.{0}.delete\")]";

    public abstract void clickAdd();

    public abstract ActivityCondition setConditionKey(ConditionKey conditionKey);
    public abstract ActivityCondition setOperatorKey(String operatorKey);
    public abstract ActivityCondition setConditionValue(String value);
    public abstract String getAddCondition();
    public abstract String getConditionKey();
    public abstract String getOperatorKey();
    public abstract String getConditionValue();

    public int conditionIndex = 0;



    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        this.conditionIndex = i;
    }

    @Override
    public int getNext() {
        return conditionIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }


    @Override
    public int getParentIndex() {
        return 0;
    }


}
