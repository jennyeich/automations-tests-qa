package hub.hub2_0.common.conditions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class ExternalEventSubmittedCondition extends ActivityCondition {

    private static final String CONDITION_VALUE_BOOLEAN = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue.{1}\"]";


    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addCondition;
    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = CONDITION_KEY)
    private String conditionKey;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY )
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_CAPSULA_HUB2, readValue = CONDITION_VALUE)
    private String conditionValue;
    @SeleniumPath(value = CONDITION_VALUE_BOOLEAN, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2, readValue = CONDITION_VALUE_BOOLEAN)
    private String booleanValue;


    public enum ConditionKey implements hub.hub2_0.common.conditions.ConditionKey {
        TYPE(Hub2Constants.TYPE),
        SUBTYPE(Hub2Constants.SUBTYPE),
        STRING_VALUE_1(Hub2Constants.STRING_VALUE_1),
        STRING_VALUE_2(Hub2Constants.STRING_VALUE_2),
        NUMERIC_VALUE_1(Hub2Constants.NUMERIC_VALUE_1),
        NUMERIC_VALUE_2(Hub2Constants.NUMERIC_VALUE_2),
        DATE_VALUE_1(Hub2Constants.DATE_VALUE_1),
        DATE_VALUE_2(Hub2Constants.DATE_VALUE_2),
        BOOLEAN_VALUE_1(Hub2Constants.BOOLEAN_VALUE_1),
        BOOLEAN_VALUE_2(Hub2Constants.BOOLEAN_VALUE_2);


        private final String conditionName;
        ConditionKey(final String name) {
            this.conditionName = name;
        }

        @Override
        public String toString() {
            return conditionName;
        }
    }


    public void clickAdd(){
        this.addCondition = "Click add condition";
    }

    @Override
    public ExternalEventSubmittedCondition setConditionKey(hub.hub2_0.common.conditions.ConditionKey conditionKey) {
        this.conditionKey = conditionKey.toString();
        return this;
    }

    @Override
    public ExternalEventSubmittedCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey.toString();
        return this;
    }

    @Override
    public ExternalEventSubmittedCondition setConditionValue(String value){
        this.conditionValue = value;
        this.booleanValue = null;
        return this;
    }

    public ExternalEventSubmittedCondition clickBooleanValue(String value) {
        this.booleanValue = value;
        this.conditionValue = null;
        return this;
    }

    @Override
    public String getAddCondition() {
        return addCondition;
    }

    @Override
    public String getConditionKey() {
        return conditionKey;
    }

    @Override
    public String getOperatorKey() {
        return operatorKey;
    }

    @Override
    public String getConditionValue() {
        return conditionValue;
    }

    public String getBooleanValue() {
        return this.booleanValue;
    }


}
