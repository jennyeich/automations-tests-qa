package hub.hub2_0.common.conditions.operators;

import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.OperatorKey;

public class ShoppingCartOperators implements OperatorKey {

    public static final String CONTAINS_EXECTLY = Hub2Constants.IS_EXACTLY;
    public static final String DOES_NOT_CONTAIN = Hub2Constants.DOES_NOT_CONTAINS;
    public static final String CONTAINS_MORE_THAN = Hub2Constants.IS_GREATER_THAN;
    public static final String CONTAINS_LESS_THAN = Hub2Constants.IS_LESS_THAN;
    public static final String CONTAINS_AT_LEAST = Hub2Constants.IS_GREATER_THAN_OR_EQUAL_TO;
    public static final String CONTAINS_NO_MORE_THAN = Hub2Constants.IS_LESS_THAN_OR_EQUAL_TO;

}
