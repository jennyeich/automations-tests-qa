package hub.hub2_0.common.conditions.operators;

import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.OperatorKey;

public class DateOperators implements OperatorKey {

    public static final String DATE_IS = Hub2Constants.DATE_IS;
    public static final String DATE_IS_NOT = Hub2Constants.DATE_IS_NOT;
    public static final String DATE_IS_BEFORE = Hub2Constants.DATE_IS_BEFORE;
    public static final String DATE_IS_ON_OR_AFTER = Hub2Constants.DATE_IS_AFTER;
    public static final String DATE_IS_BETWEEN = Hub2Constants.DATE_IS_BETWEEN;

}
