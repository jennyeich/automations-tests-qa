package hub.hub2_0.common.conditions.operators;

import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.OperatorKey;

public class ListOperators implements OperatorKey {

    public static final String IS_EXACTLY = Hub2Constants.IS_EXACTLY;
    public static final String IS_NOT = Hub2Constants.IS_NOT;
}
