package hub.hub2_0.common.conditions.operators;

import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.OperatorKey;

public class IntOperators implements OperatorKey {

    public static final String IS_GREATER_THAN = Hub2Constants.IS_GREATER_THAN;
    public static final String IS_LESS_THAN = Hub2Constants.IS_LESS_THAN;
    public static final String IS_GREATER_THAN_OR_EQUAL_TO = Hub2Constants.IS_GREATER_THAN_OR_EQUAL_TO;
    public static final String IS_LESS_THAN_OR_EQUAL_TO = Hub2Constants.IS_LESS_THAN_OR_EQUAL_TO;
    public static final String IS_EXACTLY = Hub2Constants.IS_EXACTLY;
}
