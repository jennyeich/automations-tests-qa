package hub.hub2_0.common.conditions.operators;

import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.OperatorKey;

public class InputOperators implements OperatorKey {

    public static final String IS_EXACTLY = Hub2Constants.IS_EXACTLY;
    public static final String IS_ONE_OF = Hub2Constants.IS_ONE_OF;
    public static final String IS_NOT = Hub2Constants.IS_NOT;
    public static final String IS_NOT_ONE_OF = Hub2Constants.IS_NOT_ONE_OF;
}
