package hub.hub2_0.common.conditions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class FilledOutAFormCondition extends ActivityCondition {


    public static final String CONDITION_VALUE_SELECT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue\"]";

    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addCondition;
    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = CONDITION_KEY )
    private String conditionKey;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY )
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE_SELECT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_BY_TEXT_HUB2, readValue = CONDITION_VALUE_SELECT )
    private String conditionValue;


    public enum ConditionKey implements hub.hub2_0.common.conditions.ConditionKey {
        FORM_NAME(Hub2Constants.FORM_NAME);

        private final String conditionName;
        ConditionKey(final String name) {
            this.conditionName = name;
        }

        @Override
        public String toString() {
            return conditionName;
        }
    }

    public void clickAdd(){
        this.addCondition = "Click add condition";
    }
    @Override
    public FilledOutAFormCondition setConditionKey(hub.hub2_0.common.conditions.ConditionKey conditionKey) {
        this.conditionKey = conditionKey.toString();
        return this;
    }

    @Override
    public FilledOutAFormCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey.toString();
        return this;
    }

    @Override
    public FilledOutAFormCondition setConditionValue(String value){
        this.conditionValue = value;
        return this;
    }

    @Override
    public String getAddCondition() {
        return addCondition;
    }

    @Override
    public String getConditionKey() {
        return conditionKey;
    }

    @Override
    public String getOperatorKey() {
        return operatorKey;
    }

    @Override
    public String getConditionValue() {
        return conditionValue;
    }


}
