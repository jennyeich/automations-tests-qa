package hub.hub2_0.common.conditions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.items.SelectItems;

public class MadeAPurchaseCondition extends ActivityCondition implements Monetary{

    private final String SELECT_ITEMS_BUTTON = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.itemsPopulation.trigger\"]";

    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addCondition;
    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = CONDITION_KEY)
    private String conditionKey;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY)
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_CAPSULA_HUB2, readValue = CONDITION_VALUE)
    private String conditionValue;
    @SeleniumPath(value = SELECT_ITEMS_BUTTON, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String selectItemsButton;

    private SelectItems selectItems;

    @SeleniumPath(value = CONDITION_DELETE_BUTTON, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String deleteCondition;

    public void clickAdd(){
        this.addCondition = "Click add condition";
    }

    public enum ConditionKey implements hub.hub2_0.common.conditions.ConditionKey {
        SHOPPING_CART(Hub2Constants.SHOPPING_CART),
        TOTAL_SPEND(Hub2Constants.TOTAL_SPEND),
        VALUE_OF_SPECIFIC_ITEMS(Hub2Constants.VALUE_OF_SPECIFIC_ITEMS),
        BRANCH_ID(Hub2Constants.BRANCH_ID),
        POS_ID(Hub2Constants.POS_ID),
        PURCHASE_TAGS(Hub2Constants.PURCHASE_TAGS),
        NUMBER_OF_MEMBERS(Hub2Constants.NUMBER_OF_MEMBERS),
        ORDER_TYPE(Hub2Constants.ORDER_TYPE),
        PURCHASE_SOURCE_TYPE(Hub2Constants.PURCHASE_SOURCE_TYPE),
        PURCHASE_SOURCE_NAME(Hub2Constants.PURCHASE_SOURCE_NAME);


        private final String conditionName;
        ConditionKey(final String name) {
            this.conditionName = name;
        }

        @Override
        public String toString() {
            return conditionName;
        }
    }


    @Override
    public MadeAPurchaseCondition setConditionKey(hub.hub2_0.common.conditions.ConditionKey conditionKey) {
        this.conditionKey = conditionKey.toString();
        return this;
    }

    @Override
    public MadeAPurchaseCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey.toString();
        return this;
    }

    @Override
    public MadeAPurchaseCondition setConditionValue(String value){
        this.conditionValue = value;
        return this;
    }


    public MadeAPurchaseCondition setSelectItems(SelectItems selectItems) {
        this.selectItemsButton = "Click select Items dialog";
        this.selectItems = selectItems;
        return this;
    }

    public MadeAPurchaseCondition clickDeleteConditionButton() {
        this.deleteCondition = "Click delete condition button";
        return this;
    }

    public SelectItems getSelectItems() {
        return selectItems;
    }

    @Override
    public String getAddCondition() {
        return addCondition;
    }

    @Override
    public String getConditionKey() {
        return conditionKey;
    }

    @Override
    public String getOperatorKey() {
        return operatorKey;
    }

    @Override
    public String getConditionValue() {
        return conditionValue;
    }


    @Override
    public void setNext(int i) {
        this.conditionIndex = i;
        if(this.selectItems != null) {
            this.selectItems.setNext(i);
        }
    }

}
