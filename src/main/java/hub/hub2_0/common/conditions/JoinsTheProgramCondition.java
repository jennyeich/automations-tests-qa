package hub.hub2_0.common.conditions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class JoinsTheProgramCondition extends ActivityCondition {

    private static final String CONDITION_VALUE_YES = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue.yes\"]";
    private static final String CONDITION_VALUE_NO = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue.no\"]";


    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addCondition;
    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = CONDITION_KEY)
    private String conditionKey;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY)
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE_YES, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK )
    private String yes;
    @SeleniumPath(value = CONDITION_VALUE_NO, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK )
    private String no;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_CAPSULA_HUB2, readValue = CONDITION_VALUE)
    private String conditionValue;



    public void clickAdd(){
        this.addCondition = "Click add condition";
    }
    public enum ConditionKey implements hub.hub2_0.common.conditions.ConditionKey {

        REFERRED_CODE(Hub2Constants.REFFERED_CODE);

        private final String conditionName;
        ConditionKey(final String name) {
            this.conditionName = name;
        }

        @Override
        public String toString() {
            return conditionName;
        }
    }


    @Override
    public JoinsTheProgramCondition setConditionKey(hub.hub2_0.common.conditions.ConditionKey conditionKey) {
        this.conditionKey = conditionKey.toString();
        return this;
    }


    public JoinsTheProgramCondition clickYes() {
        this.yes = ActivityCondition.BOOLEAN_YES;
        this.no = null;
        this.conditionValue = null;
        this.operatorKey = null;
        return this;
    }

    public JoinsTheProgramCondition clickNo() {
        this.no =  ActivityCondition.BOOLEAN_NO;
        this.yes = null;
        this.conditionValue = null;
        return this;
    }

    @Override
    public JoinsTheProgramCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey.toString();
        return this;
    }

    @Override
    public String getAddCondition() {
        return addCondition;
    }

    @Override
    public String getConditionKey() {
        return conditionKey;
    }

    @Override
    public String getOperatorKey() {
        return operatorKey;
    }

    @Override
    public String getConditionValue() {
        return conditionValue;
    }
    @Override
    public JoinsTheProgramCondition setConditionValue(String conditionValue) {
        this.conditionValue = conditionValue;
        return this;
    }


}
