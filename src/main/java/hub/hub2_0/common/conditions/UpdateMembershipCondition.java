package hub.hub2_0.common.conditions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.YesNo;
import hub.hub2_0.common.items.SelectValue;


public class UpdateMembershipCondition extends ActivityCondition {

    private static final String CONDITION_VALUE_BOOLEAN = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue.{1}\"]";
    private static final String CHANGE_FROM_SELECTOR = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue.updateMembershipPopup.from\"]";
    private static final String CHANGE_TO_SELECTOR = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue.updateMembershipPopup.to\"]";

    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addCondition;
    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = CONDITION_KEY)
    private String conditionKey;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY)
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_CAPSULA_HUB2, readValue = CONDITION_VALUE)
    private String conditionValue;
    @SeleniumPath(value = CONDITION_VALUE_BOOLEAN, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2, readValue = CONDITION_VALUE_BOOLEAN)
    private String booleanValue;

    @SeleniumPath(value = CHANGE_FROM_SELECTOR, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String changeFrom;
    private SelectValue selectValueFrom;

    @SeleniumPath(value = CHANGE_TO_SELECTOR, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String changeTo;
    private SelectValue selectValueTo;


    public void clickAdd(){
        this.addCondition = "Click add condition";
    }

    @Override
    public UpdateMembershipCondition setConditionKey(hub.hub2_0.common.conditions.ConditionKey conditionKey) {
        this.conditionKey = conditionKey.toString();
        return this;
    }

    public enum ConditionKey implements hub.hub2_0.common.conditions.ConditionKey {
        FIRST_NAME(Hub2Constants.FIRST_NAME),
        LAST_NAME(Hub2Constants.LAST_NAME),
        PHONE_NUMBER(Hub2Constants.PHONE_NUMBER),
        EMAIL(Hub2Constants.EMAIL),
        GENDER(Hub2Constants.GENDER),
        GENERIC_STRING(Hub2Constants.GENERIC_STRING),
        GENERIC_INTEGER(Hub2Constants.GENERIC_INTEGER),
        POINTS(Hub2Constants.POINTS),
        BUDGET_WEIGHTED(Hub2Constants.BUDGET_WEIGHTED),
        ALLOW_SMS(Hub2Constants.ALLOW_SMS),
        ALLOW_EMAIL(Hub2Constants.ALLOW_EMAIL),
        ALLOW_PUSH(Hub2Constants.ALLOW_PUSH),
        ALLOW_LOCATION_SERVICES(Hub2Constants.ALLOW_LOCATION_SERVICES),
        APP_USER(Hub2Constants.APP_USER),
        JOINING_CODE_BULK_TAG(Hub2Constants.JOINING_CODE_BULK_TAG),
        UPDATE_DURING_REGISTRATION(Hub2Constants.UPDATE_DURING_REGISTRATION);

        private final String conditionName;
        ConditionKey(final String name) {
            this.conditionName = name;
        }

        @Override
        public String toString() {
            return conditionName;
        }
    }
    public UpdateMembershipCondition clickBooleanValue(YesNo value) {
        this.booleanValue = value.toString();
        return this;
    }

    @Override
    public String getAddCondition() {
        return addCondition;
    }


    @Override
    public UpdateMembershipCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey;
        return this;
    }

    @Override
    public UpdateMembershipCondition setConditionValue(String value){
        this.conditionValue = value;
        return this;
    }

    @Override
    public String getConditionKey() {
        return conditionKey;
    }

    @Override
    public String getOperatorKey() {
        return this.operatorKey;
    }

    @Override
    public String getConditionValue() {
        return this.conditionValue;
    }


    public SelectValue getSelectValueFrom() {
        return selectValueFrom;
    }
    public UpdateMembershipCondition setSelectValueFrom(SelectValue selectValueFrom) {
        this.changeFrom = "open any item from dialog";
        this.selectValueFrom = selectValueFrom;
        return this;
    }

    public SelectValue getSelectValueTo() {
        return selectValueTo;
    }

    public UpdateMembershipCondition setSelectValueTo(SelectValue selectValueTo) {
        this.changeTo = "open any item to dialog";;
        this.selectValueTo = selectValueTo;
        return this;
    }
}
