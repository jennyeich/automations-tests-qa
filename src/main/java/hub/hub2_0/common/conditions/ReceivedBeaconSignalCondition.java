package hub.hub2_0.common.conditions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class ReceivedBeaconSignalCondition extends ActivityCondition {


    private static final String CONDITION_VALUE_SELECTOR = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue.{1}\"]";

    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addCondition;
    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = CONDITION_KEY)
    private String conditionKey;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY)
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_CAPSULA_HUB2, readValue = CONDITION_VALUE)
    private String conditionValue;

    @SeleniumPath(value = CONDITION_VALUE_SELECTOR, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = CONDITION_VALUE_SELECTOR)
    private String subtype;


    public enum ConditionKey implements hub.hub2_0.common.conditions.ConditionKey {
        SUBTYPE(Hub2Constants.SUBTYPE),
        UUID(Hub2Constants.UUID),
        MAJOR(Hub2Constants.MAJOR),
        MINOR(Hub2Constants.MINOR);

        private final String conditionName;
        ConditionKey(final String name) {
            this.conditionName = name;
        }

        @Override
        public String toString() {
            return conditionName;
        }
    }


    public enum Subtype {
        ENTER("Enter"),
        EXIT("Exit");

        private final String subtype;
        Subtype(final String subtype) {
            this.subtype = subtype;
        }

        @Override
        public String toString() {
            return subtype;
        }
    }

    public void clickAdd(){
        this.addCondition = "Click add condition";
    }
    @Override
    public ReceivedBeaconSignalCondition setConditionKey(hub.hub2_0.common.conditions.ConditionKey conditionKey) {
        this.conditionKey = conditionKey.toString();
        return this;
    }

    @Override
    public ReceivedBeaconSignalCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey.toString();
        return this;
    }

    @Override
    public ReceivedBeaconSignalCondition setConditionValue(String value){
        this.conditionValue = value;
        this.subtype= null;
        return this;
    }

    public ReceivedBeaconSignalCondition setSubtype(Subtype subtype) {
        this.subtype = subtype.toString();
        this.conditionValue = null;
        return this;
    }

    @Override
    public String getAddCondition() {
        return addCondition;
    }

    @Override
    public String getConditionKey() {
        return conditionKey;
    }

    @Override
    public String getOperatorKey() {
        return operatorKey;
    }

    @Override
    public String getConditionValue() {
        return conditionValue;
    }

    public String getSubtype() {
        return subtype;
    }

}
