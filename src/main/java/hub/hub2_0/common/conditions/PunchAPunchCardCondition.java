package hub.hub2_0.common.conditions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class PunchAPunchCardCondition extends ActivityCondition {

    private static final String CONDITION_VALUE_ADVANCED = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue\"]";
    private static final String CONDITION_VALUE_YES = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue.yes\"]";
    private static final String CONDITION_VALUE_NO = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue.no\"]";


    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addCondition;
    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = CONDITION_KEY)
    private String conditionKey;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY)
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = CONDITION_VALUE)
    private String conditionValue;


    @SeleniumPath(value = CONDITION_VALUE_ADVANCED, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.ADVANCED_SELECT_HUB2)
    private String benefitName;
    @SeleniumPath(value = CONDITION_VALUE_YES, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK )
    private String lastPunchYes;
    @SeleniumPath(value = CONDITION_VALUE_NO, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK )
    private String lastPunchNo;


    public enum ConditionKey implements hub.hub2_0.common.conditions.ConditionKey {
        BENEFIT(Hub2Constants.BENEFIT),
        NUMBER_OF_PUNCHES(Hub2Constants.NUMBER_OF_PUNCHES),
        IS_LAST_PUNCH(Hub2Constants.IS_LAST_PUNCH);

        private final String conditionName;
        ConditionKey(final String name) {
            this.conditionName = name;
        }

        @Override
        public String toString() {
            return conditionName;
        }
    }

    public void clickAdd(){
        this.addCondition = "Click add condition";
    }
    @Override
    public PunchAPunchCardCondition setConditionKey(hub.hub2_0.common.conditions.ConditionKey conditionKey) {
        this.conditionKey = conditionKey.toString();
        return this;
    }

    @Override
    public PunchAPunchCardCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey.toString();

        return this;
    }

    @Override
    public PunchAPunchCardCondition setConditionValue(String value){
        this.conditionValue = value;
        this.benefitName= null;
        this.lastPunchYes = null;
        this.lastPunchNo = null;
        return this;
    }

    public PunchAPunchCardCondition setBenefitName(String benefitName) {
        this.benefitName = benefitName;
        this.conditionValue = null;
        this.lastPunchYes = null;
        this.lastPunchNo = null;
        return this;
    }

    public PunchAPunchCardCondition clickYes() {
        this.lastPunchYes = ActivityCondition.BOOLEAN_YES;
        this.lastPunchNo = null;
        this.benefitName = null;
        this.conditionValue = null;
        return this;
    }

    public PunchAPunchCardCondition clickNo() {
        this.lastPunchNo =  ActivityCondition.BOOLEAN_NO;
        this.lastPunchYes = null;
        this.benefitName = null;
        this.conditionValue = null;
        return this;
    }

    @Override
    public String getAddCondition() {
        return addCondition;
    }

    @Override
    public String getConditionKey() {
        return conditionKey;
    }

    @Override
    public String getOperatorKey() {
        return operatorKey;
    }

    @Override
    public String getConditionValue() {
        return conditionValue;
    }

    public String getBenefitName() {
        return benefitName;
    }

    public String getLastPunchYes() {
        return lastPunchYes;
    }

    public String getLastPunchNo() {
        return lastPunchNo;
    }

}
