package hub.hub2_0.common.conditions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class TappedClaimButtonCondition extends ActivityCondition {

    private static final String CONDITION_SELECTOR = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue\"]";
    private static final String CONDITION_ADVANCED_SELECTOR = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"activity.condition.{0}.conditionValue\"]";

    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addCondition;
    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = CONDITION_KEY)
    private String conditionKey;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPERATOR_KEY)
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_CAPSULA_HUB2, readValue = CONDITION_VALUE)
    private String conditionValue;

    @SeleniumPath(value = CONDITION_SELECTOR, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_BY_TEXT_HUB2, readValue = CONDITION_SELECTOR)
    private String catalogItems;
    @SeleniumPath(value = CONDITION_ADVANCED_SELECTOR, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = CONDITION_ADVANCED_SELECTOR)
    private String benefit;


    public enum ConditionKey implements hub.hub2_0.common.conditions.ConditionKey {
        CATALOG_ITEMS(Hub2Constants.CATALOG_ITEMS),
        TAG(Hub2Constants.TAG),
        BENEFIT(Hub2Constants.BENEFIT);

        private final String conditionName;
        ConditionKey(final String name) {
            this.conditionName = name;
        }

        @Override
        public String toString() {
            return conditionName;
        }
    }

    public void clickAdd(){
        this.addCondition = "Click add condition";
    }
    @Override
    public TappedClaimButtonCondition setConditionKey(hub.hub2_0.common.conditions.ConditionKey conditionKey) {
        this.conditionKey = conditionKey.toString();
        return this;
    }

    @Override
    public TappedClaimButtonCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey.toString();
        return this;
    }

    @Override
    public TappedClaimButtonCondition setConditionValue(String value){
        this.conditionValue = value;
        this.catalogItems = null;
        this.benefit = null;
        return this;
    }

    public TappedClaimButtonCondition setCatalogItems(String value){
        this.catalogItems = value;
        this.benefit = null;
        this.conditionValue = null;
        return this;
    }

    public TappedClaimButtonCondition setBenefit(String value){
        this.benefit = value;
        this.conditionValue = null;
        this.catalogItems = null;
        return this;
    }

    @Override
    public String getAddCondition() {
        return addCondition;
    }

    @Override
    public String getConditionKey() {
        return conditionKey;
    }

    @Override
    public String getOperatorKey() {
        return operatorKey;
    }

    @Override
    public String getConditionValue() {
        return conditionValue;
    }

    public String getCatalogItems() {
        return catalogItems;
    }

    public String getBenefit() {
        return benefit;
    }


}