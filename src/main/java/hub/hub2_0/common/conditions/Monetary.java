package hub.hub2_0.common.conditions;

import hub.hub2_0.services.loyalty.LoyaltyService;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.text.MessageFormat;
import java.util.List;

public interface Monetary {

    public static final String CURRENCY_LABEL = "//div[@class='ui label label']";

    String CURRENCY_PREVIEW = "//div[contains(@title,\"{0}\")]";

    default boolean checkIfAllCurrencyPreviewCorrect(String expectedCurrency) {
        String pathToCurrency = MessageFormat.format(CURRENCY_PREVIEW,expectedCurrency);
        List<WebElement> elements = LoyaltyService.findElements(By.xpath(pathToCurrency));
        if(elements.size() > 0)
            return true;
        return false;
    }

   default String getCurrency() {
        return LoyaltyService.getText(By.xpath(CURRENCY_LABEL));
   }

    default boolean checkIfAllCurrencyLabelsCurrect(String currencySign, int expectedNumOfLabels){
        List<WebElement> elements = LoyaltyService.findElements(By.xpath(Monetary.CURRENCY_LABEL));

        if(elements.size() != expectedNumOfLabels)
            return false;
        for (WebElement label: elements ) {
            if( !label.getText().equals(currencySign))
                return false;
        }

        return true;
    }
}
