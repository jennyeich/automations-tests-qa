package hub.hub2_0.common.filters;

import hub.base.BaseService;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.CampaignFilterView;
import org.openqa.selenium.By;
import utils.AutomationException;
import static hub.base.BaseService.fillSeleniumObject;
import static hub.base.BaseService.log4j;
import static hub.hub2_0.common.enums.CampaignFilterView.DEAL;
import static hub.hub2_0.common.enums.CampaignFilterView.RULE;

public class CampaignFilter extends ListFilters {

    private int counter = 0;
    private final String CAMPAIGN_PREFIX = "filter-settings.toggle.button.activity.";

    private static final String RULE_FILTER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}rule\"]";
    private static final String DEAL_FILTER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}deal\"]";
    private static final String ONE_TIME_ACTION_FILTER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}onetime\"]";
    private static final String COMPLETED_FILTER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}completed\"]";
    private static final String DRAFT_FILTER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}draft\"]";
    private static final String ARCHIVED_CAMPAIGNS_FILTER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"filter-settings.toggle.button.campaign.archived\"]";

    @SeleniumPath(value = RULE_FILTER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String ruleFilter;
    @SeleniumPath(value = DEAL_FILTER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String dealFilter;
    @SeleniumPath(value = ONE_TIME_ACTION_FILTER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String oneTimeActionFilter;
    @SeleniumPath(value = ACTIVE_FILTER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String activeFilter;
    @SeleniumPath(value = COMPLETED_FILTER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String completedFilter;
    @SeleniumPath(value = STOPPED_FILTER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String stoppedFilter;
    @SeleniumPath(value = FUTURE_FILTER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String futureFilter;
    @SeleniumPath(value = DRAFT_FILTER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String draftFilter;
    @SeleniumPath(value = ARCHIVED_CAMPAIGNS_FILTER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String archivedCampaignsFilter;

    // Add this at the end in order to close the filter view after setting.
    @SeleniumPath(value = TRIGGER_FILTERS, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String triggerFilters;

    private void initFilters() {
        this.ruleFilter = null;
        this.dealFilter = null;
        this.oneTimeActionFilter = null;
        this.activeFilter = null;
        this.completedFilter = null;
        this.stoppedFilter = null;
        this.futureFilter = null;
        this.draftFilter = null;
        this.archivedCampaignsFilter = null;
        setPrefix(CAMPAIGN_PREFIX);
    }

    @Override
    public void toggleFilters(boolean requiredState, IFilter... filters) throws AutomationException {
        String actualFilterState;

        this.triggerFilters = "Click View filter";
        BaseService.getDriver().findElement(By.xpath(TRIGGER_FILTERS)).click(); // click the filter button in order to read the status of the filters
        log4j.info("open the filter view");
        initFilters(); //set all filters to null before setting them
        for (IFilter filter:filters) {
            switch ((CampaignFilterView)filter) {
                case RULE:
                    actualFilterState = BaseService.getDriver().findElement(By.xpath(resolveLocator(RULE_FILTER))).findElement(By.className("cbx")).getAttribute("value");
                    ruleFilter = (requiredState == Boolean.parseBoolean(actualFilterState)) ? (ruleFilter = null) : (ruleFilter = RULE.name());
                    log4j.info("set <" + RULE.name() + "> filter state to <" + requiredState +">"  );
                    break;
                case DEAL:
                    actualFilterState = BaseService.getDriver().findElement(By.xpath(resolveLocator(DEAL_FILTER))).findElement(By.className("cbx")).getAttribute("value");
                    dealFilter = (requiredState == Boolean.parseBoolean(actualFilterState)) ? null : DEAL.name();
                    log4j.info("set <" + DEAL.name() + "> filter state to <" + requiredState +">");
                    break;
                case ONE_TIME_ACTION:
                    actualFilterState = BaseService.getDriver().findElement(By.xpath(resolveLocator(ONE_TIME_ACTION_FILTER))).findElement(By.className("cbx")).getAttribute("value");
                    oneTimeActionFilter = (requiredState == Boolean.parseBoolean(actualFilterState)) ? null : CampaignFilterView.ONE_TIME_ACTION.name();
                    log4j.info("set <" + CampaignFilterView.ONE_TIME_ACTION.name() + "> filter state to <" + requiredState +">" );
                    break;
                case ACTIVE:
                    actualFilterState = BaseService.getDriver().findElement(By.xpath(resolveLocator(ACTIVE_FILTER))).findElement(By.className("cbx")).getAttribute("value");
                    activeFilter = (requiredState == Boolean.parseBoolean(actualFilterState)) ? null : CampaignFilterView.ACTIVE.name();
                    log4j.info("set <" + CampaignFilterView.ACTIVE.name() + "> filter state to <" + requiredState +">");
                    break;
                case COMPLETED:
                    actualFilterState = BaseService.getDriver().findElement(By.xpath(resolveLocator(COMPLETED_FILTER))).findElement(By.className("cbx")).getAttribute("value");
                    completedFilter = (requiredState == Boolean.parseBoolean(actualFilterState)) ? null : CampaignFilterView.COMPLETED.name();
                    log4j.info("set <" + CampaignFilterView.COMPLETED.name() + "> filter state to <" + requiredState +">");
                    break;
                case STOPPED:
                    actualFilterState = BaseService.getDriver().findElement(By.xpath(resolveLocator(STOPPED_FILTER))).findElement(By.className("cbx")).getAttribute("value");
                    stoppedFilter = (requiredState == Boolean.parseBoolean(actualFilterState)) ? null : CampaignFilterView.STOPPED.name();
                    log4j.info("set <" + CampaignFilterView.STOPPED.name() + "> filter state to <" + requiredState +">");
                    break;
                case FUTURE:
                    actualFilterState = BaseService.getDriver().findElement(By.xpath(resolveLocator(FUTURE_FILTER))).findElement(By.className("cbx")).getAttribute("value");
                    futureFilter = (requiredState == Boolean.parseBoolean(actualFilterState)) ? null : CampaignFilterView.FUTURE.name();
                    log4j.info("set <" + CampaignFilterView.FUTURE.name() + "> filter state to <" + requiredState +">");
                    break;
                case DRAFT:
                    actualFilterState = BaseService.getDriver().findElement(By.xpath(resolveLocator(DRAFT_FILTER))).findElement(By.className("cbx")).getAttribute("value");
                    draftFilter = (requiredState == Boolean.parseBoolean(actualFilterState)) ? null : CampaignFilterView.DRAFT.name();
                    log4j.info("set <" + CampaignFilterView.DRAFT.name() + "> filter state to <" + requiredState +">");
                    break;
                case ARCHIVED_CAMPAIGNS:
                    actualFilterState = BaseService.getDriver().findElement(By.xpath(ARCHIVED_CAMPAIGNS_FILTER)).findElement(By.className("cbx")).getAttribute("value");
                    archivedCampaignsFilter = (requiredState == Boolean.parseBoolean(actualFilterState)) ? null : CampaignFilterView.ARCHIVED_CAMPAIGNS.name();
                    log4j.info("set <" + CampaignFilterView.ARCHIVED_CAMPAIGNS.name() + "> filter state to <" + requiredState +">");
                    break;
            }
        }
        fillSeleniumObject(this);
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        this.counter = i;
    }

    @Override
    public int getNext() {
        return this.counter;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getParentIndex() {
        return 0;
    }


}
