package hub.hub2_0.common.filters;

import hub.base.BaseSeleniumObjectHub2;
import hub.hub2_0.Hub2Constants;
import utils.AutomationException;

import java.text.MessageFormat;

public abstract class ListFilters extends BaseSeleniumObjectHub2 implements IListView {

    public static final String TRIGGER_FILTERS = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"popup.filter-settings.trigger\"]";
    public static final String ACTIVE_FILTER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}active\"]";
    public static final String STOPPED_FILTER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}stopped\"]";
    public static final String FUTURE_FILTER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}future\"]";

    public String resolveLocator(String locator)
    {
        return MessageFormat.format(locator,getPrefix());
    }

    public abstract void toggleFilters(boolean requiredState,IFilter ...filter) throws AutomationException;


}
