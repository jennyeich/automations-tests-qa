package hub.hub2_0.common.filters;

import hub.base.BaseService;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.GiftFilterView;
import org.openqa.selenium.By;
import utils.AutomationException;

import static hub.base.BaseService.fillSeleniumObject;
import static hub.base.BaseService.log4j;

public class GiftsFilter extends ListFilters {

    private int counter = 0;
    private final String GIFT_PREFIX = "filter-settings.toggle.button.gifts.";
    private static final String EXPIRED_FILTER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"filter-settings.toggle.button.gifts.completed\"]";

    @SeleniumPath(value = ACTIVE_FILTER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String activeFilter;
    @SeleniumPath(value = STOPPED_FILTER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String stoppedFilter;
    @SeleniumPath(value = FUTURE_FILTER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String futureFilter;
    @SeleniumPath(value = EXPIRED_FILTER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String expiredFilter;

    // Add this at the end in order to close the filter view after setting.
    @SeleniumPath(value = TRIGGER_FILTERS, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String triggerFilters;

    private void initFilters() {
        this.activeFilter = null;
        this.stoppedFilter = null;
        this.futureFilter = null;
        this.expiredFilter=null;
        setPrefix(GIFT_PREFIX);
    }


    @Override
    public void toggleFilters(boolean requiredState, IFilter... filters) throws AutomationException {
        String actualFilterState;

        this.triggerFilters = "Click View filter";
        BaseService.getDriver().findElement(By.xpath(TRIGGER_FILTERS)).click(); // click the filter button in order to read the status of the filters
        log4j.info("open the filter view");
        initFilters(); //set all filters to null before setting them
        for (IFilter filter:filters) {
            switch ((GiftFilterView)filter) {
                case ACTIVE:
                    actualFilterState = BaseService.getDriver().findElement(By.xpath(resolveLocator(ACTIVE_FILTER))).findElement(By.className("cbx")).getAttribute("value");
                    activeFilter = (requiredState == Boolean.parseBoolean(actualFilterState)) ? null : GiftFilterView.ACTIVE.name();
                    log4j.info("set <" + GiftFilterView.ACTIVE.name() + "> filter state to <" + requiredState +">");
                    break;
                case STOPPED:
                    actualFilterState = BaseService.getDriver().findElement(By.xpath(resolveLocator(STOPPED_FILTER))).findElement(By.className("cbx")).getAttribute("value");
                    stoppedFilter = (requiredState == Boolean.parseBoolean(actualFilterState)) ? null : GiftFilterView.STOPPED.name();
                    log4j.info("set <" + GiftFilterView.STOPPED.name() + "> filter state to <" + requiredState +">");
                    break;
                case FUTURE:
                    actualFilterState = BaseService.getDriver().findElement(By.xpath(resolveLocator(FUTURE_FILTER))).findElement(By.className("cbx")).getAttribute("value");
                    futureFilter = (requiredState == Boolean.parseBoolean(actualFilterState)) ? null : GiftFilterView.FUTURE.name();
                    log4j.info("set <" + GiftFilterView.FUTURE.name() + "> filter state to <" + requiredState +">");
                    break;
                case EXPIRED:
                    actualFilterState = BaseService.getDriver().findElement(By.xpath(resolveLocator(EXPIRED_FILTER))).findElement(By.className("cbx")).getAttribute("value");
                    expiredFilter = (requiredState == Boolean.parseBoolean(actualFilterState)) ? null : GiftFilterView.EXPIRED.name();
                    log4j.info("set <" + GiftFilterView.EXPIRED.name() + "> filter state to <" + requiredState +">");
                    break;
            }
        }
        fillSeleniumObject(this);
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        this.counter = i;
    }

    @Override
    public int getNext() {
        return this.counter;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getParentIndex() {
        return 0;
    }
}
