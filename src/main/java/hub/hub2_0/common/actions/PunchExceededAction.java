package hub.hub2_0.common.actions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.ActivityActions;

public class PunchExceededAction extends ActivityAction{

    private static final String SELECT_BENEFIT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.punchExceeded.{1}.punchCardKey\"]";

    @SeleniumPath(value = PERFORM_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String performTheAction;
    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String punchExceededBtn;
    @SeleniumPath(value = SELECT_BENEFIT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.ADVANCED_SELECT_HUB2, readValue = SELECT_BENEFIT)
    private String selectBenefit;

    private DelayAction delayAction;

    public PunchExceededAction(){
        if(!isInCase())
            this.objectPrefix = ACTIVITY_PREFIX;

    }
    public DelayAction getDelayAction() {
        return delayAction;
    }

    public void setDelayAction(DelayAction delayAction) {
        this.delayAction = delayAction;
    }


    public void clickPunchAPunchCardBtn() {
        this.punchExceededBtn = ActivityActions.PunchExceeded.toString();
        this.performTheAction = ActivityActions.PunchExceeded.toString();
    }

    public String getSelectBenefit() {
        return selectBenefit;
    }

    public void setSelectBenefit(String selectBenefit) {
        this.selectBenefit = selectBenefit;
    }



    @Override
    public void editActivityAction(ActivityAction after) {
        PunchAPunchCardAction action = (PunchAPunchCardAction)after;
        if(!getSelectBenefit().equals(action.getSelectBenefit()))
            this.setSelectBenefit(action.getSelectBenefit());


    }


    @Override
    public String getPerformTheAction() {
        return performTheAction;
    }


    @Override
    public void clearPerformTheAction() {
        this.performTheAction = null;
        this.punchExceededBtn = null;
    }

    @Override
    public void setNext(int i) {
        super.setNext(i);
        if(delayAction != null) {
            delayAction.setNext(actionIndex);
            delayAction.setIsInCase(isInCase());
            delayAction.setPrefix(getPrefix());
        }
    }
}
