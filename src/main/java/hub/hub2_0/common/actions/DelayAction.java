package hub.hub2_0.common.actions;

import hub.base.BaseSeleniumObject;
import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.DelayUnits;

public class DelayAction extends BaseSeleniumObjectHub2 {

    public static final String IMMEDIATLY= "immediately";
    public static final String AFTER = "after";
    public static final String CANCEL= "cancel";
    public static final String SAVE = "save";

    public static final String DELAY_BUTTON = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.{1}.delay.open.modal\"]";

    public static final String SELECT_RADIO ="//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.{1}.delay.radio.{2}\"]" ;
    public static final String AFTER_INPUT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.{1}.delay.input.after.value\"]/input" ;
    public static final String AFTER_UNITS_DROPDOWN ="//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.{1}.delay.dropdown.{2}\"]" ;
    private static final String FINISH_BTN = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.{1}.delay.btn.{2}\"]";

    @SeleniumPath(value = DELAY_BUTTON, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String addDelay;

    @SeleniumPath(value = SELECT_RADIO, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String delayType;
    @SeleniumPath(value = AFTER_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2)
    private String afterInput;
    @SeleniumPath(value = AFTER_UNITS_DROPDOWN, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2)
    private String afterUnits;

    @SeleniumPath(value = FINISH_BTN, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String finishBtn;



    private int actionIndex;


    public DelayAction(){
        this.addDelay = "Click delay";
        if(!isInCase())
            this.objectPrefix = ActivityAction.ACTIVITY_PREFIX;
    }


    public String getDelayType() {
        return delayType;
    }

    public DelayAction selectDelayType(String type) {
        this.delayType = type;
        return this;
    }

    public String getAfterInput() {
        return afterInput;
    }

    public DelayAction setAfterInput(String afterInput) {
        this.afterInput = afterInput;
        return this;
    }

    public String getAfterUnits() {
        return afterUnits;
    }

    public DelayAction setAfterUnits(DelayUnits unit) {
        this.afterUnits = unit.toString();
        return this;
    }

    public String getFinishBtn() {
        return finishBtn;
    }


    public void clickFinish(String finishType) {
        this.finishBtn = finishType;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        actionIndex = i;
    }

    @Override
    public int getNext() {
        return actionIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public int getParentIndex() {
        return 0;
    }
}
