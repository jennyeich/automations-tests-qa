package hub.hub2_0.common.actions;

import hub.base.SeleniumPath;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SendEmail;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.ActivityActions;

public class SendEmailAction extends ActivityAction {

    private static final String TEMPLATE_SELECT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.sendMemberEmail.{1}.externalTemplateId.{2}\"]";
    private static final String SUBJECT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"subject.{0}\"]";
    private static final String FROM = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"from.{0}\"]";

    @SeleniumPath(value = PERFORM_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String performTheAction;
    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String sendEmailBtn;
    @SeleniumPath(value = TEMPLATE_SELECT, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2)
    private String templateSelect;
    @SeleniumPath(value = SUBJECT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.TEXT_AREA_HUB2, readValue = SUBJECT)
    private String subject;
    @SeleniumPath(value = FROM, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.TEXT_AREA_HUB2, readValue = FROM)
    private String from;

    private DelayAction delayAction;

    public SendEmailAction(){
        if(!isInCase())
            this.objectPrefix = ACTIVITY_PREFIX;

    }

    public DelayAction getDelayAction() {
        return delayAction;
    }


    public void setDelayAction(DelayAction delayAction) {
        this.delayAction = delayAction;
    }

    public void clickSendEmailBtnBtn() {
        this.sendEmailBtn = ActivityActions.SendEmail.toString();
        this.performTheAction = ActivityActions.SendEmail.toString();
    }

    public String getTemplateSelect() {
        return templateSelect;
    }

    public void setTemplateSelect(String templateSelect) {
        this.templateSelect = templateSelect;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    @Override
    public void editActivityAction(ActivityAction after) {
        SendEmailAction action = (SendEmailAction)after;
        if(!getTemplateSelect().equals(action.getTemplateSelect()))
            this.setTemplateSelect(action.getTemplateSelect());
        if(!getSubject().equals(action.getSubject()))
            this.setSubject(action.getSubject());
        if(!getFrom().equals(action.getFrom()))
            this.setFrom(action.getFrom());
    }

    @Override
    public String getPerformTheAction() {
        return performTheAction;
    }


    @Override
    public void clearPerformTheAction() {
        this.performTheAction = null;
        this.sendEmailBtn = null;
    }

    @Override
    public void setNext(int i) {
        super.setNext(i);
        if(delayAction != null) {
            delayAction.setNext(actionIndex);
            delayAction.setIsInCase(isInCase());
            delayAction.setPrefix(getPrefix());
        }
    }
}




