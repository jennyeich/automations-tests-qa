package hub.hub2_0.common.actions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.ActivityActions;

/**
 * Created by Goni on 1/29/2018.
 */

public class SendTextMessageAction extends ActivityAction {

    private static final String MESSAGE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.sendMemberSms.{1}.mentionTextArea\"]";


    @SeleniumPath(value = PERFORM_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String performTheAction;
    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String sendSmsBtn;
    @SeleniumPath(value = MESSAGE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.TEXT_AREA_HUB2, readValue = MESSAGE)
    private String message;

    private DelayAction delayAction;

    public SendTextMessageAction(){
        if(!isInCase())
            this.objectPrefix = ACTIVITY_PREFIX;

    }

    public void clickSendTextMessageBtn() {
        this.sendSmsBtn = ActivityActions.SendTextMessage.toString();
        this.performTheAction = ActivityActions.SendTextMessage.toString();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void editActivityAction(ActivityAction after) {
        SendTextMessageAction action = (SendTextMessageAction)after;
        if(!getMessage().equals(action.getMessage()))
            this.setMessage(action.getMessage());
    }

    public DelayAction getDelayAction() {
        return delayAction;
    }


    public void setDelayAction(DelayAction delayAction) {
        this.delayAction = delayAction;

    }

    @Override
    public String getPerformTheAction() {
        return performTheAction;
    }


    @Override
    public void clearPerformTheAction() {
        this.performTheAction = null;
        this.sendSmsBtn = null;
    }
    @Override
    public void setNext(int i) {
        super.setNext(i);
        if(delayAction != null) {
            delayAction.setNext(actionIndex);
            delayAction.setIsInCase(isInCase());
            delayAction.setPrefix(getPrefix());
        }
    }

}





