package hub.hub2_0.common.actions;

import hub.base.BaseSeleniumObjectHub2;
import hub.hub2_0.Hub2Constants;

public abstract class ActivityAction extends BaseSeleniumObjectHub2 {

    public static final String ACTIVITY_PREFIX = "activity.";

    public static final String PERFORM_ACTION = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.action-type.{1}\"]";
    public static final String SELECT_ACTION = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.action-type.{1}.{2}\"]";
    public static final String DELETE_ACTION = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.{1}.delete\"]";


    protected int actionIndex;

    public abstract String getPerformTheAction();

    public abstract void clearPerformTheAction();

    public abstract void editActivityAction(ActivityAction after);

    public abstract void setDelayAction(DelayAction delayAction);

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        actionIndex = i;
    }

    @Override
    public void setPrefix(String prefix) {
        this.objectPrefix = prefix;
    }


    @Override
    public int getNext() {
        return actionIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public int getParentIndex() {
        return 0;
    }
}


