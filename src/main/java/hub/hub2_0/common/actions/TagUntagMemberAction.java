package hub.hub2_0.common.actions;

import hub.base.BaseService;
import hub.base.SeleniumPath;
import hub.hub2_0.common.enums.ActivityActions;

public class TagUntagMemberAction extends ActivityAction {

    public static final String TAGS = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.tagMembership.{1}.value\"]/input";


    @SeleniumPath(value = PERFORM_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String performTheAction;
    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String tagUntag;

    @SeleniumPath(value = TAGS, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = TAGS)
    private String tagText;

    private DelayAction delayAction;

    public TagUntagMemberAction(){
        if(!isInCase())
            this.objectPrefix = ACTIVITY_PREFIX;

    }

    public String getTagText() {return tagText;}
    public void setTagText(String tagText)
        {
            this.tagText = tagText;
        }


    @Override
    public void editActivityAction(ActivityAction after) {
        TagUntagMemberAction action = (TagUntagMemberAction)after;
        if(!getTagText().equals(action.getTagText()))
            this.setTagText(action.getTagText());
    }

    public void clickTagBtn() {
        this.tagUntag = ActivityActions.Tag.toString();
        this.performTheAction = ActivityActions.Tag.toString();
    }

    public void clickUnTagBtn() {
        this.tagUntag = ActivityActions.UnTag.toString();
        this.performTheAction = ActivityActions.UnTag.toString();
    }

    public DelayAction getDelayAction() {
        return delayAction;
    }

    public void setDelayAction(DelayAction delayAction) {
        this.delayAction = delayAction;
    }



    @Override
    public String getPerformTheAction() {
        return performTheAction;
    }


    @Override
    public void clearPerformTheAction() {
        this.performTheAction = null;
        this.tagUntag = null;

    }

    @Override
    public void setNext(int i) {
        super.setNext(i);
        if(delayAction != null) {
            delayAction.setNext(actionIndex);
            delayAction.setIsInCase(isInCase());
            delayAction.setPrefix(getPrefix());
        }
    }
}

