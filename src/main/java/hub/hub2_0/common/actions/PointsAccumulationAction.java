package hub.hub2_0.common.actions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.Monetary;
import hub.hub2_0.common.enums.ActivityActions;
import hub.hub2_0.common.items.SelectItems;

public class PointsAccumulationAction extends ActivityAction implements Monetary {

    private static final String ACCUMULATION_TYPE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.addPoints.{1}.type.{2}\"]" ;
    private static final String NUM_OF_POINTS = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.addPoints.{1}.points\"]/input" ;
    private static final String POINTS_RATE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.addPoints.{1}.pointsPerRate\"]/input" ;

    public static final String SELECT_ITEMS_BUTTON = "//*[@class=\"sixteen wide column\"]/div/span//p[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.addPoints.{1}.itemsPopulation.trigger\"]";
    private static final String ACCUMULATION_UNITS = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.addPoints.{1}.budgetType.{2}\"]" ;

    public static final String EXPAND_EXCLUDE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.addPoints.{1}.ExcludeItems.limits\"]/div[@class=\"title\"]";
    private static final String EXCLUDE_CHECKBOX = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.addPoints.{1}.itemsPopulationStatus\"]";
    private static final String EXCLUDE_SELECT_ITEMS_BUTTON = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.addPoints.{1}.excluded.itemsPopulation.trigger\"]";


    @SeleniumPath(value = PERFORM_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String performTheAction;
    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String pointsAccumulationBtn;
    @SeleniumPath(value = ACCUMULATION_TYPE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = ACCUMULATION_TYPE)
    private String accumulationType;
    @SeleniumPath(value = NUM_OF_POINTS, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = NUM_OF_POINTS)
    private String numOfPoints;
    @SeleniumPath(value = POINTS_RATE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = POINTS_RATE)
    private String pointsRate;
    @SeleniumPath(value = SELECT_ITEMS_BUTTON, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String selectItemsButton;
    private SelectItems selectItems;
    @SeleniumPath(value = ACCUMULATION_UNITS, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = ACCUMULATION_UNITS)
    private String accumulationUnits;
    @SeleniumPath(value = EXPAND_EXCLUDE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM, readValue = EXPAND_EXCLUDE)
    private String expandExclude;
    @SeleniumPath(value = EXCLUDE_CHECKBOX, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CHECKBOX_HUB2, readValue = EXCLUDE_CHECKBOX)
    private String excludeItemsCheckbox;
    @SeleniumPath(value = EXCLUDE_SELECT_ITEMS_BUTTON, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String excludeSelectItemsButton;
    private SelectItems selectItemsToExclude;

    private DelayAction delayAction;

    public PointsAccumulationAction(){
        if(!isInCase())
            this.objectPrefix = ACTIVITY_PREFIX;

    }

    public enum AccumulationType {
        ACCUMULATE_FIXED_AMOUNT_OF_POINTS(Hub2Constants.ACCUMULATE_FIXED_AMOUNT_OF_POINTS),
        ACCUMULATE_PERCENTAGE_OF_AMOUNT_SPENT(Hub2Constants.ACCUMULATE_PERCENTAGE_OF_AMOUNT_SPENT),
        ACCUMULATE_BY_RATE(Hub2Constants.ACCUMULATE_BY_RATE);

        private final String accumulationType;
        AccumulationType(final String name) {
            this.accumulationType = name;
        }

        @Override
        public String toString() {
            return accumulationType;
        }
    }

    public enum AccumulationUnits {
        POINTS(Hub2Constants.ACCUMULATION_UNITS_POINTS),
        CREDIT(Hub2Constants.ACCUMULATION_UNITS_CREDIT);

        private final String accumulationUnits;
        AccumulationUnits(final String name) {
            this.accumulationUnits = name;
        }

        @Override
        public String toString() {
            return accumulationUnits;
        }
    }

    public void clickPointsAccumulationBtn() {
        this.performTheAction = ActivityActions.PointsAccumulation.toString();
        this.pointsAccumulationBtn = ActivityActions.PointsAccumulation.toString();
    }

    public String getAccumulationType() {
        return accumulationType;
    }

    public void setAccumulationType(AccumulationType accumulationType) {
        this.accumulationType = accumulationType.toString();
    }

    public String getNumOfPoints() {
        return numOfPoints;
    }

    public void setNumOfPoints(String numOfPoints) {
        this.numOfPoints = numOfPoints;
    }

    public String getPointsRate() {
        return pointsRate;
    }

    public void setPointsRate(String pointsRate) {
        this.pointsRate = pointsRate;
    }

    public void clickSelectItemsButton() {
        this.selectItemsButton = "Click select Items im Points Accumulations action.";
    }

    public SelectItems getSelectItems() {
        return selectItems;
    }

    public void setSelectItems(SelectItems selectItems) {
        this.selectItems = selectItems;
    }

    public String getAccumulationUnits() {
        return accumulationUnits;
    }

    public void setAccumulationUnits(AccumulationUnits accumulationUnits) {
        this.accumulationUnits = accumulationUnits.toString();
    }

    public void clickExpandExclude() {
        this.expandExclude = "Expand exclude items.";
    }

    public void clickExcludeItemsCheckbox() {
        this.excludeItemsCheckbox = "Clicked exclude items checkbox.";
    }

    public void clickExcludeSelectItemsButton() {
        this.excludeSelectItemsButton = "Click select items in Exclude items.";
    }

    public SelectItems getSelectItemsToExclude() {
        return selectItemsToExclude;
    }

    public void setSelectItemsToExclude(SelectItems selectItemsToExclude) {
        clickExpandExclude();
        clickExcludeItemsCheckbox();
        clickExcludeSelectItemsButton();
        this.selectItemsToExclude = selectItemsToExclude;
    }

    @Override
    public String getPerformTheAction() {
        return performTheAction;
    }

    @Override
    public void clearPerformTheAction() {
        this.performTheAction = null;
        this.pointsAccumulationBtn = null;
    }

    @Override
    public void editActivityAction(ActivityAction after) {

    }


    public DelayAction getDelayAction() {
        return delayAction;
    }


    public void setDelayAction(DelayAction delayAction) {
        this.delayAction = delayAction;
    }

    @Override
    public void setNext(int i) {
        super.setNext(i);
        if(delayAction != null) {
            delayAction.setNext(actionIndex);
            delayAction.setIsInCase(isInCase());
            delayAction.setPrefix(getPrefix());
        }
    }
}
