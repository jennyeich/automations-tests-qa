package hub.hub2_0.common.actions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.ActivityActions;

/**
 * Created by Goni on 1/30/2018.
 */

public class UpdateMembershipAction extends ActivityAction {

    private static final String  NUMBER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.updateMembershipExpiration.{1.durationValue\"]/input";

    @SeleniumPath(value = PERFORM_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String performTheAction;
    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String updateMembershipBtn;
    @SeleniumPath(value = NUMBER, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = NUMBER)
    private String number;

    private DelayAction delayAction;

    public UpdateMembershipAction(){
        if(!isInCase())
            this.objectPrefix = ACTIVITY_PREFIX;

    }

    public void clickUpdateMembershipBtn() {
        this.updateMembershipBtn = ActivityActions.UpdateMembership.toString();
        this.performTheAction = ActivityActions.UpdateMembership.toString();
    }


    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }


    @Override
    public void editActivityAction(ActivityAction after) {
        UpdateMembershipAction action = (UpdateMembershipAction)after;
        if(!getNumber().equals(action.getNumber()))
            this.setNumber(action.getNumber());
    }

    @Override
    public String getPerformTheAction() {
        return performTheAction;
    }

    public DelayAction getDelayAction() {
        return delayAction;
    }

    public void setDelayAction(DelayAction delayAction) {
        this.delayAction = delayAction;
    }


    @Override
    public void clearPerformTheAction() {
        this.performTheAction = null;
        this.updateMembershipBtn = null;
    }

    @Override
    public void setNext(int i) {
        super.setNext(i);
        if(delayAction != null) {
            delayAction.setNext(actionIndex);
            delayAction.setIsInCase(isInCase());
            delayAction.setPrefix(getPrefix());
        }
    }
}

