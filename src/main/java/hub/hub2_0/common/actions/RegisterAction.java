package hub.hub2_0.common.actions;

import hub.base.SeleniumPath;
import hub.hub2_0.common.enums.ActivityActions;

/**
 * Created by Goni on 1/29/2018.
 */

public class RegisterAction extends ActivityAction {

    @SeleniumPath(value = PERFORM_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String performTheAction;
    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String registerBtn;

    private DelayAction delayAction;

    public RegisterAction(){
        if(!isInCase())
            this.objectPrefix = ACTIVITY_PREFIX;

    }
    public DelayAction getDelayAction() {
        return delayAction;
    }


    public void setDelayAction(DelayAction delayAction) {
        this.delayAction = delayAction;
    }


    public void clickRegisterBtn() {
        this.registerBtn = ActivityActions.Register.toString();
        this.performTheAction = ActivityActions.Register.toString();
    }

    @Override
    public void editActivityAction(ActivityAction after) {
        RegisterAction action = (RegisterAction)after;

    }

    @Override
    public String getPerformTheAction() {
        return performTheAction;
    }


    @Override
    public void clearPerformTheAction() {
        this.performTheAction = null;
        this.registerBtn = null;
    }

    @Override
    public void setNext(int i) {
        super.setNext(i);
        if(delayAction != null) {
            delayAction.setNext(actionIndex);
            delayAction.setIsInCase(isInCase());
            delayAction.setPrefix(getPrefix());
        }
    }
}




