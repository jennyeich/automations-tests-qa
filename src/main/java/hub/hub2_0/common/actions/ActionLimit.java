package hub.hub2_0.common.actions;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class ActionLimit extends BaseSeleniumObjectHub2 {

    private static final String EXPAND_LIMIT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"accordion.title.{0}actions.assignAsset.{1}.limit\"]";
    private static final String LIMIT_CHECKBOX = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.assignAsset.{1}.limit.active\"]";
    private static final String LIMIT_VALUE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.assignAsset.{1}.limit.value\"]/input";


    @SeleniumPath(value = EXPAND_LIMIT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM, readValue = EXPAND_LIMIT)
    private String expandLimit;
    @SeleniumPath(value = LIMIT_CHECKBOX, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CHECKBOX_HUB2, readValue = LIMIT_CHECKBOX)
    private String limitCheckbox;
    @SeleniumPath(value = LIMIT_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = LIMIT_VALUE)
    private String limitValue;

    public ActionLimit(){
        if(!isInCase())
            this.objectPrefix = ActivityAction.ACTIVITY_PREFIX;

    }
    private int limitIndex = 0;

    public String getExpandLimit() {
        return expandLimit;
    }

    public void clickExpandLimit() {
        this.expandLimit = "clicked expand limit section in action " + limitIndex;
    }

    public String getLimitCheckbox() {
        return limitCheckbox;
    }

    public void clickLimitCheckbox() {
        this.limitCheckbox = "clicked limit checkbox in action" + limitIndex;
    }

    public String getLimitValue() {
        return limitValue;
    }

    public void setLimitValue(String limitValue) {
        this.limitValue = limitValue;
    }

    public   void setNext (int i) {
        this.limitIndex = i;
    }

    public int getNext () {
        return limitIndex;
    }

    public int getNextTag (){ return 0; }
    public String getNextTagsPath(int i) { return null; }
    public int getCreateActionCounter(){ return 0; }


    @Override
    public int getParentIndex() {
        return 0;
    }

}
