package hub.hub2_0.common.actions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.ActivityActions;

/**
 * Created by Goni on 1/28/2018.
 */
@Deprecated
//lottery was removed from hub
public class SendLotteryAction extends ActivityAction {

    private static final String SELECT_LOTTERY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.giveRandomReward.{1}.lotteryKey\"]";
    private static final String  QUANTITY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.giveRandomReward.{1}.quantity\"]/input";

    @SeleniumPath(value = PERFORM_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String performTheAction;
    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String sendLotteryBtn;
    @SeleniumPath(value = SELECT_LOTTERY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_BY_TEXT_HUB2, readValue = SELECT_LOTTERY)
    private String selectLottery;
    @SeleniumPath(value = QUANTITY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = QUANTITY)
    private String quantity;

    private ActionLimit actionLimit;
    private DelayAction delayAction;

    public SendLotteryAction(){
        if(!isInCase())
            this.objectPrefix = ACTIVITY_PREFIX;

    }

    public DelayAction getDelayAction() {
        return delayAction;
    }


    public void setDelayAction(DelayAction delayAction) {
        this.delayAction = delayAction;
    }

    public ActionLimit getActionLimit() {
        return actionLimit;
    }

    public void setActionLimit(String limitValue) {
        actionLimit = new ActionLimit();
        actionLimit.clickExpandLimit();
        actionLimit.clickLimitCheckbox();
        actionLimit.setLimitValue(limitValue);
    }


//    public void clickSendLotteryBtn() {
//        this.sendLotteryBtn = ActivityActions.SendLottery.toString();
//        this.performTheAction = ActivityActions.SendLottery.toString();
//    }

    public String getSelectLottery() {
        return selectLottery;
    }

    public void setSelectLottery(String selectLottery) {
        this.selectLottery = selectLottery;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @Override
    public void editActivityAction(ActivityAction after) {
        SendLotteryAction action = (SendLotteryAction)after;
        if(!getSelectLottery().equals(action.getSelectLottery()))
            this.setSelectLottery(action.getSelectLottery());

        if(!getQuantity().equals(action.getQuantity()))
            this.setQuantity(action.getQuantity());
    }

    @Override
    public String getPerformTheAction() {
        return performTheAction;
    }

    @Override
    public void clearPerformTheAction() {
        this.performTheAction = null;
        this.sendLotteryBtn = null;
    }

    @Override
    public void setNext(int i) {
        super.setNext(i);
        if(actionLimit != null) {
            actionLimit.setNext(actionIndex);
            actionLimit.setIsInCase(isInCase());
            actionLimit.setPrefix(getPrefix());
        }
        if(delayAction != null) {
            delayAction.setNext(actionIndex);
            delayAction.setIsInCase(isInCase());
            delayAction.setPrefix(getPrefix());
        }
    }


}

