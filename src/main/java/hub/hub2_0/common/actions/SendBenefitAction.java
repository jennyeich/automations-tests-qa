package hub.hub2_0.common.actions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.ActivityActions;

/**
 * Created by Goni on 1/28/2018.
 */

public class SendBenefitAction extends ActivityAction {

    private static final String SELECT_BENEFIT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.assignAsset.{1}.benefitKey\"]";
    private static final String  QUANTITY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.assignAsset.{1}.quantity\"]/input";


    @SeleniumPath(value = PERFORM_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String performTheAction;
    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String sendBenefitBtn;
    @SeleniumPath(value = SELECT_BENEFIT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.ADVANCED_SELECT_HUB2, readValue = SELECT_BENEFIT)
    private String selectBenefit;
    @SeleniumPath(value = QUANTITY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = QUANTITY)
    private String quantity;

    private ActionLimit actionLimit;
    private DelayAction delayAction;

    public SendBenefitAction(){
        if(!isInCase())
            this.objectPrefix = ACTIVITY_PREFIX;

    }

    public DelayAction getDelayAction() {
        return delayAction;
    }

    @Override
    public void setDelayAction(DelayAction delayAction) {
        this.delayAction = delayAction;
    }

    public ActionLimit getActionLimit() {
        return actionLimit;
    }

    public void setActionLimit(String limitValue) {
        actionLimit = new ActionLimit();
        actionLimit.clickExpandLimit();
        actionLimit.clickLimitCheckbox();
        actionLimit.setLimitValue(limitValue);
    }

    public void clickSendBenefitBtn() {
        this.sendBenefitBtn = ActivityActions.SendBenefit.toString();
        this.performTheAction = ActivityActions.SendBenefit.toString();
    }

    public String getSelectBenefit() {
        return selectBenefit;
    }

    public void setSelectBenefit(String selectBenefit) {
        this.selectBenefit = selectBenefit;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @Override
    public void editActivityAction(ActivityAction after) {
        SendBenefitAction action = (SendBenefitAction)after;
        if(!getSelectBenefit().equals(action.getSelectBenefit()))
            this.setSelectBenefit(action.getSelectBenefit());

        if(!getQuantity().equals(action.getQuantity()))
            this.setQuantity(action.getQuantity());
    }


    @Override
    public String getPerformTheAction() {
        return performTheAction;
    }


    @Override
    public void clearPerformTheAction() {
        this.performTheAction = null;
        this.sendBenefitBtn = null;
    }

    @Override
    public void setNext(int i) {
        super.setNext(i);
        if(actionLimit != null) {
            actionLimit.setNext(actionIndex);
            actionLimit.setIsInCase(isInCase());
            actionLimit.setPrefix(getPrefix());
        }
        if(delayAction != null) {
            delayAction.setNext(actionIndex);
            delayAction.setIsInCase(isInCase());
            delayAction.setPrefix(getPrefix());
        }
    }

}

