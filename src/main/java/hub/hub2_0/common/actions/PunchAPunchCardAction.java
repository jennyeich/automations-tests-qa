package hub.hub2_0.common.actions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.ActivityActions;

/**
 * Created by Goni on 1/30/2018.
 */

public class PunchAPunchCardAction extends ActivityAction{

    private static final String SELECT_BENEFIT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.punch.{1}.punchCardKey\"]";
    private static final String  NUM_OF_PUNCHES = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.punch.{1}.numberOfPunches\"]/input";

    @SeleniumPath(value = PERFORM_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String performTheAction;
    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String punchAPunchcardBtn;
    @SeleniumPath(value = SELECT_BENEFIT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.ADVANCED_SELECT_HUB2, readValue = SELECT_BENEFIT)
    private String selectBenefit;
    @SeleniumPath(value = NUM_OF_PUNCHES, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = NUM_OF_PUNCHES)
    private String numOfPunches;

    private ActionLimit actionLimit;
    private DelayAction delayAction;

    public PunchAPunchCardAction(){
        if(!isInCase())
            this.objectPrefix = ACTIVITY_PREFIX;

    }

    public DelayAction getDelayAction() { return delayAction; }

    public void setDelayAction(DelayAction delayAction) {
        this.delayAction = delayAction;

    }

    public ActionLimit getActionLimit() {
        return actionLimit;
    }

    public void setActionLimit(String limitValue) {
        actionLimit = new ActionLimit();
        actionLimit.clickExpandLimit();
        actionLimit.clickLimitCheckbox();
        actionLimit.setLimitValue(limitValue);
    }


    public void clickPunchAPunchCardBtn() {
        this.punchAPunchcardBtn = ActivityActions.PunchThePunchCard.toString();
        this.performTheAction = ActivityActions.PunchThePunchCard.toString();
    }

    public String getSelectBenefit() {
        return selectBenefit;
    }

    public void setSelectBenefit(String selectBenefit) {
        this.selectBenefit = selectBenefit;
    }

    public String getNumOfPunches() {
        return numOfPunches;
    }

    public void setNumOfPunches(String numOfPunches) {
        this.numOfPunches = numOfPunches;
    }


    @Override
    public void editActivityAction(ActivityAction after) {
        PunchAPunchCardAction action = (PunchAPunchCardAction)after;
        if(!getSelectBenefit().equals(action.getSelectBenefit()))
            this.setSelectBenefit(action.getSelectBenefit());

        if(!getNumOfPunches().equals(action.getNumOfPunches()))
            this.setNumOfPunches(action.getNumOfPunches());
    }


    @Override
    public String getPerformTheAction() {
        return performTheAction;
    }


    @Override
    public void clearPerformTheAction() {
        this.performTheAction = null;
        this.punchAPunchcardBtn = null;
    }

    @Override
    public void setNext(int i) {
        super.setNext(i);
        if(actionLimit != null) {
            actionLimit.setNext(actionIndex);
            actionLimit.setIsInCase(isInCase());
            actionLimit.setPrefix(getPrefix());
        }
        if(delayAction != null) {
            delayAction.setNext(actionIndex);
            delayAction.setIsInCase(isInCase());
            delayAction.setPrefix(getPrefix());
        }
    }

}

