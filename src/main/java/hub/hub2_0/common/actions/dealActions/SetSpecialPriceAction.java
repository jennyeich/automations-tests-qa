package hub.hub2_0.common.actions.dealActions;

import hub.base.BaseService;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.DelayAction;
import hub.hub2_0.common.conditions.Monetary;
import hub.hub2_0.common.enums.ActivityActions;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.common.items.SelectPopulation;

public class SetSpecialPriceAction extends ActivityAction implements Monetary {

    private static final String SELECT_ACTION ="//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}actions.action-type.{1}.{2}\")]" ;
    private static final String SELECT_ITEMS_TRIGGER = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountSpecialPrice{1}.{2}.itemsPopulation.trigger\"]";
    private static final String PRICE_INPUT = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountSpecialPrice{1}.{2}.amount\"]/input";

    private static final String LIMIT_DISCOUNT_EXPAND = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountSpecialPrice{1}.{2}.limits\"]/div[@class=\"title\"]";
    private static final String LIMIT_DISCOUNT_TIMES_CHECKBOX = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountSpecialPrice{1}.{2}.limit.checkbox\"]";
    private static final String LIMIT_DISCOUNT_TIMES_INPUT = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountSpecialPrice{1}.{2}.limit.value\"]/input";
    private static final String LIMIT_DISCOUNT_AMOUNT_CHECKBOX = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountSpecialPrice{1}.{2}.amountLimit.checkbox\"]";
    private static final String LIMIT_DISCOUNT_AMOUNT_INPUT = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountSpecialPrice{1}.{2}.amountLimit.value\"]/input";

    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = SELECT_ACTION)
    private String setSpecialPrice;
    @SeleniumPath(value = SELECT_ITEMS_TRIGGER, type = SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String selectItemsButton;
    private SelectPopulation selectPopulation;
    @SeleniumPath(value = PRICE_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = PRICE_INPUT)
    private String specialPriceValue;
    @SeleniumPath(value = LIMIT_DISCOUNT_EXPAND, type = SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String limitDiscountExpand;
    @SeleniumPath(value = LIMIT_DISCOUNT_TIMES_CHECKBOX, type = SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT, elementInputType = SeleniumPath.ElementInputType.CHECKBOX_HUB2, readValue = LIMIT_DISCOUNT_TIMES_CHECKBOX)
    private String limitDiscountTimesCheckbox;
    @SeleniumPath(value = LIMIT_DISCOUNT_TIMES_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = LIMIT_DISCOUNT_TIMES_INPUT)
    private String limitDiscountTimesValue;
    @SeleniumPath(value = LIMIT_DISCOUNT_AMOUNT_CHECKBOX, type = SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT, elementInputType = SeleniumPath.ElementInputType.CHECKBOX_HUB2, readValue = LIMIT_DISCOUNT_AMOUNT_CHECKBOX)
    private String limitDiscountAmountCheckbox;
    @SeleniumPath(value = LIMIT_DISCOUNT_AMOUNT_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = LIMIT_DISCOUNT_AMOUNT_INPUT)
    private String limitDiscountAmountValue;

    public SetSpecialPriceAction(){
        if(!isInCase())
            this.objectPrefix = ActivityAction.ACTIVITY_PREFIX;
    }

    public void clickSpecialPrice() {
        this.setSpecialPrice = ActivityActions.SetSpecialPrice.toString();
    }

    private void clickSelectItemsButton() {
        this.selectItemsButton = "Clicked Select items in special price";
    }

    public SelectPopulation getSelectItems() {
        return selectPopulation;
    }

    public void setSelectItems(SelectPopulation selectPopulation) {
        this.clickSelectItemsButton();
        this.selectPopulation = selectPopulation;
    }

    public String getSpecialPriceValue() {
        return specialPriceValue;
    }

    public void setSpecialPriceValue(String specialPriceValue) {
        this.specialPriceValue = specialPriceValue;
    }

    public void clickLimitDiscountExpand() {
        this.limitDiscountExpand = "Clicked expand 'discount limit' section";
    }

    public String getLimitDiscountTimesCheckboxState() {
        return limitDiscountTimesCheckbox;
    }

    public void clickLimitDiscountTimesCheckbox() {
        this.limitDiscountTimesCheckbox = "Clicked 'limit discount by times' checkbox";
    }

    public String getLimitDiscountTimesValue() {
        return limitDiscountTimesValue;
    }

    public void setLimitDiscountTimesValue(String limitDiscountTimesValue) {
        this.limitDiscountTimesValue = limitDiscountTimesValue;
    }

    public String getLimitDiscountAmountCheckboxState() {
        return limitDiscountAmountCheckbox;
    }

    public void clickLimitDiscountAmountCheckbox() {
        this.limitDiscountAmountCheckbox = "Clicked 'limit discount by amount' checkbox";
    }

    public String getLimitDiscountAmountValue() {
        return limitDiscountAmountValue;
    }

    public void setLimitDiscountAmountValue(String limitDiscountAmountValue) {
        this.limitDiscountAmountValue = limitDiscountAmountValue;
    }

    public String getPerformTheAction() {
        return null;
    }

    public void clearPerformTheAction(){ }
    public void editActivityAction(ActivityAction after){}

    public void setDelayAction(DelayAction delayAction){}
}
