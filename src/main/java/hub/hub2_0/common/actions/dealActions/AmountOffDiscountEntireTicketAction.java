package hub.hub2_0.common.actions.dealActions;

import hub.base.BaseService;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.DelayAction;
import hub.hub2_0.common.conditions.Monetary;
import hub.hub2_0.common.enums.ActivityActions;
import hub.hub2_0.common.items.SelectItems;

public class AmountOffDiscountEntireTicketAction extends ActivityAction implements Monetary {

    private static final String SELECT_ACTION = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}actions.action-type.{1}.{2}\")]";
    private static final String DISCOUNT_AMOUNT = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountAmountOffOnEntireTicket.{1}.amount\"]/input";
    private static final String EXCLUDE_ITEMS_EXPAND = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountAmountOffOnEntireTicket.{1}.excludeItems.limits\"]/div[@class=\"title\"]";
    private static final String EXCLUDE_ITEMS_CHECKBOX = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountAmountOffOnEntireTicket.{1}.excludeItems.checkbox\"]";
    private static final String EXCLUDE_ITEMS_TRIGGER = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountAmountOffOnEntireTicket.{1}.excludeItems.itemsPopulation.trigger\"]";

    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = SELECT_ACTION)
    private String amountOffDiscount;
    @SeleniumPath(value = DISCOUNT_AMOUNT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = DISCOUNT_AMOUNT)
    private String discountAmount;
    @SeleniumPath(value = EXCLUDE_ITEMS_EXPAND, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String excludeItemsExpand;
    @SeleniumPath(value = EXCLUDE_ITEMS_CHECKBOX, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CHECKBOX_HUB2, readValue = EXCLUDE_ITEMS_CHECKBOX)
    private String excludeItemsCheckbox;
    @SeleniumPath(value = EXCLUDE_ITEMS_TRIGGER, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String excludeItemsButton;
    private SelectItems selectItemsToExclude;

    public AmountOffDiscountEntireTicketAction() {
        if (!isInCase())
            this.objectPrefix = ActivityAction.ACTIVITY_PREFIX;
    }

    public void clickAmountOffDiscount() {
        this.amountOffDiscount = ActivityActions.AmountOffDiscount.toString();
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public void clickExcludeItemsExpand() {
        this.excludeItemsExpand = "Clicked expand 'Don't discount these items'";
    }

    public String getExcludeItemsCheckboxState() {
        return excludeItemsCheckbox;
    }

    public void clickExcludeItemsCheckbox() {
        this.excludeItemsCheckbox = "Clicked 'Exclude items from discount' checkbox";
    }

    private void clickExcludeItemsButton() {
        this.excludeItemsButton = "Clicked 'select items' to exclude from discount";
    }

    public SelectItems getSelectItemsToExclude() {
        return selectItemsToExclude;
    }

    public void setSelectItemsToExclude(SelectItems selectItemsToExclude) {
        this.clickExcludeItemsButton();
        this.selectItemsToExclude = selectItemsToExclude;
    }

    public String getPerformTheAction() {
        return null;
    }

    public void clearPerformTheAction() {
    }

    public void editActivityAction(ActivityAction after) {
    }

    public void setDelayAction(DelayAction delayAction) {
    }

}
