package hub.hub2_0.common.actions.dealActions;

import hub.base.BaseService;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.DelayAction;
import hub.hub2_0.common.enums.ActivityActions;
import hub.hub2_0.common.items.SelectItems;

public class PercentOffDiscountEntireTicketAction extends ActivityAction {

    private static final String SELECT_ACTION = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}actions.action-type.{1}.{2}\")]";
    private static final String DISCOUNT_PERCENTAGE = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountPercentOffOnEntireTicket.{1}.percent\"]/input";
    private static final String EXCLUDE_ITEMS_EXPAND = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountPercentOffOnEntireTicket.{1}.excludeItems.limits\"]/div[@class=\"title\"]";
    private static final String EXCLUDE_ITEMS_CHECKBOX = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountPercentOffOnEntireTicket.{1}.excludeItems.checkbox\"]";
    private static final String EXCLUDE_ITEMS_TRIGGER = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountPercentOffOnEntireTicket.{1}.excludeItems.itemsPopulation.trigger\"]";
    private static final String LIMIT_DISCOUNT_EXPAND = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountPercentOffOnEntireTicket.{1}.limits\"]/div[@class=\"title\"]";
    private static final String LIMIT_DISCOUNT_AMOUNT_CHECKBOX = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountPercentOffOnEntireTicket.{1}.amountLimit.checkbox\"]";
    private static final String LIMIT_DISCOUNT_AMOUNT_INPUT = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountPercentOffOnEntireTicket.{1}.amountLimit.value\"]/input";


    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = SELECT_ACTION)
    private String percentOffDiscount;
    @SeleniumPath(value = DISCOUNT_PERCENTAGE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = DISCOUNT_PERCENTAGE)
    private String discountPercentage;
    @SeleniumPath(value = EXCLUDE_ITEMS_EXPAND, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String excludeItemsExpand;
    @SeleniumPath(value = EXCLUDE_ITEMS_CHECKBOX, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CHECKBOX_HUB2, readValue = EXCLUDE_ITEMS_CHECKBOX)
    private String excludeItemsCheckbox;
    @SeleniumPath(value = EXCLUDE_ITEMS_TRIGGER, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String excludeItemsButton;
    private SelectItems selectItemsToExclude;

    @SeleniumPath(value = LIMIT_DISCOUNT_EXPAND, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String limitDiscountExpand;
    @SeleniumPath(value = LIMIT_DISCOUNT_AMOUNT_CHECKBOX, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CHECKBOX_HUB2, readValue = LIMIT_DISCOUNT_AMOUNT_CHECKBOX)
    private String limitDiscountCheckbox;
    @SeleniumPath(value = LIMIT_DISCOUNT_AMOUNT_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = LIMIT_DISCOUNT_AMOUNT_INPUT)
    private String limitDiscountValue;


    public PercentOffDiscountEntireTicketAction() {
        if (!isInCase())
            this.objectPrefix = ActivityAction.ACTIVITY_PREFIX;
    }


    public void clickPercentOffDiscount() {
        this.percentOffDiscount = ActivityActions.PercentOffDiscount.toString();
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public void clickExcludeItemsExpand() {
        this.excludeItemsExpand = "Clicked expand 'Don't discount these items'";
    }

    public String getExcludeItemsCheckboxState() {
        return excludeItemsCheckbox;
    }

    public void clickExcludeItemsCheckbox() {
        this.excludeItemsCheckbox = "Clicked 'Exclude items from discount' checkbox";
    }

    private void clickExcludeItemsButton() {
        this.excludeItemsButton = "Clicked 'select items' to exclude from discount";
    }

    public SelectItems getSelectItemsToExclude() {
        return selectItemsToExclude;
    }

    public void setSelectItemsToExclude(SelectItems selectItemsToExclude) {
        this.clickExcludeItemsButton();
        this.selectItemsToExclude = selectItemsToExclude;
    }

    public void clickLimitDiscountExpand() {
        this.limitDiscountExpand = "Clicked 'Limit discount expand'";
    }

    public String getLimitDiscountCheckboxState() {
        return limitDiscountCheckbox;
    }

    public void clickLimitDiscountCheckbox() {
        this.limitDiscountCheckbox = "Clicked 'Limit total discount' checkbox";
    }

    public String getLimitDiscountValue() {
        return limitDiscountValue;
    }

    public void setLimitDiscountValue(String limitDiscountValue) {
        this.limitDiscountValue = limitDiscountValue;
    }


    public String getPerformTheAction() {
        return null;
    }

    public void clearPerformTheAction() {
    }

    public void editActivityAction(ActivityAction after) {
    }

    public void setDelayAction(DelayAction delayAction) {
    }

}
