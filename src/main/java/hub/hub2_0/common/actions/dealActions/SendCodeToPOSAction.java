package hub.hub2_0.common.actions.dealActions;

import hub.base.BaseService;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.DelayAction;
import hub.hub2_0.common.enums.ActivityActions;

public class SendCodeToPOSAction extends ActivityAction {

    private static final String CODE_INPUT = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.sendCodesToPos.{1}.value\"]/input";
    private static final String CODE_TYPE = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.sendCodesToPos.{1}.posCodeType.{2}\"]";

    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = SELECT_ACTION)
    private String sendCodeToPOS;
    @SeleniumPath(value = CODE_TYPE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = CODE_TYPE)
    private String codeType;
    @SeleniumPath(value = CODE_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_CAPSULA_HUB2, readValue = CODE_INPUT)
    private String codeInput;
    @SeleniumPath(value = DELETE_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String clickDelete;

    public enum CodeType {
        ITEM_CODE(Hub2Constants.DEALS_ITEM_CODE),
        DEAL_CODE(Hub2Constants.DEALS_DEAL_CODE);

        private final String codeType;

        CodeType(final String codeType) {
            this.codeType = codeType;
        }

        @Override
        public String toString() {
            return codeType;
        }
    }


    public SendCodeToPOSAction() {
        if (!isInCase())
            this.objectPrefix = ActivityAction.ACTIVITY_PREFIX;
    }

    public void clickDealCodeToPOS() {
        this.sendCodeToPOS = ActivityActions.SendCodeToPOS.toString();
    }

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(CodeType codeType) {
        this.codeType = codeType.toString();
    }

    public String getCodeInput() {
        return codeInput;
    }

    public void setCodeInput(String codeInput) {
        this.codeInput = codeInput;
    }

    public void clickDeleteAction() {
        this.clickDelete = "Delete action 'Send code to POS'";
    }


    public String getPerformTheAction() {
        return null;
    }

    public void clearPerformTheAction() {
    }

    public void editActivityAction(ActivityAction after) {
    }

    public void setDelayAction(DelayAction delayAction) {
    }

}
