package hub.hub2_0.common.actions.dealActions;

import hub.base.BaseService;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.DelayAction;
import hub.hub2_0.common.conditions.Monetary;
import hub.hub2_0.common.enums.ActivityActions;
import hub.hub2_0.common.items.SelectPopulation;

public class AmountOffDiscountAction extends ActivityAction implements Monetary {

    private static final String SELECT_ACTION ="//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"{0}actions.action-type.{1}.{2}\")]" ;
    private static final String DISCOUNT_AMOUNT = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountAmountOff{1}.{2}.amount\"]/input";
    private static final String SELECT_ITEMS_TRIGGER = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountAmountOff{1}.{2}.itemsPopulation.trigger\"]";
    private static final String LIMIT_DISCOUNT_EXPAND = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountAmountOff{1}.{2}.limits\"]/div[@class=\"title\"]";
    private static final String LIMIT_DISCOUNT_TIMES_CHECKBOX = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountAmountOff{1}.{2}.limit.checkbox\"]";
    private static final String LIMIT_DISCOUNT_TIMES_INPUT = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountAmountOff{1}.{2}.limit.value\"]/input";
    private static final String LIMIT_DISCOUNT_AMOUNT_CHECKBOX = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountAmountOff{1}.{2}.amountLimit.checkbox\"]";
    private static final String LIMIT_DISCOUNT_AMOUNT_INPUT = "//*[@" + BaseService.HUB2_AUTOMATION_STR + "=\"{0}actions.discountAmountOff{1}.{2}.amountLimit.value\"]/input";

    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = SELECT_ACTION)
    private String amountOffDiscount;
    @SeleniumPath(value = DISCOUNT_AMOUNT, type = SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = DISCOUNT_AMOUNT)
    private String discountAmount;
    @SeleniumPath(value = SELECT_ITEMS_TRIGGER, type = SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String selectItemsButton;
    private SelectPopulation selectItems;
    @SeleniumPath(value = LIMIT_DISCOUNT_EXPAND, type = SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String limitDiscountExpand;
    @SeleniumPath(value = LIMIT_DISCOUNT_TIMES_CHECKBOX, type = SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT, elementInputType = SeleniumPath.ElementInputType.CHECKBOX_HUB2, readValue = LIMIT_DISCOUNT_TIMES_CHECKBOX)
    private String limitDiscountTimesCheckbox;
    @SeleniumPath(value = LIMIT_DISCOUNT_TIMES_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = LIMIT_DISCOUNT_TIMES_INPUT)
    private String limitDiscountTimesValue;
    @SeleniumPath(value = LIMIT_DISCOUNT_AMOUNT_CHECKBOX, type = SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT, elementInputType = SeleniumPath.ElementInputType.CHECKBOX_HUB2, readValue = LIMIT_DISCOUNT_AMOUNT_CHECKBOX)
    private String limitDiscountAmountCheckbox;
    @SeleniumPath(value = LIMIT_DISCOUNT_AMOUNT_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH_DISCOUNT, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = LIMIT_DISCOUNT_AMOUNT_INPUT)
    private String limitDiscountAmountValue;


    public AmountOffDiscountAction(){
        if(!isInCase())
            this.objectPrefix = ActivityAction.ACTIVITY_PREFIX;
    }


    public void clickAmountOffDiscount() {
        this.amountOffDiscount = ActivityActions.AmountOffDiscount.toString();
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    private void clickSelectItemsButton() {
        this.selectItemsButton = "Clicked 'select items'";
    }

    public SelectPopulation getSelectItems() {
        return selectItems;
    }

    public void setSelectItems(SelectPopulation selectItems) {
        this.clickSelectItemsButton();
        this.selectItems = selectItems;
    }

    public void clickLimitDiscountExpand() {
        this.limitDiscountExpand = "Clicked 'Limit discount expand'";
    }

    public String getLimitDiscountTimesCheckboxState() {
        return limitDiscountTimesCheckbox;
    }

    public void clickLimitDiscountTimesCheckbox() {
        this.limitDiscountTimesCheckbox = " Clicked 'Limit Discount Times Checkbox'";
    }

    public String getLimitDiscountTimesValue() {
        return limitDiscountTimesValue;
    }

    public void setLimitDiscountTimesValue(String limitDiscountTimesValue) {
        this.limitDiscountTimesValue = limitDiscountTimesValue;
    }

    public String getLimitDiscountAmountCheckboxState() {
        return limitDiscountAmountCheckbox;
    }

    public void clickLimitDiscountAmountCheckbox() {
        this.limitDiscountAmountCheckbox = "Clicked 'Limit total discount' checkbox";
    }

    public String getLimitDiscountAmountValue() {
        return limitDiscountAmountValue;
    }

    public void setLimitDiscountAmountValue(String limitDiscountAmountValue) {
        this.limitDiscountAmountValue = limitDiscountAmountValue;
    }


    public String getPerformTheAction() {
        return null;
    }

    public void clearPerformTheAction(){ }
    public void editActivityAction(ActivityAction after){}

    public void setDelayAction(DelayAction delayAction){}

}
