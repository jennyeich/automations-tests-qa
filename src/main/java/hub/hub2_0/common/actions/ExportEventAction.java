package hub.hub2_0.common.actions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.ActivityActions;

public class ExportEventAction extends ActivityAction{

    private static final String NAME = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.exportEvent.{1}.name\"]/input";
    private static final String URL = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.exportEvent.{1}.destination\"]/input";


    @SeleniumPath(value = PERFORM_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String performTheAction;
    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String exportEventButton;
    @SeleniumPath(value = NAME, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = NAME)
    private String name;
    @SeleniumPath(value = URL, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = URL)
    private String url;

    private DelayAction delayAction;

    public ExportEventAction(){
        if(!isInCase())
            this.objectPrefix = ACTIVITY_PREFIX;

    }

    public DelayAction getDelayAction() {
        return delayAction;
    }

    public void setDelayAction(DelayAction delayAction) {
        this.delayAction = delayAction;
    }


    public String getExportEventButton() {
        return exportEventButton;
    }

    public void clickExportEventButton() {
        this.exportEventButton = ActivityActions.ExportEvent.toString();
        this.performTheAction = ActivityActions.ExportEvent.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    @Override
    public void editActivityAction(ActivityAction after) {
        ExportEventAction action = (ExportEventAction)after;
        if(!getName().equals(action.getName()))
            this.setName(action.getName());

        if(!getUrl().equals(action.getUrl()))
            this.setUrl(action.getUrl());
    }

    @Override
    public String getPerformTheAction() {
        return performTheAction;
    }


    @Override
    public void clearPerformTheAction() {
        this.performTheAction = null;
        this.exportEventButton = null;
    }
    @Override
    public void setNext(int i) {
        super.setNext(i);
        if(delayAction != null) {
            delayAction.setNext(actionIndex);
            delayAction.setIsInCase(isInCase());
            delayAction.setPrefix(getPrefix());
        }
    }

}
