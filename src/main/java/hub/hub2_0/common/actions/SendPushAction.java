package hub.hub2_0.common.actions;

import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.ActivityActions;

/**
 * Created by Goni on 1/29/2018.
 */

public class SendPushAction extends ActivityAction {

    private static final String MESSAGE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.sendPersonalPush.{1}.message.mentionTextArea\"]";
    private static final String OPEN_SCREEN_AT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.sendPersonalPush.{1}.genericActionArgs.appScreen.{2}\"]";
    private static final String BUTTON_TEXT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"{0}actions.sendPersonalPush.{1}.genericActionArgs.buttonText\"]/input";

    @SeleniumPath(value = PERFORM_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String performTheAction;
    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String sendPushBtn;
    @SeleniumPath(value = MESSAGE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.TEXT_AREA_HUB2, readValue = MESSAGE)
    private String message;
    @SeleniumPath(value = OPEN_SCREEN_AT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = OPEN_SCREEN_AT)
    private String openScreenAt;
    @SeleniumPath(value = BUTTON_TEXT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2 ,readValue = BUTTON_TEXT)
    private String buttonText;


    private DelayAction delayAction;

    public SendPushAction(){
        if(!isInCase())
            this.objectPrefix = ACTIVITY_PREFIX;

    }

    public DelayAction getDelayAction() {
        return delayAction;
    }


    public void setDelayAction(DelayAction delayAction) {
        this.delayAction = delayAction;

    }


    public void clickSendPushBtn() {
        this.sendPushBtn = ActivityActions.SendAPushNotification.toString();
        this.performTheAction = ActivityActions.SendAPushNotification.toString();
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOpenScreenAt() {
        return openScreenAt;
    }

    public void setOpenScreenAt(String openScreenAt) {
        this.openScreenAt = openScreenAt;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    @Override
    public void editActivityAction(ActivityAction after) {
        SendPushAction action = (SendPushAction)after;
        if(!getMessage().equals(action.getMessage()))
            this.setMessage(action.getMessage());
    }

    @Override
    public String getPerformTheAction() {
        return performTheAction;
    }


    @Override
    public void clearPerformTheAction() {
        this.performTheAction = null;
        this.sendPushBtn = null;
    }

    @Override
    public void setNext(int i) {
        super.setNext(i);
        if(delayAction != null) {
            delayAction.setNext(actionIndex);
            delayAction.setIsInCase(isInCase());
            delayAction.setPrefix(getPrefix());
        }
    }
}



