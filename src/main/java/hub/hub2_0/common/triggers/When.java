package hub.hub2_0.common.triggers;

import hub.base.BaseSeleniumObject;
import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class When extends BaseSeleniumObjectHub2 {

    public static final String WHEN = "trigger.select";
    public static final String SEE_MORE = "trigger.see-more";
    public static final String SELECT_TRIGGER = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"trigger.{0}\"]";



    @SeleniumPath(value =WHEN, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String when;
    @SeleniumPath(value = SEE_MORE, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String seeMore;
    @SeleniumPath(value = SELECT_TRIGGER, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String selectTrigger;


    public void clickWhen() {
        this.when = "Click on When";
        this.seeMore = "Click on see more";
    }

    public String getSelectTrigger() {
        return selectTrigger;
    }

    public void setSelectTrigger(String selectTrigger) {
        this.selectTrigger = selectTrigger;
    }

    public int getCreateActionCounter() {
        return 0;
    }
    public int getNextTag () {return 0;}
    public String getNextTagsPath (int i){return "";}
    public void setNext (int counter) {}
    public int getNext (){return 0;}

    @Override
    public void setParentIndex(int i) {

    }

    @Override
    public int getParentIndex() {
        return 0;
    }
}

