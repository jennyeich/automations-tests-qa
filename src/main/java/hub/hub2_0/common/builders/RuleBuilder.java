package hub.hub2_0.common.builders;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.Rule;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.basePages.loyalty.cases.RuleCase;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.actions.PointsAccumulationAction;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.enums.ActivityActions;
import hub.hub2_0.common.enums.SelectedItemsDialogButtons;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.common.triggers.When;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;


public class RuleBuilder extends LoyaltyBuilder {

    public final static Logger log4j = Logger.getLogger(RuleBuilder.class);

    ////**** Build simple rules ****////

    public Activity buildActivity(String trigger, Box campaign, Rule rule, List<ActivityActions> activityActionList, String freeTxt ) throws Exception {
        return buildActivity(trigger, campaign, rule, Lists.newArrayList(), activityActionList,freeTxt);
    }

    public Activity buildActivity(String trigger, Box campaign, Rule rule, List<ApplyCondition> conditions, List<ActivityActions> activityActionList, String freeTxt) throws Exception {
        List<ActivityAction> actions = new ArrayList<>();
        for(ActivityActions activityActions : activityActionList) {
            actions.add(ActionsBuilder.buildAction(activityActions.toString(), freeTxt));
        }
        Activity currentActivity = buildRule(trigger, campaign, rule, conditions, actions);

        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    public Activity buildActivity(String trigger, Box campaign, Rule rule, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        Activity currentActivity = buildRule(trigger, campaign, rule, conditions, activityActionList);
        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    private Activity buildRule(String trigger, Box campaign, Rule rule, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        setTrigger(trigger, rule);
        buildActivity(campaign, rule, conditions, activityActionList);
        return rule;
    }

    ////**** Build rule with cases ****////

    public Activity buildActivityWithCases(String trigger, Box campaign, Rule rule, List<ApplyCondition> conditions, List<Case> cases) throws Exception {
        setTrigger(trigger, rule);
        Activity currentActivity = buildActivityWithCases(rule, conditions, cases);
        log4j.info("Create Rule with trigger: " + trigger);
        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    public Activity buildActivityWithCases(String trigger, Box campaign, Rule rule, List<ApplyCondition> conditions, List<Case> cases, boolean editMode) throws Exception {
        Activity currentActivity = null;
        setTrigger(trigger, rule);
        if(editMode){
            currentActivity = buildRuleWithCasesForEdit(trigger, rule, conditions, cases);
        }else {
            currentActivity = buildActivityWithCases(rule, conditions, cases);
            currentActivity.clickSplitCases();
        }
        log4j.info("Create Rule with trigger: " + trigger);
        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    public Activity buildRuleWithCasesForEdit(String trigger, Rule rule, List<ApplyCondition> conditions,List<Case> cases) throws Exception {
        // Update when object - trigger and conditions.
        setTrigger(trigger, rule);
        setActivityConditions(rule, conditions);
        for (Case aCase:cases) {
            rule.addCase(aCase);
        }
        log4j.info("Update Rule with trigger: " + trigger);
        return rule;
    }

    ////**** Build rule with per occurrences mode ****////

    public Activity buildActivity(String trigger, Box campaign, Rule rule, List<ApplyCondition> conditions, List<OccurrencesCondition> occurrencesConditionList, List<ActivityAction> activityActionList) throws Exception {
        setTrigger(trigger, rule);
        Activity currentActivity = buildActivityPerOccurrencesMode(rule, conditions, occurrencesConditionList, activityActionList);
        log4j.info("Total Actions in Rule (BOX:" + campaign.getBoxName() + ")-->" + rule.getActions().size());
        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    ////**** Build rule from template ****////

    public Activity buildActivityFromTemplate(String trigger, Box campaign, Rule rule, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        Activity currentActivity = buildRuleFromTemplate(trigger, campaign, rule, conditions, activityActionList);
        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    public Activity buildRuleFromTemplate(String trigger, Box campaign, Rule rule, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        // Update when object - trigger and conditions.
        setTrigger(trigger, rule);
        Case caze = buildCaseSimpleMode(activityActionList);
        rule.addCase(caze);
        setActivityConditions(rule, conditions);

        log4j.info("Total Actions in Rule (BOX:" + campaign.getBoxName() + ")-->" + caze.getActions().size());
        return rule;
    }

    private void setTrigger(String trigger, Rule rule) {
        if(trigger != null){
            When when = setWhen(trigger);
            rule.setWhen(when);
        }
    }

    private When setWhen(String triggerName) {
        When when  = new When();
        when.setSelectTrigger(triggerName);
        return when;
    }

    protected Activity buildPerOccurrencesMode(Activity activity, List<OccurrencesCondition> occurrencesCondition, List<ActivityAction> actions) {
        ((Rule)activity).clickPerOccurrencesMode();
        activity.setOccurrencesConditions(occurrencesCondition);
        activity.setActions(actions);
        return activity;
    }

    protected Case buildCaseSimpleMode(List<ActivityAction> actions) throws Exception {
        Case caze = new RuleCase();
        caze.setActions(actions);
        return caze;
    }

    public PointsAccumulationAction getAccumulationActionByRate(String numOfPoints, String forEvery, PointsAccumulationAction.AccumulationUnits units, SelectItems selectItems, SelectItems excludeItems) {
        PointsAccumulationAction pointsAccumulationAction = getBasicPointsAccumulation(numOfPoints, units);
        pointsAccumulationAction.setAccumulationType(PointsAccumulationAction.AccumulationType.ACCUMULATE_BY_RATE);
        pointsAccumulationAction.setPointsRate(forEvery);
        pointsAccumulationAction.clickSelectItemsButton();

        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        pointsAccumulationAction.setSelectItems(selectItems);
        if (excludeItems != null) {
            excludeItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
            pointsAccumulationAction.setSelectItemsToExclude(excludeItems);
        }
        return pointsAccumulationAction;
    }

    public PointsAccumulationAction getAccumulationActionByRate(String numOfPoints, String forEvery, SelectItems selectItems, SelectItems excludeItems) {
        PointsAccumulationAction pointsAccumulationAction = getBasicPointsAccumulation(numOfPoints);
        pointsAccumulationAction.setAccumulationType(PointsAccumulationAction.AccumulationType.ACCUMULATE_BY_RATE);
        pointsAccumulationAction.setPointsRate(forEvery);
        pointsAccumulationAction.clickSelectItemsButton();

        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        pointsAccumulationAction.setSelectItems(selectItems);
        if (excludeItems != null) {
            excludeItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
            pointsAccumulationAction.setSelectItemsToExclude(excludeItems);
        }
        return pointsAccumulationAction;
    }

    private PointsAccumulationAction getBasicPointsAccumulation(String numOfPoints, PointsAccumulationAction.AccumulationUnits units) {
        PointsAccumulationAction pointsAccumulationAction = getBasicPointsAccumulation(numOfPoints);
        pointsAccumulationAction.setAccumulationUnits(units);
        return pointsAccumulationAction;
    }

    /* this method is used in case of setting wallet to Points instead of Both */
    private PointsAccumulationAction getBasicPointsAccumulation(String numOfPoints) {
        PointsAccumulationAction pointsAccumulationAction = new PointsAccumulationAction();
        pointsAccumulationAction.clickPointsAccumulationBtn();
        pointsAccumulationAction.setNumOfPoints(numOfPoints);
        return pointsAccumulationAction;
    }

}