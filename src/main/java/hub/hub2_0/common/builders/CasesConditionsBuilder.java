package hub.hub2_0.common.builders;

import hub.hub2_0.common.conditions.AssetCondition;
import hub.hub2_0.common.conditions.MadeAPurchaseCondition;
import hub.hub2_0.common.conditions.PointsCondition;
import hub.hub2_0.common.conditions.UpdateMembershipCondition;
import hub.hub2_0.common.conditions.cases.*;
import hub.hub2_0.common.conditions.global.membership.MemberFieldCondition;
import hub.hub2_0.common.conditions.global.membership.MemberNonRegistrationFormFields;
import hub.hub2_0.common.enums.SelectedItemsDialogButtons;
import hub.hub2_0.common.items.SelectValue;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.common.items.SpecificValue;


public class CasesConditionsBuilder {


    public static GeneralCondition buildGeneralConditionMadeAPurchase(String itemCodes, MadeAPurchaseCondition.ConditionKey conditionKey, String operator, String value) {
        SelectItems selectItems = new SelectItems();
        selectItems.setItemCodes(itemCodes);
        selectItems.clickButton(SelectedItemsDialogButtons.Apply.toString());
        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(conditionKey)
                .setOperatorKey(operator)
                .setConditionValue(value)
                .setSelectItems(selectItems);

        GeneralCondition generalCondition = new CaseMadeAPurchaseCondition();
        generalCondition.setGeneralCondition(madeAPurchaseCondition);
        return generalCondition;
    }

    public static GeneralCondition buildGeneralConditionMadeAPurchaseSimple(MadeAPurchaseCondition.ConditionKey conditionKey, String operator, String value) {

        MadeAPurchaseCondition madeAPurchaseCondition = new MadeAPurchaseCondition();
        madeAPurchaseCondition.setConditionKey(conditionKey)
                .setOperatorKey(operator)
                .setConditionValue(value);

        GeneralCondition generalCondition = new CaseMadeAPurchaseCondition();
        generalCondition.setGeneralCondition(madeAPurchaseCondition);
        return generalCondition;
    }
    public static GeneralCondition buildGeneralConditionPoints(PointsCondition.ConditionKey conditionKey, String operator, String value) {
        PointsCondition pointsConditionForCase = new PointsCondition();
        pointsConditionForCase.setConditionKey(conditionKey).
                setOperatorKey(operator)
                .setConditionValue(value);

        GeneralCondition generalCondition = new CasePointsCondition();
        generalCondition.setGeneralCondition(pointsConditionForCase);
        return generalCondition;
    }

    public static MembersCondition buildMembersCondition(MemberNonRegistrationFormFields conditionKey, String operator, String value) {
        MemberFieldCondition memberFieldConditionCase = MemberFieldCondition.newCondition().
                setFieldsSelect(conditionKey.toString()).setOperatorKey(operator).setConditionValue(value);

        MembersCondition membersCondition = new MembersCondition();
        membersCondition.setMembersCondition(memberFieldConditionCase);
        return membersCondition;
    }

    public static GeneralCondition buildGeneralConditionUpdateMember(UpdateMembershipCondition.ConditionKey conditionKey, String operator,String value) {
        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();
        updateMembershipCondition.setConditionKey(conditionKey)
                .setOperatorKey(operator)
                .setConditionValue(value);

        GeneralCondition generalCondition = new CaseUpdateMembershipCondition();
        generalCondition.setGeneralCondition(updateMembershipCondition);
        return generalCondition;
    }

    public static GeneralCondition buildGeneralConditionUpdateMember(UpdateMembershipCondition.ConditionKey conditionKey, SpecificValue from,SpecificValue to) {
        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();
        updateMembershipCondition.setConditionKey(conditionKey)
                .setSelectValueFrom((new SelectValue()).setSpecificValue(from))
                .setSelectValueTo((new SelectValue()).setSpecificValue(to));

        GeneralCondition generalCondition = new CaseUpdateMembershipCondition();
        generalCondition.setGeneralCondition(updateMembershipCondition);
        return generalCondition;
    }

    public static GeneralCondition buildGeneralConditionUpdateMember(UpdateMembershipCondition.ConditionKey conditionKey, SelectValue from, SelectValue to) {
        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();
        updateMembershipCondition.setConditionKey(conditionKey)
                .setSelectValueFrom(from)
                .setSelectValueTo(to);

        GeneralCondition generalCondition = new CaseUpdateMembershipCondition();
        generalCondition.setGeneralCondition(updateMembershipCondition);
        return generalCondition;
    }

    public static GeneralCondition buildGeneralConditionReceivedAnAsset(AssetCondition.ConditionKey conditionKey, String operator, String value) {
        AssetCondition assetCondition = new AssetCondition();
        assetCondition.setConditionKey(conditionKey)
                .setOperatorKey(operator)
                .setConditionValue(value);

        GeneralCondition generalCondition = new CaseAssetCondition();
        generalCondition.setGeneralCondition(assetCondition);
        return generalCondition;
    }

}
