package hub.hub2_0.common.builders;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.hub2_0.basePages.loyalty.Activity;
import hub.hub2_0.basePages.loyalty.Box;
import hub.hub2_0.basePages.loyalty.Deal;
import hub.hub2_0.basePages.loyalty.OccurrencesCondition;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.basePages.loyalty.cases.DealCase;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.enums.ActivityActions;

import java.util.ArrayList;
import java.util.List;

public class DealBuilder extends LoyaltyBuilder {

    ////**** Build simple deals ****////

    public Activity buildEntireTicketDiscountActivity(Box campaign, Deal deal, List<ActivityActions> activityActionList, String freeTxt) throws Exception {
        return buildEntireTicketDiscountActivity(campaign, deal, Lists.newArrayList(), activityActionList, freeTxt);
    }

    public Activity buildEntireTicketDiscountActivity(Box campaign, Deal deal, List<ApplyCondition> conditions, List<ActivityActions> activityActionList, String freeTxt) throws Exception {
        List<ActivityAction> actions = new ArrayList<>();
        for (ActivityActions activityActions : activityActionList) {
            actions.add(ActionsBuilder.buildAction(activityActions.toString(), freeTxt));
        }
        Activity currentActivity = super.buildActivity(campaign, deal, conditions, actions);
        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    public Activity buildEntireTicketDiscountActivity(Box campaign, Deal deal, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        Activity currentActivity = super.buildActivity(campaign, deal, conditions, activityActionList);
        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    public Activity buildEntireTicketDiscountDealEditMode(Box campaign, Deal deal, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        Activity currentActivity = super.buildActivity(campaign, deal, conditions, activityActionList);
        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    public Activity buildSpecificItemsDiscountDeal(Box campaign, Deal deal, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        deal.clickSpecificItemsDiscount();
        Activity currentActivity = super.buildActivity(campaign, deal, conditions, activityActionList);
        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    public Activity buildSpecificItemsDiscountDealEditMode(Box campaign, Deal deal, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        deal.setSpecificItemsDiscountEditMode();
        Activity currentActivity = super.buildActivity(campaign, deal, conditions, activityActionList);
        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    public Activity buildAdvancedDiscountDeal(Box campaign, Deal deal, List<ApplyCondition> conditions, List<OccurrencesCondition> occurrencesConditionList, List<ActivityAction> activityActionList) throws Exception {
        deal.clickAdvancedDiscount();
        Activity currentActivity = super.buildActivityPerOccurrencesMode(deal, conditions, occurrencesConditionList, activityActionList);
        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    public Activity buildAdvancedDiscountDealEDitMode(Box campaign, Deal deal, List<ApplyCondition> conditions, List<OccurrencesCondition> occurrencesConditionList, List<ActivityAction> activityActionList) throws Exception {
        deal.setAdvancedDiscountEditMode();
        Activity currentActivity = super.buildActivityPerOccurrencesMode(deal, conditions, occurrencesConditionList, activityActionList);
        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    ////**** Build deal with cases ****////

    public Activity buildMultipleCasesDeal(Box campaign, Deal deal, List<ApplyCondition> conditions, List<Case> cases) throws Exception {
        Activity currentActivity = buildActivityWithCases(deal, conditions, cases);
        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    public Activity buildActivityWithCases(Box campaign, Deal deal, List<ApplyCondition> conditions, List<Case> cases, boolean editMode) throws Exception {
        Activity currentActivity = null;
        if (editMode) {
            currentActivity = buildDealWithCasesForEdit(deal, conditions, cases);
        } else {
            currentActivity = buildActivityWithCases(deal, conditions, cases);
        }

        campaign.addActivity(currentActivity);
        return currentActivity;
    }

    public Activity buildDealWithCasesForEdit(Deal deal, List<ApplyCondition> conditions, List<Case> cases) throws Exception {
        setActivityConditions(deal, conditions);
        for (Case aCase : cases) {
            deal.addCase(aCase);
        }
        log4j.info("Update Deal.");
        return deal;
    }

    protected Activity buildPerOccurrencesMode(Activity activity, List<OccurrencesCondition> occurrencesCondition, List<ActivityAction> actions) {
        ((Deal) activity).clickAdvancedDiscount();
        activity.setOccurrencesConditions(occurrencesCondition);
        activity.setActions(actions);
        return activity;
    }

    protected Case buildCaseSimpleMode(List<ActivityAction> actions) throws Exception {
        Case caze = new DealCase();
        caze.setActions(actions);
        return caze;
    }
}
