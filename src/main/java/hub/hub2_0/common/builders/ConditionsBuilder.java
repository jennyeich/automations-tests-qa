package hub.hub2_0.common.builders;

import hub.hub2_0.common.conditions.UpdateMembershipCondition;
import hub.hub2_0.common.items.SelectValue;
import hub.hub2_0.common.items.SpecificValue;

public class ConditionsBuilder {

    public static SpecificValue buildSpecificValue(String inputOperators,String value) {
        SpecificValue specificValue = new SpecificValue();
        specificValue.setOperatorKey(inputOperators).setConditionValueInput(value);
        return specificValue;
    }


    public static SpecificValue buildBooleanSpecificValue(String value) {

        SpecificValue specificValue = new SpecificValue();
        specificValue.setConditionValueCheck(value);
        return specificValue;
    }

    public static UpdateMembershipCondition buildUpdateMemberCondition(UpdateMembershipCondition.ConditionKey conditionKey, String operator, String value) {
        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();
        updateMembershipCondition.setConditionKey(conditionKey)
                .setOperatorKey(operator)
                .setConditionValue(value);

        return updateMembershipCondition;
    }

    public static UpdateMembershipCondition buildUpdateMemberCondition(UpdateMembershipCondition.ConditionKey conditionKey, SelectValue from, SelectValue to) {
        UpdateMembershipCondition updateMembershipCondition = new UpdateMembershipCondition();
        updateMembershipCondition.setConditionKey(conditionKey)
                .setSelectValueFrom(from)
                .setSelectValueTo(to);

        return updateMembershipCondition;
    }
}
