package hub.hub2_0.common.builders;

import hub.hub2_0.basePages.loyalty.*;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.basePages.loyalty.gifts.GiftDisplay;
import hub.hub2_0.basePages.loyalty.gifts.Gift;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.conditions.ApplyCondition;

import java.util.List;



public class GiftsBuilder extends DealBuilder {

    public static GiftDisplay newDisplay() {
        return new GiftDisplay();
    }


    public Gift buildEntireTicketDiscountActivityGift(String name, String timestamp, List<ApplyCondition> conditions, List<ActivityAction> activityActionList,GiftDisplay giftDisplay) throws Exception {
        Gift gift = new Gift();
        gift.setActivityName(name + timestamp);
        gift.setActivityDescription(DEFAULT_GIFT_DESCRIPTION + timestamp);
        log4j.info("building new gift");
        log4j.info("Gift name: " + gift.getActivityName());
        Gift currentGift = (Gift) buildActivityGift(gift, conditions, activityActionList, giftDisplay);
        return currentGift;
    }

    public Gift buildEntireTicketDiscountActivityGift(String name, GiftDisplay giftDisplay, String tiemstamp, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        Gift currentGift = buildEntireTicketDiscountActivityGift(name,tiemstamp,conditions,activityActionList, giftDisplay);
        currentGift.setGiftDisplay(giftDisplay);
        return currentGift;
    }


    public Gift buildSpecificItemsDiscountGift(String name, String timestamp, List<ApplyCondition> conditions, List<ActivityAction> activityActionList, GiftDisplay giftDisplay) throws Exception {
        Gift gift = new Gift();
        gift.setActivityName(name + timestamp);
        gift.setActivityDescription(DEFAULT_GIFT_DESCRIPTION + timestamp);
        gift.clickSpecificItemsDiscount();
        log4j.info("building new gift with specific items discount");
        log4j.info("Gift name: "+gift.getActivityName());
        Gift currentGift = (Gift)buildActivityGift(gift, conditions, activityActionList, giftDisplay);
        return currentGift;
    }


    public Gift buildAdvancedDiscountGift(String name,String timestamp, List<ApplyCondition> conditions, List<OccurrencesCondition> occurrencesConditionList, List<ActivityAction> activityActionList, GiftDisplay giftDisplay) throws Exception {
        Gift gift = new Gift();
        gift.setActivityName(name + timestamp);
        gift.setActivityDescription(DEFAULT_GIFT_DESCRIPTION + timestamp);
        gift.clickAdvancedDiscount();
        log4j.info("building new gift with advanced discount");
        log4j.info("Gift name: "+gift.getActivityName());
        Gift currentGift = (Gift)buildActivityPerOccurrencesModeGift(gift, conditions, occurrencesConditionList, activityActionList, giftDisplay);

        return currentGift;
    }

    public Gift buildMultipleCasesGift(String name, String timestamp, List<ApplyCondition> conditions, List<Case> cases, GiftDisplay giftDisplay) throws Exception {
        Gift gift = new Gift();
        gift.setActivityName(name + timestamp);
        gift.setActivityDescription(DEFAULT_GIFT_DESCRIPTION + timestamp);
        log4j.info("building new gift with cases");
        Gift currentGift = (Gift)buildGiftWithCases(gift, conditions, cases, giftDisplay);

        return currentGift;
    }

}
