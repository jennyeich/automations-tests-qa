package hub.hub2_0.common.builders;

import hub.hub2_0.common.actions.*;
import hub.hub2_0.common.enums.ActivityActions;
import hub.hub2_0.common.enums.ScreenApps;
import org.apache.log4j.Logger;

public class ActionsBuilder {

    public final static Logger log4j = Logger.getLogger(ActionsBuilder.class);

    public static final String TAG = "tag_";
    public static final String EXPORT_EVENT_NAME = "exportEvent_";
    public static final String EXPORT_EVENT_URL = "https://www.como.com";



    public static ActivityAction buildPunchAPunchCardAction(String benefit) throws Exception{
        return  buildPunchAPunchCardAction(benefit, "1");
    }

    public static ActivityAction buildPunchAPunchCardAction(String benefit, String numOfPunches) throws Exception{
        PunchAPunchCardAction punchAPunchCardAction = new PunchAPunchCardAction();
        punchAPunchCardAction.clickPunchAPunchCardBtn();
        punchAPunchCardAction.setSelectBenefit(benefit);
        punchAPunchCardAction.setNumOfPunches(numOfPunches);
        return punchAPunchCardAction;
    }

    public static ActivityAction buildPunchAPunchCardAction(String benefit, String numOfPunches, String actionLimit) throws Exception{
        PunchAPunchCardAction punchAPunchCardAction = (PunchAPunchCardAction) buildPunchAPunchCardAction(benefit, numOfPunches);
        punchAPunchCardAction.setActionLimit(actionLimit);
        return punchAPunchCardAction;
    }

    public static ActivityAction buildPunchExceededAction(String benefit) throws Exception{
        return  buildPunchExceededAction(benefit, "1");
    }

    public static ActivityAction buildPunchExceededAction(String benefit, String numOfPunches) throws Exception{
        PunchExceededAction punchExceededAction = new PunchExceededAction();
        punchExceededAction.clickPunchAPunchCardBtn();
        punchExceededAction.setSelectBenefit(benefit);
        return punchExceededAction;
    }


    public static ActivityAction buildRegisterAction() throws Exception{
        RegisterAction registerAction = new RegisterAction();
        registerAction.clickRegisterBtn();
        return registerAction;
    }

    public static ActivityAction buildSendTextMessageAction(String message) throws Exception{
        SendTextMessageAction sendTextMessageAction = new SendTextMessageAction();
        sendTextMessageAction.clickSendTextMessageBtn();
        sendTextMessageAction.setMessage(message);
        return sendTextMessageAction;
    }

    public static ActivityAction buildSendPushAction(String message) throws Exception{
        SendPushAction sendPushAction = new SendPushAction();
        sendPushAction.clickSendPushBtn();
        sendPushAction.setMessage(message);
        sendPushAction.setOpenScreenAt(ScreenApps.InfoUpdatesTC.name());
        sendPushAction.setButtonText("OK");
        return sendPushAction;
    }

//    public static ActivityAction buildSendLotteryAction(String benefit) throws Exception {
//        return buildSendLotteryAction(benefit, "1");
//    }

//    public static ActivityAction buildSendLotteryAction(String benefit, String quantity) throws Exception{
//        SendLotteryAction sendLotteryAction = new SendLotteryAction();
//        sendLotteryAction.clickSendLotteryBtn();
//        sendLotteryAction.setSelectLottery(benefit);
//        sendLotteryAction.setQuantity(quantity);
//        return sendLotteryAction;
//    }
//
//    public static ActivityAction buildSendLotteryAction(String benefit, String quantity, String actionLimit) throws Exception{
//        SendLotteryAction sendLotteryAction = (SendLotteryAction) buildSendLotteryAction(benefit, quantity);
//        sendLotteryAction.setActionLimit(actionLimit);
//        return sendLotteryAction;
//    }

    public static ActivityAction buildSendEmailAction(String template) throws Exception {
        return buildSendEmailAction(template, "Test email subject","Test email from");
    }

    public static ActivityAction buildSendEmailAction(String template, String subject,String from) throws Exception{
        SendEmailAction sendEmailAction = new SendEmailAction();
        sendEmailAction.clickSendEmailBtnBtn();
        sendEmailAction.setTemplateSelect(template);
        //sendEmailAction.setSubject(subject);
        //sendEmailAction.setFrom(from);
        return sendEmailAction;
    }


    public static ActivityAction buildSendBenefitAction(String benefit) throws Exception{
        return buildSendBenefitAction(benefit, "1");
    }

    public static ActivityAction buildSendBenefitAction(String benefit, String quantity) throws Exception{
        SendBenefitAction sendBenefitAction = new SendBenefitAction();
        sendBenefitAction.clickSendBenefitBtn();
        if(!benefit.isEmpty()) {
            sendBenefitAction.setSelectBenefit(benefit);
            sendBenefitAction.setQuantity(quantity);
        }
        return sendBenefitAction;
    }

    public static ActivityAction buildSendBenefitAction(String benefit, String quantity, String actionLimit) throws Exception{
        SendBenefitAction sendBenefitAction = (SendBenefitAction) buildSendBenefitAction(benefit, quantity);
        sendBenefitAction.setActionLimit(actionLimit);
        return sendBenefitAction;
    }

    public static ActivityAction buildExportEventAction(String name, String url) {
        ExportEventAction exportEventAction = new ExportEventAction();
        exportEventAction.clickExportEventButton();
        exportEventAction.setName(name);
        exportEventAction.setUrl(url);
        return exportEventAction;
    }

    public static ActivityAction buildUpdateMembershipAction(String numOfYears) throws Exception{
        UpdateMembershipAction updateMembershipAction = new UpdateMembershipAction();
        updateMembershipAction.clickUpdateMembershipBtn();
        updateMembershipAction.setNumber(numOfYears);
        return updateMembershipAction;
    }

    public static ActivityAction buildTagMemberAction(String tag) throws Exception{
        TagUntagMemberAction tagUntagMember = new TagUntagMemberAction();
        tagUntagMember.clickTagBtn();
        tagUntagMember.setTagText(tag);
        return tagUntagMember;
    }

    public static ActivityAction buildUntagMemberAction(String tag) throws Exception{
        TagUntagMemberAction tagUntagMember = new TagUntagMemberAction();
        tagUntagMember.clickUnTagBtn();
        tagUntagMember.setTagText(tag);
        return tagUntagMember;
    }

    public static ActivityAction buildPointsAccumulationAction(String numOfPoints) {
        PointsAccumulationAction pointsAccumulationAction = new PointsAccumulationAction();
        pointsAccumulationAction.clickPointsAccumulationBtn();
        pointsAccumulationAction.setNumOfPoints(numOfPoints);
        return pointsAccumulationAction;
    }

    public static ActivityAction buildAction(String actionName, String freeTxt) throws Exception {
        ActivityAction activityAction = null;
        // set the tag/untag fields
        if (ActivityActions.Tag.toString().equals(actionName)) {

            activityAction = buildTagMemberAction(freeTxt);

        } else if (ActivityActions.UnTag.toString().equals(actionName)) {

            activityAction = buildUntagMemberAction(freeTxt);

        } else if (ActivityActions.SendBenefit.toString().equals(actionName)) {

            activityAction = buildSendBenefitAction(freeTxt);

        } else if(ActivityActions.ExportEvent.toString().equals(actionName)) {

            activityAction = buildExportEventAction(EXPORT_EVENT_NAME + freeTxt, EXPORT_EVENT_URL);

        } else if(ActivityActions.SendAPushNotification.toString().equals(actionName)) {

            activityAction = buildSendPushAction(freeTxt);

        } else if(ActivityActions.SendTextMessage.toString().equals(actionName)) {

            activityAction = buildSendTextMessageAction(freeTxt);

        } else if(ActivityActions.Register.toString().equals(actionName)) {

            activityAction = buildRegisterAction();

        } else if(ActivityActions.PunchThePunchCard.toString().equals(actionName)) {

            activityAction = buildPunchAPunchCardAction(freeTxt);

        } else if(ActivityActions.PunchExceeded.toString().equals(actionName)){

            activityAction = buildPunchExceededAction(freeTxt);

        }else if(ActivityActions.UpdateMembership.toString().equals(actionName)) {

            activityAction = buildUpdateMembershipAction(freeTxt);

        } else if(ActivityActions.SendEmail.toString().equals(actionName)) {

            activityAction = buildSendEmailAction(freeTxt);

        } else if(ActivityActions.PointsAccumulation.toString().equals(actionName)) {

            activityAction = buildPointsAccumulationAction(freeTxt);
        }

        return activityAction;
    }


}
