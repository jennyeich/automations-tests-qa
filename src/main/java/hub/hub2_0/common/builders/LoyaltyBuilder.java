package hub.hub2_0.common.builders;

import hub.hub2_0.basePages.loyalty.*;
import hub.hub2_0.basePages.loyalty.cases.Case;
import hub.hub2_0.basePages.loyalty.gifts.GiftDisplay;
import hub.hub2_0.common.actions.ActivityAction;
import hub.hub2_0.common.conditions.ActivityCondition;
import hub.hub2_0.common.conditions.ApplyCondition;
import hub.hub2_0.common.enums.ActivityStatus;
import hub.hub2_0.common.enums.ActivityType;
import hub.hub2_0.common.items.SelectItems;
import hub.hub2_0.services.loyalty.LoyaltyService;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public abstract class LoyaltyBuilder {

    public final static Logger log4j = Logger.getLogger(LoyaltyBuilder.class);
    protected static final String DEFAULT_RULE_DESCRIPTION = "Description from Rule ";
    protected static final String DEFAULT_DEAL_DESCRIPTION = "Description from Deal ";
    protected static final String DEFAULT_GIFT_DESCRIPTION = "Description from Gift ";

    protected abstract Activity buildPerOccurrencesMode(Activity activity, List<OccurrencesCondition> occurrencesCondition, List<ActivityAction> actions);
    protected abstract Case buildCaseSimpleMode(List<ActivityAction> actions)throws Exception;

    public Box buildBox(String boxName) throws Exception{
        int numOfBoxes = LoyaltyService.getNumOfExistsBoxes();
        Box box = new Box();
        box.setBoxName(boxName); //Campiagn name e.g:"VIPS" ,"HOLIDAY_DEAL"
        box.setNext(0); // By default the new box added to the head of the list. if <LAST CREATE> sorting order is changed than it will be added to the end of the list
        log4j.info("New Box is being created :  "+ boxName + " currently there are:"  + (numOfBoxes + 1 ) +" Boxes. " + numOfBoxes +" Appear in UI and 1 Under creation.");
        return box;
    }

    public Activity getConcreteActivity(Box box, ActivityType activityType, ActivityStatus activityStatus, String name) throws Exception {
        String timestamp = String.valueOf(System.currentTimeMillis());
        if (activityType.equals(ActivityType.Rule)) {
            Rule rule = new Rule(activityStatus);
            rule.setActivityName(name + timestamp);
            rule.setActivityDescription(DEFAULT_RULE_DESCRIPTION +  timestamp);
            rule.setNext(box.getNext());
            log4j.info("building new rule Activity");
            return rule;
        } else if(activityType.equals(ActivityType.Deal)) {
            Deal deal = new Deal(activityStatus);
            deal.setActivityName(name + timestamp);
            deal.setActivityDescription(DEFAULT_DEAL_DESCRIPTION +  timestamp);
            deal.setNext(box.getNext());
            log4j.info("building new deal Activity");
            return deal;
        }
        else {
            return null;
        }
    }

    public Activity getConcreteActivity(Box box, ActivityType activityType, String name, String description) throws Exception {
        String timestamp = String.valueOf(System.currentTimeMillis());
        if(activityType.equals(ActivityType.Rule)) {
            Rule rule = new Rule();
            if( name!= null) {
                rule.setActivityName(name + timestamp);
            }
            if(description != null) {
                rule.setActivityDescription(description);
            }
            rule.setNext(box.getNext());
            log4j.info("building new rule Activity");
            return rule;
        } else if(activityType.equals(ActivityType.Deal)) {
            Deal deal = new Deal();
            if( name!= null) {
                deal.setActivityName(name + timestamp);
            }
            if(description != null) {
                deal.setActivityDescription(description);
            }
            deal.setNext(box.getNext());
            log4j.info("building new deal Activity");
            return deal;
        } else {
            return null;
        }
    }

    public Box buildClickAddActivity(ActivityType activityType)throws Exception {
        Box box = new Box();
        box.clickAddActivity();
        if(ActivityType.Rule.equals(activityType)) {
            box.clickRuleActivity();
        }else if(ActivityType.Deal.equals(activityType)) {
            box.clickDealActivity();
        }
        box.setNext(0);
        return box;
    }



    protected Activity buildActivity(Box campaign, Activity activity, List<ApplyCondition> conditions, List<ActivityAction> activityActionList) throws Exception {
        setActivityConditions(activity, conditions);
        activity.setActions(activityActionList);
        log4j.info("Total Actions in activity (BOX:" + campaign.getBoxName() + ")-->" + activity.getActions().size());
        return activity; // Activity can be Rule/Deal
    }

    public Activity buildActivityGift(Activity activity, List<ApplyCondition> conditions, List<ActivityAction> activityActionList, GiftDisplay giftDisplay) throws Exception {
        setActivityConditions(activity, conditions);
        activity.setActions(activityActionList);
        activity.setGiftDisplay(giftDisplay);

        return activity; // Activity can be Rule/Deal
    }

    protected Activity buildActivityWithCases(Activity activity, List<ApplyCondition> conditions, List<Case> cases) throws Exception {
        setActivityConditions(activity, conditions);
        activity.clickSplitCases();
        activity.setCases(cases);

        return activity;
    }

    protected Activity buildGiftWithCases(Activity activity, List<ApplyCondition> conditions, List<Case> cases, GiftDisplay giftDisplay) throws Exception {
        setActivityConditions(activity, conditions);
        activity.clickSplitCases();
        activity.setCases(cases);
        activity.setGiftDisplay(giftDisplay);

        return activity;
    }

    protected Activity buildActivityPerOccurrencesMode(Activity activity, List<ApplyCondition> conditions, List<OccurrencesCondition> occurrencesConditionList, List<ActivityAction> activityActionList) throws Exception {
        setActivityConditions(activity, conditions);
        activity = buildPerOccurrencesMode(activity,occurrencesConditionList, activityActionList);
        return activity; // Activity can be Rule/Deal
    }

    protected Activity buildActivityPerOccurrencesModeGift(Activity activity, List<ApplyCondition> conditions, List<OccurrencesCondition> occurrencesConditionList, List<ActivityAction> activityActionList, GiftDisplay giftDisplay) throws Exception {
        setActivityConditions(activity, conditions);
        activity = buildPerOccurrencesMode(activity,occurrencesConditionList, activityActionList);
        activity.setGiftDisplay(giftDisplay);

        return activity;
    }


    protected void setActivityConditions(Activity activity, List<ApplyCondition> conditions) {
        List<GlobalCondition> globalConditions = new ArrayList<>();
        List<ActivityCondition> activityConditions = new ArrayList<>();
        for (ApplyCondition applyCondition : conditions) {
            if(applyCondition instanceof GlobalCondition){
                globalConditions.add((GlobalCondition) applyCondition);
            }else if(applyCondition instanceof ActivityCondition){
                activityConditions.add((ActivityCondition)applyCondition);
            }
        }
        activity.setGlobalConditions(globalConditions);
        activity.setConditions(activityConditions);
    }

    public static SelectItems selectItemsByItemCode(String... itemCodes) {
        SelectItems selectItems = new SelectItems();
        selectItems.clickItemCodesRadioButton();
        selectItems.setItemCodes(itemCodes);
        selectItems.clickApply();
        return selectItems;
    }

    public static SelectItems selectItemsByDepartmentCode(String... departmentCodes) {
        SelectItems selectItems = new SelectItems();
        selectItems.clickDepartmentCodesRadioButton();
        selectItems.setDepartmentCodes(departmentCodes);
        selectItems.clickApply();
        return selectItems;
    }

}
