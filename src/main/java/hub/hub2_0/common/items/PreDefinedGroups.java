package hub.hub2_0.common.items;

import hub.base.BaseSeleniumObject;
import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class PreDefinedGroups extends BaseSeleniumObjectHub2 {

    private static final String SEARCH_GROUP= "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.itemsGroup.searchBox\")]/input";
    private static final String GROUP_CHECK = "//div[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"checkbox\")]";


    @SeleniumPath(value = SEARCH_GROUP, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_HUB2, readValue = SEARCH_GROUP)
    private String search;
    @SeleniumPath(value = GROUP_CHECK, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CHECKBOX, readValue = GROUP_CHECK)
    private String groupSelected;

    private int parentIndex = 0;

    public PreDefinedGroups(String groupName) {
        this.search = groupName;
        this.groupSelected = groupName;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getGroupSelected() {
        return groupSelected;
    }

    public void setGroupSelected(String groupSelected) {
        this.groupSelected = groupSelected;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        this.parentIndex = i;
    }

    @Override
    public int getNext() {
        return parentIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public int getParentIndex() {
        return parentIndex;
    }
}
