package hub.hub2_0.common.items;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class BundleLine extends BaseSeleniumObjectHub2 {

    private static final String LINE_CHECKBOX = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"applyOnBundleLines.{0}.checkbox\")]";
    private static final String LINE_DROPDOWN = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"applyOnBundleLines.{0}.quantity.{1}\")]";

    @SeleniumPath(value = LINE_CHECKBOX, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = LINE_CHECKBOX)
    private String lineCheckbox;
    @SeleniumPath(value = LINE_DROPDOWN, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2, readValue = LINE_DROPDOWN)
    private String lineQuantity;

    int index = 0;

    public String getLineCheckboxState() {
        return lineCheckbox;
    }

    public void clickLineCheckbox() {
        this.lineCheckbox = "Clicked line checkbox";
    }

    public String getLineQuantity() {
        return lineQuantity;
    }

    public void selectLineQuantity(String lineQuantity) {
        this.lineQuantity = lineQuantity;
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        this.index = i;
    }

    @Override
    public int getNext() {
        return index;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
