package hub.hub2_0.common.items;

import hub.base.BaseSeleniumObject;
import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.ConditionRelations;

import java.util.ArrayList;
import java.util.List;

public class AdvancedRule extends BaseSeleniumObjectHub2 {

    public static final String ADVANCED_RULES = "advancedRules";

    private final static String CONDITION_RELATIONS_CLICK = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.advancedRules.conditionRelations\")]";
    private final static String CONDITION_RELATIONS_AND_OR = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.advancedRules.conditionRelations.{0}\")]";
    private final static String SAVE_AS_GROUP = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.saveAsGroup\")]";
    private final static String CLEAR = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.advancedRules.clearAll\")]";

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<AdvancedRuleCondition> advancedRuleConditions;
    @SeleniumPath(value = CONDITION_RELATIONS_CLICK, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK )
    private String conditionRelations;
    @SeleniumPath(value = CONDITION_RELATIONS_AND_OR, type = SeleniumPath.Type.BY_DYNAMIC_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2 )
    private String conditionRelationsAndOr;

    @SeleniumPath(value = SAVE_AS_GROUP, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String saveAsGroupBtn;
    @SeleniumPath(value = CLEAR, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String clearBtn;

    NewGroup newGroup;

    private int conditionsCounter = 0;
    private int parentIndex = 0;


    private AdvancedRule addCondition(AdvancedRuleCondition condition) {
        condition.setNext(conditionsCounter);
        getAdvancedRuleConditions().add(condition);
        conditionsCounter++;
        return this;
    }

    public List<AdvancedRuleCondition> getAdvancedRuleConditions() {
        if(advancedRuleConditions == null)
            advancedRuleConditions = new ArrayList<>();
        return advancedRuleConditions;
    }
    public AdvancedRule setAdvancedRuleConditions(AdvancedRuleCondition... advancedRuleConditions) {
        for(AdvancedRuleCondition condition : advancedRuleConditions) {
            if(conditionsCounter > 0)
                condition.clickAdd();
            addCondition(condition);
        }
        return this;

    }

    public String getConditionRelations() {
        return conditionRelations;
    }

    public AdvancedRule setConditionRelations(String conditionRelations) {
        this.conditionRelations = conditionRelations;
        this.conditionRelationsAndOr = conditionRelations;
        return this;
    }

    public String getSaveAsGroupBtn() {
        return saveAsGroupBtn;
    }

    public void clickSaveAsGroupBtn(String groupName,String finishAction) {
        this.saveAsGroupBtn = ADVANCED_RULES;
        newGroup = new NewGroup();
        newGroup.setName(groupName);
        newGroup.perform(finishAction);
    }

    public String getClearBtn() {
        return clearBtn;
    }

    public void clickClearBtn() {
        this.clearBtn = ADVANCED_RULES;
        advancedRuleConditions = null;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

        if(advancedRuleConditions != null) {
            for (AdvancedRuleCondition ruleCondition : advancedRuleConditions) {
                ruleCondition.setParentIndex(i);
            }
        }
    }

    @Override
    public void setParentIndex(int i) {
        parentIndex = i;
        if(newGroup !=null) {
            newGroup.setNext(i);
            newGroup.setParentIndex(i);
        }
    }

    @Override
    public int getParentIndex() {
        return parentIndex;
    }

    @Override
    public int getNext() {
        return parentIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public static AdvancedRule newRule() {
        return  new AdvancedRule();
    }
}
