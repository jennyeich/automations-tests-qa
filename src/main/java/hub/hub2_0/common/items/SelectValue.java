package hub.hub2_0.common.items;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class SelectValue extends BaseSeleniumObjectHub2{



    private static final String ANY_VALUE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"updateMembershipPopup.anyValue\"]";
    public static final String EMPTY_VALUE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"updateMembershipPopup.noValue\"]";
    public static final String SPECIFIC_VALUE = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"updateMembershipPopup.specificValue\"]";

    public static final String CANCEL = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "='updateMembershipPopup.cancel']";
    public static final String APPLY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "='updateMembershipPopup.apply']";

    @SeleniumPath(value = ANY_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String anyValueRadioButton;

    @SeleniumPath(value = EMPTY_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String emptyValueRadioButton;

    @SeleniumPath(value = SPECIFIC_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String specificValueRadioButton;
    private SpecificValue specificValue;

    @SeleniumPath(value = APPLY, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String applyBtn;
    @SeleniumPath(value = CANCEL, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String cancelBtn;

    public static SelectValue SELECT_VALUE_ANY_VALUE(){
        return (new SelectValue()).anyValue();
    }
    public static SelectValue SELECT_VALUE_EMPTY_VALUE(){
        return (new SelectValue()).emptyValue();
    }
    public static SelectValue SELECT_VALUE_SPECIFIC_VALUE(SpecificValue specificValue){
        return (new SelectValue()).setSpecificValue(specificValue);
    }



    public SelectValue anyValue() {
        this.anyValueRadioButton = "Click Any value radio button";
        clickApply();
        return this;
    }

    public SelectValue emptyValue() {
        this.emptyValueRadioButton = "Click Empty value radio button";
        clickApply();
        return this;
    }
    public SelectValue clickSpecificValueRadioButton() {
        this.specificValueRadioButton = "Click Specific value radio button";
        return this;
    }
    public SelectValue clickApply() {
        this.applyBtn = "click apply";
        return this;
    }

    public SelectValue clickCancel() {
        this.cancelBtn = "click cancel";
        return this;
    }

    public SelectValue setSpecificValue(SpecificValue specificValue) {
        clickSpecificValueRadioButton();
        this.specificValue = specificValue;
        clickApply();
        return this;
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
