package hub.hub2_0.common.items;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;


public class NewGroup extends BaseSeleniumObjectHub2 {

    public static final String SAVE = "save";
    public static final String CANCEL = "cancel";
    private static String items;


    private final static String NAME = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.saveAsGroup.name\")]/input";
    private final static String FINISH_BTN = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.saveAsGroup.{0}\")]";


    @SeleniumPath(value = NAME, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = NAME)
    private String name;
    @SeleniumPath(value = FINISH_BTN, type = SeleniumPath.Type.BY_DYNAMIC_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2)
    private String finishBtn;

    private int parentIndex = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void perform(String whatToPerform){
        this.finishBtn = whatToPerform;
    }

    public String getFinishBtn() {
        return finishBtn;
    }



    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        parentIndex = i;
    }

    @Override
    public int getNext() {
        return parentIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public int getParentIndex() {
        return parentIndex;
    }
}
