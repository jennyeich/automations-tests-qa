package hub.hub2_0.common.items;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;


public class SpecificValue extends BaseSeleniumObjectHub2 {

    private final static String OPERATOR_KEY = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"updateMembershipPopup.specificValue.operatorKey\"]";//to select the field to change
    private final static String OPERATOR_KEY_SELECT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"updateMembershipPopup.specificValue.operatorKey.{0}\"]";//to select the field to change
    private final static String CONDITION_VALUE_INPUT = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"updateMembershipPopup.specificValue.conditionValue\"]/input";
    private final static String CONDITION_VALUE_CHECK = "//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"updateMembershipPopup.specificValue.conditionValue.{0}\"]";//yes or no

    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String operatorKey;
    @SeleniumPath(value = OPERATOR_KEY_SELECT, type = SeleniumPath.Type.BY_DYNAMIC_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_HUB2)
    private String operatorKeySelect;
    @SeleniumPath(value = CONDITION_VALUE_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_CAPSULA_HUB2, readValue = CONDITION_VALUE_INPUT)
    private String conditionValueInput;
    @SeleniumPath(value = CONDITION_VALUE_CHECK, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CHECKBOX_HUB2, readValue = CONDITION_VALUE_INPUT)
    private String conditionValueCheck;

    public String getOperatorKey() {
        return operatorKey;
    }

    public SpecificValue setOperatorKey(String operatorKey) {
        this.operatorKey = "click";
        this.operatorKeySelect = operatorKey;
        return this;
    }

    public String getConditionValueInput() {
        return conditionValueInput;
    }

    public SpecificValue setConditionValueInput(String conditionValueInput) {
        this.conditionValueInput = conditionValueInput;
        return this;
    }

    public String getConditionValueCheck() {
        return conditionValueCheck;
    }

    public SpecificValue setConditionValueCheck(String conditionValueCheck) {
        this.conditionValueCheck = conditionValueCheck;
        return this;
    }

    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
