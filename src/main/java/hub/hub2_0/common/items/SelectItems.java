package hub.hub2_0.common.items;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.enums.SelectedItemsDialogButtons;

public class SelectItems extends BaseSeleniumObjectHub2 implements SelectPopulation {


    private static final String ANY_ITEM = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.any\")]";
    public static final String ITEM_CODES = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.itemCodes\")]";
    public static final String DEPARTMENT_CODES = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.departmentCodes\")]";
    public static final String PREDEFINED_GROUPS = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.itemsGroup\")]";
    public static final String ADVANCED_RULE = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.advancedRules\")]";

    public static final String FINISH_ACTION_BUTTON = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.{0}\")]";

    @SeleniumPath(value = ANY_ITEM, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String anyItemRadioButton;

    @SeleniumPath(value = ITEM_CODES, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String itemCodesRadioButton;
    private CodesGroup itemCodes;

    @SeleniumPath(value = DEPARTMENT_CODES, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String departmentCodesRadioButton;
    private CodesGroup departmentCodes;

    @SeleniumPath(value = PREDEFINED_GROUPS, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String predefinedGroupsRadioButton;
    private PreDefinedGroups preDefinedGroups;

    @SeleniumPath(value = ADVANCED_RULE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String advancedRulesRadioButton;
    private AdvancedRule advancedRule;

    @SeleniumPath(value = FINISH_ACTION_BUTTON, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String finishActionBtn;

    private int selectItemsIndex = 0;


    public void clickAnyItemRadioButton() {
        this.anyItemRadioButton = "Click Any Item radio button";
    }

    public void clickItemCodesRadioButton() {
        this.itemCodesRadioButton = "Click Item Codes radio button";
    }

    public void clickDepartmentCodesRadioButton() {
        this.departmentCodesRadioButton = "Click Departments Codes radio button";
    }

    public void clickPredefinedGroupsRadioButton() {
        this.predefinedGroupsRadioButton = "Click Predefined groups radio button";
    }

    public void clickAdvancedRulesRadioButton() {
        this.advancedRulesRadioButton = "Click Advanced Rules radio button";
    }

    public void setPreDefinedGroup(PreDefinedGroups preDefinedGroups) {
        this.preDefinedGroups = preDefinedGroups;
    }

    public PreDefinedGroups getPreDefinedGroup() {
        return this.preDefinedGroups;
    }

    public void setAdvancedRule(AdvancedRule advancedRule) {
        this.advancedRule = advancedRule;
    }

    public AdvancedRule getAdvancedRule() {
        return this.advancedRule;
    }

    public void setItemCodes(CodesGroup itemCodes) {
        this.itemCodes = itemCodes;
    }

    public void setItemCodes(String... codes) {
        CodesGroup itemCodes = new CodesGroup();
        itemCodes.setCodes(CodesGroup.ITEM_CODES_GROUP, codes);
        setItemCodes(itemCodes);
    }

    private CodesGroup getItemCodes() {
        return this.itemCodes;
    }

    public void setDepartmentCodes(CodesGroup departmentCodes) {
        this.departmentCodes = departmentCodes;
    }

    public void setDepartmentCodes(String... codes) {
        CodesGroup departmentCodes = new CodesGroup();
        departmentCodes.setCodes(CodesGroup.DEPARTMENT_CODES_GROUP, codes);
        setDepartmentCodes(departmentCodes);
    }

    private CodesGroup getDepartmentCodes() {
        return this.departmentCodes;
    }

    public void clickButton(String selectedBtn) {
        this.finishActionBtn = selectedBtn;
    }

    public void clickApply() {
        this.clickButton(SelectedItemsDialogButtons.Apply.toString());
    }

    public void clickCancel() {
        this.clickButton(SelectedItemsDialogButtons.Cancel.toString());
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        this.selectItemsIndex = i;
        if (departmentCodes != null) {
            departmentCodes.setNext(i);
            departmentCodes.setParentIndex(i);
        }
        if (itemCodes != null) {
            itemCodes.setNext(i);
            itemCodes.setParentIndex(i);
        }
        if (advancedRule != null) {
            advancedRule.setNext(i);
            advancedRule.setParentIndex(i);
        }

        if (preDefinedGroups != null) {
            preDefinedGroups.setNext(i);
        }
    }

    @Override
    public int getNext() {
        return this.selectItemsIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public void setParentIndex(int i) {

    }

    @Override
    public int getParentIndex() {
        return 0;
    }
}
