package hub.hub2_0.common.items;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

public class DepartmentCode extends BaseSeleniumObjectHub2 implements Item {

    private static final String DEPARTMENT_CODE_INPUT = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"temsPopulation.departmentCodes.item.{0}\")]/input";

    @SeleniumPath(value = DEPARTMENT_CODE_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_AND_ENTER_HUB2, readValue = DEPARTMENT_CODE_INPUT)
    private String departmentCode;

    private int itemIndex = 0;
    private int parentIndex = 0;

    @Override
    public String getItem() {
        return departmentCode;
    }

    @Override
    public void setItem(String departmentCode) {
        this.departmentCode = departmentCode;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        this.itemIndex = i;
    }

    @Override
    public int getNext() {
        return itemIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public void setParentIndex(int i) {
        parentIndex = i;
    }

    @Override
    public int getParentIndex() {
        return parentIndex;
    }
}
