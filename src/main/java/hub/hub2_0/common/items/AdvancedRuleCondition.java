package hub.hub2_0.common.items;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;
import hub.hub2_0.common.conditions.Monetary;

public class AdvancedRuleCondition extends BaseSeleniumObjectHub2 implements Monetary {

    private static final String ADD_CONDITION = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.advancedRules.addCondition\")]";
    private static final String CONDITION_KEY = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.advancedRules.condition.{0}.conditionKey.{1}\")]";
    public static final String OPERATOR_KEY = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.advancedRules.condition.{0}.conditionsOperator.{1}\")]";
    public static final String CONDITION_VALUE = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.advancedRules.condition.{0}.conditionValue\")]/input";


    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2)
    private String addCondition;

    @SeleniumPath(value = CONDITION_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_INNER_ELEMENT_HUB2)
    private String conditionKey;
    @SeleniumPath(value = OPERATOR_KEY, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_INNER_ELEMENT_HUB2)
    private String operatorKey;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_CAPSULA_HUB2, readValue = CONDITION_VALUE)
    private String conditionValue;


    private int conditionCounter = 0;
    private int parentIndex = 0;

    public void clickAdd() {
        this.addCondition = "Click add advanced rule condition";
    }

    public String getConditionKey() {
        return conditionKey;
    }

    public AdvancedRuleCondition setConditionKey(String conditionKey) {
        this.conditionKey = conditionKey;
        return this;
    }

    public String getOperatorKey() {
        return operatorKey;
    }

    public AdvancedRuleCondition setOperatorKey(String operatorKey) {
        this.operatorKey = operatorKey;
        return this;
    }

    public String getConditionValue() {
        return conditionValue;
    }

    public AdvancedRuleCondition setConditionValue(String conditionValue) {
        this.conditionValue = conditionValue;
        return this;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        conditionCounter = i;
    }

    @Override
    public int getNext() {
        return conditionCounter;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public void setParentIndex(int i) {
        parentIndex = i;
    }

    @Override
    public int getParentIndex() {
        return parentIndex;
    }
}
