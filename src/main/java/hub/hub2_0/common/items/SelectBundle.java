package hub.hub2_0.common.items;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

import java.util.ArrayList;
import java.util.List;

public class SelectBundle extends BaseSeleniumObjectHub2 implements SelectPopulation {

    private static final String ENTIRE_RECURRING_BUNDLE = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"applyOnBundleLines.button.entireBundle\")]";
    private static final String SPECIFIC_ITEMS_FROM_BUNDLE = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"applyOnBundleLines.button.specificItems\")]";
    private static final String APPLY_BUTTON = "//button[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"applyOnBundleLines.apply\")]";

    @SeleniumPath(value = ENTIRE_RECURRING_BUNDLE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String entireRecurringBundleButton;
    @SeleniumPath(value = SPECIFIC_ITEMS_FROM_BUNDLE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String specificItemsFromBundleButton;
    @SeleniumPath(value = "", type = SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    private List<BundleLine> bundleLines;
    @SeleniumPath(value = APPLY_BUTTON, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String applyButton;

    int bundleLinesCounter = 0;


    public void clickEntireRecurringBundleButton() {
        this.entireRecurringBundleButton = "Clicked 'Entire Recurring Bundle Button'";
    }

    public void clickSpecificItemsFromBundleButton() {
        this.specificItemsFromBundleButton = "Clicked Specific Items From Bundle Button";
    }

    public void setBundleLines(List<BundleLine> bundleLines) {
        for (BundleLine bundleLine : bundleLines) {
            addBundleLine(bundleLine);
        }
    }

    public List<BundleLine> getBundleLines() {
        if (bundleLines == null)
            bundleLines = new ArrayList<>();
        return bundleLines;
    }


    public void addBundleLine(BundleLine bundleLine) {
        bundleLine.setNext(bundleLinesCounter);
        getBundleLines().add(bundleLine);
        bundleLinesCounter++;
    }


    public void clickApplyButton() {
        this.applyButton = "Clicked Apply Button";
    }


    @Override
    public int getParentIndex() {
        return 0;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
