package hub.hub2_0.common.items;

import hub.base.BaseSeleniumObjectHub2;
import hub.base.SeleniumPath;
import hub.hub2_0.Hub2Constants;

import java.util.ArrayList;
import java.util.List;

public class CodesGroup extends BaseSeleniumObjectHub2 {

    public static final String DEPARTMENT_CODES_GROUP = "departmentCodes";
    public static final String ITEM_CODES_GROUP = "itemCodes";

    private final static String SAVE_AS_GROUP = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\"itemsPopulation.saveAsGroup\")]";
    private final static String CLEAR = "//*[contains(@" + Hub2Constants.HUB2_AUTOMATION_STR + ",\".clearAll\")]";

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE, elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<Item> itemsList;

    @SeleniumPath(value = SAVE_AS_GROUP, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String saveAsGroupBtn;
    @SeleniumPath(value = CLEAR, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_HUB2_1PARAM)
    private String clearBtn;

    NewGroup newGroup;

    private int itemsIndex = 0;
    private int parentIndex = 0;

    public void setCodes(String groupType,String... codes) {
        if(this.itemsList == null) {
            this.itemsList = new ArrayList<>();
        }
        for(String code: codes) {
            if(groupType.equalsIgnoreCase(ITEM_CODES_GROUP))
                addItemCode(code);
            else if(groupType.equalsIgnoreCase(DEPARTMENT_CODES_GROUP))
                addDepartmentCode(code);
        }
    }

    private void addDepartmentCode(String departmentCodeStr) {
        DepartmentCode departmentCode = new DepartmentCode();
        departmentCode.setItem(departmentCodeStr);
        departmentCode.setNext(itemsIndex);
        this.itemsList.add(departmentCode);
        itemsIndex++;
    }

    private void addItemCode(String itemCodeStr){
        ItemCode itemCode = new ItemCode();
        itemCode.setItem(itemCodeStr);
        itemCode.setNext(itemsIndex);
        this.itemsList.add(itemCode);
        itemsIndex++;
    }

    public String getSaveAsGroupBtn() {
        return saveAsGroupBtn;
    }

    public void clickSaveAsGroupBtn(String groupName,String finishAction) {
        this.saveAsGroupBtn = "Save as group clicked";
        newGroup = new NewGroup();
        newGroup.setName(groupName);
        newGroup.perform(finishAction);
    }

    public String getClearBtn() {
        return clearBtn;
    }

    public void clickClearBtn() {
        itemsList = null;
    }

    @Override
    public void setNext(int i) {

        if(itemsList != null) {
            for (Item item : itemsList) {
                if(item instanceof ItemCode)
                    ((ItemCode) item).setParentIndex(i);
                else if(item instanceof DepartmentCode)
                    ((DepartmentCode) item).setParentIndex(i);
            }
        }
        if(newGroup !=null) {
            newGroup.setNext(i);
            newGroup.setParentIndex(i);
        }

    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }


    @Override
    public void setParentIndex(int i) {
        parentIndex = i;
    }

    @Override
    public int getParentIndex() {
        return parentIndex;
    }

    @Override
    public int getNext() {
        return parentIndex;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
