package hub.hub2_0;

public class Hub2Constants {

    public static final String HUB2_AUTOMATION_STR =  "data-automation-id";

    //Condition keys
    public static final String BENEFIT = "context.Asset.Template";
    public static final String NUMBER_OF_PUNCHES = "context.TotalPunchesAfterOperation";
    public static final String IS_LAST_PUNCH = "context.LastPunch";

    public static final String CREDIT_AMOUNT = "context.Data.Amounts.BudgetWeighted";
    public static final String CREDIT_NEW_BALANCE = "context.Data.NewPointsSet.BudgetWeighted";
    public static final String NUMBER_OF_POINTS = "context.Data.Amounts.Points";
    public static final String POINTS_NEW_BALANACE = "context.Data.NewPointsSet.Points";
    public static final String REFFERED_CODE = "context.Referred";

    //Condition keys
    public static final String TYPE = "context.Data.Type";
    public static final String SUBTYPE = "context.Data.SubType";
    public static final String STRING_VALUE_1 = "context.Data.Data.StringValue1";
    public static final String STRING_VALUE_2 = "context.Data.Data.StringValue2";
    public static final String NUMERIC_VALUE_1 = "context.Data.Data.NumericValue1";
    public static final String NUMERIC_VALUE_2 = "context.Data.Data.NumericValue2";
    public static final String DATE_VALUE_1 = "context.Data.Data.DateValue1";
    public static final String DATE_VALUE_2 = "context.Data.Data.DateValue2";
    public static final String BOOLEAN_VALUE_1 = "context.Data.Data.BooleanValue1";
    public static final String BOOLEAN_VALUE_2 = "context.Data.Data.BooleanValue2";

    public static final String PAYMENT_AMOUNT = "context.ActualCharge";
    public static final String PAYMENT_FLOW = "context.PaymentFlow";
    public static final String ITEM_CODE = "item.ItemCode";

    public static final String FORM_NAME = "context.formId";
    public static final String PRICE_IN_POINTS = "context.SourceDataPriceInPoints";

    public static final String TOTAL_SPEND = "context.Purchase.TotalSum";
    public static final String BRANCH_ID = "context.Purchase.BranchID";
    public static final String POS_ID = "context.Purchase.PosID";
    public static final String NUMBER_OF_MEMBERS = "context.Purchase.NumOfMembers";
    public static final String PURCHASE_TAGS = "context.Purchase.Tags";
    public static final String SHOPPING_CART = "shoppingCart.TotalQuantity";
    public static final String VALUE_OF_SPECIFIC_ITEMS = "shoppingCart.TotalPrice";
    public static final String ORDER_TYPE = "context.Purchase.OrderType";
    public static final String PURCHASE_SOURCE_TYPE = "context.Purchase.TransactionSource";
    public static final String PURCHASE_SOURCE_NAME = "context.Purchase.TransactionSourceName";

    public static final String UUID = "context.Data.Data.uuid";
    public static final String MAJOR = "context.Data.Data.major";
    public static final String MINOR = "context.Data.Data.minor";

    public static final String TAG = "context.Tag";
    public static final String CATALOG_ITEMS = "context.Catalog.Item";

    //condition keys for update membership
    public static final String PHONE_NUMBER = "context.UpdatedFields.PhoneNumber";
    public static final String EMAIL = "context.UpdatedFields.Email";
    public static final String FIRST_NAME = "context.UpdatedFields.FirstName";
    public static final String LAST_NAME = "context.UpdatedFields.LastName";
    public static final String GENDER = "context.UpdatedFields.Gender";
    public static final String MEMBER_ID = "context.UpdatedFields.MemberID";
    public static final String GENERIC_STRING = "context.UpdatedFields.GenericString1";
    public static final String GENERIC_INTEGER = "context.UpdatedFields.GenericInteger1";

    public static final String POINTS = "context.UpdatedFields.Points";
    public static final String BUDGET_WEIGHTED = "context.UpdatedFields.BudgetWeighted";
    public static final String ALLOW_SMS = "context.UpdatedFields.AllowSMS";
    public static final String ALLOW_EMAIL = "context.UpdatedFields.AllowEmail";
    public static final String ALLOW_LOCATION_SERVICES = "context.UpdatedFields.LocationEnabled";
    public static final String ALLOW_PUSH = "context.UpdatedFields.PushNotificationEnabled";
    public static final String APP_USER = "context.UpdatedFields.MobileAppUsed";
    public static final String UPDATE_DURING_REGISTRATION = "context.Operation";
    public static final String JOINING_CODE_BULK_TAG = "context.UpdatedFields.CodeTag[0].NewValue";

    //Condition operator keys
    public static final String IS_GREATER_THAN = ">";
    public static final String IS_LESS_THAN = "<";
    public static final String IS_GREATER_THAN_OR_EQUAL_TO = ">=";
    public static final String IS_LESS_THAN_OR_EQUAL_TO = "<=";
    public static final String IS_EXACTLY = "==";
    public static final String IS_BETWEEN = "numberIsBetween";
    public static final String IS_NOT = "!=";
    public static final String IS_ONE_OF = "containsAtLeastOneValue";
    public static final String IS_NOT_ONE_OF = "doesNotContainAnyOfTheValues";
    public static final String DOES_NOT_CONTAINS = "containsNone";

    public static final String DATE_IS = "dateIsSame";
    public static final String DATE_IS_NOT = "!dateIsSame";
    public static final String DATE_IS_BEFORE = "dateTimeIsBefore";
    public static final String DATE_IS_AFTER = "dateTimeIsAfter";
    public static final String DATE_IS_BETWEEN = "dateTimeIsBetweenIsAfter";

    public static final String IN_THE_QUANTITY_OF = "quantity";
    public static final String IN_THE_TOTAL_SPEND_OF = "spend";

    public static final String IN_APP = "inApp";
    public static final String IN_STORE = "inStore";

    public static final String ACCUMULATE_FIXED_AMOUNT_OF_POINTS = "fixed";
    public static final String ACCUMULATE_PERCENTAGE_OF_AMOUNT_SPENT = "percentage";
    public static final String ACCUMULATE_BY_RATE = "rate";
    public static final String ACCUMULATION_UNITS_POINTS = "Points";
    public static final String ACCUMULATION_UNITS_CREDIT = "Budget";

    // Deals
    public static final String DEALS_ITEM_CODE = "itemCode";
    public static final String DEALS_DEAL_CODE = "dealCode";

}
