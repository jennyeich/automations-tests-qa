package hub.common.pages;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by lior on 7/10/17.
 */
public class DashboardPage extends BaseSeleniumObject {

    public static final String DASHBOARD_KPIS_DROPDOWN_1 = "(//a[contains(@class,\"icon-gear\")])[1]";
    public static final String DASHBOARD_KPIS_DROPDOWN_2 = "(//a[contains(@class,\"icon-gear\")])[2]";
    public static final String DASHBOARD_KPIS_DROPDOWN_3 = "(//a[contains(@class,\"icon-gear\")])[3]";
    public static final String DASHBOARD_KPIS_DROPDOWN_4 = "(//a[contains(@class,\"icon-gear\")])[4]";
    public static final String DASHBOARD_KPI_JOINED_THE_CLUB = "//li[@automationid='dashboard.widget.joinedclub']";
    public static final String DASHBOARD_KPI_CONNECT_WITH_FACEBOOK = "//li[@automationid='dashboard.widget.connectedwithfb']";
    public static final String DASHBOARD_KPI_SHARE_ON_FACEBOOK = "//li[@automationid='dashboard.widget.fbpost']";
    public static final String DASHBOARD_KPI_FILLED_OUT_A_FORM = "//li[@automationid='dashboard.widget.formfilled']";



    @SeleniumPath(value = DASHBOARD_KPIS_DROPDOWN_1, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = DASHBOARD_KPIS_DROPDOWN_1)
    private String dashboardKpisDropdown1;
    @SeleniumPath(value = DASHBOARD_KPIS_DROPDOWN_2, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = DASHBOARD_KPIS_DROPDOWN_2)
    private String dashboardKpisDropdown2;
    @SeleniumPath(value = DASHBOARD_KPIS_DROPDOWN_3, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = DASHBOARD_KPIS_DROPDOWN_3)
    private String dashboardKpisDropdown3;
    @SeleniumPath(value = DASHBOARD_KPIS_DROPDOWN_4, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = DASHBOARD_KPIS_DROPDOWN_4)
    private String dashboardKpisDropdown4;



    public String getDashboardKpisDropdown1() {
        return dashboardKpisDropdown1;
    }

    public void setDashboardKpisDropdown1(String dashboardKpisDropdown1) {
        this.dashboardKpisDropdown1 = dashboardKpisDropdown1;
    }

    public String getDashboardKpisDropdown2() {
        return dashboardKpisDropdown2;
    }

    public void setDashboardKpisDropdown2(String dashboardKpisDropdown2) {
        this.dashboardKpisDropdown2 = dashboardKpisDropdown2;
    }

    public String getDashboardKpisDropdown3() {
        return dashboardKpisDropdown3;
    }

    public void setDashboardKpisDropdown3(String dashboardKpisDropdown3) {
        this.dashboardKpisDropdown3 = dashboardKpisDropdown3;
    }

    public String getDashboardKpisDropdown4() {
        return dashboardKpisDropdown4;
    }

    public void setDashboardKpisDropdown4(String dashboardKpisDropdown4) {
        this.dashboardKpisDropdown4 = dashboardKpisDropdown4;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
