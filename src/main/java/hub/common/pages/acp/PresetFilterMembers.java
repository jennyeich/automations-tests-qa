package hub.common.pages.acp;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 9/25/2017.
 */

public class PresetFilterMembers extends BaseSeleniumObject {

    public static final String ADD_ITEM = "//a[contains(text(),\"Add Item\")]";
    public static final String TITLE = "//input[@name=\"title\"]";
    public static final String ADD_ACTION_RULE = "//span[contains(text(),\"find_users_add_action_rule\")]";
    public static final String MEMBER_ACTION_TYPE = "rule_action";
    public static final String CHOOSE_DATES = "//select[@automationid=\"filterMembersChooseDates\"]";
    public static final String FROM_THE_LAST_X_DAYS = "//input[@automationid=\"filterMembersChooseDatesFromRange\"]";
    public static final String SAVE = "//*[@id=\"main\"]/div/button[@automationid=\"save\"]";


    @SeleniumPath(value = ADD_ITEM, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = ADD_ITEM)
    private String addItem;
    @SeleniumPath(value = TITLE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = TITLE)
    private String title;
    @SeleniumPath(value = ADD_ACTION_RULE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String addActionRule;
    @SeleniumPath(value = MEMBER_ACTION_TYPE, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = MEMBER_ACTION_TYPE)
    private String memberActionType;
    @SeleniumPath(value = CHOOSE_DATES, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = CHOOSE_DATES)
    private String chooseDate;
    @SeleniumPath(value = FROM_THE_LAST_X_DAYS, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = FROM_THE_LAST_X_DAYS)
    private String fromTheLastXDays;
    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE,readValue = "")
    private String save;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMemberActionType() {
        return memberActionType;
    }

    public void setMemberActionType(String memberActionType) {
        this.memberActionType = memberActionType;
    }

    public String getChooseDate() {
        return chooseDate;
    }

    public void setChooseDate(String chooseDate) {
        this.chooseDate = chooseDate;
    }

    public String getFromTheLastXDays() {
        return fromTheLastXDays;
    }

    public void setFromTheLastXDays(String fromTheLastXDays) {
        this.fromTheLastXDays = fromTheLastXDays;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

