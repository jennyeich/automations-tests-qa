package hub.common.pages.acp;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 9/18/2017.
 */

public class ImportUsersKey extends BaseSeleniumObject {

    public static final String CUSTOM_USER_KEYS_TEXTAREA = "//*[@id=\"customUserkeys\"]";
    public static final String LIST_TITLE = "//*[@id=\"title\"]";
    public static final String SAVE = "//button[@type=\"submit\"]";

    @SeleniumPath(value = CUSTOM_USER_KEYS_TEXTAREA, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.TEXT_AREA, readValue = CUSTOM_USER_KEYS_TEXTAREA)
    private String customUserKeysTextarea;
    @SeleniumPath(value = LIST_TITLE, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = LIST_TITLE)
    private String listTitle;
    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = SAVE)
    private String saveBtn;

    public String getCustomUserKeysTextarea() {
        return customUserKeysTextarea;
    }

    public void setCustomUserKeysTextarea(String customUserKeysTextarea) {
        this.customUserKeysTextarea = customUserKeysTextarea;
    }

    public String getListTitle() {
        return listTitle;
    }

    public void setListTitle(String listTitle) {
        this.listTitle = listTitle;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

