package hub.common.pages.acp;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import org.openqa.selenium.Keys;

/**
 * Created by Goni on 9/26/2017.
 */
//The businessPlan is always DISALLOW by default
public class BusinessPlanPage extends BaseSeleniumObject {

    public static final String USER_PERMISSION_TAGS = "//div[@automationid='dropdownBusinessPlansTags']";
    public static final String TAG_SEARCH_BOX = "//div[@automationid='dropdownBusinessPlansTags']//input[@type=\"search\"]";
    public static final String USER_PERMISSION_ADD_BUTTON = "(//button[@class='submitButton ng-scope' and contains(text(), 'Add')])[1]";

    @SeleniumPath(value = USER_PERMISSION_TAGS, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = USER_PERMISSION_TAGS)
    private String userPermissionTags;
    @SeleniumPath(value = TAG_SEARCH_BOX, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TAG_SEARCH_BOX)
    private String tagSearchBox;
    @SeleniumPath(value = USER_PERMISSION_ADD_BUTTON, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = USER_PERMISSION_ADD_BUTTON)
    private String userPermissionAddButton;

    public String getUserPermissionTags() {
        return userPermissionTags;
    }

    public void setUserPermissionTags(String userPermissionTags) {
        this.userPermissionTags = userPermissionTags;
    }

    public String getTagSearchBox() {
        return tagSearchBox;
    }

    public void setTagSearchBox(String tagSearchBox) {
        this.tagSearchBox = tagSearchBox + Keys.ENTER;
    }
    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

