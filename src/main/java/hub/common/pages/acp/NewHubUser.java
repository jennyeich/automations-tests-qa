package hub.common.pages.acp;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 11/8/2017.
 */

public class NewHubUser extends BaseSeleniumObject {

    public static final String FIRST_NAME = "firstname";
    public static final String LAST_NAME = "lastname";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";
    public static final String SEARCH_BY = "//*[@id=\"infoForm\"]/div[4]/select";
    public static final String SEARCH_INPUT = "search";
    public static final String SEARCH_BUTTON = "//button[@title=\"acp.usersmanagement.create.search.tooltip\"]";
    public static final String CHOOSEN_APP_CHK = "//input[starts-with(@id,\"search_results_location_\")]";
    public static final String ADD_TO_USER = "//button[contains(text(),\"Add to user\")]";
    public static final String SAVE = "//button[contains(text(),\"Save\")]";


    @SeleniumPath(value = FIRST_NAME, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = FIRST_NAME)
    private String firstName;
    @SeleniumPath(value = LAST_NAME, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = LAST_NAME)
    private String lastName;
    @SeleniumPath(value = EMAIL, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = EMAIL)
    private String email;
    @SeleniumPath(value = PASSWORD, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = PASSWORD)
    private String password;
    @SeleniumPath(value = SEARCH_BY, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = SEARCH_BY)
    private String searchBy;
    @SeleniumPath(value = SEARCH_INPUT, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = SEARCH_INPUT)
    private String searchInput;
    @SeleniumPath(value = SEARCH_BUTTON, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = "")
    private String searchBtn;
    @SeleniumPath(value = CHOOSEN_APP_CHK, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = "")
    private String choosenAppChk;
    @SeleniumPath(value = ADD_TO_USER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = "")
    private String addToUser;

    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = "")
    private String save;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSearchBy() {
        return searchBy;
    }

    public void setSearchBy(String searchBy) {
        this.searchBy = searchBy;
    }

    public String getSearchInput() {
        return searchInput;
    }

    public void setSearchInput(String searchInput) {
        this.searchInput = searchInput;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public enum SearchBySelect{

        Location_name("location name"),
        User_email("user email"),
        Location_id("location id"),
        ;

        private final String text;


        SearchBySelect(final String text) {
            this.text = text;

        }

        @Override
        public String toString() {
            return text;
        }
    }
}

