package hub.common.pages.acp;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import org.openqa.selenium.Keys;

import java.text.MessageFormat;

/**
 * Created by Goni on 9/25/2017.
 */

public class UserPlanPage extends BaseSeleniumObject {

    public static final String USER_PERMISSION_TAGS = "//div[@automationid='dropdownUserPlansTags']";
    public static final String TAG_SEARCH_BOX = "//div[@automationid='dropdownUserPlansTags']//input[@type=\"search\"]";

    public static final String USER_PERMISSION_PERMISSION_LEVEL = "//div[@selected-item=\"new_perm_level\"]";
    public static final String PERMISSION_LEVEL_ALLOW_OR_DISALLOW = "//*[starts-with(@id,\"ui-select-choices-row\")]/div/span[contains(text(),\"{0}\")]";
    public static final String USER_PERMISSION_ADD_BUTTON = "(//button[@class='submitButton ng-scope' and contains(text(), 'Add')])[1]";


    @SeleniumPath(value = USER_PERMISSION_TAGS, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = USER_PERMISSION_TAGS)
    private String userPermissionTags;

    @SeleniumPath(value = TAG_SEARCH_BOX, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TAG_SEARCH_BOX)
    private String tagSearchBox;

    @SeleniumPath(value = USER_PERMISSION_PERMISSION_LEVEL, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = USER_PERMISSION_PERMISSION_LEVEL)
    private String userPermissionPermissionLevel;

    @SeleniumPath(value = PERMISSION_LEVEL_ALLOW_OR_DISALLOW, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT2_THROUGH_LABEL, readValue = PERMISSION_LEVEL_ALLOW_OR_DISALLOW)
    private String permissionLevelAllowOrDisallow;

    @SeleniumPath(value = USER_PERMISSION_ADD_BUTTON, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = USER_PERMISSION_ADD_BUTTON)
    private String userPermissionAddButton;

    public String getUserPermissionTags() {
        return userPermissionTags;
    }

    public void setUserPermissionTags(String userPermissionTags) {
        this.userPermissionTags = userPermissionTags;
    }

    public String getUserPermissionPermissionLevel() {
        return userPermissionPermissionLevel;
    }

    public void setUserPermissionPermissionLevel(String userPermissionPermissionLevel) {
        this.userPermissionPermissionLevel = userPermissionPermissionLevel;
    }

    public String getTagSearchBox() {
        return tagSearchBox;
    }

    public void setTagSearchBox(String tagSearchBox) {
        this.tagSearchBox = tagSearchBox + Keys.ENTER;
    }

    public String getPermissionLevelAllowOrDisallow() {

        return permissionLevelAllowOrDisallow;
    }

    public void setPermissionLevelAllowOrDisallow(String permissionLevel) {
        this.permissionLevelAllowOrDisallow = MessageFormat.format(PERMISSION_LEVEL_ALLOW_OR_DISALLOW, permissionLevel);
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

