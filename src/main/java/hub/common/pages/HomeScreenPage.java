package hub.common.pages;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by lior on 7/12/17.
 */
public class HomeScreenPage extends BaseSeleniumObject {

    public static final String CHOOSE_LAYOUT_ARROW = "//i[contains(@class,\"icon-arrow_down\")]";
    public static final String SIMPLE_LAYOUT = "//div[@translate='tcalts.title.MainLocationTC-0x2x2' and contains(@class,\"tcalts-title\")]";
    public static final String STYLIST_LAYOUT = "//div[@translate='tcalts.title.MainLocationTC-3x0' and contains(@class,\"tcalts-title\")]";
    public static final String SLIM_LAYOUT = "//div[@translate='tcalts.title.MainLocationTC-0x3' and contains(@class,\"tcalts-title\")]";
    public static final String STUDIO_LAYOUT = "//div[@translate='tcalts.title.MainLocationTC-0x4' and contains(@class,\"tcalts-title\")]";
    public static final String THIN_LAYOUT = "//div[@translate='tcalts.title.MainLocationTC-2x0x2' and contains(@class,\"tcalts-title\")]";
    public static final String MAIN_LOCATION_TC_FULL_SCREEN_0x3 = "//div[@translate='tcalts.title.MainLocationTC-FullScreen-0x3' and contains(@class,\"tcalts-title\")]";
    public static final String MAIN_LOCATION_TC_FULL_SCREEN_0x4 = "//div[@translate='tcalts.title.MainLocationTC-FullScreen-0x4' and contains(@class,\"tcalts-title\")]";
    public static final String MAIN_LOCATION_TC_FULL_SCREEN_0x2x2 = "//div[@translate='tcalts.title.MainLocationTC-FullScreen-0x2x2' and contains(@class,\"tcalts-title\")]";
    public static final String MAIN_LOCATION_TC_FULL_SCREEN_0x3x3 = "//div[@translate='tcalts.title.MainLocationTC-FullScreen-0x3x3' and contains(@class,\"tcalts-title\")]";
    public static final String HANDY_BARISTA_LAYOUT = "//div[@translate='tcalts.title.MainLocationTC' and contains(@class,\"tcalts-title\")]";






    @SeleniumPath(value = CHOOSE_LAYOUT_ARROW, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = CHOOSE_LAYOUT_ARROW)
    private String chooseLayoutArrow;




    public String getChooseLayoutArrow() {
        return chooseLayoutArrow;
    }

    public void setChooseLayoutArrow(String chooseLayoutArrow) {
        this.chooseLayoutArrow = chooseLayoutArrow;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
