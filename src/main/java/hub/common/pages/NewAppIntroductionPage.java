package hub.common.pages;

import org.openqa.selenium.By;

/**
 * Created by Goni on 3/23/2017.
 */
public class NewAppIntroductionPage  {

    // fields and elements
    public static final By APP_NAME = By.id("displayName");
    public static final By WEBSITE = By.id("website");
    public static final By PHONE = By.id("phone");
    public static final By GENERAL = By.id( "general");
    public static final By POSITION = By.id("position");
}
