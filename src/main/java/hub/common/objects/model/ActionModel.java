package hub.common.objects.model;

import java.util.Date;

/**
 * Created by doron on 22/05/2017.
 */
public class ActionModel {
    private final String title;
    private final String stringDate;
    private final Date date;
    public ActionModel(String title, String date) {
        this.title = title;
        this.stringDate = date;
        this.date = new Date(stringDate);
    }

    public String getTitle() {
        return title;
    }
    public Date getDate() {
        return date;
    }

}
