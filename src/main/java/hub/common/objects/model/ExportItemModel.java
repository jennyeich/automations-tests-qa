package hub.common.objects.model;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by doron on 22/05/2017.
 */
public class ExportItemModel extends BaseSeleniumObject {

    public static final String DATE = "creationDate";
    public static final String MEMBERS_COUNT = "memberCount";
    public static final String STATUS = "status";

    @SeleniumPath(value = "", type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.LABEL, readValue = MEMBERS_COUNT)
    private String membersCountLabel;
    @SeleniumPath(value = "", type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.LABEL, readValue = DATE)
    private String dateLabel;
    @SeleniumPath(value = "", type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.LABEL, readValue = STATUS)
    private String status;
    
    public int getMembersCount() {
        return Integer.parseInt(membersCountLabel);
     }

    public Calendar getDate() throws ParseException {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        cal.setTime(sdf.parse(dateLabel));
        return cal;
    }

    public String getMembersCountLabel() {return membersCountLabel; }

    public String getDateLabel() throws ParseException {return dateLabel; }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
