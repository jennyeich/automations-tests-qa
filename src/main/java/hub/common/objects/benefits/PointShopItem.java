package hub.common.objects.benefits;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 9/12/2017.
 */

public class PointShopItem extends BaseSeleniumObject {

    //add item
    public static final String ADD_ITEM = "//div[1]/span[contains(text(),\"Add Item\")]";
    public static final String TITLE = "(//*[@id=\"giftTitle\"])";
    public static final String TOGGLE_BLOCK = "(//*[starts-with(@id,\"gift-\")]/i)";


    public static final String TEXT = "(//*[@id=\"text\"])";
    public static final String PRICE = "(//*[@id=\"price\"])";
    public static final String CHOOSE_GIFT = "(//*[@id=\"asset_id\"])";
    public static final String CHOOSE_IMAGE = "(//*[starts-with(@id,\"gift-\")]/div[2]/div[3]/div/div[1])";
    public static final String IMAGE_PATH = "(//*[starts-with(@id,\"gift-\")]//li[2]/img)";

    @SeleniumPath(value = ADD_ITEM, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT,readValue = ADD_ITEM)
    private String addItem;
    @SeleniumPath(value = TITLE, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = TITLE)
    private String title;
    @SeleniumPath(value = TOGGLE_BLOCK, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE,readValue = TOGGLE_BLOCK)
    private String toggleBlock;
    @SeleniumPath(value = TEXT, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = TEXT)
    private String description;
    @SeleniumPath(value = PRICE, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = PRICE)
    private String price;
    @SeleniumPath(value = CHOOSE_GIFT, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = CHOOSE_GIFT)
    private String chooseGift;
    @SeleniumPath(value = CHOOSE_IMAGE, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String chooseImage;
    @SeleniumPath(value = IMAGE_PATH, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String imagePath;

    public int counter;

    public PointShopItem(PointShopPage pointShop) {
        setNext(pointShop.getItemCounter()+1);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getToggleBlock() {
        return toggleBlock;
    }

    public void setToggleBlock(String toggleBlock) {
        this.toggleBlock = toggleBlock;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getChooseGift() {
        return chooseGift;
    }

    public void setChooseGift(String chooseGift) {
        this.chooseGift = chooseGift;
    }

    public String getChooseImage() {
        return chooseImage;
    }

    public void setChooseImage(String chooseImage) {
        this.chooseImage = chooseImage;
    }
    public String getImagePath() {
        return imagePath;
    }
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    public void setNext (int counter) {this.counter = counter;}

    public int getNext (){return counter;}

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

