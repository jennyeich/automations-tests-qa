package hub.common.objects.benefits;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 5/22/2017.
 */
public class LotteryReward extends BaseSeleniumObject implements IAsset{

    public static final String TITLE = "//input[@name=\"rewardname\"]";
    public static final String MAX_REWARDS = "//input[@name=\"max_rewards\"]";
    public static final String TAGS = "//*[@id=\"newReward\"]/div[2]/tags-input/div/div/input";
    public static final String READ_TAGS = "//*[@id=\"newReward\"]/div[2]/tags-input/div/div/ul/li/ti-tag-item/ng-include/span";
    public static final String SAVE = "save";


    @SeleniumPath(value = TITLE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = TITLE)
    private String title;
    @SeleniumPath(value = MAX_REWARDS, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = MAX_REWARDS)
    private String maxRewards;
    @SeleniumPath(value = TAGS, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = READ_TAGS)
    private List<String> tags;

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY,readValue = "")
    List<RewardOptions> rewardOptionsList;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMaxRewards() {
        return maxRewards;
    }

    public void setMaxRewards(String maxRewards) {
        this.maxRewards = maxRewards;
    }

    public List<RewardOptions> getRewardOptionsList() {
        if(rewardOptionsList == null)
            rewardOptionsList = new ArrayList<>();
        return rewardOptionsList;
    }

    public void setRewardOptionsList(List<RewardOptions> rewardOptionsList) {
        this.rewardOptionsList = rewardOptionsList;
    }

    public void addRewardOptions(RewardOptions rewardOptions) {
        getRewardOptionsList().add(rewardOptions);
    }
    @SeleniumPath(value = SAVE, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT)
    private String saveButton;


    public List<String> getTags() {
        return tags;
    }
    public void setTags(List<String> tags) {
        this.tags = tags;
    }
    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
