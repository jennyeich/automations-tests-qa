package hub.common.objects.benefits;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 12/12/2017.
 */

public class RewardOptions extends BaseSeleniumObject {

    public static final String PROBABILITY = "//select[@name=\"probability\"]";
    public static final String REWARD_TYPE = "(//select[@name=\"RewardType\"])[1]";
    public static final String REWARD = "(//select[@name=\"Template\"])[1]";
    public static final String PUSH_MESSAGE = "(//input[@name=\"Message\"])[1]";
    public static final String LIMIT = "(//input[@name=\"Limit\"])[1]";
    public static final String NOTIFY_USERS ="(//select[@name=\"NotifyUsers\"])[1]";
    public static final String ADD_REWARD ="//button[contains(text(),\"Add Reward Option\")]";

    @SeleniumPath(value = PROBABILITY, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = PROBABILITY)
    private String probability;
    @SeleniumPath(value = REWARD_TYPE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = REWARD_TYPE)
    private String rewardType;
    @SeleniumPath(value = REWARD, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = REWARD)
    private String reward;
    @SeleniumPath(value = LIMIT, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = LIMIT)
    private String limit;
    @SeleniumPath(value = PUSH_MESSAGE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = PUSH_MESSAGE)
    private String pushMessage;
    @SeleniumPath(value = NOTIFY_USERS, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT,readValue = NOTIFY_USERS)
    private String notify;
    @SeleniumPath(value = ADD_REWARD, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT)
    private String addRewardBtn;

    public String getProbability() {
        return probability;
    }

    public void setProbability(String probability) {
        this.probability = probability;
    }

    public String getRewardType() {
        return rewardType;
    }

    public void setRewardType(String rewardType) {
        this.rewardType = rewardType;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public String getPushMessage() {
        return pushMessage;
    }

    public void setPushMessage(String pushMessage) {
        this.pushMessage = pushMessage;
    }

    public String getNotify() {
        return notify;
    }

    public void setNotify(String notify) {
        this.notify = notify;
    }

    public String getLimit() {
        return limit;
    }

    public void setLimit(String limit) {
        this.limit = limit;
    }

    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

}

