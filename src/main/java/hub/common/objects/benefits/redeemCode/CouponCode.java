package hub.common.objects.benefits.redeemCode;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

public class CouponCode extends BaseSeleniumObject {

    public static final String ADD_COUPON_CODE = "//span[contains(text(),\"Add Coupon Code\")]";
    public static final String CODE_NAME = "couponName";
    public static final String CHOOSE_AUTOMATION = "code_id";
    public static final String SAVE = "save";
    public static final String CLICK_ARROW = "//*[contains(@class,\"icon-arrow_down\")]";
    public static final String BULK_NAME = "name";
    public static final String BULK_TAG = "tag";
    public static final String DESCRIPTION = "description";
    public static final String NUM_OF_CODES = "units";
    public static final String CODE_LENGTH = "codes_length";
    public static final String CUSTOM_CODES = "codes";
    public static final String GENERATE_NEW_CODES = "//*[@ng-click=\"generate();\"]";


    @SeleniumPath(value = ADD_COUPON_CODE, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = ADD_COUPON_CODE)
    private String addNewCode;
    @SeleniumPath(value = CODE_NAME, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = CODE_NAME)
    private String codeName;
    @SeleniumPath(value = CHOOSE_AUTOMATION, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = CHOOSE_AUTOMATION)
    private String chooseAutomation;
    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT, readValue = SAVE)
    private String save;
    @SeleniumPath(value = CLICK_ARROW, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE,readValue =CLICK_ARROW)
    private String clickArrow;
    @SeleniumPath(value = BULK_NAME, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =BULK_NAME)
    private String bulkName;
    @SeleniumPath(value = BULK_TAG, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =BULK_TAG)
    private String bulkTag;
    @SeleniumPath(value = DESCRIPTION, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =DESCRIPTION)
    private String description;
    @SeleniumPath(value = NUM_OF_CODES, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =NUM_OF_CODES)
    private String numOfCodes;
    @SeleniumPath(value = CODE_LENGTH, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =CODE_LENGTH)
    private String codeLength;
    @SeleniumPath(value = CUSTOM_CODES, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.TEXT_AREA,readValue =CUSTOM_CODES)
    private String codes;
    @SeleniumPath(value = GENERATE_NEW_CODES, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = GENERATE_NEW_CODES)
    private String generateNewCodes;


    public String getBulkName() {return bulkName;}
    public void setBulkName(String bulk_name) {this.bulkName = bulk_name;}
    public void setDescription(String description){
        this.description = description;
    }
    public String getDescription(){
        return description;
    }
    public String getBulkTag() {return bulkTag;}
    public void setBulkTag(String bulk_tag) {this.bulkTag = bulk_tag;}
    public String getCodes() {return codes;}
    public void setCodes(String codes) {this.codes = codes;}
    public String getNumOfCodes() {return numOfCodes;}
    public void setNumOfCodes(String numOfCodes) {this.numOfCodes = numOfCodes;}
    public String getCodeLength() {return codeLength;}
    public void setCodeLength(String codeLength) {this.codeLength = codeLength;}
    public String getCodeName() {return codeName;}
    public void setCodeName(String codeName) {this.codeName = codeName;}

    public String getChooseAutomation() {
        return chooseAutomation;
    }

    public void setChooseAutomation(String chooseAutomation) {
        this.chooseAutomation = chooseAutomation;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
