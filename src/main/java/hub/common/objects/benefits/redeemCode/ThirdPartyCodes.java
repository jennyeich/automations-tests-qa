package hub.common.objects.benefits.redeemCode;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Jenny on 6/14/2017.
 */
public class ThirdPartyCodes extends BaseSeleniumObject {

    public static final String GENERATE_NEW_CODE = "//span[contains(text(),\"Generate new codes\")]";
    public static final String CODE_NAME = "couponName";
    public static final String CHOOSE_ASSET = "code_asset";
    public static final String SAVE = "//*[@id=\"main\"]/div/button";
    public static final String CLICK_ARROW = "//*[@id=\"main\"]/div/div[3]/i";
    public static final String BULK_NAME = "name";
    public static final String BULK_TAG = "tag";
    public static final String DESCRIPTION = "description";
    public static final String NUM_OF_CODES = "units";
    public static final String CODE_LENGTH = "codes_length";
    public static final String CUSTOM_CODES = "codes";
    public static final String GENERATE_NEW_CODES = "//*[@data-ng-click=\"trdpItemCtrl.generateBulk();\"]";


    @SeleniumPath(value = GENERATE_NEW_CODE, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = GENERATE_NEW_CODE)
    private String generateNewCode;
    @SeleniumPath(value = CODE_NAME, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = CODE_NAME)
    private String codeName;
    @SeleniumPath(value = CHOOSE_ASSET, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = CHOOSE_ASSET)
    private String chooseAsset;
    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT, readValue = SAVE)
    private String save;
    @SeleniumPath(value = CLICK_ARROW, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE,readValue =CLICK_ARROW)
    private String clickArrow;
    @SeleniumPath(value = BULK_NAME, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =BULK_NAME)
    private String bulk_name;
    @SeleniumPath(value = BULK_TAG, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =BULK_TAG)
    private String bulk_tag;
    @SeleniumPath(value = DESCRIPTION, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =DESCRIPTION)
    private String description;
    @SeleniumPath(value = NUM_OF_CODES, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =NUM_OF_CODES)
    private String numOfCodes;
    @SeleniumPath(value = CODE_LENGTH, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =CODE_LENGTH)
    private String codeLength;
    @SeleniumPath(value = CUSTOM_CODES, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.TEXT_AREA,readValue =CUSTOM_CODES)
    private String codes;
    @SeleniumPath(value = GENERATE_NEW_CODES, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = GENERATE_NEW_CODES)
    private String generateNewCodes;


    public String getBulk_name() {return bulk_name;}
    public void setBulk_name(String bulk_name) {this.bulk_name = bulk_name;}
    public void setDescription(String description){
        this.description = description;
    }
    public String getDescription(){
        return description;
    }
    public String getBulk_tag() {return bulk_tag;}
    public void setBulk_tag(String bulk_tag) {this.bulk_tag = bulk_tag;}
    public String getCodes() {return codes;}
    public void setCodes(String codes) {this.codes = codes;}
    public String getNumOfCodes() {return numOfCodes;}
    public void setNumOfCodes(String numOfCodes) {this.numOfCodes = numOfCodes;}
    public String getCodeLength() {return codeLength;}
    public void setCodeLength(String codeLength) {this.codeLength = codeLength;}
    public String getCodeName() {return codeName;}
    public void setCodeName(String codeName) {this.codeName = codeName;}
    public String getChooseAsset() {return chooseAsset;}
    public void setChooseAsset(String chooseAsset) {this.chooseAsset = chooseAsset;}
    public String getNextTagsPath(int i){return "";}

    @Override
    public void setNext(int i) {}
    @Override
    public int getNext() {
        return 0;
    }
    public int getNextTag () {return 0;}
    @Override
    public int getCreateActionCounter() {
        return 0;
    }


}
