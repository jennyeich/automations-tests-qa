package hub.common.objects.benefits.redeemCode;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Jenny on 11/6/2016.
 */
public class RedeemCodeBulk extends BaseSeleniumObject {

    public static final String REWARD_SKU ="//*[@data-ng-model=\"smartAssetsEditCtrl.assetData.sku_code\"]";


    @SeleniumPath(value = REWARD_SKU, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = REWARD_SKU)
    private String rewardSKU;

    public RedeemCodeBulk (){}
    public RedeemCodeBulk(String rewardSKU) {
        this.rewardSKU = rewardSKU;
    }

    public String getRewardSKU() {
        return rewardSKU;
    }

    public void setRewardSKU(String rewardSKU) {
        this.rewardSKU = rewardSKU;
    }

    public String getNextTagsPath(int i){
        return "";
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
