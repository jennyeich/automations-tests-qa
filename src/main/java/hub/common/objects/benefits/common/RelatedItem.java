package hub.common.objects.benefits.common;

import java.util.ArrayList;

/**
 * Created by Jenny on 11/9/2016.
 */
public class RelatedItem {

    String GiftItem_DealCode;
    ArrayList<String> RelatedItemCodes;

    public String getGiftItem_DealCode() {
        return GiftItem_DealCode;
    }

    public void setGiftItem_DealCode(String giftItem_DealCode) {
        GiftItem_DealCode = giftItem_DealCode;
    }

    public ArrayList<String> getRelatedItemCodes() {
        return RelatedItemCodes;
    }

    public void setRelatedItemCodes(ArrayList<String> relatedItemCodes) {
        RelatedItemCodes = relatedItemCodes;
    }
}
