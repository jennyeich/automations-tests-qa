package hub.common.objects.benefits.common;

import hub.common.objects.common.FilterLocationsListbyTag;

/**
 * Created by Jenny on 11/7/2016.
 */
public class Button {

    private String buttonText;
    private String buttonAction; //Will be choosen from the buttonActionList enum

    /*
    For actions which have additional fields fill the relevant field:
    For example: For "Call Us" fill the phone number: CallUs_PhoneNumber
     */
    //Additional fields:
    private String openAppScreen_ChooseAnApp;
    private String OpenURLInExternalBrowser_PageURL;
    private String ShareItemonFacebook_ChooseAnItem;
    private String CallUs_PhoneNumber;
    private String ScanQRCode_ChooseQRCodeType;
    private String OpenWebPage_PageURL;

    //FB content object
    private ShareContentOnFB contentFB;
    //pop up object
    private OpenPopUpMSG popUPMessage;
    //filter locations list by tag
    private FilterLocationsListbyTag filterLocationsListbyTag;

    public String getOpenAppScreen_ChooseAnApp() {
        return openAppScreen_ChooseAnApp;
    }

    public void setOpenAppScreen_ChooseAnApp(String openAppScreen_ChooseAnApp) {
        this.openAppScreen_ChooseAnApp = openAppScreen_ChooseAnApp;
    }

    public String getOpenURLInExternalBrowser_PageURL() {
        return OpenURLInExternalBrowser_PageURL;
    }

    public void setOpenURLInExternalBrowser_PageURL(String openURLInExternalBrowser_PageURL) {
        OpenURLInExternalBrowser_PageURL = openURLInExternalBrowser_PageURL;
    }

    public String getShareItemonFacebook_ChooseAnItem() {
        return ShareItemonFacebook_ChooseAnItem;
    }

    public void setShareItemonFacebook_ChooseAnItem(String shareItemonFacebook_ChooseAnItem) {
        ShareItemonFacebook_ChooseAnItem = shareItemonFacebook_ChooseAnItem;
    }

    public String getCallUs_PhoneNumber() {
        return CallUs_PhoneNumber;
    }

    public void setCallUs_PhoneNumber(String callUs_PhoneNumber) {
        CallUs_PhoneNumber = callUs_PhoneNumber;
    }

    public String getScanQRCode_ChooseQRCodeType() {
        return ScanQRCode_ChooseQRCodeType;
    }

    public void setScanQRCode_ChooseQRCodeType(String scanQRCode_ChooseQRCodeType) {
        ScanQRCode_ChooseQRCodeType = scanQRCode_ChooseQRCodeType;
    }

    public String getOpenWebPage_PageURL() {
        return OpenWebPage_PageURL;
    }

    public void setOpenWebPage_PageURL(String openWebPage_PageURL) {
        OpenWebPage_PageURL = openWebPage_PageURL;
    }

    public ShareContentOnFB getContentFB() {
        return contentFB;
    }

    public void setContentFB(ShareContentOnFB contentFB) {
        this.contentFB = contentFB;
    }

    public OpenPopUpMSG getPopUPMessage() {
        return popUPMessage;
    }

    public void setPopUPMessage(OpenPopUpMSG popUPMessage) {
        this.popUPMessage = popUPMessage;
    }

    public FilterLocationsListbyTag getFilterLocationsListbyTag() {
        return filterLocationsListbyTag;
    }

    public void setFilterLocationsListbyTag(FilterLocationsListbyTag filterLocationsListbyTag) {
        this.filterLocationsListbyTag = filterLocationsListbyTag;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public String getButtonAction() {
        return buttonAction;
    }

    public void setButtonAction(String buttonAction) {
        this.buttonAction = buttonAction;
    }


}
