package hub.common.objects.benefits.common;

/**
 * Created by Jenny on 11/9/2016.
 */
public class OpenPopUpMSG {


    String title;
    String messageText;
    Button button;
    String showCancelButtononPopupMessage; //Yes/No

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }

    public String getShowCancelButtononPopupMessage() {
        return showCancelButtononPopupMessage;
    }

    public void setShowCancelButtononPopupMessage(String showCancelButtononPopupMessage) {
        this.showCancelButtononPopupMessage = showCancelButtononPopupMessage;
    }
}
