package hub.common.objects.benefits.common;

/**
 * Created by Jenny on 11/8/2016.
 */
public class DiscountType {

    private String discountType; //choose from DiscountTypeList
    private String fixedAmount_discountParamsSum; //relevant for fixed Amount type
    private String BBB_Discount_Params_Sub_String;
    private String percent_discountPercent; //relevant for percent

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public String getFixedAmount_discountParamsSum() {
        return fixedAmount_discountParamsSum;
    }

    public void setFixedAmount_discountParamsSum(String fixedAmount_discountParamsSum) {
        this.fixedAmount_discountParamsSum = fixedAmount_discountParamsSum;
    }

    public String getBBB_Discount_Params_Sub_String() {
        return BBB_Discount_Params_Sub_String;
    }

    public void setBBB_Discount_Params_Sub_String(String BBB_Discount_Params_Sub_String) {
        this.BBB_Discount_Params_Sub_String = BBB_Discount_Params_Sub_String;
    }

    public String getPercent_discountPercent() {
        return percent_discountPercent;
    }

    public void setPercent_discountPercent(String percent_discountPercent) {
        this.percent_discountPercent = percent_discountPercent;
    }

    public String getPercent_discountPercentLimitAmount() {
        return percent_discountPercentLimitAmount;
    }

    public void setPercent_discountPercentLimitAmount(String percent_discountPercentLimitAmount) {
        this.percent_discountPercentLimitAmount = percent_discountPercentLimitAmount;
    }

    private String percent_discountPercentLimitAmount; //relevant for percent

}
