package hub.common.objects.benefits.common;

/**
 * Created by Jenny on 11/7/2016.
 */
public enum buttonActionList {



    NoAction("No SmartGiftAction"),
    OpenAppScreen("Open App Screen"),
    CloseAppScreen("Close App Screen"),
    OpenURLInExternalBrowser("Open Url In external Browser"),
    ShareItemOnFB("Share Item on Facebook"),
    CallUS("Call Us"),
    ScanReceipt("Scan Receipt"),
    GiveFeedback("Give Feedback"),
    NavigatetoLocation("Navigate to Location"),
    ShareContentonFacebook("Share content on Facebook"),
    ScanQRCode("Scan QR Code"),
    OpenPopupMessage("Open Pop-up Message"),
    RatetheApp("Rate the App"),
    ShareaTextPost_notFacebook("Share a Text Post (not Facebook)"),
    OpenWebPage("Open Web Page"),
    OpenWebView("Open Web View"),
    FilterLocationListbyTag("Filter Location List by Tag"),
    OpenCatalog("Open catalog"),

   /* ScanReceipt("Scan Receipt"),
    ScanReceipt("Scan Receipt"),
    ScanReceipt("Scan Receipt"),
    ScanReceipt("Scan Receipt"),
    ScanReceipt("Scan Receipt"),
    ScanReceipt("Scan Receipt"),
    ScanReceipt("Scan Receipt"),
    ScanReceipt("Scan Receipt"),
    ScanReceipt("Scan Receipt"),
    ScanReceipt("Scan Receipt"),
    ScanReceipt("Scan Receipt"),
    ScanReceipt("Scan Receipt"),
    ScanReceipt("Scan Receipt"),
    */

    ;
    private final String text;

    buttonActionList(final String text) {
        this.text = text;
    }


    @Override
    public String toString() {
        return text;
    }


}
