package hub.common.objects.benefits.common;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by Jenny on 11/9/2016.
 */
public class ShareContentOnFB {

    private String postName;
    private String description;
    private String title;
    private String link;
    private Image image;

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public ArrayList<String> getFilterByTag() {
        return FilterByTag;
    }

    public void setFilterByTag(ArrayList<String> filterByTag) {
        FilterByTag = filterByTag;
    }

    private ArrayList<String> FilterByTag;



}
