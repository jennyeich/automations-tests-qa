package hub.common.objects.benefits.common;

/**
 * Created by Jenny on 11/9/2016.
 */
public enum QRCodeType {


    CouponCode("Coupon Code"),
    IDCode("ID Code"),
    OpensWebView("Opens Web View"),
    ;
    private final String text;

    QRCodeType(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }


}
