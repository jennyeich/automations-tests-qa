package hub.common.objects.benefits.common;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import org.openqa.selenium.By;

import java.util.Date;

/**
 * Created by Jenny on 11/7/2016.
 */
public class Rule extends BaseSeleniumObject {

    public static final String ADD_NEW_RULE = "//*[@id=\"main\"]/div/div[2]/button";
    @SeleniumPath(value = ADD_NEW_RULE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String addNewRule;
    int counter;

    public static final String SUNDAY = "//*[@id=\"main\"]/div/div[2]/div[1]/div/div/div[1]/ul/li[1]";
    public static final String MONDAY= "//*[@id=\"main\"]/div/div[2]/div[1]/div/div/div[1]/ul/li[2]";
    public static final String TUESDAY = "//*[@id=\"main\"]/div/div[2]/div[1]/div/div/div[1]/ul/li[3]";
    public static final String WEDNESDAY = "//*[@id=\"main\"]/div/div[2]/div[1]/div/div/div[1]/ul/li[4]";
    public static final String THURSDAY = "//*[@id=\"main\"]/div/div[2]/div[1]/div/div/div[1]/ul/li[5]";
    public static final String FRIDAY = "//*[@id=\"main\"]/div/div[2]/div[1]/div/div/div[1]/ul/li[6]";
    public static final String SATURDAY = "//*[@id=\"main\"]/div/div[2]/div[1]/div/div/div[1]/ul/li[7]";

    @SeleniumPath(value = SUNDAY , type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String sunday;
    @SeleniumPath(value = MONDAY , type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String monday;
    @SeleniumPath(value = TUESDAY , type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String tuesday;
    @SeleniumPath(value = WEDNESDAY , type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String wednsday;
    @SeleniumPath(value = THURSDAY , type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String thursday;
    @SeleniumPath(value = FRIDAY, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String friday;
    @SeleniumPath(value = SATURDAY , type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String saturday;


    private Date from;
    private Date until;

    //TODO - add the support for those fields
    public static final By Create_Gift_Add_New_Rule_Valid_From_ = By.xpath("//*[@id=\"main\"]/div/div[2]/div[2]/div/div/div[2]/input");
    public static final By Create_Gift_Add_New_Rule_Valid_Until_ = By.xpath("//*[@id=\"main\"]/div/div[2]/div[2]/div/div/div[3]/input");
    public static final By Create_Gift_Add_New_Rule_Remove = By.xpath("//*[@id=\"main\"]/div/div[2]/div[2]/h6/button");

    private int ruleCouner;

    public String getSunday() {
        return sunday;
    }

    public void setSunday(String sunday) {
        this.sunday = sunday;
    }

    public String getMonday() {
        return monday;
    }

    public void setMonday(String monday) {
        this.monday = monday;
    }

    public String getTuesday() {
        return tuesday;
    }

    public void setTuesday(String tuesday) {
        this.tuesday = tuesday;
    }

    public String getWednsday() {
        return wednsday;
    }

    public void setWednsday(String wednsday) {
        this.wednsday = wednsday;
    }

    public String getThursday() {
        return thursday;
    }

    public void setThursday(String thursday) {
        this.thursday = thursday;
    }

    public String getFriday() {
        return friday;
    }

    public void setFriday(String friday) {
        this.friday = friday;
    }

    public String getSaturday() {
        return saturday;
    }

    public void setSaturday(String saturday) {
        this.saturday = saturday;
    }


    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getUntil() {
        return until;
    }

    public void setUntil(Date until) {
        this.until = until;
    }

    public String getNextTagsPath(int i){return "";}
    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}



