package hub.common.objects.benefits.common;

/**
 * Created by Jenny on 11/9/2016.
 */
public enum DiscountTypeList {


    NONE("None"),
    ROUNDDOWN("Round Down"),
    ROUND("Round"),
    FIXEDAMOUNT("Fixed Amount"),
    BBBDISCOUNT("BBB discount"),
    PERCENT("Percent"),
    ;
    private final String text;

    DiscountTypeList(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

}
