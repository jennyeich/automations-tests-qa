package hub.common.objects.benefits;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;

import java.util.ArrayList;

/**
 * Created by Goni on 5/17/2017.
 */
public class PointShopPage extends BaseSeleniumObject {

    public static final String SAVE = "//button[contains(text(),'Save')]";
    private int itemCounter=0;

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    ArrayList<PointShopItem> items;
    @SeleniumPath(value = SAVE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE,readValue = SAVE)
    private String save;


    public PointShopPage() {
    }

    public void addItem(PointShopItem item){
        setItemCounter(getItems().size() + 1);
        items.add(item);

    }

    public ArrayList<PointShopItem> getItems() {
        if(items == null)
            items = new ArrayList<>();
        return items;
    }

    public int getItemCounter() {
        return itemCounter;
    }

    public void setItemCounter(int itemCounter) {
        this.itemCounter = itemCounter;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    public void setNext (int counter) {this.itemCounter = counter;}

    public int getNext (){return itemCounter;}

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
