package hub.common.objects.benefits.giftCard;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Jenny on 11/4/2016.
 */
public class GiftCardBulkCode extends BaseSeleniumObject {


    public static final String OPEN_CODES = "//*[@id=\"buldSection\"]/bulk-code/div[1]/i";
    public static final String BULK_NAME = "name";
    public static final String BULK_TAG = "tag";
    public static final String DESCRIPTION = "(//*[@id=\"description\"])[2]";
    public static final String CUSTOM_CODES = "codes";
    public static final String GENERATE_CODES = "//button[contains(text(),\"Generate Codes\")]";

    @SeleniumPath(value = OPEN_CODES, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE,readValue =OPEN_CODES)
    private String saveButton;
    @SeleniumPath(value = BULK_NAME, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =BULK_NAME)
    private String bulk_name;
    @SeleniumPath(value = BULK_TAG, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =BULK_TAG)
    private String bulk_tag;
    @SeleniumPath(value = DESCRIPTION, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =DESCRIPTION)
    private String description;
    @SeleniumPath(value = CUSTOM_CODES, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =CUSTOM_CODES)
    private String codes;
    @SeleniumPath(value = GENERATE_CODES, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK,readValue =GENERATE_CODES)
    private String generateCode;



    public String getBulk_name() {return bulk_name;}
    public void setBulk_name(String bulk_name) {this.bulk_name = bulk_name;}
    public void setDescription(String description){
        this.description = description;
    }
    public String getDescription(){
        return description;
    }
    public String getBulk_tag() {return bulk_tag;}
    public void setBulk_tag(String bulk_tag) {this.bulk_tag = bulk_tag;}
    public String getCodes() {return codes;}
    public void setCodes(String codes) {this.codes = codes;}
    public String getNextTagsPath (int i){
        return  "";
    }

    public void clickGenerateCode() {this.generateCode = "generate";}

    @Override
    public void setNext(int i) {
    }

    @Override
    public int getNext() {
        return 0;
    }
    public int getNextTag () {
        return 0;
    }

    @Override
    public int getCreateActionCounter()
    {
        return 0;
    }
}
