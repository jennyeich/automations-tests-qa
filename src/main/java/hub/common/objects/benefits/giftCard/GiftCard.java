package hub.common.objects.benefits.giftCard;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.common.TimeUnits;
import java.util.ArrayList;


/**
 * Created by Jenny on 11/4/2016.
 */
public class GiftCard extends BaseSeleniumObject {


    public static final String SAVE = "save";
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String CHOOSE_IMAGE = "images";
    public static final String VALUE = "value";
    public static final String COST = "cost";
    public static final String MIN_PAYMENT_AMOUNT = "//*[@data-ng-model=\"assetsEditCtrl.assetData.payment_min\"]";
    public static final String MAX_PAYMENT_AMOUNT ="//*[@data-ng-model=\"assetsEditCtrl.assetData.payment_max\"]";
    public static final String VALID_FROM = "//*[@automationid=\"validFrom\"]//input[@name=\"datepicker\"]";
    public static final String VALID_UNTIL  = "//*[@automationid=\"validUntil\"]//input[@name=\"datepicker\"]";
    public static final String CAN_RELOADED ="//*[@id=\"newItem\"]/div[2]/div[2]/select";
    public static final String DEACTIVATE_AFTER = "secondsValid";
    public static final String DEACTIVATE_AFTER_UNITES = "//*[@id=\"newItem\"]/div[2]/div[5]/div[1]/div/div[2]/select";
    public static final String READ_DEACTIVATE_AFTER_UNITES = "//*[@id=\"editItem\"]/div[2]/div[5]/div[1]/div/div[2]/select";
    public static final String ACTIVATE_AFTER = "secondsDelay";
    public static final String ACTIVATE_AFTER_UNITS = "//*[@id=\"newItem\"]/div[2]/div[5]/div[2]/div/div[2]/select";
    public static final String READ_ACTIVATE_AFTER_UNITS = "//*[@id=\"editItem\"]/div[2]/div[5]/div[2]/div/div[2]/select";
    public static final String TAGS = "//*[@id=\"newItem\"]/div[2]/tags-input/div/div/input";
    public static final String READ_TAGS = "//*[@id=\"editItem\"]/div[2]/tags-input/div/div/ul/li[1]/ti-tag-item/ng-include/span";


    @SeleniumPath(value = TITLE, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =TITLE)
    private String title;
    @SeleniumPath(value = DESCRIPTION, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =DESCRIPTION)
    private String description;
    @SeleniumPath(value = CHOOSE_IMAGE, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CHOOSE_IMAGE,readValue =CHOOSE_IMAGE)
    private String chooseImage;
    @SeleniumPath(value = VALUE, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =VALUE)
    private String value;
    @SeleniumPath(value = COST, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =COST)
    private String cost;
    @SeleniumPath(value = CAN_RELOADED, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT,readValue =CAN_RELOADED)
    private String canReloaded;
    @SeleniumPath(value = MIN_PAYMENT_AMOUNT, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =MIN_PAYMENT_AMOUNT)
    private String min_payment_amount;
    @SeleniumPath(value = MAX_PAYMENT_AMOUNT, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =MAX_PAYMENT_AMOUNT)
    private String max_payment_amount;


    @SeleniumPath(value = VALID_FROM, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.DATE,readValue =VALID_FROM)
    private String validFrom;
    @SeleniumPath(value = VALID_UNTIL, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.DATE,readValue =VALID_UNTIL)
    private String validUntil;
    @SeleniumPath(value = DEACTIVATE_AFTER, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = DEACTIVATE_AFTER)
    private String deactivateAfterNum;
    @SeleniumPath(value = DEACTIVATE_AFTER_UNITES, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT, readValue = READ_DEACTIVATE_AFTER_UNITES)
    private TimeUnits deactivateAfterUnits;
    @SeleniumPath(value = ACTIVATE_AFTER, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =ACTIVATE_AFTER )
    private String activateAfterNum;
    @SeleniumPath(value = ACTIVATE_AFTER_UNITS, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT, readValue = READ_ACTIVATE_AFTER_UNITS)
    private TimeUnits activateAfterUnits;
    @SeleniumPath(value = TAGS, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = READ_TAGS)
    private ArrayList<String> tags;
    @SeleniumPath(value = SAVE, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE,readValue =SAVE)
    private String saveButton;

    public void setTitle(String title){
      this.title = title;
  }
    public String getTitle(){
      return title;
  }
    public void setDescription(String description){
        this.description = description;
    }
    public String getDescription(){
        return description;
    }
    public void setChooseImage(boolean choose){
        if(choose) chooseImage = "";
        else chooseImage = null;
    }
    public void setValue(String value){
        this.value = value;
    }
    public String getValue(){
        return value;
    }
    public void setCost(String cost){
        this.cost = cost;
    }
    public String getCost(){
        return cost;
    }
    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }
    public String getValidFrom() {
        return validFrom;
    }
    public void setValidUntil(String validUntil) {this.validUntil = validUntil; }
    public String getValidUntil() {
        return validUntil;
    }
    public String getNextTagsPath (int i){
        return  "";
    }

    public String getMin_payment_amount() {return min_payment_amount;}
    public void setMin_payment_amount(String min_payment_amount) {this.min_payment_amount = min_payment_amount;}
    public String getMax_payment_amount() {return max_payment_amount;}
    public void setMax_payment_amount(String max_payment_amount) {this.max_payment_amount = max_payment_amount;}
    public String getDeactivateAfterNum() {return deactivateAfterNum;}
    public void setDeactivateAfterNum(String deactivateAfterNum) {this.deactivateAfterNum = deactivateAfterNum;}
    public TimeUnits getDeactivateAfterUnits() {return deactivateAfterUnits;}
    public void setDeactivateAfterUnits(TimeUnits deactivateAfterUnits) {this.deactivateAfterUnits = deactivateAfterUnits;}
    public String getActivateAfterNum() {return activateAfterNum;}
    public void setActivateAfterNum(String activateAfterNum) {this.activateAfterNum = activateAfterNum;}
    public TimeUnits getActivateAfterUnits() {return activateAfterUnits;}
    public void setActivateAfterUnits(TimeUnits activateAfterUnits) {this.activateAfterUnits = activateAfterUnits;}
    public ArrayList<String> getTags() {return tags;}
    public void setTags(ArrayList<String> tags) {this.tags = tags;}

    public String getCanReloaded() {return canReloaded;}
    public void setCanReloaded(String canReloaded) {this.canReloaded = canReloaded; }

    @Override
    public void setNext(int i) {
    }

    @Override
    public int getNext() {
        return 0;
    }
    public int getNextTag () {
        return 0;
    }

    @Override
    public int getCreateActionCounter()
    {
        return 0;
    }
}
