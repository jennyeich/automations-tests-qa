package hub.common.objects.content.information;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 13/08/2017.
 */
public class CategoryPage extends BaseSeleniumObject {

    public static final String ADD_CATEGORY = "//a[contains(text(),\"Add Category\")]";
    public static final String TITLE = "title";
    public static final String CHOOSE_IMAGE = "//span[contains(text(),\"Choose Image\")]";
    public static final String SAVEBUTTON = "//button[@automationid=\"save\"]";

    int counter;

    @SeleniumPath(value = ADD_CATEGORY, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT, readValue = "")
    private String addCategory;
    @SeleniumPath(value = TITLE, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = TITLE)
    private String title;
    @SeleniumPath(value=CHOOSE_IMAGE,type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CHOOSE_IMAGE,readValue = CHOOSE_IMAGE)
    private String chooseImage;
    @SeleniumPath(value=SAVEBUTTON,type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE,readValue = "")
    private String saveButton;

    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}
    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle(){return this.title;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }



}

