package hub.common.objects.content.information;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.services.catalog.CatalogService;

import java.beans.IntrospectionException;

/**
 * Created by Goni on 13/08/2017.
 */
public class CatalogItemPage extends BaseSeleniumObject {

    public static final String CATALOGS_ADD_ITEM = "(//a[contains(text(),\"Add Item\")])[1]";
    public static final String CHOOSE_IMAGE = "//*[@id=\"editItem\"]/div/div[1]/div[1]";
    public static final String TITLE = "itemTitle";
    public static final String CATEGORY = "//select[@name=\"category_id\"]";
    public static final String SAVE_BUTTON = "//button[@automationid=\"save\"]";

    int counter;

    @SeleniumPath(value=CATALOGS_ADD_ITEM,type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK,readValue = "")
    private String addItem;
    @SeleniumPath(value=CHOOSE_IMAGE,type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CHOOSE_IMAGE,readValue = CHOOSE_IMAGE)
    private String chooseImage;
    @SeleniumPath(value = TITLE, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = TITLE)
    private String title;
    @SeleniumPath(value = CATEGORY, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = CATEGORY)
    private String category;
    @SeleniumPath(value=SAVE_BUTTON,type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT,readValue = "")
    private String saveButton;


    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public String getAddItem() {
        return addItem;
    }

    public void setAddItem(boolean isNew){
        if(isNew)
            addItem = "yes";
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getCategory() {
        return category;
    }
    public String getTitle() {
        return title;
    }

}
