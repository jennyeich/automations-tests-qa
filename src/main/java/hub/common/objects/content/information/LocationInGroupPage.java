package hub.common.objects.content.information;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 9/24/2017.
 */

public class LocationInGroupPage extends BaseSeleniumObject {

    public static final String ADD_LOCATION = "//a[contains(text(),\"Add Location\")]";
    public static final String ADDRESS = "branch_address";
    public static final String PHONE = "//*[@name=\"phone\"]";
    public static final String SAVE = "//button[contains(text(),\"Save\")]";

    @SeleniumPath(value = ADD_LOCATION, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = "")
    private String addLocation;
    @SeleniumPath(value = ADDRESS, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_KEY_TRIGGER, readValue = ADDRESS)
    private String address;
    @SeleniumPath(value = PHONE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = PHONE)
    private String phone;
    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = "")
    private String save;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

