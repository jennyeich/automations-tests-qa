package hub.common.objects.content.information;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 9/25/2017.
 */

public class BadgesPage extends BaseSeleniumObject {

    public static final String ADD_ITEM = "//a[contains(text(),\"Add Item\")]";
    public static final String TAG_NAME = "badgename";
    public static final String CHOOSE_IMAGE = "//*[@id=\"editItem\"]/div/div/div[1]";
    public static final String IMAGE_PATH = "//*[@id=\"editItem\"]/div/div/div[2]/div/ul/li[2]/img";
    public static final String SAVE = "//button[@automationid=\"save\"]";


    @SeleniumPath(value = ADD_ITEM, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = ADD_ITEM)
    private String addItem;
    @SeleniumPath(value = TAG_NAME, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = TAG_NAME)
    private String tagName;
    @SeleniumPath(value = CHOOSE_IMAGE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String chooseImage;
    @SeleniumPath(value = IMAGE_PATH, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String imagePath;
    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = "")
    private String save;

    public String getTagName() {
        return tagName;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

