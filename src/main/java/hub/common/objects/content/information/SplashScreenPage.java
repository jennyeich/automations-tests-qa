package hub.common.objects.content.information;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 9/25/2017.
 */

public class SplashScreenPage extends BaseSeleniumObject {

    public static final String ADD_ITEM = "//a[contains(text(),\"Add Splash Screen Ad\")]";
    public static final String TITLE = "txtTitle";
    public static final String CHOOSE_IMAGE = "//*[@id=\"gallery\"]/div[1]/div";
    public static final String IMAGE_PATH = "//*[@id=\"gallery\"]/div[2]/div/ul/li[2]/img";
    public static final String SAVE = "//button[@automationid=\"save\"]";

    @SeleniumPath(value = ADD_ITEM, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = ADD_ITEM)
    private String addItem;
    @SeleniumPath(value = TITLE, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = TITLE)
    private String title;
    @SeleniumPath(value = CHOOSE_IMAGE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String chooseImage;
    @SeleniumPath(value = IMAGE_PATH, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String imagePath;
    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = "")
    private String save;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

