package hub.common.objects.content.information;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 9/25/2017.
 */

public class CustomScreenPage extends BaseSeleniumObject {

    public static final String ADD_ITEM = "//a[contains(text(),\"Add Item\")]";
    public static final String ITEM_TITLE = "itemTitle";
    public static final String CHOOSE_IMAGE = "//*[@id=\"editItem\"]/div/div[1]/div[1]";
    public static final String IMAGE_PATH = "//*[@id=\"editItem\"]/div/div[1]/div[2]/div/ul/li[2]/img";
    public static final String BUTTON_ACTION = "action_name";
    public static final String SAVE = "//button[@automationid=\"save\"]";


    @SeleniumPath(value = ADD_ITEM, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = ADD_ITEM)
    private String addItem;
    @SeleniumPath(value = ITEM_TITLE, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = ITEM_TITLE)
    private String itemTitle;
    @SeleniumPath(value = CHOOSE_IMAGE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String chooseImage;
    @SeleniumPath(value = IMAGE_PATH, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String imagePath;
    @SeleniumPath(value = BUTTON_ACTION, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL)
    private String buttonAction;
    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = "")
    private String save;

    public String getItemTitle() {
        return itemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public String getButtonAction() {
        return buttonAction;
    }

    public void setButtonAction(String buttonAction) {
        this.buttonAction = buttonAction;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

