package hub.common.objects.content.information;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 9/24/2017.
 */

public class FilesPage extends BaseSeleniumObject {

    public static final String CHOOSE_FILE = "//input[@type=\"file\"]";

    @SeleniumPath(value = CHOOSE_FILE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.FILE, readValue = CHOOSE_FILE)
    private String file;

    public String getChooseFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

