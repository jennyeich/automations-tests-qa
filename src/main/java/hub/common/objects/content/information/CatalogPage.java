package hub.common.objects.content.information;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 13/08/2017.
 */
public class CatalogPage extends BaseSeleniumObject {
    //catalog
    public static final String ADD_CATALOG = "(//span[contains(text(),\"Add Item\")])[1]";
    public static final String TITLE = "title";
    public static final String SAVE_CATALOG = "//button[@automationid=\"save\"]";
    int counter;

    @SeleniumPath(value = ADD_CATALOG, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = "")
    private String addCatalog;
    @SeleniumPath(value = TITLE, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = TITLE)
    private String title;
    @SeleniumPath(value = SAVE_CATALOG, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = SAVE_CATALOG)
    private String saveCatalog;

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    private List<CategoryPage> categories = new ArrayList<>();

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    private List<CatalogItemPage> catalogItemPages = new ArrayList<>();

    public List<CatalogItemPage> getCatalogItemPages() {
        return catalogItemPages;
    }

    public void addCatalogItemPage(CatalogItemPage catalogItemPage){
        this.catalogItemPages.add(catalogItemPage);
    }
    public List<CategoryPage> getCategoriess() {
        return categories;
    }

    public void addCategoryPage(CategoryPage categoryPage){
        this.categories.add(categoryPage);
    }

    public String getNextTagsPath(int i) {
        return "";
    }

    public void setNext(int i) {
        this.counter = i;
    }

    public int getNext() {
        return counter;
    }

    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    public String getTitle() { return title; }


}


