package hub.common.objects.content.information;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 9/24/2017.
 */

public class LocationsPage extends BaseSeleniumObject {

    public static final String ADD_GROUP = "//a[contains(text(),\"Add Item\")]";
    public static final String GROUP_TITLE = "title";
    public static final String SAVE_GROUP = "//button[contains(text(),\"Save\")]";
    public static final String SELECT_GROUP = "//a[contains(text(),\"";


    @SeleniumPath(value = ADD_GROUP, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = ADD_GROUP)
    private String addGroup;
    @SeleniumPath(value = GROUP_TITLE, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = GROUP_TITLE)
    private String groupTitle;
    @SeleniumPath(value = SAVE_GROUP, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = "")
    private String save;
    @SeleniumPath(value = SELECT_GROUP, type = SeleniumPath.Type.BY_DYNAMIC_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = "")
    private String selectGroup;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "")
    List<LocationInGroupPage> locationsInGroup;

    public List<LocationInGroupPage> getLocationsInGroup() {
        if(locationsInGroup == null)
            locationsInGroup = new ArrayList<>();
        return locationsInGroup;
    }

    public void addLocationInGroup(LocationInGroupPage groupPages) {
        getLocationsInGroup().add(groupPages);
    }

    public String getGroupTitle() {
        return groupTitle;
    }

    public void setGroupTitle(String groupTitle) {
        this.groupTitle = groupTitle;
    }



    @Override
    public String getNextTagsPath(int i) {
        return getGroupTitle();
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

