package hub.common.objects.content;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.benefits.IAsset;

public class Template extends BaseSeleniumObject implements IAsset {

    public static final String TITLE = "title";
    public static final String SUBJECT = "//*[@automationid=\"subject\"]/textarea";
    public static final String IMAGE = "image";
    public static final String IMAGE_WITH_TEXT = "imageWithText";
    public static final String CODE = "code";
    public static final String CHOOSE_FILE = "uploadHtmlFile";
    public static final String CHOOSE_IMAGE = "//div[@automationid=\"images\"]/div[1]/div";
    public static final String IMAGE_PATH = "//li[@ng-repeat=\"image in images | limitTo:limit\"][1]";

    public static final String EMAIL_TITLE = "//*[@automationid=\"email-text-title\"]/textarea";
    public static final String EMAIL_TEXT = "//*[@automationid=\"email-text\"]/textarea";
    public static final String ADD_LINK = "link";
    public static final String MARKETING_EMAIL =  "marketingEmail";

    public static final String SAVE_CLOSE_BUTTON = "save";
    public static final String YES = "yes";


    @SeleniumPath(value = TITLE, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TITLE)
    private String title;
    @SeleniumPath(value = SUBJECT, type=SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.TEXT_AREA, readValue = SUBJECT)
    private String subject;
    @SeleniumPath(value = IMAGE, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String image;
    @SeleniumPath(value = IMAGE_WITH_TEXT, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String imageWithText;
    @SeleniumPath(value = CODE, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String code;
    @SeleniumPath(value = CHOOSE_IMAGE, type=SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String chooseImage;
    @SeleniumPath(value = IMAGE_PATH, type=SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String image_path;
    @SeleniumPath(value = CHOOSE_FILE, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.FILE)
    private String uploadFile;
    @SeleniumPath(value = ADD_LINK, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = ADD_LINK)
    private String addLinkToImage;


    @SeleniumPath(value = EMAIL_TITLE, type=SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = EMAIL_TITLE)
    private String emailTitle;
    @SeleniumPath(value = EMAIL_TEXT, type=SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = EMAIL_TEXT)
    private String emailText;
    @SeleniumPath(value = MARKETING_EMAIL, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String marketingEmail;
    @SeleniumPath(value = SAVE_CLOSE_BUTTON, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String saveButton;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void selectImageLayout() {
        this.image = YES;
        this.chooseImage = YES;
        this.image_path = YES;
        this.code = null;
        this.imageWithText = null;
        this.uploadFile = null;
        this.emailTitle = null;
        this.emailText = null;

    }

    public void selectImageWithTextLayout() {
        this.imageWithText = YES;
        this.chooseImage = YES;
        this.image_path = YES;
        this.code = null;
        this.uploadFile = uploadFile;
        this.image = null;
    }

    public void selectCodeLayout(String uploadFile) {
        this.code = YES;
        this.uploadFile = uploadFile;
        this.image = null;
        this.chooseImage = null;
        this.image_path = null;
        this.imageWithText = null;
        this.emailTitle = null;
        this.emailText = null;
    }

    public String getAddLinkToImage() {
        return addLinkToImage;
    }

    public void setAddLinkToImage(String addLinkToImage) {
        this.addLinkToImage = addLinkToImage;
    }

    public String getEmailTitle() {
        return emailTitle;
    }

    public void setEmailTitle(String emailTitle) {
        this.emailTitle = emailTitle;
    }

    public String getEmailText() {
        return emailText;
    }

    public void setEmailText(String emailText) {
        this.emailText = emailText;
    }

    public void setMarketingEmailOff() {
        this.marketingEmail = "OFF";
    }

    public void setSaveButton(String saveButton) {
        this.saveButton = saveButton;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {  }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

}
