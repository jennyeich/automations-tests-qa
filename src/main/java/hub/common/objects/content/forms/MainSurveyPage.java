package hub.common.objects.content.forms;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 6/18/2017.
 */
public class MainSurveyPage extends BaseSeleniumObject {


    public static final String SURVEY_TITLE = "//a[contains(text(),\"";
    public static final String SURVEY_URL = "//../div[@automationid=\"surveyFormUrl\"]";
    public static final String DELETE_ICON = "/../div[@class=\"block_actions\"]/a[2]";
    public static final String EXPORT_RESULTS = "/../button[contains(text(),\"Export Results\")]";
    public static final String RESULTS = "/../a[contains(text(),\"Results\")]";
    public static final String QUESTIONS = "/../a[contains(text(),\"Questions\")]";



    @SeleniumPath(value = SURVEY_TITLE, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = SURVEY_TITLE)
    private String surveyTitle;
    @SeleniumPath(value = "", type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.LABEL, readValue = SURVEY_URL)
    private String surveyUrl;
    @SeleniumPath(value = DELETE_ICON, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = "")
    private String deleteForm;
    @SeleniumPath(value = QUESTIONS, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = "")
    private String questions;

    int counter;

    public String getSurveyTitle() {
        return surveyTitle;
    }

    public void setSurveyTitle(String surveyTitle) {
        this.surveyTitle = surveyTitle;
    }

    public String getSurveyUrl() {
        return surveyUrl;
    }

    public void setSurveyUrl(String surveyUrl) {
        this.surveyUrl = surveyUrl;
    }


    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

}