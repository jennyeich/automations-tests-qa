package hub.common.objects.content.forms;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by taya.ashkenazi on 6/5/17.
 */
public class GeneralFormPage extends BaseSeleniumObject {


    public static final String FORM_TITLE = "//button[contains(text(),\"";
    public static final String FORM_URL = "//../div[@automationid=\"generalFormUrl\"]";
    public static final String DELETE_ICON = "/../div[@class=\"block_actions\"]/a[2]";
    public static final String EDIT_FIELDS = "/../a[contains(text(),\"Edit Fields\")]";
    public static final String EXPORT_RESULTS = "/../button[contains(text(),\"Export Results\")]";
    public static final String DOWNLOAD_RESULTS = "/../button[contains(text(),\"Download Results\")]";



    @SeleniumPath(value = FORM_TITLE, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = FORM_TITLE)
    private String formTitle;
    @SeleniumPath(value = "", type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.LABEL, readValue = FORM_URL)
    private String formUrl;
    @SeleniumPath(value = DELETE_ICON, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = "")
    private String deleteForm;
    @SeleniumPath(value = EDIT_FIELDS, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = "")
    private String editFields;

    int counter;

    public String getFormTitle() {
        return formTitle;
    }

    public void setFormTitle(String formTitle) {
        this.formTitle = formTitle;
    }

    public String getFormUrl() {
        return formUrl;
    }

    public void setFormUrl(String formUrl) {
        this.formUrl = formUrl;
    }


    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

}

