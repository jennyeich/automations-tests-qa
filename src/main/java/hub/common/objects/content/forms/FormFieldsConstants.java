package hub.common.objects.content.forms;

/**
 * Created by Goni on 6/12/2017.
 */
public class FormFieldsConstants {

    public static final String FIELD_TITLE_PATH = "//*[@id=\"container\"]/div/div/div/form/div/h3";
    public static final String FIELD_DESCRIPTION_PATH = "//div[@ng-bind=\"field.description\"]";
    public static final String FIELD_ANSWER_TYPE = "//div[@ng-if=\"field.type == '";
    public static final String FIELD_MANDATORY_PATH = "//span[contains(text(),\"*\")]";
    public static final String SUBMIT_BUTTON = "//button[@type=\"submit\"]";

}
