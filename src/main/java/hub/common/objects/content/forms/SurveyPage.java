package hub.common.objects.content.forms;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 6/18/2017.
 */
public class SurveyPage extends BaseSeleniumObject implements IForm{

    public static final String CLICK_ADD_NEW_SURVEY = "(//a[contains(text(),\"Add Item\")])[1]";
    public static final String TITLE = "itemTitle";
    public static final String DESCRIPTION = "description";
    public static final String THANK_YOU_MESSAGE_TITLE = "thankyou_title";
    public static final String THANK_YOU_MESSAGE_CONTENT = "thankyou_content";
    public static final String RECIPIENTS_OF_COMPLETED_FORMS = "notify_email";
    public static final String SAVE = "save";

    int counter;

    @SeleniumPath(value = CLICK_ADD_NEW_SURVEY, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT, readValue = "")
    private String addNewSurvey;
    @SeleniumPath(value = TITLE, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = TITLE)
    private String title;
    @SeleniumPath(value = DESCRIPTION, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = DESCRIPTION)
    private String description;
    @SeleniumPath(value = THANK_YOU_MESSAGE_TITLE, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = THANK_YOU_MESSAGE_TITLE)
    private String thankYouMessageTitle;
    @SeleniumPath(value = THANK_YOU_MESSAGE_CONTENT, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = THANK_YOU_MESSAGE_CONTENT)
    private String thankYouMessageContent;
    @SeleniumPath(value = RECIPIENTS_OF_COMPLETED_FORMS, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = RECIPIENTS_OF_COMPLETED_FORMS)
    private String recipientsOfCompletedForms;
    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT, readValue = "")
    private String save;

    public String getTitle() {return title;}
    public void setTitle(String title) {this.title = title;}
    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}
    public String getRecipientsOfCompletedForms() {return recipientsOfCompletedForms;}
    public void setRecipientsOfCompletedForms(String recipientsOfCompletedForms) {this.recipientsOfCompletedForms = recipientsOfCompletedForms;}
    public String getThankYouMessageTitle() {return thankYouMessageTitle;}
    public void setThankYouMessageTitle(String thankYouMessageTitle) {this.thankYouMessageTitle = thankYouMessageTitle;}
    public String getThankYouMessageContent() {return thankYouMessageContent;}
    public void setThankYouMessageContent(String thankYouMessageContent) {this.thankYouMessageContent = thankYouMessageContent;}

    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

}
