package hub.common.objects.content.forms;


/**
 * Created by Goni on 6/18/2017.
 */
public interface IForm {
    String getTitle();
    void setTitle(String title);
    String getDescription();
    void setDescription(String description);
    String getRecipientsOfCompletedForms();
    void setRecipientsOfCompletedForms(String recipientsOfCompletedForms);
    String getThankYouMessageTitle();
    void setThankYouMessageTitle(String thankYouMessageTitle);
    String getThankYouMessageContent();
    void setThankYouMessageContent(String thankYouMessageContent);

}
