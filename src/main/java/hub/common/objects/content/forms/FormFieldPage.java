package hub.common.objects.content.forms;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.common.FormFieldAnswerType;
import hub.common.objects.common.YesNoList;

/**
 * Created by Goni on 6/12/2017.
 */
public class FormFieldPage extends BaseSeleniumObject {

    public static final String CLICK_ADD_NEW_ITEM = "(//a[contains(text(),\"Add Item\")])[1]";
    public static final String TITLE = "itemTitle";
    public static final String DESCRIPTION = "description";
    public static final String MANDATORY = "//select[@data-ng-model=\"formFieldsEditCtrl.item.required\"]";
    public static final String ANSWER_TYPE = "field_type";
    public static final String DEFAULT_ANSWER = "default_form_field_type_id";
    public static final String SAVE = "//button[contains(text(),\"Save\")]";
    public static final String SAVE_ITEM_ORDER = "//button[contains(text(),\"Save Item Order\")]";


    public static final String TEXT = "text";
    public static final String CHECKBOX = "checkbox";
    public static final String MULTI_SELECT_ONE_CHOICE = "buttons";
    public static final String MULT_SELECT = "multi-select";
    public static final String TEXT_BOX = "textarea";
    public static final String TIME = "time";
    public static final String DATE = "date";

    @SeleniumPath(value = CLICK_ADD_NEW_ITEM, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT, readValue = "")
    private String addNewField;
    @SeleniumPath(value = TITLE, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = TITLE)
    private String title;
    @SeleniumPath(value = DESCRIPTION, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = DESCRIPTION)
    private String description;
    @SeleniumPath(value = MANDATORY, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT, readValue = MANDATORY)
    private String isMandatory;
    @SeleniumPath(value = ANSWER_TYPE, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = ANSWER_TYPE)
    private String answerType;
    @SeleniumPath(value = DEFAULT_ANSWER, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = DEFAULT_ANSWER)
    private String defaultAnswer;
    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT, readValue = "")
    private String save;
    @SeleniumPath(value = SAVE_ITEM_ORDER, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT, readValue = "")
    private String saveItemOrder;


    int counter;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIsMandatory() {
        if(YesNoList.YES.toString().equals(isMandatory))
            return "*";
        else
            return "";
    }

    public void setIsMandatory(String isMandatory) {
        this.isMandatory = isMandatory;
    }


    public String getAnswerType() {
       if(FormFieldAnswerType.FREE_TEXT_LINE.toString().equals(answerType)){
           answerType = TEXT;
       }else if(FormFieldAnswerType.CHECK_BOX.toString().equals(answerType)){
           answerType = CHECKBOX;
       }else if(FormFieldAnswerType.MULTIPLE_ANSWERS.toString().equals(answerType)){
           answerType = MULT_SELECT;
       }else if(FormFieldAnswerType.MULTIPLE_CHOICE_ONE_ANSWER.toString().equals(answerType)){
           answerType = MULTI_SELECT_ONE_CHOICE;
       }else if(FormFieldAnswerType.DATE.toString().equals(answerType)){
           answerType = DATE;
       }else if(FormFieldAnswerType.TIME.toString().equals(answerType)){
           answerType = TIME;
       }else if(FormFieldAnswerType.FREE_TEXT_BOX.toString().equals(answerType)){
           answerType = TEXT_BOX;
       }
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    public String getDefaultAnswer() {
        return defaultAnswer;
    }

    public void setDefaultAnswer(String defaultAnswer) {
        this.defaultAnswer = defaultAnswer;
    }

    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
