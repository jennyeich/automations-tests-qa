package hub.common.objects.content.branding;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 9/25/2017.
 */

public class DesignPage extends BaseSeleniumObject {

    public static final String NAV_TYPE_USE_SIDE_MENU = "//div[@action=\"sideMenu\"]/s3-img/img";
    public static final String NAV_TYPE_REGULAR = "//*[@id=\"main\"]/div/div[2]/div[2]/div[2]/s3-img/img";
    public static final String SAVE = "//button[@automationid=\"save\"]";

    @SeleniumPath(value = NAV_TYPE_USE_SIDE_MENU, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String useSideMenu;
    @SeleniumPath(value = NAV_TYPE_REGULAR, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String useRegular;
    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String save;

    public String getUseSideMenu() {
        return useSideMenu;
    }

    public void setUseSideMenu(String useSideMenu) {
        this.useSideMenu = useSideMenu;
    }

    public String getUseRegular() {
        return useRegular;
    }

    public void setUseRegular(String useRegular) {
        this.useRegular = useRegular;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

