package hub.common.objects.operations;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

public class FieldOption extends BaseSeleniumObject {

    public static final String ADD_OPTIONS = "//*[@automationid=\"add_option\"]";
    public static final String OPTION_TEXT = "//*[@automationid=\"option_label_{0}\"]";
    public static final String OPTION_VALUE = "//*[@automationid=\"option_value_{0}\"]";



    @SeleniumPath(value = ADD_OPTIONS, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addOptions;
    @SeleniumPath(value = OPTION_TEXT, type=SeleniumPath.Type.BY_DYNAMIC_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = OPTION_TEXT)
    private String optionText;
    @SeleniumPath(value = OPTION_VALUE, type=SeleniumPath.Type.BY_DYNAMIC_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = OPTION_VALUE)
    private String optionValue;

    private int index = 0;


    public String getAddOptions() {
        return addOptions;
    }

    public void clickAddOptions() {
        this.addOptions = "add option";
    }

    public void addOption(String optionText,String optionValue){
        this.addOptions = "add option";
        this.optionText = optionText;
        this.optionValue = optionValue;

    }

    public String getOptionText() {
        return optionText;
    }

    public void setOptionText(String optionText) {
        this.optionText = optionText;
    }

    public String getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(String optionValue) {
        this.optionValue = optionValue;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {
        this.index = i;
    }

    @Override
    public int getNext() {
        return index;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
