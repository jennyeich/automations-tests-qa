package hub.common.objects.operations;

/**
 * Created by Jenny on 11/6/2016.
 */
public enum CountryCode_APP_SETTINGS {

    UK("United Kingdom - 44"),
    US("United States - 1"),
    IL("Israel - 972"),
    Can("Canada - 1"),
    BR("Brazil - 55"),
    CL("Chile - 56"),
    CO("Colombia - 57"),
    DK("Denmark - 45"),
    IT("Italy - 39"),
    KZ("Kazakstan - 7"),
    MX("Mexico - 52"),
    PA("Panama - 507"),
    ZA("South Africa - 27"),
    SE("Sweden - 46"),
    TR("Turkey - 90"),
    AE("United Arab Emirates - 971"),
    ;

    private final String text;


    CountryCode_APP_SETTINGS(final String text) {
        this.text = text;

    }

    @Override
    public String toString() {
        return text;
    }

}
