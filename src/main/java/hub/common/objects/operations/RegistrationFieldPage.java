package hub.common.objects.operations;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 3/27/2017.
 */
public class RegistrationFieldPage  extends BaseSeleniumObject {

    public static final String ADD_FIELD = "//a[contains(text(),'Add Field')]";
    public static final String FIELD_TYPE = "field_type_id";
    public static final String FIELD_TITLE = "title";
    public static final String OPTIONAL_DESCRIPTION = "description";
    public static final String EXTERNAL_REGISTRATION_FIELD_NAME = "external_name";
    public static final String SHOW_IN_UPDATE_FORM = "allow_update";
    public static final String SHOW_ON_REGISTRATION_FORM = "show_on_register";
    public static final String REQUIRED = "required";
    public static final String DEFAULT_VALUE = "value";
    public static final String DISABLE_FIELD_UPDATES = "disable_update";
    public static final String MIN_NO_OF_CHARACTERS = "registerForm_input_minlength";
    public static final String MAX_NO_OF_CHARACTERS = "registerForm_input_maxlength";
    public static final String SAVE = "//button[contains(text(),'Save')]";

    @SeleniumPath(value = ADD_FIELD, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT,readValue = ADD_FIELD)
    private String addField;
    @SeleniumPath(value = FIELD_TYPE, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = FIELD_TYPE)
    private String fieldType;
    @SeleniumPath(value = FIELD_TITLE, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = FIELD_TITLE)
    private String fieldTitle;

    @SeleniumPath(value = OPTIONAL_DESCRIPTION, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = OPTIONAL_DESCRIPTION)
    private String optionalDescription;
    @SeleniumPath(value = EXTERNAL_REGISTRATION_FIELD_NAME, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = EXTERNAL_REGISTRATION_FIELD_NAME)
    private String externalRegistrationFieldName;
    @SeleniumPath(value = SHOW_IN_UPDATE_FORM, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = SHOW_IN_UPDATE_FORM)
    private String showInUpdateForm;
    @SeleniumPath(value = SHOW_ON_REGISTRATION_FORM, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = SHOW_ON_REGISTRATION_FORM)
    private String showOnRegistrationForm;
    @SeleniumPath(value = REQUIRED, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT,readValue = REQUIRED)
    private String required;
    @SeleniumPath(value = DEFAULT_VALUE, type=SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT, readValue = DEFAULT_VALUE)
    private String defaultValue;
    @SeleniumPath(value = DISABLE_FIELD_UPDATES, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = DISABLE_FIELD_UPDATES)
    private String disableFieldUpdates;
    @SeleniumPath(value = MIN_NO_OF_CHARACTERS, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = MIN_NO_OF_CHARACTERS)
    private String minNoOfCharacters;
    @SeleniumPath(value = MAX_NO_OF_CHARACTERS, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = MAX_NO_OF_CHARACTERS)
    private String maxNoOfCharacters;

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    private List<FieldOption> fieldOptionList;

    @SeleniumPath(value = SAVE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT,readValue = SAVE)
    private String save;

    public String getFieldType() {
        return fieldType;
    }

    public List<FieldOption> getFieldOptionList() {
        if(fieldOptionList == null)
            fieldOptionList = new ArrayList<>();
        return fieldOptionList;
    }

    public void addFieldOption(FieldOption fieldOption) {
        getFieldOptionList().add(fieldOption);
        fieldOption.setNext(getFieldOptionList().size()-1);

    }

    public void setFieldOptionList(List<FieldOption> fieldOptionList) {
        this.fieldOptionList = fieldOptionList;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getFieldTitle() {
        return fieldTitle;
    }

    public void setFieldTitle(String fieldTitle) {
        this.fieldTitle = fieldTitle;
    }

    public String getOptionalDescription() {
        return optionalDescription;
    }

    public void setOptionalDescription(String optionalDescription) {
        this.optionalDescription = optionalDescription;
    }

    public String getExternalRegistrationFieldName() {
        return externalRegistrationFieldName;
    }

    public void setExternalRegistrationFieldName(String externalRegistrationFieldName) {
        this.externalRegistrationFieldName = externalRegistrationFieldName;
    }

    public String getShowInUpdateForm() {
        return showInUpdateForm;
    }

    public void setShowInUpdateForm(String showInUpdateForm) {
        this.showInUpdateForm = showInUpdateForm;
    }

    public String getShowOnRegistrationForm() {
        return showOnRegistrationForm;
    }

    public void setShowOnRegistrationForm(String showOnRegistrationForm) {
        this.showOnRegistrationForm = showOnRegistrationForm;
    }

    public String getRequired() {
        return required;
    }

    public void setRequired(String required) {
        this.required = required;
    }

    public String getDefaultValue() { return defaultValue; }

    public void setDefaultValue(String defaultValue) { this.defaultValue = defaultValue; }

    public String getDisableFieldUpdates() {
        return disableFieldUpdates;
    }

    public void setDisableFieldUpdates(String disableFieldUpdates) {
        this.disableFieldUpdates = disableFieldUpdates;
    }

    public String getMinNoOfCharacters() {
        return minNoOfCharacters;
    }

    public void setMinNoOfCharacters(String minNoOfCharacters) {
        this.minNoOfCharacters = minNoOfCharacters;
    }

    public String getMaxNoOfCharacters() {
        return maxNoOfCharacters;
    }

    public void setMaxNoOfCharacters(String maxNoOfCharacters) {
        this.maxNoOfCharacters = maxNoOfCharacters;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}


