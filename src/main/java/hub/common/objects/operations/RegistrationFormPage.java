package hub.common.objects.operations;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 3/26/2017.
 */
public class RegistrationFormPage extends BaseSeleniumObject {


    public static final String EXTERNAL_CLUB =  "//*[@ng-model=\"infoData.externalClub\"]";
    public static final String SAVE_1 = "(//button[contains(text(),'Save')])[1]";
    public static final String SAVE_2 = "(//button[contains(text(),'Save')])[2]";


    @SeleniumPath(value = EXTERNAL_CLUB, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT,readValue = EXTERNAL_CLUB)
    private String externalClubYesNo;

    @SeleniumPath(value = SAVE_1, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT,readValue = SAVE_1)
    private String save1;

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    List<RegistrationFieldPage> fields;

    @SeleniumPath(value = SAVE_2, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT,readValue = SAVE_2)
    private String save2;


    public String getExternalClubYesNo() {
        return externalClubYesNo;
    }

    public void setExternalClubYesNo(String externalClubYesNo) {
        this.externalClubYesNo = externalClubYesNo;
    }

    public void addField(RegistrationFieldPage field) {
        if(fields == null){
            fields = new ArrayList<RegistrationFieldPage>();
        }
        fields.add(field);
    }

    public List<RegistrationFieldPage> getFields(){
        return fields;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }


}


