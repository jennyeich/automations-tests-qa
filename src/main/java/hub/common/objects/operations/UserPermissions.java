package hub.common.objects.operations;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import org.openqa.selenium.Keys;

import java.text.MessageFormat;

/**
 * Created by lior on 7/4/17.
 */
public class UserPermissions extends BaseSeleniumObject {


    public static final String USER_PERMISSION_TAGS = "//div[@automationid='dropdownUserPermissionsTags']";
    public static final String TAG_SEARCH_BOX = "//div[@automationid='dropdownUserPermissionsTags']/div//input";

    public static final String USER_PERMISSION_PERMISSION_LEVEL = "//div[@selected-item=\"new_perm_level\"]";
    public static final String PERMISSION_LEVEL_ALLOW_OR_DISALLOW = "//ul[@id=\"ui-select-choices-1\"]//div/span[contains(text(), \"{0}\")]";
    public static final String USER_PERMISSION_ADD_BUTTON = "(//button[@class='submitButton ng-scope' and contains(text(), 'Add')])[1]";
    public static final String UPDATE_USER_SAVE_BUTTON = "//button[@class='submitButton button ng-scope' and contains(text(), 'Save')]";

    //Permission tags
    public static final String FIND_USERS_FIELD_DELETE = "//div[text()[starts-with(.,'findUsers.field - ')]]/a";
    public static final String FIND_USERS_SECTION_DELETE = "//div[text()[starts-with(.,'findUsers.section - ')]]/a";
    public static final String DASHBOARD_WIDGET_DELETE = "//div[text()[starts-with(.,'dashboard.widget - ')]]/a";
    public static final String HOME_SCREEN_LAYOUT_DELET = "//div[text()[starts-with(.,'layout.MainLocationTC - ')]]/a";
    public static final String SMART_ACTIONS_LAYOUT_DELET = "//div[text()[starts-with(.,'smartAction.actions - ')]]/a";
    public static final String LAYOUT_MAIN_LOCATION_TC_DELETE = "//div[text()[starts-with(.,'layout.MainLocationTC - ')]]/a";
    public static final String EMAIL_TEMPLATE_PAGE_DELETE = "//div[text()[contains(.,'email-templates')]]/a";


    public static final String HUB2_TEMPLATE_PRODUCT = "//div[text()[contains(.,'Hub2_product_template')]]/a";


    @SeleniumPath(value = USER_PERMISSION_TAGS, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = USER_PERMISSION_TAGS)
    private String userPermissionTags;

    @SeleniumPath(value = TAG_SEARCH_BOX, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TAG_SEARCH_BOX)
    private String tagSearchBox;

    @SeleniumPath(value = USER_PERMISSION_PERMISSION_LEVEL, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = USER_PERMISSION_PERMISSION_LEVEL)
    private String userPermissionPermissionLevel;

    @SeleniumPath(value = PERMISSION_LEVEL_ALLOW_OR_DISALLOW, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT2_THROUGH_LABEL, readValue = PERMISSION_LEVEL_ALLOW_OR_DISALLOW)
    private String permissionLevelAllowOrDisallow;

    @SeleniumPath(value = USER_PERMISSION_ADD_BUTTON, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = USER_PERMISSION_ADD_BUTTON)
    private String userPermissionAddButton;

    /*@SeleniumPath(value = UPDATE_USER_SAVE_BUTTON, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = UPDATE_USER_SAVE_BUTTON)
    private String updateUserSaveButton;*/





    public String getUserPermissionTags() {
        return userPermissionTags;
    }

    public void setUserPermissionTags(String userPermissionTags) {
        this.userPermissionTags = userPermissionTags;
    }

    public String getUserPermissionPermissionLevel() {
        return userPermissionPermissionLevel;
    }

    public void setUserPermissionPermissionLevel(String userPermissionPermissionLevel) {
        this.userPermissionPermissionLevel = userPermissionPermissionLevel;
    }

    public String getTagSearchBox() {
        return tagSearchBox;
    }

    public void setTagSearchBox(String tagSearchBox) {
        this.tagSearchBox = tagSearchBox + Keys.ENTER;
    }

    public String getPermissionLevelAllowOrDisallow() {

        return permissionLevelAllowOrDisallow;
    }

    public void setPermissionLevelAllowOrDisallow(String permissionLevelAllowOrDisallow) {
        this.permissionLevelAllowOrDisallow = MessageFormat.format(PERMISSION_LEVEL_ALLOW_OR_DISALLOW, permissionLevelAllowOrDisallow);
    }

    public String getUserPermissionAddButton() {
        return userPermissionAddButton;
    }

    public void setUserPermissionAddButton(String userPermissionAddButton) {
        this.userPermissionAddButton = userPermissionAddButton;
    }

    /*public String getUpdateUserSaveButton() {
        return updateUserSaveButton;
    }

    public void setUpdateUserSaveButton(String updateUserSaveButton) {
        this.updateUserSaveButton = updateUserSaveButton;
    }*/

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
