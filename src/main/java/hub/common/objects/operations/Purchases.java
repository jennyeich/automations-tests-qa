package hub.common.objects.operations;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Jenny on 7/20/2017.
 */
public class Purchases extends BaseSeleniumObject{


    public static final String SAVE_PURCHASES_OPTIONS = "SaveAnonymousPurchase";


    @SeleniumPath(value = SAVE_PURCHASES_OPTIONS, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = SAVE_PURCHASES_OPTIONS)
    private String getSavePurchasesOptions;

    public String getSavePurchasesOptions() {return getSavePurchasesOptions;}
    public void setSavePurchasesOptions(String getSavePurchasesOptions) {this.getSavePurchasesOptions = getSavePurchasesOptions;}

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    public void setNext(int counter) {}
    public int getNext() {return 0;}

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
