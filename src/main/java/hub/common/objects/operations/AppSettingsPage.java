package hub.common.objects.operations;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 4/30/2017.
 */
public class AppSettingsPage extends BaseSeleniumObject {

    public static final String COUNTRY_CODE = "//*[@id=\"short_name\"]/select";
    public static final String SAVE = "//button[contains(text(),'Save')]";
    public static final String VERTICAL =  "sf_vertical";


    MemberIdentificationAndRegistration memberIdentificationAndRegistration;

    @SeleniumPath(value = COUNTRY_CODE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = COUNTRY_CODE)
    private String countryCode;
    @SeleniumPath(value = VERTICAL, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = VERTICAL)
    private String vertical;
    @SeleniumPath(value = SAVE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT)
    private String save;

    public MemberIdentificationAndRegistration getMemberIdentificationAndRegistration() {return memberIdentificationAndRegistration;}
    public void setMemberIdentificationAndRegistration(MemberIdentificationAndRegistration memberIdentificationAndRegistration) {this.memberIdentificationAndRegistration = memberIdentificationAndRegistration;}

    public String getVertical() {
        return vertical;
    }

    public void setVertical(String vertical) {
        this.vertical = vertical;
    }
    public String getCountryCode() { return countryCode; }
    public void setCountryCode(String countryCode) { this.countryCode = countryCode; }


    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
