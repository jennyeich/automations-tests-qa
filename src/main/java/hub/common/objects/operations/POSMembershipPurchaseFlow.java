package hub.common.objects.operations;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

public class POSMembershipPurchaseFlow extends BaseSeleniumObject {

    public static final String ALLOW_POS_REGISTRATION = "AllowPOSRegistration";
    public static final String ADD_MEMBERSHIP_ITEMCODE = "//i[@ng-click=\"ctrlPs.addJoiningItemCode();\"]";
    public static final String REG_ITEM_CODE = "SendCodeFromTagBySMS_ItemCode";

    @SeleniumPath(value = ALLOW_POS_REGISTRATION, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT,readValue = ALLOW_POS_REGISTRATION)
    private String allowPosRegistration;
    @SeleniumPath(value = ADD_MEMBERSHIP_ITEMCODE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addMembershipItemcode;
    @SeleniumPath(value = REG_ITEM_CODE, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = ALLOW_POS_REGISTRATION)
    private String regItemCode;


    public String getAllowPosRegistration() {
        return allowPosRegistration;
    }

    public void setAllowPosRegistration(String allowPosRegistration) {
        this.allowPosRegistration = allowPosRegistration;
        if(this.allowPosRegistration.equals(PurchaseRegistrationOptions.AUTO_REGISTER.toString()))
            addMembershipItemcode = "click addMembershipItemcode";
    }

    public String getRegItemCode() {
        return regItemCode;
    }

    public void setRegItemCode(String regItemCode) {
        this.regItemCode = regItemCode;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
