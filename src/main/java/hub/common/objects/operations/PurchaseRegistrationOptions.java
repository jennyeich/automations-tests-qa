package hub.common.objects.operations;

public enum PurchaseRegistrationOptions {

    NONE("string:None"),
    SEND_CODES_BY_SMS("string:SendJoiningCode"),
    AUTO_REGISTER("string:AutoRegister")
    ;

    private final String text;


    PurchaseRegistrationOptions(final String text) {
        this.text = text;

    }

    @Override
    public String toString() {
        return text;
    }
}
