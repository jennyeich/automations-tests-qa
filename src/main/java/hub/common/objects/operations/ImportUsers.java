package hub.common.objects.operations;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 9/12/2017.
 */

public class ImportUsers extends BaseSeleniumObject {


    public static final String UPLOAD_FILE = "uploadCsvFile";
    public static final String IDENTIFIER = "identifier";
    public static final String IMPORT_BUTTON ="import_button";


    @SeleniumPath(value = IDENTIFIER, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = IDENTIFIER)
    private String identifier;
    @SeleniumPath(value = UPLOAD_FILE, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.FILE, readValue = UPLOAD_FILE)
    private String uploadFile;
    @SeleniumPath(value = IMPORT_BUTTON, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = IMPORT_BUTTON)
    private String importBtn;



    public String getUploadFile() {
        return uploadFile;
    }

    public void setUploadFile(String uploadFile) {
        this.uploadFile = uploadFile;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

