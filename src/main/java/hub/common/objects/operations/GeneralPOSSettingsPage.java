package hub.common.objects.operations;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.util.ArrayList;

/**
 * Created by Goni on 3/23/2017.
 */
public class GeneralPOSSettingsPage extends BaseSeleniumObject{

    public static final String API_KEY = "//*[@automationid=\"apiKey_value\"]";
    public static final String ALLOWMULTIPURCHASES =  "AllowMultiplePurchasesForPOSIdentifiers";
    public static final String AUTOMATION_DELAY_TIME = "AutomationDelayMS";
    public static final String SAVE = "//button[contains(text(),'Save')]";


    private int counter=1;

    @SeleniumPath(value = API_KEY, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = API_KEY)
    private String apiKey;
    @SeleniumPath(value = ALLOWMULTIPURCHASES, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT,readValue = ALLOWMULTIPURCHASES)
    private String allowMultiPurchasesYesNo;
    @SeleniumPath(value = AUTOMATION_DELAY_TIME, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = AUTOMATION_DELAY_TIME)
    private String delayTime;

    Purchases purchases;
    POSMembershipPurchaseFlow purchaseFlow;

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    ArrayList<MemberNotes> memberNotes;
    @SeleniumPath(value = SAVE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT,readValue = SAVE)
    private String save;

    public String getDelayTime() {
        return delayTime;
    }
    public void setDelayTime(String delayTime) {
        this.delayTime = delayTime;
    }
    public String getAllowMultiPurchasesYesNo() {
        return allowMultiPurchasesYesNo;
    }
    public void setAllowMultiPurchasesYesNo(String allowMultiPurchases) {this.allowMultiPurchasesYesNo = allowMultiPurchases;}
    public String getApiKey() {
        return apiKey;
    }
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }


    public void setMemberNotesCount(int i) {counter = i;}
    public int getMemberNotesCount() {return counter;}
    public Purchases getPurchases() {return purchases;}
    public void setPurchases(Purchases purchases) {this.purchases = purchases;}

    public POSMembershipPurchaseFlow getPurchaseFlow() {
        return purchaseFlow;
    }

    public void setPurchaseFlow(POSMembershipPurchaseFlow purchaseFlow) {
        this.purchaseFlow = purchaseFlow;
    }

    public void addMemberNotes(MemberNotes memberNote, GeneralPOSSettingsPage posSettingsPage){

        memberNote.setNext(posSettingsPage.getMemberNotesCount());
        getMemberNotes().add(memberNote);
        //condition.setCreateActionCounter(this.getNext()+1);
        posSettingsPage.setMemberNotesCount(posSettingsPage.getMemberNotesCount()+1);

    }

    private ArrayList<MemberNotes> getMemberNotes() {
        if(memberNotes == null)
            memberNotes = new ArrayList<>();
        return memberNotes;
    }

    public void setMemberNotes(ArrayList<MemberNotes> memberNotes) {this.memberNotes = memberNotes;}

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {}
    @Override
    public int getNext() {
        return 0;
    }
    @Override
    public int getNextTag() {
        return 0;
    }
    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
