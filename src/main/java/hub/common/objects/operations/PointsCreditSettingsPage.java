package hub.common.objects.operations;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 3/26/2017.
 */
public class PointsCreditSettingsPage extends BaseSeleniumObject {

    private static final String BUSINESS_WALLET = "businessWallet";
    public static final String ACCUMULATION_VERSION =  "//*[@name=\"AccumulationVersion\"]";
    public static final String PAY_WITH_VERIFICATION_CODE = "//*[@id='PaymentRequiresVerificationCode']";
    public static final String PAY_WITH_BUDGET_RATIO = "PayWithBudgetRatio";
    public static final String SHOW_CURRENCY_WITH_DECIMAL = "decimals_amount";
    public static final String MEMBER_WALLET_PAY_WITH_CREDIT = "PayWithBudgetType";
    public static final String SAVE = "//button[contains(text(),'Save')]";

    @SeleniumPath(value = BUSINESS_WALLET, type=SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL ,readValue = BUSINESS_WALLET)
    private String businessWallet;
    @SeleniumPath(value = ACCUMULATION_VERSION, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT,readValue = ACCUMULATION_VERSION)
    private String accumulationVersion;
    @SeleniumPath(value = SHOW_CURRENCY_WITH_DECIMAL, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT,readValue = SHOW_CURRENCY_WITH_DECIMAL)
    private String showCurrencyWithDecimal;
    @SeleniumPath(value = PAY_WITH_BUDGET_RATIO, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = PAY_WITH_BUDGET_RATIO)
    private String payWithBudgetRatio;
    @SeleniumPath(value = PAY_WITH_VERIFICATION_CODE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT,readValue = PAY_WITH_VERIFICATION_CODE)
    private String PaymentRequiresVerificationCodeYesNo;
    @SeleniumPath(value = MEMBER_WALLET_PAY_WITH_CREDIT, type=SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL ,readValue = MEMBER_WALLET_PAY_WITH_CREDIT)
    private String payWithCredit;
    @SeleniumPath(value = SAVE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT,readValue = SAVE)
    private String save;

    public enum BusinessWallet {
        //RDS_value(ui label)
        Points("string:Points"),
        Budget("string:Budget"),
        Both("string:Both");

        private String wallet;
        BusinessWallet(String wallet) {
            this.wallet = wallet;
        }

        public String dbValue(){return this.name();}

        @Override
        public String toString() {return wallet; }
    }




    public String getBusinessWallet() {
        return businessWallet;
    }

    public void setBusinessWallet(String businessWallet) {
        this.businessWallet = businessWallet;
    }


    public String getShowCurrencyWithDecimal() {
        return showCurrencyWithDecimal;
    }

    public void setShowCurrencyWithDecimal(String showCurrencyWithDecimal) {
        this.showCurrencyWithDecimal = showCurrencyWithDecimal;
    }

    public String getPayWithBudgetRatio() {
        return payWithBudgetRatio;
    }

    public void setPayWithBudgetRatio(String payWithBudgetRatio) {
        this.payWithBudgetRatio = payWithBudgetRatio;
    }

    public String getAccumulationVersion() {
        return accumulationVersion;
    }

    public void setAccumulationVersion(String accumulationVersion) {
        this.accumulationVersion = accumulationVersion;
    }

    public String getPaymentRequiresVerificationCodeYesNo() {
        return PaymentRequiresVerificationCodeYesNo;
    }

    public void setPaymentRequiresVerificationCodeYesNo(String paymentRequiresVerificationCodeYesNo) {
        PaymentRequiresVerificationCodeYesNo = paymentRequiresVerificationCodeYesNo;
    }

    public String getPayWithCredit() {
        return payWithCredit;
    }

    public void setPayWithCredit(String payWithCredit) {
        this.payWithCredit = payWithCredit;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

