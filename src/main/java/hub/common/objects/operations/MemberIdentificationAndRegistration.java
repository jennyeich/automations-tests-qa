package hub.common.objects.operations;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Jenny on 7/11/2017.
 */
public class MemberIdentificationAndRegistration extends BaseSeleniumObject {


    public static final String moveFromMemberIdentificationToUnique = "//*[@ng-click=\"moveIntoUniqueFields()\"]";
    public static final String moveFromUniqueFiledsToPOS = "//*[@ng-click=\"moveIntoFieldsForID()\"]";
    public static final String CUSTOM_FIELD_FOR_POS_IDENTIFICATION = "customFieldForIdentification";

    @SeleniumPath(value = "", type=SeleniumPath.Type.BY_XPATH2,elementInputType = SeleniumPath.ElementInputType.SELECT_FROM_OPEN_FILED,readValue = "")
    private String membershipFields;
    @SeleniumPath(value = moveFromMemberIdentificationToUnique, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE,readValue = "")
    private String moveFromMemberToUnique;
    @SeleniumPath(value = "", type=SeleniumPath.Type.BY_XPATH2,elementInputType = SeleniumPath.ElementInputType.SELECT_FROM_OPEN_FILED,readValue = "")
    private String uniqueFields;
    @SeleniumPath(value =moveFromUniqueFiledsToPOS , type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE,readValue = "")
    private String moveFromUniqueToPOSFields;
    @SeleniumPath(value = "", type=SeleniumPath.Type.BY_XPATH2,elementInputType = SeleniumPath.ElementInputType.SELECT_FROM_OPEN_FILED,readValue = "")
    private String fieldsForPOSIdentification;
    @SeleniumPath(value =CUSTOM_FIELD_FOR_POS_IDENTIFICATION , type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = "")
    private String customFieldForPOSIdentification;

    public String getMembershipFields() {return membershipFields;}
    public void setMembershipFields(String membershipFields) {this.membershipFields = "//*[@automationid=\"dropDownMembershipFields\"]/option[@label=\"" + membershipFields + "\"]";}
    public String getUniqueFields() {return uniqueFields;}
    public void setUniqueFields(String uniqueFields) {this.uniqueFields = "//*[@automationid=\"dropDownUniqueFields\"]/option[@label=\"" + uniqueFields + "\"]";}
    public String getFieldsForPOSIdentification() {return fieldsForPOSIdentification;}
    public void setFieldsForPOSIdentification(String fieldsForPOSIdentification) {this.fieldsForPOSIdentification = "//*[@automationid=\"dropDownPOSFields\"]/option[@label=\"" + fieldsForPOSIdentification + "\"]";}
    public String getMoveFromMemberToUnique() {return moveFromMemberToUnique;}
    public void setMoveFromMemberToUnique(String moveFromMemberToUnique) {this.moveFromMemberToUnique = moveFromMemberToUnique;}
    public String getMoveFromUniqueToPOSFields() {return moveFromUniqueToPOSFields;}
    public void setMoveFromUniqueToPOSFields(String moveFromUniqueToPOSFields) {this.moveFromUniqueToPOSFields = moveFromUniqueToPOSFields;}
    public String getCustomFieldForPOSIdentification() {return customFieldForPOSIdentification;}
    public void setCustomFieldForPOSIdentification(String customFieldForPOSIdentification) {this.customFieldForPOSIdentification = customFieldForPOSIdentification;}


    @Override
    public String getNextTagsPath(int i) {
        return null;
    }
    @Override
    public void setNext(int i) {}
    @Override
    public int getNext() {
        return 0;
    }
    @Override
    public int getNextTag() {
        return 0;
    }
    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
