package hub.common.objects.operations;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Jenny on 7/20/2017.
 */
public class MemberNotes extends BaseSeleniumObject{

    public static final String ADD_MEMBER_NOTE = "//*[@ng-click=\"ctrlPs.addMemberNote();\"]";
    public static final String MEMBER_NOTES = "(//*[@id=\"notes\"])";
    public static final String TARGET = "(//*[@id=\"target_type\"])";
    public static final String TYPE = "(//*[@id=\"notes_type\"])";

    private int counter;
    @SeleniumPath(value = ADD_MEMBER_NOTE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE,readValue = ADD_MEMBER_NOTE)
    private String addMemberNote;
    @SeleniumPath(value = MEMBER_NOTES, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TEXT_AREA,readValue = MEMBER_NOTES)
    private String memberNotes;
    @SeleniumPath(value = TARGET, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = TARGET)
    private String target;
    @SeleniumPath(value = TYPE, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = TYPE)
    private String type;


   // public MemberNotes (GeneralPOSSettingsPage generalPOSSettingsPage){

       // setNext(generalPOSSettingsPage.getMemberNotesCount());
      //  generalPOSSettingsPage.setMemberNotesCount(generalPOSSettingsPage.getMemberNotesCount() + 1);
   // }


    public String getTarget() {return target;}
    public void setTarget(String target) {this.target = target;}

    public String getType() {return type;}
    public void setType(String type) {this.type = type;}

    public String getMemberNotes() {return memberNotes;}
    public void setMemberNotes(String memberNotes) {this.memberNotes = memberNotes;}

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    public void setNext(int counter) {this.counter = counter;}
    public int getNext() {return counter;}

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
