package hub.common.objects.operations;

/**
 * Created by Jenny on 11/6/2016.
 */
public enum SMSConfigurationProvider {

    COMO_TWILIO("//span[contains(text(),'como-twilio')]"),
    COMO_UNICELL_MARKETING("//*[@id=\"ui-select-choices-row-1-1\"]"),
    COMO_UNICELL_OPERATIONAL("//*[@id=\"ui-select-choices-row-1-2\"]"),
    COMO_NEXMO("//span[contains(text(),'como-nexmo')]"),
    COMO_PLIVO("//span[contains(text(),'como-plivo')]"),
    COMO_CUSTOM("//span[contains(text(),'Custom...')]"),
    NEXMO_CANANDA("//span[contains(text(),'nexmo-canada')]"),
    NEXMO_SHORTCODE("//span[contains(text(),'nexmo-shortcode-35342')]")

    ;

    private final String text;


    SMSConfigurationProvider(final String text) {
        this.text = text;

    }

    @Override
    public String toString() {
        return text;
    }



}
