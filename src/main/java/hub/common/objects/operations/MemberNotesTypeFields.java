package hub.common.objects.operations;

/**
 * Created by Jenny on 11/6/2016.
 */
public enum MemberNotesTypeFields {

    TEXT("Text"),
    URL("Url")
    ;

    private final String text;


    MemberNotesTypeFields(final String text) {
        this.text = text;

    }

    @Override
    public String toString() {
        return text;
    }



}
