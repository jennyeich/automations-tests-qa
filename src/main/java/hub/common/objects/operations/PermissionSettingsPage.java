package hub.common.objects.operations;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

public class PermissionSettingsPage extends BaseSeleniumObject {

    private static final String BUSINESS_PLANS = "businessPlans.select";
    private static final String ADD_BUTTON = "businessPlans.add";
    private static final String DELETE_BUSINESS_PLAN = "//div[contains(text(),'%s')]//a";

    @SeleniumPath(value = BUSINESS_PLANS, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.SELECT ,readValue = BUSINESS_PLANS)
    private String businessPlan;
    @SeleniumPath(value = ADD_BUTTON, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String addBusinessPlan;
    @SeleniumPath(value = DELETE_BUSINESS_PLAN, type=SeleniumPath.Type.BY_XPATH2, elementInputType = SeleniumPath.ElementInputType.CLICK_AND_APPROVE)
    private String deleteBusinessPlan;


    public String getBusinessPlan() {
        return businessPlan;
    }

    public void setBusinessPlan(String businessPlan) {
        this.businessPlan = businessPlan;
    }


    public void clickAddBusinessPlan() {
        this.addBusinessPlan = "Clicked Add business plan.";
    }

    public String getDeleteBusinessPlan() {
        return deleteBusinessPlan;
    }

    public void setDeleteBusinessPlan(String deleteBusinessPlan) {
        this.deleteBusinessPlan = String.format(DELETE_BUSINESS_PLAN, deleteBusinessPlan);
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
