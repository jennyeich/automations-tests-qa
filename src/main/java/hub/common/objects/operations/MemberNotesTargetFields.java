package hub.common.objects.operations;

/**
 * Created by Jenny on 11/6/2016.
 */
public enum MemberNotesTargetFields {

    POS("POS"),
    RECEIPT("Receipt")
    ;

    private final String text;


    MemberNotesTargetFields(final String text) {
        this.text = text;

    }

    @Override
    public String toString() {
        return text;
    }



}
