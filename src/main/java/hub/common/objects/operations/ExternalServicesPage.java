package hub.common.objects.operations;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

public class ExternalServicesPage extends BaseSeleniumObject {

    public static final String BUSINESS_ADDRESS = "emailData.addres";
    public static final String FROM_NAME = "emailData.from";
    public static final String COMO = "comoDomain";
    public static final String CUSTOM = "customDomain";
    public static final String COMO_DOMAIN_PREFIX = "comoDomainPrefix";
    public static final String COMO_DOMAIN_SUFFIX = "comoDomainSuffix";
    public static final String CUSTOM_DOMAIN_PREFIX = "customDomainPrefix";
    public static final String CUSTOM_DOMAIN_SUFFIX = "customDomainSuffix";
    public static final String GENERATE_DKIM = "emailSettings.generateDkim";

    public static final String DKIM_HOSTNAME = "hostname";
    public static final String DKIM_VALUE = "email_marketing.value";
    public static final String AUTHENTICATE = "emailSettings.authenticate";
    public static final String AUTHENTICATION_STATUS = "authStatus";
    public static final String SAVE = "emailSettings.save";
    public static final String YES = "yes";
    public static final String SMS_CONFIGURATION = "//*[@id=\"SMSConfig\"]/div/a/span[2]";
    public static final String FROM = "fromSMSConfig";
    public static final String OPT_OUT_BLY_LINK = "unsubscribeByLink";
    public static final String OPT_OUT_BY_REPLY = "unsubscribeByReply";
    public static final String SAVE_SMS = "saveSmsConfig";




    @SeleniumPath(value = SMS_CONFIGURATION, type=SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_XPATH, readValue = SMS_CONFIGURATION)
    private String smsConfiguration;
    @SeleniumPath(value = FROM, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = FROM)
    private String from;
    @SeleniumPath(value = OPT_OUT_BLY_LINK, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.CHECKBOX,readValue = OPT_OUT_BLY_LINK)
    private String unsubscribe_link;
    @SeleniumPath(value = OPT_OUT_BY_REPLY, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.CHECKBOX,readValue = OPT_OUT_BY_REPLY)
    private String unsubscribe_reply;
    @SeleniumPath(value = SAVE_SMS, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = SAVE_SMS)
    private String saveSMS;
    @SeleniumPath(value = BUSINESS_ADDRESS, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = BUSINESS_ADDRESS)
    private String businessAddress;
    @SeleniumPath(value = FROM_NAME, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = FROM_NAME)
    private String fromName;
    @SeleniumPath(value = COMO, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = COMO)
    private String como;
    @SeleniumPath(value = CUSTOM, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = CUSTOM)
    private String custom;
    @SeleniumPath(value = COMO_DOMAIN_PREFIX, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = COMO_DOMAIN_PREFIX)
    private String comoDomainPrefix;
    @SeleniumPath(value = COMO_DOMAIN_SUFFIX, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = COMO_DOMAIN_SUFFIX)
    private String comoDomainSuffix;
    @SeleniumPath(value = CUSTOM_DOMAIN_PREFIX, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = CUSTOM_DOMAIN_PREFIX)
    private String customDomainPrefix;
    @SeleniumPath(value = CUSTOM_DOMAIN_SUFFIX, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = CUSTOM_DOMAIN_SUFFIX)
    private String customDomainSuffix;
    @SeleniumPath(value = GENERATE_DKIM, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = GENERATE_DKIM)
    private String generateDkim;
    @SeleniumPath(value = DKIM_HOSTNAME, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = DKIM_HOSTNAME)
    private String dkimHostName;
    @SeleniumPath(value = DKIM_VALUE, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = DKIM_VALUE)
    private String dkimValue;
    @SeleniumPath(value = AUTHENTICATE, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = AUTHENTICATE)
    private String authenticate;
    @SeleniumPath(value = AUTHENTICATION_STATUS, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = AUTHENTICATION_STATUS)
    private String authenticationStatus;
    @SeleniumPath(value = SAVE, type=SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = SAVE)
    private String save;


    public String getBusinessAddress() {
        return businessAddress;
    }

    public void setBusinessAddress(String businessAddress) {
        this.businessAddress = businessAddress;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getComoDomainPrefix() {
        return comoDomainPrefix;
    }
    public String getSaveSMS() { return saveSMS; }
    public void setSaveSMS(String saveSMS) {
        this.saveSMS = saveSMS;
    }

    public void setComoDomainPrefix(String comoDomainPrefix) {
        this.comoDomainPrefix = comoDomainPrefix;
    }
    public String getSmsConfiguration() { return smsConfiguration; }
    public void setSmsConfiguration(String smsConfiguration) {
        this.smsConfiguration = smsConfiguration;
        setSaveSMS("yes");
    }
    public String getFrom() { return from; }
    public void setFrom(String from) {

        this.from = from;
        setSaveSMS("yes");
    }

    public String getUnsubscribe_link() { return unsubscribe_link; }
    public void setUnsubscribe_link(String unsubscribe_link) { this.unsubscribe_link = unsubscribe_link; }
    public String getUnsubscribe_reply() { return unsubscribe_reply; }

    public void setUnsubscribe_reply(String unsubscribe_reply) { this.unsubscribe_reply = unsubscribe_reply; }


    public String getComoDomainSuffix() {
        return comoDomainSuffix;
    }

    public void selectComo() {
        this.como = YES;
        this.custom = null;
        this.customDomainPrefix = null;
        this.customDomainSuffix = null;
        this.dkimHostName = null;
        this.dkimValue = null;
        this.authenticationStatus = null;
    }

    public void selectCustom() {
        this.custom = YES;
        this.como = null;
        this.comoDomainPrefix = null;
        this.comoDomainSuffix = null;
    }

    public String getCustomDomainPrefix() {
        return customDomainPrefix;
    }

    public void setCustomDomainPrefix(String customDomainPrefix) {
        this.customDomainPrefix = customDomainPrefix;
    }

    public String getCustomDomainSuffix() {
        return customDomainSuffix;
    }

    public void setCustomDomainSuffix(String customDomainSuffix) {
        this.customDomainSuffix = customDomainSuffix;
    }

    public String getDkimHostName() {
        return dkimHostName;
    }

    public void setDkimHostName(String dkimHostName) {
        this.dkimHostName = dkimHostName;
    }

    public String getDkimValue() {
        return dkimValue;
    }

    public void setDkimValue(String dkimValue) {
        this.dkimValue = dkimValue;
    }

    public String getAuthenticationStatus() {
        return authenticationStatus;
    }

    public void setAuthenticationStatus(String authenticationStatus) {
        this.authenticationStatus = authenticationStatus;
    }

    public void clickGenerateDkim() {
        this.generateDkim = YES;
    }

    public void clickAuthenticate() {
        this.authenticate = YES;
    }

    public void clickSave() {
        this.save = YES;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
