package hub.common.objects.operations;

/**
 * Created by Jenny on 11/6/2016.
 */
public enum SavePurchaseOptions {

    PURCHASE_ALL_MEMBERS("Purchases of members"),
    ALL_PURCHASES("All purchases (including non-members)"),
    PURCHASE_OF_MEMBERS_OR_NO_CUSTOMER("Purchases of members or purchases with no customer")
    ;

    private final String text;


    SavePurchaseOptions(final String text) {
        this.text = text;

    }

    @Override
    public String toString() {
        return text;
    }



}
