package hub.common.objects.oldObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 6/8/2017.
 */
public class ConditionPurchaseInRange extends BaseSeleniumObject {

    public static final String SUM_MIN =  "sumMin";
    public static final String SUM_MAX =  "sumMax";


    @SeleniumPath(value = SUM_MIN, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = SUM_MIN)
    private String sumMin;
    @SeleniumPath(value = SUM_MAX, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = SUM_MAX)
    private String sumMax;

    public String getSumMin() {
        return sumMin;
    }

    public void setSumMin(String sumMin) {
        this.sumMin = sumMin;
    }

    public String getSumMax() {
        return sumMax;
    }

    public void setSumMax(String sumMax) {
        this.sumMax = sumMax;
    }


    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
