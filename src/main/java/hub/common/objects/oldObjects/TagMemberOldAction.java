package hub.common.objects.oldObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 6/8/2017.
 */
public class TagMemberOldAction extends BaseSeleniumObject {

    public static final String TAG_FIELD = "(//*[@automationid=\"genericInput\"]/input)";
    public static final String SELECT_ACTION ="(//*[@automationid=\"genericInput\"]/select)";


    @SeleniumPath(value = TAG_FIELD, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TAG_FIELD)
    String tagText;

    @SeleniumPath(value = SELECT_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_XPATH, readValue = SELECT_ACTION)
    String action;

    public int counter;


    public TagMemberOldAction(IOldCounter automation){
        setNext(automation.getTagMemberCount());
        automation.setTagMemberCount(automation.getTagMemberCount() + 1);
    }
    public String getTagText() {
        return tagText;
    }

    public void setTagText(String tagText) {
        this.tagText = tagText;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }


    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    public void setNext (int counter) {this.counter = counter;}
    public int getNext (){return counter;}

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

}
