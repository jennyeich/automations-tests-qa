package hub.common.objects.oldObjects;

/**
 * Created by Goni on 11/5/2017.
 */

public enum BudgetTypeOldAction {
    Default("Default (Point/Credit Settings)"),
    Points("(//option[@label=\"Tag member\"])"),
    Credit("(//option[@label=\"Untag member\"])");

    private final String name;
    BudgetTypeOldAction(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
