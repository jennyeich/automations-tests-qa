package hub.common.objects.oldObjects;

/**
 * Created by Goni on 6/8/2017.
 */
public enum TagOptions {

    Tag("(//option[@label=\"Tag member\"])"),
    UnTag("(//option[@label=\"Untag member\"])")
    ;

    private final String name;
    TagOptions(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
