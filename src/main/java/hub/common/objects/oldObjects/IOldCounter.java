package hub.common.objects.oldObjects;

/**
 * Created by Goni on 6/8/2017.
 */
public interface IOldCounter{

    void setTagMemberCount(int i);
    int getTagMemberCount();

    int getConditionCount ();
    void setConditionCount (int i);

    int getActionCount ();
    void setActionCount (int i);

    void setPointsAccumulationCount(int i);
    int getPointsAccumulationCount();

}
