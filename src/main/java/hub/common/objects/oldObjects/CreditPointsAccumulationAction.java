package hub.common.objects.oldObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 11/5/2017.
 */

public class CreditPointsAccumulationAction extends BaseSeleniumObject {

    public static final String DEFAULT_MULTIPLIER = "(//*[@id=\"Default_Multiplier\"])";
    public static final String DEFAULT_CONSTANT = "(//*[@id=\"Default_Const\"])";
    public static final String BUDGET_TYPE = "(//*[@id=\"BudgetType\"])";


    @SeleniumPath(value = DEFAULT_MULTIPLIER, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = DEFAULT_MULTIPLIER)
    String defaultMultiplier;
    @SeleniumPath(value = DEFAULT_CONSTANT, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = DEFAULT_CONSTANT)
    String defaultConstant;
    @SeleniumPath(value = BUDGET_TYPE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = BUDGET_TYPE)
    String budgetType;
    public int counter;


    public CreditPointsAccumulationAction(IOldCounter automation){
        setNext(automation.getPointsAccumulationCount());
        automation.setPointsAccumulationCount(automation.getPointsAccumulationCount() + 1);
    }

    public String getDefaultMultiplier() {
        return defaultMultiplier;
    }

    public void setDefaultMultiplier(String defaultMultiplier) {
        this.defaultMultiplier = defaultMultiplier;
    }

    public String getDefaultConstant() {
        return defaultConstant;
    }

    public void setDefaultConstant(String defaultConstant) {
        this.defaultConstant = defaultConstant;
    }

    public String getBudgetType() {
        return budgetType;
    }

    public void setBudgetType(String budgetType) {
        this.budgetType = budgetType;
    }

    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    public void setNext (int counter) {this.counter = counter;}
    public int getNext (){return counter;}

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }


}

