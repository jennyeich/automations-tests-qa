package hub.common.objects.oldObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.smarObjects.triggers.OldAutomationTriggers;

import java.util.ArrayList;

/**
 * Created by Goni on 06/06/2017.
 */
public class OldAutomation extends BaseSeleniumObject implements IOldCounter{

    public static final String ADD_AUTOMATION = "addItem";
    public static final String AUTOMATION_NAME = "name";
    public static final String TRIGGER = "trigger";
    public static final String MAX_TIMES_RECEIVABLE_PER_MEMBER = "maxInvokesOfRulesPerTag";
    public static final String SAVE = "save";

    private int tagMemberCount = 1;
    private int pointCreditAccumulationCount = 1;

    @SeleniumPath(value = ADD_AUTOMATION, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT)
    private String addAutomation;

    @SeleniumPath(value = AUTOMATION_NAME, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = AUTOMATION_NAME)
    private String automationName;
    @SeleniumPath(value = TRIGGER, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = TRIGGER)
    private String trigger;
    @SeleniumPath(value = MAX_TIMES_RECEIVABLE_PER_MEMBER, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = MAX_TIMES_RECEIVABLE_PER_MEMBER)
    private String maxTimesReceivablePerMember;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    private ArrayList<OldCondition> conditions;

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    private ArrayList<OldAction> actions; //crate new automation action

    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT)
    private String save;

    public String getMaxTimesReceivablePerMember() {
        return maxTimesReceivablePerMember;
    }

    public void setMaxTimesReceivablePerMember(String maxTimesReceivablePerMember) {
        this.maxTimesReceivablePerMember = maxTimesReceivablePerMember;
    }

    public void addCondition(OldCondition condition, IOldCounter automation){
        condition.setNext(automation.getConditionCount());
        getConditions().add(condition);
        condition.setCreateActionCounter(this.getNext()+1);
        automation.setConditionCount(automation.getConditionCount()+1);
    }

    private ArrayList<OldCondition> getConditions() {
        if(conditions == null)
            conditions = new ArrayList<>();
        return conditions;
    }

    public void setConditions(ArrayList<OldCondition> conditions) {this.conditions = conditions;}

    public void addAction(OldAction action, IOldCounter automation){
        action.setNext(automation.getActionCount());
        getActions().add(action);
        action.setCreateActionCounter(this.getNext()+1);
        automation.setActionCount(automation.getActionCount()+1);
    }
    public ArrayList<OldAction> getActions() {

        if(actions == null)
            actions = new ArrayList<>();
        return actions;

    }


    @Override
    public void setTagMemberCount(int i) {
        this.tagMemberCount = i;
    }

    @Override
    public int getTagMemberCount() {
        return this.tagMemberCount;
    }

    public void setActions(ArrayList<OldAction> actions) {this.actions = actions;}

    public String getAutomationName() {
        return automationName;
    }

    public void setAutomationName(String automationName) {
        this.automationName = automationName;
    }

    public String getTrigger() {
        return trigger;
    }

    public void setTrigger(OldAutomationTriggers trigger) {
        this.trigger = trigger.toString();
    }


    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    @Override
    public int getConditionCount() {
        return 0;
    }

    @Override
    public void setConditionCount(int i) {

    }

    @Override
    public int getActionCount() {
        return 0;
    }

    @Override
    public void setActionCount(int i) {

    }

    @Override
    public void setPointsAccumulationCount(int i) {
        pointCreditAccumulationCount = i;
    }

    @Override
    public int getPointsAccumulationCount() {
        return pointCreditAccumulationCount;
    }
}
