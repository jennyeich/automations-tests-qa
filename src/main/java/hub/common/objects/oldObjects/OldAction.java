package hub.common.objects.oldObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 6/7/2017.
 */
public class OldAction extends BaseSeleniumObject {

    int counter;
    public int createActionCounter;

    public static final String ADD_ACTION = "//*[@ng-click=\"addAction();\"]";
    public static final String PERFORM_THE_ACTION = "action_name";


    @SeleniumPath(value = ADD_ACTION, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT)
    private static String  AddAction;
    @SeleniumPath(value = PERFORM_THE_ACTION, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = PERFORM_THE_ACTION)
    String performTheAction;

    private TagMemberOldAction tagMember;
    private CreditPointsAccumulationAction accumulationOldAction;

    public CreditPointsAccumulationAction getAccumulationOldAction() {
        return accumulationOldAction;
    }

    public void setAccumulationOldAction(CreditPointsAccumulationAction accumulationOldAction) {
        this.accumulationOldAction = accumulationOldAction;
    }

    public String getPerformTheAction() {return performTheAction;}
    public void setPerformTheAction(String performTheAction) {this.performTheAction = performTheAction;}
    public TagMemberOldAction getTagMember() {
        return tagMember;
    }

    public void setTagMember(TagMemberOldAction tagMember) {
        this.tagMember = tagMember;
    }

    public void setCreateActionCounter(int createActionCounter) {this.createActionCounter = createActionCounter;}
    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
