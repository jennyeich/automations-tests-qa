package hub.common.objects.oldObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 6/7/2017.
 */
public class OldCondition extends BaseSeleniumObject {

    public static final String ADD_CONDITION = "//i[@ng-click=\"addCondition();\"]";
    public static final String CONDITION_TYPE = "ConditionType";

    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT)
    private String AddCondition;

    @SeleniumPath(value = CONDITION_TYPE, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_XPATH,readValue = CONDITION_TYPE)
    private String conditionType;

    public int counter;
    public int createActionCounter;

    public ConditionPurchaseInRange getPurchaseInRange() {
        return purchaseInRange;
    }

    public void setPurchaseInRange(ConditionPurchaseInRange purchaseInRange) {
        this.purchaseInRange = purchaseInRange;
    }

    public ConditionPurchaseInRange purchaseInRange;


    public String getConditionType() {
        return conditionType;
    }

    public void setConditionType(String conditionType) {
        this.conditionType = conditionType;
    }
    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    public void setCreateActionCounter(int createActionCounter) {this.createActionCounter = createActionCounter;}

    @Override
    public int getCreateActionCounter() {
        return createActionCounter;
    }
}
