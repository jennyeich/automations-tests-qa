package hub.common.objects.oldObjects;

/**
 * Created by Goni on 6/8/2017.
 */
public enum ConditionTypes {

    PurchaseTotalInRange("//option[@label=\"The purchase total is in the following range\"]")
    ;

    private final String name;
    ConditionTypes(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
