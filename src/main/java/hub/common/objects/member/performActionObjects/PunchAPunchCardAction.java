package hub.common.objects.member.performActionObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Jenny on 4/26/2017.
 */
public class PunchAPunchCardAction extends BaseSeleniumObject {

    public static final String NUMBER_OF_PUNCHES = "autoPunchAmount";
    public static final String CHOOSE_PUNCH_CARD = "//*[@ng-model=\"action_params.asset\"]";
    public static final String SUBMIT_BUTTON = "//*[@ng-confirm-click=\"submit();\"]";



    @SeleniumPath(value = NUMBER_OF_PUNCHES, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = NUMBER_OF_PUNCHES)
    private String numberOfPunches;
    @SeleniumPath(value = CHOOSE_PUNCH_CARD, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = CHOOSE_PUNCH_CARD)
    private String choosePunchCard;
    @SeleniumPath(value = SUBMIT_BUTTON, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_ALERT)
    private String SUBMIT;


    public String getChoosePunchCard() {return choosePunchCard;}
    public void setChoosePunchCard(String choosePunchCard) {this.choosePunchCard = choosePunchCard;}
    public String getNumberOfPunches() {return numberOfPunches;}
    public void setNumberOfPunches(String numberOfPunches) {this.numberOfPunches = numberOfPunches;}
    public String getNextTagsPath (int i){return "";}

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }


}
