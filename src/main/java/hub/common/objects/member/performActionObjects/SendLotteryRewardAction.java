package hub.common.objects.member.performActionObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import org.openqa.selenium.Keys;

/**
 * Created by Goni on 10/3/2017.
 */

public class SendLotteryRewardAction extends BaseSeleniumObject {

    public static final String SELECTED_ASSET = "//div[@ng-if=\"action_type == 'sendRandomReward'\"]/select[1]";
    public static final String SEND_PUSH_NOTIFICATION = "//*[@ng-model=\"action_params.notify_user\"]";
    public static final String SUBMIT_BUTTON = "//*[@ng-confirm-click=\"submit();\"]";

    @SeleniumPath(value = SELECTED_ASSET, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = SELECTED_ASSET)
    private String selectAsset;
    @SeleniumPath(value = SEND_PUSH_NOTIFICATION, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT, readValue = SEND_PUSH_NOTIFICATION)
    private String pushNotificationYesNo;
    @SeleniumPath(value = SUBMIT_BUTTON, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_ALERT)
    private String submit;

    public String getSelectedAsset() {return selectAsset;}
    public void setSelectedAsset(String selectAsset) {this.selectAsset = selectAsset;}
    public String getPushNotificationYesNo() {return pushNotificationYesNo;}
    public void setPushNotificationYesNo(String pushNotificationYesNo) {this.pushNotificationYesNo = pushNotificationYesNo;}


    public String getNextTagsPath (int i){return "";}

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

