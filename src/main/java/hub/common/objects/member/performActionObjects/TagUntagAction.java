package hub.common.objects.member.performActionObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Jenny on 12/4/2016.
 */
public class TagUntagAction extends BaseSeleniumObject{

    public static final String TAG_TEXT = "tagMembershipTag";
    public static final String ACTION = "//*[@ng-model=\"action_params.Operation\"]";
    public static final String SUBMIT_BUTTON = "//*[@ng-confirm-click=\"submit();\"]";

    @SeleniumPath(value = TAG_TEXT, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TAG_TEXT)
    private String tagUntagText;
    @SeleniumPath(value = ACTION, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = ACTION)
    private String action;
    @SeleniumPath(value = SUBMIT_BUTTON, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_ALERT)
    private String submit;

    public String getNextTagsPath (int i){return "";}
    public String getTagUntagText() {return tagUntagText;}
    public void setTagUntagText(String tagUntagText) {this.tagUntagText = tagUntagText;}
    public String getAction() {return action;}
    public void setAction(String action) {this.action = action;}

    @Override
    public void setNext(int i) {
    }
    @Override
    public int getNext() {
        return 0;
    }
    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
