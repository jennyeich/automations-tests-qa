package hub.common.objects.member.performActionObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by goni on 2/5/2017.
 */
public class AddPointsAction extends BaseSeleniumObject {

    public static final String POINTS_AMOUNT = "pointsAmount";
    public static final String SUBMIT_BUTTON = "//*[@ng-confirm-click=\"submit();\"]";

    @SeleniumPath(value = POINTS_AMOUNT, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = POINTS_AMOUNT)
    private String pointsAmount;
    @SeleniumPath(value = SUBMIT_BUTTON, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_ALERT)
    private String SUBMIT;

    public String getPointsAmount() {return pointsAmount;}
    public void setPointsAmount(String pointsAmount) {this.pointsAmount = pointsAmount;}

    public String getNextTagsPath (int i){return "";}

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
