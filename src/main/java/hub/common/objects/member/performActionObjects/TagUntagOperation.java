package hub.common.objects.member.performActionObjects;

/**
 * Created by Jenny on 12/4/2016.
 */
public enum TagUntagOperation {

    Tag("Tag"),
    Untag("UnTag")
    ;

    private final String text;

    TagUntagOperation(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
