package hub.common.objects.member.performActionObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import org.openqa.selenium.Keys;

/**
 * Created by Jenny on 12/4/2016.
 */
public class SendGiftOrPunchAction extends BaseSeleniumObject{

    public static final String ASSETS_LIST_DROPDOWN = "//a[@aria-label=\"Select box select\"]";
    public static final String ASSETS_SEARCH_BOX = "//*[@id=\"main\"]/div/div[9]/div/div/div[1]/div/div/div[1]/input";
    public static final String SEND_PUSH_NOTIFICATION = "//*[@ng-model=\"action_params.notify_user\"]";
    public static final String SUBMIT_BUTTON = "//*[@ng-confirm-click=\"submit();\"]";

    @SeleniumPath(value = ASSETS_LIST_DROPDOWN, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = "")
    private String openAssetList;
    @SeleniumPath(value = ASSETS_SEARCH_BOX, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = ASSETS_SEARCH_BOX)
    private String searchBox;
    @SeleniumPath(value = SEND_PUSH_NOTIFICATION, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT, readValue = SEND_PUSH_NOTIFICATION)
    private String pushNotificationYesNo;
    @SeleniumPath(value = SUBMIT_BUTTON, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_ALERT)
    private String SUBMIT;

    public String getOpenAssetList() {return openAssetList;}
    public void setOpenAssetList(String openAssetList) {this.openAssetList = openAssetList;}
    public String getPushNotificationYesNo() {return pushNotificationYesNo;}
    public void setPushNotificationYesNo(String pushNotificationYesNo) {this.pushNotificationYesNo = pushNotificationYesNo;}

    public String getSearchBox() {
        return searchBox;
    }

    public void setSearchBox(String searchBox) {
        this.searchBox = searchBox + Keys.ENTER;
    }

    public String getNextTagsPath (int i){return "";}

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
