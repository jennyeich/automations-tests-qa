package hub.common.objects.member.performActionObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Goni on 6/25/2017.
 */
public class AddAmountOfCreditAction extends BaseSeleniumObject {

    public static final String POINTS_AMOUNT = "pointsAmount";
    public static final String ADD_BUTTON = "//*[@ng-confirm-click=\"submit();\"]";

    @SeleniumPath(value = POINTS_AMOUNT, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = POINTS_AMOUNT)
    private String amount;
    @SeleniumPath(value = ADD_BUTTON, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_ALERT)
    private String addBtn;

    public String getAmount() {return amount;}
    public void setAmount(String amount) {this.amount = amount;}

    public String getNextTagsPath (int i){return "";}

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

}
