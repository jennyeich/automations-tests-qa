package hub.common.objects.member;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.member.performSmartActionObjects.*;

public class PerformSmartActionOnMember extends BaseSeleniumObject {


    public static final String PERFORM_ACTION = "//select[@id=\"action_name\"]";


    @SeleniumPath(value = PERFORM_ACTION, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_WITH_WAIT, readValue = PERFORM_ACTION)
    private String performAction;


    AddPointOrCreditAction addPointOrCreditAction;
    PunchAPunchCardAction punchAPunchCardAction;
    SendAnAssetAction sendAnAssetAction;
    SendLotteryRewardAction sendLotteryRewardAction;
    TagOrUnTagAction tagOrUnTagAction;
    UpdateExpirationDateAction updateExpirationDateAction;
    SendEmailAction sendEmailAction;
    SendSMSAction sendSMSAction;


    public String getPerformAction() {
        return performAction;
    }

    public void setPerformAction(String performAction) {
        this.performAction = performAction;
    }

    public AddPointOrCreditAction getAddPointOrCreditAction() {
        return addPointOrCreditAction;
    }

    public void setAddPointOrCreditAction(AddPointOrCreditAction addPointOrCreditAction) {
        this.addPointOrCreditAction = addPointOrCreditAction;
    }

    public PunchAPunchCardAction getPunchAPunchCardAction() {
        return punchAPunchCardAction;
    }

    public void setPunchAPunchCardAction(PunchAPunchCardAction punchAPunchCardAction) {
        this.punchAPunchCardAction = punchAPunchCardAction;
    }

    public SendAnAssetAction getSendAnAssetAction() {
        return sendAnAssetAction;
    }

    public void setSendAnAssetAction(SendAnAssetAction sendAnAssetAction) {
        this.sendAnAssetAction = sendAnAssetAction;
    }

    public SendLotteryRewardAction getSendLotteryRewardAction() {
        return sendLotteryRewardAction;
    }

    public void setSendLotteryRewardAction(SendLotteryRewardAction sendLotteryRewardAction) {
        this.sendLotteryRewardAction = sendLotteryRewardAction;
    }

    public SendEmailAction getSendEmailAction() {
        return sendEmailAction;
    }

    public void setSendEmailAction(SendEmailAction sendEmailAction) {
        this.sendEmailAction = sendEmailAction;
    }

    public TagOrUnTagAction getTagOrUnTagAction() {
        return tagOrUnTagAction;
    }

    public void setTagOrUnTagAction(TagOrUnTagAction tagOrUnTagAction) {
        this.tagOrUnTagAction = tagOrUnTagAction;
    }

    public UpdateExpirationDateAction getUpdateExpirationDateAction() {
        return updateExpirationDateAction;
    }

    public void setUpdateExpirationDateAction(UpdateExpirationDateAction updateExpirationDateAction) {
        this.updateExpirationDateAction = updateExpirationDateAction;
    }


    public SendSMSAction getSendSMSAction() { return sendSMSAction; }
    public void setSendSMSAction(SendSMSAction sendSMSAction) { this.sendSMSAction = sendSMSAction; }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
