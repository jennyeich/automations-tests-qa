package hub.common.objects.member;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by lior on 5/22/17.
 */
public class FilterByBigQuery extends BaseSeleniumObject {

    public static final String FILTER_BY_BIG_QUERY_BUTTON = "filterUsersBQ";

    @SeleniumPath(value = FILTER_BY_BIG_QUERY_BUTTON, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT, readValue = FILTER_BY_BIG_QUERY_BUTTON)
    private String filterByQueryButton;



    public String filterByQueryButton() {
        return filterByQueryButton;
    }

    public void filterByQueryButton(String filterByQueryButton) {
        this.filterByQueryButton = filterByQueryButton;
    }

    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
