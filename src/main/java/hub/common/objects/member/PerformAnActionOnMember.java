package hub.common.objects.member;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.member.performActionObjects.*;

/**
 * Created by Jenny on 12/4/2016.
 */
public class PerformAnActionOnMember extends BaseSeleniumObject {

    public static final String PERFORM_ACTION = "//select[@automationid=\"dropdownPerformAction\"]";
    public static final String PUSH_NOTIFICATION_MESSAGE = "pushmessage";
    public static final String SEND_PUSH_NOTIFICATION = "sendPush";

    @SeleniumPath(value = PERFORM_ACTION, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = PERFORM_ACTION)
    private String performAction;
    @SeleniumPath(value = PUSH_NOTIFICATION_MESSAGE, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.TEXT_AREA, readValue = PUSH_NOTIFICATION_MESSAGE)
    private String pushNotificationMessage;
    @SeleniumPath(value = SEND_PUSH_NOTIFICATION, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK_AND_APPROVE, readValue = SEND_PUSH_NOTIFICATION)
    private String sendPushNotification;

    SendGiftOrPunchAction sendGiftOrPunchAction;
    SendLotteryRewardAction sendLotteryRewardAction;
    AddPointsAction addPointsAction;
    TagUntagAction tagUntagAction;
    PunchAPunchCardAction punchAPunchCard;
    AddAmountOfCreditAction addAmountOfCredit;


    public String getPerformAction() {return performAction;}
    public void setPerformAction(String performAction) {this.performAction = performAction;}
    public SendGiftOrPunchAction getSendGiftOrPunchAction() {return sendGiftOrPunchAction;}
    public void setSendGiftOrPunchAction(SendGiftOrPunchAction sendGiftOrPunchAction) {this.sendGiftOrPunchAction = sendGiftOrPunchAction;}
    public TagUntagAction getTagUntagAction() {return tagUntagAction;}
    public void setTagUntagAction(TagUntagAction tagUntagAction) {this.tagUntagAction = tagUntagAction;}
    public PunchAPunchCardAction getPunchAPunchCard() {return punchAPunchCard;}
    public void setPunchAPunchCard(PunchAPunchCardAction punchAPunchCard) {this.punchAPunchCard = punchAPunchCard;}
    public AddAmountOfCreditAction getAddAmountOfCredit() {
        return addAmountOfCredit;
    }

    public void setAddAmountOfCredit(AddAmountOfCreditAction addAmountOfCredit) {
        this.addAmountOfCredit = addAmountOfCredit;
    }
    public void setPushNotificationMessage(String message){
        this.pushNotificationMessage =message;
        this.sendPushNotification = "send";
    }
    public String getPushNotificationMessage(){return pushNotificationMessage;}
    public AddPointsAction getAddPointsAction() {return addPointsAction;}
    public void setAddPointsAction(AddPointsAction addPointsAction){this.addPointsAction = addPointsAction;}

    public SendLotteryRewardAction getSendLotteryRewardAction() {
        return sendLotteryRewardAction;
    }

    public void setSendLotteryRewardAction(SendLotteryRewardAction sendLotteryRewardAction) {
        this.sendLotteryRewardAction = sendLotteryRewardAction;
    }

    public void setNext(int i) {
    }

    @Override
    public int getNext() {
        return 0;
    }
    public String getNextTagsPath (int i){return "";}


    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
