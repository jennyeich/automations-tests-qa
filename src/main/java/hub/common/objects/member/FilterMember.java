package hub.common.objects.member;

import hub.base.BaseSeleniumObject;
import hub.base.BaseService;
import hub.base.SeleniumPath;
import hub.common.objects.common.DateAndTimeInFilter;
import hub.common.objects.common.FilterMembersEmailEvents;
import hub.common.objects.common.FilterMembersEmailSentFrom;
import hub.common.objects.model.ExportItemModel;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;

/**
 * Created by lior on 5/8/17.
 */
public class FilterMember extends BaseSeleniumObject {

    public static final String MEMBERSHIP_STATUS_REGISTERED = "MembershipStatus.registered";
    public static final String MEMBERSHIP_STATUS_NON_REGISTERED = "MembershipStatus.not_registered";

    public static final String HAS_CONSENT = "memberHasConcetTermsAndConditions";
    public static final String NO_CONSENT = "memberHasNotConcetTermsAndConditions";
    public static final String PREVIOUS_CONSENT = "businessHasConcetTermsAndConditions";

    public static final String CHOOSE_PRESET_FILTER = "selected_preset";
    public static final String ADD_SEARCH_FILTER = "//i[@ng-click='ctrlFU.addRule();']";
    public static final String FIND_CUSTOMERS = "//i[@ng-click='ctrlFU.addUserFields();']";
    public static final String MEMBER_ACTION_TYPE = "rule_action";
    public static final String SOURCE = "equals_field_name_Data_source";
    public static final By NUMBER_OF_MEMBERS = By.xpath("//div/h3[@automationid=\"numOfMembers\"]");
    public static final String NUMBER_OF_ACTIONS_LABEL = "count_type";

    public static final String ITEM_CODE = "equals_field_name_ItemCode";

    public static final String APPLY_FIELDS_PER_ACTION_OR_TOTAL = "amount_field_name";
    public static final String MORE_THAN_OR_EQUAL_TO_NUMBER_OF_ACTION_DATA = "count_from";
    public static final String PURCHASE_TOTAL_IS_GREATER_THAN = "equals_field_name_TotalSum_MoreThan";
    public static final String LESS_THEN_OR_EQUAL_TO_NUMBER_OF_ACTION_DATA = "count_to";
    public static final String CHOOSE_DATES = "//select[@ng-model='rule.date_type']";
    public static final String FROM_THE_LAST_X_DAYS = "//input[@ng-model='rule.amount_days']";
    public static final String NUMBER_OF_ACTIONS_FROM = "count_from";
    public static final String NUMBER_OF_ACTIONS_TO = "count_to";
    public static final String TOTAL_IS_GREATER_THAN = "equals_field_name_TotalSum_MoreThan";
    public static final String TOTAL_IS_LESS_THAN = "equals_field_name_TotalSum_LessThan";
    public static final String EXPORT_BUTTON = "exportButton";
    public static final String REPORTS = "exportItem";
    public static final String PAYMENT_TYPE = "equals_field_name_Data_BudgetType";
    public static final String TOTAL_AMOUNT_RANGE_FROM = "amount_from";
    public static final String TOTAL_AMOUNT_RANGE_TO = "amount_to";
    public static final String TOTAL_VALUE_MORE_THAN_OR_EQUAL_TO_DATA = "amount_from";
    public static final String TOTAL_VALUE_LESS_THAN_OR_EQUAL_TO_DATA = "amount_to";
    public static final String FIRST_NAME = "FirstName";
    public static final String PUSH_NOTIFICATION_ENABLED = "//div[@ng-bind='FSCtrl.getTitle(FSCtrl.field);' and contains(text(), 'Push Notification Enabled (iOS only)')]";

    public static final String PERFORM_SMART_ACTION_DROP_DOWN = "action_name";
    public static final String SMART_ACTION_SEND_AN_ASSET = "//*[@value=\"string:assignAsset\"]";
    public static final String SMART_ACTION_SEND_AN_EMAIL = "//*[@value=\"string:sendMemberEmail\"]";

    public static final String CHOOSE_TEMPLATE = "emailTemplate.filter.select";
    public static final String SENT_FROM = "equals_field_name_Data_source";
    public static final String EMAIL_EVENTS = "equals_field_name_Data_event";



    @SeleniumPath(value = MEMBERSHIP_STATUS_REGISTERED, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = MEMBERSHIP_STATUS_REGISTERED)
    private String membershipStatusRegistered;
    @SeleniumPath(value = MEMBERSHIP_STATUS_NON_REGISTERED, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = MEMBERSHIP_STATUS_NON_REGISTERED)
    private String membershipStatusNonRegistered;
    @SeleniumPath(value = HAS_CONSENT, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CHECKBOX, readValue = HAS_CONSENT)
    private Boolean hasConsent;
    @SeleniumPath(value = NO_CONSENT, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CHECKBOX, readValue = NO_CONSENT)
    private Boolean noConsent;
    @SeleniumPath(value = PREVIOUS_CONSENT, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CHECKBOX, readValue = PREVIOUS_CONSENT)
    private Boolean previousConsent;
    @SeleniumPath(value = CHOOSE_PRESET_FILTER, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = CHOOSE_PRESET_FILTER)
    private String choosePresetFilter;
    @SeleniumPath(value = ADD_SEARCH_FILTER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = ADD_SEARCH_FILTER)
    private String addSearchFilter;
    @SeleniumPath(value = MEMBER_ACTION_TYPE, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = MEMBER_ACTION_TYPE)
    private String memberActionType;
    @SeleniumPath(value = SOURCE, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT, readValue = SOURCE)
    private String source;
    @SeleniumPath(value = CHOOSE_TEMPLATE, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = CHOOSE_TEMPLATE)
    private String templateName;
    @SeleniumPath(value = APPLY_FIELDS_PER_ACTION_OR_TOTAL, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = APPLY_FIELDS_PER_ACTION_OR_TOTAL)
    private String applyFieldsPerActionOrTotal;

    @SeleniumPath(value = ITEM_CODE, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = ITEM_CODE)
    private String itemCode;

    @SeleniumPath(value = NUMBER_OF_ACTIONS_LABEL, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = NUMBER_OF_ACTIONS_LABEL)
    private String numberOfActionsLabel;
    @SeleniumPath(value = NUMBER_OF_ACTIONS_FROM, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = NUMBER_OF_ACTIONS_FROM)
    private String numberOfActionsFrom;
    @SeleniumPath(value = NUMBER_OF_ACTIONS_TO, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = NUMBER_OF_ACTIONS_TO)
    private String numberOfActionsTo;
    @SeleniumPath(value = TOTAL_AMOUNT_RANGE_FROM, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TOTAL_AMOUNT_RANGE_FROM)
    private String totalAmountRangeFrom;
    @SeleniumPath(value = TOTAL_AMOUNT_RANGE_TO, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TOTAL_AMOUNT_RANGE_TO)
    private String totalAmountRangeTo;
    @SeleniumPath(value = TOTAL_VALUE_MORE_THAN_OR_EQUAL_TO_DATA, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TOTAL_VALUE_MORE_THAN_OR_EQUAL_TO_DATA)
    private String totalValueMoreThanOrEqualToData;
    @SeleniumPath(value = TOTAL_VALUE_LESS_THAN_OR_EQUAL_TO_DATA, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TOTAL_VALUE_LESS_THAN_OR_EQUAL_TO_DATA)
    private String totalValueLessThanOrEqualToData;
    @SeleniumPath(value = MORE_THAN_OR_EQUAL_TO_NUMBER_OF_ACTION_DATA, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = MORE_THAN_OR_EQUAL_TO_NUMBER_OF_ACTION_DATA)
    private String numberOfActionsData;
    @SeleniumPath(value = PURCHASE_TOTAL_IS_GREATER_THAN, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = PURCHASE_TOTAL_IS_GREATER_THAN)
    private String purchaseTotalIsGreaterThan;
    @SeleniumPath(value = LESS_THEN_OR_EQUAL_TO_NUMBER_OF_ACTION_DATA, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = LESS_THEN_OR_EQUAL_TO_NUMBER_OF_ACTION_DATA)
    private String lessThenOrEqualToNumberOfActionData;



    @SeleniumPath(value = SENT_FROM, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = SENT_FROM)
    private FilterMembersEmailSentFrom sentFrom;
    @SeleniumPath(value = EMAIL_EVENTS, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = EMAIL_EVENTS)
    private FilterMembersEmailEvents emailEvents;



    @SeleniumPath(value = CHOOSE_DATES, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = CHOOSE_DATES)
    private String chooseDate;
    @SeleniumPath(value = FROM_THE_LAST_X_DAYS, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = FROM_THE_LAST_X_DAYS)
    private String fromTheLastXDays;
    @SeleniumPath(value = TOTAL_IS_GREATER_THAN, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TOTAL_IS_GREATER_THAN)
    private String totalIsGreaterThan;
    @SeleniumPath(value = TOTAL_IS_LESS_THAN, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TOTAL_IS_LESS_THAN)
    private String totalIsLessThan;
    @SeleniumPath(value = EXPORT_BUTTON, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_WAIT)
    private String exportButton;
    @SeleniumPath(value = "", type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.EXPORT_REPORTS,readValue = REPORTS)
    private ArrayList<ExportItemModel> exports;
    @SeleniumPath(value = PAYMENT_TYPE, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = PAYMENT_TYPE)
    private String paymentType;

    @SeleniumPath(value = FIND_CUSTOMERS, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = FIND_CUSTOMERS)
    private String findCustomers;
    @SeleniumPath(value = FIRST_NAME, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = FIRST_NAME)
    private String firstName;

    @SeleniumPath(value = PERFORM_SMART_ACTION_DROP_DOWN, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = PERFORM_SMART_ACTION_DROP_DOWN)
    private String performSmartActionDropDown;

    DateAndTimeInFilter dateAndTimeInFilter;


    public String getMembershipStatusRegistered() {
        return membershipStatusRegistered;
    }

    public void setMembershipStatusRegistered(String membershipStatusRegistered) {
        this.membershipStatusRegistered = membershipStatusRegistered;
    }

    public String getMembershipStatusNonRegistered() {
        return membershipStatusNonRegistered;
    }

    public void setMembershipStatusNonRegistered(String membershipStatusNonRegistered) {
        this.membershipStatusNonRegistered = membershipStatusNonRegistered;
    }

    public Boolean getHasConsent() {
        return hasConsent;
    }

    public void checkHasConsent() {
        this.hasConsent = true;
    }

    public Boolean getNoConsent() {
        return noConsent;
    }

    public void checkNoConsent() {
        this.noConsent = true;
    }

    public Boolean getPreviousConsent() {
        return previousConsent;
    }

    public void checkPreviousConsent() {
        this.previousConsent = true;
    }

    public String getChoosePresetFilter() {
        return choosePresetFilter;
    }

    public void setChoosePresetFilter(String choosePresetFilter) {
        this.choosePresetFilter = choosePresetFilter;
    }

    public String getAddSearchFilter() {
        return addSearchFilter;
    }

    public void setAddSearchFilter(String addSearchFilter) {
        this.addSearchFilter = addSearchFilter;
    }

    public String getMemberActionType() {
        return memberActionType;
    }

    public void setMemberActionType(String memberActionType) {
        this.memberActionType = memberActionType;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getNumberOfActionsLabel() {
        return numberOfActionsLabel;
    }

    public void setNumberOfActionsLabel(String numberOfActionsLabel) {
        this.numberOfActionsLabel = numberOfActionsLabel;
    }

    public String getNumberOfActionsData() {
        return numberOfActionsData;
    }

    public void setNumberOfActionsData(String numberOfActionsData) {
        this.numberOfActionsData = numberOfActionsData;
    }

    public String getPurchaseTotalIsGreaterThan() {
        return purchaseTotalIsGreaterThan;
    }

    public void setPurchaseTotalIsGreaterThan(String purchaseTotalIsGreaterThan) {
        this.purchaseTotalIsGreaterThan = purchaseTotalIsGreaterThan;
    }

    public String getLessThenOrEqualToNumberOfActionData() {
        return lessThenOrEqualToNumberOfActionData;
    }

    public void setLessThenOrEqualToNumberOfActionData(String lessThenOrEqualToNumberOfActionData) {
        this.lessThenOrEqualToNumberOfActionData = lessThenOrEqualToNumberOfActionData;
    }

    public String getChooseDate() {
        return chooseDate;
    }

    public void setChooseDate(String chooseDate) {
        this.chooseDate = chooseDate;
    }

    public String getFromTheLastXDays() {
        return fromTheLastXDays;
    }

    public void setFromTheLastXDays(String fromTheLastXDays) {
        this.fromTheLastXDays = fromTheLastXDays;
    }

    public String getNumberOfActionsFrom() {
        return numberOfActionsFrom;
    }

    public void setNumberOfActionsFrom(String numberOfActionsFrom) {
        this.numberOfActionsFrom = numberOfActionsFrom;
    }

    public String getNumberOfActionsTo() {
        return numberOfActionsTo;
    }

    public void setNumberOfActionsTo(String numberOfActionsTo) {
        this.numberOfActionsTo = numberOfActionsTo;
    }

   /* public String getPunchCardList() {
        return punchCardList;
    }

    public void setPunchCardList(String punchCardList) {
        this.punchCardList = punchCardList;
    }

    public String getPunchCard1() {
        return punchCard1;
    }

    public void setPunchCard1(String punchCard1) {
        this.punchCard1 = punchCard1;
    }*/

    public DateAndTimeInFilter getDateAndTimeInFilter() {
        return dateAndTimeInFilter;
    }

    public void setDateAndTimeInFilter(DateAndTimeInFilter dateAndTimeInFilter) {
        this.dateAndTimeInFilter = dateAndTimeInFilter;
    }

    public String getTotalIsGreaterThan() {
        return totalIsGreaterThan;
    }

    public void setTotalIsGreaterThan(String totalIsGreaterThan) {
        this.totalIsGreaterThan = totalIsGreaterThan;
    }

    public String getTotalIsLessThan() {
        return totalIsLessThan;
    }

    public void setTotalIsLessThan(String totalIsLessThan) {
        this.totalIsLessThan = totalIsLessThan;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getApplyFieldsPerActionOrTotal() {
        return applyFieldsPerActionOrTotal;
    }

    public void setApplyFieldsPerActionOrTotal(String applyFieldsPerActionOrTotal) {
        this.applyFieldsPerActionOrTotal = applyFieldsPerActionOrTotal;
    }

    public String getTotalAmountRangeFrom() {
        return totalAmountRangeFrom;
    }

    public void setTotalAmountRangeFrom(String totalAmountRangeFrom) {
        this.totalAmountRangeFrom = totalAmountRangeFrom;
    }

    public String getTotalAmountRangeTo() {
        return totalAmountRangeTo;
    }

    public void setTotalAmountRangeTo(String totalAmountRangeTo) {
        this.totalAmountRangeTo = totalAmountRangeTo;
    }

    public String getTotalValueMoreThanOrEqualToData() {
        return totalValueMoreThanOrEqualToData;
    }

    public void setTotalValueMoreThanOrEqualToData(String totalValueMoreThanOrEqualToData) {
        this.totalValueMoreThanOrEqualToData = totalValueMoreThanOrEqualToData;
    }

    public String getTotalValueLessThanOrEqualToData() {
        return totalValueLessThanOrEqualToData;
    }

    public void setTotalValueLessThanOrEqualToData(String totalValueLessThanOrEqualToData) {
        this.totalValueLessThanOrEqualToData = totalValueLessThanOrEqualToData;
    }

    public String getFindCustomers() {
        return findCustomers;
    }

    public void setFindCustomers(String findCustomers) {
        this.findCustomers = findCustomers;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPerformSmartActionDropDown() {
        return performSmartActionDropDown;
    }

    public void setPerformSmartActionDropDown(String performSmartActionDropDown) {
        this.performSmartActionDropDown = performSmartActionDropDown;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public FilterMembersEmailSentFrom getSentFrom() {
        return sentFrom;
    }

    public void setSentFrom(FilterMembersEmailSentFrom sentFrom) {
        this.sentFrom = sentFrom;
    }

    public FilterMembersEmailEvents getEmailEvents() {
        return emailEvents;
    }

    public void setEmailEvents(FilterMembersEmailEvents emailEvents) {
        this.emailEvents = emailEvents;
    }


    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public void export() {
        String xpath = "//*[@automationid=\'" + EXPORT_BUTTON + "\']";
        BaseService.getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
        BaseService.getDriverWait().until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        BaseService.findElementByAutomationId(EXPORT_BUTTON).click();
    }

    public ArrayList<ExportItemModel> getExports() {
        return exports;
    }
}

