package hub.common.objects.member;

import hub.base.BaseSeleniumObject;
import hub.base.BaseService;
import hub.base.SeleniumPath;
import hub.common.objects.model.ExportItemModel;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;

/**
 * Created by Jenny on 12/2/2016.
 */
public class FindMember extends BaseSeleniumObject {

    public static final String FIRST_NAME = "FirstName";
    public static final String LAST_NAME = "LastName";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String GENDER = "Gender";
    public static final String EMAIL = "Email";
    public static final String USER_KEY = "//*[@name=\"userkey\"]";
    public static final String MEMBERSHIP_KEY = "//*[@name=\"Membershipkey\"]";
    public static final String BIRTHDAY = "datePicker";
    public static final String BIRTHMONTH = "birthdayMonth";
    public static final String GOV_ID = "GovID";
    public static final String SEARCH_BUTTON = "searchButton";
    public static final String EXPORT_BUTTON = "exportButton";
    public static final String REPORTS = "exportItem";
    public static final String EXTRA_IDENTIFIERS = "//div[@action=\"ExtraIdentifiers\"]";
    public static final String MOBILE_APP_USER = "//div[@action=\"MobileAppUsed\"]";
    public static final String LOCATION_ENABLED = "//div[@action=\"LocationEnabled\"]";
    public static final String MEMBERSHIP_STATUS = "//div[@action='membershipStatus']";


    @SeleniumPath(value = FIRST_NAME, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = FIRST_NAME)
    private String firstName;
    @SeleniumPath(value = LAST_NAME, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = LAST_NAME)
    private String lastName;
    @SeleniumPath(value = PHONE_NUMBER, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = PHONE_NUMBER)
    private String phoneNumber;
    @SeleniumPath(value = GENDER, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = GENDER)
    private String gender;
    @SeleniumPath(value = EMAIL, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = EMAIL)
    private String email;
    @SeleniumPath(value = USER_KEY, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = USER_KEY)
    private String userKey;
    @SeleniumPath(value = MEMBERSHIP_KEY, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = MEMBERSHIP_KEY)
    private String membershipKey;
    @SeleniumPath(value = BIRTHDAY, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.DATE, readValue = BIRTHDAY)
    private String birthday;
    @SeleniumPath(value = BIRTHMONTH, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.SELECT, readValue = BIRTHMONTH)
    private String birthMonth;
    @SeleniumPath(value = GOV_ID, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = GOV_ID)
    private String govId;

    @SeleniumPath(value = EXTRA_IDENTIFIERS, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = EXTRA_IDENTIFIERS)
    private String extraIdentifiers;
    @SeleniumPath(value = MOBILE_APP_USER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = MOBILE_APP_USER)
    private String mobileAppUser;
    @SeleniumPath(value = LOCATION_ENABLED, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = LOCATION_ENABLED)
    private String locationEnabled;

    @SeleniumPath(value = SEARCH_BUTTON, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT, readValue = "")
    private String searchButton;
    @SeleniumPath(value = EXPORT_BUTTON, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_WAIT, readValue = "")
    private String exportButton;
    @SeleniumPath(value = "", type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.EXPORT_REPORTS,readValue = REPORTS)
    private ArrayList<ExportItemModel> exports;

//    @SeleniumPath(value = CLICK_ON_MEMBER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT, readValue = CLICK_ON_MEMBER)
//    private String clickMember;

    public String getMembershipKey() { return membershipKey; }

    public void setMembershipKey(String membershipKey) { this.membershipKey = membershipKey; }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastname(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getNextTagsPath(int i) {
        return "";
    }

    public String getExtraIdentifiers() {
        return extraIdentifiers;
    }

    public void setExtraIdentifiers(String extraIdentifiers) {
        this.extraIdentifiers = extraIdentifiers;
    }

    public String getMobileAppUser() {
        return mobileAppUser;
    }

    public void setMobileAppUser(String mobileAppUser) {
        this.mobileAppUser = mobileAppUser;
    }

    public String getLocationEnabled() {
        return locationEnabled;
    }

    public void setLocationEnabled(String locationEnabled) {
        this.locationEnabled = locationEnabled;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
    public void setGovId(String govId) {
        this.govId = govId;
    }
    public String getGovId() {
        return govId;
    }
    public void export() {
        String xpath = "//*[@automationid=\'" + EXPORT_BUTTON + "\']";
        BaseService.getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
        BaseService.getDriverWait().until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        BaseService.findElementByAutomationId(EXPORT_BUTTON).click();
    }


    public ArrayList<ExportItemModel> getExports() {
        return exports;
    }

    public void setExports(boolean readExport) {
        if (readExport){
        this.exports = new ArrayList<ExportItemModel>();
        }
    }
}