package hub.common.objects.member;

import hub.base.BaseSeleniumObject;
import hub.base.BaseService;
import hub.base.SeleniumPath;
import org.openqa.selenium.WebElement;
import utils.StringUtils;

/**
 * Created by Goni on 02/14/2017.
 */
public class MemberDetails  extends BaseSeleniumObject {


    public static final String FIRST_NAME = "FirstName";
    public static final String LAST_NAME = "LastName";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String EMAIL = "Email";
    public static final String GENDER = "//form/div[contains(.,'Gender')]//select";
    public static final String BIRTHDAY = "//div[@automationid=\"Birthday\"]//*[@id=\"datePicker\"]";
    public static final String MEMBERSHIP_EXPIRATION = "//div[@automationid=\"ExpirationDate\"]//*[@id=\"datePicker\"]";
    public static final String ALLOW_SMS = "//input[@automationid=\"AllowSMS\"]";
    public static final String ALLOW_EMAIL = "//input[@automationid=\"AllowEmail\"]";
    public static final String GENERIC_STRING ="GenericString1";
    public static final String GENERIC_INTEGER ="GenericInteger1";
    public static final String USER_KEY = "//input[@ng-value=\"userinfo.UserKey\"]";
    public static final String MEMBERSHIP_KEY = "//input[@ng-value=\"userinfo.Key\"]";
    public static final String UPDATE = "updateMember";
    public static final String UNSUBSCRIBE_SMS = "unsubscribeSms";
    public static final String UNSUBSCRIBE_EMAIL = "unsubscribeEmail";
    public static final String DELETE = "deleteMember";
    public static final String REFRESH = "refreshMember";
    public static final String CONSENT_FOR_HUB = "//input[@ng-value=\"userinfo.ConsentForHub|translate\"]";
    int counter;


    @SeleniumPath(value = FIRST_NAME, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = FIRST_NAME)
    private String firstName;
    @SeleniumPath(value = LAST_NAME, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = LAST_NAME)
    private String lastName;
    @SeleniumPath(value = PHONE_NUMBER, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = PHONE_NUMBER)
    private String phoneNumber;
    @SeleniumPath(value = EMAIL, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = EMAIL)
    private String email;
    @SeleniumPath(value = GENDER, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = GENDER)
    private String gender;
    @SeleniumPath(value = BIRTHDAY, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.DATE, readValue = BIRTHDAY)
    private String birhday;
    @SeleniumPath(value = MEMBERSHIP_EXPIRATION, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.DATE, readValue = MEMBERSHIP_EXPIRATION)
    private String membershipExpiration;
    @SeleniumPath(value = GENERIC_STRING, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = GENERIC_STRING)
    private String genericString;
    @SeleniumPath(value = GENERIC_INTEGER, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = GENERIC_INTEGER)
    private String genericInteger;
    @SeleniumPath(value = ALLOW_SMS, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = ALLOW_SMS)
    private String allowSMS;
    @SeleniumPath(value = ALLOW_EMAIL, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = ALLOW_EMAIL)
    private String allowEmail;
    @SeleniumPath(value = USER_KEY, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = USER_KEY)
    private String  userKey;
    @SeleniumPath(value = MEMBERSHIP_KEY, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = MEMBERSHIP_KEY)
    private String membershipKey;
    @SeleniumPath(value = CONSENT_FOR_HUB, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = CONSENT_FOR_HUB)
    private String consentForHub;
    @SeleniumPath(value = UPDATE, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String updateButton;
    @SeleniumPath(value = UNSUBSCRIBE_SMS, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_AND_APPROVE)
    private Boolean unsubscribeSMS;
    @SeleniumPath(value = UNSUBSCRIBE_EMAIL, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_AND_APPROVE)
    private Boolean unsubscribeEmail;
    @SeleniumPath(value = DELETE, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK_AND_APPROVE)
    private String deleteButton;
    @SeleniumPath(value = REFRESH, type = SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String refreshButton;

    public String getBirhday() {
        return birhday;
    }

    public void setBirhday(String birhday) {
        this.birhday = birhday;
    }
    public String getFirstName(){
        return readField(this.firstName,"firstName");

    }
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }
    public String getLastName()
    {
        return readField(this.lastName,"lastName");
    }

    public void setLastname(String lastName) {this.lastName = lastName;}
    public String getPhoneNumber() { return readField(this.phoneNumber,"phoneNumber");}
    public void setPhoneNumber(String phoneNumber) {this.phoneNumber = phoneNumber;}
    public String getGender() {return gender;}
    public void setGender(String gender) {this.gender = gender;}
    public String getEmail() { return readField(this.email,"email");}
    public void setEmail(String email) {this.email = email;}
    public String getMembershipExpiration() {
        return membershipExpiration;
    }
    public void setMembershipExpiration(String membershipExpiration) { this.membershipExpiration = membershipExpiration; }
    public String getAllowEmail() { return allowEmail; }
    public void setAllowEmail(String allowEmail) { this.allowEmail = allowEmail; }
    public String getAllowSMS() { return allowSMS; }
    public void setAllowSMS(String allowSMS) { this.allowSMS = allowSMS; }

    public String getGenericString() {
        return genericString;
    }

    public void setGenericString(String genericString) {
        this.genericString = genericString;
    }


    public String getGenericInteger() {
        return genericInteger;
    }

    public void setGenericInteger(String genericInteger) {
        this.genericInteger = genericInteger;
    }

    public void setUpdate(boolean update){
        if (update)
            updateButton="update clicked";
        else
            updateButton = null;
    }

    public boolean isUnsubscribeSMSEnabled() {
        WebElement element = BaseService.findElementByAutomationId(UNSUBSCRIBE_SMS);
        return element.isEnabled();
    }

    public void clickUnsubscribeSMS() {
        this.unsubscribeSMS = true;
    }

    public boolean isUnsubscribeEmailEnabled() {

        WebElement element = BaseService.findElementByAutomationId(UNSUBSCRIBE_EMAIL);
        return element.isEnabled();
    }

    public void clickUnsubscribeEmail() {
        this.unsubscribeEmail = true;
    }

    public String getConsentForHub() {
        return consentForHub;
    }

    public void setConsentForHub(String consentForHub) {
        this.consentForHub = consentForHub;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getMembershipKey() {
        return membershipKey;
    }

    public void setMembershipKey(String membershipKey) {
        this.membershipKey = membershipKey;
    }

    public void setDelete(boolean delete) {
        if (delete)
            deleteButton="delete clicked";
        else
            deleteButton = null;
    }
    public void setRefresh(boolean refresh) {
        if (refresh)
            refreshButton="refresh clicked";
        else
            refreshButton = null;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}
}
