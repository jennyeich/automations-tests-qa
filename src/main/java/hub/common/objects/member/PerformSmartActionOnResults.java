package hub.common.objects.member;

public enum PerformSmartActionOnResults {

    SendAnAsset("Send an Asset"),
    SendALotteryRewared("Send a Lottery Reward"),
    AddPointsOrCredit("Add Points/Credit"),
    PunchAPunchCard("Punch the Punch Card"),
    TagUntag("Tag/Untag Member"),
    UpdateExpirationDate("Update Expiration Date"),
    SendEmail("Send Email"),
    SendSMS("Send a Text Message (SMS)")
    ;

    private final String text;

    PerformSmartActionOnResults(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
