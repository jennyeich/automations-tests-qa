package hub.common.objects.member;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by lior on 5/8/17.
 */

public class FilterBySQL extends BaseSeleniumObject {
    public static final String FILTER_BY_SQL_BUTTON = "//button[contains(.,'Filter Members')]";

    @SeleniumPath(value = FILTER_BY_SQL_BUTTON, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT, readValue = FILTER_BY_SQL_BUTTON)
    private String filterBySqlButton;



    public String getFilterBySqlButton() {
        return filterBySqlButton;
    }

    public void setFilterBySqlButton(String filterBySqlButton) {
        this.filterBySqlButton = filterBySqlButton;
    }

    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
