package hub.common.objects.member;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Jenny on 12/2/2016.
 */
public class NewMember extends BaseSeleniumObject {

    public static final String CLICK_ADD_NEW_MEMBER = "nav-item-newuser";
    public static final String FIRST_NAME = "new_user_FirstName";
    public static final String LAST_NAME = "new_user_LastName";
    public static final String PHONE_NUMBER = "new_user_PhoneNumber";
    public static final String GENDER = "//select[@id='new_user_Gender']";
    public static final String EMAIL = "new_user_Email";
    public static final String ExtClubID = "new_user_ExtClubMemberID";
    public static final String ADDRESS_HOME = "new_user_AddressHome";
    public static final String ADDRESS_FLOOR = "new_user_AddressFloor";
    public static final String ADDRESS_STREET = "new_user_AddressStreet";
    public static final String ADDRESS_LINE1 = "new_user_AddressLine1";
    public static final String ADDRESS_LINE2 = "new_user_AddressLine2";
    public static final String ALLOW_SMS = "new_user_AllowSMS";
    public static final String ALLOW_EMAIL = "new_user_AllowEmail";


    public static final String GOV_ID = "new_user_GovID";
    public static final String BIRTHDAY = "//div/div[@id=\"new_user_Birthday\"]/div//*[@id=\"datePicker\"]";
    public static final String MEMBERSHIP_EXPIRATION = "//div/div[@id=\"new_user_ExpirationDate\"]/div//*[@id=\"datePicker\"]";
    public static final String GENERIC_STRING_1 = "//input[@id=\"new_user_GenericString1\"]";
    public static final String GENERIC_STRING_2 = "new_user_GenericString2";
    public static final String GENERIC_INTEGER_1 = "new_user_GenericInteger1";
    public static final String GENERIC_INTEGER_2 = "//select[@id='new_user_GenericInteger2']";
    public static final String HOME_BRANCH_ID = "new_user_HomeBranchID";
    public static final String ANNIVERSARY = "//div/div[@id=\"new_user_Anniversary\"]/div//*[@id=\"datePicker\"]";
    public static final String ADDRESS_ZIP_CODE = "new_user_AddressZipCode";
    public static final String FAVORITE_BRANCH_ID = "new_user_FavoriteBranchID";
    public static final String CREATE = "//button[@automationid=\"save\"]";
    int counter;

    @SeleniumPath(value = CLICK_ADD_NEW_MEMBER, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT, readValue = "")
    private String createNewMember;
    @SeleniumPath(value = FIRST_NAME, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = FIRST_NAME)
    private String firstName;
    @SeleniumPath(value = LAST_NAME, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = LAST_NAME)
    private String lastName;
    @SeleniumPath(value = PHONE_NUMBER, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = PHONE_NUMBER)
    private String phoneNumber;
    @SeleniumPath(value = GENDER, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = GENDER)
    private String gender;
    @SeleniumPath(value = EMAIL, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = EMAIL)
    private String email;
    @SeleniumPath(value = BIRTHDAY, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.DATE, readValue = BIRTHDAY)
    private String birthday;
    @SeleniumPath(value = GOV_ID, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = GOV_ID)
    private String govId;
    @SeleniumPath(value = GENERIC_STRING_1, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = GENERIC_STRING_1)
    private String genericString1;
    @SeleniumPath(value = GENERIC_STRING_2, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.SELECT, readValue = GENERIC_STRING_2)
    private String genericString2;
    @SeleniumPath(value = GENERIC_INTEGER_1, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = GENERIC_INTEGER_1)
    private String genericInteger1;
    @SeleniumPath(value = GENERIC_INTEGER_2, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = GENERIC_INTEGER_2)
    private String genericInteger2;
    @SeleniumPath(value = MEMBERSHIP_EXPIRATION, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.DATE, readValue = MEMBERSHIP_EXPIRATION)
    private String membershipExpiration;
    @SeleniumPath(value = HOME_BRANCH_ID, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = HOME_BRANCH_ID)
    private String homeBranchId;
    @SeleniumPath(value = ANNIVERSARY, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.DATE, readValue = ANNIVERSARY)
    private String anniversary;
    @SeleniumPath(value = ADDRESS_ZIP_CODE, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = ADDRESS_ZIP_CODE)
    private String addressZipCode;
    @SeleniumPath(value = FAVORITE_BRANCH_ID, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = FAVORITE_BRANCH_ID)
    private String favoriteBranchId;
    @SeleniumPath(value = ExtClubID, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = ExtClubID)
    private String extClubID;
    @SeleniumPath(value = ADDRESS_HOME, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = ADDRESS_HOME)
    private String addressHome;
    @SeleniumPath(value = ADDRESS_FLOOR, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = ADDRESS_FLOOR)
    private String addressFloor;
    @SeleniumPath(value = ADDRESS_STREET, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = ADDRESS_STREET)
    private String addressStreet;
    @SeleniumPath(value = ADDRESS_LINE1, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = ADDRESS_LINE1)
    private String addressLine1;
    @SeleniumPath(value = ADDRESS_LINE2, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.INPUT_FOCUS, readValue = ADDRESS_LINE2)
    private String addressLine2;
    @SeleniumPath(value = ALLOW_SMS, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = ALLOW_SMS)
    private String allowSMS;
    @SeleniumPath(value = ALLOW_EMAIL, type = SeleniumPath.Type.BY_ID, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = ALLOW_EMAIL)
    private String allowEmail;
    @SeleniumPath(value = CREATE, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_WAIT, readValue = "")
    private String clickMember;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastname(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getMembershipExpiration() {
        return membershipExpiration;
    }

    public void setMembershipExpiration(String membershipExpiration) {
        this.membershipExpiration = membershipExpiration;
    }

    public String getNextTagsPath(int i) {
        return "";
    }

    public void setNext(int i) {
        this.counter = i;
    }

    public int getNext() {
        return counter;
    }

    public int getNextTag() {
        return 0;
    }

    public String getGenericString1() {
        return genericString1;
    }

    public void setGenericString1(String genericString) {
        this.genericString1 = genericString;
    }

    public String getGenericInteger1() {
        return genericInteger1;
    }

    public void setGenericInteger1(String genericInteger1) {
        this.genericInteger1 = genericInteger1;
    }

    public void setGenericInteger2(String genericInteger2) {
        this.genericInteger2 = genericInteger2;
    }

    public String getGenericString2() {
        return genericString2;
    }

    public void setGenericString2(String genericString2) {
        this.genericString2 = genericString2;
    }

    public String getHomeBranchId() {
        return homeBranchId;
    }

    public void setHomeBranchId(String homeBranchId) {
        this.homeBranchId = homeBranchId;
    }

    public String getAnniversary() {
        return anniversary;
    }

    public void setAnniversary(String anniversary) {
        this.anniversary = anniversary;
    }

    public String getAddressZipCode() {
        return addressZipCode;
    }

    public void setAddressZipCode(String addressZipCode) {
        this.addressZipCode = addressZipCode;
    }

    public String getFavoriteBranchId() {
        return favoriteBranchId;
    }

    public void setFavoriteBranchId(String favoriteBranchId) {
        this.favoriteBranchId = favoriteBranchId;
    }

    public String getExtClubID() {
        return extClubID;
    }

    public void setExtClubID(String extClubID) {
        this.extClubID = extClubID;
    }

    public String getAddressHome() {
        return addressHome;
    }

    public void setAddressHome(String addressHome) {
        this.addressHome = addressHome;
    }

    public String getAddressFloor() {
        return addressFloor;
    }

    public void setAddressFloor(String addressFloor) {
        this.addressFloor = addressFloor;
    }

    public String getAddressStreet() {
        return addressStreet;
    }

    public void setAddressStreet(String addressStreet) {
        this.addressStreet = addressStreet;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAllowSMS() {
        return allowSMS;
    }

    public void setAllowSMS(String allowSMS) {
        this.allowSMS = allowSMS;
    }

    public String getAllowEmail() {
        return allowEmail;
    }

    public void setAllowEmail(String allowEmail) {
        this.allowEmail = allowEmail;
    }


    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public void setGovId(String govId) {
        this.govId = govId;
    }

    public String getGovId() {
        return govId;
    }
}