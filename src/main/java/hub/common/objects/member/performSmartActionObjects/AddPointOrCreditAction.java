package hub.common.objects.member.performSmartActionObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.util.ArrayList;

public class AddPointOrCreditAction extends BaseSeleniumObject {

    public static final String NUMBER_TO_ADD = "//input[starts-with(@id,\"addPoints_amount_\")]";
    public static final String TYPE_POINTS = "//select[@id[contains(.,'addPoints_budgetType_')]]/optgroup/option[1]";
    public static final String TYPE_CREDIT = "//select[@id[contains(.,'addPoints_budgetType_')]]/optgroup/option[2]";
    public static final String ADD_TAGS = "//input[@ng-model=\"newTag.text\"]";
    public static final String PERFORM_ACTION_BUTTON = "//button[@ng-if=\"ctrl.selectedAction.cronDelay.schedulingInit == 'immediate'\"]";


    @SeleniumPath(value = NUMBER_TO_ADD, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = NUMBER_TO_ADD)
    private String numberToAdd;
    @SeleniumPath(value = TYPE_POINTS, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = TYPE_POINTS)
    private String typePoints;
    @SeleniumPath(value = TYPE_CREDIT, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = TYPE_CREDIT)
    private String typeCredit;
    @SeleniumPath(value = ADD_TAGS, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = ADD_TAGS)
    private ArrayList<String> addTags;
    @SeleniumPath(value = PERFORM_ACTION_BUTTON, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_ALERT)
    private String performActionButton;

    Timing timing;


    public String getNumberToAdd() {
        return numberToAdd;
    }

    public void setNumberToAdd(String numberToAdd) {
        this.numberToAdd = numberToAdd;
    }

    public String getTypePoints() {
        return typePoints;
    }

    public void setTypePoints(String typePoints) {
        this.typePoints = typePoints;
    }

    public String getTypeCredit() {
        return typeCredit;
    }

    public void setTypeCredit(String typeCredit) {
        this.typeCredit = typeCredit;
    }

    public ArrayList<String> getAddTags() {
        return addTags;
    }

    public void setAddTags(ArrayList<String> addTags) {
        this.addTags = addTags;
    }

    public Timing getTiming() {
        return timing;
    }

    public void setTiming(Timing timing) {
        this.timing = timing;
    }

    public String getPerformActionButton() {
        return performActionButton;
    }

    public void setPerformActionButton(String performActionButton) {
        this.performActionButton = performActionButton;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
