package hub.common.objects.member.performSmartActionObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.util.ArrayList;

public class TagOrUnTagAction extends BaseSeleniumObject {

    public static final String TAG_TEXT = "//input[@ng-model=\"fieldParent[fieldName]\" and @id[contains(.,'tagMember_tag_')]]";
    public static final String ACTION_TAG= "//input[@value=\"Tag\"]";
    public static final String ACTION_UNTAG= "//input[@value=\"UnTag\"]";
    public static final String ADD_TAGS = "//input[@ng-model=\"newTag.text\"]";
    public static final String PERFORM_ACTION_BUTTON = "//button[@ng-if=\"ctrl.selectedAction.cronDelay.schedulingInit == 'immediate'\"]";


    @SeleniumPath(value = TAG_TEXT, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TAG_TEXT)
    private String tagUntagText;
    @SeleniumPath(value = ACTION_TAG, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = ACTION_TAG)
    private String actionTag;
    @SeleniumPath(value = ACTION_UNTAG, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = ACTION_UNTAG)
    private String actionUntag;
    @SeleniumPath(value = ADD_TAGS, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = ADD_TAGS)
    private ArrayList<String> addTags;
    @SeleniumPath(value = PERFORM_ACTION_BUTTON, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_ALERT)
    private String performActionButton;

    Timing timing;


    public String getTagUntagText() {
        return tagUntagText;
    }

    public void setTagUntagText(String tagUntagText) {
        this.tagUntagText = tagUntagText;
    }

    public String getActionTag() {
        return actionTag;
    }

    public void setActionTag(String actionTag) {
        this.actionTag = actionTag;
    }

    public String getActionUntag() {
        return actionUntag;
    }

    public void setActionUntag(String actionUntag) {
        this.actionUntag = actionUntag;
    }

    public ArrayList<String> getAddTags() {
        return addTags;
    }

    public void setAddTags(ArrayList<String> addTags) {
        this.addTags = addTags;
    }

    public Timing getTiming() {
        return timing;
    }

    public void setTiming(Timing timing) {
        this.timing = timing;
    }

    public String getPerformActionButton() {
        return performActionButton;
    }

    public void setPerformActionButton(String performActionButton) {
        this.performActionButton = performActionButton;
    }


    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
