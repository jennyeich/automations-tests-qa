package hub.common.objects.member.performSmartActionObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Timing;

import java.util.ArrayList;

public class SendEmailAction extends BaseSeleniumObject {

    public static final String CHOOSE_TEMPLATE = "mailTemplateSelect";
    public static final String SUBJECT = "mailTemplateSubject";
    public static final String FROM = "mailTemplateFrom";
    public static final String TAGS = "//*[@ng-model=\"newTag.text\"]";
    public static final String TIMING = "//*[@ng-model =\"CronCtrl.ngModel.schedulingInit\"]";

    public static final String PERFORM_ACTION_BUTTON = "//button[contains(text(),\"Perform Action\")]";



    @SeleniumPath(value = CHOOSE_TEMPLATE, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.SELECT_WITH_WAIT, readValue = CHOOSE_TEMPLATE)
    private String templateName;
    @SeleniumPath(value = SUBJECT, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = SUBJECT)
    private String subject;
    @SeleniumPath(value = FROM, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = FROM)
    private String from;
    @SeleniumPath(value = TAGS, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = TAGS)
    private ArrayList<String> tags;

    @SeleniumPath(value = PERFORM_ACTION_BUTTON, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_ALERT)
    private String performActionButton;


    Timing timing;
    public int tagCounter;
    public int counter;

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateName() {
        return templateName;
    }

    public String getSubject() {
        return subject;
    }

    public String getFrom() {
        return from;
    }

    public Timing getTiming() {
        return timing;
    }

    public void setTiming(Timing timing) {
        this.timing = timing;
    }

    public String getPerformActionButton() {
        return performActionButton;
    }

    public void setPerformActionButton(String performActionButton) {
        this.performActionButton = performActionButton;
    }







    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
