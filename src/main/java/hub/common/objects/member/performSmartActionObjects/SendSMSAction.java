package hub.common.objects.member.performSmartActionObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Timing;

import java.util.ArrayList;

public class SendSMSAction extends BaseSeleniumObject {

    public static final String TEXT_MESSAGE = "mention";
    public static final String TAGS = "//*[@ng-model=\"newTag.text\"]";
    public static final String PERFORM_ACTION_BUTTON = "//button[contains(text(),\"Perform Action\")]";


    @SeleniumPath(value = TEXT_MESSAGE, type = SeleniumPath.Type.BY_AUTOMATION_ID, elementInputType = SeleniumPath.ElementInputType.TEXT_AREA, readValue = TEXT_MESSAGE)
    private String textMessage;
    @SeleniumPath(value = TAGS, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = TAGS)
    private ArrayList<String> tags;
    @SeleniumPath(value = PERFORM_ACTION_BUTTON, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_ALERT)
    private String performActionButton;


    Timing timing;
    public int tagCounter;
    public int counter;

    public Timing getTiming() {
        return timing;
    }
    public void setTiming(Timing timing) {
        this.timing = timing;
    }
    public String getTextMessage() { return textMessage; }
    public void setTextMessage(String textMessage) { this.textMessage = textMessage; }
    public ArrayList<String> getTags() { return tags; }
    public void setTags(ArrayList<String> tags) { this.tags = tags; }

    public String getPerformActionButton() {
        return performActionButton;
    }
    public void setPerformActionButton(String performActionButton) {
        this.performActionButton = performActionButton;
    }



    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
