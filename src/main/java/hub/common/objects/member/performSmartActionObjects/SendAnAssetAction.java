package hub.common.objects.member.performSmartActionObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.util.ArrayList;

public class SendAnAssetAction extends BaseSeleniumObject {

    public static final String ASSETS_LIST_DROPDOWN = "//select[@ng-options=\"a.assetValue as a.title + (a.active == 0 ? ' ('+translate.instant('asset_pseudostatus.Deactivated')+')' : '') group by translate.instant('group_label.' + (a.prepaid == 'Y' ? 'prepaid' : a.type)) for a in assets\"]";
    public static final String SEND_PUSH_NOTIFICATION_CHECKBOX = "//input[@type=\"checkbox\" and @ng-model=\"fieldParent[fieldName]\"]";
    public static final String ADD_TAGS = "//input[@ng-model=\"newTag.text\"]";
    public static final String PERFORM_ACTION_BUTTON = "//button[@ng-if=\"ctrl.selectedAction.cronDelay.schedulingInit == 'immediate'\"]";
    public static final String ASSETS_LIST_DROPDOWN_CLICK = "//select[@ng-model=\"fieldParent[fieldName]\"]";



    @SeleniumPath(value = ASSETS_LIST_DROPDOWN, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = ASSETS_LIST_DROPDOWN)
    private String assetsListDropdown;
    @SeleniumPath(value = ASSETS_LIST_DROPDOWN_CLICK, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = ASSETS_LIST_DROPDOWN_CLICK)
    private String assetsListDropdownClick;
    @SeleniumPath(value = SEND_PUSH_NOTIFICATION_CHECKBOX, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = SEND_PUSH_NOTIFICATION_CHECKBOX)
    private String sendPushNotificationCheckbox;
    @SeleniumPath(value = ADD_TAGS, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = ADD_TAGS)
    private ArrayList<String> addTags;
    @SeleniumPath(value = PERFORM_ACTION_BUTTON, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_ALERT)
    private String performActionButton;

    Timing timing;



    public String getAssetsListDropdown() {
        return assetsListDropdown;
    }

    public void setAssetsListDropdown(String assetsListDropdown) {
        this.assetsListDropdown = assetsListDropdown;
    }

    public String getSendPushNotificationCheckbox() {
        return sendPushNotificationCheckbox;
    }

    public void setSendPushNotificationCheckbox(String sendPushNotificationCheckbox) {
        this.sendPushNotificationCheckbox = sendPushNotificationCheckbox;
    }

    public ArrayList<String> getAddTags() {
        return addTags;
    }

    public void setAddTags(ArrayList<String> addTags) {
        this.addTags = addTags;
    }

    public Timing getTiming() {
        return timing;
    }

    public void setTiming(Timing timing) {
        this.timing = timing;
    }

    public String getPerformActionButton() {
        return performActionButton;
    }

    public void setPerformActionButton(String performActionButton) {
        this.performActionButton = performActionButton;
    }

    public String getAssetsListDropdownClick() {
        return assetsListDropdownClick;
    }

    public void setAssetsListDropdownClick(String assetsListDropdownClick) {
        this.assetsListDropdownClick = assetsListDropdownClick;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
