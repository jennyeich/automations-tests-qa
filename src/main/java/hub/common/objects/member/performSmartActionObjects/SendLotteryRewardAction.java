package hub.common.objects.member.performSmartActionObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.util.ArrayList;

public class SendLotteryRewardAction extends BaseSeleniumObject {

    public static final String ASSETS_LIST_DROPDOWN = "//select[@ng-options=\"a.id as a.name for a in items\"]";
    public static final String ADD_TAGS = "//input[@ng-model=\"newTag.text\"]";
    public static final String PERFORM_ACTION_BUTTON = "//button[@ng-if=\"ctrl.selectedAction.cronDelay.schedulingInit == 'immediate'\"]";


    @SeleniumPath(value = ASSETS_LIST_DROPDOWN, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = ASSETS_LIST_DROPDOWN)
    private String assetsListDropdown;
    @SeleniumPath(value = ADD_TAGS, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = ADD_TAGS)
    private ArrayList<String> addTags;
    @SeleniumPath(value = PERFORM_ACTION_BUTTON, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE_ALERT)
    private String performActionButton;

    Timing timing;


    public String getAssetsListDropdown() {
        return assetsListDropdown;
    }

    public void setAssetsListDropdown(String assetsListDropdown) {
        this.assetsListDropdown = assetsListDropdown;
    }

    public ArrayList<String> getAddTags() {
        return addTags;
    }

    public void setAddTags(ArrayList<String> addTags) {
        this.addTags = addTags;
    }

    public Timing getTiming() {
        return timing;
    }

    public void setTiming(Timing timing) {
        this.timing = timing;
    }

    public String getPerformActionButton() {
        return performActionButton;
    }

    public void setPerformActionButton(String performActionButton) {
        this.performActionButton = performActionButton;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
