package hub.common.objects.member.performSmartActionObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.common.TimingUnit;

public class Timing extends BaseSeleniumObject {

    public static final String TIMING_TYPES = "//select[@ng-model=\"CronCtrl.ngModel.schedulingInit\"]";
    public static final String VALUE_INPUT = "//input[@ng-model=\"CronCtrl.ngModel.schedulingValue\"]";
    public static final String TIMING_UNITS = "//select[@ng-model=\"CronCtrl.ngModel.schedulingType\"]";


    @SeleniumPath(value = TIMING_TYPES, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = TIMING_TYPES)
    private String timingTypes;
    @SeleniumPath(value = VALUE_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = VALUE_INPUT)
    private String valueInput;
    @SeleniumPath(value = TIMING_UNITS, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = TIMING_UNITS)
    private TimingUnit timingUnits;


    public String getTimingTypes() {
        return timingTypes;
    }

    public void setTimingTypes(String timingTypes) {
        this.timingTypes = timingTypes;
    }

    public String getValueInput() {
        return valueInput;
    }

    public void setValueInput(String valueInput) {
        this.valueInput = valueInput;
    }

    public TimingUnit getTimingUnits() {
        return timingUnits;
    }

    public void setTimingUnits(TimingUnit timingUnits) {
        this.timingUnits = timingUnits;
    }

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
