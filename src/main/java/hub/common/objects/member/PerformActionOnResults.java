package hub.common.objects.member;

/**
 * Created by Jenny on 12/4/2016.
 */
public enum PerformActionOnResults {
    SendAPushNotification("Send a Push Notification"),
    SendAGiftOrPunch("Send a Gift or Punch Card"),
    SendALotteryReward("Send a Lottery Reward"),
    AddPoints("Add Points/Credit"),
    AutoPunch("Autopunch"),
    TagUntag("Tag/Untag"),
    AddAmountToCredit("Add Amount to Credit")
    ;

    private final String text;

    PerformActionOnResults(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
