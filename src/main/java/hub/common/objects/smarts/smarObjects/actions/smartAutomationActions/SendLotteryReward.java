package hub.common.objects.smarts.smarObjects.actions.smartAutomationActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;

import java.util.ArrayList;

/**
 * Created by Jenny on 12/20/2016.
 */
public class SendLotteryReward extends BaseSeleniumObject implements ISmartAction {

    public static final String CHOOSE_ASSET = "(//select[starts-with(@id,'giveRandomReward_')])";
    public static final String TAGS = "(//*[@ng-model=\"newTag.text\"])";
    public int counter;
    public  int tagCounter;


    @SeleniumPath(value = CHOOSE_ASSET, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = CHOOSE_ASSET)
    String assetName;
    @SeleniumPath(value = TAGS, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = TAGS)
    private ArrayList<String> tags;

    Occurrences occurrences;
    Timing timing;

    public Timing getTiming() {
        return timing;
    }

    public void setTiming(Timing timing) {
        this.timing = timing;
    }
    public String getAssetName() {return assetName;}
    public void setAssetName(String assetName) {this.assetName = assetName;}
    public void setTags(ArrayList<String> tags) {this.tags = tags;}
    public Occurrences getOccurrences() {return occurrences;}
    public void setOccurrences(Occurrences occurrences) {this.occurrences = occurrences;}
    public void setNextTag (int i) {this.tagCounter = i;}
    public int getNextTag () {return tagCounter;}
    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter = i;}
    public int getNext (){return counter;}
    public ArrayList<String> getTags() {return tags;}

    public SendLotteryReward(ISmartAutomationCounter automation){

        setNext(automation.getSendLotteryCount());
        automation.setSendLotteryCount(automation.getSendLotteryCount() + 1);
    }

    public void setTags(ArrayList<String> tags,ICounter automation) {
        this.tags = tags;
        setNextTag(automation.getTagsCounter());
        automation.setTagsCounter(automation.getTagsCounter()+1);
    }



    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
