package hub.common.objects.smarts.smarObjects.actions.smartAutomationActions;

/**
 * Created by Jenny on 11/27/2016.
 */
public enum AmountType {
    Points("Points"),
    Credit("Credit")
    ;

    private final String name;

    AmountType(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }



}
