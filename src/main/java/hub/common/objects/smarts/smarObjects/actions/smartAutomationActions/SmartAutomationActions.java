package hub.common.objects.smarts.smarObjects.actions.smartAutomationActions;

/**
 * Created by Jenny on 11/27/2016.
 */
public enum SmartAutomationActions {


    SendAnAsset("Send an Asset"),
    SendALotteryReward("Send a Lottery Reward"),
    SendAPushNotification("Send a Push Notification"),
    AddSetNumberofPoints("Add Points/Credit"),
    PunchThePunchCard("Punch the Punch Card"),
    TagMember("Tag/Untag Member"),
    SendPopUPMessage("Send a Pop-up Message"),
    SendTextSMS("Send a Text Message (SMS)"),
    RegisterMembership("Register Member"),
    UnregisterMembership("Unregister Member"),
    ExportEvent("Export Event"),
    UpdateExpirationDate("Update Expiration Date"),
    SendEmail("Send Email")
    ;

    private final String name;
    SmartAutomationActions(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }



}
