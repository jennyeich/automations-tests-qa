package hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ISmartAssetCounter;

import java.util.ArrayList;

/**
 * Created by Jenny on 11/27/2016.
 */
public class AmountOff extends BaseSeleniumObject implements IDiscountType{


    int counter;


    AmountOffNumberText amountOffNumber;
    SetDiscountOnItemsCheckBox setDiscountOnItemsCheckBox;
    LimitOfferLimitedToTimes limitOfferLimitedToTimes;

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    ArrayList <SetDiscountOnItems> discountOnItems;

    public String getNextTagsPath (int i){return  "";}
    public ArrayList<SetDiscountOnItems> getDiscountOnItems() {
        if(discountOnItems == null)
            discountOnItems = new ArrayList<>();
        return discountOnItems;
    }


    public void addSetDiscountOnItems(SetDiscountOnItems setDiscountOnItems, ISmartAssetCounter automation){

        setDiscountOnItems.setNext(automation.getSetDisocuntOnItemsCount());
        getDiscountOnItems().add(setDiscountOnItems);
        setDiscountOnItems.setCreateActionCounter(automation.getActionCount());
        automation.setSetDisocuntOnItemsCount(automation.getSetDisocuntOnItemsCount()+1);

    }

    public void setDiscountOnItems(ArrayList<SetDiscountOnItems> discountOnItems) {this.discountOnItems = discountOnItems;}

    public SetDiscountOnItemsCheckBox getSetDiscountOnItemsCheckBox() {return setDiscountOnItemsCheckBox;}
    public void setSetDiscountOnItemsCheckBox(SetDiscountOnItemsCheckBox setDiscountOnItemsCheckBox) {this.setDiscountOnItemsCheckBox = setDiscountOnItemsCheckBox;}
    public AmountOffNumberText getAmountOffNumber() {return amountOffNumber;}
    public void setAmountOffNumber(AmountOffNumberText amountOffNumber) {this.amountOffNumber = amountOffNumber;}
    public LimitOfferLimitedToTimes getLimitOfferLimitedToTimes() {return limitOfferLimitedToTimes;}
    public void setLimitOfferLimitedToTimes(LimitOfferLimitedToTimes limitOfferLimitedToTimes) {this.limitOfferLimitedToTimes = limitOfferLimitedToTimes;}

    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    public AmountOff(ISmartAssetCounter smartAsset){
        setNext(smartAsset.getAmountOffCount());
        smartAsset.setAmountOffCount(smartAsset.getAmountOffCount()+ 1);
    }


    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
