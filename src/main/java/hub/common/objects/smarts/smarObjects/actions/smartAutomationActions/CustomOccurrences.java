package hub.common.objects.smarts.smarObjects.actions.smartAutomationActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Group;

/**
 * Created by Jenny on 12/20/2016.
 */
public class CustomOccurrences extends BaseSeleniumObject {


    public static final String CUSTOM_OCCURRENCES_FOR_EACH = "(//div[@class=\"column small-2\"]/div/div/div/input)";
    public static final String FOREACH_QUANTITY_TYPE = "(//*[@ng-model=\"smartActionsParamsGroupCtrl.ngModel\"])";
    public static final String IN_THE_GROUP = "(//*[@id=\"calcConditionId_\"])";
    public static final String LIMIT_ACTION_OCCURRENCES = "(//*[starts-with(@id,'displayAmountLimit')])";
    public static final String LIMIT_TO_TIMES = "(//*[@id=\"amountLimit\"])";

    int counter;


    @SeleniumPath(value = CUSTOM_OCCURRENCES_FOR_EACH, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = CUSTOM_OCCURRENCES_FOR_EACH)
    private String customOccurrences_forEach;
    @SeleniumPath(value = FOREACH_QUANTITY_TYPE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_XPATH, readValue = FOREACH_QUANTITY_TYPE)
    private String customOccurrences_forEachTotal;
    @SeleniumPath(value = LIMIT_ACTION_OCCURRENCES, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = LIMIT_ACTION_OCCURRENCES)
    private String limitActionOccurrencesPerAutomation;
    @SeleniumPath(value = LIMIT_TO_TIMES, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = LIMIT_TO_TIMES)
    private String limitToTimes;

    Group group;


    public String getCustomOccurrencesForEach() {return customOccurrences_forEach;}
    public void setCustomOccurrencesForEach(String customOccurrences_forEach) {this.customOccurrences_forEach = customOccurrences_forEach;}
    public String getCustomOccurrencesForEachTotal() {return customOccurrences_forEachTotal;}
    public void setCustomOccurrencesForEachTotal(String customOccurrences_forEachTotal) {this.customOccurrences_forEachTotal = customOccurrences_forEachTotal;}
    public String getLimitActionOccurrencesPerAutomation() {return limitActionOccurrencesPerAutomation;}
    public void setLimitActionOccurrencesPerAutomation(String limitActionOccurrencesPerAutomation) {this.limitActionOccurrencesPerAutomation = limitActionOccurrencesPerAutomation;}
    public String getLimitToTimes() {return limitToTimes;}
    public void setLimitToTimes(String limitToTimes) {this.limitToTimes = limitToTimes;}
    public Group getGroup() {return group;}
    public void setGroup(Group group) {
        this.group = group;
    }
    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter = i;}
    public int getNext (){return counter;}
    public CustomOccurrences(ISmartAutomationCounter automation){
        setNext(automation.getCustomOccurrencesCounter());
        automation.setCustomOccurrencesCounter(automation.getCustomOccurrencesCounter()+ 1);
        automation.setShoppingCartAndCustomOccursCounter(automation.getShoppingCartAndCustomOccursCounter()+1);
    }
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
