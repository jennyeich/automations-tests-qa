package hub.common.objects.smarts.smarObjects.actions.smartGiftActions.dealItemCode;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.util.ArrayList;

/**
 * Created by Jenny on 12/1/2016.
 */
public class RelatedItemCode extends BaseSeleniumObject {


    public static final String ADD_DEAL_CODE = "//*[@ng-click=\"addItemCodesCtrl.addItem()\"]";
    public static final String DEAL_CODE_VALUE = "//*[@ng-model=\"item.code\"]";
    public static final String RELATED_DEAL_CODE = "(//*[@ng-model=\"newTag.text\"])[4]";

    @SeleniumPath(value = ADD_DEAL_CODE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    String addItemCode;
    @SeleniumPath(value = DEAL_CODE_VALUE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT)
    String itemCodeValue;
    @SeleniumPath(value = RELATED_DEAL_CODE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS)
    private ArrayList<String> itemCodes;

    public String getItemCodeValue() {return itemCodeValue;}

    public void setItemCodeValue(String itemCodeValue) {this.itemCodeValue = itemCodeValue;}

    public ArrayList<String> getItemCodes() {return itemCodes;}

    public void setItemCodes(ArrayList<String> itemCodes) {this.itemCodes = itemCodes;}

    public String getNextTagsPath (int i){return  "";}

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
