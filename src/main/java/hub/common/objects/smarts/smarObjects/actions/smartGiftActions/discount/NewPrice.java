package hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ISmartAssetCounter;

import java.util.ArrayList;

/**
 * Created by Jenny on 11/27/2016.
 */
public class NewPrice extends BaseSeleniumObject implements IDiscountType{

    int counter;


    AmountOffNumber newPriceNumber;
    LimitAmount limitAmount;
    SetDiscountOnItemsCheckBox setDiscountOnItemsCheckBox;
    LimitOfferLimitedToTimes limitOfferLimitedToTimes;

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    ArrayList <SetDiscountOnItems> discountOnItems;


    public ArrayList<SetDiscountOnItems> getDiscountOnItems() {
        if(discountOnItems == null)
            discountOnItems = new ArrayList<>();
        return discountOnItems;
    }


    public void addSetDiscountOnItems(SetDiscountOnItems setDiscountOnItems, ISmartAssetCounter automation){

        setDiscountOnItems.setNext(automation.getSetDisocuntOnItemsCount());
        getDiscountOnItems().add(setDiscountOnItems);
        setDiscountOnItems.setCreateActionCounter(automation.getActionCount());
        automation.setSetDisocuntOnItemsCount(automation.getSetDisocuntOnItemsCount()+1);

    }

    public void setDiscountOnItems(ArrayList<SetDiscountOnItems> discountOnItems) {this.discountOnItems = discountOnItems;}
    public LimitOfferLimitedToTimes getLimitOfferLimitedToTimes() {return limitOfferLimitedToTimes;}
    public void setLimitOfferLimitedToTimes(LimitOfferLimitedToTimes limitOfferLimitedToTimes) {this.limitOfferLimitedToTimes = limitOfferLimitedToTimes;}

    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    public NewPrice(ISmartAssetCounter smartAsset){
        setNext(smartAsset.getNewPriceCount());
        smartAsset.setNewPriceCount(smartAsset.getNewPriceCount()+ 1);
    }

    public LimitAmount getLimitAmount() {return limitAmount;}
    public void setLimitAmount(LimitAmount limitAmount) {this.limitAmount = limitAmount;}
    public AmountOffNumber getNewPriceNumber() {return newPriceNumber;}
    public void setNewPriceNumber(AmountOffNumber newPriceNumber) {this.newPriceNumber = newPriceNumber;}
    public SetDiscountOnItemsCheckBox getSetDiscountOnItemsCheckBox() {return setDiscountOnItemsCheckBox;}
    public void setSetDiscountOnItemsCheckBox(SetDiscountOnItemsCheckBox setDiscountOnItemsCheckBox) {this.setDiscountOnItemsCheckBox = setDiscountOnItemsCheckBox;}
    public String getNextTagsPath (int i){return  "";}


    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
