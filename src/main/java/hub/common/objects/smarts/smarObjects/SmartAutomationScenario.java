package hub.common.objects.smarts.smarObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.ApplyIf;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Condition;

import java.util.ArrayList;

/**
 * Created by Jenny on 11/24/2016.
 */
public class SmartAutomationScenario extends BaseSeleniumObject{

    public static final String ADD_SCENARIO = "//*[@ng-click=\"ctrlAEdit.addNewScenario();\"]";
    public static final String SCENARIO_NAME_CLICK_AND_EDIT = "(//*[@title=\"smartActionScenario.name\"]/div/div/div[1]/div/div[2]/p)";
    public static final String OR = "(//*[@ng-click=\"ctrlOpAndOr.ngModel = conditionOperator\"][2])";
    public int counter;

    @SeleniumPath(value = ADD_SCENARIO, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = ADD_SCENARIO)
    private String addScenario;
    @SeleniumPath(value = SCENARIO_NAME_CLICK_AND_EDIT, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_AND_INPUT, readValue = SCENARIO_NAME_CLICK_AND_EDIT)
    private String scenarioName;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    private ArrayList<Condition> conditions;
    @SeleniumPath(value = OR, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = OR)
    private String orOperator;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    private ArrayList<SmartAutomationAction> smartAutomationActions; //crate new automation action

    public String getScenarioName() {
        return scenarioName;
    }

    public void setScenarioName(String scenarioName) {
        this.scenarioName = scenarioName;

    }

    public void addCondition(Condition condition, ICounter automation){
        condition.setNext(automation.getConditionCount());
        getConditions().add(condition);
        condition.setCreateActionCounter(this.getNext()+1);
        automation.setConditionCount(automation.getConditionCount()+1);

        if (condition.getApplyIf()== ApplyIf.SHOPPING_CART)
            automation.setShoppingCartAndCustomOccursCounter(automation.getShoppingCartAndCustomOccursCounter()+1);

    }

    private ArrayList<Condition> getConditions() {
        if(conditions == null)
            conditions = new ArrayList<>();
        return conditions;
    }

    public void setConditions(ArrayList<Condition> conditions) {this.conditions = conditions;}
    public ArrayList<SmartAutomationAction> getSmartAutomationActions() {

        if(smartAutomationActions == null)
            smartAutomationActions = new ArrayList<>();
        return smartAutomationActions;

    }


    public void setSmartAutomationActions(ArrayList<SmartAutomationAction> smartAutomationActions) {this.smartAutomationActions = smartAutomationActions;}
    public  String getNextTagsPath(int i){
        return "";
    }

    public void setNext (int i) {
        this.counter=i;
        if (counter > 1) {
            addScenario="Y";
        }
    }
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}
    public String getOrOperator() {return orOperator;}
    public void setOrOperator() {this.orOperator = "true";}


    public void addAction(SmartAutomationAction action,ICounter automation){

        action.setNext(automation.getActionCount());
        getSmartAutomationActions().add(action);
        action.setCreateActionCounter(this.getNext());
        automation.setActionCount(automation.getActionCount()+1);
    }

    public SmartAutomationScenario (){
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
