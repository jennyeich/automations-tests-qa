package hub.common.objects.smarts.smarObjects.settings;


import hub.common.objects.smarts.ICounter;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Condition;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.ApplyLimitTimes;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import org.hsqldb.Trigger;

public interface ISettings {

     String getAutomationName();
    void setAutomationName(String s); //Automation name e.g:"Receives or Uses Points_"+ timestamp


    //void setTrigger(ITrigger t);
    ITrigger getTrigger();
    void setTrigger (ITrigger trigger);


    void setApplyLimitTimes(ApplyLimitTimes applyLimitTimes);

    void addCondition(Condition cond, ICounter automation);




}

