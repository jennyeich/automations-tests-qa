package hub.common.objects.smarts.smarObjects.actions.smartGiftActions.dealItemCode;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.util.ArrayList;

/**
 * Created by Jenny on 12/1/2016.
 */
public class AddItemCode extends BaseSeleniumObject{

    public static final String DEAL_CODES = "(//*[@ng-model=\"newTag.text\"])[3]";
    public static final String DEFINE_RELATED_ITEM = "//input[@automationid=\"checkboxRelatedItems\"]";
    public static final String READ_DEAL_CODES = "(//*[@ng-model=\"newTag.text\"])[3]";


    @SeleniumPath(value = DEAL_CODES, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS, readValue = READ_DEAL_CODES)
    private ArrayList<String> itemCodes;
    @SeleniumPath(value = DEFINE_RELATED_ITEM, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String defineRelatedItem;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    ArrayList <RelatedItemCode> relatedRelatedItemCodes;



    public String getDefineRelatedItem() {return defineRelatedItem;}
    public void setDefineRelatedItem(String defineRelatedItem) {this.defineRelatedItem = defineRelatedItem;}

    public ArrayList<String> getItemCodes() {

        if(itemCodes == null)
            itemCodes = new ArrayList<>();
        return itemCodes;

    }
    public void setItemCodes(ArrayList<String> itemCodes) {this.itemCodes = itemCodes;}
    public ArrayList<RelatedItemCode> getRelatedRelatedItemCodes() {

        if(relatedRelatedItemCodes == null)
            relatedRelatedItemCodes = new ArrayList<>();
        return relatedRelatedItemCodes;

    }
    public void setRelatedRelatedItemCodes(ArrayList<RelatedItemCode> relatedRelatedItemCodes) {
        this.relatedRelatedItemCodes = relatedRelatedItemCodes;
    }


    public String getNextTagsPath (int i){return  "";}

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
