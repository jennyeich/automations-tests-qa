package hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Group;


/**
 * Created by Jenny on 11/29/2016.
 */
public class SetDiscountOnItems extends BaseSeleniumObject {

    int counter;
    int createActionCounter;

    public static final String ADD_DISCOUNT = "(//P[@ng-click=\"DiscountCtrl.addDiscountCondition();\"])";
    public static final String FOR_EACH_SELECT = "(//select[starts-with(@id,'conditionType')])";
   // public static final String FOR_EACH_VALUE = "conditionValue_0";
    public static final String FOR_EACH_SPEND_VALUE = "(//*[@ng-model=\"condition.value\"])";
    public static final String DISCOUNT_ONN_OFF = "(//*[starts-with(@for,'conditionApplyDiscount_object')])";


    @SeleniumPath(value = ADD_DISCOUNT, type = SeleniumPath.Type.BY_GENERATE_NEXT,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    String addDsicount;
    @SeleniumPath(value = FOR_EACH_SELECT, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL)
    String forEachSelect;
  //  @SeleniumPath(value = FOR_EACH_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT)
  //  String forEachQuanityValue;
    @SeleniumPath(value = FOR_EACH_SPEND_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT)
    String forEachSpendValue;
    @SeleniumPath(value = DISCOUNT_ONN_OFF, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    String onnOffDiscount;

    Group group;

    public Group getGroup() {return group;}
    public void setGroup(Group group, ICounter smartGift) {
        this.group = group;
        group.setCreateActionCounter(this.getNext());
    }
    public String getForEachSelect() {
        return forEachSelect;
    }
    public void setForEachSelect(String forEachSelect) {
        this.forEachSelect = forEachSelect;
    }
    public String getForEachSpendValue() {return forEachSpendValue;}
    public void setForEachSpendValue(String forEachSpendValue) {
        this.forEachSpendValue = forEachSpendValue;
    }
   // public String getForEachQuanityValue() {return forEachQuanityValue;}
  //  public void setForEachQuanityValue(String forEachQuanityValue) {this.forEachQuanityValue = forEachQuanityValue;}
    public String getNextTagsPath (int i){return  "";}
    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}
    public String getOnnOffDiscount() {return onnOffDiscount;}
    public void setOnnOffDiscount(String onnOffDiscount) {this.onnOffDiscount = onnOffDiscount;}

    public SetDiscountOnItems(ICounter automation){
        automation.setShoppingCartAndCustomOccursCounter(automation.getShoppingCartAndCustomOccursCounter()+1);
    }


    public int getCreateActionCounter() {return createActionCounter;}
    public void setCreateActionCounter(int createActionCounter) {this.createActionCounter = createActionCounter;}
}
