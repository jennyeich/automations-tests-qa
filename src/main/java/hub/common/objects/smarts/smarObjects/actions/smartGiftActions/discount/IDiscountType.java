package hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount;

import hub.common.objects.smarts.ISmartAssetCounter;

/**
 * Created by Goni on 7/18/2017.
 */

public interface IDiscountType {

    public void addSetDiscountOnItems(SetDiscountOnItems setDiscountOnItems, ISmartAssetCounter smartAsset);
    public void setLimitOfferLimitedToTimes(LimitOfferLimitedToTimes limitOfferLimitedToTimes);
    public void setSetDiscountOnItemsCheckBox(SetDiscountOnItemsCheckBox setDiscountOnItemsCheckBox);
}
