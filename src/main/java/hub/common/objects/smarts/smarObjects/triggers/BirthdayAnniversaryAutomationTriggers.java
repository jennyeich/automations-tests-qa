package hub.common.objects.smarts.smarObjects.triggers;


public enum BirthdayAnniversaryAutomationTriggers implements ITrigger{

    MatchingBirthdayMonthOnly("Matching birthday month only"),
    MatchingBirthdayMonthAndDate("Matching birthday month and date"),
    MatchingAnniversaryMonthOnly("Matching anniversary month only"),
    MatchingAnniversaryMonthAndDate("Matching anniversary month and date")
    ;


    private final String name;
    BirthdayAnniversaryAutomationTriggers(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
