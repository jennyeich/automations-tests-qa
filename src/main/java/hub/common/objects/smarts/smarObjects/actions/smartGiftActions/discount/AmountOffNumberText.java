package hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ISmartAssetCounter;

/**
 * Created by Jenny on 3/7/2017.
 */
public class AmountOffNumberText extends BaseSeleniumObject {

    int counter;
    public static final String AMOUNT_OFF_NUMBER = "(//*[starts-with(@id, \"58\")]/div/div/div/div[2]/div[2]/input)";

    @SeleniumPath(value = AMOUNT_OFF_NUMBER, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT_TEXT, readValue = AMOUNT_OFF_NUMBER)
    String amountOffNumber;

    public String getAmountOffNumber() {return amountOffNumber;}
    public void setAmountOffNumber(String amountOffNumber) {this.amountOffNumber = amountOffNumber;}


    public AmountOffNumberText(ISmartAssetCounter smartAsset){
        setNext(smartAsset.getAmountOffNumberCount());
        smartAsset.setAmountOffNumberCount(smartAsset.getAmountOffNumberCount()+ 1);
    }

    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
