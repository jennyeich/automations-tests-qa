package hub.common.objects.smarts.smarObjects.actions.smartGiftActions.dealItemCode;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

import java.util.ArrayList;

/**
 * Created by Jenny on 12/1/2016.
 */
public class RelatedDealCode extends BaseSeleniumObject {


    public static final String ADD_DEAL_CODE = "//*[@ng-click=\"addDealCodesCtrl.addDeal()\"]";
    public static final String DEAL_CODE_VALUE = "//*[@ng-model=\"deal.code\"]";
    public static final String RELATED_DEAL_CODE = "(//*[@ng-model=\"newTag.text\"])[4]";

    @SeleniumPath(value = ADD_DEAL_CODE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    String addDealCode;
    @SeleniumPath(value = DEAL_CODE_VALUE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT)
    String dealCodeValue;
    @SeleniumPath(value = RELATED_DEAL_CODE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS)
    private ArrayList<String> dealCodes;


    public ArrayList<String> getDealCodes() {return dealCodes;}
    public void setDealCodes(ArrayList<String> dealCodes) {this.dealCodes = dealCodes;}
    public String getDealCodeValue() {return dealCodeValue;}
    public void setDealCodeValue(String dealCodeValue) {this.dealCodeValue = dealCodeValue;}
    public String getNextTagsPath (int i){return  "";}

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
