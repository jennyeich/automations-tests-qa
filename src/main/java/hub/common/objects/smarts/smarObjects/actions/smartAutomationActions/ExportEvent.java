package hub.common.objects.smarts.smarObjects.actions.smartAutomationActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;

import java.util.ArrayList;

/**
 * Created by Jenny on 12/20/2016.
 */
public class ExportEvent extends BaseSeleniumObject implements ISmartAction {

    public static final String NAME = "(//input[starts-with(@id,\"exportEvent_name_\")])";
    public static final String DESTINATION_URL = "(//input[@id=\"url_destination\"])";
    public static final String TAGS = "(//*[@ng-model=\"newTag.text\"])";
    public static int counter;
    public  int tagCounter;
    int i;

    @SeleniumPath(value = NAME, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = NAME)
    String name;
    @SeleniumPath(value = DESTINATION_URL, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = DESTINATION_URL)
    String url;
    @SeleniumPath(value = TAGS, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = TAGS)
    private ArrayList<String> tags;

    Timing timing;

    public ArrayList<String> getTags() {return tags;}
    @Override
    public void setTiming(Timing timing) {
        this.timing = timing;
    }

    @Override
    public void setOccurrences(Occurrences occurrences) {

    }

    public void setTags(ArrayList<String> tags, ICounter automation) {
        this.tags = tags;
        setNextTag(automation.getTagsCounter());
        automation.setTagsCounter(automation.getTagsCounter()+1);
    }

    public String getUrl() {return url;}
    public void setUrl(String url) {this.url = url;}
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}
    public void setNextTag (int i) {this.tagCounter = i;}
    public int getNextTag () {return tagCounter;}
    public String getNextTagsPath (int i){return "";}
    public void setNext (int counter) {this.counter = counter;}
    public int getNext (){return counter;}
    public ExportEvent(ISmartAutomationCounter automation){
        setNext(automation.getExportEventCount());
        automation.setExportEventCount(automation.getExportEventCount() + 1);
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
