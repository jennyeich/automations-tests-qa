package hub.common.objects.smarts.smarObjects.actions.smartAutomationActions;

/**
 * Created by Jenny on 11/27/2016.
 */
public enum ForEachTotalType {

    TotalQuantity("(//select[@automationid=\"dropdownOccurencesTotal\"]/option[@label=\"Total Quantity\"])"),
    TotalSum("(//select[@automationid=\"dropdownOccurencesTotal\"]/option[@label=\"Total Sum\"])");

    private final String name;

    ForEachTotalType(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }



}
