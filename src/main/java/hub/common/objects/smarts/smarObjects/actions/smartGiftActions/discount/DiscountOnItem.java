package hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount;

import hub.hub1_0.common.objects.smarts.smarObjects.conditions.DiscountItems_ForEach;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.GroupField;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.GroupOperators;

public class DiscountOnItem {

	private String itemName;
	private String timeStamp;
	private String onOff;
	private String count;
	private DiscountItems_ForEach discountItems_forEach;
	private GroupField groupField;
	private GroupOperators groupOperators;

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getOnOff() {
		return onOff;
	}

	public void setOnOff(String onOff) {
		this.onOff = onOff;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public DiscountItems_ForEach getDiscountItems_forEach() {
		return discountItems_forEach;
	}

	public void setDiscountItems_forEach(DiscountItems_ForEach discountItems_forEach) {
		this.discountItems_forEach = discountItems_forEach;
	}

	public GroupField getGroupField() {
		return groupField;
	}

	public void setGroupField(GroupField groupField) {
		this.groupField = groupField;
	}

	public GroupOperators getGroupOperators() {
		return groupOperators;
	}

	public void setGroupOperators(GroupOperators groupOperators) {
		this.groupOperators = groupOperators;
	}
}
