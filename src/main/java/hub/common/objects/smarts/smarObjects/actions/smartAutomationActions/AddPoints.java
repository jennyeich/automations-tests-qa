package hub.common.objects.smarts.smarObjects.actions.smartAutomationActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;

import java.util.ArrayList;

/**
 * Created by Jenny on 12/20/2016.
 */
public class AddPoints extends BaseSeleniumObject implements ISmartAction {

    public static final String NUMBER_OF_POINTS = "(//input[starts-with(@id,'addPoints_amount')])";
    public static final String AMOUNT_TYPE  = "(//select[starts-with(@id,'addPoints_budgetType')])";
    public static final String TAGS = "(//*[@ng-model=\"newTag.text\"])";
    int tagCounter;
    int counter;


    @SeleniumPath(value = NUMBER_OF_POINTS, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = NUMBER_OF_POINTS)
    String numOfPoints;
    @SeleniumPath(value = AMOUNT_TYPE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = AMOUNT_TYPE)
    String amountType;
    @SeleniumPath(value = TAGS, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = TAGS)
    private ArrayList<String> tags;

    Occurrences occurrences;
    Timing timing;

    public Timing getTiming() {
        return timing;
    }

    public void setTiming(Timing timing) {
        this.timing = timing;
    }
    public String getNumOfPoints() {return numOfPoints;}
    public void setNumOfPoints(String numOfPoints) {this.numOfPoints = numOfPoints;}
    public String getAmountType() {return amountType;}
    public void setAmountType(String amountType) {this.amountType = amountType;}
    public Occurrences getOccurrences() {return occurrences;}
    public void setOccurrences(Occurrences occurrences) {this.occurrences = occurrences;}
    public ArrayList<String> getTags() {return tags;}
    public void setTags(ArrayList<String> tags,ICounter automation) {
        this.tags = tags;
        setNextTag(automation.getTagsCounter());
        automation.setTagsCounter(automation.getTagsCounter()+1);
    }

    public void setNextTag (int i) {this.tagCounter = i;}
    public int getNextTag () {return tagCounter;}
    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter = i;}
    public int getNext (){return counter;}
    public AddPoints(ISmartAutomationCounter automation){
        setNext(automation.getAddPointsCount());
        automation.setAddPointsCount(automation.getAddPointsCount() + 1);
    }


    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
