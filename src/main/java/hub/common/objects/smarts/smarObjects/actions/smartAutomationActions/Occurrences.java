package hub.common.objects.smarts.smarObjects.actions.smartAutomationActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;

/**
 * Created by Jenny on 12/20/2016.
 */
public class Occurrences extends BaseSeleniumObject {


    public static final String OCCURRENCES_TYPE = "(//*[@ng-options=\"option for option in ['fixed','custom']\"])";
    public static final String OCCURRENCES_TIMES = "(//*[@ng-model=\"fieldParent[fieldName].occurrences\"])";
    int counter;


    @SeleniumPath(value = OCCURRENCES_TYPE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = OCCURRENCES_TYPE)
    private String occurrences_type;
    @SeleniumPath(value = OCCURRENCES_TIMES, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = OCCURRENCES_TIMES)
    private String occurrences_times;

    CustomOccurrences customOccurrences;

    public String getOccurrences_type() {return occurrences_type;}
    public void setOccurrences_type(String occurrences_type) {this.occurrences_type = occurrences_type;}
    public String getOccurrences_times() {return occurrences_times;}
    public void setOccurrences_times(String occurrences_times) {this.occurrences_times = occurrences_times;}
    public CustomOccurrences getCustomOccurrences() {return customOccurrences;}
    public void setCustomOccurrences(CustomOccurrences customOccurrences) {this.customOccurrences = customOccurrences;}

    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter = i;}
    public int getNext (){return counter;}
    public Occurrences(ICounter automation){

        setNext(automation.getOccurrencesCounter());
        automation.setOccurrencesCounter(automation.getOccurrencesCounter()+ 1);
    }
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
