package hub.common.objects.smarts.smarObjects.actions.smartAutomationActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;

import java.util.ArrayList;

/**
 * Created by Jenny on 12/20/2016.
 */
public class SendSMS extends BaseSeleniumObject implements ISmartAction {

    public static final String TEXT_MESSAGE = "(//*[contains(@id,\"sendMemberSms\")]/div/div/div/mention-container/textarea)";
    public static final String TAGS = "(//*[@ng-model=\"newTag.text\"])";
    public  int counter;
    public  int tagCounter;

    @SeleniumPath(value = TEXT_MESSAGE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TEXT_MESSAGE)
    String txtMessage;
    @SeleniumPath(value = TAGS, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = TAGS)
    private ArrayList<String> tags;

    Timing timing;

    public String getTxtMessage() {
        return txtMessage;
    }
    public void setTxtMessage(String txtMessage) {
        this.txtMessage = txtMessage;
    }
    public ArrayList<String> getTags() {return tags;}
    public void setNextTag (int i) {this.tagCounter = i;}
    public int getNextTag () {return tagCounter;}
    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter = i;}
    public int getNext (){return counter;}
    public SendSMS(ISmartAutomationCounter automation){
        setNext(automation.getSendSMSCount());
        automation.setSendSMSCount(automation.getSendSMSCount()+ 1);
    }

    public void setTags(ArrayList<String> tags,ICounter automation) {
        this.tags = tags;
        setNextTag(automation.getTagsCounter());
        automation.setTagsCounter(automation.getTagsCounter()+1);
    }
    public Timing getTiming() {
        return timing;
    }

    public void setTiming(Timing timing) {
        this.timing = timing;
    }

    public void setOccurrences(Occurrences occurrences) {}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
