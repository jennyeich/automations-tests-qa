package hub.common.objects.smarts.smarObjects.actions.smartGiftActions;

/**
 * Created by Jenny on 11/27/2016.
 */
public enum SmartGiftActions {


    AddDiscount("Add Discount"),
    AddDealCode("Add Deal Code"),
    AddItemCode("Add Item Code")
    ;

    private final String name;
    SmartGiftActions(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }



}
