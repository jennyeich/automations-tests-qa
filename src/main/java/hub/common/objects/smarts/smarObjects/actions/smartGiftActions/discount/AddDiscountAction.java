package hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ISmartAssetCounter;

/**
 * Created by Jenny on 11/27/2016.
 */
public class AddDiscountAction extends BaseSeleniumObject{

    public static final String ACTION_DESCRIPTION = "(//input[contains(@id, \"addDiscount_description\")])";
    public static final String DISCOUNT_TYPE ="(//*[@id=\"rewardType\"])";

    public  int counter;

    @SeleniumPath(value = ACTION_DESCRIPTION, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = ACTION_DESCRIPTION)
    String descritpion;
    @SeleniumPath(value = DISCOUNT_TYPE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = DISCOUNT_TYPE)
    String discountType;


    PercentOff percentOff;
    FreeItem freeItem;
    AmountOff amountOff;
    NewPrice newPrice;


    public AmountOff getAmountOff() {return amountOff;}
    public void setAmountOff(AmountOff amountOff) {this.amountOff = amountOff;}
    public String getDiscountType() {return discountType;}
    public void setDiscountType(String discountType) {this.discountType = discountType;}
    public FreeItem getFreeItem() {return freeItem;}
    public void setFreeItem(FreeItem freeItem) {this.freeItem = freeItem;}
    public PercentOff getPercentOff() {
        return percentOff;
    }
    public void setPercentOff(PercentOff percentOff) {
        this.percentOff = percentOff;
    }
    public String getDescritpion() {
        return descritpion;
    }
    public void setDescritpion(String descritpion) {
        this.descritpion = descritpion;
    }
    public String getNextTagsPath (int i){return  "";}
    public NewPrice getNewPrice() { return newPrice; }
    public void setNewPrice(NewPrice newPrice) { this.newPrice = newPrice; }


    public int getNext (){return counter;}
    public AddDiscountAction(ISmartAssetCounter smartAsset){
        setNext(smartAsset.getAddDiscountCount());
        smartAsset.setAddDiscountCount(smartAsset.getAddDiscountCount()+ 1);
    }


    public void setNext (int i) {this.counter = i;}
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
