package hub.common.objects.smarts.smarObjects.actions.smartAutomationActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.common.TimingUnit;
import hub.common.objects.smarts.ICounter;

/**
 * Created by Goni on 5/14/2017.
 */
public class Timing extends BaseSeleniumObject {

    public static final String TIMING_TYPES = "(//select[@ng-model=\"SchedulingCtrl.ngModel.schedulingInit\"])";
    public static final String VALUE_INPUT = "(//input[@ng-model=\"SchedulingCtrl.ngModel.schedulingValue\"])";
    public static final String TIMING_UNITS = "(//select[@ng-model=\"SchedulingCtrl.ngModel.schedulingType\"])";
    int counter;


    @SeleniumPath(value = TIMING_TYPES, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = TIMING_TYPES)
    private String timingTypes;
    @SeleniumPath(value = VALUE_INPUT, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = VALUE_INPUT)
    private String valueInput;
    @SeleniumPath(value = TIMING_UNITS, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = TIMING_UNITS)
    private TimingUnit timingUnits;

    public String getTimingTypes() {
        return timingTypes;
    }

    public void setTimingTypes(String timingTypes) {
        this.timingTypes = timingTypes;
    }

    public String getValueInput() {
        return valueInput;
    }

    public void setValueInput(String valueInput) {
        this.valueInput = valueInput;
    }

    public TimingUnit getTimingUnits() {
        return timingUnits;
    }

    public void setTimingUnits(TimingUnit timingUnits) {
        this.timingUnits = timingUnits;
    }


    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter = i;}
    public int getNext (){return counter;}
    public Timing(ICounter automation){

        setNext(automation.getTimingCounter());
        automation.setTimingCounter(automation.getTimingCounter()+ 1);
    }
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
