package hub.common.objects.smarts.smarObjects.triggers;

/**
 * Created by doron on 23/05/2017.
 */
public enum OldAutomationTriggers {

    MakesPurchase("Makes a purchase"),
    UpdatesMembershipDetails("Updates membership details");

    private final String name;
    OldAutomationTriggers(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
