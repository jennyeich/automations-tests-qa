package hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ISmartAssetCounter;

/**
 * Created by Jenny on 3/7/2017.
 */
public class SetDiscountOnItemsCheckBox extends BaseSeleniumObject {

    int counter;

    public static final String SET_DISCOUNT_ON_ITEMS = "(//*[@id=\"enableDiscounts\"])";
    public static final String LIMIT_OFFER = "(//*[@id=\"displayOfferLimit\"])";


    @SeleniumPath(value = SET_DISCOUNT_ON_ITEMS, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = SET_DISCOUNT_ON_ITEMS)
    String setDicountCheckbox;
    @SeleniumPath(value = LIMIT_OFFER, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = LIMIT_OFFER)
    String DiscountsLimitOffer;

    public String getSetDicountCheckbox() {return setDicountCheckbox;}
    public void setSetDicountCheckbox(String setDicountCheckbox) {this.setDicountCheckbox = setDicountCheckbox;}
    public String getDiscountsLimitOffer() {
        return DiscountsLimitOffer;
    }
    public void setDiscountsLimitOffer(String discountsLimitOffer) {
        DiscountsLimitOffer = discountsLimitOffer;
    }


    public SetDiscountOnItemsCheckBox(ISmartAssetCounter smartAsset){
        setNext(smartAsset.getSetDisocuntOnItemsCheckBoxCount());
        smartAsset.setSetDisocuntOnItemsCheckBoxCount(smartAsset.getSetDisocuntOnItemsCheckBoxCount()+ 1);
    }

    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
