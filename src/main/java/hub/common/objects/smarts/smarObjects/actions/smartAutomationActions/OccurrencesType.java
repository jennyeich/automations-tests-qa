package hub.common.objects.smarts.smarObjects.actions.smartAutomationActions;

/**
 * Created by Jenny on 11/27/2016.
 */
public enum OccurrencesType {


    fixed("fixed"),
    custom("custom")
    ;

    private final String name;
    OccurrencesType(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }



}
