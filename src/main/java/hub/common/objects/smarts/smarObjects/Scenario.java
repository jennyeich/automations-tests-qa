package hub.common.objects.smarts.smarObjects;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartGiftActions.ISmartAssetAction;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.SmartGiftAction;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.ApplyIf;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Condition;

import java.util.ArrayList;

/**
 * Created by Jenny on 11/24/2016.
 */
public class Scenario extends BaseSeleniumObject{

    public static final String SCENARIO_NAME = "(//*[@automationid=\"edit_input\"])";
    public static final String SCENARIO_NAME_CLICK = "(//*[@automationid=\"edit_p\"])";
    public static final String OR = "(//*[@ng-click=\"ctrlOpAndOr.ngModel = conditionOperator\"][2])";
    public static final String ADD_SCENARIO = "//*[@ng-click=\"smartAssetsEditCtrl.addNewScenario();\"]";
    public static final String CLOSE_SCENARIO = "(//*[@ng-click=\"isOpen = !isOpen\"])";
    public int counter;



    @SeleniumPath(value = ADD_SCENARIO, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = ADD_SCENARIO)
    String addScenario;
    @SeleniumPath(value = SCENARIO_NAME_CLICK, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = SCENARIO_NAME_CLICK)
    String clickToEdit;
    @SeleniumPath(value = SCENARIO_NAME, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = SCENARIO_NAME)
    String scenarioName;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    ArrayList<Condition> conditions;
    @SeleniumPath(value = OR, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = OR)
    String orOperator;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    private ArrayList<ISmartAssetAction> smartGiftActions;

    public String getScenarioName() {
        return scenarioName;
    }

    public void setScenarioName(String scenarioName) {
        this.scenarioName = scenarioName;
    }


    public void addCondition(Condition condition, ICounter automation){

        condition.setNext(automation.getConditionCount());
        getConditions().add(condition);
        condition.setCreateActionCounter(this.getNext());
        automation.setConditionCount(automation.getConditionCount()+1);

        if (condition.getApplyIf()== ApplyIf.SHOPPING_CART)
            automation.setShoppingCartAndCustomOccursCounter(automation.getShoppingCartAndCustomOccursCounter()+1);

    }

    private ArrayList<Condition> getConditions() {
        if(conditions == null)
            conditions = new ArrayList<>();
        return conditions;
    }

    public void setConditions(ArrayList<Condition> conditions) {this.conditions = conditions;}

    public ArrayList<ISmartAssetAction> getSmartGiftActions() {
        if(smartGiftActions == null)
            smartGiftActions = new ArrayList<>();
        return smartGiftActions;
    }

    public void setSmartGiftActions(ArrayList<SmartGiftAction> smartAutomationActions) {this.smartGiftActions = smartGiftActions;}

    public  String getNextTagsPath(int i){
        return "";
    }

    public void setNext (int i) {
        this.counter=i;
        if (counter > 1) {
            addScenario="Y";
        }
    }
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}
    public String getOrOperator() {return orOperator;}
    public void setOrOperator() {this.orOperator = "true";}


    public void addAction(ISmartAssetAction action, ICounter automation){

        action.setNext(automation.getActionCount());
        getSmartGiftActions().add(action);
        action.setCreateActionCounter(this.getNext());
        automation.setActionCount(automation.getActionCount()+1);

    }


    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
