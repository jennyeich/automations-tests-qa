package hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ISmartAssetCounter;

/**
 * Created by Jenny on 3/7/2017.
 * The Limit amount set to the discount (checkbox + input)
 */
public class LimitAmount extends BaseSeleniumObject {

    public static final String LIMIT_AMOUNT = "(//*[@id=\"displayAmountLimit\"])";
    public static final String LIMIT_AMOUNT_AMOUNT = "(//*[@id=\"amountLimit\"])";

    int counter;
    @SeleniumPath(value = LIMIT_AMOUNT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = LIMIT_AMOUNT)
    String limitAMount;
    @SeleniumPath(value = LIMIT_AMOUNT_AMOUNT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = LIMIT_AMOUNT_AMOUNT)
    String amountNumberLimit;

    public String getAmountNumberLimit() {return amountNumberLimit;}
    public void setAmountNumberLimit(String amountNumberLimit) {this.amountNumberLimit = amountNumberLimit;}
    public String getLimitAMount() {
        return limitAMount;
    }
    public void setLimitAMount(String limitAMount) {
        this.limitAMount = limitAMount;
    }

    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    public LimitAmount(ISmartAssetCounter smartAsset){
        setNext(smartAsset.getLimitAmountCount());
        smartAsset.setLimitAmountCount(smartAsset.getLimitAmountCount()+ 1);
    }


    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
