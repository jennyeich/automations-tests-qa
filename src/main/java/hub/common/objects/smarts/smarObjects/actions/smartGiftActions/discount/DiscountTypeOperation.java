package hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount;

/**
 * Created by Jenny on 11/30/2016.
 */
public enum DiscountTypeOperation {


    PercentOff("Percent Off"),
    FreeItem("Free Item"),
    AmountOff("Amount Off"),
    NewPrice("New Price")
    ;

    private final String name;
    DiscountTypeOperation(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
