package hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ISmartAssetCounter;

/**
 * Created by Jenny on 3/9/2017.
 */
public class LimitOfferLimitedToTimes extends BaseSeleniumObject {

    int counter;

    public static final String LIMIT_OFFER_AMOUNT = "(//*[@id=\"offerLimit\"])";

    @SeleniumPath(value = LIMIT_OFFER_AMOUNT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = LIMIT_OFFER_AMOUNT)
    String DiscountsLimitOfferTimes;


    public String getDiscountsLimitOfferTimes() {
        return DiscountsLimitOfferTimes;
    }
    public void setDiscountsLimitOfferTimes(String discountsLimitOfferTimes, ISmartAssetCounter smartAsset) {
        DiscountsLimitOfferTimes = discountsLimitOfferTimes;
        setNext(smartAsset.getLimitOfferTimesCount());
        smartAsset.setLimitOfferTimesCount(smartAsset.getLimitOfferTimesCount()+ 1);
    }

    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
