package hub.common.objects.smarts;

import hub.base.SeleniumPath;
import hub.common.objects.smarts.smarObjects.Scenario;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Preset;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by Jenny on 10/05/2017.
 */
public class SmartClubDeal extends AbstractSmartObject implements ISmartAssetCounter {

    //smart gift actions counters
    private int AddDiscoutCounter=1;
    private int percentOffCounter=1;
    private int freeItemCounter = 1;
    private int amountOffCounter = 1;
    private int newPriceCounter =1;
    private int setDiscountOnItemsCount = 1;
    private int setDiscountOnItemsCheckBoxCount = 1;
    private int limitAmountCounter =1;
    private int amountOffNumberCounter=1;
    private int limitOfferTimesCounter =1;
    private int shoppingCartAndCustomOccursCounter = 0;

    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    public static final String CHOOSE_IMAGE = "//*[@id=\"backimg3\"]/div[1]/div/i";
    public static final String IMAGE_PATH = "//*[@id=\"backimg3\"]/div[2]/div/ul/li[2]/img";
    public static final String VALUE = "//*[@id=\"smartAssets\"]/div[2]/div[1]/div[1]/div[5]/div[1]/input";
    public static final String READ_VALUE = "//*[@id=\"editItem\"]/div[2]/div[1]/div[1]/div[4]/div[1]/input";
    public static final String COST = "//*[@id=\"smartAssets\"]/div[2]/div[1]/div[1]/div[5]/div[2]/input";
    public static final String READ_COST = "//*[@id=\"editItem\"]/div[2]/div[1]/div[4]/div[2]/input";
    public static final String VALID_FROM = "//*[@id=\"smartAssets\"]/div[2]/div[1]/div[3]/div/div/div/div/div[1]/input";
    public static final String VALID_FROM_HOUR = "//*[@id=\"editItem\"]/div[2]/div[1]/div[3]/div/div/div/div/div[1]/input";
    public static final String VALID_FROM_HOURS_AMPM = "//*[@id=\"smartAssets\"]/div[2]/div[1]/div[2]/div/div/div/div/div[2]/div/div/input";
    public static final String READ_VALID_FROM_HOURS_AMPM = "//*[@id=\"editItem\"]/div[2]/div[3]/div/div/div/div/div[2]/div/div/span/span[2]";
    public static final String VALID_UNTIL = "//*[@id=\"smartAssets\"]/div[2]/div[1]/div[3]/div/div/div/div/div[1]/input[@name=\"datepicker\"]";
    public static final String SCENARIOS = "//span[contains(text(),'Scenarios')]";

    public static final String TAGS = "//*[@id=\"smartAssets\"]/div[2]/div[1]/tags-input/div/div";
    public static final String READ_TAGS = "//*[@id=\"smartAssets\"]/div[3]/div[1]/tags-input/div/div/span";
    public static final String SAVE_CLOSE_BUTTON = "//button[contains(text(),\"Save and Close\")]";
    public static final String SAVE_AS_PRESET_ARROW = "//*[@action=\"savePreset\"]";
    public static final String SAVE_AS_PRESET = "//*[@ng-click=\"smartAssetsEditCtrl.save(true)\"]";



    @SeleniumPath(value = TITLE, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TITLE)
    private String title;
    @SeleniumPath(value = DESCRIPTION, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = DESCRIPTION)
    private String description;
    @SeleniumPath(value = CHOOSE_IMAGE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String choose_image;
    @SeleniumPath(value = IMAGE_PATH, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private String image_path;
    @SeleniumPath(value = VALUE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = READ_VALUE)
    private String value;
    @SeleniumPath(value = COST, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = READ_COST)
    private String cost;
    @SeleniumPath(value = VALID_FROM, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.DATE)
    private String validFrom;
    @SeleniumPath(value = VALID_UNTIL, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.DATE)
    private String validUntil;
    @SeleniumPath(value = VALID_FROM_HOUR, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT)
    private Date validFromHours;
    @SeleniumPath(value = VALID_FROM_HOURS_AMPM, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK,readValue = READ_VALID_FROM_HOURS_AMPM)
    private String validFromMAPM;
    @SeleniumPath(value = TAGS, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = READ_TAGS)
    private ArrayList<String> tags;

    Preset preset;

    public Preset getPreset() {return preset;}
    public void setPreset(Preset preset) {this.preset = preset;}

    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public String getCost() {
        return cost;
    }
    public void setCost(String cost) {
        this.cost = cost;
    }
    public String getValidFrom() {
        return validFrom;
    }
    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }
    public String getValidUntil() {
        return validUntil;
    }
    public void setValidUntil(String validUntil) {
        this.validUntil = validUntil;
    }
    public ArrayList<String> getTags() {
        return tags;
    }
    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getChoose_image() {
        return choose_image;
    }
    public void setChoose_image(String choose_image) {
        this.choose_image = choose_image;
    }
    public String getImage_path() {
        return image_path;
    }
    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
    public Date getValidFromHours() {
        return validFromHours;
    }
    public void setValidFromHours(Date validFromHours) {
        this.validFromHours = validFromHours;
    }
    public String getValidFromMAPM() {
        return validFromMAPM;
    }
    public void setValidFromMAPM(String validFromMAPM) {
        this.validFromMAPM = validFromMAPM;
    }
    public int getShoppingCartAndCustomOccursCounter(){return shoppingCartAndCustomOccursCounter;}
    public void setShoppingCartAndCustomOccursCounter(int i){
        shoppingCartAndCustomOccursCounter =i;}

    @SeleniumPath(value = SCENARIOS, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_SCROLL)
    String scenarios_tab;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    ArrayList<Scenario> scenarios;


    public void setScenarios(ArrayList<Scenario> scenarios){
        this.scenarios = scenarios;
    }
    public void addScenario(Scenario scenario){
        scenario.setNext(getScenarios().size() + 1);
        scenarios.add(scenario);

    }

    public ArrayList<Scenario> getScenarios() {
        if(scenarios == null)
            scenarios = new ArrayList<>();
        return scenarios;
    }

    @SeleniumPath(value = SAVE_CLOSE_BUTTON, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String saveButton;
    @SeleniumPath(value = SAVE_AS_PRESET_ARROW, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    public String saveAsPresetArrow;
    @SeleniumPath(value = SAVE_AS_PRESET, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    public String saveAsPreset;

    public String getSaveAsPresetArrow() {return saveAsPresetArrow;}
    public void setSaveAsPresetArrow(String saveAsPresetArrow) {this.saveAsPresetArrow = saveAsPresetArrow;}
    public String getSaveButton() {return saveButton;}
    public void setSaveButton(String saveButton) {this.saveButton = saveButton;}
    public String getSaveAsPreset() {return saveAsPreset;}
    public void setSaveAsPreset(String saveAsPreset) {this.saveAsPreset = saveAsPreset;}

    public int getAddDiscountCount() {return AddDiscoutCounter;}
    public void setAddDiscountCount(int i) {AddDiscoutCounter =i;}
    public int getPercentOffCount() {return percentOffCounter;}
    public void setPercentOffCount(int i) {percentOffCounter=i;}
    public int getFreeItemCount() {return freeItemCounter;}
    public void setFreeItemCount(int i) {freeItemCounter=i;}
    public int getAmountOffCount() {return amountOffCounter;}
    public void setAmountOffCount(int i) {amountOffCounter=i;}
    public int getNewPriceCount() { return newPriceCounter; }
    public void setNewPriceCount(int newPriceCounter) { this.newPriceCounter = newPriceCounter; }

    public int getSetDisocuntOnItemsCount() {return setDiscountOnItemsCount;}
    public void setSetDisocuntOnItemsCount(int i) {setDiscountOnItemsCount = i;}
    public int getLimitAmountCount (){return limitAmountCounter;}
    public void setLimitAmountCount (int i) {this.limitAmountCounter =i;}

    public int getAmountOffNumberCount (){return amountOffNumberCounter;}
    public void setAmountOffNumberCount (int i){amountOffNumberCounter=i;}

    public int getSetDisocuntOnItemsCheckBoxCount (){return setDiscountOnItemsCheckBoxCount;}
    public void setSetDisocuntOnItemsCheckBoxCount (int i){this.setDiscountOnItemsCheckBoxCount =i;}

    public int getLimitOfferTimesCount (){return limitOfferTimesCounter;}
    public void setLimitOfferTimesCount (int i) {this.limitOfferTimesCounter =i;}

}
