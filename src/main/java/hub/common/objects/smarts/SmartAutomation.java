package hub.common.objects.smarts;

import hub.base.SeleniumPath;
import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;
import hub.common.objects.smarts.smarObjects.settings.ISettings;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;

import java.util.ArrayList;

/**
 * Created by Jenny on 11/3/2016.
 */

public class SmartAutomation extends AbstractSmartObject implements ISmartAutomationCounter{

    public static final String SAVE_AND_CLOSE = "//*[@translate=\"save_and_close\"]";
    public static final String SCENARIOS = "//span[contains(text(),'Scenarios')]";
    public static final String SAVE_AS_PRESET_ARROW = "//*[@action=\"savePreset\"]";
    public static final String MEMBERSHIP_REGISTER_CHK = "MembershipStatus.registered";
    public static final String MEMBERSHIP_NOT_REGISTER_CHK = "MembershipStatus.not_registered";
    public static final String SAVE_AS_PRESET = "//*[@ng-click=\"ctrlAEdit.saveAsPreset()\"]";

    public Settings settings;
    private int sendAssetCounter=1;
    private int tagMemberCounter=1;
    private int addPointsCounter = 1;
    private int sendSMSCounter =1;
    private int sendEmailCounter = 1;
    private int punchAPunchCardCounter =1;
    private int unregisterCounter=1;
    private int registerCounter=1;
    private int sendLotteryCounter=1;
    private int exportEventCounter =1;
    private int shoppingCartAndCustomOccursCounter = 1;
    private int sendPushNotificationCounter=1;
    private int sendPopUpCounter=1;

    @SeleniumPath(value = MEMBERSHIP_REGISTER_CHK, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = MEMBERSHIP_REGISTER_CHK)
    String registerChk;
    @SeleniumPath(value = MEMBERSHIP_NOT_REGISTER_CHK, type=SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = MEMBERSHIP_NOT_REGISTER_CHK)
    String notRegisterChk;
    @SeleniumPath(value = SCENARIOS, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_SCROLL)
    String scenarios_tab;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    ArrayList<SmartAutomationScenario> scenarios;


    public ISettings getSettings() {return settings;}
    public void setSettings(ISettings settings){this.settings = (Settings)settings;}

    public void setScenarios(ArrayList<SmartAutomationScenario> scenarios){
        this.scenarios = scenarios;
    }
    public void addScenario(SmartAutomationScenario scenario){
        scenario.setNext(getScenarios().size() + 1);
        scenarios.add(scenario);

    }


    public ArrayList<SmartAutomationScenario> getScenarios() {
        if(scenarios == null)
            scenarios = new ArrayList<>();
        return scenarios;
    }

    public String getRegisterChk() {
        return registerChk;
    }

    public void setRegisterChk(String registerChk) {
        this.registerChk = registerChk;
    }

    public String getNotRegisterChk() {
        return notRegisterChk;
    }

    public void setNotRegisterChk(String notRegisterChk) {
        this.notRegisterChk = notRegisterChk;
    }


    @SeleniumPath(value = SAVE_AND_CLOSE, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String saveButton;
    @SeleniumPath(value = SAVE_AS_PRESET_ARROW, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_WAIT)
    public String saveAsPresetArrow;
    @SeleniumPath(value = SAVE_AS_PRESET, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_WAIT)
    public String saveAsPreset;


    public String getSaveAsPresetArrow() {return saveAsPresetArrow;}
    public void setSaveAsPresetArrow(String saveAsPresetArrow) {this.saveAsPresetArrow = saveAsPresetArrow;}
    public String getSaveAsPreset() {return saveAsPreset;}
    public void setSaveAsPreset(String saveAsPreset) {this.saveAsPreset = saveAsPreset;}
    public String getSaveButton() {return saveButton;}
    public void setSaveButton(String saveButton) {this.saveButton = saveButton;}

    public void setSendAssetCount(int i) {sendAssetCounter = i;}
    public int getSendAssetCount() {return sendAssetCounter;}
    public void setTagMemberCount(int i) {tagMemberCounter = i;}
    public int getTagMemberCount() {return tagMemberCounter;}
    public int getAddPointsCount (){return addPointsCounter;}
    public void setAddPointsCount (int i) {addPointsCounter = i;}
    public int getSendSMSCount (){return sendSMSCounter;}
    public void setSendSMSCount (int i){sendSMSCounter =i;}
    public int getSendEmailCount(){return sendEmailCounter; }
    public void setSendEmailCount(int i){sendEmailCounter = i;}
    public int getPunchAPunchCardCount () {return punchAPunchCardCounter;}
    public void setPunchAPunchCardCount (int i){punchAPunchCardCounter =i;}
    public int getUnregisterCount () {return unregisterCounter;}
    public void setUnregisterCount (int i){unregisterCounter=i;}
    public int getRegisterCount () {return registerCounter;}
    public void setRegisterCount (int i){registerCounter=i;}
    public int getExportEventCount (){return exportEventCounter;}
    public void setExportEventCount (int i) {exportEventCounter=i;}

    @Override
    public int getSendPushNotificationCount() {
        return sendPushNotificationCounter;
    }
    @Override
    public void setSendPushNotificationCount(int i) {
        sendPushNotificationCounter = i;
    }

    @Override
    public int getSendPopUpMessageCount() {
        return sendPopUpCounter;
    }

    @Override
    public void setSendPopUpMessageCount(int i) {
        sendPopUpCounter = i;
    }

    public int getSendLotteryCount (){return sendLotteryCounter;}
    public void setSendLotteryCount (int i) {sendLotteryCounter=i;}
    public int getShoppingCartAndCustomOccursCounter(){return shoppingCartAndCustomOccursCounter;}
    public void setShoppingCartAndCustomOccursCounter(int i){
        shoppingCartAndCustomOccursCounter =i;}

}
