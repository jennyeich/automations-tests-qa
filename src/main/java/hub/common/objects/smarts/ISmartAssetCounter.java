package hub.common.objects.smarts;

import hub.common.objects.benefits.IAsset;
import hub.common.objects.smarts.smarObjects.Scenario;

import java.util.ArrayList;

/**
 * Created by Goni on 5/25/2017.
 */
public interface ISmartAssetCounter extends ICounter,IAsset{

    int getAddDiscountCount ();
    void setAddDiscountCount (int i);

    int getPercentOffCount ();
    void setPercentOffCount (int i);

    int getNewPriceCount ();
    void setNewPriceCount (int i);

    int getFreeItemCount ();
    void setFreeItemCount (int i);

    int getAmountOffCount ();
    void setAmountOffCount (int i);

    int getSetDisocuntOnItemsCount ();
    void setSetDisocuntOnItemsCount (int i);

    int getLimitAmountCount ();
    void setLimitAmountCount (int i);

    int getAmountOffNumberCount ();
    void setAmountOffNumberCount (int i);

    int getSetDisocuntOnItemsCheckBoxCount ();
    void setSetDisocuntOnItemsCheckBoxCount (int i);

    int getLimitOfferTimesCount ();
    void setLimitOfferTimesCount (int i);


    void addScenario(Scenario scenario);

    ArrayList<Scenario> getScenarios();


    void setValidUntil(String date);

}
