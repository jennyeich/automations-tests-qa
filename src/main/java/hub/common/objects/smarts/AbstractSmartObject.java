package hub.common.objects.smarts;

import hub.base.BaseSeleniumObject;

/**
 * Created by Goni on 5/25/2017.
 */
public abstract class AbstractSmartObject extends BaseSeleniumObject implements ICounter {

    private int tagCounter = 1;
    private int occcurancesCounter = 1;
    private int customOcccurancesCounter = 1;
    private int conditionCounter =1;
    private int actionCounter = 1;
    private int dateAndTimeCounter = 1;
    private int daysTimeConditionCounter = 1;
    private int daysTimeCounter = 1;

    //condition value counters
    private int conditionValueCounter =1;
    private int conditionTagValueCounter =1;
    private int conditionSelectValueCounter =1;
    private int conditionDateValueCounter = 1;

    //condition operation counters
    private int conditionEqualOperatorCounter =1;
    private int conditionStringOperatorCounter =1;
    private int conditionIntOperatorCounter =1;
    private int conditionDateOperatorCounter =1;
    private int conditionTagOperatorCounter =1;

    int timingCounter =1;


    public  String getNextTagsPath(int i){
        return "";
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public int getConditionEqualOperatorCounter() {
        return conditionEqualOperatorCounter;
    }
    public void setConditionEqualOperatorCounter(int i) {
        this.conditionEqualOperatorCounter = i;
    }

    public int getConditionDateOperatorCounter() {
        return conditionDateOperatorCounter;
    }

    public void setConditionDateOperatorCounter(int i) {
        this.conditionDateOperatorCounter = i;
    }
    public int getConditionStringOperatorCounter() {
        return conditionStringOperatorCounter;
    }
    public void setConditionStringOperatorCounter(int i) {
        this.conditionStringOperatorCounter = i;
    }
    public int getConditionIntOperatorCounter() {
        return conditionIntOperatorCounter;
    }
    public void setConditionIntOperatorCounter(int i) {
        this.conditionIntOperatorCounter = i;
    }
    public int getConditionTagOperatorCounter() {
        return conditionTagOperatorCounter;
    }
    public void setConditionTagOperatorCounter(int i) {
        this.conditionTagOperatorCounter = i;
    }

    @Override
    public int getConditionValueCounter() {
        return conditionValueCounter;
    }

    @Override
    public void setConditionValueCounter(int i) {
        this.conditionValueCounter = i;
    }

    @Override
    public int getConditionTagValueCounter() {
        return conditionTagValueCounter;
    }

    @Override
    public void setConditionTagValueCounter(int i) {
        conditionTagValueCounter = i;
    }

    @Override
    public int getConditionDateValueCounter() {
        return conditionDateValueCounter;
    }

    @Override
    public void setConditionDateValueCounter(int i) {
        conditionDateValueCounter = i;
    }

    @Override
    public int getConditionSelectValueCounter() {
        return conditionSelectValueCounter;
    }

    @Override
    public void setConditionSelectValueCounter(int i) {
        conditionSelectValueCounter = i;
    }



    public int getTagsCounter () {return tagCounter;}
    public void setTagsCounter (int i) {tagCounter=i;}
    public int getOccurrencesCounter () {return occcurancesCounter;}
    public void setOccurrencesCounter (int i) {occcurancesCounter =i;}
    public int getCustomOccurrencesCounter () {return customOcccurancesCounter;}
    public void setCustomOccurrencesCounter (int i) {customOcccurancesCounter = i;}

    public int getConditionCount () {return conditionCounter;}
    public void setConditionCount (int i){conditionCounter = i;}
    public int getActionCount () {return actionCounter;}
    public void setActionCount (int i) {actionCounter = i;}

    @Override
    public int getDateAndTimeConditionCounter() {
        return dateAndTimeCounter;
    }


    @Override
    public void setDateAndTimeConditionCounter(int i) {
        dateAndTimeCounter = i;
    }

    @Override
    public int getDaysTimeConditionCounter() {
        return daysTimeConditionCounter;
    }

    @Override
    public void setDaysTimeConditionCounter(int i) {
        daysTimeConditionCounter = i;
    }

    @Override
    public int getDaysTimeCounter() {
        return daysTimeCounter;
    }

    @Override
    public void setDaysTimeCounter(int i) {
        daysTimeCounter = i;
    }

    @Override
    public int getTimingCounter() {
        return timingCounter;
    }

    @Override
    public void setTimingCounter(int i) {
        timingCounter = i;
    }
}
