package hub.common.objects.smarts;

/**
 * Created by Jenny on 1/10/2017.
 */
public interface ICounter {

    int getTagsCounter ();
    void setTagsCounter (int i);

    int getOccurrencesCounter ();
    void setOccurrencesCounter (int i);

    int getCustomOccurrencesCounter ();
    void setCustomOccurrencesCounter (int i);

    int getShoppingCartAndCustomOccursCounter();
    void setShoppingCartAndCustomOccursCounter(int i);

    int getConditionCount ();
    void setConditionCount (int i);

    int getActionCount ();
    void setActionCount (int i);


    int getConditionValueCounter();
    void setConditionValueCounter(int conditionValueCounter);

    int getConditionTagValueCounter();
    void setConditionTagValueCounter(int conditionTagValueCounter) ;

    int getConditionSelectValueCounter() ;
    void setConditionSelectValueCounter(int conditionSelectValueCounter) ;

    int getConditionDateValueCounter();

    void setConditionDateValueCounter(int conditionDateValueCounter) ;

    int getConditionStringOperatorCounter() ;
    void setConditionStringOperatorCounter(int conditionStringOperatorCounter) ;


    int getConditionIntOperatorCounter();
    void setConditionIntOperatorCounter(int conditionIntOperatorCounter);

    int getConditionTagOperatorCounter();
    void setConditionTagOperatorCounter(int conditionTagOperatorCounter);

    int getConditionEqualOperatorCounter();
    void setConditionEqualOperatorCounter(int conditionCommonOperatorCounter);

    int getConditionDateOperatorCounter();
    void setConditionDateOperatorCounter(int conditionDateOperatorCounter);


    int getDateAndTimeConditionCounter();

    void setDateAndTimeConditionCounter(int i);

    int getDaysTimeConditionCounter();

    void setDaysTimeConditionCounter(int i);
    int getDaysTimeCounter();

    void setDaysTimeCounter(int i);

    int getTimingCounter();

    void setTimingCounter(int i);


}
