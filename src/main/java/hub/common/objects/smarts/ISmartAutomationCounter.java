package hub.common.objects.smarts;

import hub.common.objects.smarts.smarObjects.SmartAutomationScenario;

import java.util.ArrayList;

/**
 * Created by Goni on 5/25/2017.
 */
public interface ISmartAutomationCounter extends ICounter {


    void setScenarios(ArrayList<SmartAutomationScenario> scenarios);

    void addScenario(SmartAutomationScenario scenario);


    String getSaveAsPreset();
    void setSaveAsPresetArrow(String s);
    void setSaveButton(String s);

    int getSendAssetCount ();
    void setSendAssetCount (int i);

    int getTagMemberCount ();
    void setTagMemberCount (int i);

    int getAddPointsCount ();
    void setAddPointsCount (int i);

    int getSendSMSCount ();
    void setSendSMSCount (int i);

    int getSendEmailCount();
    void setSendEmailCount(int i);

    int getPunchAPunchCardCount ();
    void setPunchAPunchCardCount (int i);

    int getUnregisterCount ();
    void setUnregisterCount (int i);

    int getRegisterCount ();
    void setRegisterCount (int i);

    int getSendLotteryCount ();
    void setSendLotteryCount (int i);

    int getExportEventCount();

    void setExportEventCount(int i);

    int getSendPushNotificationCount();

    void setSendPushNotificationCount(int i);

    int getSendPopUpMessageCount();

    void setSendPopUpMessageCount(int i);
}
