package hub.common.objects.common;

public enum FilterMembersEmailSentFrom {
    POS("POS"),
    AUTOMATION("Automation"),
    HUB("Hub"),
    CLIENT("Client"),
    ADMIN("Admin"),
    SERVER("Server")

    ;

    private final String text;

    FilterMembersEmailSentFrom(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
