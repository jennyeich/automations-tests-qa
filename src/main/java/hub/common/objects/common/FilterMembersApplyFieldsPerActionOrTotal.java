package hub.common.objects.common;

/**
 * Created by lior on 5/29/17.
 */
public enum FilterMembersApplyFieldsPerActionOrTotal {

    PER_ACTION("Per action"),
    TOTAL("Total")
    ;


    private final String text;

    FilterMembersApplyFieldsPerActionOrTotal(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
