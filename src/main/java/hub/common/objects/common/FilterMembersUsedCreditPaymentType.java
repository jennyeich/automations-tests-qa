package hub.common.objects.common;

/**
 * Created by lior on 5/29/17.
 */
public enum FilterMembersUsedCreditPaymentType {


    POINTS("Points"),
    CREDIT("Credit"),
    GIFT_CARD("Gift Card"),
    EXTERNALPAYMENT("External Payment")

    ;




    private final String text;

    FilterMembersUsedCreditPaymentType(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
