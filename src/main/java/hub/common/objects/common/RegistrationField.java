package hub.common.objects.common;

/**
 * Created by Goni on 3/27/2017.
 */
public enum RegistrationField {

    FirstNAme("First Name"),
    LastName("Last Name"),
    PhoneNumber("Phone Number"),
    Email("Email Address"),
    Gender("Gender"),
    Birthday("Birthday"),
    Anniversary("Anniversary"),
    ExpirationDate("Expiration Date"),
    ClubMemberID("Club Member ID"),
    CarNumber("Custom Text 1"),
    GenericInteger("Custom Number 1"),
    OfficialIDNumber("Official ID Number"),
    zipCode("Zip Code"),
    homeBranchID("Home Branch ID"),
    extClubID("External Member ID"),
    addressHome("Address Home"),
    addressFloor("Address Floor"),
    addressStreet("Address Street"),
    addressLine1("Address Line 1"),
    addressLine2("Address Line 2"),
    FavoriteBranchID("Favorite Branch ID"),
    AllowEmail("Allow Email"),
    CustomText1("Custom Text 1"),
    CustomText2("Custom Text 2"),
    AllowSMS("Allow SMS"),
    GenericInteger1("Custom Number 1"),
    GenericInteger2("Custom Number 2"),
    ;

    private final String name;
    RegistrationField(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
