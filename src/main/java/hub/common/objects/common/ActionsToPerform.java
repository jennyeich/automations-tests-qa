package hub.common.objects.common;

/**
 * Created by doron on 22/05/2017.
 */
public enum ActionsToPerform {
    TAG_UNTAG_MEMBER("Tag/Untag Member"),
    POINT_CREDIT_ACCUMULATION("Point/Credit Accumulation");;

    private String text;

    ActionsToPerform(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
