package hub.common.objects.common;

/**
 * Created by Goni on 5/14/2017.
 */
public enum TimingUnit {

    SECONDS("seconds"),
    MINUTES("minutes"),
    HOURS("hours"),
    DAYS("days"),
    WEEKS("weeks")
    ;

    private final String name;

    TimingUnit(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
