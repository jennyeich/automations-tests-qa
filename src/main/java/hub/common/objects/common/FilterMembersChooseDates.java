package hub.common.objects.common;

/**
 * Created by lior on 5/9/17.
 */
public enum FilterMembersChooseDates {

    CHOOSE_FROM_CALENDAR ("Choose from Calendar"),
    CHOOSE_RANGE ("Choose a Range:")
    ;

    private final String text;

    FilterMembersChooseDates(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
