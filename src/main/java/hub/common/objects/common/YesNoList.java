package hub.common.objects.common;



/**
 * Created by Jenny on 11/13/2016.
 */
public enum YesNoList {

    YES("string:Y"),
    NO("string:N");

    private final String name;

    YesNoList(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}


