package hub.common.objects.common;

/**
 * Created by Jenny on 12/2/2016.
 */
public enum Gender {

    MALE("Male"),
    FEMALE("Female"),
    OTHER("Other")
    ;

    private final String text;

    Gender(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
