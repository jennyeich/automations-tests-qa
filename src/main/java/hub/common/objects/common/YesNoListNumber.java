package hub.common.objects.common;

// This enum support different kind of YES-NO .
// NOTE: currently the YES-NO are not aligned to be the same for all drop list item since it's required changes in the server also.
public enum YesNoListNumber {
    YES("number:1"),
    NO("number:0");

    private final String name;

    YesNoListNumber(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}


