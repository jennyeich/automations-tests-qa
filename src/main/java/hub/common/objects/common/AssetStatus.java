package hub.common.objects.common;

/**
 * Created by Goni on 9/6/2017.
 */

public enum AssetStatus {

    EXPIRED,
    DEACTIVATED,
    ACTIVE

}
