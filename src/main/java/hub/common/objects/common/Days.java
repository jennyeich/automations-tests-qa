package hub.common.objects.common;

/**
 * Created by Goni on 3/1/2017.
 */
public enum Days {
    MONDAY("Monday"),
    TUESDAY("Tuesday"),
    WEDNESDAY("Wednesday"),
    THURSDAY("Thursday"),
    FRIDAY("Friday"),
    SATURDAY("Saturday"),
    SUNDAY("Sunday"),
    ;

    private final String name;
    Days(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }


}
