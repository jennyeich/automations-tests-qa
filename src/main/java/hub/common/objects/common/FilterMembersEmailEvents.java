package hub.common.objects.common;

public enum FilterMembersEmailEvents {

    EMAIL_SENT("Email was sent"),
    EMAIL_REACHED_INBOX("Email reached the member's inbox"),
    EMAIL_FAILED_TO_REACH_INBOC("Email failed to reach the member's inbox"),
    EMAIL_WAS_OPENED("Email was opened"),
    MEMBER_TAPPED_LINK("Member tapped a link in email"),
    EMAIL_CLASSIFIED_AS_SPAM("Email was classified as spam"),
    EMAIL_WAS_REJECTED("Email was rejected by the member's email service")


    ;

    private final String text;

    FilterMembersEmailEvents(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
