package hub.common.objects.common;

/**
 * Created by lior on 7/5/17.
 */
public enum PermissionLevel {

    ALLOW("Allow"),
    DISALLOW("Disallow"),
    ;


    private final String text;

    PermissionLevel(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
