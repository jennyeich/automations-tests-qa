package hub.common.objects.common;

/**
 * Created by Goni on 6/20/2017.
 */
public enum TextAreaParameters {
    Membership_FirstName("Membership.FirstName"),
    Membership_LastName("Membership.LastName"),
    Membership_Anniversary("Membership.Anniversary"),
    Membership_Birthday("Membership.Birthday"),
    Membership_Tag("Membership.Tag"),
    Membership_Email("Membership.Email"),
    Membership_FavoriteBranchID("Membership.FavoriteBranchID"),
    Membership_Gender("Membership.Gender"),
    Membership_HomeBranchID("Membership.HomeBranchID"),
    Membership_MemberID("Membership.MemberID"),
    Membership_PhoneNumber("Membership.PhoneNumber"),
    Membership_Points("Membership.Points"),
    Membership_AccumulatedPoints("Membership.AccumulatedPoints"),
    Membership_AnniversaryMonth("Membership.AnniversaryMonth"),
    Membership_ExpirationDate("context.Membership.ExpirationDate"),
    Membership_BirthdayMonthAndDay("Membership.BirthdayMonthAndDay");

    private final String name;

    TextAreaParameters(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
