package hub.common.objects.common;

/**
 * Created by Goni on 6/12/2017.
 */
public enum FormFieldAnswerType {

    FREE_TEXT_LINE("Free text line"),
    MULTIPLE_CHOICE_ONE_ANSWER("Multiple choice (one answer)"),
    CHECK_BOX("Check box"),
    FREE_TEXT_BOX("Free text box"),
    MULTIPLE_ANSWERS("Multiple choice (multiple answers)"),
    TIME("Time"),
    DATE("Date");

    private final String name;

    FormFieldAnswerType(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
