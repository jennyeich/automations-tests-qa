package hub.common.objects.common;

public enum FilterMembersSource {

    Hub("string:Hub"),
    Client("string:Client"),
    POS("string:POS"),
    Automation("string:Automation"),
    App("string:App"),
    Import("string:Import"),
    LandingPage("string:LandingPage")
    ;

    public final String source;

    FilterMembersSource(final String source) {
        this.source = source;
    }

}
