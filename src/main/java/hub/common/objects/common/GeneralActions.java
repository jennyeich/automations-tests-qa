package hub.common.objects.common;

/**
 * Created by Goni on 9/25/2017.
 */

public enum GeneralActions {
    NoAction("No Action"),
    OpenAppScreen("Open App Screen"),
    CloseAppScreen("Close App Screen"),
    OpenUrlInExternalBrowser("Open URL In External Browser"),
    ;

    private final String text;

    GeneralActions(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}

