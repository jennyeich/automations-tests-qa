package hub.common.objects.common;

/**
 * Created by Goni on 3/26/2017.
 */
public enum AccumulationVersion {

    OLD("string:1"),
    NEW("string:2");

    private final String name;

    AccumulationVersion(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
