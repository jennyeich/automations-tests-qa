package hub.common.objects.common;

/**
 * Created by lior on 5/9/17.
 */
public enum FilterActionTypes {

    LAUNCHED_THE_APP_FOR_THE_FIRST_TIME ("Launched the app for the 1st time"),
    OPENED_THE_APP ("Opened the app"),
    PUNCHED_A_PUNCH_CARD ("Punched a punch card"),
    REDEEMED_A_GIFT ("Redeemed a gift"),
    ATTEMPTED_TO_REDEEM_A_GIFT("Attempted to redeem a gift"),
    RECEIVED_AN_ASSET ("Received an asset"),
    RECEIVED_OR_USED_POINTS ("Received/used points"),
    TAPPED_CLAIM ("Tapped \"claim\" (trigger button)"),
    UNREGISTERED ("Unregistered"),
    EVENT_EXPORTED ("Event Exported"),
    USED_CREDIT ("Used Credit"),
    RECEIVED_CREDIT ("Received Credit"),
    CONSENTED_TC("Consented to T&C"),
    MADE_A_PURCHASE ("Made a purchase"),
    PURCHASED_A_SPECIFIC_ITEM ("Purchased a specific item"),
    CONNECTED_TO_FB_THROUGH_THE_APP ("Connected to FB through the app"),
    SHARED_AN_ITEM_TO_FB ("Shared an item to FB"),
    SEND_MEMBER_SMS("Send Member SMS"),
    BEACON_SIGNAL ("Beacon Signal"),
    SEND_MEMBER_EMAIL("Send Member Email")
    ;


    private final String text;

    FilterActionTypes(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }


}
