package hub.common.objects.common;

/**
 * Created by Goni on 4/3/2017.
 */
public enum ShowCurrencyWithDecimal {
    Without_decimal_point("number:0"),
    With_decimal_point("number:2")
    ;

    private final String text;

    ShowCurrencyWithDecimal(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
