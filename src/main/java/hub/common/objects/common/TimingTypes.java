package hub.common.objects.common;

/**
 * Created by Goni on 5/14/2017.
 */
public enum TimingTypes {
    IMMEDIATE("immediate"),
    DELAYED("delayed"),
    ;

    private final String name;
    TimingTypes(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
