package hub.common.objects.common;

/**
 * Created by Jenny on 11/16/2016.
 */
public enum Automation_TimeUnits {

    NONE("string:"),
    HOURS("string:hour"),
    DAYS("string:day"),
    WEEKS("string:week"),
    MONTHS("string:month"),
    YEARS("string:year")
    ;

    private final String text;

    Automation_TimeUnits(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }


}
