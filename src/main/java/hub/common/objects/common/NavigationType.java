package hub.common.objects.common;

/**
 * Created by Goni on 9/25/2017.
 */

public enum NavigationType {
    SIDE_MENU("sidemenu"),
    REGULAR("regular");

    private final String name;

    NavigationType(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
