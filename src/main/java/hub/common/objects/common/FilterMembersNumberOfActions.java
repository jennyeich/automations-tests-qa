package hub.common.objects.common;

/**
 * Created by lior on 5/9/17.
 */
public enum FilterMembersNumberOfActions {

    WITHIN_A_RANGE_AND_EQUAL_TO ("Within a range (and equal to)"),
    MORE_THAN_OR_EQUAL_TO ("More than or equal to"),
    LESS_THAN_OR_EQUAL_TO_INCLUDES_THE_VALUE_ZERO("Less than or equal to (includes the value 0)")

    ;

    private final String text;

    FilterMembersNumberOfActions(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
