package hub.common.objects.common;

/**
 * Created by lior on 7/4/17.
 */
public enum Tags {

    EXTRA_IDENTIFIERS("findUsers.field-ExtraIdentifiers (ExtraIdentifiers)"),
    MOBILE_APP_USER("findUsers.field-MobileAppUsed (MobileAppUsed)"),
    LOCATION_ENABLED("findUsers.field-LocationEnabled (LocationEnabled)"),
    PUSH_NOTIFICATION_ENABLED("findUsers.field-PushNotificationEnabled (PushNotificationEnabled)"),
    FIND_USERS_FIELD("findUsers.field- ()"),
    MEMBERSHIP_STATUS("findUsers.section-membershipStatus (membershipStatus)"),
    DASHBOARD_WIDGET("dashboard.widget- ()"),
    CONNECT_WITH_FACEBOOK("dashboard.widget-connectedwithfb (connectedwithfb)"),
    SHARE_ON_FACEBOOK("dashboard.widget-fbpost (fbpost)"),
    FILLED_A_FORM("dashboard.widget-formfilled (formfilled)"),
    SIMPLE_LAYOUT("layout.MainLocationTC-MainLocationTC-0x2x2 (MainLocationTC-0x2x2)"),
    STYLIST_LAYOUT("layout.MainLocationTC-MainLocationTC-3x0 (MainLocationTC-3x0)"),
    SLIM_LAYOUT("layout.MainLocationTC-MainLocationTC-0x3 (MainLocationTC-0x3)"),
    STUDIO_LAYOUT("layout.MainLocationTC-MainLocationTC-0x4 (MainLocationTC-0x4)"),
    THIN_LAYOUT("layout.MainLocationTC-MainLocationTC-2x0x2 (MainLocationTC-2x0x2)"),
    SMART_ACTION_ASSIGN_ASSET("smartAction.actions-assignAsset (assignAsset)"),
    MAIN_LOCATION_TC_FULL_SCREEN_0x3_LAYOUT("layout.MainLocationTC-MainLocationTC-FullScreen-0x3 (MainLocationTC-FullScreen-0x3)"),
    MAIN_LOCATION_TC_FULL_SCREEN_0x4_LAYOUT("layout.MainLocationTC-MainLocationTC-FullScreen-0x4 (MainLocationTC-FullScreen-0x4)"),
    MAIN_LOCATION_TC_FULL_SCREEN_0x2x2_LAYOUT("layout.MainLocationTC-MainLocationTC-FullScreen-0x2x2 (MainLocationTC-FullScreen-0x2x2)"),
    MAIN_LOCATION_TC_FULL_SCREEN_0x3x3_LAYOUT("layout.MainLocationTC-MainLocationTC-FullScreen-0x3x3 (MainLocationTC-FullScreen-0x3x3)"),
    MAIN_LOCATION_TC("layout.MainLocationTC- ()"),
    SEND_MEMBER_SMS("actionTasks.actions-SendMemberSMS (SendMemberSMS)"),
    SEND_MEMBER_SMS_SMART_ACTION("smartAction.actions-sendMemberSms (sendMemberSms)"),
    POINT_CREDIT_SETTINGS_BUDGET("pointCreditSettings.section-budget (budget)"),
    ACCUMULATION_TYPE_BUDGET("actionTasks.accumulationType-Budget (Credit)"),
    SMART_ACTION_GROUP_PURCHASE("smartAction.context_types.logRow.conditionGroups-smartActions.groups.PurchaseBasket (Shopping Cart)"),
    FILTER_USERS_BIGQUERY("filterUsers.button-bigQuery (bigQuery)"),
    SMART_ACTIONS("users.smartActions- ()"),
    SCHEDULED_ACTIONS("nav.item.scheduled-scheduledActions (scheduledActions)"),
    FILTER_USERS_SQL("filterUsers.button-sql (sql)"),
    ACTION_LIST("users.actions-actions_list (actions_list)"),
    MEMBER_NOTES("pos_settings-member_notes (Member Notes)"),
    ALLOW_PARTIAL_PAYMENT("pointCreditSettings.section-allowPartialPayment (allowPartialPayment)"),
    PAYMENT_METHOD("pointCreditSettings.section-paymentMethod (paymentMethod)"),
    CUSTOM_FIELD_FOR_IDENTIFICATION("app_settings.field-customFieldForIdentification (Custom Field for POS Identification)"),
    SEND_MEMBER_EMAIL_SMART_ACTION("smartAction.actions-sendMemberEmail (sendMemberEmail)"),
    EMAIL_TEMPLATE("nav.item-email-templates (Email Templates)"),

    //Hub2 permission tags
    HUB2_BUDGET_TYPE_TRIGGERS("smartAction-payWithBugdet"),
    HUB2_RULE_PURCHASE_BASKET("smartAction.context_types.logRow.conditionGroups-smartActions.groups.PurchaseBasket"),
    HUB2_DEAL_PURCHASE_BASKET("smartAction.context_types.redeem.conditionGroups-smartActions.groups.PurchaseBasket"),
    HUB2_TEMPLATE_PRODUCT("Hub2_product_template")
    ;


    private final String text;

    Tags(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
