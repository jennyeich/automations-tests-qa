package hub.common.objects.common;

/**
 * Created by Goni on 5/22/2017.
 */
public enum RewardTypes {

    GIVE_ASSET("GiveAsset"),
    SEND_PUSH("SendPush")
    ;

    private final String text;

    RewardTypes(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
