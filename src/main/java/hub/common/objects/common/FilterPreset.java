package hub.common.objects.common;

/**
 * Created by lior on 5/8/17.
 */
public enum FilterPreset {

    DIDNT_OPEN_THE_APP_IN_LAST_60_DAYS ("Didn't open the App in last 60 days"),
    USERS_WHO_SHARED_ON_FACEBOOK ("Users who shared on Facebook"),
    OPENED_THE_APP_MORE_THAN_TWICE_IN_THE_LAST_60_DAYS ("Opened the App more than twice in last 60 days"),
    JOIND_THE_CLUB_IN_THE_LAST_30_DAYS ("Joined the club in last 30 days"),
    PUNCHED_A_PUNCH_CARD_IN_THE_LAST_30_DAYS ("Punched a punch card in last 30 days"),
    REDEEMED_A_GIFT_IN_THE_LAST_90_DAYS ("Redeemed a gift in last 90 days")
    ;

    private final String text;

    FilterPreset(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }

}
