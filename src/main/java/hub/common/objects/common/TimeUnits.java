package hub.common.objects.common;

/**
 * Created by Jenny on 11/16/2016.
 */
public enum TimeUnits {

    MINUTES("number:60"),
    HOURS("number:3600"),
    DAYS("number:86400"),
    WEEKS("number:604800")
    ;

    private final String text;

    TimeUnits(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }


}
