package hub.common.objects.common;

/**
 * Created by Jenny on 11/9/2016.
 */
public class FilterLocationsListbyTag {

    String filterByTag;
    String chooseLocationGroupOptional;

    public String getFilterByTag() {
        return filterByTag;
    }

    public void setFilterByTag(String filterByTag) {
        this.filterByTag = filterByTag;
    }

    public String getChooseLocationGroupOptional() {
        return chooseLocationGroupOptional;
    }

    public void setChooseLocationGroupOptional(String chooseLocationGroupOptional) {
        this.chooseLocationGroupOptional = chooseLocationGroupOptional;
    }
}
