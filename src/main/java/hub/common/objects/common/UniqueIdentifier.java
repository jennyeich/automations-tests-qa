package hub.common.objects.common;

/**
 * Created by Goni on 9/12/2017.
 */

public enum UniqueIdentifier {

    COMMON_EXT_ID("Common External ID"),
    OFFICIAL_ID_NUMBER("Official ID Number"),
    PHONE_NUMBER("Phone Number"),
    CLUB_MEMBER_ID("Club Member ID");


    private final String text;

    UniqueIdentifier(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
