package hub.common.objects.common;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;


/**
 * Created by lior on 5/11/17.
 */
public class DateAndTimeInFilter extends BaseSeleniumObject {

    public static final String FROM_DATE = "//*[contains(@field-name,\"from\")]//*[@name=\"datepicker\"]";
    public static final String UNTIL_DATE = "//*[contains(@field-name,\"to\")]//*[@name=\"datepicker\"]";


    @SeleniumPath(value = FROM_DATE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.DATE,readValue = FROM_DATE)
    String fromDate;
    @SeleniumPath(value = UNTIL_DATE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.DATE,readValue = UNTIL_DATE)
    String untilDate;





    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getUntilDate() {
        return untilDate;
    }

    public void setUntilDate(String untilDate) {
        this.untilDate = untilDate;
    }


    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }


    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }


}
