package hub.services;

import hub.base.BaseService;
import hub.common.pages.DashboardPage;
import hub.hub1_0.services.Navigator;

import java.lang.reflect.Method;

/**
 * Created by lior on 7/10/17.
 */
public class DashboardService extends BaseService {


    public static void openDashboard() throws Exception {

        Navigator.dataAndBIPage();
        Navigator.refresh();
        Navigator.waitForLoad();
        Navigator.refresh();
        Navigator.waitForLoad();
    }

    public static void openKpiDropdown(String drpInd) throws Exception {

        DashboardPage dashboardPage = new DashboardPage();
        Method method = dashboardPage.getClass().getMethod("setDashboardKpisDropdown" + drpInd, String.class);
        method.invoke(dashboardPage,"Y");
        fillSeleniumObject(dashboardPage);

    }



}
