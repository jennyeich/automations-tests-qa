package hub.services.catalog;

import hub.base.BaseService;
import hub.common.objects.content.information.CatalogItemPage;
import hub.common.objects.content.information.CatalogPage;
import hub.hub1_0.services.Navigator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static hub.base.BaseService.*;

/**
 * Created by Goni on 14/08/2017.
 */
public class CatalogService {

    public static void createNewCatalog(CatalogPage catalog) throws Exception {
        Navigator.contentTabCatalogs();
        fillSeleniumObject(catalog);
        waitForLoad();
    }

    public static void selectCatalog(String catalogName) throws Exception {
        Navigator.contentTabCatalogs();
        getDriver().findElement(By.xpath("//a[contains(text(),\""+ catalogName +"\")]")).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }

    public static CatalogItemPage editCatalogItem(String title,String categoryName) throws Exception{
        CatalogItemPage catalogItemPage = new CatalogItemPage();
        catalogItemPage.setTitle(title);
        catalogItemPage.setCategory(categoryName);
        catalogItemPage.setAddItem(false);
        fillSeleniumObject(catalogItemPage);
        waitForLoad();
        return catalogItemPage;
    }

    public static CatalogItemPage selectCatalogItemFromList(CatalogItemPage catalogItem) throws InterruptedException {

        WebElement categoryDiv = getCategory(catalogItem.getCategory());
        WebElement catalogItemDiv = getCatalogItem(categoryDiv,catalogItem.getTitle());
        catalogItemDiv.click();
        return catalogItem;
    }


    public static boolean isItemExist(CatalogItemPage catalogItem){

        try {
            WebElement categoryDiv = getCategory(catalogItem.getCategory());
            WebElement catalogItemDiv = getCatalogItem(categoryDiv,catalogItem.getTitle());
            return catalogItemDiv!=null;
        } catch (Exception e) {
            return false;
        }
    }

    public static WebElement getCategory(String name){
        WebElement categoriesCollection = BaseService.findElementByDiffrentAttribute("ng-model","ctrlCList.items");
        List<WebElement> liCategories = BaseService.findElementsByDiffrentAttribute(categoriesCollection,"ng-repeat","item in ctrlCList.items");
        for (WebElement element:liCategories) {
            String categoryName = element.findElement(By.tagName("div")).getText();
            if (categoryName.contains(name)){
                return element;
            }

        }
        return null;
    }

    public static WebElement getCatalogItem(WebElement categoryDiv,String _itemName) throws InterruptedException {
        WebElement catalogItemsCollection = categoryDiv.findElement(By.tagName("ul"));
        List<WebElement> liItems =  BaseService.findElementsByDiffrentAttribute(catalogItemsCollection,"ng-repeat","item in item.items");
        for (WebElement item:liItems) {
            String itemName = item.findElement(By.tagName("div")).getText();
            if (itemName.contains(_itemName)){
                WebElement div = item.findElement(By.tagName("div"));
                BaseService.scrollToElement(div);
                WebElement a = div.findElement(By.tagName("a"));
                Thread.sleep(2000);
                return a;
            }
        }
        return null;
    }
}
