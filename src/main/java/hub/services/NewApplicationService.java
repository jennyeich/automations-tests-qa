package hub.services;

import hub.base.BaseService;
import hub.base.basePages.NewApplicationPage;
import hub.common.objects.common.RegistrationField;
import hub.common.objects.common.YesNoList;
import hub.common.objects.operations.AppSettingsPage;
import hub.common.objects.operations.GeneralPOSSettingsPage;
import hub.common.objects.operations.RegistrationFieldPage;
import hub.common.objects.operations.RegistrationFormPage;
import hub.common.pages.NewAppIntroductionPage;
import hub.hub1_0.services.Navigator;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import server.common.ApiClientFields;
import server.common.Response;
import server.internal.UpdateApiClient;
import utils.AutomationException;
import utils.EnvProperties;
import utils.PropsReader;

import java.io.File;
import java.util.List;
import java.util.Random;

/**
 * Created by Goni on 3/23/2017.
 */
public class NewApplicationService extends BaseService {

    public static final String APP_NAME_PREFIX = "Automation_";
    public static final Logger log4j = Logger.getLogger(NewApplicationService.class);
    public static final int WAIT_AFTER_SELENIUM_ACTION_MILLIS = 1000;
    public static final String VERSION_TITLE = "version";
    public static final String NO_BUSINESS = "None";
    public static CharSequence timestamp = String.valueOf(System.currentTimeMillis());
    public static final String HUB_USER = PropsReader.getPropValuesForEnv("user");
    public static final String FACEBOOK_USER ="https://www.facebook.com/ladoreecafe/";
    private static final String PUBLISH_BTN_XPATH = "//button[@automationid=\"publishBtn\"]";

    public static EnvProperties envProperties;
    private static String appName;

    public static EnvProperties getEnvProperties() {
        if (envProperties == null) {
            setEnvProperties();
        }
        return envProperties;
    }


    public static int publishApp() throws Exception {
        Navigator.contentTabPublishApp();
        publish();
        waitForLoad();
        return getVersion();
    }

    public static int getVersion() throws InterruptedException {
        getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@automationid=\'"+VERSION_TITLE+"\']")));
        String publishVersionTitle =findElementByAutomationId(VERSION_TITLE).getText();
        String publishVersionString = publishVersionTitle.substring(publishVersionTitle.lastIndexOf(' '));
        int version = (Integer.valueOf(publishVersionString.trim()).intValue());
        return version;
    }

    public static int createAppFromScratch() throws Exception {
        waitForLoad(By.id("loading-bar-spinner"));
        createNewAppRegisterPage();
        //wait until logout label is visible - the page was loaded
        //init and publish first time
        initNewApplication();
        publish();
        waitForLoad();
        return getVersion();
    }
    public static void createNewApp() throws Exception {
        createNewAppFromUrl();
        //set env
        setEnvProperties();
        initRegistrationForm();
        configureApplication();
    }


    public static void createNewAppFromUrl() throws Exception{
        getCreateNewAppStartPage();
        createNewAppRegisterPage();
        //wait until logout label is visible - the page was loaded
        //init and publish first time
        initNewApplication();
        publish();
        waitForLoad();

    }

    private static void getCreateNewAppStartPage() {
        getDriver().get(PropsReader.getPropValuesForEnv("create.new.app.url"));
        log4j.info("in : " + PropsReader.getPropValuesForEnv("create.new.app.url"));
        waitForLoad(By.id("loading-bar-spinner"));
    }

    public static void createNewAppRegisterPage() throws Exception {

        String randNum = getRandomPhoneNumber();
        appName = APP_NAME_PREFIX  + randNum;

        //init new application with name as input from user
        if(!PropsReader.getPropValuesForEnv("busines.name").equals(NO_BUSINESS)){
            appName = PropsReader.getPropValuesForEnv("busines.name");
        }

        log4j.info("New Application name: " + appName);

        WebElement appNameElm = getDriver().findElement(NewApplicationPage.APP_NAME);
        appNameElm.sendKeys(appName);
        Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);

        //phone prefix
        WebElement phonePrefix = getDriver().findElement(NewApplicationPage.PHONE_PREFIX);
        phonePrefix.click();
        Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);
        String prefix = PropsReader.getPropValuesForEnv("phone.prefix");
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        Select select1 = new Select(phonePrefix);
        select1.selectByVisibleText(prefix);
        phonePrefix.click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        WebElement phone = getDriver().findElement(NewApplicationPage.PHONE);
        phone.sendKeys(randNum);
        Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);

        WebElement startBtn = getDriver().findElement(NewApplicationPage.START);
        startBtn.click();
        Thread.sleep(8000);
        log4j.info("Finished Register page for new app" );
    }



    private static String getRandomPhoneNumber() {

        long timeSeed = System.nanoTime(); // to get the current date time value
        double randSeed = Math.random() * 1000; // random number generation
        long midSeed = (long) (timeSeed * randSeed); // mixing up the time and

        String s = midSeed + "";
        String subStr = s.substring(0, 9);
        return subStr;
    }

    public static void createAppWithFacebook() throws Exception {
        waitForLoad(By.id("loading-bar-spinner"));
        createNewAppRegisterPage();
        getDriver().findElement(By.name("facebook")).sendKeys(FACEBOOK_USER);
        Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);
        getDriver().findElement(By.className("FacebookFetchButton")).click();
        Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);
        Thread.sleep(10000);
        waitForLoad(By.id("loading-bar-spinner"));
        WebElement general = getDriver().findElement(NewAppIntroductionPage.GENERAL);
        general.sendKeys("this is a test application for automated tests " + timestamp.toString());
        clickNextButton();
        clickNextButton();
        clickNextButton();
        clickNextButton();

    }
    private static void clickNextButton() throws InterruptedException {
        String clickNextScript = "$('.submitButton').click();";
        executeJavascript(clickNextScript);
        Thread.sleep(2000);
        waitForLoad();
    }
    public static void initNewApplication() throws AutomationException{
        try {

            String clickNextScript = "$('.submitButton').click();";
            getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(NewAppIntroductionPage.APP_NAME));
            WebElement appNameElm = getDriver().findElement(NewAppIntroductionPage.APP_NAME);
            appNameElm.sendKeys(appName);
            Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);

            WebElement website = getDriver().findElement(NewAppIntroductionPage.WEBSITE);
            website.sendKeys("http://www.qa_auto" + timestamp.toString() + ".com/");
            Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);

            WebElement phone = getDriver().findElement(NewAppIntroductionPage.PHONE);
            phone.sendKeys(timestamp.toString());
            Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);

            WebElement general = getDriver().findElement(NewAppIntroductionPage.GENERAL);
            general.sendKeys("this is a test application for automated tests " + timestamp.toString());
            Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);

            WebElement position = getDriver().findElement(NewAppIntroductionPage.POSITION);
            position.sendKeys(timestamp.toString());
            Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);

            //next to page 2
            executeJavascript("$('.submitButton')[1].click();");
            Thread.sleep(5000);

            //fill background+logo
            String logoIcon = "(//*[@id=\"logoIcon\"]/div/div[2]/div[1]/input[@type=\"file\"])";
            WebElement logoIconElm = getDriver().findElement(By.xpath(logoIcon));
            uploadFile(logoIconElm);
            Thread.sleep(5000);

            String appBackgroundImage = "(//*[@id=\"appBackgroundImage\"]/div/div[2]/div[1]/input[@type=\"file\"])";
            WebElement appBackgroundImageElm = getDriver().findElement(By.xpath(appBackgroundImage));
            uploadFile(appBackgroundImageElm);
            Thread.sleep(5000);

            selectColorSchema();

            //next again to page 3
            executeJavascript(clickNextScript);
            Thread.sleep(5000);

            //fill content
            String xpathToChooseFile = "(//div[@class=\"upload-image\"]/input[@type=\"file\"])";
            List<WebElement> images = getDriver().findElements(By.xpath(xpathToChooseFile));
            for (int i = 1; i <= images.size(); i++) {
                WebElement uploadFileField = getDriver().findElement(By.xpath(xpathToChooseFile + "[" + i + "]"));
                uploadFile(uploadFileField);

                Thread.sleep(10000);
                WebElement title = getDriver().findElement(By.xpath("(//input[@name=\"wmTitle\"])" + "[" + i + "]"));
                String titleStr = title.getAttribute("value");
                title.clear();
                Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
                title.sendKeys(titleStr);
                if(i ==2){
                    executeJavascript("window.scrollTo(0,1000);");
                }
            }
            Thread.sleep(5000);
            //next to page 4
            executeJavascript(clickNextScript);
            Thread.sleep(2000);
            executeJavascript("window.scrollTo(0, 0);");
            Thread.sleep(10000);

            WebElement subscription = getDriver().findElement(By.xpath("//*[@id=\"sidebar\"]/div/select"));
            subscription.click();
            Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);
            Select select = new Select(subscription);
            select.selectByValue("2");
            Thread.sleep(3000);

            executeJavascript("$('#nextStep').click();");
            Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);
            Thread.sleep(8000);
            (new WebDriverWait(getDriver(), 10)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PUBLISH_BTN_XPATH)));
            log4j.info("Finished Creation of new application" );

        } catch (Exception e) {
            if(e instanceof NoSuchElementException){
                handleNoSuchElementException((NoSuchElementException)e);
            }else{
                throw new AutomationException(e);
            }

        }
    }

    private static void selectColorSchema() throws Exception{
        waitForLoad();
        long index = Math.abs((new Random()).nextInt(10));
        String xpath = "//*[@id=\"colorscheme\"]/div[" + (index+1) + "]";
        WebElement colorSchemaItem = getDriver().findElement(By.xpath(xpath));
        colorSchemaItem.click();
        Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);
        Thread.sleep(3000);
    }

    private static void uploadFile(WebElement element) {
        if (getDriver() instanceof RemoteWebDriver) {
            LocalFileDetector detector = new LocalFileDetector();
            File localFile = detector.getLocalFile(resourcesDir + "cat.jpg");
            if (element instanceof RemoteWebElement)
                ((RemoteWebElement) element).setFileDetector(detector);
            String absolutePath = localFile.getAbsolutePath();
            element.sendKeys(absolutePath);
        } else {
            element.sendKeys(resourcesDir + "cat.jpg");
        }
    }

    public static void publish() throws InterruptedException {

        getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PUBLISH_BTN_XPATH)));
        getDriverWait().until(ExpectedConditions.elementToBeClickable(By.xpath(PUBLISH_BTN_XPATH)));
        WebElement publishBtn = getDriver().findElement(By.xpath(PUBLISH_BTN_XPATH));
        Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);
        publishBtn.click();
        Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);
        (new WebDriverWait(getDriver(), 600)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath(PUBLISH_BTN_XPATH)));
        log4j.info("Finished Publish" );
    }

    private static void configureApplication() throws Exception {
        initAppSettings();
        performSaveAPIClientSettings();
    }

    private static void initAppSettings() throws Exception {
        try {
            Navigator.operationTabAppSettings();
            Navigator.refresh();
            AppSettingsPage appSettingsPage = new AppSettingsPage();
            fillSeleniumObject(appSettingsPage);
            waitForLoad();
            log4j.info("Finished initAppSettings");

        } catch (NoSuchElementException e) {
        handleNoSuchElementException(e);
    }
}

    private static void performSaveAPIClientSettings() {
        //Save requiered settings for the application to run test as needed
        UpdateApiClient saveApiClientRequest = new UpdateApiClient(getEnvProperties().getServerToken());
        saveApiClientRequest.addProp(ApiClientFields.PAYMENT_REQUIRES_VERIFICATION_CODE.key(), false);
        saveApiClientRequest.addProp(ApiClientFields.ALLOW_MULTIPLE_PURCHASES_FOR_POS_IDENTIFIERS.key(), true);
        saveApiClientRequest.addProp(ApiClientFields.AUTOMATION_DELAY_MS.key(), 2000);
        saveApiClientRequest.addProp(ApiClientFields.ACCUMULATION_VERSION.key(), 2);
        saveApiClientRequest.sendRequestByLocation(getEnvProperties().getLocationId(), Response.class);
    }


    public static void initRegistrationForm() throws Exception {
        Navigator.operationsTabRegistrationForm();
        waitForLoad();
        RegistrationFormPage registrationFormPage = new RegistrationFormPage();
        registrationFormPage.setExternalClubYesNo(YesNoList.NO.toString());
        //registrationFormPage.addField(createField(RegistrationField.Email.toString())); - Now added as default when creating new app
        registrationFormPage.addField(createField(RegistrationField.Gender.toString()));
        registrationFormPage.addField(createField(RegistrationField.ExpirationDate.toString()));
        registrationFormPage.addField(createField(RegistrationField.Anniversary.toString()));
        registrationFormPage.addField(createField(RegistrationField.OfficialIDNumber.toString()));
        registrationFormPage.addField(createField(RegistrationField.ClubMemberID.toString()));
        registrationFormPage.addField(createField(RegistrationField.CarNumber.toString()));
        registrationFormPage.addField(createField(RegistrationField.GenericInteger.toString()));
        registrationFormPage.addField(createField(RegistrationField.AllowEmail.toString()));
        registrationFormPage.addField(createField(RegistrationField.AllowSMS.toString()));
        fillSeleniumObject(registrationFormPage);
        waitForLoad();
        log4j.info("Finished initRegistrationForm");
    }

    private static RegistrationFieldPage createField(String field) {
        RegistrationFieldPage fieldPage = new RegistrationFieldPage();
        fieldPage.setFieldType(field.toString());
        fieldPage.setFieldTitle(field.toString());
        return fieldPage;
    }


    private static String getLocationID() throws Exception{

        String url = getDriver().getCurrentUrl();

        String locationId = url.substring(url.indexOf("_id=") + 4, url.lastIndexOf("&"));
        String locId =  PropsReader.getPropValuesForEnv("location_prefix") + locationId;
        log4j.info("locationId : " + locId);
        return locId;

    }

    private static void setEnvProperties() {

        envProperties = EnvProperties.getInstance();
        try {
            envProperties.setLocationId(getLocationID());
            envProperties.setServerToken(PropsReader.getPropValuesForEnv("serverToken"));
            envProperties.setApiKey(getApiKey());
            envProperties.setApplicationName(appName);
            log4j.info("Finished setEnvProperties -  ApiKey:" + envProperties.getApiKey());
            log4j.info("LocationId: " + envProperties.getLocationId());
            log4j.info("Application Name: " + envProperties.getApplicationName());
        } catch (Exception e) {
            log4j.error("Failed reading properties file",e);
        }

    }

    private static String getApiKey() {

        String apiKeyText = "NO_API_KEY";
         try {
             Navigator.operationsTabPOSSettings();
             WebElement posSettingsPage = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(GeneralPOSSettingsPage.API_KEY)));
             apiKeyText  = posSettingsPage.getText();
         }catch (Exception e){
            log4j.error(e.getMessage(),e);
        }
                log4j.info("apiKey : " + apiKeyText);
        return apiKeyText;

    }


    public static void createNewAppRegisterPageWithCountrySelection(String country) throws Exception {

        try {
            getCreateNewAppStartPage();
            String randNum = getRandomPhoneNumber();
            appName = APP_NAME_PREFIX  + randNum;

            //init new application with name as input from user
            if(!PropsReader.getPropValuesForEnv("busines.name").equals(NO_BUSINESS)){
                appName = PropsReader.getPropValuesForEnv("busines.name");
            }

            log4j.info("New Application name: " + appName);

            WebElement appNameElm = getDriver().findElement(NewApplicationPage.APP_NAME);
            appNameElm.sendKeys(appName);
            Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);


            //phone prefix
            WebElement phonePrefix = getDriver().findElement(NewApplicationPage.PHONE_PREFIX);
            phonePrefix.click();
            Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            Select select1 = new Select(phonePrefix);
            select1.selectByVisibleText(country);
            phonePrefix.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

            WebElement phone = getDriver().findElement(NewApplicationPage.PHONE);
            phone.sendKeys(randNum);
            Thread.sleep(WAIT_AFTER_SELENIUM_ACTION_MILLIS);

            WebElement startBtn = getDriver().findElement(NewApplicationPage.START);
            startBtn.click();
            Thread.sleep(8000);
            log4j.info("Finished Register page for new app" );

            initNewApplication();

            publish();
            waitForLoad();
            setEnvProperties();


        } catch (NoSuchElementException e) {
            handleNoSuchElementException(e);
        }



    }



}
