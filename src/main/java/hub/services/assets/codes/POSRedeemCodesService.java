package hub.services.assets.codes;


import hub.base.BaseService;
import hub.base.basePages.BaseBenefitsPage;
import hub.hub1_0.base.basePages.BaseTabsPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by Lior on 11/3/2016.
 */



public class POSRedeemCodesService extends BaseService{

    public static void addPOSRedeemCodeBulk ( String name, String SKU, String numberOfCodes, String codeLength) throws Exception {

        waitForLoad();

        System.out.println("in addPOSRedeemCodeBulk ");
        getDriver().findElement(BaseTabsPage.TAB_BENEFITS).click();


        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);


        getDriver().findElement(BaseBenefitsPage.Codes).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        getDriver().findElement(BaseBenefitsPage.POSRedeemCodes).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        //check if codes for automation ('ServerAutomation')  already generated
        List<WebElement> codesList = getDriver().findElements(By.xpath("//*[@id=\"main\"]/div/div"));
        for (int i=1 ; i<codesList.size() ; i++) {
            WebElement code = codesList.get(i);
            if(code.getText().equals(name))
                return;

        }
        getDriver().findElement(BaseBenefitsPage.AddPOSRedeemCodes).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        getDriver().findElement(BaseBenefitsPage.AddPOSRedeemCodes_Name).sendKeys(name);
        getDriver().findElement(BaseBenefitsPage.AddPOSRedeemCodes_RewardSKU).sendKeys(SKU);


        getDriver().findElement(BaseBenefitsPage.AddPOSRedeemCodes_SaveButton).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        waitForLoad();

        getDriver().findElement(BaseBenefitsPage.AddPOSRedeemCodes_CreateBulkArrow).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        getDriver().findElement(BaseBenefitsPage.AddPOSRedeemCodes_BulkName).sendKeys(name);
        getDriver().findElement(BaseBenefitsPage.AddPOSRedeemCodes_BulkTag).sendKeys(name);
        getDriver().findElement(BaseBenefitsPage.AddPOSRedeemCodes_BulkDescription).sendKeys(name);
        getDriver().findElement(BaseBenefitsPage.AddPOSRedeemCodes_NumberOfCodes).sendKeys(numberOfCodes);
        getDriver().findElement(BaseBenefitsPage.AddPOSRedeemCodes_CodeLength).sendKeys(codeLength);
        getDriver().findElement(BaseBenefitsPage.AddPOSRedeemCodes_GenerateNewCodes).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

      waitForLoad();
    }
}
