package hub.services.assets;

import hub.base.BaseService;
import hub.base.basePages.BaseBenefitsPage;
import hub.common.objects.benefits.LotteryReward;
import hub.common.objects.benefits.PointShopItem;
import hub.common.objects.benefits.PointShopPage;
import hub.common.objects.benefits.giftCard.GiftCard;
import hub.common.objects.benefits.giftCard.GiftCardBulkCode;
import hub.common.objects.benefits.redeemCode.CouponCode;
import hub.common.objects.benefits.redeemCode.ThirdPartyCodes;
import hub.common.objects.operations.JoiningCode;
import hub.common.objects.smarts.SmartClubDeal;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.SmartPunchCard;
import hub.hub1_0.common.objects.benefits.smartGiftActions.ClubDeal;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Preset;
import hub.hub1_0.services.Navigator;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import utils.AutomationException;

import java.util.List;


/**
 * Created by Jenny on 9/21/2016
 */
public class AssetService extends BaseService {



    //Finds the gift according to the title
    public static boolean findGift(String title) throws Exception {

        Navigator.benefitsTabSmartGifts();
        return Navigator.findGift(title);
    }


    //Finds the lottery according to the title
    public static boolean findLotteryReward(String title) throws Exception {

        Navigator.benefitsTabLotteryRewardsMenu();
        return Navigator.findLotteryReward(title);
    }

    //Finds the gift according to the title
    public static boolean findClubDeal(String title) throws Exception {

        Navigator.benefitsTabSmartClubDeals();
        return Navigator.findGift(title);
    }


    public static SmartGift returnSmartGiftObject (String title) throws Exception {

        SmartGift gift = new SmartGift();
        if (findGift(title)) { //find and click on the gift
            readSeleniumObject(gift); //fill all gift fields
            return gift;
        }
        return null;
    }



    public static void createSmartGift(SmartGift gift) throws Exception {

        if (gift.getSaveAsPreset()!=null)
            gift.setSaveAsPresetArrow("Yes");
        else
            gift.setSaveButton("Yes");
        //Navigate to benefits tab -> add smart gift
        Navigator.benefitsTabAddSmartGift();
        fillSeleniumObject(gift);
        getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseBenefitsPage.GiftMainTitle));
        waitForLoad();
        Navigator.refresh();
        log4j.info("gift was created successfully. gift's title is:  " + gift.getTitle());
    }


    public static void createGiftCard (GiftCard giftCard) throws InterruptedException, AutomationException {

        //Navigate to benefits tab -> add smart gift
        Navigator.benefitsTabAddGiftCard();
        fillSeleniumObject(giftCard);
        getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseBenefitsPage.GiftCardsMainTitle));
        waitForLoad();

    }

    public static void generateGiftCardCodes (GiftCardBulkCode giftCardBulkCode) throws Exception {

        fillSeleniumObject(giftCardBulkCode);
        waitForLoad();
        Navigator.refresh();

    }



    public static void createSmartPunchCard(SmartPunchCard punchCard) throws Exception {

        if (punchCard.getSaveAsPreset()!=null)
            punchCard.setSaveAsPresetArrow("Yes");
        else
            punchCard.setSaveButton("Yes");

        //Navigate to benefits tab -> manage->add punch card
        Navigator.benefitsTabAddSmartPunchCards();
        fillSeleniumObject(punchCard);
        waitForLoad();
        Navigator.refresh();
    }

    public static void createSmartClubDeal(SmartClubDeal smartClubDeal) throws InterruptedException, AutomationException {

        if (smartClubDeal.getSaveAsPreset()!=null)
            smartClubDeal.setSaveAsPresetArrow("Yes");
        else
            smartClubDeal.setSaveButton("Yes");

        //Navigate to benefits tab -> add smart gift
        Navigator.benefitsTabAddSmartClubDeal();
        fillSeleniumObject(smartClubDeal);
        getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseBenefitsPage.GiftMainTitle));
        waitForLoad();
        Navigator.refresh();
    }

    public static void createOldClubDeal(ClubDeal clubDeal) throws InterruptedException, AutomationException {

        //Navigate to benefits tab -> add club deal
        Navigator.benefitsTabAddClubDeal();
        fillSeleniumObject(clubDeal);
        getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseBenefitsPage.GiftMainTitle));
        waitForLoad();
        Navigator.refresh();
    }
    public static void addItemToPointShop(PointShopPage pointShop,String giftTitle) throws InterruptedException, AutomationException{

        //Navigate to benefits tab -> add smart gift to point shop
        Navigator.benefitsTabPointShop();
        PointShopItem item = new PointShopItem(pointShop);
        item.setTitle(giftTitle);
        item.setDescription(giftTitle);
        item.setPrice("2");
        item.setChooseGift(giftTitle);
        pointShop.addItem(item);
        fillSeleniumObject(pointShop);
        waitForLoad();
    }

    public static void createLotteryReward(LotteryReward lotteryReward) throws Exception{
        Navigator.benefitsTabAddLotteryRewards();
        fillSeleniumObject(lotteryReward);
        waitForLoad();
        Navigator.refresh();
    }

    public static void createThirdPartCodes (ThirdPartyCodes thirdPartyCodes) throws Exception {

        Navigator.thirdPartCodes();
        fillSeleniumObject(thirdPartyCodes);
        waitForLoad();
    }


    public static void deletePresetFromNewSmartGift (Preset preset) throws Exception {

        try {
            Navigator.benefitsTabAddSmartGift();
                waitForLoad();
            Thread.sleep(10000);
            fillSeleniumObject(preset);
            Thread.sleep(10000);
        }catch (AutomationException e){
            if(e.getMessage().contains("ElementNotVisibleException"))
                return;
            if(e.getMessage().contains("NoSuchElementException"))
                return;
        }


    }

    public static void deletePresetFromExistingGift (SmartGift smartGift, Preset preset) throws Exception {

        try {
            findGift(smartGift.getTitle());
            waitForLoad();
            Thread.sleep(10000);
            fillSeleniumObject(preset);
            Thread.sleep(10000);
        }catch (AutomationException e) {
            if(e.getMessage().contains("ElementNotVisibleException"))
                return;
            if(e.getMessage().contains("NoSuchElementException"))
                return;
        }
    }



    public static Boolean checkIfPresetExistsSmartGift(String presetName) throws Exception{
        Thread.sleep(10000);
        Navigator.benefitsTabAddSmartGift();
        Thread.sleep(20000);
        java.util.List<WebElement> presets = getDriver().findElements(By.id("preset"));
        Select select = new Select(presets.get(0));
        try {
            select.selectByVisibleText(presetName);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    public static void deletePresetFromNewClubDeal (Preset preset) throws Exception{

        try {
            Navigator.benefitsTabAddSmartClubDeal();
            waitForLoad();
            Thread.sleep(10000);
            fillSeleniumObject(preset);
            Thread.sleep(10000);
        }catch (AutomationException e)
        {
            if(e.getMessage().contains("ElementNotVisibleException"))
                return;
            if(e.getMessage().contains("NoSuchElementException"))
                return;
        }
    }

    public static void deletePresetFromExistingClubDeal (SmartClubDeal smartGift, Preset preset) throws Exception {
        try {
            findClubDeal(smartGift.getTitle());
            waitForLoad();
            fillSeleniumObject(preset);
        }catch (AutomationException e)
        {
            if(e.getMessage().contains("ElementNotVisibleException"))
                return;
            if(e.getMessage().contains("NoSuchElementException"))
                return;
        }

    }



    public static Boolean checkIfPresetExistsClubDeal(String presetName) throws Exception{
        Thread.sleep(10000);
        Navigator.benefitsTabAddSmartClubDeal();
        Thread.sleep(20000);
        List<WebElement> presets = getDriver().findElements(By.id("preset"));
        Select select = new Select(presets.get(0));
        try {
            select.selectByVisibleText(presetName);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }


    public static void removeItemsFromPointShop() throws Exception{

        Navigator.benefitsTabPointShop();
        List<WebElement> removeBtns = getDriver().findElements(By.xpath("//*[@class=\"remove-button\"]"));
        while (!removeBtns.isEmpty()) {
            WebElement element = removeBtns.get(removeBtns.size()-1);
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            checkForAlerts();
            removeBtns = getDriver().findElements(By.xpath("//*[@class=\"remove-button\"]"));
        }
    }

    public static void createPointShopWithItems(PointShopPage pointShop) throws Exception{
        //Navigate to benefits tab -> add smart gift to point shop
        Navigator.benefitsTabPointShop();
        fillSeleniumObject(pointShop);
        waitForLoad();
    }

    public static String getLotteryRewardId(String name)throws Exception {

        String id = "";
        try{
            findLotteryReward(name);
            id = concatNumber();
        }catch(TimeoutException e){
            //try again
            Thread.sleep(2000);
            findLotteryReward(name);
            id = concatNumber();
        }
        return id;


    }

    private static String concatNumber() throws InterruptedException {
        String url = getDriver().getCurrentUrl();
        String autoNumber = url.substring(url.lastIndexOf("/") + 1, url.indexOf("?"));
        return autoNumber;
    }

    public static void createJoiningCodes(JoiningCode joiningCode) throws Exception{
        Navigator.joiningCodes();
        fillSeleniumObject(joiningCode);
        waitForLoad();
    }

    public static void createCouponCodeBulkCodes(CouponCode couponCode) throws Exception{
        Navigator.couponCodes();
        fillSeleniumObject(couponCode);
        waitForLoad();
    }
}

