package hub.services;

import hub.common.objects.member.FilterByBigQuery;
import hub.common.objects.member.FilterBySQL;
import hub.common.objects.member.FilterMember;
import hub.hub1_0.services.Navigator;

import static hub.base.BaseService.WAIT_AFTER_CLICK_MILLSEC;
import static hub.base.BaseService.fillSeleniumObject;
import static hub.base.BaseService.waitForLoad;

/**
 * Created by lior on 7/25/17.
 */
public class FilterMembersService {

    public static void openFilterMembersPageAndFilterAndOpenSmartActionDropDown() throws Exception {

        Navigator.dataAndBIPageFilterMember();
        FilterByBigQuery byBigQuery = new FilterByBigQuery();
        //FilterBySQL sql = new FilterBySQL();
        fillSeleniumObject(byBigQuery);
        waitForLoad();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        FilterMember filterMember = new FilterMember();
        filterMember.setPerformSmartActionDropDown("Y");
        fillSeleniumObject(filterMember);
    }
}
