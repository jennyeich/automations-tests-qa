package hub.services;

import hub.base.BaseService;
import hub.common.objects.common.*;
import hub.common.objects.member.FilterMember;
import hub.common.objects.operations.ImportUsers;
import hub.common.objects.operations.UserPermissions;
import hub.common.pages.acp.*;
import hub.hub1_0.services.Navigator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import utils.AutomationException;
import utils.EnvProperties;
import utils.PropsReader;

import java.beans.IntrospectionException;
import java.text.MessageFormat;
import java.util.List;

/**
 * Created by lior on 7/9/17.
 */
public class AcpService extends BaseService {

    public static void setPermissionTag(String tagToSearch, String permissionLevel) throws Exception {

        Navigator.acpTabManageComoHubUsersUpdateUserCurrentUser();
        UserPermissions userPermissions = new UserPermissions();
        userPermissions.setTagSearchBox(tagToSearch);
        userPermissions.setPermissionLevelAllowOrDisallow(permissionLevel);
        userPermissions.setUserPermissionAddButton("Y");
        fillSeleniumObject(userPermissions);
        Navigator.refresh();
    }

    public static boolean isElementExist(String elementName) {
        return getDriver().findElements(By.xpath(elementName)).size() > 0;
    }


    public static boolean isElementExist(By element) {
        return getDriver().findElements(element).size() > 0;
    }

    public static boolean isElementExistInSelectOptionByLabel(String elementName) {
        return (getDriver().findElements(By.xpath("//option[@label=\"" + elementName + "\"]")).size() > 0);
    }


    public static void openFindCustomersInFilterMembers() throws Exception {
        Navigator.dataAndBIPageFilterMember();
        FilterMember filterMember = new FilterMember();
        filterMember.setFindCustomers("Y");
        fillSeleniumObject(filterMember);
    }

    public static void checkUserPermissionTags(String tagName) throws Exception {

        Navigator.acpTabManageComoHubUsersUpdateUserCurrentUser();

        List<WebElement> findUsersTagToDelete = getDriver().findElements(By.xpath(tagName));

        for (int i= (findUsersTagToDelete.size()-1); i>=0 ;i--)
        {
            WebElement tag = findUsersTagToDelete.get(i);
            tag.click();
            BaseService.checkForAlerts();
            Thread.sleep(3000);
            findUsersTagToDelete = getDriver().findElements(By.xpath(tagName));
            Thread.sleep(3000);

        }
    }


    public static void uncheckApplication(String locationId) throws Exception{
        Navigator.acpTabManageComoHubUsersUpdateUserCurrentUser();
        String id = locationId.substring(locationId.indexOf("_")+1);
        WebElement checkbox = getDriver().findElement(By.xpath("//label/span[contains(text(),\"" + id + "\")]//../../input"));
        if(checkbox.isSelected())
            checkbox.click();
        getDriver().findElement(By.xpath("//button[contains(text(),\"Save\")]")).click();
        waitForLoad();
    }

    public static void importMembers(String fileToImport) throws Exception{
        Navigator.acpTabImportUsersImproved();
        ImportUsers importUsers = new ImportUsers();
        importUsers.setUploadFile(resourcesDir + fileToImport);
        importUsers.setIdentifier(UniqueIdentifier.PHONE_NUMBER.toString());
        fillSeleniumObject(importUsers);
        waitForLoad();
        Navigator.refresh();
    }

    public static void addFilterMemberPreset(String days, String actionType) throws Exception {
        Navigator.acpTabPresetsFilterMember();
        PresetFilterMembers presetFilterMembers = new PresetFilterMembers();
        presetFilterMembers.setTitle(actionType + " from the last " + days + " days");
        presetFilterMembers.setMemberActionType(actionType);

        presetFilterMembers.setChooseDate(FilterMembersChooseDates.CHOOSE_RANGE.toString());
        presetFilterMembers.setFromTheLastXDays(days);

        fillSeleniumObject(presetFilterMembers);
        waitForLoad();
    }

    public static void addUserPlan(String name,String description,Tags tag,PermissionLevel permissionLevel) throws Exception {
        Navigator.acpTabUserPlans();
        addPlan(name, description);
        //select user plan after save
        String xpath = MessageFormat.format("//a[contains(text(),\"{0}\")]",name);
        getDriver().findElement(By.xpath(xpath)).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
        setUserPlanPermissionTag(tag.toString(),permissionLevel.toString());
    }

    /**
     * addBusinessPlan - The businessPlan is always DISALLOW by default
     */
    public static void addBusinessPlan(String name,List<String> tagsToSearch) throws Exception{
        Navigator.acpTabBusinessPlans();
        addPlan(name, PermissionLevel.DISALLOW.toString());
        //select user plan after save
        String xpath = MessageFormat.format("//a[contains(text(),\"{0}\")]",name);
        getDriver().findElement(By.xpath(xpath)).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
        for (int i = 0; i < tagsToSearch.size(); i++) {
            BusinessPlanPage businessPlanPagePlanPage = new BusinessPlanPage();
            businessPlanPagePlanPage.setTagSearchBox(tagsToSearch.get(i));
            fillSeleniumObject(businessPlanPagePlanPage);
            waitForLoad();
        }
    }

    public static void deleteBusinessPlan(String name) throws Exception {
        Navigator.acpTabBusinessPlans();
        String trashCanXpath = MessageFormat.format("//a[contains(text(),\"{0}\")]//parent::div//a[contains(@class,\"icon-trash_can\")]", name);
        getDriver().findElement(By.xpath(trashCanXpath)).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        Navigator.refresh();
    }

    private static void addPlan(String name, String description) throws InterruptedException, AutomationException {

        UserPlans userPlans = new UserPlans();
        userPlans.setName(name);
        userPlans.setDescription(description);
        fillSeleniumObject(userPlans);
        waitForLoad();
        Navigator.refresh();
    }

    private static void setUserPlanPermissionTag(String tagToSearch, String permissionLevel) throws Exception {

        UserPlanPage userPlanPage = new UserPlanPage();
        userPlanPage.setTagSearchBox(tagToSearch);
        userPlanPage.setPermissionLevelAllowOrDisallow(permissionLevel);
        fillSeleniumObject(userPlanPage);
        waitForLoad();
    }

    public static void addPermissionTagUser() throws Exception {

        Navigator.acpTabManageComoHubUsersAddNewUser();
        NewHubUser newHubUser = new NewHubUser();
        String email = PropsReader.getPropValuesForHub("user.permission");
        String name = email.substring(0,email.indexOf("@"));
        newHubUser.setFirstName(name);
        newHubUser.setLastName(name);
        newHubUser.setEmail(email);
        newHubUser.setPassword(PropsReader.getPropValuesForHub("password.permission"));
        newHubUser.setSearchBy(NewHubUser.SearchBySelect.Location_id.toString());
        newHubUser.setSearchInput(EnvProperties.getInstance().getLocationId());
        fillSeleniumObject(newHubUser);
    }
}


