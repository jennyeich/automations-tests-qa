package hub.services.automations;

import hub.base.BaseService;
import hub.common.objects.oldObjects.*;
import hub.hub1_0.services.Navigator;
import utils.AutomationException;

import java.beans.IntrospectionException;

/**
 * Created by Goni on 06/06/2017.
 */
public class OldAutomationService extends BaseService {

    public static void createAutomation (OldAutomation autoamtion) throws AutomationException, InterruptedException {
        Navigator.operationsTabNewAutomation();
        fillSeleniumObject(autoamtion);
        waitForLoad();
        waitForLoad(BaseService.getByAutomationId("loading_spinner"));
    }



}
