package hub.services;

import hub.base.BaseService;
import hub.common.pages.HomeScreenPage;
import hub.hub1_0.services.Navigator;
import org.openqa.selenium.By;

/**
 * Created by lior on 7/12/17.
 */
public class HomeScreenService extends BaseService {

    public static void openHomeScreenAndShowLayouts() throws Exception {

        Navigator.contentTabLayoutHomeScreen();
        getDriver().findElement(By.xpath(HomeScreenPage.CHOOSE_LAYOUT_ARROW.toString())).click();
    }
}
