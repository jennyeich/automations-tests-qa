package hub.services;

import hub.base.BaseService;
import hub.base.basePages.BaseContentPage;
import hub.common.objects.common.NavigationType;
import hub.common.objects.common.YesNoList;
import hub.common.objects.content.Template;
import hub.common.objects.content.branding.DesignPage;
import hub.common.objects.content.forms.*;
import hub.common.objects.content.information.*;
import hub.hub1_0.services.Navigator;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;

import javax.mail.*;
import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;

/**
 * Created by taya.ashkenazi on 6/5/17.
 */
public class ContentService extends BaseService {
    public static final Logger log4j = Logger.getLogger(ContentService.class);


    private static final String pathToDir = System.getProperty("user.dir") + File.separator + "src" + File.separator + "test";
    private static final String downloadsDir = System.getProperty("user.dir") + File.separator + "src" + File.separator + "main" + File.separator + "resources" +
            File.separator + "downloads" + File.separator;
    public static final String CHECKMAIL_TXT_FILE = "checkmail.txt";


    public static void createNewForm(FormPage formPage) throws Exception {

        Navigator.contentTabGeneralForms();
        fillSeleniumObject(formPage);
        Navigator.refresh();

    }

    public static boolean isFormCreated (String formTitle,String pathPrefix) {
        waitForLoad();
        String xpath = MessageFormat.format(pathPrefix + "{0}\"" + ")]",formTitle);
        By title = By.xpath(xpath);
        boolean isExist = getDriver().findElements(title).size() > 0;
        return isExist;

    }


    public static boolean openFormInWeb (String formUrl) throws Exception{
        return openFormInWeb(formUrl,BaseContentPage.FormSubmitButton);
    }

    public static boolean openFormInWeb (String formUrl,By xpathButton) throws Exception{

        formUrl = formUrl.substring(0, formUrl.indexOf("&token"));
        switchToNewTab(formUrl); //switches to new tab
        Thread.sleep(6000);
        try {
            getDriver().findElement(xpathButton);
            return true;
        }catch(NoSuchElementException e){
            throw e;
        }

    }

    public static boolean openFormInWebWithUserToken(String formUrl,By xpathButton,String userToken) throws Exception{
        formUrl = formUrl.replaceAll("<user_token>",userToken);
        switchToNewTab(formUrl); //switches to new tab
        Thread.sleep(6000);
        boolean isOpenForm = getDriver().findElements(xpathButton).size() > 0;
        return isOpenForm;
    }

    public static void switchToNewTab(String url) {

        ((JavascriptExecutor) getDriver()).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<String>( getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs.get(1));
        getDriver().get(url);
    }

    public static void switchToNewTabNavigateLink(String url) {

        ((JavascriptExecutor) getDriver()).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<String>( getDriver().getWindowHandles());
        getDriver().switchTo().window(tabs.get(1));
        getDriver().navigate().to(url);
    }

    public static void closeFormTab (){
        WebDriver driver = getDriver();
        ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        driver.close();
        driver.switchTo().window(tabs.get(0));
    }


    public static boolean isCorrectFormOpeninWeb (String description){
        String descriptionText = getDriver().findElement(BaseContentPage.FormDescription).getText();
        return (description.equals(descriptionText));
    }

    public static String extractFormUrl (String formTitle){
        return  extractFormUrl(formTitle, GeneralFormPage.FORM_TITLE,GeneralFormPage.FORM_URL);
    }

    public static String extractFormUrl (String formTitle,String pathPrefix,String pathSuffix){
        String xpath = MessageFormat.format(pathPrefix + "{0}\"" + ")]" + "{1}",formTitle, pathSuffix);
        By title = By.xpath(xpath);
        String url = getDriver().findElement(title).getText();
        return  url;
    }

    public static void deleteForm (String formTitle) throws Exception{

        deleteForm(formTitle,GeneralFormPage.FORM_TITLE);
    }

    public static void deleteForm (String formTitle,String pathPrefix) throws Exception{

        Navigator.refresh();
        Navigator.contentTabGeneralForms();
        deleteTheForm(formTitle, pathPrefix);
    }

    public static void deleteFormFromSurvey (String formTitle,String pathPrefix) throws Exception{

        Navigator.refresh();
        Navigator.contentTabSurvey();
        deleteTheForm(formTitle, pathPrefix);
    }

    private static void deleteTheForm(String formTitle, String pathPrefix) throws InterruptedException {
        String xpath = MessageFormat.format(pathPrefix + "{0}\"" + ")]" + "{1}",formTitle, GeneralFormPage.DELETE_ICON);
        WebElement deleteIcon = getDriver().findElement(By.xpath(xpath));
        deleteIcon.click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        checkForAlerts();
    }

    public static void updateForm(String formTitle, FormFieldPage fieldPage) throws Exception {
        Navigator.refresh();
        Navigator.contentTabGeneralForms();
        String xpath = MessageFormat.format(GeneralFormPage.FORM_TITLE + "{0}\"" + ")]" + "{1}",formTitle, GeneralFormPage.EDIT_FIELDS);
        getDriver().findElement(By.xpath(xpath)).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
        addFormField(fieldPage);
    }

    public static void addFormField(FormFieldPage fieldPage) throws Exception {
        fillSeleniumObject(fieldPage);
        Navigator.refresh();
    }

    public static boolean checkFormField(String xpathField,String expected,String xpathFieldType) {

       boolean asExpected;
       if(xpathFieldType != null){
            String typeAttrPath = FormFieldsConstants.FIELD_ANSWER_TYPE + xpathFieldType + "'\"]";
            try {
                getDriver().findElement(By.xpath(typeAttrPath));
                asExpected = true;
            }catch (NoSuchElementException e){
                asExpected = false;
            }
       }else{

           try {
               WebElement element = getDriver().findElement(By.xpath(xpathField));
               String actual = element.getText();
               asExpected = actual.contains(expected);
           }catch (NoSuchElementException e){
               if(expected.equals(""))
                    asExpected = true;
               else
                   asExpected = false;
           }

       }

       return asExpected;
    }

    public static boolean submitForm(FormPage formPage) throws Exception{
        WebElement submitBtn = getDriver().findElement(By.xpath(FormFieldsConstants.SUBMIT_BUTTON));
        submitBtn.click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        try {
            WebElement thanksElm = getDriver().findElement(By.xpath("//*[@ng-bind=\"ctrlGeneralForm.info.form.thankyou_title\"]"));
            return thanksElm.getText().equals(formPage.getThankYouMessageTitle());

        }catch (NoSuchElementException e){
            return false;
        }
    }

    public static void fillForm(String value,String fieldType) {

        if(FormFieldPage.TEXT.equals(fieldType)) {
            WebElement input = getDriver().findElement(By.xpath("//input[@type=\"" + FormFieldPage.TEXT + "\"]"));
            input.sendKeys(value);
        }else if(FormFieldPage.CHECKBOX.equals(fieldType)){
            WebElement element = getDriver().findElement(By.xpath("//input[@type=\"" + FormFieldPage.CHECKBOX + "\"]"));
            element.click();
        }else if(FormFieldPage.TEXT_BOX.equals(fieldType)){
            WebElement input = getDriver().findElement(By.xpath("//textarea"));
            input.sendKeys(value);
        }
    }

    public static boolean checkMail(List<String> inputs, String subject) throws Exception {
        return checkMail(inputs, subject, true);
    }

    /**
     * Go over the gmail account automationcomo@gmail.com to look for the notification we send from the form in the HUB.
     * Get all the inbox messages and take the subject content.
     * @return true if the given message to search is found.
     * @throws Exception
     */
    public static boolean checkMail(List<String> inputs, String subject, boolean isExist) throws Exception{

        boolean result = false;

        String host = "imap.gmail.com";
        String username = "automationcomo@gmail.com";
        String password = "qaComo2017";

        //create properties field
        Properties properties = new Properties();

        properties.put("mail.imap.host", host);
        properties.put("mail.imap.ssl.enable", "true");
        properties.put("mail.imap.auth", "true");
        properties.setProperty("mail.store.protocol", "imap");

        Session session = Session.getDefaultInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username,password);
            }
        });

        //create the POP3 store object and connect with the pop server
        Store store = session.getStore("imaps");

        store.connect(host, username, password);

        //create the folder object and open it
        Folder emailFolder = store.getFolder("INBOX");
        emailFolder.open(Folder.READ_WRITE);

        // retrieve the messages from the folder in an array and print it
        Message[] messages = emailFolder.getMessages();

        for (int i = 0, n = messages.length; i < n; i++) {
            Message message = messages[i];
            String msg = message.getSubject();

            //the message we look for is found
            if(msg.contains(subject)) {
                result = true;
                FileOutputStream file = new FileOutputStream(pathToDir + File.separator + CHECKMAIL_TXT_FILE);
                message.writeTo(file);
                //read the content of the message as string
                String content = FileUtils.readFileToString(new File(pathToDir + File.separator + CHECKMAIL_TXT_FILE));
                String updatedContent = content.replaceAll("=\r\n", "");
                for(String input: inputs) {
                    if ( updatedContent.contains(input) != isExist) {
                        result = false;
                        log4j.info("failed for input: " + input);
                        break;
                    }
                }
                message.setFlag(Flags.Flag.DELETED, true);
                break;
            }

       }
        emailFolder.expunge();

        //close the store and folder objects
        emailFolder.close(true);
        store.close();

        return result;
    }

   public static void deleteGeneratedFiles() {

            File dir = new File(pathToDir);
            for (File file: dir.listFiles()) {
                if (!file.isDirectory()) {
                    log4j.info("Delete mail message content file: " + file.getName());
                    file.delete();
                }
            }
    }

    public static String exportResults(String formTitle) {
        return exportResults(formTitle,GeneralFormPage.FORM_TITLE);
    }

    public static String exportResults(String formTitle,String pathPrefix) {
        String xpath = MessageFormat.format(pathPrefix + "{0}\"" + ")]" + "{1}",formTitle, GeneralFormPage.EXPORT_RESULTS);
        getDriver().findElement(By.xpath(xpath)).click();
        waitForLoad();
        WebElement btn = getDriver().findElement(By.xpath(xpath +  "/../a"));
        String fileUrl = btn.getAttribute("data-ng-href");
        return fileUrl;
    }


    public static boolean readResults(String csvFile,List<String> inputs) throws Exception {

        boolean valid = true;

        URL oracle = new URL(csvFile);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(oracle.openStream()));
        String inputLine;
        boolean firstLine = true;
        while ((inputLine = in.readLine()) != null) {
            if(firstLine){
                firstLine = false;
                continue;
            }
            // use comma as separator
            for (String input: inputs) {
                if (inputLine.contains(input)) {
                    valid = valid && true;
                    log4j.info("Form field "  + input + " exist");
                } else {
                    valid = valid && false;
                    log4j.info("Failed test: Form field "  + input + " not exist");
                }
            }
        }
        in.close();
        return valid;
    }

    public static boolean readResults(File csvFile,List<String> inputs) throws Exception {
        boolean valid = true;
        BufferedReader in = new BufferedReader(
                new FileReader(csvFile));
        String inputLine;
        int i = 0;
        String input;
        while ((inputLine = in.readLine()) != null && i < inputs.size()) {
            input = inputs.get(i);
                if (inputLine.contains(input)) {
                    valid = valid && true;
                    log4j.info("Text "  + input + " exist");
                } else {
                    valid = valid && false;
                    log4j.info("Failed test: Text "  + input + " not exist");
                }
                i++;
        }
        in.close();
        return valid;
    }

    public static void startSurvey() throws Exception{
        WebElement startBtn = getDriver().findElement(BaseContentPage.SurveyStartButton);
        startBtn.click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
    }

    public static void createNewSurvey(SurveyPage surveyPage) throws Exception {

        Navigator.contentTabSurvey();
        fillSeleniumObject(surveyPage);
        Navigator.refresh();

    }

    public static void updateSurvey(String title, SurveyQuestionPage questionPage) throws Exception {
        String xpath = MessageFormat.format(MainSurveyPage.SURVEY_TITLE + "{0}\"" + ")]" + "{1}",title, MainSurveyPage.QUESTIONS);
        getDriver().findElement(By.xpath(xpath)).click();
        waitForLoad();
        createQuestion(questionPage);
    }

    public static void createQuestion(SurveyQuestionPage questionPage) throws Exception {
        fillSeleniumObject(questionPage);
        Navigator.refresh();
    }

    public static boolean isCorrectSurveyOpenInWeb(String title){
        String titleText = getDriver().findElement(BaseContentPage.SurveyTitle).getText();
        return (title.equals(titleText));
    }

    public static boolean checkIfSurveyAnswered(SurveyPage surveyPage){
        try {
            WebElement thanksElm = getDriver().findElement(By.xpath("//*[@ng-bind=\"ctrlGeneralForm.info.form.thankyou_title\"]"));
            return thanksElm.getText().equals(surveyPage.getThankYouMessageTitle());

        }catch (NoSuchElementException e){
            return false;
        }
    }

    public static boolean checkQuestion(SurveyQuestionPage questionPage,String xpathFieldType) throws Exception{

        boolean asExpected = false;
        //deal with answer types
        if(xpathFieldType != null){
            String typeAttrPath = FormFieldsConstants.FIELD_ANSWER_TYPE + xpathFieldType + "'\"]";
            try {

                if(YesNoList.YES.toString().equals(questionPage.getIsMandatory())){
                    String script = "";
                    if(xpathFieldType.equals(SurveyQuestionPage.TEXT)) {
                        script ="angular.element($('" + SurveyQuestionPage.INPUT + "')).val(\"is mandatory\").triggerHandler(\"change\");angular.element($('input')).scope().$apply();";
                    }else if(xpathFieldType.equals(SurveyQuestionPage.CHECKBOX)) {
                        script = "var element = document.evaluate('" + "//button[contains(text(),\"is mandatory\")]" + "' ,document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null ).singleNodeValue;" +
                                "angular.element(element).triggerHandler(\"click\"); ";
                    }else if(xpathFieldType.equals(SurveyQuestionPage.TEXT_BOX)) {
                        script ="angular.element($('" + SurveyQuestionPage.TEXT_AREA + "')).val(\"is mandatory\").triggerHandler(\"change\");angular.element($('input')).scope().$apply();";
                    }
                    executeJavascript(script);
                    Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
                    //if question is mandatory,the button will only appears after filling data
                    try {
                        getDriver().findElement(BaseContentPage.NextQuestionButton);
                        asExpected = true;
                    } catch (NoSuchElementException e) {
                        asExpected = false;
                    }

                }else{
                    //if question not mandatory,the button should appear anyway
                    try {
                        getDriver().findElement(BaseContentPage.NextQuestionButton);
                        asExpected = true;
                    }catch (NoSuchElementException e){
                        asExpected = false;
                    }
                }
            }catch (NoSuchElementException e){
                asExpected = false;
            }
        }
        return asExpected;
    }

    public static void createGroup(String timestamp,int numOfBranches) throws Exception{

        Navigator.contentTabLocations();
        LocationsPage locationsPage = new LocationsPage();
        locationsPage.setGroupTitle("Group" + timestamp);
        for (int i=0;i<numOfBranches;i++) {
            LocationInGroupPage locationGroupPage = new LocationInGroupPage();
            locationGroupPage.setAddress(String.valueOf(generateRandomChar()) + String.valueOf(generateRandomChar()));
            //generate 7 digits random number
            String phone = String.valueOf(Math.round(Math.random() * 8999999) + 1000000);
            locationGroupPage.setPhone(phone);
            locationsPage.addLocationInGroup(locationGroupPage);
        }
        fillSeleniumObject(locationsPage);
        waitForLoad();
    }
    public static char generateRandomChar(){
        Random r = new Random();
        char c = (char)(r.nextInt(26) + 'a');
        return c;
    }


    public static void deactivateMessage(int messageIndex) throws Exception{
        Navigator.contentTabWelcomeMessages();
        getDriver().findElement(By.xpath("//*[@id=\"main\"]/div/ul/li[" + messageIndex + "]//div[@class=\"block_actions\"]/a[1]")).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }

    public static void addWebView(String webViewName, String baseUrl) throws Exception{
        Navigator.contentTabWebViews();
        WebViewPage webViewPage = new WebViewPage();
        webViewPage.setName(webViewName);
        webViewPage.setTitle(webViewName);
        webViewPage.setBaseUrl(baseUrl);
        fillSeleniumObject(webViewPage);
        waitForLoad();
    }

    public static void addCustomScreen(String customScreenName, String action) throws Exception{
        Navigator.contentTabCustomScreen();
        CustomScreenPage customScreenPage = new CustomScreenPage();
        customScreenPage.setItemTitle(customScreenName);
        customScreenPage.setButtonAction(action);
        fillSeleniumObject(customScreenPage);
        waitForLoad();
    }

    public static void addBadge(String tagName) throws Exception{
        Navigator.contentTabBadges();
        BadgesPage badgesPage = new BadgesPage();
        badgesPage.setTagName(tagName);
        fillSeleniumObject(badgesPage);
        waitForLoad();
    }

    public static void addPhoto(String photoName,String owner) throws Exception{
        Navigator.contentTabPhotoGallery();
        PhotoGalleryPage photoPage = new PhotoGalleryPage();
        photoPage.setItemTitle(photoName);
        photoPage.setOwner(owner);
        fillSeleniumObject(photoPage);
        waitForLoad();
    }

    public static void addSplashScreen(String name) throws Exception{
        Navigator.contentTabSplashScreen();
        SplashScreenPage page = new SplashScreenPage();
        page.setTitle(name);
        fillSeleniumObject(page);
        waitForLoad();
    }

    public static void setNavigationType(String navigationType) throws Exception{
        Navigator.contentTabDesign();
        DesignPage page = new DesignPage();
        if(NavigationType.SIDE_MENU.toString().equals(navigationType))
            page.setUseSideMenu(NavigationType.SIDE_MENU.toString());
        else if(NavigationType.REGULAR.toString().equals(navigationType))
            page.setUseRegular(NavigationType.REGULAR.toString());
        fillSeleniumObject(page);
        waitForLoad();
    }

    public static void createTemplate(Template template) throws Exception {
        Navigator.contentTabAddTemplate();
        fillSeleniumObject(template);
        getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseContentPage.EmailTemplatesTitle));
        waitForLoad();
        Navigator.refresh();
    }

    public static boolean isTemplateExist(String templateName) throws Exception {
        Navigator.contentTabEmailTemplatesManager();
        List<WebElement> templatesList = getDriver().findElements(By.xpath("//div[contains(@class, \"table_row\")]/div/a"));
        for (int i=0 ; i<templatesList.size() ; i++) {
            WebElement template = templatesList.get(i);
            if(template.getText().equals(templateName)) {
                return true;
            }
        }
        return false;
    }

    public static void editTemplate(String templateName) throws Exception{
        Navigator.contentTabEmailTemplatesManager();
        try {
            WebElement element = getDriver().findElement(By.xpath("//*[contains(text(), '" + templateName + "')]"));
            element.click();
            waitForLoad();
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail(String.format("Failed to edit template %s.", templateName));
        }
    }

    public static void downloadTemplateStatisticsFile(String templateName, File file) throws Exception {
        Files.deleteIfExists(file.toPath()); //delete old report file.

        File downloadsFolder = new File(downloadsDir);
        if(!downloadsFolder.exists())
            downloadsFolder.mkdir();

        editTemplate(templateName);

        boolean exist = false;
        long timeoutCounter = 0;
        Thread.sleep(2000);
        while (!exist && (timeoutCounter <= 150)) {
            try {
                Navigator.refresh();
                Thread.sleep(1000);
                WebElement downloadElement = getDriver().findElement(By.xpath("//*[contains(@ng-repeat, 'item in ctrlETEdit.statistics')][1]//*[contains(@ng-click, 'ctrlETEdit.downloadStatistics(item.campaignId)')]"));
                if(downloadElement.isDisplayed()){
                    log4j.info("Report element exist in page.");
                }
                downloadElement.click();
                Thread.sleep(60000);
                exist = true;

            } catch (NoSuchElementException e) {
                Thread.sleep(1000);
                timeoutCounter++;
                log4j.info("Report element not displayed yet in page. timeout counter is:  " + timeoutCounter);
            }
        }


    }

    public static boolean checkTemplateStatistics(File file, List<String> statisticsInputs) throws Exception{
        return readResults(file, statisticsInputs);
    }

    public static void fillUnsubscribePhoneNumber (String phone) {

        getDriver().findElement(By.id("phonenumber")).sendKeys(phone);
        getDriver().findElement(By.xpath("//*[@ng-click=\"unsubscribe(phonenumber,'sms');\"]")).click();


    }

}
