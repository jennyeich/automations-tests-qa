package hub.services;

import hub.base.BaseService;
import hub.common.pages.acp.ImportUsersKey;
import hub.hub1_0.services.Navigator;
import org.openqa.selenium.By;

/**
 * Created by Goni on 9/18/2017.
 */

public class DataBIService extends BaseService {


    public static void importUserKeys(StringBuffer userKeys) throws Exception {

        Navigator.importUserKeysPage();
        ImportUsersKey importUsersKey = new ImportUsersKey();
        importUsersKey.setCustomUserKeysTextarea(userKeys.toString());
        importUsersKey.setListTitle("init members list");
        fillSeleniumObject(importUsersKey);
        Navigator.waitForLoad();
    }

    public static void clickLastCreatedUsersKeysList() throws Exception{
        Navigator.importUserKeysPage();
        getDriver ().findElement(By.xpath("(//div[@ng-bind=\"item.title\"])[1]")).click();
        Navigator.waitForLoad();
    }
}

