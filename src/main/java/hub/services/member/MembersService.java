package hub.services.member;

import com.google.common.collect.Lists;
import hub.base.BaseService;
import hub.base.basePages.BaseBenefitsPage;
import hub.base.basePages.BaseDataAndBIPage;
import hub.common.objects.benefits.LotteryReward;
import hub.common.objects.common.YesNoList;
import hub.common.objects.member.*;
import hub.common.objects.member.performActionObjects.AddPointsAction;
import hub.common.objects.member.performActionObjects.SendGiftOrPunchAction;
import hub.common.objects.member.performSmartActionObjects.*;
import hub.common.objects.model.ExportItemModel;
import hub.common.objects.smarts.SmartGift;
import hub.common.objects.smarts.SmartPunchCard;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.AmountType;
import hub.hub1_0.services.Navigator;
import hub.hub2_0.common.enums.ActivityActions;
import hub.services.NewApplicationService;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import server.v2_8.Consent;
import server.v2_8.UpdateMembership;
import server.v2_8.UpdateMembershipResponse;
import server.v2_8.UpdatedFields;
import utils.AutomationException;
import utils.PropsReader;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static hub.services.NewApplicationService.timestamp;
import static java.lang.System.currentTimeMillis;

/**
 * Created by Jenny on 9/22/2016.
 */
public class MembersService extends BaseService{



    public static Logger log4j = Logger.getLogger(MembersService.class);
    public static final long TIMEOUT_FOR_HUB_UPDATE_WAIT_SEC = 120;
    public static final String APP = "App";

    public static void updateMemberDetails(MemberDetails updatedMember) throws Exception {

        findAndGetMember(updatedMember);
        updateMember(updatedMember);
    }

    private static void findAndGetMember (MemberDetails updatedMember) throws Exception {

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(updatedMember.getPhoneNumber());
        findAndGetMember(findMember);
    }

    public static void findAndGetMember (String  memberPhone) throws Exception {

        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(memberPhone);
        findAndGetMember(findMember);
    }

    public static void deleteMember(MemberDetails updatedMember) throws Exception {

        findAndGetMember(updatedMember);
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        fillSeleniumObject(updatedMember);
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();

    }


    public static void updateMember(MemberDetails updatedMember) throws InterruptedException, AutomationException {
        Thread.sleep(1000);
        updatedMember.setUpdate(true);
        fillSeleniumObject(updatedMember);
        waitForLoad();
    }

    //try to get member after find action if member not exist return null
    public static MemberDetails tryFindAndGetMemberByPhoneNumber(NewMember member) throws InterruptedException, AutomationException {
        try {
            MemberDetails memberDetails = new MemberDetails();
            findMemberByPhoneNumber(member);
            readSeleniumObject(memberDetails);
            waitForLoad();
            return memberDetails;
        } catch (Exception ex) {
            return null;
        }
    }
    //try to get member after find action if member not exist return null
    public static MemberDetails tryFindAndGetMember(FindMember member,String name) {
        try {
            return findAndGetMember(member,name);
        }
        catch (Exception ex){
            return null;
        }
    }
    public static MemberDetails findAndGetMember(FindMember member) throws Exception {
        MemberDetails memberDetails = new MemberDetails();
        findMember(member);
        List<WebElement> elements = findElementsByAutomationId("result");
        if(elements.isEmpty())
            throw new Exception("No member was found with phone: " + member.getPhoneNumber());
        elements.get(0).click();
        waitForLoad();
        readSeleniumObject(memberDetails);
        waitForLoad();
        return memberDetails;
    }



    public static MemberDetails clickOnFoundMember() throws Exception{
        List<WebElement> elements = getDriver().findElements(By.xpath("//*[contains(@class,\"filter results\")]/a"));
        if(elements.isEmpty())
            throw new Exception("No member was found");
        elements.get(0).click();
        waitForLoad();
        MemberDetails memberDetails = new MemberDetails();
        readSeleniumObject(memberDetails);
        waitForLoad();
        return memberDetails;
    }

    public static MemberDetails getDeletedMemberDetails () throws Exception {

        MemberDetails memberDetails = new MemberDetails();
        waitForLoad();
        readSeleniumObject(memberDetails);
        waitForLoad();
        return memberDetails;

    }
    public static MemberDetails findAndGetMember(FindMember member,String name) throws Exception {
        MemberDetails memberDetails = new MemberDetails();
        findMember(member);
        if (name!=null){
            memberDetails = findMemberByNameInMemberResult(name);
        } else {
            findElementsByAutomationId("result").get(0).click();
        }
        waitForLoad();
        return memberDetails;
    }

    private static MemberDetails findMemberByNameInMemberResult(String name) throws Exception {
         waitForLoad();
         FindMember findMember = new FindMember();
         findMember.setFirstName(name);
        findMember(findMember);
        MemberDetails memberDetails = new MemberDetails();
        List<WebElement> results = findElementsByAutomationId("result");
        for (WebElement res:results) {
            //search in multiple results
            if (findElementByAutomationId(res,"resultDetails").getText().contains(name)){
                res.click();
                waitForLoad();
                readSeleniumObject(memberDetails);
                waitForLoad();
                return memberDetails;
            }
        }
        return null;
    }

    public static void findMember(FindMember member) throws InterruptedException, AutomationException {
        Navigator.dataAndBIPageFindMember();
        fillSeleniumObject(member);
        waitForLoad();
        waitForExportButtonToBeClickable();
        waitForLoad();
    }


    public static void clickMember () throws Exception {


        List<WebElement> elements = getDriver().findElements(By.xpath("//*[@automationid=\"result\"]"));
        if(elements.isEmpty())
            throw new Exception("No member was found");
        elements.get(0).click();
        waitForLoad();
    }

    private static void waitForExportButtonToBeClickable() {
        String xpath = "//*[@automationid=\'exportButton\']";
        BaseService.getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
        BaseService.getDriverWait().until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)));
        waitForLoad();
    }

    public static List<ExportItemModel> exportFindMembers() throws Exception {
        FindMember findMember = new FindMember();
        Navigator.dataAndBIPageFindMember();
        fillSeleniumObject(findMember);
        waitForExportButtonToBeClickable();
        findMember.export();
        waitForLoad();
        readSeleniumObject(findMember);
        return findMember.getExports();
    }


    public static List<ExportItemModel> exportFilterMemberBySql(FilterMember filter) throws Exception {
        Navigator.dataAndBIPageFilterMember();
        fillSeleniumObject(filter);
        filter.export();
        waitForLoad();
        Thread.sleep(3000);
        readSeleniumObject(filter);
        return filter.getExports();
    }
    public static String waitForExportDone(WebElement wrapDiv) throws InterruptedException {
        String status;

        status = BaseService.findElementByAutomationId(wrapDiv, "status").getText();
        int index = 0;
        // wait until status is Download
        while ((!status.contentEquals("Download")) && index < 20) {
            Thread.sleep(50);
            try {
                status = BaseService.findElementByAutomationId(wrapDiv, "status").getText();
            } catch (Exception ex) {

            }
            index++;
        }
        return status;
    }
//    public static void filterMemberBySql(FilterMember filter) throws InterruptedException, IntrospectionException, IllegalAccessException, AWTException {
//
//        Navigator.dataAndBIPageFilterMember();
//        fillSeleniumObject(filter);
//        Thread.sleep(1000);
//        FilterBySQL sql = new FilterBySQL();
//        //sql.setFilterBySqlButton(sql.getFilterBySqlButton());
//        if (filter.getNumberOfActionsFrom()!= null){
//            Navigator.refresh();
//        }
//        fillSeleniumObject(sql);
//        waitForExportButtonToBeClickable();
//        waitForLoad();
//
//    }

    public static void filterMemberByBigQuery(FilterMember filter) throws InterruptedException, AutomationException{

        Navigator.dataAndBIPageFilterMember();
        Navigator.refresh();
        fillSeleniumObject(filter);
        Thread.sleep(1000);
        FilterByBigQuery bigQuery = new FilterByBigQuery();
        getDriver().findElement(By.id(FilterByBigQuery.FILTER_BY_BIG_QUERY_BUTTON)).click();
        waitForExportButtonToBeClickable();
        waitForLoad();
    }

    public static FindMember findMemberByPhoneNumber(FindMember member) throws Exception {
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        findAndGetMember(findMember,null);

        return findMember;
    }

    public static FindMember findMemberByPhoneNumber(NewMember member) throws Exception {
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        findAndGetMember(findMember,null);
        return findMember;
    }

    public static FindMember findMembersByFirstName(String firstName) throws InterruptedException, AutomationException {
        FindMember findMember = new FindMember();
        findMember.setFirstName(firstName);
        findMember(findMember);
        return findMember;
    }
    public static FindMember findMembers() throws InterruptedException, AutomationException {
        FindMember findMember = new FindMember();
        findMember(findMember);
        return findMember;
    }
    public static MemberDetails deleteMember(NewMember member) throws Exception {
        FindMember findMember = new FindMember();
        findMember.setPhoneNumber(member.getPhoneNumber());
        MemberDetails memberDetails = findAndGetMember(findMember,member.getFirstName());
        MemberDetails deleteMemberDetails = new MemberDetails();
        deleteMemberDetails.setDelete(true);
        fillSeleniumObject(deleteMemberDetails);
        waitForLoad();
        return memberDetails;
    }


    @Deprecated
    public static boolean performAnActionSendGift(SmartGift smartGift) throws Exception {

        return performAnActionSendAsset(smartGift.getTitle());
    }
    @Deprecated
    public static boolean performAnActionSendAsset(String assetTitle) throws Exception {

        PerformAnActionOnMember performAnActionaction = new PerformAnActionOnMember();
        performAnActionaction.setPerformAction(PerformActionOnResults.SendAGiftOrPunch.toString());
        SendGiftOrPunchAction sendGift = new SendGiftOrPunchAction();
        sendGift.setSearchBox(assetTitle);
        sendGift.setPushNotificationYesNo(YesNoList.NO.toString());
        performAnActionaction.setSendGiftOrPunchAction(sendGift);
        MembersService.performActionOnMember(performAnActionaction);
        return MembersService.isGiftExists(assetTitle);
    }

    @Deprecated
    public static void performAnActionAddPoints(String numOfPoints) throws Exception {

        log4j.info("perform action add points");
        PerformAnActionOnMember performAnActionaction = new PerformAnActionOnMember();
        performAnActionaction.setPerformAction(PerformActionOnResults.AddPoints.toString());
        AddPointsAction addPoints = new AddPointsAction();
        addPoints.setPointsAmount(numOfPoints);
        performAnActionaction.setAddPointsAction(addPoints);
        MembersService.performActionOnMember(performAnActionaction);

    }


    public static void performAnActionReedem() throws Exception{


       WebElement element = getDriver().findElement(By.xpath("//div[@automationid=\"userAsset\"]//button[contains(text(),\"Redeem\")]"));
       element.click();
       Thread.sleep(TIMEOUT_FOR_HUB_UPDATE_WAIT_SEC);
       checkForAlerts();
       Navigator.refresh();
    }

    public static void deleteExistingMember () throws Exception {

        WebElement element = getDriver().findElement(By.xpath("//*[@automationid=\"deleteMember\"]"));
        element.click();
        Thread.sleep(TIMEOUT_FOR_HUB_UPDATE_WAIT_SEC);
        checkForAlerts();
        Thread.sleep(1000);
        refreshHubPage();
        getDriver().navigate().back();

    }


    public static void performActionOnMember(PerformAnActionOnMember action) throws InterruptedException, AutomationException {

        Navigator.refresh();
        waitForLoad();
        getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@automationid=\"dropdownPerformAction\"]")));
        fillSeleniumObject(action);
        waitForLoad();
        BaseService.dismissNoticeMsg();
    }

    /**
     * Smart Actions
     **/

    public static String performSmartActionAddPoints(String numOfPoints, CharSequence timeStamp) throws Exception{

        log4j.info("perform smart action add points");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.AddPointsOrCredit.toString());
        AddPointOrCreditAction addPointOrCreditAction = new AddPointOrCreditAction();
        addPointOrCreditAction.setNumberToAdd(numOfPoints);
        addPointOrCreditAction.setTypePoints(AmountType.Points.toString());
        addPointOrCreditAction.setPerformActionButton("Y");
        String actionTag = "AddPoints" +timeStamp;
        ArrayList<String> tags = Lists.newArrayList(actionTag);
        addPointOrCreditAction.setAddTags(tags);
        log4j.info("Action tag: " +actionTag);
        performSmartAction.setAddPointOrCreditAction(addPointOrCreditAction);
        MembersService.performSmartActionOnMember(performSmartAction);

        return actionTag;
    }

    public static String performSmartActionSendAnAsset(String smartGift, CharSequence timeStamp) throws Exception {

        log4j.info("perform smart action send asset");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.SendAnAsset.toString());
        SendAnAssetAction sendAnAssetAction = new SendAnAssetAction();
        sendAnAssetAction.setAssetsListDropdown(smartGift);
        sendAnAssetAction.setPerformActionButton("Y");
        String actionTag = "SendAsset" +timeStamp;
        ArrayList<String> tags = Lists.newArrayList(actionTag);
        sendAnAssetAction.setAddTags(tags);
        log4j.info("Action tag: " +actionTag);
        performSmartAction.setSendAnAssetAction(sendAnAssetAction);
        MembersService.performSmartActionOnMember(performSmartAction);

        return actionTag;
    }

    public static String performSmartActionSendAnAsset(SmartGift smartGift, CharSequence timeStamp) throws Exception {

        log4j.info("perform smart action send asset");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.SendAnAsset.toString());
        SendAnAssetAction sendAnAssetAction = new SendAnAssetAction();
        sendAnAssetAction.setAssetsListDropdown(smartGift.getTitle());
        sendAnAssetAction.setPerformActionButton("Y");
        String actionTag = "SendAsset" +timeStamp;
        ArrayList<String> tags = Lists.newArrayList(actionTag);
        sendAnAssetAction.setAddTags(tags);
        log4j.info("Action tag: " +actionTag);
        performSmartAction.setSendAnAssetAction(sendAnAssetAction);
        MembersService.performSmartActionOnMember(performSmartAction);

        return actionTag;
    }

    public static void openSmartActionSendAssetDropDown() throws Exception{

        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.SendAnAsset.toString());
        SendAnAssetAction sendAnAssetAction = new SendAnAssetAction();
        sendAnAssetAction.setAssetsListDropdownClick("Y");
        performSmartAction.setSendAnAssetAction(sendAnAssetAction);
        fillSeleniumObject(performSmartAction);

    }
    public static boolean performSmartActionSendAAsset(String assetTitle)throws Exception{

        log4j.info("perform smart action send asset");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.SendAnAsset.toString());
        SendAnAssetAction sendAnAssetAction = new SendAnAssetAction();
        sendAnAssetAction.setAssetsListDropdown(assetTitle);
        sendAnAssetAction.setPerformActionButton("Y");
        String actionTag = assetTitle;
        ArrayList<String> tags = Lists.newArrayList(actionTag);
        sendAnAssetAction.setAddTags(tags);
        log4j.info("Action tag: " +actionTag);
        performSmartAction.setSendAnAssetAction(sendAnAssetAction);
        MembersService.performSmartActionOnMember(performSmartAction);
        return MembersService.isGiftExists(assetTitle);
    }

    public static String performSmartActionSendAPunchCard(SmartPunchCard punchCard, CharSequence timeStamp)throws Exception{

        return performSmartActionSendAPunchCard(punchCard.getTitle(), timeStamp);

    }

    public static String performSmartActionSendAPunchCard(String punchCardTitle, CharSequence timeStamp)throws Exception{

        log4j.info("perform smart action send punch card");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.SendAnAsset.toString());
        SendAnAssetAction sendAnAssetAction = new SendAnAssetAction();
        sendAnAssetAction.setAssetsListDropdown(punchCardTitle);
        sendAnAssetAction.setPerformActionButton("Y");
        String actionTag = "SendPunchCard" +timeStamp;
        ArrayList<String> tags = Lists.newArrayList(actionTag);
        sendAnAssetAction.setAddTags(tags);
        log4j.info("Action tag: " +actionTag);
        performSmartAction.setSendAnAssetAction(sendAnAssetAction);
        MembersService.performSmartActionOnMember(performSmartAction);

        return actionTag;

    }

    public static String performSmartActionAddCredit(String numOfCredit,CharSequence timeStamp) throws Exception {

        log4j.info("perform smart action add credit");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.AddPointsOrCredit.toString());
        AddPointOrCreditAction addPointOrCreditAction = new AddPointOrCreditAction();
        addPointOrCreditAction.setNumberToAdd(numOfCredit);
        addPointOrCreditAction.setTypeCredit(AmountType.Credit.toString());
        addPointOrCreditAction.setPerformActionButton("Y");
        String actionTag = "AddCredit" +timeStamp;
        ArrayList<String> tags = Lists.newArrayList(actionTag);
        addPointOrCreditAction.setAddTags(tags);
        log4j.info("Action tag: " +actionTag);
        performSmartAction.setAddPointOrCreditAction(addPointOrCreditAction);
        MembersService.performSmartActionOnMember(performSmartAction);

        return actionTag;
    }

    public static void performTagMemberSmartAction(String tag,String actionTag1) throws Exception{
        log4j.info("perform smart action tag member");
        performTagUntagMemberSmartAction(tag, actionTag1, ActivityActions.Tag.toString());

    }

    public static void performUntagMemberSmartAction(String tag,String actionTag1) throws Exception{
        log4j.info("perform smart action Untag member");
        performTagUntagMemberSmartAction(tag, actionTag1, ActivityActions.UnTag.toString());
    }

    private static void performTagUntagMemberSmartAction(String tag,String actionTag1, String action) throws Exception {
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.TagUntag.toString());
        TagOrUnTagAction tagOrUnTagAction = new TagOrUnTagAction();
        tagOrUnTagAction.setTagUntagText(tag);
        if(ActivityActions.Tag.toString().equals(action)) {
            tagOrUnTagAction.setActionTag("Y");
        } else if(ActivityActions.UnTag.toString().equals(action)){
            tagOrUnTagAction.setActionUntag("Y");
        }
        tagOrUnTagAction.setPerformActionButton("Y");
        String actionTag = actionTag1;
        ArrayList<String> tags = Lists.newArrayList(actionTag);
        tagOrUnTagAction.setAddTags(tags);
        log4j.info("Action tag: " +actionTag);
        performSmartAction.setTagOrUnTagAction(tagOrUnTagAction);
        MembersService.performSmartActionOnMember(performSmartAction);
    }

    public static String [] performSmartActionTagMember(CharSequence timeStamp) throws Exception{

        String tag = "TagFromAutomation" +timestamp;
        String actionTag = "TagMember" +timeStamp;
        performTagMemberSmartAction(tag,actionTag);


        String [] arr = new String [2];
        arr[0] = tag;
        arr[1] = actionTag;


        return arr;
    }

    public static String performSmartActionUpdateExpirationDate(CharSequence timeStamp) throws Exception{

        log4j.info("perform smart action update expiration date");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.UpdateExpirationDate.toString());
        UpdateExpirationDateAction updateExpirationDateAction = new UpdateExpirationDateAction();
        updateExpirationDateAction.setUpdateByInput("2");
        updateExpirationDateAction.setPerformActionButton("Y");
        String actionTag = "UpdateExpirationDate" +timeStamp;
        ArrayList<String> tags = Lists.newArrayList(actionTag);
        updateExpirationDateAction.setAddTags(tags);
        log4j.info("Action tag: " +actionTag);
        performSmartAction.setUpdateExpirationDateAction(updateExpirationDateAction);
        MembersService.performSmartActionOnMember(performSmartAction);

        return actionTag;
    }

    public static String performSmartActionSendLotteryReward(LotteryReward lotteryReward, CharSequence timeStamp) throws Exception{

        log4j.info("perform smart action send lottery reward");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.SendALotteryRewared.toString());
        SendLotteryRewardAction sendLotteryRewardAction = new SendLotteryRewardAction();
        sendLotteryRewardAction.setAssetsListDropdown(lotteryReward.getTitle());
        sendLotteryRewardAction.setPerformActionButton("Y");
        String actionTag = "SendLotteryReward" +timeStamp;
        ArrayList<String> tags = Lists.newArrayList(actionTag);
        sendLotteryRewardAction.setAddTags(tags);
        log4j.info("Action tag: " +actionTag);
        performSmartAction.setSendLotteryRewardAction(sendLotteryRewardAction);
        MembersService.performSmartActionOnMember(performSmartAction);

        return actionTag;
    }


    public static String performSmartActionPunchAPunchCard(SmartPunchCard punchCard, CharSequence timeStamp) throws Exception{

        return performSmartActionPunchAPunchCard(punchCard.getTitle(),timeStamp);
    }

    public static String performSmartActionPunchAPunchCard(String punchCardTitle, int numOfPunches) throws Exception{

        log4j.info("perform smart action punch a punch card");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.PunchAPunchCard.toString());
        PunchAPunchCardAction punchAPunchCardAction = new PunchAPunchCardAction();
        punchAPunchCardAction.setNumberOfPunches(String.valueOf(numOfPunches));
        punchAPunchCardAction.setChoosePunchCard(punchCardTitle);
        punchAPunchCardAction.setPerformActionButton("Y");
        String actionTag = punchCardTitle;
        ArrayList<String> tags = Lists.newArrayList(actionTag);
        punchAPunchCardAction.setAddTags(tags);
        log4j.info("Action tag: " +actionTag);
        performSmartAction.setPunchAPunchCardAction(punchAPunchCardAction);
        MembersService.performSmartActionOnMember(performSmartAction);


        return actionTag;

    }

    public static String performSmartActionPunchAPunchCard(String punchCardTitle, CharSequence timeStamp) throws Exception{

        log4j.info("perform smart action punch a punch card");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.PunchAPunchCard.toString());
        PunchAPunchCardAction punchAPunchCardAction = new PunchAPunchCardAction();
        punchAPunchCardAction.setNumberOfPunches("1");
        punchAPunchCardAction.setChoosePunchCard(punchCardTitle);
        punchAPunchCardAction.setPerformActionButton("Y");
        String actionTag = "PunchAPunchCard" +timeStamp;
        ArrayList<String> tags = Lists.newArrayList(actionTag);
        punchAPunchCardAction.setAddTags(tags);
        log4j.info("Action tag: " +actionTag);
        performSmartAction.setPunchAPunchCardAction(punchAPunchCardAction);
        MembersService.performSmartActionOnMember(performSmartAction);


        return actionTag;
    }


    public static void performSmartActionSendEmail(String templateName) throws Exception {
        log4j.info("perform smart action send email");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.SendEmail.toString());
        SendEmailAction sendEmailAction = new SendEmailAction();
        sendEmailAction.setTemplateName(templateName);
        performSmartAction.setSendEmailAction(sendEmailAction);
        MembersService.performSmartActionOnMember(performSmartAction);

    }


    public static String performSmartActionSendSMS(String smsMessage,CharSequence timeStamp) throws Exception {


        log4j.info("perform smart action send sms");
        PerformSmartActionOnMember performSmartAction = new PerformSmartActionOnMember();
        performSmartAction.setPerformAction(PerformSmartActionOnResults.SendSMS.toString());
        SendSMSAction sendSMSAction = new SendSMSAction();
        sendSMSAction.setTextMessage(smsMessage);
        String actionTag = "SendSMS" + currentTimeMillis();
        ArrayList<String> tags = Lists.newArrayList(actionTag);
        sendSMSAction.setTags(tags);
        log4j.info("Action tag: " +actionTag);
        performSmartAction.setSendSMSAction(sendSMSAction);
        MembersService.performSmartActionOnMember(performSmartAction);

        return actionTag;

    }


    public static void performSmartActionOnMember(PerformSmartActionOnMember action) throws InterruptedException, AutomationException {
        waitForLoad();
        getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//select[@id=\"action_name\"]")));
        fillSeleniumObject(action);
        waitForLoad();
        BaseService.dismissNoticeMsg();
        Navigator.refresh();
    }

    public static boolean isGiftExists(String giftName) throws InterruptedException {

        waitForLoad();
        boolean exist = false;
        long timeoutCounter = 0;
        while (!exist && (timeoutCounter <= TIMEOUT_FOR_HUB_UPDATE_WAIT_SEC)) {
            try {
                refreshMemberDetails();
                Thread.sleep(2000);
                String gifts = getDriver().findElement(BaseDataAndBIPage.Member_gifts).getText();
                exist = gifts.contains(giftName);
                if (!exist)
                    timeoutCounter++;

            } catch (NoSuchElementException e) {
                Thread.sleep(500);
                timeoutCounter++;
            }
        }

        return exist;

    }

    public static boolean isAllAssetsExist(String assetName,int occurences) throws InterruptedException {

        waitForLoad();
        boolean exist = false;
        long timeoutCounter = 0;
        while (!exist && (timeoutCounter <= TIMEOUT_FOR_HUB_UPDATE_WAIT_SEC)) {
            try {
                refreshMemberDetails();
                Thread.sleep(1000);
                List<WebElement> assets = getDriver().findElements(BaseDataAndBIPage.Member_gifts);

                if(assets.size() == occurences){
                    int count = 0;
                    for(WebElement elem:assets){
                        if(elem.getText().equals(assetName))
                            count++;
                    }
                    exist = (count == occurences);
                }

                if (!exist)
                    timeoutCounter++;

            } catch (NoSuchElementException e) {
                Thread.sleep(500);
                timeoutCounter++;
            }
        }

        return exist;

    }

    public static Object executeJavascript(String script, WebElement webElement) throws InterruptedException {

        JavascriptExecutor je = (JavascriptExecutor) getDriver();
        Object retVal = je.executeScript(script, webElement);
        TimeUnit.SECONDS.sleep(2);
        return retVal;
    }

    //returns true is tag exists
    public static boolean isTagExists(String tag) throws InterruptedException {

        waitForLoad();
        boolean exist = false;
        long timeoutCounter = 0;
        Thread.sleep(2000);
        while (!exist && (timeoutCounter <= TIMEOUT_FOR_HUB_UPDATE_WAIT_SEC)) {
            try {
                refreshMemberDetails();
                Thread.sleep(2000);
                String tagXpath = MessageFormat.format(BaseDataAndBIPage.Member_tags_xpath,tag);
                String tags = getDriver().findElement(By.xpath(tagXpath)).getText();
                exist = tags.contains(tag);
                if (!exist)
                    timeoutCounter++;

            } catch (NoSuchElementException e) {
                Thread.sleep(2000);
                timeoutCounter++;
            }
        }
        return exist;
    }

    public static boolean checkMemberStatus(String expectedStatus) throws InterruptedException{

        waitForLoad();
        boolean exist = false;
        long timeoutCounter = 0;
        Thread.sleep(2000);
        while (!exist && (timeoutCounter <= TIMEOUT_FOR_HUB_UPDATE_WAIT_SEC)) {
            try {
                refreshMemberDetails();
                Thread.sleep(1000);
                String status = getDriver().findElement(BaseDataAndBIPage.Member_status).getAttribute("value");
                exist = status.equals(expectedStatus);
                if (!exist)
                    timeoutCounter++;

            } catch (NoSuchElementException e) {
                Thread.sleep(1000);
                timeoutCounter++;
            }
        }

        if (!exist) {
            log4j.info("Did not find status " + expectedStatus +". counter: " + timeoutCounter +
                    " MembershipKey: " + getMembershipKey());
        }
        return exist;


    }


    public static String numberOfMembers() {
        waitForLoad();
        return getDriver().findElement(FilterMember.NUMBER_OF_MEMBERS).getText();

    }

    public static String getLastLog() {
        waitForLoad();
        return getDriver().findElement(By.xpath(MembersLog.LAST_LOG)).getText();

    }


    //returns true if the correct amount of points were added to credit
    public static boolean isPointsAddedToCredit(int oldBudget) throws InterruptedException {

        return isPointsAddedToCredit(oldBudget,10);
    }
    //returns true if the correct amount of points were added to credit
    public static boolean isPointsAddedToCredit(int oldCredit,int newCredit) throws InterruptedException {

        waitForLoad();
        boolean budgetUpdated = false;
        long timeoutCounter = 0;
        Thread.sleep(2000);
        while (!budgetUpdated && (timeoutCounter <= TIMEOUT_FOR_HUB_UPDATE_WAIT_SEC)) {
            try {
                refreshMemberDetails();
                Thread.sleep(2000);
                String budgetText = getDriver().findElement(BaseDataAndBIPage.Member_credit).getText();
                String budget = budgetText.replace(",", "").replace(".00", "");

                try {
                    int newBudget = Integer.valueOf(budget);
                    if ((newBudget - oldCredit) == newCredit)
                        budgetUpdated = true;
                    if (!budgetUpdated)
                        timeoutCounter++;
                }catch (NumberFormatException e) {
                    timeoutCounter++;
                    continue;
                }
            }catch (NoSuchElementException e) {
                Thread.sleep(500);
                timeoutCounter++;
            }
        }

        return budgetUpdated;
    }


    //returns true if the correct amount of points were added to points
    public static boolean isPointsAdded(String oldPoints, String givenPoints) throws InterruptedException {

        waitForLoad();
        boolean pointsUpdated = false;
        long timeoutCounter = 0;
        Thread.sleep(2000);
        while (!pointsUpdated && (timeoutCounter <= TIMEOUT_FOR_HUB_UPDATE_WAIT_SEC)) {
            try {
                refreshMemberDetails();
                Thread.sleep(2000);
                String pointsText = getDriver().findElement(BaseDataAndBIPage.Member_points).getText();
                String points = pointsText.replace(",", "").replace(".00", "");

                try {
                    int newPoints = Integer.valueOf(points);

                    if (newPoints - Integer.valueOf(oldPoints) == Integer.valueOf(givenPoints))
                        pointsUpdated = true;
                    if (!pointsUpdated)
                        timeoutCounter++;
                }catch (NumberFormatException e){
                    timeoutCounter++;
                    continue;
                }

            } catch (NoSuchElementException e) {
                Thread.sleep(500);
                timeoutCounter++;
            }
        }

        return pointsUpdated;
    }

    private static void refreshMemberDetails() throws InterruptedException {
        WebElement element = getDriver().findElement(BaseDataAndBIPage.REFRESH); // refresh member details
        executeJavascript("arguments[0].click();", element);
    }

    public static void refreshHubPage() throws InterruptedException{
        Navigator.refresh();
    }

    public static void createNewMember(NewMember member) throws InterruptedException, AutomationException {

        String membershipKey = createMemberDefaultConsent(member);

        if(Boolean.valueOf(PropsReader.getPropValuesForEnv(Consent.REGISTER_MEMBERS_BY_CONSENT)).booleanValue()) {
            UpdateMembershipResponse response = updateMembershipConsent(membershipKey, APP);
            Assert.assertEquals(String.format("Consent expected to be 'yes' but actually was %s", response.getMembership().getConsent()), "yes", response.getMembership().getConsent());
        }
        Navigator.refresh();
        waitForLoad();
    }

    public static String createMemberDefaultConsent(NewMember member) throws AutomationException, InterruptedException {
        Navigator.operationsTabNewMember();
        fillSeleniumObject(member);
        waitForLoad();
        Navigator.refresh();
        waitForLoad();
        return getMembershipKey();
    }


    //returns User's Key
    public static String getUserKey() {
        waitForLoad();
        WebElement userKey = getDriver().findElement(BaseDataAndBIPage.Find_MemberBy_User_Key);
        return userKey.getAttribute("value");
    }

    public static String getMembershipKey() {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(BaseDataAndBIPage.Find_MemberBy_Membership_Key);
        return memberKey.getAttribute("value");
    }

    public static String getMemberFirstName() {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"FirstName\"]"));
        return memberKey.getAttribute("value");
    }

    public static String getMemberLastName() {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"LastName\"]"));
        return memberKey.getAttribute("value");
    }

    public static String getMemberPhoneNumber() {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"PhoneNumber\"]"));
        return memberKey.getAttribute("value");
    }


    public static String getMemberOfficialIDNumber() {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"GovID\"]"));
        return memberKey.getAttribute("value");
    }


    public static String getMemberExternalClubMemberID() {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"ExtClubMemberID\"]"));
        return memberKey.getAttribute("value");
    }


    public static String getMemberAddressFloor() {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"AddressFloor\"]"));
        return memberKey.getAttribute("value");
    }

    public static String getMemberAddressHome() {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"AddressHome\"]"));
        return memberKey.getAttribute("value");
    }


    public static String getMemberEmail() {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"Email\"]"));
        return memberKey.getAttribute("value");
    }

    public static String getMemberAddressStreet() {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"AddressStreet\"]"));
        return memberKey.getAttribute("value");
    }

    public static String getMemberAddressLine1 () {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"AddressLine1\"]"));
        return memberKey.getAttribute("value");
    }

    public static String getMemberAddressLine2 () {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"AddressLine2\"]"));
        return memberKey.getAttribute("value");
    }

    public static String getMemberGenericString1 () {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"GenericString1\"]"));
        return memberKey.getAttribute("value");
    }


    public static String getMemberAllowSMS () {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"AllowSMS\"]"));
        return memberKey.getAttribute("value");
    }
    public static String getMemberAllowEmail () {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"AllowEmail\"]"));
        return memberKey.getAttribute("value");
    }

    public static String getMemberFriendReferralCode () {
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@ng-value=\"userinfo.ReferralCode|translate\"]"));
        return memberKey.getAttribute("value");
    }


    public static String getMemberUnsubscribeSMS () {
        String s;
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"unsubscribeSms\"]"));

        try {s = memberKey.getAttribute("value");}
        catch (Exception e) {s = "";}
        return s;
    }

      public static String getMemberUnsubscribeEmail () {
        String s;
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"unsubscribeEmail\"]"));

        try {s = memberKey.getAttribute("value");}
        catch (Exception e) {s = "";}
        return s;
    }

    public static String getMemberDisconnectUser () {
        String s;
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"disconnectUser\"]"));

        try {s = memberKey.getAttribute("value");}
        catch (Exception e) {s = "";}
        return s;
    }

    public static String getMemberDeleteUser () {
        String s;
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"deleteMember\"]"));

        try {
            s = memberKey.getAttribute("value");
        } catch (Exception e) {
            s = "";
        }
        return s;
    }

    public static String getMemberUpdateButton () {
        String s;
        waitForLoad();
        WebElement memberKey = getDriver().findElement(By.xpath("//*[@automationid=\"updateMember\"]"));

        try {s = memberKey.getAttribute("value");}
        catch (Exception e) {s = "";}
        return s;
    }









    public static NewMember getMemberFields () {

        NewMember newMember = new NewMember() ;
        newMember.setFirstName(getMemberFirstName());
        newMember.setLastname(getMemberLastName());
        newMember.setPhoneNumber(getMemberPhoneNumber());
        newMember.setGovId(getMemberOfficialIDNumber());
        newMember.setExtClubID(getMemberExternalClubMemberID());
        newMember.setAddressFloor(getMemberAddressFloor());
        newMember.setAddressHome(getMemberAddressHome());
        newMember.setEmail(getMemberEmail());
        newMember.setAddressStreet(getMemberAddressStreet());
        newMember.setAddressLine1(getMemberAddressLine1());
        newMember.setAddressLine2(getMemberAddressLine2());
        newMember.setGenericString1(getMemberGenericString1());
        return newMember;

    }


    public static void deactivateSmartPunchCard(String assetName) throws InterruptedException, AutomationException {
        Navigator.benefitsTabPunchCards();
        getDriver().findElement(BaseBenefitsPage.SideMenu_SmartPunchCards).click();
        TimeUnit.SECONDS.sleep(1);
        deactivateAsset(assetName,3);
    }


    public static void deactivateGift(String assetName) throws InterruptedException, AutomationException {
        Navigator.benefitsTabGifts();
        getDriver().findElement(BaseBenefitsPage.Smart_Gifts).click();
        TimeUnit.SECONDS.sleep(1);
        deactivateAsset(assetName,3);
    }


    public static void deactivateOldClubDeal(String name) throws Exception{
        Navigator.benefitsTabGifts();
        getDriver().findElement(BaseBenefitsPage.ClubDealsPage).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        getDriver().findElement(BaseBenefitsPage.OldClubDealPage).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        deactivateClubDeal(name);
    }

    private static void deactivateClubDeal(String assetName) {

        waitForLoad();
        List<WebElement> assets = getDriver().findElements(By.xpath("//*[starts-with(@class,'item-list')]/div"));
        int size = assets.size() - 1;
        //i starts from size of assets array and search backward until reaching 4 ( because of the structure of the asset list)
        for (int i = size; i >= 3; i--) {
            String path = "//*[starts-with(@class,'item-list')]/div[" + i + "]//div//*[contains(text(),\""+ assetName + "\")]";
            try {
                getDriver().findElement(By.xpath(path));
                getDriver().findElement(By.xpath("//*[@id=\"gift-" + (i - 3) + "\"]/button[@id=\"display-deactivate-btn\"]")).click();
                Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
                break;
            } catch (Exception e) {
                continue;//continu to next asset
            }
        }
        waitForLoad();
    }

    public static void deactivateSmartClubDeals() throws InterruptedException, AutomationException {
        Navigator.benefitsTab();
        waitForLoad();
        getDriver().findElement(BaseBenefitsPage.ClubDealsPage).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
        getDriver().findElement(BaseBenefitsPage.SmartDealPage).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();

        TimeUnit.SECONDS.sleep(1);
        deactivateAllClubDeals();
    }

    private static void deactivateAsset(String assetName,int indexOfFirstAssetInList) throws InterruptedException, AutomationException {

        waitForLoad();
        List<WebElement> assets = getDriver().findElements(By.xpath("//*[starts-with(@class,'item-list')]/div"));
        int size = assets.size() - 1;
        //i starts from size of assets array and search backward until reaching 4 ( because of the structure of the asset list)
        for (int i = size; i >= indexOfFirstAssetInList; i--) {
            String path = "//*[starts-with(@class,'item-list')]/div[" + i + "]/div//*[contains(text(),'" + assetName + "')]";
            try {
                //if element with this name exist in this index,then take the index and find its deactive button
                getDriver().findElement(By.xpath(path));
                getDriver().findElement(By.xpath("//*[@id=\"gift-" + (i - indexOfFirstAssetInList) + "\"]/button[@id=\"display-deactivate-btn\"]")).click();
                TimeUnit.SECONDS.sleep(1);
                break;
            } catch (Exception e) {
                continue;//continue to next asset
            }
        }
        waitForLoad();
    }

    private static void deactivateAllClubDeals() throws InterruptedException, AutomationException{

        waitForLoad();
        List<WebElement> assets = getDriver().findElements(By.xpath("//*[starts-with(@class,'item-list')]/div"));
        int size = assets.size() - 2;
        //i starts from size of assets array and search backward until reaching 4 ( because of the structure of the asset list)
        for (int i = 0; i < size; i++) {
            ///  String path = "//*[starts-with(@class,'item-list')]/div[" + i + "]/div//*[contains(text(),'" + assetName + "')]";
            try {
                //if element with this name exist in this index,then take the index and find its deactive button
                ///  findElementByXPath(path);
                getDriver().findElement(By.xpath("//*[@id=\"gift-" + (i) + "\"]/button[@id=\"display-deactivate-btn\"]")).click();
                TimeUnit.SECONDS.sleep(1);
                /// break;
            } catch (Exception e) {
                ///continue;//continu to next asset
            }
        }
        waitForLoad();
    }


    public static boolean isPunchCardPunched(String punchName) throws Exception {

        return isPunchCardPunched(punchName, "1");
    }

    public static boolean isPunchCardPunched(String punchName, String num) throws Exception {

        waitForLoad();
        boolean exist = false;
        long timeoutCounter = 0;
        Thread.sleep(2000);
        while (!exist && (timeoutCounter <= TIMEOUT_FOR_HUB_UPDATE_WAIT_SEC)) {
            try {
                refreshMemberDetails();
                Thread.sleep(1000);
                String punchs = getDriver().findElement(BaseDataAndBIPage.Member_gifts).getText();
                if (punchs.contains(punchName)) {
                    String numOfPunches = getDriver().findElement(BaseDataAndBIPage.Member_last_recieved_punchcard_punches_description).getText();
                    exist = numOfPunches.contains("punched " + num + " out of 5");
                    if (!exist)
                        timeoutCounter++;
                } else {
                    timeoutCounter++;
                }

            } catch (NoSuchElementException e) {
                Thread.sleep(1000);
                timeoutCounter++;
            }
        }

        return exist;
    }


    public static void setLogger(Logger logger) {
        log4j = logger;
    }

    public static int getNumberOfMembersOnFindMember() {
        String text = BaseService.findElementByAutomationId("numOfMembers").getText();
        return Integer.parseInt(text);
    }

    public static int getFindMemberExportCount() {
        List<WebElement> exports = BaseService.findElementsByAutomationId("exportItem");
        return exports.size();
    }
    public static int getFilterMemberExportCount() {
        List<WebElement> exports = BaseService.findElementsByAutomationId("exportItem");
        return exports.size();
    }

    public static ExportItemModel getLastExport() throws InterruptedException {
        Thread.sleep(1500);
        return new ExportItemModel();
    }

    public static int getFilterCountMembersResult() {
        String text = BaseService.findElementByAutomationId("numOfMembers").getText();
        return Integer.parseInt(text);
    }

    private static UpdateMembershipResponse updateMembershipConsent(String membershipKey, String origin) {
        String locationId = NewApplicationService.getEnvProperties().getLocationId();
        String serverToken = NewApplicationService.getEnvProperties().getServerToken();
        UpdateMembership updateMembership = new UpdateMembership();
        updateMembership.setLocationID(locationId);
        updateMembership.setMembershipKey(membershipKey);
        updateMembership.setToken(serverToken);
        updateMembership.setOrigin(origin);
        UpdatedFields updatedFields = new UpdatedFields();
        updateMembership.setUpdatedFields(updatedFields);

        return (UpdateMembershipResponse) updateMembership.sendRequestByTokenAndLocation(serverToken, locationId, UpdateMembershipResponse.class);
    }

    private static boolean checkLogInHub(String expectedMessage) {
        List<WebElement> elements = getDriver().findElements(By.xpath("//*[@ng-repeat=\"log in logs\"]"));
        for (WebElement element: elements) {
            String logRow =  element.getText();
            if(logRow.contains(expectedMessage))
                return true;
        }
       return false;
    }

    public static boolean isLogAppears(String msg) throws Exception{
        waitForLoad();
        boolean exist = false;
        long timeoutCounter = 0;
        Thread.sleep(2000);
        while (!exist && (timeoutCounter <= TIMEOUT_FOR_HUB_UPDATE_WAIT_SEC)) {
            try {
                Navigator.refresh();
                Thread.sleep(1000);
                exist = checkLogInHub(msg);
                if (!exist)
                    timeoutCounter++;

            } catch (NoSuchElementException e) {
                Thread.sleep(1000);
                timeoutCounter++;
            }
        }
        return exist;

    }

    private static String checkLog(String expectedMessage) {
        String logRow = "";
        List<WebElement> elements = getDriver().findElements(By.xpath("//*[@ng-repeat=\"log in logs\"]"));
        for (WebElement element: elements) {
            logRow =  element.getText().replace(" ","");
            log4j.info("logRow: " + logRow);
            if(logRow.contains(expectedMessage))
                return element.getText();
        }
        return logRow;
    }

    public static String getLogAppears(String msg) throws Exception{
        waitForLoad();
        String foundLog = "";
        long timeoutCounter = 0;
        Thread.sleep(2000);
        while (foundLog.equals("") && (timeoutCounter <= TIMEOUT_FOR_HUB_UPDATE_WAIT_SEC)) {
            try {
                Navigator.refresh();
                Thread.sleep(1000);
                foundLog = checkLog(msg.replace(" ",""));
                if (foundLog.equals(""))
                    timeoutCounter++;

            } catch (NoSuchElementException e) {
                Thread.sleep(1000);
                timeoutCounter++;
            }
        }
        return foundLog;

    }

//    public static boolean performAnActionAddAmountOfCreditAndValidate(String amount,CharSequence timeStamp) throws Exception{
//        log4j.info("perform action add Credit");
//        performSmartActionAddCredit(amount,timeStamp);
//        return MembersService.isPointsAddedToCredit(0,Integer.valueOf(amount).intValue());
//    }

    public static void deactivateSmartClubDeal(String name) throws Exception{
        Navigator.benefitsTab();
        waitForLoad();
        getDriver().findElement(BaseBenefitsPage.ClubDealsPage).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
        getDriver().findElement(BaseBenefitsPage.SmartDealPage).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
        deactivateClubDeal(name);
    }

    public static String checkUserLog(String expectedMessage) {

        List<WebElement> elements = getDriver().findElements(By.xpath("//*[@ng-switch=\"log.Action\"]"));
        for (WebElement element: elements) {
            String logRow =  element.getText();
            if(logRow.contains(expectedMessage))
                return logRow;
        }
        return "No matching log found";

    }
}


