package hub.services.member;

import hub.base.BaseSeleniumObject;

public class MembersLog extends BaseSeleniumObject {

    public static final String LOGS = "ng-repeat=\"log in logs\"";
    public static final String LAST_LOG = "(//*[@ng-repeat=\"log in logs\"])[1]";

    public static String EMAIL_SENT_MESSAGE(String templateName, String source) {  return String.format("%s was emailed to member (from %s)", templateName, source);}
    public static String EMAIL_REACHED_MESSAGE(String templateName) {  return String.format("%s reached member inbox", templateName);}
    public static String EMAIL_OPENED_MESSAGE(String templateName) {  return String.format("%s was opened by member", templateName);}



    @Override
    public String getNextTagsPath(int i) {
        return null;
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
