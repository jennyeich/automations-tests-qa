package hub.hub1_0.services.automations;

import hub.base.BaseService;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.SmartAutomation;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Preset;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.services.Navigator;
import hub.common.objects.smarts.SmartBirthdayAndAnniversaryAutomation;
import hub.common.objects.smarts.smarObjects.settings.ISettings;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import utils.AutomationException;

import java.beans.IntrospectionException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static hub.base.BaseService.*;


/**
 * Created by Jenny on 9/26/2016.
 */


public class SmartAutomationService  {


    public static void createSmartAutomation (ISmartAutomationCounter automation)throws InterruptedException, AutomationException {

        if (automation.getSaveAsPreset()!=null)
            automation.setSaveAsPresetArrow("Yes");
        else
            automation.setSaveButton("Yes");

        if(automation instanceof SmartAutomation){
            Navigator.operationsTabNewSmartAutoamtion();
            fillSeleniumObject((SmartAutomation)automation);
        }else{
            Navigator.operationsTabNewSmartBirthdayAutoamtion();
            fillSeleniumObject((SmartBirthdayAndAnniversaryAutomation)automation);
        }

        //wait for page to load
        waitForLoad();
        //wait for all automations in list to load
        waitForLoad(BaseService.getByAutomationId("loading_spinner"));
        //wait for the 'Add New' button to appear
        getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),'Add New')]")));

    }



    public static void clickSmartAutomation(String autoName) throws Exception{

        Navigator.operationsTabSmartAutomations();
        WebElement webElement = getSmartAutomationElement(autoName);
        webElement.click();
        TimeUnit.SECONDS.sleep(3);
    }

    public static void deletePresetFromNewAutomation (Preset preset) throws Exception {

        try {
            Navigator.operationsTabNewSmartAutoamtion();
            waitForLoad();
            Thread.sleep(10000);
            fillSeleniumObject(preset);
            Thread.sleep(10000);
        }catch (AutomationException e) {
            if(e.getMessage().contains("ElementNotVisibleException"))
                return;
        }

    }

    public static void deletePresetFromExistingAutomation (SmartAutomation automation,Preset preset) throws Exception {

        try
        {
            clickSmartAutomation(automation.getSettings().getAutomationName());
            waitForLoad();
            Thread.sleep(10000);
            fillSeleniumObject(preset);
            Thread.sleep(10000);
        } catch (AutomationException e) {
            if(e.getMessage().contains("ElementNotVisibleException"))
                return;

        }

    }



    public static Boolean checkIfPresetExists (String presetName) throws Exception{
        Thread.sleep(10000);
        Navigator.operationsTabNewSmartAutoamtion();
        Thread.sleep(20000);
        List<WebElement> presets = getDriver().findElements(By.id("preset"));
        Select select = new Select(presets.get(0));
        try {
            select.selectByVisibleText(presetName);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }


    private static String getAutomationRuleID(ISettings settings) throws Exception {
        String autoURL = getAutomationURL(settings);
        if(settings instanceof Settings) {
            return "automation_" + autoURL;
        }else{
            return "userFieldsAutomations_" + autoURL;
        }

    }


    public static WebElement checkIfAutomationCreated(String autoName) throws InterruptedException {

        return getSmartAutomationElement(autoName);

    }

    private static WebElement getSmartAutomationElement(String autoName) {
        String xPath = "//a[contains(text(),'" + autoName + "')]";
        WebElement automation = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
        return automation;
    }


    public static String getAutomationURL (ISettings settings)throws Exception {

        if(settings instanceof Settings) {
            Navigator.operationsTabSmartAutomations();
        }else{
            Navigator.operationsTabSmartBirthdayAutoamtion();
        }
        Thread.sleep(3000);
        String autoNumber = "Automation not found";
        try{
            WebElement automation = getSmartAutomationElement(settings.getAutomationName());
            autoNumber = selectAutomationAndGetRuleId(automation);
        }catch(TimeoutException e){
            //try again
            Thread.sleep(2000);
            WebElement automation = getSmartAutomationElement(settings.getAutomationName());
            autoNumber = selectAutomationAndGetRuleId(automation);
        }
        return autoNumber;


    }


    private static String selectAutomationAndGetRuleId(WebElement automation) throws InterruptedException {
        automation.click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(),\"Edit Automation\")]")));
        String autoNumber = concatNumber();
        return autoNumber;
    }


    private static String concatNumber() throws InterruptedException {
        String url = getDriver().getCurrentUrl();
        String autoNumber = url.substring(url.lastIndexOf("/") + 1, url.indexOf("?"));
        return autoNumber;
    }


}
