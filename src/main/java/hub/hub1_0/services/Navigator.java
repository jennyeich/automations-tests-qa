package hub.hub1_0.services;

import hub.base.BaseService;
import hub.base.basePages.BaseAcpPage;
import hub.base.basePages.BaseBenefitsPage;
import hub.base.basePages.BaseContentPage;
import hub.base.basePages.BaseDataAndBIPage;
import hub.hub1_0.base.basePages.BaseOperationsPage;
import hub.hub1_0.base.basePages.BaseTabsPage;
import hub.hub2_0.basePages.loyalty.LoyaltyCenterPage;
import hub.hub2_0.services.loyalty.Gifts.GiftService;
import hub.services.UIDebugLogger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import utils.AutomationException;
import utils.EnvProperties;
import utils.JsonUtils;
import utils.PropsReader;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * Created by Jenny on 9/21/2016.
 */
public class Navigator  extends BaseService {


//    private static boolean whatfixClicked = false;

    public static void selectApplicationFromEnv() throws Exception{
        String locationId= PropsReader.getPropValuesForEnv("locationID");
        selectApplicationByLocationId(locationId);
    }

    public static void selectApplication() throws Exception{
        String locationId= EnvProperties.getInstance().getLocationId();
        selectApplicationByLocationId(locationId);
    }



    public static void selectApplicationByLocationId(String locationId) throws Exception{
        String locId = locationId.substring(locationId.indexOf('_') + 1);
        String urlForDashboard = PropsReader.getPropValuesForEnv("HubURL").replace("login","dashboard/qlik2");
        String url = urlForDashboard + "?location_id=" + locId + "&location_version=0";
        getDriver().get(url);
        waitForLoad();
        refresh();
        waitForLoad();
        waiForPageToBeReady();
    }

    public static void selectApplication(String appName) throws InterruptedException, AutomationException {

        try {
            waiForPageToBeReady();

            WebElement appSelect = getDriver().findElement(BaseTabsPage.APP_DROPDOWN);
            getDriverWait().until(ExpectedConditions.elementToBeClickable(appSelect));

            Actions action = new Actions(getDriver());
            action.moveToElement(appSelect).build().perform();
            Thread.sleep(5000);


            WebElement placeholder = getDriver().findElement(By.xpath("//input[@placeholder=\"By name/ID\"]"));
            action.moveToElement(placeholder).click(placeholder).build().perform();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            action.sendKeys(placeholder,appName).build().perform();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

            String xPath = "//span[text()=\"" + appName + "\"]";

            WebElement element = getDriver().findElement(By.xpath(xPath));
            action.click(element).build().perform();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
            waiForPageToBeReady();
        }  catch (NoSuchElementException e) {
            handleException(e,"selectApplication");
        }


    }

    public static void selectCreateNewApplication() throws AutomationException,InterruptedException {

        try {

            waiForPageToBeReady();

            WebElement appSelect = getDriver().findElement(BaseTabsPage.APP_DROPDOWN);
            getDriverWait().until(ExpectedConditions.elementToBeClickable(appSelect));

            Actions action = new Actions(getDriver());
            action.moveToElement(appSelect).build().perform();
            Thread.sleep(5000);


            WebElement placeholder = getDriver().findElement(By.xpath("//input[@placeholder=\"By name/ID\"]"));
            action.moveToElement(placeholder).click(placeholder).build().perform();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            action.sendKeys(placeholder,"Create New App").build().perform();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);



            WebElement element = findElementByAutomationId("createApp");
            action.click(element).build().perform();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad(By.id("loading-bar-spinner"));
            waiForPageToBeReady();

        }  catch (NoSuchElementException e) {
            handleException(e,"selectCreateNewApplication");
        }

    }

    //Navigate to the benefits tab
    public static void benefitsTab() throws AutomationException,InterruptedException {

        try {
            waitForLoad();
            WebElement element = getDriver().findElement(BaseTabsPage.TAB_BENEFITS);
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            BaseService.setHub2mode(false); // return flag to Hub1 state
            //wait until 'gift' side menu title is visible
            waitForLoad();
            element = getDriverWait().until(ExpectedConditions.elementToBeClickable(BaseBenefitsPage.Gifts_main_menu));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            UIDebugLogger.addMsgToLog(UIDebugLogger.INFO,"Select  <Assets Tab>");
            waitForLoad();
        } catch (NoSuchElementException e) {
            handleException(e,"benefitsTab");
        }

    }



    //Navigate to the gift menu
    public static void benefitsTabGifts() throws AutomationException,InterruptedException {

        try {
            benefitsTab();
            waitForLoad();
            getDriver().findElement(BaseBenefitsPage.Gifts_main_menu).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"benefitsTabGifts");
        }
    }

    public static void benefitsTabPointShop() throws AutomationException,InterruptedException{
        try {
            benefitsTab();
            waitForLoad();
            getDriver().findElement(BaseBenefitsPage.PointShop_main_menu).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        } catch (NoSuchElementException e) {
            handleException(e,"benefitsTabPointShop");
        }

    }

    public static boolean findGift(String title) throws AutomationException,InterruptedException {

        boolean found = false;
        int i=0;
        String path = "//*[@id=\"gift-" + i+ "\"]/div/a/h6";
        List<WebElement> elements  = getDriver().findElements(By.xpath(path));
        while (!found && elements.size()>0) {
            if (elements.get(0).getText().equals(title)) {
                found = true;
                elements.get(0).click();
                Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            } else {
                i=i+1;
                path = "//*[@id=\"gift-" + i+ "\"]/div/a/h6";
                elements = getDriver().findElements(By.xpath(path));
            }
        }
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        return found;
    }

    public static boolean findLotteryReward(String title) throws AutomationException,InterruptedException {

        boolean found = false;
        int i=0;
        String path = "//*[@id=\"reward-" + i+ "\"]/div/a/h6";
        List<WebElement> elements  = getDriver().findElements(By.xpath(path));
        while (!found && elements.size()>0) {
            if (elements.get(0).getText().equals(title)) {
                found = true;
                elements.get(0).click();
                Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            } else {
                i=i+1;
                path = "//*[@id=\"reward-" + i+ "\"]/div/a/h6";
                elements = getDriver().findElements(By.xpath(path));
            }
        }
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        return found;
    }


    public static void benefitsTabSmartGifts() throws Exception {

        try {
            benefitsTabGifts();
            getDriver().findElement(BaseBenefitsPage.Smart_Gifts).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"benefitsTabSmartGifts");
        }

    }

    public static void benefitsTabAddSmartGift() throws Exception {

        try {
            benefitsTabSmartGifts();
            //wait until 'Create gift' title is visible
            WebElement element = getDriverWait().until(ExpectedConditions.elementToBeClickable(BaseBenefitsPage.Add_Smart_gift));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        } catch (NoSuchElementException e) {
            handleException(e,"benefitsTabAddSmartGift");
        }
    }

    public static void benefitsTabAddGiftCard() throws AutomationException,InterruptedException {

        try {
            benefitsTabGifts();
            getDriver().findElement(BaseBenefitsPage.gift_cards).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
            //wait until 'Create gift' title is visible
            WebElement element = getDriverWait().until(ExpectedConditions.elementToBeClickable(BaseBenefitsPage.Add_Gift_card));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"benefitsTabAddGiftCard");
        }
    }


    public static void benefitsTabSmartClubDeals () throws Exception {

        try {
            benefitsTab();
            waitForLoad();
            getDriver().findElement(BaseBenefitsPage.ClubDealsPage).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();

            getDriver().findElement(BaseBenefitsPage.SmartDealPage).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"benefitsTabSmartClubDeals");
        }
    }

    public static void benefitsTabAddSmartClubDeal() throws AutomationException,InterruptedException {

        try {
            benefitsTab();
            waitForLoad();
            getDriver().findElement(BaseBenefitsPage.ClubDealsPage).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();

            getDriver().findElement(BaseBenefitsPage.SmartDealPage).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
            //wait until 'Create gift' title is visible
            WebElement element = getDriverWait().until(ExpectedConditions.elementToBeClickable(BaseBenefitsPage.Add_Club_Deal));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"benefitsTabAddSmartClubDeal");
        }
    }

    public static void benefitsTabAddClubDeal() throws AutomationException,InterruptedException {

        try {
            benefitsTab();
            waitForLoad();
            getDriver().findElement(BaseBenefitsPage.ClubDealsPage).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();

            getDriver().findElement(BaseBenefitsPage.OldClubDealPage).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
            //wait until 'Create gift' title is visible
            WebElement element = getDriverWait().until(ExpectedConditions.elementToBeClickable(BaseBenefitsPage.Add_Old_Club_Deal));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"benefitsTabAddClubDeal");
        }
    }

    public static void benefitsTabCodes () throws AutomationException,InterruptedException{

        try {
            benefitsTab();
            waitForLoad();
            getDriver().findElement(BaseBenefitsPage.Codes).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"benefitsTabCodes");
        }

    }

    public static void thirdPartCodes ()throws AutomationException,InterruptedException{

        try {
            benefitsTabCodes();
            getDriver().findElement(BaseBenefitsPage.THIRD_PARTY_Codes).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            UIDebugLogger.addMsgToLog(UIDebugLogger.INFO,"Select  <Third Party Codes>");
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"thirdPartCodes");
        }

    }

    public static void couponCodes ()throws AutomationException,InterruptedException{

        try {
            benefitsTabCodes();
            getDriver().findElement(BaseBenefitsPage.COUPON_Codes).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            UIDebugLogger.addMsgToLog(UIDebugLogger.INFO,"Select  <Coupon Codes>");
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"couponCodes");
        }

    }

    public static void dataAndBIPageFindMember() throws AutomationException,InterruptedException {

        try {
            dataAndBIPage();
            getDriver().findElement(BaseDataAndBIPage.Find_MembersPage).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            UIDebugLogger.addMsgToLog(UIDebugLogger.INFO,"Select  <Find Member>");
            waitForLoad();
        } catch (NoSuchElementException e) {
            handleException(e,"dataAndBIPageFindMember");
        }
    }

    public static void dataAndBIPageFilterMember() throws AutomationException,InterruptedException {

        try {
            dataAndBIPage();
            waitForLoad();
            getDriver().findElement(BaseDataAndBIPage.Filter_MembersPage).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            UIDebugLogger.addMsgToLog(UIDebugLogger.INFO,"Select  <Filter Members>");
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"dataAndBIPageFilterMember");
        }
    }

    public static void acpTab() throws AutomationException,InterruptedException {

        try {
            waitForLoad();
            WebElement element = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseTabsPage.TAB_ACP));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            UIDebugLogger.addMsgToLog(UIDebugLogger.INFO,"Select  <ACP Tab>");
            BaseService.setHub2mode(false); // return flag to Hub1 state
        }  catch (NoSuchElementException e) {
            handleException(e,"acpTab");
        }
    }


    public static void loyaltyTabPage()throws AutomationException,InterruptedException{

        try {

            waitForLoad();
            WebElement element = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseTabsPage.TAB_LOYALTY_CENTER));
            element.click();
            BaseService.setHub2mode(true);
            UIDebugLogger.addMsgToLog(UIDebugLogger.INFO,"Select  <Loyalty Center Tab>");
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            //checkForWhatfix();

        }  catch (NoSuchElementException e) {
            handleException(e,"loyaltyTabPage");
        }

    }

    public static void loyaltyTabPageAndClickOnGiftPage() throws AutomationException,InterruptedException {

        try {

            waitForLoad();
            WebElement element = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseTabsPage.TAB_LOYALTY_CENTER));
            element.click();
            BaseService.setHub2mode(true);
            UIDebugLogger.addMsgToLog(UIDebugLogger.INFO,"Select  <Loyalty Center Tab>");
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            TimeUnit.SECONDS.sleep(3);
            WebElement element2 = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(LoyaltyCenterPage.GIFTS));
            element2.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }  catch (NoSuchElementException e) {
            handleException(e,"loyaltyTabPageAndClickOnGiftPage");
        }
    }

    public static void loyaltyTabPageAndClickOnGiftPageAndOpenGiftAnalysis(String giftName) throws AutomationException,InterruptedException {

        try {
            waitForLoad();
            WebElement element = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseTabsPage.TAB_LOYALTY_CENTER));
            element.click();
            BaseService.setHub2mode(true);
            UIDebugLogger.addMsgToLog(UIDebugLogger.INFO,"Select  <Loyalty Center Tab>");
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            WebElement element2 = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(LoyaltyCenterPage.GIFTS));
            element2.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            GiftService.clickOnGiftAnalysis(giftName);

        }  catch (NoSuchElementException e) {
            handleException(e,"loyaltyTabPageAndClickOnGiftAnalysis");
        }
    }

    public static void loyaltyTabPageAndClickOnCampaignCenterPage() throws AutomationException,InterruptedException {

        try {

            waitForLoad();
            WebElement element = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseTabsPage.TAB_LOYALTY_CENTER));
            element.click();
            BaseService.setHub2mode(true);
            UIDebugLogger.addMsgToLog(UIDebugLogger.INFO,"Select  <Loyalty Center Tab>");
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            WebElement element2 = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(LoyaltyCenterPage.CAMPAIGN_CENTER));
            element2.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);

        }  catch (NoSuchElementException e) {
            handleException(e,"loyaltyTabPageAndClickOnCampaignCenter");
        }
    }

//    private static void checkForWhatfix(){
//
//        if(!whatfixClicked) {
//            try {
//                getDriverWait().withTimeout(5, TimeUnit.SECONDS).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"wfx-frame-smartPopup\"]")));
//                getDriver().switchTo().frame(driver.findElement(By.xpath("//*[@id=\"wfx-frame-smartPopup\"]")));
//                getDriver().findElement(By.xpath("/html/body/div[2]/div[2]/div[1]/div/button")).click();
//                Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
//                getDriver().switchTo().defaultContent();
//                whatfixClicked = true;
//            } catch (Exception e) {
//                //do nothing if not exist
//            }
//        }
//    }

    public static void acpTabPresets() throws AutomationException,InterruptedException {
        try {
            acpTab();
            WebElement element = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseAcpPage.Presets));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"acpTabPresets");
        }
    }

    public static void acpTabUserPlans() throws AutomationException,InterruptedException {
        acpTab();
        WebElement element = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseAcpPage.User_Plans));
        element.click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }

    public static void acpTabBusinessPlans() throws AutomationException,InterruptedException {
        acpTab();
        WebElement element = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseAcpPage.Business_Plans));
        element.click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }

    public static void acpTabManageComoHubUsersUpdateUserCurrentUser() throws AutomationException,InterruptedException {
        try {
            acpTab();
            waitForLoad();
            getDriver().findElement(BaseAcpPage.Manage_Como_Hub_Users).click();
            waitForLoad();
            getDriver().findElement(BaseAcpPage.Update_User).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            Navigator.refresh();
            getDriver().findElement(BaseAcpPage.Update_User_Me).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        } catch (NoSuchElementException e) {
            handleException(e,"acpTabManageComoHubUsersUpdateUserCurrentUser");
        }

    }

    public static void acpTabManageComoHubUsersAddNewUser() throws AutomationException,InterruptedException {
        try {
            acpTab();
            waitForLoad();
            getDriver().findElement(BaseAcpPage.Manage_Como_Hub_Users).click();
            waitForLoad();
            getDriver().findElement(BaseAcpPage.Create_New_User).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        } catch (NoSuchElementException e) {
            handleException(e,"acpTabManageComoHubUsersAddNewUser");
        }

    }

    public static void dataAndBIPage() throws AutomationException,InterruptedException{
        try {
            waitForLoad();
            getDriver().findElement(BaseTabsPage.TAB_DATA_BI).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            UIDebugLogger.addMsgToLog(UIDebugLogger.INFO,"Select  <Data & BI Tab>");
            BaseService.setHub2mode(false); // return flag to Hub1 state
            waitForLoad();
        } catch (NoSuchElementException e) {
            handleException(e,"dataAndBIPage");
        }
    }

    public static void importUserKeysPage() throws AutomationException,InterruptedException {
        try {
            waitForLoad();
            dataAndBIPage();
            getDriver().findElement(BaseDataAndBIPage.Import_User_Keys_Menu).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        } catch (NoSuchElementException e) {
            handleException(e,"importUserKeysPage");
        }
    }

    public static void operationsTab()throws AutomationException,InterruptedException{

        try {
            waitForLoad();
            WebElement element = getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseTabsPage.TAB_OPERATION));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            UIDebugLogger.addMsgToLog(UIDebugLogger.INFO,"Select  <operation Tab>");
            BaseService.setHub2mode(false); // return flag to Hub1 state
        }catch (NoSuchElementException e) {
            handleException(e,"operationsTab");
        }
    }
    public static void autoPilotPage()throws AutomationException,InterruptedException{
        try {
            operationsTab();
            getDriver().findElement(BaseOperationsPage.AutopilotModePage).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }catch (NoSuchElementException e) {
            handleException(e,"autoPilotPage");
        }
    }
    public static void operationsTabNewMember()throws AutomationException,InterruptedException{

        try {
            operationsTab();
            waitForLoad();
            getDriver().findElement(BaseOperationsPage.Registration).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            getDriver().findElement(BaseOperationsPage.New_Members).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"operationsTabNewMember");
        }

    }

    public static void operationsTabNewSmartAutoamtion()throws AutomationException,InterruptedException{

        try {
            operationsTabSmartAutomations();
            WebElement element = getDriver().findElement(BaseOperationsPage.Add_New_SmartAutomation);
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
            getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),\"Save and Close\")]")));
        } catch (NoSuchElementException e) {
            handleException(e,"operationsTabNewSmartAutoamtion");
        }
    }

    public static void operationsTabNewSmartBirthdayAutoamtion()throws AutomationException,InterruptedException {

        try {
            waitForLoad();
            operationsTab();
            waitForLoad();
            getDriver().findElement(BaseOperationsPage.AutopilotModePage).click();
            waitForLoad();
            WebElement element = getDriver().findElement(BaseOperationsPage.Birthday_Automations_menu);
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            WebElement element2 = getDriver().findElement(BaseOperationsPage.Add_New_SmartAutomation);
            element2.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
            getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//button[contains(text(),\"Save and Close\")]")));
        }  catch (NoSuchElementException e) {
            handleException(e,"operationsTabNewSmartBirthdayAutoamtion");
        }
    }

    public static void operationsTabSmartBirthdayAutoamtion()throws AutomationException,InterruptedException {

        try {
            waitForLoad();
            operationsTab();
            waitForLoad();
            getDriver().findElement(BaseOperationsPage.AutopilotModePage).click();
            waitForLoad();
            WebElement element = getDriver().findElement(BaseOperationsPage.Birthday_Automations_menu);
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        }  catch (NoSuchElementException e) {
            handleException(e,"operationsTabSmartBirthdayAutoamtion");
        }

    }

    public static void operationsTabSmartAutomations()throws AutomationException,InterruptedException {

        try {
            waitForLoad();
            operationsTab();
            waitForLoad();
            getDriver().findElement(BaseOperationsPage.AutopilotModePage).click();
            waitForLoad();
            getDriver().findElement(BaseOperationsPage.Smart_Automations_tab).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(),\"Add New\")]")));
            waitForLoad(BaseService.getByAutomationId("loading_spinner"));
        }  catch (NoSuchElementException e) {
            handleException(e,"operationsTabSmartAutomations");
        }
    }

    public static void operationsTabPOSSettings()throws AutomationException,InterruptedException{

        try {
            operationsTab();
            waitForLoad();
            getDriver().findElement(BaseOperationsPage.Settings).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
            getDriver().findElement(BaseOperationsPage.Settings_pos_settings).click();
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"operationsTabPOSSettings");
        }

    }

    //Navigate to the punch cards menu
    public static void benefitsTabPunchCards() throws AutomationException,InterruptedException {

        try {
            benefitsTab();
            waitForLoad();
            ;
            getDriver().findElement(BaseBenefitsPage.SideMenuPunchCards).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"benefitsTabPunchCards");
        }
    }


    public static void benefitsTabAddSmartPunchCards() throws AutomationException,InterruptedException {

        try {
            benefitsTabPunchCards();
            getDriver().findElement(BaseBenefitsPage.SideMenu_SmartPunchCards).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
            //wait until 'Add punch card' title is visible
            WebElement element = getDriverWait().until(ExpectedConditions.elementToBeClickable(BaseBenefitsPage.Add_punch_card));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"benefitsTabAddSmartPunchCards");
        }
    }
    public static void refresh() {
        getDriver().navigate().refresh();
        waitForLoad();
    }

    public static void operationsTabPointsCreditSettings() throws Exception{
        try {
            operationsTab();
            waitForLoad();
            getDriver().findElement(BaseOperationsPage.Settings).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            getDriver().findElement(BaseOperationsPage.Settings_point_budget_settings).click();
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"operationsTabPointsCreditSettings");
        }
    }

    public static void operationsTabPermissionSettings() throws Exception{
        try {
            operationsTab();
            waitForLoad();
            getDriver().findElement(BaseOperationsPage.Settings).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            getDriver().findElement(BaseOperationsPage.Settings_permission_settings).click();
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"operationsTabPermissionSettings");
        }
    }


    public static void operationsTabRegistrationForm() throws Exception{
        try {
            operationsTab();
            waitForLoad();
            getDriver().findElement(BaseOperationsPage.Registration).click();
            Thread.sleep(5000);
            getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseOperationsPage.Registration_registration_form));
            WebElement regFormSubmenu = getDriver().findElement(BaseOperationsPage.Registration_registration_form);
            regFormSubmenu.click();
            Thread.sleep(5000);
            waitForLoad();
            getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[contains(text(),\"Registration Form\")]")));
        } catch (NoSuchElementException e) {
            handleException(e,"operationsTabRegistrationForm");
        }
    }

    public static void operationTabAppSettings() throws Exception{
        try {
            operationsTab();
            waitForLoad();
            getDriver().findElement(BaseOperationsPage.Settings).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
            getDriver().findElement(BaseOperationsPage.Settings_app_settings).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"operationTabAppSettings");
        }
    }

    public static void operationTabExternalSettings() throws Exception{
        try {
            operationsTab();
            waitForLoad();
            getDriver().findElement(BaseOperationsPage.Settings).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
            getDriver().findElement(BaseOperationsPage.Settings_external_settings).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"operationTabExternalSettings");
        }
    }


    public static void contentTab() throws AutomationException,InterruptedException {
        try {
            waitForLoad();
            getDriver().findElement(BaseTabsPage.TAB_CONTENT).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            UIDebugLogger.addMsgToLog(UIDebugLogger.INFO,"Select  <Content Tab>");
            BaseService.setHub2mode(false); // return flag to Hub1 state
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"contentTab");
        }
    }

    public static void contentTabInformation() throws AutomationException,InterruptedException {
        contentTab();
        getDriver().findElement(BaseContentPage.Information_menu).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }

    public static void contentTabBranding() throws AutomationException,InterruptedException {
        contentTab();
        getDriver().findElement(BaseContentPage.Branding_menu).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }

    public static void contentTabDesign() throws AutomationException,InterruptedException {
        contentTabBranding();
        getDriver().findElement(BaseContentPage.Branding_Design).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }

    public static void contentTabLocations() throws AutomationException,InterruptedException {
        contentTabInformation();
        getDriver().findElement(BaseContentPage.Information_Locations).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }

    public static void contentTabWelcomeMessages() throws AutomationException,InterruptedException {
        contentTabInformation();
        getDriver().findElement(BaseContentPage.Welcome_messages).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }
    public static void contentTabFiles() throws AutomationException,InterruptedException {
        contentTabInformation();
        getDriver().findElement(BaseContentPage.Files).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }
    public static void contentTabCustomScreen() throws AutomationException,InterruptedException {
        contentTabInformation();
        getDriver().findElement(BaseContentPage.Custom_screen).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }
    public static void contentTabWebViews() throws AutomationException,InterruptedException {
        contentTabInformation();
        getDriver().findElement(BaseContentPage.Web_views).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }
    public static void contentTabBadges() throws AutomationException,InterruptedException {
        contentTabInformation();
        getDriver().findElement(BaseContentPage.Badges).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }
    public static void contentTabPhotoGallery() throws AutomationException,InterruptedException {
        contentTabInformation();
        getDriver().findElement(BaseContentPage.Photo_gallery).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }
    public static void contentTabSplashScreen() throws AutomationException,InterruptedException {
        contentTabInformation();
        getDriver().findElement(BaseContentPage.Splash_screen_ads).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }
    public static void contentTabCatalogs() throws Exception{
        try {
            contentTab();
            waitForLoad();
            getDriver().findElement(BaseContentPage.Catalogs).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"contentTabCatalogs");
        }
    }

    public static void contentTabPublishApp() throws Exception {
        try {
            contentTab();
            waitForLoad();
            getDriver().findElement(BaseContentPage.UpdateApp).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            getDriver().findElement(BaseContentPage.PublishApp).click();
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"contentTabPublishApp");
        }
    }

    public static void contentTabGeneralForms() throws Exception {
        try {
            contentTab();
            Navigator.refresh();
            waitForLoad();
            getDriver().findElement(BaseContentPage.Forms).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            getDriver().findElement(BaseContentPage.GeneralForms).click();
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"contentTabGeneralForms");
        }
    }

    public static void contentTabSurvey() throws Exception {
        try {
            contentTab();
            waitForLoad();
            getDriver().findElement(BaseContentPage.Forms).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            getDriver().findElement(BaseContentPage.Survey).click();
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"contentTabSurvey");
        }
    }

    public static void contentTabLayoutHomeScreen() throws Exception {
        try {
            contentTab();
            waitForLoad();
            getDriver().findElement(BaseContentPage.Layout).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            getDriver().findElement(BaseContentPage.Layout_HomeScreen).click();
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"contentTabLayoutHomeScreen");
        }

    }

    public static void contentTabEmailTemplatesManager() throws Exception {
        try {
            contentTab();
            waitForLoad();
            getDriver().findElement(BaseContentPage.EmailTemplatesManager).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"contentTabEmailTemplatesManager");
        }
    }

    public static void contentTabAddTemplate() throws Exception {
        try {
            contentTabEmailTemplatesManager();
            //wait until 'Add new" button is visible
            WebElement element = getDriverWait().until(ExpectedConditions.elementToBeClickable(BaseContentPage.Add_Template));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"contentTabAddTemplate");
        }
    }

    public static void benefitsTabAddLotteryRewards() throws Exception{
        try {
            benefitsTabLotteryRewardsMenu();
            //wait until 'Add Lottery Reward' title is visible
            WebElement element = getDriverWait().until(ExpectedConditions.elementToBeClickable(BaseBenefitsPage.Add_Lottery_Reward));
            element.click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        } catch (NoSuchElementException e) {
            handleException(e,"benefitsTabAddLotteryRewards");
        }

    }

    public static void benefitsTabLotteryRewardsMenu() throws Exception{
        try {
            benefitsTab();
            waitForLoad();
            getDriver().findElement(BaseBenefitsPage.LotteryRewards_main).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"benefitsTabLotteryRewardsMenu");
        }
    }

    public static void operationsTabNewAutomation() throws AutomationException,InterruptedException {
        try {
            autoPilotPage();
            getDriver().findElement(BaseOperationsPage.Automations_tab).click();
            Navigator.refresh();
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"operationsTabNewAutomation");
        }
    }

    public static void acpTabImportUsersImproved() throws Exception{
        try {
            acpTab();
            waitForLoad();
            getDriver().findElement(BaseAcpPage.Import_Users_Improved).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
            //open import from csv section
            getDriver().findElement(By.xpath("//*[@id=\"main\"]/div/div[4]/i")).click();
        }  catch (NoSuchElementException e) {
            handleException(e,"acpTabImportUsersImproved");
        }
    }

    public static void acpTabPresetsFilterMember() throws Exception{
        try {
            acpTabPresets();
            waitForLoad();
            getDriver().findElement(BaseAcpPage.Presets_Filter_members).click();
            Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
            waitForLoad();
        }  catch (NoSuchElementException e) {
            handleException(e,"acpTabPresetsFilterMember");
        }
    }

    
    protected static void handleException(NoSuchElementException e,String invokedFrom) throws AutomationException {
        String jsonMessage = e.getMessage().substring(e.getMessage().indexOf('{'),e.getMessage().indexOf('}')+1);
        jsonMessage = jsonMessage.replace("=\"","='");//we need to replace special character " for json to '
        jsonMessage = jsonMessage.replace("\"]","']");//we need to replace special character " for json to '
        Map<String, Object> data = JsonUtils.convertJsonStringToMap(jsonMessage);
        throw new AutomationException( "Exception invoked from << Navigator."+ invokedFrom+">>\n  << Unable to locate element with: " + data.get("method") + " = " + data.get("selector") + " >>",e);
    }

    public static void joiningCodes() throws Exception{
        operationsTab();
        waitForLoad();
        getDriver().findElement(BaseOperationsPage.Registration).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        getDriverWait().until(ExpectedConditions.visibilityOfElementLocated(BaseOperationsPage.Registration_joiningCodes)).click();
        Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
        waitForLoad();
    }
}
