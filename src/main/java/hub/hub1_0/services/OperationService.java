package hub.hub1_0.services;

import com.google.appengine.labs.repackaged.com.google.common.collect.Lists;
import hub.base.BaseService;
import hub.common.objects.common.RegistrationField;
import hub.common.objects.operations.*;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.SmartAutomationAction;
import hub.hub1_0.common.objects.smarts.smarObjects.settings.Settings;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static hub.hub2_0.services.loyalty.BaseHub2Service.waitForLoad;

/**
 * Created by Goni on 06/06/2017.
 */
public class OperationService extends BaseService {

    public static final Logger log4j = Logger.getLogger(OperationService.class);

    public static void updateAppSettings(AppSettingsPage appSettingsPage) throws Exception {

        Navigator.operationTabAppSettings();
        fillSeleniumObject(appSettingsPage);

    }

    public static void loadAppSettings(AppSettingsPage appSettingsPage) throws Exception {

        Navigator.operationTabAppSettings();
        readSeleniumObject(appSettingsPage);

    }

    public static void updatePOSSettings(GeneralPOSSettingsPage generalPOSSettingsPage) throws Exception {

        Navigator.operationsTabPOSSettings();
        fillSeleniumObject(generalPOSSettingsPage);

    }

    public static void updatePointsCreditSettings(PointsCreditSettingsPage pointsCreditSettingsPage) throws Exception{
        Navigator.operationsTabPointsCreditSettings();
        fillSeleniumObject(pointsCreditSettingsPage);
    }

    public static void updatePermissionSettings(PermissionSettingsPage permissionSettingsPage) throws Exception{
        Navigator.operationsTabPermissionSettings();
        fillSeleniumObject(permissionSettingsPage);
    }

    public static void cleanMemberNotes() throws Exception {

        Navigator.operationsTabPOSSettings();

        List<WebElement> notes = getDriver().findElements(By.xpath("//*[@data-ng-click=\"ctrlPs.removeMemberNote($index);\"]"));
        int size = notes.size();
        //go over the member notes and delete them
        for (int i = 0; i < size; i++) {
            try {
                getDriver().findElement(By.xpath("(//*[@data-ng-click=\"ctrlPs.removeMemberNote($index);\"])[" + 1 + "]")).click();
                TimeUnit.SECONDS.sleep(1);
            } catch (Exception e) {
            }
        }
        getDriver().findElement(By.xpath("//*[@data-ng-click=\"ctrlPs.savePurchaseWeights();\"]")).click();
    }

    public static void openNewSmartAutomationAddTriggerAndOpenActionsDropDown() throws Exception {

        Navigator.operationsTabNewSmartAutoamtion();

        Settings settings = new Settings();
        settings.setTrigger(Triggers.SubmitAPurchase);
        fillSeleniumObject(settings);
        getDriver().findElement(By.xpath(SmartAutomationAction.SCENARIOS_TAB)).click();
        getDriver().findElement(By.xpath(SmartAutomationAction.ADD_ACTION)).click();
        Thread.sleep(2000);
        getDriver().findElement(By.xpath(SmartAutomationAction.PERFORM_THE_ACTION)).click();

    }

    public static void addRequiredFieldsToRegistrationForm(List<RegistrationField> registrationFields, String isRequired) throws Exception{
        addRequiredFieldsToRegistrationForm(registrationFields, isRequired, null);
    }
    public static void addRequiredFieldsToRegistrationForm(List<RegistrationField> registrationFields, String isRequired, String defaultValue) throws Exception {
        log4j.info("Navigate to registration tab" );
        Navigator.operationsTabRegistrationForm();
        //Nevigate to registration tab
        RegistrationFormPage registrationFormPage = new RegistrationFormPage();
        for(RegistrationField field:registrationFields ) {
            try {
                getDriver().findElement(By.xpath("//*[contains(text(), '" + field.toString() + "')]"));
                log4j.info("Field " + field.toString() + " already exist in Registration form.");
            } catch (Exception e) {
                registrationFormPage.addField(createField(field.toString(), isRequired, defaultValue));
            }

        }
        fillSeleniumObject(registrationFormPage);
        waitForLoad();
        log4j.info("Finished to Add required fields to Registration form");
    }

    public static  List<String> getAvailableRegistrationFields() throws Exception{
        log4j.info("Nevigate to registration tab" );
        Navigator.operationsTabRegistrationForm();
        List<String> registrationFields = Lists.newArrayList();

        List<WebElement> fields = getDriver().findElements(By.xpath("//*[@ng-bind=\"field.title\"]"));
        for (WebElement element : fields) {
            registrationFields.add(element.getText().trim());
        }

        return registrationFields;
    }

    public static void removeFieldsFromRegistrationForm(List<RegistrationField> registrationFields) throws Exception {
        log4j.info("Nevigate to registration tab" );
        Navigator.operationsTabRegistrationForm();
        WebElement element;
        for(RegistrationField field:registrationFields ) {
            try {
                element = getDriver().findElement(By.xpath("//*[contains(text(), '" + field.toString() + "')]//ancestor::div[1]//*[contains(@class,'icon-trash_can')]"));
                element.click();
                Thread.sleep(WAIT_AFTER_CLICK_MILLSEC);
                Alert alert = getDriver().switchTo().alert();
                alert.accept();
                waitForLoad();
                dismissNoticeMsg();
                log4j.info("Field " + field.toString() + " was removed successfully from registration form.");
            } catch (Exception e) {
                log4j.info("Field " + field.toString() + " doesnt exist in Registration form.");
            }
        }
        getDriver().findElement(By.xpath("(//*[contains(@automationid, 'save')])[2]")).click();
        waitForLoad();
        Navigator.refresh();
    }

    private static RegistrationFieldPage createField(String field, String isRequired, String defaultValue) {
        RegistrationFieldPage fieldPage = new RegistrationFieldPage();
        fieldPage.setFieldType(field.toString());
        fieldPage.setFieldTitle(field.toString());
        fieldPage.setRequired(isRequired);
        fieldPage.setDefaultValue(defaultValue);
        return fieldPage;
    }

    public static void setExternalSettings(ExternalServicesPage externalServicesPage) throws Exception {
        Navigator.operationTabExternalSettings();
        fillSeleniumObject(externalServicesPage);
        waitForLoad();
    }

    public static String checkUnsubscribeMethod () throws Exception {

        Navigator.operationTabExternalSettings();
        WebElement webElement = getDriver().findElement(By.xpath("//*[@automationid=\"smsUnsubscribeConfiguration\"]"));
        return webElement.getAttribute("val");

    }

    private static RegistrationFormPage buildRegistrationFormWithCustomField (String text1, String value1,String text2, String value2, RegistrationField field) throws Exception {

        RegistrationFormPage registrationFormPage = new RegistrationFormPage();
        FieldOption fieldOption = new FieldOption();
        fieldOption.addOption(text1,value1);
        FieldOption fieldOption2 = new FieldOption();
        fieldOption2.addOption(text2,value2);

        RegistrationFieldPage fieldPage = createField(field.toString());
        fieldPage.addFieldOption(fieldOption);
        fieldPage.addFieldOption(fieldOption2);
        registrationFormPage.addField(fieldPage);

        return registrationFormPage;

    }

    private  static RegistrationFieldPage createField(String field) {
        RegistrationFieldPage fieldPage = new RegistrationFieldPage();
        fieldPage.setFieldType(field.toString());
        fieldPage.setFieldTitle(field.toString());
        return fieldPage;
    }

    public static void createRegistrationFormWithCustomField(String text1, String value1,String text2, String value2, RegistrationField field) throws Exception {
        log4j.info("add custom field to the registration form");
        RegistrationFormPage formPage = buildRegistrationFormWithCustomField(text1,  value1, text2,  value2, field);
        Navigator.operationsTabRegistrationForm();
        fillSeleniumObject(formPage);
        waitForLoad();
    }

}




