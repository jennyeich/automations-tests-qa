package hub.hub1_0.common.objects.smarts.smarObjects.settings;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;
import hub.common.objects.smarts.smarObjects.settings.ISettings;
import hub.common.objects.smarts.smarObjects.triggers.BirthdayAnniversaryAutomationTriggers;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.ApplyIf;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Condition;

import java.util.ArrayList;

public class BirthdayAnniversarySettings extends BaseSeleniumObject implements ISettings {

    public static final String SMART_AUTOMATION_NAME = "name";
    public static final String BIRTHDAY_ANNIVERSARY_TRIGGER = "//select[@automationid=\"usersFieldsAutomations.fieldName\"]";
    public static final String DESCRIPTION = "description";
    public static final String RUN_EVERY_DAY_AT_HOURS = "//select[@automationid=\"cronExpression.hours\"]";
    public static final String RUN_EVERY_DAY_AT_MINUTES = "//select[@automationid=\"cronExpression.minutes\"]";
    int counter;

    @SeleniumPath(value = SMART_AUTOMATION_NAME, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = SMART_AUTOMATION_NAME)
    public String automationName;
    @SeleniumPath(value = BIRTHDAY_ANNIVERSARY_TRIGGER, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = BIRTHDAY_ANNIVERSARY_TRIGGER)
    BirthdayAnniversaryAutomationTriggers trigger;
    @SeleniumPath(value = DESCRIPTION, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = DESCRIPTION)
    public String description;

    @SeleniumPath(value = RUN_EVERY_DAY_AT_HOURS, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = RUN_EVERY_DAY_AT_HOURS)
    public String runEveryDayAtHours;
    @SeleniumPath(value = RUN_EVERY_DAY_AT_MINUTES, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = RUN_EVERY_DAY_AT_MINUTES)
    public String runEveryDayAtMinutes;

    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    ArrayList<Condition> conditions;




    public String getAutomationName() {return automationName;}
    public void setAutomationName(String automationName) {this.automationName = automationName;}

    public void setTrigger (ITrigger trigger){
        this.trigger = (BirthdayAnniversaryAutomationTriggers)trigger;
    }
    public ITrigger getTrigger (){return trigger;}

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRunEveryDayAtHours() {
        return runEveryDayAtHours;
    }

    public void setRunEveryDayAtHours(String runEveryDayAtHours) {
        this.runEveryDayAtHours = runEveryDayAtHours;
    }

    public String getRunEveryDayAtMinutes() {
        return runEveryDayAtMinutes;
    }

    public void setRunEveryDayAtMinutes(String runEveryDayAtMinutes) {
        this.runEveryDayAtMinutes = runEveryDayAtMinutes;
    }

    public ArrayList<Condition> getConditions() {
        if(conditions == null)
            conditions = new ArrayList<Condition>();
        return conditions;
    }

    public void addCondition(Condition condition, ICounter automation){

        condition.setNext(automation.getConditionCount());
        getConditions().add(condition);
        condition.setCreateActionCounter(getConditions().size());
        automation.setConditionCount(automation.getConditionCount()+1);

        if (condition.getApplyIf()== ApplyIf.SHOPPING_CART)
            automation.setShoppingCartAndCustomOccursCounter(automation.getShoppingCartAndCustomOccursCounter()+1);

    }

    public void setConditions(ArrayList<Condition> conditions) {this.conditions = conditions;}

    public ApplyLimitTimes getApplyLimitTimes() {
        return null;
    }
    public void setApplyLimitTimes(ApplyLimitTimes applyLimitTimes) {

    }
    public  String getNextTagsPath(int i){
        return "";
    }
    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
