package hub.hub1_0.common.objects.smarts.smarObjects.actions.smartGiftActions;

import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.AddDiscountAction;

/**
 * Created by Goni on 9/11/2017.
 */

public interface ISmartAssetAction {

    public void setNext (int i);
    public int getNext ();
    public void setCreateActionCounter(int createActionCounter);
    public void setDiscount(AddDiscountAction discount);
}
