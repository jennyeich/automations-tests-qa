package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Goni on 5/15/2017.
 */
public enum ExternalEventField implements IApplyField{

    //Look according to the label

    Type("Type"),
    Subtype("Subtype"),
    StringValue1("String Value 1"),
    StringValue2("String Value 2"),
    NumericValue1("Numeric Value 1"),
    NumericValue2("Numeric Value 2"),
    DateValue1("Date Value 1"),
    DateValue2("Date Value 2"),
    BooleanValue1("Boolean Value 1"),
    BooleanValue2("Boolean Value 2")
    ;

    private final String name;
    ExternalEventField(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
