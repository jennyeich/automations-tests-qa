package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by goni on 2/14/2017.
 */
public interface IApplyValue {

    String getValue();

    void setValue(String selectValue);
}
