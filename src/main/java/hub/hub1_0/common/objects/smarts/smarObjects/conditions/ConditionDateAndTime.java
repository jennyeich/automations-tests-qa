package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;

/**
 * Created by goni on 2/28/2017.
 */
public class ConditionDateAndTime extends BaseSeleniumObject implements Condition {

    public static final String ADD_CONDITION = "(//*[@ng-click=\"SACtrl.addCondition()\"])";
    public static final String CONDITION_NAME = "(//*[@automationid=\"condition_name\"])";
    public static final String CONDITION_APPLY_IF = "(//*[@ng-model=\"condition.paramsGroup\"])";

    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_GENERATE_NEXT,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    public static String  addCondition;
    @SeleniumPath(value = CONDITION_NAME, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = CONDITION_NAME)
    String name;
    @SeleniumPath(value = CONDITION_APPLY_IF, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = CONDITION_APPLY_IF)
    public ApplyIf applyIf;

    public DateAndTime dateAndTime;

    public int counter;
    public int createActionCounter;



    public String getName() {
        return name;
    }
    @Override
    public void setName(String name) {
        this.name = name;
    }

    public ApplyIf getApplyIf() {
        return applyIf;
    }
    public void setApplyIf( ApplyIf applyIf) {
        this.applyIf = applyIf;
    }
    public int getCreateActionCounter() {return createActionCounter;}
    public void setCreateActionCounter(int createActionCounter) {this.createActionCounter = createActionCounter;}
    public String getNextTagsPath (int i){return "";}
    public void setNext (int counter) {this.counter = counter;}
    public int getNext (){return counter;}

    public int getNextTag () {return 0;}
    public ConditionDateAndTime(ICounter automation){
        setNext(automation.getConditionValueCounter());
    }

    public DateAndTime getDateAndTime() {
        return dateAndTime;
    }

    public void setDateAndTime(DateAndTime dateAndTime) {
        this.dateAndTime = dateAndTime;
    }

}
