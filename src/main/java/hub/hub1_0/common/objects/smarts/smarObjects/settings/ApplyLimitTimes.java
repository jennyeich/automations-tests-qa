package hub.hub1_0.common.objects.smarts.smarObjects.settings;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Jenny on 11/6/2016.
 */
public class ApplyLimitTimes extends BaseSeleniumObject {


    public static final String  APPLY_LIMIT_TIMES = "//*[@ng-model=\"ctrlAEdit.item.applyLimitTimes\"]";
    public static final String  LIMITATION_TAG = "maxInvokesOfRulesPerTag_Tag";
    public static final String  PER_MEMBER_CHECKBOX =  "//*[@ng-model=\"ctrlAEdit.item.limitedTimesPerMember\"]";
    public static final String  PER_MEMBER_TIMES = "maxInvokesOfRulesPerTag";
    int counter;


    @SeleniumPath(value = APPLY_LIMIT_TIMES, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE,readValue = APPLY_LIMIT_TIMES)
    String  ApplyLimitTimes;
    @SeleniumPath(value = LIMITATION_TAG, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = LIMITATION_TAG)
    String limitationTag;
    @SeleniumPath(value = PER_MEMBER_CHECKBOX, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK,readValue = PER_MEMBER_CHECKBOX)
    String perMemberCheckBox;
    @SeleniumPath(value = PER_MEMBER_TIMES, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = PER_MEMBER_TIMES)
    String perMemberTimes;

    PerTimePeriod perTimePeriod;

    public String getApplyLimitTimes() {return ApplyLimitTimes;}
    public void setApplyLimitTimes(String applyLimitTimes) {ApplyLimitTimes = applyLimitTimes;}
    public String getLimitationTag() {return limitationTag;}
    public void setLimitationTag(String limitationTag) {this.limitationTag = limitationTag;}
    public String getPerMemberCheckBox() {return perMemberCheckBox;}
    public void setPerMemberCheckBox(String perMemberCheckBox) {this.perMemberCheckBox = perMemberCheckBox;}
    public String getPerMemberTimes() {return perMemberTimes;}
    public void setPerMemberTimes(String perMemberTimes) {this.perMemberTimes = perMemberTimes;}
    public PerTimePeriod getPerTimePeriod() {return perTimePeriod;}
    public void setPerTimePeriod(PerTimePeriod perTimePeriod) {this.perTimePeriod = perTimePeriod;}



    public  String getNextTagsPath(int i){
        return "";
    }
    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
