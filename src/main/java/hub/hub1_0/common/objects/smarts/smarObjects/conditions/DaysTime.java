package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by goni on 2/28/2017.
 */
public class DaysTime  extends BaseSeleniumObject {

    public static final String IS = "(//*[@ng-options=\"selectedDay.id as selectedDay.name for selectedDay in daysCtrl.daysArraySelect\"])";
    public static final String FROM_TIME = "(//*[@time-obj=\"day.fromTime\"]/div/input[@ng-model=\"timeInput.inlineModel.time\"])";
    public static final String FROM_TIME_PM ="(//*[@time-obj=\"day.fromTime\"]/div/span[@class=\"ampm ng-scope\"])";
    public static final String TO_TIME= "(//*[@time-obj=\"day.toTime\"]/div/input[@ng-model=\"timeInput.inlineModel.time\"])";
    public static final String TO_TIME_AM ="(//*[@time-obj=\"day.toTime\"]/div/span[@class=\"ampm ng-scope\"])";
    public static final String ADD_DAY = "(//*[@ng-click=\"daysCtrl.addDay()\"])";


    @SeleniumPath(value = IS, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = IS )
    String is;
    @SeleniumPath(value = FROM_TIME, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TIME,readValue = FROM_TIME)
    String fromTime;
    @SeleniumPath(value = FROM_TIME_PM, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK,readValue = FROM_TIME_PM)
    String fromTimePM;
    @SeleniumPath(value = TO_TIME, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TIME,readValue = TO_TIME)
    String toTime;
    @SeleniumPath(value = TO_TIME_AM, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK,readValue = TO_TIME_AM)
    String toTimeAM;
    @SeleniumPath(value = ADD_DAY, type = SeleniumPath.Type.BY_GENERATE_NEXT,elementInputType = SeleniumPath.ElementInputType.CLICK,readValue = ADD_DAY)
    String addDay;

    public int counter;
    public int createActionCounter;

    public String getAddDay() {
        return addDay;
    }

    public void setAddDay() {
        addDay="Y";
    }

    public int getCreateActionCounter() {return createActionCounter;}
    public void setCreateActionCounter(int i) {
        this.createActionCounter = i;
    }


    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    public void setNext (int i) {
        this.counter=i;

    }
    public int getNext (){return counter;}

    @Override
    public int getNextTag() {
        return 0;
    }
    public String getIs() {
        return is;
    }

    public void setIs(String is) {
        this.is = is;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getToTimeAM() {
        return toTimeAM;
    }

    public void setToTimeAM() {
        this.toTimeAM = "true";
    }


    public String getFromTimeAMPM() {
        return fromTimePM;
    }

    public void setFromTimePM() {
        this.fromTimePM = "true";
    }

    public String getToTime() {
        return toTime;
    }

    public void setToTime(String toTime) {
        this.toTime = toTime;
    }



}
