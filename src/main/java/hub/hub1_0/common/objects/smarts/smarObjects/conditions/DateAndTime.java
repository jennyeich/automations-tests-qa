package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;

/**
 * Created by goni on 2/28/2017.
 */
public class DateAndTime extends BaseSeleniumObject{

    public static final String FROM_DATE = "(//*[contains(@ng-model,\"fromDateTime\")]/div/div[1]/*[@name=\"datepicker\"])";
    public static final String UNTIL_DATE = "(//*[contains(@ng-model,\"toDateTime\")]/div/div[1]/*[@name=\"datepicker\"])";
    public static final String FROM_AND_TIME= "(//*[contains(@ng-model,\"fromDateTime\")]/div/div[2]/div/div/input[contains(@ng-model,\"time\")])";
    public static final String UNTIL_AND_TIME = "(//*[contains(@ng-model,\"toDateTime\")]/div/div[2]/div/div/input[contains(@ng-model,\"time\")])";
    public static final String FROM_AM = "(//*[contains(@ng-model,\"fromDateTime\")]/div/div[2]/div/div/*[@ng-if=\"timeInput.amMode\"])";
    public static final String UNTIL_AM = "(//*[contains(@ng-model,\"toDateTime\")]/div/div[2]/div/div/*[@ng-if=\"timeInput.amMode\"])";

    @SeleniumPath(value = FROM_DATE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.DATE,readValue = FROM_DATE)
    String fromDate;
    @SeleniumPath(value = UNTIL_DATE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.DATE,readValue = UNTIL_DATE)
    String untilDate;
    @SeleniumPath(value = FROM_AND_TIME, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TIME,readValue = FROM_AND_TIME)
    String fromTime;
    @SeleniumPath(value = UNTIL_AND_TIME, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TIME,readValue = UNTIL_AND_TIME)
    String untilTime;
    @SeleniumPath(value = FROM_AM, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK,readValue = FROM_AM)
    String fromAM;
    @SeleniumPath(value = UNTIL_AM, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK,readValue = UNTIL_AM)
    String untilAM;

    public int counter;

    public DateAndTime (ICounter automation){

        setNext(automation.getDateAndTimeConditionCounter());
        automation.setDateAndTimeConditionCounter(automation.getDateAndTimeConditionCounter() + 1);
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getUntilDate() {
        return untilDate;
    }

    public void setUntilDate(String untilDate) {
        this.untilDate = untilDate;
    }

    public String getFromTime() {
        return fromTime;
    }

    public void setFromTime(String fromTime) {
        this.fromTime = fromTime;
    }

    public String getUntilTime() {
        return untilTime;
    }

    public void setUntilTime(String untilTime) {
        this.untilTime = untilTime;
    }

    public String getFromAM() {
        return fromAM;
    }

    public void setFromAM() {
        this.fromAM = "true";
    }

    public String getUntilAM() {
        return untilAM;
    }

    public void setUntilAM() {
        this.untilAM = "true";
    }

    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    public void setNext (int counter) {this.counter = counter;}
    public int getNext (){return counter;}

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

}
