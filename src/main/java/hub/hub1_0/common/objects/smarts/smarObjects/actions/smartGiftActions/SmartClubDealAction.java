package hub.hub1_0.common.objects.smarts.smarObjects.actions.smartGiftActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.hub1_0.common.objects.benefits.smartGiftActions.dealItemCode.AddDealCodeClubDeal;
import hub.hub1_0.common.objects.benefits.smartGiftActions.dealItemCode.AddItemCodeClubDeal;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.discount.AddDiscountAction;

/**
 * Created by Goni on 9/11/2017.
 */

public class SmartClubDealAction extends BaseSeleniumObject implements ISmartAssetAction{

    int counter;
    public int createActionCounter;

    public static final String ADD_ACTION = "(//*[@ng-click=\"SACtrl.addAction()\"])";
    public static final String ACTION_NAME = "(//*[@automationid=\"action_name\"])";
    public static final String PERFORM_THE_ACTION = "(//select[@automationid=\"dropdownPerformAction\"])";

    @SeleniumPath(value = ADD_ACTION, type = SeleniumPath.Type.BY_GENERATE_NEXT,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private static String  addAction;
    @SeleniumPath(value = ACTION_NAME, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = ACTION_NAME)
    String actionName;
    @SeleniumPath(value = PERFORM_THE_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = PERFORM_THE_ACTION)
    private String performTheAction;


    private AddDiscountAction discount;
    private AddDealCodeClubDeal dealCode;
    private AddItemCodeClubDeal itemCode;

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public AddDealCodeClubDeal getDealCode() {
        return dealCode;
    }

    public void setDealCode(AddDealCodeClubDeal dealCode) {
        this.dealCode = dealCode;
    }

    public AddItemCodeClubDeal getItemCode() {
        return itemCode;
    }

    public void setItemCode(AddItemCodeClubDeal itemCode) {
        this.itemCode = itemCode;
    }

    public String getPerformTheAction() {return performTheAction;}
    public void setPerformTheAction(String performTheAction) {this.performTheAction = performTheAction;}
    public AddDiscountAction getDiscount() {
        return discount;
    }
    public void setDiscount(AddDiscountAction discount) {
        this.discount = discount;
    }

    public  String getNextTagsPath(int i){
        return "";
    }


    public int getCreateActionCounter() {return createActionCounter;}
    public void setCreateActionCounter(int createActionCounter) {this.createActionCounter = createActionCounter;}
    public void setNext (int counter) {this.counter = counter;}
    public int getNext (){return counter;}

    public int getNextTag () {return 0;}
}

