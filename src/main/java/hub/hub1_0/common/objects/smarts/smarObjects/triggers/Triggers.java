package hub.hub1_0.common.objects.smarts.smarObjects.triggers;

import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;

/**
 * Created by Jenny on 11/3/2016.
 */
public enum Triggers implements ITrigger {


    LaunchestheAppForTheFirstTime("Launches the app (1st time)"),
    JoinsTheClub("Joins the club"),
    PunchesAPunchCard("Punches a punch card"),
    SubmitAPurchase("Makes a purchase"),
    RedeemAnAsset("Redeems an asset"),
    AttemptRedeemAnAsset("Attempts to redeem an asset"),
    ReceivesAnAsset("Receives an asset"),
    ConnectsWithFaceBook("Connects with Facebook"),
    OpensTheApp("Opens the app"),
    SharesToFaceBook("Shares to Facebook"),
    ReceivesOrUsesPoints("Receives or uses points"),
    FillsOutAForm("Fills out a form"),
    UpdatesMembershipDetails("Updates membership details"),
    ClaimsAReward("Taps &quot;Claim&quot; button"),
    BeaconSignal("Receives a beacon signal"),
    ScansACode("Scans a code"),
    UnsubscribeFromTextMSG("Unsubscribes from SMS"),
    TagOrUntag("Tagged or untagged"),
    Unregisters("Unregisters"),
    OverflowsPunchCard("Overflows punch card"),
    ExternalEventSubmitted("External event submitted"),
    PaysWithComoWallet("Pays with Como Wallet"),
    PunchExceeded("Overflows punch card"),
    PurchasesAnAsset("Purchases an asset (Point Shop)");




    private final String name;
    Triggers(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }



    }




