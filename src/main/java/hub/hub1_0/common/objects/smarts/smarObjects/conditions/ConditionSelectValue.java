package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;

/**
 * Created by goni on 2/15/2017.
 */
public class ConditionSelectValue extends BaseSeleniumObject implements IApplyValue{

    public static final String CONDITION_SELECT_VALUE = "(//*[starts-with(@id,'smartAction_condition_value')]//select)";

    @SeleniumPath(value = CONDITION_SELECT_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = CONDITION_SELECT_VALUE)
    String selectValue;

    public int counter;

    public String getValue() {
        return selectValue;
    }

    public void setValue(String value) {
        this.selectValue = value;
    }

    public ConditionSelectValue (ICounter automation){

        setNext(automation.getConditionSelectValueCounter());
        automation.setConditionSelectValueCounter(automation.getConditionSelectValueCounter() + 1);
    }

    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    public void setNext (int counter) {this.counter = counter;}
    public int getNext (){return counter;}

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
