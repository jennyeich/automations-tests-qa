package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Jenny on 11/30/2016.
 */
public enum DiscountItems_ForEach {


    spend("spend"),
    quantity("quantity"),

    ;

    private final String name;
    DiscountItems_ForEach(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
