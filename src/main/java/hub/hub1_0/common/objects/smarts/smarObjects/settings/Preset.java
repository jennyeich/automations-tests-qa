package hub.hub1_0.common.objects.smarts.smarObjects.settings;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Jenny on 03/8/2017.
 */
public class Preset extends BaseSeleniumObject {


    public static final String  PRESET = "preset";
    public static final String  DELETE = "(//*[@class=\"ic_block_title_delete\"])[1]";


    @SeleniumPath(value = PRESET, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = PRESET)
    public String preset;
    @SeleniumPath(value = DELETE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_AND_APPROVE, readValue = DELETE)
    public String delete;


    public String getPreset() {return preset;}
    public void setPreset(String preset) {this.preset = preset;}
    public String getDelete() {return delete;}
    public void setDelete(String delete) {this.delete = delete;}

    @Override
    public void setNext(int i) {}
    @Override
    public int getNext() {
        return 0;
    }
    public int getNextTag () {return 0;}
    @Override
    public int getCreateActionCounter() {
        return 0;
    }
    public  String getNextTagsPath(int i){
        return "";
    }

}


