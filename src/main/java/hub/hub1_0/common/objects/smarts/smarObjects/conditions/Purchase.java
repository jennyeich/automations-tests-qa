package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Jenny on 11/24/2016.
 */
public enum Purchase implements IApplyField {

    //Look according to the label
    NumOfMembers("Num of Members"),
    TotalSum("Total Sum"),
    Tags("Tags"),
    BranchID("Branch ID"),
    POSID("POS ID"),
    Employee("Employee")
    ;

    private final String name;
    Purchase(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }





}
