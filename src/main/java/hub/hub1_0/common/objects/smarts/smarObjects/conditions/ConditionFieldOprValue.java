package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by goni on 2/28/2017.
 */
public class ConditionFieldOprValue extends BaseSeleniumObject implements Condition {

    public static final String ADD_CONDITION = "(//*[@ng-click=\"SACtrl.addCondition()\"])";
    public static final String CONDITION_NAME = "(//*[@automationid=\"condition_name\"])";
    public static final String CONDITION_APPLY_IF = "(//*[@ng-model=\"condition.paramsGroup\"])";

    public static final String CONDITION_FIELD= "(//*[@ng-model=\"SACPCtrl.ngModel.param\"])";
    public static final String CONDITION_OPERATOR = "(//*[@ng-model=\"SACPCtrl.ngModel.operator\"])";




    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_GENERATE_NEXT,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    public static String  addCondition;
    @SeleniumPath(value = CONDITION_NAME, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = CONDITION_NAME)
    String name;
    @SeleniumPath(value = CONDITION_APPLY_IF, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = CONDITION_APPLY_IF)
    public ApplyIf applyIf;
    @SeleniumPath(value = CONDITION_FIELD, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = CONDITION_FIELD)
    String field;
    @SeleniumPath(value = CONDITION_OPERATOR, type = SeleniumPath.Type.BY_OPERATOR_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_XPATH,readValue = CONDITION_OPERATOR)
    String operator;


    public int counter;
    public int createActionCounter;

    public ConditionValue conditionValue;
    public ConditionSelectValue conditionSelectValue;
    public ConditionDateValue conditionDateValue;
    public ConditionTagValue conditionTagValue;
    public ConditionTextAreaValue conditionTextAreaValue;

    public Group group;


    public ConditionFieldOprValue(){}

    public String getName() {
        return name;
    }
    @Override
    public void setName(String name) {
        this.name = name;
    }

    public ApplyIf getApplyIf() {
        return applyIf;
    }
    public void setApplyIf( ApplyIf applyIf) {
        this.applyIf = applyIf;
    }
    public int getCreateActionCounter() {return createActionCounter;}
    public void setCreateActionCounter(int createActionCounter) {this.createActionCounter = createActionCounter;}
    public String getNextTagsPath (int i){return "";}
    public void setNext (int counter) {this.counter = counter;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}
    public Group getGroup() {return group;}
    public void setGroup(Group group) {this.group = group;}

    public String getField() {
        return field;
    }
    public void setField(String field) {
        this.field = field;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public ConditionValue getConditionValue() {
        return conditionValue;
    }

    public void setConditionValue(ConditionValue conditionValue) {
        this.conditionValue = conditionValue;
    }

    public ConditionTagValue getConditionTagValue() {
        return conditionTagValue;
    }

    public void setConditionTagValue(ConditionTagValue conditionTagValue) {
        this.conditionTagValue = conditionTagValue;
    }

    public ConditionSelectValue getConditionSelectValue() {
        return conditionSelectValue;
    }

    public void setConditionSelectValue(ConditionSelectValue conditionSelectValue) {
        this.conditionSelectValue = conditionSelectValue;
    }

    public ConditionDateValue getConditionDateValue() {
        return conditionDateValue;
    }

    public void setConditionDateValue(ConditionDateValue conditionDateValue) {
        this.conditionDateValue = conditionDateValue;
    }

    public ConditionTextAreaValue getConditionTextAreaValue() {
        return conditionTextAreaValue;
    }

    public void setConditionTextAreaValue(ConditionTextAreaValue conditionTextAreaValue) {
        this.conditionTextAreaValue = conditionTextAreaValue;
    }
}
