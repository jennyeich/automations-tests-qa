package hub.hub1_0.common.objects.smarts.smarObjects.settings;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;
import hub.common.objects.smarts.smarObjects.settings.ISettings;
import hub.common.objects.smarts.smarObjects.triggers.ITrigger;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.ApplyIf;
import hub.hub1_0.common.objects.smarts.smarObjects.conditions.Condition;
import hub.hub1_0.common.objects.smarts.smarObjects.triggers.Triggers;


import java.util.ArrayList;

/**
 * Created by Jenny on 11/3/2016.
 */
public class Settings extends BaseSeleniumObject implements ISettings{


    public static final String  SMART_AUTOMATION_NAME = "name";
    public static final String TRIGGER = "trigger";
    int counter;

    @SeleniumPath(value = SMART_AUTOMATION_NAME, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = SMART_AUTOMATION_NAME)
    public String automationName;
    @SeleniumPath(value = TRIGGER, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = TRIGGER)
    Triggers trigger;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    ArrayList<Condition> conditions;

    ApplyLimitTimes applyLimitTimes;
    Preset preset;


    public String getAutomationName() {return automationName;}
    public void setAutomationName(String automationName) {this.automationName = automationName;}
    public ApplyLimitTimes getApplyLimitTimes() {
        return applyLimitTimes;
    }
    public void setApplyLimitTimes(ApplyLimitTimes applyLimitTimes) {
        this.applyLimitTimes = applyLimitTimes;
    }
    public void setTrigger (ITrigger trigger){
        this.trigger = (Triggers) trigger;
    }
    public ITrigger getTrigger (){
        return trigger;
    }
    public Preset getPreset() {return preset;}
    public void setPreset(Preset preset) {this.preset = preset;}

    public ArrayList<Condition> getConditions() {
        if(conditions == null)
            conditions = new ArrayList<Condition>();
        return conditions;
    }

    public void addCondition(Condition condition, ICounter automation){

        condition.setNext(automation.getConditionCount());
        getConditions().add(condition);
        condition.setCreateActionCounter(getConditions().size());
        automation.setConditionCount(automation.getConditionCount()+1);

        if (condition.getApplyIf()== ApplyIf.SHOPPING_CART)
            automation.setShoppingCartAndCustomOccursCounter(automation.getShoppingCartAndCustomOccursCounter()+1);

    }

    public void setConditions(ArrayList<Condition> conditions) {this.conditions = conditions;}
    public  String getNextTagsPath(int i){
        return "";
    }
    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}


