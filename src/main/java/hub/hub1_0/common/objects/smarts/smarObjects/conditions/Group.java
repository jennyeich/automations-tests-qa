package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;

import java.util.ArrayList;

/**
 * Created by Jenny on 11/24/2016.
 */
public class Group extends BaseSeleniumObject {


    int counter;
    public int createActionCounter;

    public static final String CREATE_NEW_GROUP = "(//*[@ng-click=\"CConditionCtrl.startEditingCalcCondition(true);\"])";
    public static final String GROUP_NAME = "newCalcConditionTitle";
    public static final String IN_THE_GROUP = "(//*[@id=\"calcConditionId_\"])";

    public static final String SAVE = "(//button[@type=\"submit\"])[2]";

    @SeleniumPath(value = CREATE_NEW_GROUP, type = SeleniumPath.Type.BY_GENERATE_NEXT,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    String createNew;
    @SeleniumPath(value = GROUP_NAME, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = GROUP_NAME)
    String groupName;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    ArrayList<IGroupCondition> groupConditionsconditions;
    @SeleniumPath(value = SAVE, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    String save;

    @SeleniumPath(value = IN_THE_GROUP, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = IN_THE_GROUP)
    private String inTheGroup;

    public String getInTheGroup() {return inTheGroup;}

    public void setInTheGroup(String inTheGroup) {this.inTheGroup = inTheGroup;}

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
        this.setInTheGroup(groupName);
    }
    public String getNextTagsPath (int i){
        return "";
    }
    public int getNextTag () {return 0;}
    public void setCreateActionCounter(int createActionCounter) {this.createActionCounter = createActionCounter;}
    public int getCreateActionCounter() {return createActionCounter;}

    public void setNext (int i) {this.counter = i;}
    public int getNext (){return counter;}
    public Group (ICounter automation){
        setNext(automation.getShoppingCartAndCustomOccursCounter());
        setCreateActionCounter(automation.getShoppingCartAndCustomOccursCounter());
    }


    public void addCondition(ICounter automaiton, IGroupCondition groupCondition){

        getConditions().add(groupCondition);
        groupCondition.setNext(getConditions().size());

    }

    private ArrayList<IGroupCondition> getConditions() {
        if(groupConditionsconditions == null)
            groupConditionsconditions = new ArrayList<>();
        return groupConditionsconditions;
    }

    public void setConditions(ArrayList<IGroupCondition> conditions) {this.groupConditionsconditions = conditions;}


}
