package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by goni on 2/14/2017.
 */
public enum UpdateMemberOperationValue{
    //Look according to the label

    JoinTheClub("join club"),
    UpdateMembership("update membership")
    ;

    private final String name;
    UpdateMemberOperationValue(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
