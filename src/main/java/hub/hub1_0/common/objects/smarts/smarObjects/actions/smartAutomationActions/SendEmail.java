package hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Occurrences;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Timing;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;

import java.util.ArrayList;

public class SendEmail extends BaseSeleniumObject implements ISmartAction {

    public static final String CHOOSE_TEMPLATE = "(//*[@automationid =\"mailTemplateSelect\"])";
    public static final String SUBJECT = "(//*[@automationid =\"mailTemplateSubject\"])";
    public static final String FROM = "(//*[@automationid =\"mailTemplateFrom\"])";
    public static final String TAGS = "(//*[@ng-model=\"newTag.text\"])";
    public static final String TIMING = "(//*[@automationid =\"dropdownSmartActionTimingTypes\"])";


    @SeleniumPath(value = CHOOSE_TEMPLATE, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = CHOOSE_TEMPLATE)
    private String templateName;
    @SeleniumPath(value = SUBJECT, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = SUBJECT)
    private String subject;
    @SeleniumPath(value = FROM, type = SeleniumPath.Type.BY_GENERATE_XPATH, elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = FROM)
    private String from;
    @SeleniumPath(value = TAGS, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = TAGS)
    private ArrayList<String> tags;

    Timing timing;
    public int tagCounter;
    public int counter;

    public SendEmail(ISmartAutomationCounter automation){
        setNext(automation.getSendEmailCount());
        automation.setSendEmailCount(automation.getSendEmailCount()+ 1);
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateName() {
        return templateName;
    }

    public String getSubject() {
        return subject;
    }

    public String getFrom() {
        return from;
    }

    @Override
    public String getNextTagsPath(int i) { return ""; }

    @Override
    public void setNext(int counter) { this.counter = counter; }

    @Override
    public int getNext() { return counter;}

    @Override
    public int getNextTag() { return tagCounter;}

    public void setNextTag (int i) {this.tagCounter = i;}

    @Override
    public int getCreateActionCounter() { return 0;}

    @Override
    public void setTiming(Timing timing) { this.timing = timing;}

    @Override
    public void setOccurrences(Occurrences occurrences) { }

    @Override
    public void setTags(ArrayList<String> tags, ICounter automation) {
        this.tags = tags;
        setNextTag(automation.getTagsCounter());
        automation.setTagsCounter(automation.getTagsCounter()+1);
    }
}
