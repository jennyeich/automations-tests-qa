package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Goni on 6/25/2017.
 */
public enum PaymentTypeValue {

    //Look according to the label
    Points("Points"),
    Credit("Credit"),
    GiftCard("Gift Card"),
    ExternalPayment("External Payment")
    ;

    private final String name;
    PaymentTypeValue(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
