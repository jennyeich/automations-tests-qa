package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Jenny on 11/24/2016.
 */
public enum GroupField implements IApplyField{

    //Use label

    ItemCode("Item Code"),
    DepartmentCode("Department Code"),
    DepartmentName("Department Name"),
    ItemName("Item Name"),
    ItemPrice("Item Price"),
    ItemTag("Item Tag")
    ;

    private final String name;
    GroupField(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }



}
