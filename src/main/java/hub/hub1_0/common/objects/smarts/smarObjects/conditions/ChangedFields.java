package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by goni on 2/14/2017.
 */
public enum ChangedFields implements IApplyField {

    //Look according to the label

    FirstName("First Name"),
    LastName("Last Name"),
    PhoneNumber("Phone Number"),
    Email("Email"),
    Gender("Gender"),
    Birthday("Birthday"),
    ExpirationDate("ExpirationDate")
    ;

    private final String name;
    ChangedFields(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
