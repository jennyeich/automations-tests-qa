package hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Occurrences;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Timing;

import java.util.ArrayList;

/**
 * Created by Goni on 6/22/2017.
 */
public class SendPopUpMessage extends BaseSeleniumObject implements ISmartAction {

    public static final String TITLE = "(//input[@id=\"SendMemberMessage_Title\"])";
    public static final String TEXT_MESSAGE = "(//*[@id=\"SendMemberMessage_Content\"]/textarea[@ng-model=\"ment.mentionString\"])";
    public static final String CHK_INBOX = "(//input[@id=\"SendMemberMessage_Tags_inbox\"])";
    public static final String CHK_AFTER_CODE_BASE_ACTION = "(//input[@id=\"SendMemberMessage_Tags_afterCodeBasedAction\"])";
    public static final String CHK_AFTER_REDEEM = "(//input[@id=\"SendMemberMessage_Tags_afterRedeem\"])";
    public static final String CHK_NAVIGATE_HOME_SCREEN = "(//input[@id=\"SendMemberMessage_Tags_openMainLocaitonTC\"])";
    public static final String CHK_AFTER_PUNCH = "(//input[@id=\"SendMemberMessage_Tags_afterPunch\"])";
    public static final String CHK_OPEN_APP = "(//input[@id=\"SendMemberMessage_Tags_popup\"])";
    public  int counter;
    public  int tagCounter;


    @SeleniumPath(value = TITLE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TITLE)
    String title;
    @SeleniumPath(value = TEXT_MESSAGE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TEXT_AREA, readValue = TEXT_MESSAGE)
    String txtMessage;
    @SeleniumPath(value = CHK_INBOX, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = "")
    String chkAInbox;
    @SeleniumPath(value = CHK_AFTER_CODE_BASE_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = "")
    String chkAfterCodeBaseAction;
    @SeleniumPath(value = CHK_AFTER_REDEEM, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = "")
    String chkAfterRedeem;
    @SeleniumPath(value = CHK_NAVIGATE_HOME_SCREEN, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = "")
    String chkNavigateHome;
    @SeleniumPath(value = CHK_AFTER_PUNCH, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = "")
    String chkAfterPunch;
    @SeleniumPath(value = CHK_OPEN_APP, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = "")
    String chkOpenApp;
    Timing timing;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTxtMessage() {
        return txtMessage;
    }
    public void setTxtMessage(String txtMessage) {
        this.txtMessage = txtMessage;
    }
    public String getChkAfterPunch() {
        return chkAfterPunch;
    }

    public void setChkAfterPunch(String chkAfterPunch) {
        this.chkAfterPunch = chkAfterPunch;
    }
    public String getChkAInbox() {
        return chkAInbox;
    }

    public void setChkAInbox(String chkAInbox) {
        this.chkAInbox = chkAInbox;
    }

    public String getChkAfterCodeBaseAction() {
        return chkAfterCodeBaseAction;
    }

    public void setChkAfterCodeBaseAction(String chkAfterCodeBaseAction) {
        this.chkAfterCodeBaseAction = chkAfterCodeBaseAction;
    }

    public String getChkAfterRedeem() {
        return chkAfterRedeem;
    }

    public void setChkAfterRedeem(String chkAfterRedeem) {
        this.chkAfterRedeem = chkAfterRedeem;
    }

    public String getChkNavigateHome() {
        return chkNavigateHome;
    }

    public void setChkNavigateHome(String chkNavigateHome) {
        this.chkNavigateHome = chkNavigateHome;
    }

    public String getChkOpenApp() {
        return chkOpenApp;
    }

    public void setChkOpenApp(String chkOpenApp) {
        this.chkOpenApp = chkOpenApp;
    }
    public void setNextTag (int i) {this.tagCounter = i;}
    public int getNextTag () {return tagCounter;}
    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter = i;}
    public int getNext (){return counter;}

    public SendPopUpMessage(ISmartAutomationCounter automation){
        setNext(automation.getSendPopUpMessageCount());
        automation.setSendPopUpMessageCount(automation.getSendPopUpMessageCount()+ 1);
    }

    public Timing getTiming() {
        return timing;
    }

    public void setTiming(Timing timing) {
        this.timing = timing;
    }

    public void setOccurrences(Occurrences occurrences) {}

    //no need to be implemented
    public void setTags(ArrayList<String> tags, ICounter automation){}
    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}


