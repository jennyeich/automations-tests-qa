package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.base.BaseSeleniumObject;
import hub.common.objects.smarts.*;
import hub.common.objects.smarts.ISmartAssetCounter;
import hub.common.objects.smarts.SmartAutomation;

/**
 * Created by goni on 2/15/2017.
 */
public class StringOperator extends BaseSeleniumObject implements IOperator {



    public enum String_Equals_enum{

        EqualsTo("(//option[@label=\"=\"])"),
        NotEqualto("(//option[@label=\"≠\"])"),
        Contains("(//option[@label=\"Contains\"])"),
        Isnotoneof("(//option[@label=\"Is not one of\"])"),
        DoesntContain("(//option[@label=\"Doesn't contain\"])"),
        IsOneOf("(//option[@label=\"Is one of\"])")
        ;

        private final String name;
        String_Equals_enum(final String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    private String operatorGeneratedPath;
    public int counter;

    public String getOperator() {
        return operatorGeneratedPath;
    }

    public void setOperator(ISmartAutomationCounter automation, String operatorValue) {
        if(String_Equals_enum.EqualsTo.toString().equals(operatorValue) || String_Equals_enum.NotEqualto.toString().equals(operatorValue)){
            this.operatorGeneratedPath =  operatorValue + "[" + automation.getConditionEqualOperatorCounter() + "]";
        }else {
            this.operatorGeneratedPath = operatorValue + "[" + automation.getConditionStringOperatorCounter() + "]";
        }
        automation.setConditionStringOperatorCounter(automation.getConditionStringOperatorCounter() + 1);
        automation.setConditionEqualOperatorCounter(automation.getConditionEqualOperatorCounter() + 1);
    }


    public void setBirthdayAnniversaryOperator(SmartBirthdayAndAnniversaryAutomation smartBirthdayAndAnniversaryAutomation, String operatorValue) {
        if(String_Equals_enum.EqualsTo.toString().equals(operatorValue) || String_Equals_enum.NotEqualto.toString().equals(operatorValue)){
            this.operatorGeneratedPath =  operatorValue + "[" + smartBirthdayAndAnniversaryAutomation.getConditionEqualOperatorCounter() + "]";
        }else {
            this.operatorGeneratedPath = operatorValue + "[" + smartBirthdayAndAnniversaryAutomation.getConditionStringOperatorCounter() + "]";
        }
        smartBirthdayAndAnniversaryAutomation.setConditionStringOperatorCounter(smartBirthdayAndAnniversaryAutomation.getConditionStringOperatorCounter() + 1);
        smartBirthdayAndAnniversaryAutomation.setConditionEqualOperatorCounter(smartBirthdayAndAnniversaryAutomation.getConditionEqualOperatorCounter() + 1);
    }

    public void setAssetOperator(ISmartAssetCounter smartAsset, String operatorValue) {
        if(String_Equals_enum.EqualsTo.toString().equals(operatorValue) || String_Equals_enum.NotEqualto.toString().equals(operatorValue)){
            this.operatorGeneratedPath =  operatorValue + "[" + smartAsset.getConditionEqualOperatorCounter() + "]";
        }else {
            this.operatorGeneratedPath = operatorValue + "[" + smartAsset.getConditionStringOperatorCounter() + "]";
        }
        smartAsset.setConditionStringOperatorCounter(smartAsset.getConditionStringOperatorCounter() + 1);
        smartAsset.setConditionEqualOperatorCounter(smartAsset.getConditionEqualOperatorCounter() + 1);
    }


    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    public void setNext (int i) {this.counter = i;}
    public int getNext (){return counter;}

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }


}
