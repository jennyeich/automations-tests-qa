package hub.hub1_0.common.objects.smarts.smarObjects.actions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Jenny on 12/15/2016.
 */
public class PurchaseGroupBasket extends BaseSeleniumObject {

    int counter;
    public static final String CREATE_NEW = "(//span[@ng-click=\"CConditionCtrl.startEditingCalcCondition(true);\"])[1]";


    @SeleniumPath(value = CREATE_NEW, type = SeleniumPath.Type.BY_XPATH, elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE, readValue = CREATE_NEW)
    String createNew;


    public String getNextTagsPath (int i){return  "";}
    public void setNext (int i) {this.counter=i;}
    public int getNext (){return counter;}
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
