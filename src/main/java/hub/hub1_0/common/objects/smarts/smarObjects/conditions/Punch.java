package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Jenny on 11/23/2016.
 */
public enum Punch implements IApplyField {

    //Look according to the label

    LastPunch("Last Punch"),
    TotalPunchesAfterOperation("Total Punches After Operation"),
    ;

    private final String name;
    Punch(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
