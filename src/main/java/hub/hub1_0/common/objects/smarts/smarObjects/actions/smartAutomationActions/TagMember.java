package hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Occurrences;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Timing;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;

import java.util.ArrayList;

/**
 * Created by Jenny on 12/20/2016.
 */
public class TagMember extends BaseSeleniumObject implements ISmartAction {

    public static final String TAG_FIELD = "(//input[starts-with(@id,'tagMember_tag')])";
    public static final String TAGS = "(//*[@ng-model=\"newTag.text\"])";
    public static final String TAG_MEMBER = "(//*[@value=\"Tag\"])";
    public static final String UN_TAG_MEMBER = "(//*[@value=\"UnTag\"])";

    public int counter;
    public  int tagCounter;


    @SeleniumPath(value = TAG_FIELD, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = TAG_FIELD)
    String tagText;
    @SeleniumPath(value = TAG_MEMBER, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = TAG_MEMBER)
    String tag_member;
    @SeleniumPath(value = UN_TAG_MEMBER, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = UN_TAG_MEMBER)
    String untag_member;
    @SeleniumPath(value = TAGS, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = TAGS)
    private ArrayList<String> tags;

    Timing timing;

    public String getTagText() {return tagText;}
    public void setTagText(String tagText) {this.tagText = tagText;}
    public ArrayList<String> getTags() {return tags;}
    public String getTag_member() {return tag_member;}
    public void setTag_member(String tag_member) {this.tag_member = tag_member;}
    public String getUntag_member() {return untag_member;}
    public void setUntag_member(String untag_member) {this.untag_member = untag_member;}
    public void setNextTag (int i) {this.tagCounter = i;}
    public int getNextTag () {return tagCounter;}
    public String getNextTagsPath (int i){return "";}
    public void setNext (int counter) {this.counter = counter;}
    public int getNext (){return counter;}
    public TagMember(ISmartAutomationCounter automation){
        setNext(automation.getTagMemberCount());
        automation.setTagMemberCount(automation.getTagMemberCount() + 1);
    }

    public Timing getTiming() {
        return timing;
    }

    @Override
    public void setTiming(Timing timing) {
        this.timing = timing;
    }

    @Override
    public void setOccurrences(Occurrences occurrences) {
        //do nothing
    }

    public void setTags(ArrayList<String> tags, ICounter automation) {
        this.tags = tags;
        setNextTag(automation.getTagsCounter());
        automation.setTagsCounter(automation.getTagsCounter()+1);
    }


    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
