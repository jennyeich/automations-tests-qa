package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.base.BaseSeleniumObject;
import hub.common.objects.smarts.ISmartAssetCounter;
import hub.common.objects.smarts.ISmartAutomationCounter;

/**
 * Created by goni on 2/21/2017.
 */
public class DateOperator extends BaseSeleniumObject implements IOperator {

    public enum Date_Equals_enum{

        EqualsTo("(//option[@label='='])"),
        NotEqualto("(//option[@label=\"≠\"])"),
        IsBefore("(//option[@label=\"<\"])"),
        IsBeforeOrEqualsTo("(//option[@label=\"≤\"])"),
        IsAfterOrEqualsTo("(//option[@label=\"≥\"])"),
        IsAfter("(//option[@label='>'])")
        ;

        private final String name;
        Date_Equals_enum(final String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }


    private String operatorGeneratedPath;
    public int counter;

    public String getOperator() {
        return operatorGeneratedPath;
    }

    public void setOperator(ISmartAutomationCounter automation, String operatorValue) {
        if(DateOperator.Date_Equals_enum.EqualsTo.toString().equals(operatorValue) || DateOperator.Date_Equals_enum.NotEqualto.toString().equals(operatorValue)){
            this.operatorGeneratedPath =  operatorValue + "[" + automation.getConditionEqualOperatorCounter() + "]";
        }else {
            this.operatorGeneratedPath = operatorValue + "[" + automation.getConditionDateOperatorCounter() + "]";
        }

        automation.setConditionDateOperatorCounter(automation.getConditionDateOperatorCounter() + 1);
        automation.setConditionEqualOperatorCounter(automation.getConditionEqualOperatorCounter() + 1);
    }

    public void setAssetOperator(ISmartAssetCounter smartAsset, String operatorValue){
        if(DateOperator.Date_Equals_enum.EqualsTo.toString().equals(operatorValue) || DateOperator.Date_Equals_enum.NotEqualto.toString().equals(operatorValue)){
            this.operatorGeneratedPath = operatorValue + "[" + smartAsset.getConditionEqualOperatorCounter() + "]";
        }else {
            this.operatorGeneratedPath = operatorValue + "[" + smartAsset.getConditionDateOperatorCounter() + "]";
        }

        smartAsset.setConditionDateOperatorCounter(smartAsset.getConditionDateOperatorCounter() + 1);
        smartAsset.setConditionEqualOperatorCounter(smartAsset.getConditionEqualOperatorCounter() + 1);

    };


    @Override
    public String getNextTagsPath(int i) {
        return operatorGeneratedPath;
    }

    public void setNext (int i) {this.counter = i;}
    public int getNext (){return counter;}

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }


}


