package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Jenny on 11/23/2016.
 */
public enum Membership implements IApplyField{

    //Look according to the label

    FirstNAme("First Name"),
    LastName("Last Name"),
    PhoneNumber("Phone Number"),
    Email("Email"),
    Gender("Gender"),
    Birthday("Birthday"),
    ExpirationDate("ExpirationDate"),
    Tags("Tags"),
    Anniversary("Anniversary")
    ;

    private final String name;
    Membership(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
