package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Goni on 5/14/2017.
 */
public enum PunchcardOverflow implements IApplyField {

    //Look according to the label

    NumOfPunches("# of Punches"),
    Asset("Asset"),
    ;

    private final String name;
    PunchcardOverflow(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
