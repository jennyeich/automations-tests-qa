package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Jenny on 11/23/2016.
 */
@Deprecated
public enum AssetSourceValue {

    //Look according to the label

    FindFilterMember("Find/Filter Members"),
    Automation("Automation"),
    PointsShop("Points Shop")
    ;

    private final String name;
    AssetSourceValue(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
