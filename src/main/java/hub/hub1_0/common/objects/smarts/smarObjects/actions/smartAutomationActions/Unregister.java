package hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Occurrences;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Timing;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;

import java.util.ArrayList;

/**
 * Created by Jenny on 12/20/2016.
 */
public class Unregister extends BaseSeleniumObject implements ISmartAction {

    public static final String TAGS = "(//*[@ng-model=\"newTag.text\"])";
    public int counter;
    public  int tagCounter;

    @SeleniumPath(value = TAGS, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = TAGS)
    private ArrayList<String> tags;

    Timing timing;

    public ArrayList<String> getTags() {return tags;}

    public void setNextTag (int i) {this.tagCounter = i;}
    public int getNextTag () {return tagCounter;}
    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter = i;}
    public int getNext (){return counter;}
    public Unregister(ISmartAutomationCounter automation){

        setNext(automation.getUnregisterCount());
        automation.setUnregisterCount(automation.getUnregisterCount() + 1);
    }

    public Timing getTiming() {
        return timing;
    }

    @Override
    public void setTiming(Timing timing) {
        this.timing = timing;
    }

    @Override
    public void setOccurrences(Occurrences occurrences) {

    }

    public void setTags(ArrayList<String> tags, ICounter automation) {
        this.tags = tags;
        setNextTag(automation.getTagsCounter());
        automation.setTagsCounter(automation.getTagsCounter()+1);
    }


    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
