package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;

/**
 * Created by goni on 2/15/2017.
 */
public class ConditionTagValue extends BaseSeleniumObject implements IApplyValue{

    public static final String CONDITION_TAG_VALUE = "(//*[@ng-model=\"ArrayFieldCtrl.ngModel\"])";


    @SeleniumPath(value = CONDITION_TAG_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = CONDITION_TAG_VALUE)
    String tagValue;



    public int counter;
    public String getValue() {
        return tagValue;
    }
    public void setValue(String value) {
        this.tagValue = value;
    }

    public ConditionTagValue (ICounter automation){

        setNext(automation.getConditionTagValueCounter());
        automation.setConditionTagValueCounter(automation.getConditionTagValueCounter() + 1);
    }

    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    public void setNext (int counter) {this.counter = counter;}
    public int getNext (){return counter;}

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
