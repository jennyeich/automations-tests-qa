package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.base.BaseSeleniumObject;
import hub.common.objects.smarts.ISmartAssetCounter;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.SmartAutomation;

/**
 * Created by goni on 2/15/2017.
 */
public class TagOperator extends BaseSeleniumObject implements IOperator {

    public enum Tags_Equals_enum{

        ContainsOneOf("(//option[@label='Contains one of'])"),
        DoesntContainAny("(//option[@label=\"Doesn't contain any\"])"),
        ContainsAll("(//option[@label='Contains all'])")
        ;

        private final String name;
        Tags_Equals_enum(final String name) {
            this.name = name;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    private String operatorGeneratedPath;
    public int counter;

    public String getOperator() {
        return operatorGeneratedPath;
    }

    public void setOperator(ISmartAutomationCounter automation, String operatorValue) {
        this.operatorGeneratedPath =  operatorValue + "[" + automation.getConditionTagOperatorCounter() + "]";
        //setNext(automation.getConditionTagOperatorCounter());
        automation.setConditionTagOperatorCounter(automation.getConditionTagOperatorCounter() + 1);
    }

    public void setAssetOperator(ISmartAssetCounter smartAsset, String operatorValue) {
        this.operatorGeneratedPath =  operatorValue + "[" + smartAsset.getConditionTagOperatorCounter() + "]";
        //setNext(automation.getConditionTagOperatorCounter());
        smartAsset.setConditionTagOperatorCounter(smartAsset.getConditionTagOperatorCounter() + 1);
    }


    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    public void setNext (int i) {this.counter = i;}
    public int getNext (){return counter;}

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }

    public void setOperator(ISmartAssetCounter smartAsset, String operatorValue){};

}

