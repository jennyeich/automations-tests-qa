package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;

/**
 * Created by Jenny on 3/6/2017.
 */
public class GroupCondition extends BaseSeleniumObject implements IGroupCondition{


    int counter;
    public int createActionCounter;

    public static final String ADD_CONDITION = "//*[@ng-click=\"CConditionPopupCtrl.addCondition()\"]";
    public static final String CONDITION_FIELD= "(//*[@ng-submit=\"CConditionPopupCtrl.saveCalcCondition()\"]//*[@class=\"form-element\"]/*[@ng-model=\"SACPCtrl.ngModel.param\"])";
    public static final String CONDITION_EQUALS = "(//*[@ng-submit=\"CConditionPopupCtrl.saveCalcCondition()\"]//*[@ng-model=\"SACPCtrl.ngModel.operator\"])";
    public static final String CONDITION_VALUE = "(//*[@ng-submit=\"CConditionPopupCtrl.saveCalcCondition()\"]//input[starts-with(@id,'smartAction_condition_value')])";


    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private static String  addCondition;
    @SeleniumPath(value = CONDITION_FIELD, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = CONDITION_FIELD)
    String field;
    @SeleniumPath(value = CONDITION_EQUALS, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = CONDITION_EQUALS)
    String equals;
    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = CONDITION_VALUE)
    String value;


    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    public String getEquals() {
        return equals;
    }
    public void setEquals(String equals) {
        this.equals = equals;
    }
    public String getField() {
        return field;
    }
    public void setField(String field) {
        this.field = field;
    }
    public String getNextTagsPath (int i){
        return "";
    }
    public int getNextTag () {return 0;}
    public void setCreateActionCounter(int createActionCounter) {this.createActionCounter = createActionCounter;}
    public int getCreateActionCounter() {return createActionCounter;}
    public void setNext (int i) {this.counter = i;}
    public int getNext (){return counter;}



}
