package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Jenny on 11/23/2016.
 */
public enum Asset implements IApplyField {

    //Look according to the label

    Asset("Asset"),
    /*AssetSource("Asset Source"),*/
    AutomationName("Asset Source - Automation Name")
    ;

    private final String name;
    Asset(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
