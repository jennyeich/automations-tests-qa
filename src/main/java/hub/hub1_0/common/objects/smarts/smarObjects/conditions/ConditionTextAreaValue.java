package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;

/**
 * Created by Goni on 5/9/2017.
 */
public class ConditionTextAreaValue extends BaseSeleniumObject implements IApplyValue{

    public static final String CONDITION_VALUE = "(//*[starts-with(@id,'smartAction_condition_value')]/textarea)";

    @SeleniumPath(value = CONDITION_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = CONDITION_VALUE)
    String value;

    public int counter;


    public ConditionTextAreaValue (ICounter automation){

        setNext(automation.getConditionValueCounter());
        automation.setConditionValueCounter(automation.getConditionValueCounter() + 1);
    }
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    public void setNext (int counter) {this.counter = counter;}

    public int getNext (){return counter;}

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
