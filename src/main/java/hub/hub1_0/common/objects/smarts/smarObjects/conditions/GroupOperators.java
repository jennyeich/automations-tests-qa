package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Jenny on 11/23/2016.
 */
public enum GroupOperators {


    Equals("="),
    NotEquals("≠"),
    Contains("Contains"),
    Isoneof("Is one of"),
    DoesntContain("Doesn\'t contain"),
    IsNotOneOf("Is not one of"),
    ContainsOneOf("Contains one of"),
    DoesntContainOneOf("Doesn't contain one of"),
    ContainsAll("Contains all")
    ;

    private final String name;
    GroupOperators(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
