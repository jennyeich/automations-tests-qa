package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;

/**
 * Created by goni on 2/21/2017.
 */
public class ConditionDateValue extends BaseSeleniumObject implements  IApplyValue {


    public static final String CONDITION_DATE_VALUE = "(//*[@id=\"datePicker\"])";


    @SeleniumPath(value = CONDITION_DATE_VALUE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.DATE,readValue = CONDITION_DATE_VALUE)
    String dateValue;

    public int counter;
    public String getValue() {
        return dateValue;
    }
    public void setValue(String value) {
        this.dateValue = value;
    }

    public ConditionDateValue (ICounter automation){

        setNext(automation.getConditionDateValueCounter());
        automation.setConditionDateValueCounter(automation.getConditionDateValueCounter() + 1);
    }

    @Override
    public String getNextTagsPath(int i) {
        return "";
    }

    public void setNext (int counter) {this.counter = counter;}
    public int getNext (){return counter;}

    @Override
    public int getNextTag() {
        return 0;
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
