package hub.hub1_0.common.objects.smarts.smarObjects.settings;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.common.Automation_TimeUnits;

/**
 * Created by Jenny on 11/6/2016.
 */

public class PerTimePeriod extends BaseSeleniumObject {


    public static final String  PER_TIME_PERIOD_CHECKBOX =  "//*[@ng-model=\"ctrlAEdit.item.limitedTimesPerTimePeriod\"]";
    public static final String  MAX_OCCURANCES_PER_TIME_PERIOD = "MaxInvokesPerTagPerTimeframe";
    public static final String  MAX_OCCURANCES_TIME_PERIOD_NUMBER = "//*[@ng-model=\"TSCtrl.valueField\"]";
    public static final String  MAX_OCCURANCES_TIME_PERIOD_UNITS = "//*[@ng-model=\"TSCtrl.unitsField\"]";


    @SeleniumPath(value = PER_TIME_PERIOD_CHECKBOX, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE,readValue = PER_TIME_PERIOD_CHECKBOX)
    String getPerTimePeriodCheckbox;
    @SeleniumPath(value = MAX_OCCURANCES_TIME_PERIOD_UNITS, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT,readValue = MAX_OCCURANCES_TIME_PERIOD_UNITS)
    Automation_TimeUnits timePeriodUnits;
    @SeleniumPath(value = MAX_OCCURANCES_PER_TIME_PERIOD, type = SeleniumPath.Type.BY_ID,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = MAX_OCCURANCES_PER_TIME_PERIOD)
    String maxOccurrencesPerTimePeriod ;
    @SeleniumPath(value = MAX_OCCURANCES_TIME_PERIOD_NUMBER, type = SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue = MAX_OCCURANCES_TIME_PERIOD_NUMBER)
    String  timePeriodNumber;

    public String getMaxOccurrencesPerTimePeriod() {return maxOccurrencesPerTimePeriod;}
    public void setMaxOccurrencesPerTimePeriod(String maxOccurrencesPerTimePeriod) {this.maxOccurrencesPerTimePeriod = maxOccurrencesPerTimePeriod;}
    public String getTimePeriodNumber() {return timePeriodNumber;}
    public void setTimePeriodNumber(String timePeriodNumber) {this.timePeriodNumber = timePeriodNumber;}
    public Automation_TimeUnits getTimePeriodUnits() {return timePeriodUnits;}
    public void setTimePeriodUnits(Automation_TimeUnits timePeriodUnits) {this.timePeriodUnits = timePeriodUnits;}
    public  String getNextTagsPath(int i){
        return "";
    }

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
