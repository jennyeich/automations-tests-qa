package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.common.objects.smarts.ISmartAssetCounter;
import hub.common.objects.smarts.ISmartAutomationCounter;

/**
 * Created by goni on 2/8/2017.
 */
public interface IOperator {

    public String getOperator();

    public abstract void setOperator(ISmartAutomationCounter automation, String operatorValue);

    public abstract void setAssetOperator(ISmartAssetCounter smartAsset, String operatorValue);
}
