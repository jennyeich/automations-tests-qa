package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Jenny on 11/23/2016.
 */
public enum ApplyIf {

    MEMBERSHIP("Membership"),
    PURCHASE("Purchase"),
    SHOPPING_CART("Shopping Cart"),
    DATETIME("Date & Time"),
    DAYSTIME("Days & Times"),
    ASSET("Asset"),
    POINTS("Points"),
    UPDATE_MEMBERSHIP_OPERATION("Update membership operation"),
    CHANGED_FIELDS("Changed fields"),
    TAG_OPERATION("Tag operation"),
    ACTION_TAGS("Action Tags"),
    PUNCH("Punch"),
    PUNCHCARD_OVERFLOW("Punch Card Overflow"),
    EXTERNAL_EVENT("External Event"),
    WALLET_PAYMENT("Wallet Payment");
    ;

    private final String name;
    ApplyIf(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
