package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by goni on 2/6/2017.
 */
public enum Points implements IApplyField {

    //Look according to the label
    TotalSumValue("Total Sum Value"),
    Quantity("Quantity")
    ;

    private final String name;
    Points(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
