package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Goni on 3/1/2017.
 */
public class ConditionDaysTime extends BaseSeleniumObject implements Condition {

    public static final String ADD_CONDITION = "(//*[@ng-click=\"SACtrl.addCondition()\"])";
    public static final String CONDITION_NAME = "(//*[@automationid=\"condition_name\"])";
    public static final String CONDITION_APPLY_IF = "(//*[@ng-model=\"condition.paramsGroup\"])";


    @SeleniumPath(value = ADD_CONDITION, type = SeleniumPath.Type.BY_GENERATE_NEXT,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    public static String  addCondition;
    @SeleniumPath(value = CONDITION_NAME, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = CONDITION_NAME)
    String name;
    @SeleniumPath(value = CONDITION_APPLY_IF, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL,readValue = CONDITION_APPLY_IF)
    public ApplyIf applyIf;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY, readValue = "READ")
    public List<DaysTime> daysTimeList;

    public int counter;
    public int createActionCounter;
    int daysTimeCounter;

    public String getName() {
        return name;
    }
    @Override
    public void setName(String name) {
        this.name = name;
    }

    public int getDaysTimeCounter() {
        return daysTimeCounter;
    }

    public void setDaysTimeCounter(int daysTimeCounter) {
        this.daysTimeCounter = daysTimeCounter;
    }



    private List<DaysTime> getDaysTimeList() {
        if(daysTimeList == null)
            daysTimeList = new ArrayList<>();
        return daysTimeList;
    }

    public void setDaysTimeList(ArrayList<DaysTime> daysTimeList) {this.daysTimeList = daysTimeList;}

    public ApplyIf getApplyIf() {
        return applyIf;
    }
    public void setApplyIf( ApplyIf applyIf) {
        this.applyIf = applyIf;
    }

    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {
       this.counter = i;
    }
    public int getNext (){return counter;}

    public int getNextTag () {return 0;}

    public int getCreateActionCounter() {return createActionCounter;}
    public void setCreateActionCounter(int createActionCounter) {this.createActionCounter = createActionCounter;}

    public ConditionDaysTime(ICounter automation){
        setCreateActionCounter(automation.getDaysTimeConditionCounter());
        automation.setDaysTimeConditionCounter(automation.getDaysTimeConditionCounter()+1);
    }

    public void addDaysTime(DaysTime daysTime,ICounter automation){
        daysTime.setNext(automation.getDaysTimeCounter());
        getDaysTimeList().add(daysTime);
        int size = getDaysTimeList().size();
        if( size > 1){
            //if the days list is more than 1 - we set the addDay to true on the day before the last added
            (daysTimeList.get(size-2)).setAddDay();
        }
        daysTime.setCreateActionCounter(getCreateActionCounter());
        automation.setDaysTimeCounter(automation.getDaysTimeCounter() + 1);
    }
}
