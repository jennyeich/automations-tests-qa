package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Goni on 3/6/2017.
 */
public enum TagOperationValue {

    //Look according to the label
    Tag("Tag"),
    Untag("Untag")
;

    private final String name;
    TagOperationValue(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
