package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Jenny on 11/24/2016.
 */
public enum ShoppingCart implements IApplyField {

    TotalQuantity("Total Quantity"),
    TotalPrice("Total Sum");

    private final String name;
    ShoppingCart(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
