package hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions;

import hub.common.objects.smarts.ICounter;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Occurrences;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Timing;

import java.util.ArrayList;

/**
 * Created by Goni on 5/21/2017.
 */
public interface ISmartAction {

    public void setTiming(Timing timing);

    public void setOccurrences(Occurrences occurrences);

    public void setTags(ArrayList<String> tags, ICounter automation);
}
