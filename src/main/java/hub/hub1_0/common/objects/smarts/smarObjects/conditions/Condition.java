package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Jenny on 11/1/2016.
 */
public interface Condition {

    public void setName(String name);
    public ApplyIf getApplyIf();
    public void setApplyIf( ApplyIf applyIf);
    public int getCreateActionCounter();
    public void setCreateActionCounter(int createActionCounter) ;
    public void setNext (int counter);
    public int getNext ();


}
