package hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.*;

/**
 * Created by Jenny on 11/1/2016.
 */
public class SmartAutomationAction extends BaseSeleniumObject{

    int counter;
    public int createActionCounter;

    public static final String SCENARIOS_TAB = "//div[@ng-click=\"ctrlAEdit.setCurrentTab('scenarios');\"]";
    public static final String ADD_ACTION = "(//*[@ng-click=\"SACtrl.addAction()\"])";
    public static final String PERFORM_THE_ACTION = "(//*[@ng-model=\"SAOUCtrl.inlineModel.ActionType\"])";
    public static final String ACTION_NAME = "(//*[@automationid=\"action_name\"])";

    @SeleniumPath(value = ADD_ACTION, type = SeleniumPath.Type.BY_GENERATE_NEXT,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE)
    private static String addAction;
    @SeleniumPath(value = ACTION_NAME, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.INPUT, readValue = ACTION_NAME)
    String actionName;
    @SeleniumPath(value = PERFORM_THE_ACTION, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.SELECT_THROUGH_LABEL, readValue = PERFORM_THE_ACTION)
    String performTheAction;


    SendAsset sendAsset;
    AddPoints addPoints;
    SendLotteryReward lotteryReward;
    PunchAPunchCard punchAPunchCard;
    TagMember tagMember;
    SendSMS sendSMS;
    Unregister unregister;
    ExportEvent exportEvent;
    Register register;
    SendPushNotification pushNotification;
    SendPopUpMessage sendPopUpMessage;
    SendEmail sendEmail;

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getPerformTheAction() {return performTheAction;}
    public void setPerformTheAction(String performTheAction) {this.performTheAction = performTheAction;}
    public SendAsset getSendAsset() {return sendAsset;}
    public void setSendAsset(SendAsset sendAsset) {this.sendAsset = sendAsset;}
    public AddPoints getAddPoints() {return addPoints;}
    public void setAddPoints(AddPoints addPoints) {this.addPoints = addPoints;}
    public SendLotteryReward getLotteryReward() {return lotteryReward;}
    public void setLotteryReward(SendLotteryReward lotteryReward) {this.lotteryReward = lotteryReward;}
    public PunchAPunchCard getPunchAPunchCard() {return punchAPunchCard;}
    public void setPunchAPunchCard(PunchAPunchCard punchAPunchCard) {this.punchAPunchCard = punchAPunchCard;}
    public TagMember getTagMember() {return tagMember;}
    public void setTagMember(TagMember tagMember) {this.tagMember = tagMember;}
    public SendSMS getSendSMS() {return sendSMS;}
    public void setSendSMS(SendSMS sendSMS) {this.sendSMS = sendSMS;}
    public void setSendEmail(SendEmail sendEmail) {this.sendEmail = sendEmail; }
    public SendEmail getSendEmail() {return sendEmail; }
    public Unregister getUnregister() {return unregister;}
    public void setUnregister(Unregister unregister) {this.unregister = unregister;}
    public ExportEvent getExportEvent() {return exportEvent;}
    public void setExportEvent(ExportEvent exportEvent) {this.exportEvent = exportEvent;}
    public Register getRegister() {return register;}
    public void setRegister(Register register) {this.register = register;}
    public SendPushNotification getPushNotification() {
        return pushNotification;
    }
    public void setPushNotification(SendPushNotification pushNotification) {
        this.pushNotification = pushNotification;
    }
    public SendPopUpMessage getSendPopUpMessage() {
        return sendPopUpMessage;
    }
    public void setSendPopUpMessage(SendPopUpMessage sendPopUpMessage) {
        this.sendPopUpMessage = sendPopUpMessage;
    }
    public int getCreateActionCounter() {return createActionCounter;}
    public void setCreateActionCounter(int createActionCounter) {this.createActionCounter = createActionCounter;}



    public  String getNextTagsPath(int i){
        return "";
    }
    public void setNext (int counter) {this.counter = counter;}
    public int getNext (){return counter;}

    public int getNextTag () {return 0;}

}
