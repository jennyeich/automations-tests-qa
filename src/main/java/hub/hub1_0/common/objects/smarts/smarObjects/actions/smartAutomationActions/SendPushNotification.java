package hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.ICounter;
import hub.common.objects.smarts.ISmartAutomationCounter;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Occurrences;
import hub.common.objects.smarts.smarObjects.actions.smartAutomationActions.Timing;
import hub.hub1_0.common.objects.smarts.smarObjects.actions.smartAutomationActions.ISmartAction;

import java.util.ArrayList;

/**
 * Created by Goni on 6/20/2017.
 */
public class SendPushNotification extends BaseSeleniumObject  implements ISmartAction {

    public static final String TEXT_MESSAGE = "(//*[starts-with(@id,\"sendPersonalPush_message_\")]/div/div/mention-container/textarea)";
    public static final String REFRESH_ASSET_CHK ="(//*[contains(text(),\" Refresh Assets List?\")])";
    public static final String TAGS = "(//*[@ng-model=\"newTag.text\"])";
    public  int counter;
    public  int tagCounter;


    @SeleniumPath(value = TEXT_MESSAGE, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TEXT_AREA, readValue = TEXT_MESSAGE)
    String txtMessage;
    @SeleniumPath(value = REFRESH_ASSET_CHK, type = SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK, readValue = REFRESH_ASSET_CHK)
    String refreshAssetChk;
    @SeleniumPath(value = TAGS, type=SeleniumPath.Type.BY_GENERATE_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS,readValue = TAGS)
    private ArrayList<String> tags;

    Timing timing;

    public String getTxtMessage() {
        return txtMessage;
    }
    public void setTxtMessage(String txtMessage) {
        this.txtMessage = txtMessage;
    }
    public String getRefreshAssetChk() {
        return refreshAssetChk;
    }
    public void setRefreshAssetChk(String refreshAssetChk) {
        this.refreshAssetChk = refreshAssetChk;
    }
    public ArrayList<String> getTags() {return tags;}
    public void setNextTag (int i) {this.tagCounter = i;}
    public int getNextTag () {return tagCounter;}
    public String getNextTagsPath (int i){return "";}
    public void setNext (int i) {this.counter = i;}
    public int getNext (){return counter;}

    public SendPushNotification(ISmartAutomationCounter automation){
        setNext(automation.getSendPushNotificationCount());
        automation.setSendPushNotificationCount(automation.getSendPushNotificationCount()+ 1);
    }

    public Timing getTiming() {
        return timing;
    }

    public void setTiming(Timing timing) {
        this.timing = timing;
    }

    public void setOccurrences(Occurrences occurrences) {}

    public void setTags(ArrayList<String> tags, ICounter automation) {
        this.tags = tags;
        setNextTag(automation.getTagsCounter());
        automation.setTagsCounter(automation.getTagsCounter()+1);
    }

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}

