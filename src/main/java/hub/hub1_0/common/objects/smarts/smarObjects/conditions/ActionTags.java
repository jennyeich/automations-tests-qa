package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Goni on 3/6/2017.
 */
public enum ActionTags implements IApplyField{

    //Look according to the label
    ActionTags("Action Tags")
    ;

    private final String name;
    ActionTags(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
