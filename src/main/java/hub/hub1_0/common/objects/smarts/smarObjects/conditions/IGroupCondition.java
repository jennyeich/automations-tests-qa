package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Goni on 1/7/2018.
 */

public interface IGroupCondition {
    String getValue();
    void setValue(String value) ;
    String getEquals();
    void setEquals(String equals);
    String getField();
    void setField(String field);
    public int getCreateActionCounter();
    public void setCreateActionCounter(int createActionCounter) ;
    public void setNext (int counter);
    public int getNext ();
}
