package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Goni on 6/25/2017.
 */
public enum WalletPayment implements IApplyField {

    //Look according to the label
    PaymentType("Payment Type"),
    ExternalProvider("External Provider"),
    RequestedSum("Requested Sum"),
    ActualCharge("Actual Charge");

    private final String name;
    WalletPayment(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
