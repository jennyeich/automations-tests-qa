package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by goni on 2/14/2017.
 */
public enum UpdateMembershipOperation implements IApplyField {

    //Look according to the label
    Operation("Operation"),
    ;

    private final String name;
    UpdateMembershipOperation(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
