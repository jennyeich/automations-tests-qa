package hub.hub1_0.common.objects.smarts.smarObjects.conditions;

/**
 * Created by Goni on 3/6/2017.
 */
public enum TagOperation implements IApplyField {

    //Look according to the label
    Operation("Operation"),
    Tag("Tag"),
    ;

    private final String name;
    TagOperation(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
