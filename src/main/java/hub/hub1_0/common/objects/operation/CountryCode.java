package hub.hub1_0.common.objects.operation;

/**
 * Created by Jenny on 02/28/2018.
 */
public enum CountryCode {

    Israel("972 - Israel"),
    Brazil("55 - Brazil"),
    Chile("56 - Chile"),
    Colombila("57 - Colombia"),
    Denmark("45 - Denmark"),
    Italy("39 - Italy"),
    Kazakstan("7 - Kazakstan"),
    Mexico("52 - Mexico"),
    Panama("507 - Panama"),
    SouthAfrika("27 - South Africa"),
    Sweden("46 - Sweden"),
    Turkey("90 - Turkey"),
    UnitedArabEmirates("971 - United Arab Emirates"),
    UnitedKingdom("44 - United Kingdom"),
    UnitedStates("1 - United States");
    ;

    private final String name;
    CountryCode(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
