package hub.hub1_0.common.objects.benefits.smartGiftActions;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.benefits.BaseAssetFields;
import hub.common.objects.common.TimeUnits;

/**
 * Created by Goni on 9/6/2017.
 */

public class ClubDeal  extends BaseSeleniumObject {


    public static final String SAVE = "save";
    public static final String TITLE = "title";
    public static final String ITEM_SHORT_NAME = "shortName";
    public static final String DESCRIPTION = "description";
    public static final String CHOOSE_IMAGE = "images";
    public static final String VALUE = "value";
    public static final String COST = "cost";
    public static final String REQUIRES_EMPLOYEE_CODE_TO_REDEEM = "requiresEmployeeCode";
    public static final String PUSH_NOTIFICATION_MESSAGE  = "mention";
    public static final String VALID_FROM = "validFrom";
    public static final String VALID_UNTIL  = "//*[@id=\"newItem\"]/div[2]/div[3]/div/div/div/div/div[1]/input[@name=\"datepicker\"]";


    BaseAssetFields baseAssetFields;

    @SeleniumPath(value = TITLE, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =TITLE)
    private String title;
    @SeleniumPath(value = ITEM_SHORT_NAME, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =ITEM_SHORT_NAME)
    private String shortName;
    @SeleniumPath(value = DESCRIPTION, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =DESCRIPTION)
    private String description;
    @SeleniumPath(value = CHOOSE_IMAGE, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CHOOSE_IMAGE,readValue =CHOOSE_IMAGE)
    private String chooseImage;
    @SeleniumPath(value = VALUE, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =VALUE)
    private String value;
    @SeleniumPath(value = COST, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =COST)
    private String cost;
    @SeleniumPath(value = REQUIRES_EMPLOYEE_CODE_TO_REDEEM, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.SELECT,readValue =REQUIRES_EMPLOYEE_CODE_TO_REDEEM)
    private String requireEmployeeCode;
    @SeleniumPath(value = PUSH_NOTIFICATION_MESSAGE, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.INPUT,readValue =PUSH_NOTIFICATION_MESSAGE)
    private String pushNotificationMessage;
    @SeleniumPath(value = VALID_FROM, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.DATE,readValue =VALID_FROM)
    private String validFrom;
    @SeleniumPath(value = VALID_UNTIL, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.DATE,readValue =VALID_UNTIL)
    private String validUntil;
    @SeleniumPath(value = SAVE, type=SeleniumPath.Type.BY_AUTOMATION_ID,elementInputType = SeleniumPath.ElementInputType.CLICK_FORCE,readValue =SAVE)
    private String saveButton;

    public void setTitle(String title){
        this.title = title;
    }
    public String getTitle(){
        return title;
    }
    public void setShortName(String shortName){
        this.shortName = shortName;
    }
    public String getShortName(){
        return shortName;
    }
    public void setDescription(String description){
        this.description = description;
    }
    public String getDescription(){
        return description;
    }
    public void setChooseImage(boolean choose){
        if(choose) chooseImage = "";
        else chooseImage = null;
    }
    public void setValue(String value){
        this.value = value;
    }
    public String getValue(){
        return value;
    }
    public void setCost(String cost){
        this.cost = cost;
    }
    public String getCost(){
        return cost;
    }
    public void setRequierEmployeeCode(boolean require){
        if(require) requireEmployeeCode = "YES";
        else requireEmployeeCode = "NO";
    }
    public String getRequierEmployeeCode(){
        return requireEmployeeCode;
    }
    public void setPushNotification(String pushNotification){
        this.pushNotificationMessage = pushNotification;
    }
    public String getPushNotification(){
        return pushNotificationMessage;
    }
    public void setValidFrom(String validFrom) {
        this.validFrom = validFrom;
    }
    public String getValidFrom() {
        return validFrom;
    }
    public void setValidUntil(String validUntil) {
        this.validUntil = validUntil;
    }
    public String getValidUntil() {
        return validUntil;
    }
    public BaseAssetFields getBaseAssetFields() {return baseAssetFields;}
    public void setBaseAssetFields(BaseAssetFields baseAssetFields) {this.baseAssetFields = baseAssetFields;}

    public String getNextTagsPath (int i){
        return  "";
    }

    @Override
    public void setNext(int i) {
    }

    @Override
    public int getNext() {
        return 0;
    }
    public int getNextTag () {
        return 0;
    }

    @Override
    public int getCreateActionCounter()
    {
        return 0;
    }
}

