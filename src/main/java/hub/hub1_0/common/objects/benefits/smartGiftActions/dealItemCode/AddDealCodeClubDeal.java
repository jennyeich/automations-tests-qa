package hub.hub1_0.common.objects.benefits.smartGiftActions.dealItemCode;

import hub.base.BaseSeleniumObject;
import hub.base.SeleniumPath;
import hub.common.objects.smarts.smarObjects.actions.smartGiftActions.dealItemCode.RelatedDealCode;

import java.util.ArrayList;

/**
 * Created by Jenny on 12/1/2016.
 */
public class AddDealCodeClubDeal extends BaseSeleniumObject{

    public static final String READ_DEAL_CODES = "(//*[@ng-model=\"newTag.text\"])[2]";
    public static final String DEFINE_RELATED_ITEM = "//*[@ng-model=\"addDealCodesCtrl.ngModel.dealCodes.defineRelatedItemCheckBox\"]";
    public static final String DEAL_CODES = "(//*[@ng-model=\"newTag.text\"])[2]";

    @SeleniumPath(value = DEAL_CODES, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.TAGS, readValue = READ_DEAL_CODES)
    private ArrayList<String> dealCodes;
    @SeleniumPath(value = DEFINE_RELATED_ITEM, type=SeleniumPath.Type.BY_XPATH,elementInputType = SeleniumPath.ElementInputType.CLICK)
    private String defineRelatedItem;
    @SeleniumPath(value = "", type=SeleniumPath.Type.NONE,elementInputType = SeleniumPath.ElementInputType.OBJECT_ARRAY)
    ArrayList <RelatedDealCode> relatedRelatedDealCodes;


    public ArrayList<String> getDealCodes() {
        if(dealCodes == null)
            dealCodes = new ArrayList<>();
        return dealCodes;
    }
    public void setDealCodes(ArrayList<String> dealCodes) {this.dealCodes = dealCodes;}
    public String getDefineRelatedItem() {return defineRelatedItem;}
    public void setDefineRelatedItem(String defineRelatedItem) {this.defineRelatedItem = defineRelatedItem;}
    public ArrayList<RelatedDealCode> getRelatedRelatedDealCodes() {

        if(relatedRelatedDealCodes == null)
            relatedRelatedDealCodes = new ArrayList<>();
        return relatedRelatedDealCodes;

    }
    public void setRelatedRelatedDealCodes(ArrayList<RelatedDealCode> relatedRelatedDealCodes) {this.relatedRelatedDealCodes = relatedRelatedDealCodes;}

    public String getNextTagsPath (int i){return  "";}

    @Override
    public void setNext(int i) {

    }

    @Override
    public int getNext() {
        return 0;
    }
    public int getNextTag () {return 0;}

    @Override
    public int getCreateActionCounter() {
        return 0;
    }
}
