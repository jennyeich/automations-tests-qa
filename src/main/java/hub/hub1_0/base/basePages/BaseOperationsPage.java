package hub.hub1_0.base.basePages;

import org.openqa.selenium.By;

public class BaseOperationsPage {

    //automations
    public static final By AutopilotModePage = By.xpath("//*[@automationid='auto_pilot']");

    //Members
    public static final By New_Members = By.xpath("//*[@automationid=\"newuser\"]");


    //Smart birthday/anniversary automations
    public static final By Birthday_Automations_menu = By.xpath("//*[@automationid=\"usersFieldsAutomations\"]");

    //Smart automations
    public static final By Automations_tab = By.xpath("//*[@automationid='automation']");
    public static final By Smart_Automations_tab = By.id("nav-item-automations");
    public static final By Add_New_SmartAutomation = By.xpath("//a[contains(text(),\"Add New\")]");


    //settings
    public static final By Settings = By.xpath("//span[@automationid=\"settings_main\"]");
    public static final By Settings_pos_settings = By.id("nav-item-pos_settings");
    public static final By Settings_point_budget_settings = By.id("nav-item-point/budget_definitions");
    public static final By Settings_app_settings = By.id("nav-item-app_settings");
    public static final By Settings_external_settings = By.id("nav-item-external_services");
    public static final By Settings_permission_settings = By.id("nav-item-permissionsSettings");

    //Registration
    public static final By Registration = By.xpath("//span[contains(text(),'Registration')]");
    public static final By Registration_registration_form = By.xpath("//a[@automationid=\"registerForm\"]");
    public static final By Registration_joiningCodes = By.xpath("//*[@automationid=\"joinclub_codes\"]");

}
