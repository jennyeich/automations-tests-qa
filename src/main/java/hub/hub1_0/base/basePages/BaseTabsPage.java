package hub.hub1_0.base.basePages;

import hub.hub2_0.Hub2Constants;
import org.openqa.selenium.By;


public class BaseTabsPage {

    // Default locators for Hub1 and Hub2.
    public static final By TAB_DATA_BI = By.xpath("//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"navigationTab_dashboard\"]");
    public static final By TAB_CONTENT = By.xpath("//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"navigationTab_content\"]");
    public static final By TAB_BENEFITS = By.xpath("//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"navigationTab_assets\"]");
    public static final By TAB_OPERATION = By.xpath("//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"navigationTab_operations\"]");
    public static final By TAB_ACP = By.xpath("//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"navigationTab_acp\"]");
    public static final By TAB_LOYALTY_CENTER = By.xpath("//*[@" + Hub2Constants.HUB2_AUTOMATION_STR + "=\"navigationTab_businessCenter\"]");
    public static final By APP_DROPDOWN = By.xpath("//*[@id='header']/div[3]/ul[2]/li/span");
}
