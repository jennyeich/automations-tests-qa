package hub.hub1_0.base.basePages;

import hub.base.BaseService;
import org.openqa.selenium.WebDriver;

/**
 * Created by doron on 04/05/2017.
 */
public class BaseView {

    protected WebDriver _driver;

    public BaseView(){
        _driver = BaseService.getDriver();
        BaseService.waitForLoad();
    }
}
