package utils;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * This class is used to validate the date pattern and value in the Date fields of v4 Membership json object.
 * The format that is used in the v4 APIs is "yyyy-MM-dd".
 */
public class CustomDateOnlyDeserializer extends JsonDeserializer<Date> {
    private static final String datePattern = "yyyy-MM-dd";

    @Override

    public Date deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {

        JsonToken currentToken = jp.currentToken();

        if (currentToken == JsonToken.VALUE_STRING) {
            String text = jp.getText().trim();

            if (text.matches("\\d{4}-[01]\\d-[0-3]\\d")) {

                SimpleDateFormat df = new SimpleDateFormat(datePattern);
                df.setLenient(false);
                try {
                    return df.parse(text);
                } catch (ParseException e) {
                    // TODO - throw a custom runtime exception - this is important!
                    throw ctxt.weirdStringException(text, Date.class, "date format is not valid for string");
                }
            } else {
                throw ctxt.weirdStringException(text, Date.class, String.format("date format does not match the date pattern %s - value",datePattern));
            }
        } else if (currentToken == JsonToken.VALUE_NULL) {
            return getNullValue(ctxt);
        }

        ctxt.reportMappingException("Date Mapping exception");
        return null;
    }

    @Override
    public Date getNullValue(DeserializationContext ctxt) {
        return null;
    }


}