package com.como.rc

import com.google.appengine.api.datastore.*

class ReadOnlyDatastoreService constructor(val delegate: DatastoreService) : DatastoreService {

    override fun get(p0: Key?): Entity {
        return delegate.get(p0)
    }

    override fun get(p0: MutableIterable<Key>?): MutableMap<Key, Entity>? {
        return delegate.get(p0)
    }

    override fun prepare(p0: Query?): PreparedQuery {
        return delegate.prepare(p0)
    }

    override fun allocateIds(p0: String?, p1: Long): KeyRange {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun allocateIds(p0: Key?, p1: String?, p2: Long): KeyRange {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun put(p0: Entity?): Key {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun put(p0: Transaction?, p1: Entity?): Key {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun put(p0: MutableIterable<Entity>?): MutableList<Key>? {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun put(p0: Transaction?, p1: MutableIterable<Entity>?): MutableList<Key>? {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun allocateIdRange(p0: KeyRange?): DatastoreService.KeyRangeState {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun beginTransaction(): Transaction {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun beginTransaction(p0: TransactionOptions?): Transaction {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun getDatastoreAttributes(): DatastoreAttributes {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun getIndexes(): MutableMap<Index, Index.IndexState>? {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun get(p0: Transaction?, p1: Key?): Entity {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun get(p0: Transaction?, p1: MutableIterable<Key>?): MutableMap<Key, Entity>? {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun delete(vararg p0: Key?) {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun delete(p0: Transaction?, vararg p1: Key?) {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun delete(p0: MutableIterable<Key>?) {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun delete(p0: Transaction?, p1: MutableIterable<Key>?) {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun prepare(p0: Transaction?, p1: Query?): PreparedQuery {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun getCurrentTransaction(): Transaction {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun getCurrentTransaction(p0: Transaction?): Transaction {
        throw UnsupportedOperationException("Operation is forbidden")
    }

    override fun getActiveTransactions(): MutableCollection<Transaction>? {
        throw UnsupportedOperationException("Operation is forbidden")
    }

}