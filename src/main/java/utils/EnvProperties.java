package utils;

/**
 * Created by Goni on 3/23/2017.
 */
public class EnvProperties {

    private String apiKey;
    private String locationId;
    private String serverToken;
    private String applicationName;
    private boolean isNewApplication;
    private boolean inSuite;

    private static EnvProperties instance = null;

    private EnvProperties() {
    }

    public static EnvProperties getInstance() {
        if(instance == null) {
            instance = new EnvProperties();
        }
        return instance;
    }

    public boolean isNewApplication() {
        return isNewApplication;
    }

    public void setIsNewApplication(boolean newApplication) {
        isNewApplication = newApplication;
    }

    public boolean isInSuite() {
        return inSuite;
    }

    public void setInSuite(boolean inSuite) {
        this.inSuite = inSuite;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getServerToken() {
        return serverToken;
    }

    public void setServerToken(String serverToken) {
        this.serverToken = serverToken;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }
}
