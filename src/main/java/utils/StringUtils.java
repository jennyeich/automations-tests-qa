package utils;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * Created by israel on 30/01/2017.
 */

public class StringUtils {

    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static String Compress(String input) throws IOException {
        ByteArrayOutputStream obj = new ByteArrayOutputStream();
        GZIPOutputStream gzip;
        gzip = new GZIPOutputStream(obj);
        gzip.write(input.getBytes("UTF-8")); //TODO: move to config
        gzip.close();
        return Base64.internalBased64Encode(obj.toByteArray());
    }

    public static String Decompress(String input) throws IOException {
        byte[] bytes = Base64.internalBased64DecodeToBytes(input);
        GZIPInputStream gis = new GZIPInputStream(new ByteArrayInputStream(bytes));

        BufferedReader bf = new BufferedReader(new InputStreamReader(gis, "UTF-8")); //TODO: move to config
        String outStr = "";
        String line;
        while ((line=bf.readLine())!=null) {
            outStr += line;
        }

        return outStr;
    }

    public static String KeyToString(Key key, boolean nullSafe) {    // TODO: do this better with template from configuration
        if (key == null && nullSafe)
            return null;

        String result = String.format("%s:%s_%d", key.getNamespace(), key.getKind(), key.getId());
        return Base64.encode(result);
    }

    public static String KeyToString(Key key) {
        return KeyToString(key, false);
    }

    public static List<String> KeyToString(List<Key> keys) {
        return KeyToString(keys, false);
    }

    public static List<String> KeyToString(List<Key> keys, boolean nullSafe) {
        if (keys == null && nullSafe)
            return null;

        List<String> result = new ArrayList();
        for (Key key : Iterables.filter(keys, Predicates.notNull())) {
            result.add(KeyToString(key));
        }
        return result;
    }

    public static Key StringToKey(String keyStr, boolean nullSafe) { // TODO: do this better with regex from configuration
        if (keyStr == null && nullSafe)
            return null;

        try {
            String decoded = Base64.decode(keyStr);

            String[] tmp = decoded.split(":");

            // Sanity check that current namespace is the same as key's namespace
            String currentNamespace = com.google.appengine.api.NamespaceManager.get();
            if (!tmp[0].equals(currentNamespace))
                throw new RuntimeException(String.format("Incompatible namespaces while creating keys. Namespace: %s, current: %s", tmp[0], currentNamespace));

            tmp = tmp[1].split("_");

            String kind = tmp[0];
            Long id = Long.parseLong(tmp[1]);

            return KeyFactory.createKey(kind, id);
        } catch (Exception e) {
            throw new RuntimeException("Failed to translate key: " + keyStr, e);
        }
    }

    public static Key StringToKey(String keyStr) {
        return StringToKey(keyStr, false);
    }

    public static List<Key> StringToKey(List<String> keyStrs, boolean nullSafe) {
        if (keyStrs == null && nullSafe)
            return null;

        List<Key> result = new ArrayList();
        for (String keyStr : Iterables.filter(keyStrs, Predicates.notNull())) {
            result.add(StringToKey(keyStr));
        }
        return result;
    }

    public static List<Key> StringToKey(List<String> keyStrs) {
        return StringToKey(keyStrs, false);
    }


    public static String readFile(String filePath) throws Exception{
        BufferedReader br = new BufferedReader(new FileReader(filePath));

        StringBuilder sb = new StringBuilder();
        String line = br.readLine();

        while (line != null) {
            sb.append(line);
            sb.append(",");
            line = br.readLine();
        }
        String everything = sb.toString();
        br.close();
        return everything;
    }

}

