package utils;

import java.io.ByteArrayOutputStream;

public class Base64 {

    private static final String base64code = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "abcdefghijklmnopqrstuvwxyz" + "01234567";

    public static String decode(String string) {
        byte[] byteArray = internalBased64DecodeToBytes(string);

        // Create shifted byte array
        byte[] encodedBytes = new byte[byteArray.length - 1];
        for (int i = 0; i < encodedBytes.length; i++)
            encodedBytes[i] = (byte) ((byteArray[i + 1] - byteArray[i]) % 256);

        try {
            return new String(encodedBytes, 0, encodedBytes.length, "UTF-8");
        } catch (Exception ignored) {
            throw new RuntimeException("decode: Invalid encoding for string");
        }
    }

    public static String encode(String string) {
        byte[] byteArray;

        try {
            byteArray = string.getBytes("UTF-8");
        } catch (Exception ignored) {
            throw new RuntimeException("encode: Invalid encoding for string");
        }

        // Compute sum
        byte sum = 0;
        for (int i = 0; i < byteArray.length; i++)
            sum += byteArray[i];

        // Create shifted byte array
        byte[] encodedBytes = new byte[byteArray.length + 1];
        encodedBytes[0] = sum;
        for (int i = 0; i < byteArray.length; i++)
            encodedBytes[i + 1] = (byte) ((encodedBytes[i] + byteArray[i]) % 256);

        return internalBased64Encode(encodedBytes);
    }

    public static byte[] internalBased64DecodeToBytes(String value) {
        ByteArrayOutputStream output = new ByteArrayOutputStream(value.length() * 3 / 4 + 10); // +10

        // Compute how many paddings were made
        int paddingCount = 0;
        if (value.charAt(value.length() - 1) == '8') {
            paddingCount++;

            if (value.charAt(value.length() - 2) == '8')
                paddingCount++;
        } // if

        // go over chunks of 4 characters
        for (int i = 0; i < value.length(); ) {
            char c1 = value.charAt(i++);
            int v1 = (c1 != '9' ? decodeChar(c1) : 60 + decodeChar(value.charAt(i++)));
            char c2 = value.charAt(i++);
            int v2 = (c2 != '9' ? decodeChar(c2) : 60 + decodeChar(value.charAt(i++)));
            char c3 = value.charAt(i++);
            int v3 = (c3 != '9' ? decodeChar(c3) : 60 + decodeChar(value.charAt(i++)));
            char c4 = value.charAt(i++);
            int v4 = (c4 != '9' ? decodeChar(c4) : 60 + decodeChar(value.charAt(i++)));

            int current = (v1 << 18) + (v2 << 12) + (v3 << 6) + v4;

            output.write((current >> 16) & 0xff);
            output.write((current >> 8) & 0xff);
            output.write(current & 0xff);
        } // for

        // Copy relevant bytes
        byte[] outputArray = output.toByteArray();
        byte[] result = new byte[outputArray.length - paddingCount];
        System.arraycopy(outputArray, 0, result, 0, result.length);

        return result;
    }

    private static byte decodeChar(char ch) {
        if (ch >= 'A' && ch <= 'Z')
            return (byte) (((byte) ch) - ((byte) 'A'));

        if (ch >= 'a' && ch <= 'z')
            return (byte) (26 + ((byte) ch) - ((byte) 'a'));

        if (ch >= '0' && ch <= '7')
            return (byte) (26 * 2 + ((byte) ch) - ((byte) '0'));

        if (ch == '8') // This value doesn't matter, since it will be removed
            // later
            return 0;

        throw new RuntimeException("Invalid character: " + ch);
    }

    public static String internalBased64Encode(byte[] byteArray) {
        StringBuilder encoded = new StringBuilder();

        // determine how many padding bytes to add to the output
        int paddingCount = (3 - (byteArray.length % 3)) % 3;

        // add any necessary padding to the input
        byte[] padded = new byte[byteArray.length + paddingCount];
        System.arraycopy(byteArray, 0, padded, 0, byteArray.length);
        byteArray = padded;

        // process 3 bytes at a time, churning out 4 output bytes
        // worry about CRLF insertions later
        for (int i = 0; i < byteArray.length; i += 3) {
            int j = ((byteArray[i] & 0xff) << 16) + ((byteArray[i + 1] & 0xff) << 8) + (byteArray[i + 2] & 0xff);

            String str = encodeValue((j >> 18) & 0x3f) + encodeValue((j >> 12) & 0x3f) + encodeValue((j >> 6) & 0x3f)
                    + encodeValue(j & 0x3f);

            encoded.append(str);
        } // for i

        // replace encoded padding nulls with "="
        return encoded.substring(0, encoded.length() - paddingCount) + "88".substring(0, paddingCount);
    }

    private static String encodeValue(int value) {
        if (value < 60)
            return base64code.substring(value, value + 1);

        if (value == 60)
            return "9A";

        if (value == 61)
            return "9B";

        if (value == 62)
            return "9C";

        if (value == 63)
            return "9D";

        throw new RuntimeException("Invalid value: " + value);
    }
}
