package utils;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import utils.Base64;

import java.util.ArrayList;
import java.util.List;

public final class KeyUtils {

    public static String keyToString(Key key, boolean nullSafe) {    // TODO: do this better with template from configuration
        if (key == null && nullSafe)
            return null;

        String result = String.format("%s:%s_%d", key.getNamespace(), key.getKind(), key.getId());
        return Base64.encode(result);
    }

    public static String keyToString(Key key) {
        return keyToString(key, false);
    }

    public static List<String> keyToString(List<Key> keys) {
        return keyToString(keys, false);
    }

    public static List<String> keyToString(List<Key> keys, boolean nullSafe) {
        if (keys == null && nullSafe)
            return null;

        List<String> result = new ArrayList();
        for (Key key : Iterables.filter(keys, Predicates.notNull())) {
            result.add(keyToString(key));
        }
        return result;
    }

    public static Key stringToKey(String keyStr, boolean nullSafe) { // TODO: do this better with regex from configuration
        if (keyStr == null && nullSafe)
            return null;

        try {
            String decoded = Base64.decode(keyStr);

            String[] tmp = decoded.split(":");

            // Sanity check that current namespace is the same as key's namespace
            String currentNamespace = com.google.appengine.api.NamespaceManager.get();
            if (!tmp[0].equals(currentNamespace))
                throw new RuntimeException(String.format("Incompatible namespaces while creating keys. Namespace: %s, current: %s", tmp[0], currentNamespace));

            tmp = tmp[1].split("_");

            String kind = tmp[0];
            Long id = Long.parseLong(tmp[1]);

            return KeyFactory.createKey(kind, id);
        } catch (Exception e) {
            throw new RuntimeException("Failed to translate key: " + keyStr, e);
        }
    }

    public static Key stringToKey(String keyStr) {
        return stringToKey(keyStr, false);
    }

    public static List<Key> stringToKey(List<String> keyStrs, boolean nullSafe) {
        if (keyStrs == null && nullSafe)
            return null;

        List<Key> result = new ArrayList();
        for (String keyStr : Iterables.filter(keyStrs, Predicates.notNull())) {
            result.add(stringToKey(keyStr));
        }
        return result;
    }

    public static List<Key> stringToKey(List<String> keyStrs) {
        return stringToKey(keyStrs, false);
    }

}