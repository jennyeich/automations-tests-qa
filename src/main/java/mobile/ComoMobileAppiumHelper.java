package mobile;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import utils.PropsReader;

import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by tatiana on 4/4/17.
 */
public class ComoMobileAppiumHelper {
    public static final String IOS = "ios";
//  public static final String ANDROID="Android";
    public static final Logger log4j = Logger.getLogger(ComoMobileAppiumHelper.class);

    AppiumDriver driver;
    private static ComoMobileAppiumHelper instance = null;
    private  String platform = null;
    public enum FindElemType {FIND_ELEM_BY_XPATH, FIND_ELEM_BY_ID}


    protected ComoMobileAppiumHelper(String platform){
        this.platform=platform;
        try {
            if (IOS.equals(platform)){
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setCapability(CapabilityType.VERSION, PropsReader.getPropValuesForMobile(CapabilityType.VERSION));
                capabilities.setCapability("deviceName", PropsReader.getPropValuesForMobile("deviceNameIos"));
                capabilities.setCapability("platformName", PropsReader.getPropValuesForMobile("platformNameIos"));
                capabilities.setCapability("appActivity", PropsReader.getPropValuesForMobile("appActivityIos"));
                capabilities.setCapability("app", PropsReader.getPropValuesForMobile("appIos"));
                capabilities.setCapability("automationName", PropsReader.getPropValuesForMobile("automationNameIos"));
                capabilities.setCapability("udid", PropsReader.getPropValuesForMobile("udidIos"));
                driver = new IOSDriver(new URL(PropsReader.getPropValuesForMobile("appium.server.urlIos")), capabilities);
            }
            else {
                DesiredCapabilities capabilities = new DesiredCapabilities();
                capabilities.setCapability(CapabilityType.VERSION, PropsReader.getPropValuesForMobile(CapabilityType.VERSION));
                capabilities.setCapability("deviceName", PropsReader.getPropValuesForMobile("deviceName"));
                capabilities.setCapability("platformName", PropsReader.getPropValuesForMobile("platformName"));
                capabilities.setCapability("appPackage", PropsReader.getPropValuesForMobile("appPackage")); // "led.android.hm"
                capabilities.setCapability("appActivity", PropsReader.getPropValuesForMobile("appActivity"));
                driver = new AndroidDriver(new URL(PropsReader.getPropValuesForMobile("appium.server.url")), capabilities);
            }

        } catch (Exception e) {
            log4j.error("ERROR: " + e.getMessage(), e);
        }
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
    }

    public static ComoMobileAppiumHelper getInstance(String platform){
        if(instance == null){
            instance = new ComoMobileAppiumHelper(platform);
        }
        return instance;
    }

    public AppiumDriver getDriver(){
        return driver;
    }


    public WebElement getElement(String webElemId, FindElemType type  ){
        if (IOS.equals(platform))
            return driver.findElementByAccessibilityId(webElemId);

        else
            //for Android platform
            switch(type) {
                case FIND_ELEM_BY_XPATH:
                    return driver.findElement(By.xpath("//android.widget.TextView[@text='"+webElemId+"']"));//tap to start button
                case FIND_ELEM_BY_ID:
                    return driver.findElement(By.id("led.android.rammsteinqa:id/id"+webElemId));
            }
            return null; //default we should never arrive to this part of coe
    }


    public WebElement getTapToStartElem(){
        //tap to start button
        return getElement("Tap to Start",FindElemType.FIND_ELEM_BY_XPATH);
    }


    public WebElement getOkElem(){  //only for ios
        return driver.findElementByAccessibilityId("OK");
    }


//FOR FUTURE NEEDS NEED TO USE getElement func

//    public WebElement getCatalogElem(){
//        // catalog tile - main screen tile 1
//        return getElement("2",FindElemType.FIND_ELEM_BY_ID);
//    }
//
//    public WebElement getCategoryElem(){
//        //category tile
//        return getElement("Discs",FindElemType.FIND_ELEM_BY_XPATH);
//    }
//
//    public WebElement getItem1(){
//        // item with fixed amount
//        return getElement("Rammstein Paris Bersy",FindElemType.FIND_ELEM_BY_XPATH);
//    }
//    public WebElement getGenericActionButton(){
//        return driver.findElement(By.id("led.android.rammsteinqa:id/id3")); //generic Action pay with redirect button (left button on catalog item)
//    }
//
//    public WebElement getPayButton(){
//        return driver.findElement(By.id("led.android.rammsteinqa:id/id3")); // pay button in amount screen
//    }
//
//    public WebElement getCardHolderName(){
//         return driver.findElement(By.xpath("//*[@id='cardHolderName']")); //web view: card holder name
//
//   }
//
//    public WebElement getCardNum(){
//        return driver.findElement(By.xpath("//*[@id='cardNumber']"));//web view: card number
//    }
//
//    public WebElement getMonth(){
//        return driver.findElement(By.xpath("//*[@id='month']"));//web view: month
//    }
//
//
//    public WebElement getCheckmonth(){
//        return driver.findElement(By.xpath("//android.widget.CheckedTextView[@text='02']")); //web view: check month 02
//    }
//
//
//    public WebElement getYear(){
//        return driver.findElement(By.xpath("//*[@id='year']"));//web view: year
//    }
//
//
//    public WebElement getCheckYear(){
//        return driver.findElement(By.xpath("//android.widget.CheckedTextView[@text='2023']"));//web view: check year 2023
//    }
//
//    public WebElement getCVV(){
//        return driver.findElement(By.xpath("//*[@id='cvvNumber']"));//web view: CVV
//    }
//
//    public WebElement getSaveCard(){
//        return driver.findElement(By.xpath("//*[@id='submit']"));//web view: save card - pay
//    }
//
//    public WebElement getBackButton(){
//        return driver.findElement(By.id("led.android.rammsteinqa:id/id1")); //header status tile: back button
//    }
//
//    public WebElement getItem2(){
//        return driver.findElement(By.xpath("//android.widget.TextView[@text='MIG tour']")); //item with custom amount
//    }
//
//    public WebElement getCancelButton(){
//        return driver.findElement(By.id("android:id/button2")); //zero amount alert: cancel button
//    }
//
//    public WebElement getPriceCustomAmount(){
//        return driver.findElement(By.xpath("//android.widget.TextView[@text='$0.00']")); //amount view at custom amount screen
//    }
//
//    public WebElement getOneNumberPad(){
//        return driver.findElement(By.xpath("//android.widget.TextView[@text='1']")); //number pad: number 1 button
//    }
//
//    public WebElement getZeroNumberPad(){
//        return driver.findElement(By.xpath("//android.widget.TextView[@text='0']"));//number pad: number 0 button
//    }
//
//    public WebElement getEnterNumberPad(){
//        return driver.findElement(By.xpath("//android.widget.Button[@text=' ENTER ']")); //number pad: ENTER button
//    }
//
//    public WebElement getExistingCard(){
//        return driver.findElement(By.xpath("//android.widget.TextView[@text='**** **** **** 4580']")); // existing card
//    }
//
//    public WebElement getPayAlertYesButton(){
//        return driver.findElement(By.id("android:id/button3")); //pay alert: yes button
//    }
//
//    public WebElement getBackToHome(){
//        return driver.findElement(By.id("led.android.rammsteinqa:id/id4")); //Success zooz screen: back to home button
//    }
//
//    public WebElement getDeleteButton(){
//        return driver.findElement(By.id("led.android.rammsteinqa:id/id4")); //existing card: delete button
//    }
//
//    public WebElement getDeleteAlertYes(){
//        return driver.findElement(By.id("android:id/button3")); //delete alert: yes button
//    }
//
//    public WebElement getZoozItemElem(){
//        return driver.findElement(By.id("led.android.rammsteinqa:id/id3")); // zooz action - main screen tile 2
//    }
//
//    public WebElement getSideMenuButton(){
//        if (platform == "ios")
//            return driver.findElementByAccessibilityId("20"); // side menu button
//        else
//            return driver.findElement(By.id("led.android.rammsteinqa:id/id20")); // side menu button
//    }
//
//    public WebElement getSecondElemSideMenu(){
//        return driver.findElement(By.id("led.android.rammsteinqa:id/id14")); // side menu - second element: zooz item
//    }
//
//    public WebElement getFirstElemSideMenu(){
//        return driver.findElement(By.id("led.android.rammsteinqa:id/id13")); // side menu - second element: go to home screen
//    }


}
