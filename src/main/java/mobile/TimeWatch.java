package mobile;

/**
 * Created by tatiana on 7/6/17.
 */

import java.util.concurrent.TimeUnit;


public class TimeWatch {
    private static final String TAG = "TimeWatch";
    long starts;

    public static TimeWatch start() {
        return new TimeWatch();
    }

    private TimeWatch() {
        reset();
    }

    public TimeWatch reset() {
        starts = System.currentTimeMillis();
        return this;
    }

    public long time() {
        long ends = System.currentTimeMillis();
        return ends - starts;
    }

    public long time(TimeUnit unit) {
        return unit.convert(time(), TimeUnit.MILLISECONDS);
    }

    public long time(String message) {
        long time = time();
//        Log.d(TAG, message + " time: " + time);
        return time;
    }

    public long time(String message, TimeUnit unit) {
        long time = time(unit);
//        Log.d(TAG, message + " time: " + time);
        return time;
    }
}
