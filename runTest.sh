export ANDROID_HOME=/Users/jenkins_user/Library/Android/sdk/
cd client/Automations-Tests-QA/
echo start Appium
appium &
echo start tests
mvn -Dtest=TimeMeasurement test
#mvn -Dtest=TimeMeasurement -Dplatform="Android" test
echo kill Appium process
pkill -f "appium"
