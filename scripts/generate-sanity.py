#!/usr/bin/python

####################################################
#  13-generate-saniy-file.py                       #
####################################################

import sys,os,re
from shutil import copyfile

projname = sys.argv[1]
businessname = sys.argv[2]
isnewapp = sys.argv[3]
appname = sys.argv[4]
apikey = sys.argv[5]
locationid = sys.argv[6]
uidDebugLog = sys.argv[7]


if businessname is None:
    businessname = "None"

file_source = '13-env.properties.tmp'
file_target = 'env.properties'

def copy():
    copyfile(file_source, file_target)

def inplace_change(filename_target, old_string, new_string):
    with open(filename_target) as f:
        s = f.read()
        if old_string not in s:
            print ('"{old_string}" not found in {filename_target}.'.format(**locals()))
            return

    with open(filename_target, 'w') as f:
        print ('Changing "{old_string}" to "{new_string}" in {filename_target}'.format(**locals()))
        s = s.replace(old_string, new_string)
        f.write(s)

def replace(projname):
    inplace_change(file_target,"PROJECT", projname)
    inplace_change(file_target,"BUSINESS", businessname)
    inplace_change(file_target,"APP_NAME", appname)
    inplace_change(file_target,"API_KEY", apikey)
    inplace_change(file_target,"LOCATION_ID", locationid)
    inplace_change(file_target,"IS_NEW_APP", isnewapp)
    inplace_change(file_target,"UI_DEBUG_LOG", uidDebugLog)


if __name__ == "__main__":
    copy()
    replace(projname)