#!/usr/bin/python

####################################################
#  generate-tests.py                       #
####################################################

import sys,os,re,subprocess
from shutil import copyfile
import json
from collections import namedtuple
from io import StringIO
import io

file_tests = sys.argv[1]
workspace = sys.argv[2]
buf = StringIO()

with open(file_tests) as json_file:
    data = json.load(json_file)
    print(data)
    for i in range(len(data)):
           print('pkg: ' + data[i]['pkg'])
           print('className: ' + data[i]['className'])
           print('testcase: ' + data[i]['testcase'])
           print('')
           package = data[i]['pkg']
           className = data[i]['className']
           methodName = data[i]['testcase']
           fullTestClass = package + "." + className
           buf.write(fullTestClass)
           buf.write('#')
           buf.write(methodName)
           buf.write(',')


testToRun = buf.getvalue()
testToRun = testToRun[:-1]
print('testToRun: ' + testToRun)

command="mvn -Dtest=" + testToRun + " test"
print('command: ' + command)
os.chdir(workspace)
os.system('pwd')
os.system(command)





