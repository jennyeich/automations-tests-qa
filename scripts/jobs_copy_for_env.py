#!/usr/bin/python

####################################################
#  jobs_copy.py                       #
####################################################


import jenkins
import os, sys
from ConfigParser import SafeConfigParser
from view_config import JenkinsView
import xmltodict



def get_view_config_dic(server, view_name):
    view_config = server.get_view_config(view_name)
    doc = xmltodict.parse(view_config)
    return doc


def get_view_list(server):
    views = server.get_views()
    view_list = []
    for view in views:
        view_list.append(view['name'].rstrip())
    return view_list

def add_job_to_view(server, view_doc, job_name, view_name):
    (view_doc['hudson.model.ListView']['jobNames']['string']).append(job_name)
    (view_doc['hudson.model.ListView']['jobNames']['string']).sort()
    server.reconfig_view(view_name, xmltodict.unparse(view_doc, pretty=True))

#def pretty(d, indent=0):
   #for key, value in d.iteritems():
      #print '\t' * indent + str(key)
      #if isinstance(value, dict):
        # pretty(value, indent+1)
      #else:
         #print '\t' * (indent+1) + str(value)

def get_job_configs(server, job_name):
    job_config = server.get_job_config(job_name)
    doc = xmltodict.parse(job_config)
    return doc

def get_jobs_list(server, job_name_pattern="_nightly"):
        jobs = server.get_jobs()
        job_list = []
        for job in jobs:
            if job_name_pattern in job['fullname']:  job_list.append(job['fullname'].rstrip())
            else:
                continue
        return job_list

def get_job_executer_line(job_config):
     command_lines = (job_config['project']['builders']['hudson.tasks.Shell']['command']).split('\n')
     return command_lines[-1]

def replace_command(job_config, new_execute_command):
    command_line = (job_config['project']['builders']['hudson.tasks.Shell']['command']).split('\n')
    command_line[-1] = new_execute_command
    slash = '\n'
    new_command = slash.join(command_line)
    job_config['project']['builders']['hudson.tasks.Shell']['command'] = new_command
    return job_config

def create_new_job(job_name, job_config, server):
    xml = xmltodict.unparse(job_config,pretty=True)
    server.create_job(job_name, xml)


if __name__ == '__main__':
    server = jenkins.Jenkins('http://54.77.156.99/', username='goni', password='NoaTamar17')
    job_list = get_jobs_list(server)

    #job_command = {}
    #for job in job_list:
        #job_config = get_job_configs(server,job)
        #command = get_job_executer_line(job_config)
        #print (command)
        #job_command[job.replace("_nightly1","_nightly")] = command

    #job_config_initializer = get_job_configs(server,'Consent-Suite_nightly')
    #view_config = get_view_config_dic(server, 'QA-Nightly')

   #for job_name, command in job_command.items():
       # print (command)
        #new_job_config = replace_command(job_config_initializer,command)
        #print (job_name)
        #create_new_job(job_name, new_job_config, server)
        # add_job_to_view(server, view_config, job_name, 'QA-Nightly')
